.class Lcom/google/android/location/c/m;
.super Lcom/google/android/location/c/E;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/c/E;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/d/a;ZLcom/google/android/location/c/k;ZLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/d/a;",
            "Z",
            "Lcom/google/android/location/c/k;",
            "Z",
            "Lcom/google/android/location/c/l;",
            "Lcom/google/android/location/k/a/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    move-object/from16 v0, p8

    move-object/from16 v1, p9

    invoke-direct {p0, p1, p6, v0, v1}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p1

    move-object v8, p6

    .line 58
    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/c/m;->a(Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/d/a;ZLandroid/content/Context;Lcom/google/android/location/c/k;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/c/m;->c:Ljava/util/List;

    .line 64
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/location/c/m;->d:Z

    .line 65
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/d/a;ZLandroid/content/Context;Lcom/google/android/location/c/k;)Ljava/util/List;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/d/a;",
            "Z",
            "Landroid/content/Context;",
            "Lcom/google/android/location/c/k;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/c/E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 78
    sget-object v0, Lcom/google/android/location/c/F;->b:Lcom/google/android/location/c/F;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 79
    const-string v0, "phone"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 81
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_29

    .line 85
    new-instance v0, Lcom/google/android/location/c/i;

    iget-object v4, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    iget-object v5, p0, Lcom/google/android/location/c/m;->a:Lcom/google/android/location/k/a/c;

    move-object v1, p5

    move-object v3, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/i;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_29
    sget-object v0, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 92
    const-string v0, "wifi"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 93
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 94
    if-eqz p4, :cond_ba

    invoke-static {}, Lcom/google/android/location/d/b;->a()Lcom/google/android/location/d/b;

    move-result-object v3

    .line 96
    :goto_45
    new-instance v0, Lcom/google/android/location/c/M;

    iget-object v4, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    iget-object v5, p0, Lcom/google/android/location/c/m;->a:Lcom/google/android/location/k/a/c;

    move-object v1, p5

    move-object v2, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/M;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/d/e;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_53
    sget-object v0, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 102
    sget-object v0, Lcom/google/android/location/c/F;->h:Lcom/google/android/location/c/F;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 103
    if-nez v2, :cond_63

    if-eqz v3, :cond_82

    .line 104
    :cond_63
    const-string v0, "location"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 106
    :try_start_6b
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 107
    new-instance v0, Lcom/google/android/location/c/p;

    iget-object v6, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    iget-object v7, p0, Lcom/google/android/location/c/m;->a:Lcom/google/android/location/k/a/c;

    move-object v1, p5

    move-object v4, p3

    move-object v5, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/c/p;-><init>(Landroid/content/Context;ZZLcom/google/android/location/d/a;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_82
    .catch Ljava/lang/SecurityException; {:try_start_6b .. :try_end_82} :catch_e9

    .line 118
    :cond_82
    :goto_82
    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 119
    const-string v0, "sensor"

    invoke-virtual {p5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 121
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_96
    :goto_96
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_bf

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 122
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v4

    .line 123
    if-eqz v4, :cond_b2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_96

    .line 125
    :cond_b2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_96

    .line 94
    :cond_ba
    invoke-static {}, Lcom/google/android/location/d/d;->a()Lcom/google/android/location/d/d;

    move-result-object v3

    goto :goto_45

    .line 128
    :cond_bf
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 129
    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c3

    .line 131
    :cond_d3
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e8

    .line 132
    new-instance v0, Lcom/google/android/location/c/G;

    iget-object v4, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    iget-object v5, p0, Lcom/google/android/location/c/m;->a:Lcom/google/android/location/k/a/c;

    move-object v1, p5

    move-object v2, p2

    move-object v3, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/G;-><init>(Landroid/content/Context;Ljava/util/Map;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_e8
    return-object v8

    .line 112
    :catch_e9
    move-exception v0

    goto :goto_82
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/location/c/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/E;

    .line 155
    invoke-virtual {v0}, Lcom/google/android/location/c/E;->d()V

    goto :goto_6

    .line 157
    :cond_16
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/location/c/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/E;

    .line 162
    invoke-virtual {v0}, Lcom/google/android/location/c/E;->e()V

    goto :goto_6

    .line 165
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_1f

    .line 166
    iget-object v0, p0, Lcom/google/android/location/c/m;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->a()V

    .line 168
    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/location/c/m;->d:Z

    if-eqz v0, :cond_2a

    .line 169
    invoke-virtual {p0}, Lcom/google/android/location/c/m;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/c/k;->a()V

    .line 171
    :cond_2a
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/location/c/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
