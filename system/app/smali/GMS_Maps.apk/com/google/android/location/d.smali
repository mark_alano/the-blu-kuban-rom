.class public Lcom/google/android/location/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/d$1;,
        Lcom/google/android/location/d$a;
    }
.end annotation


# static fields
.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Lcom/google/android/location/d$a;

.field b:Z

.field c:Z

.field d:Z

.field e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

.field f:J

.field g:J

.field h:Z

.field i:Z

.field j:J

.field k:Lcom/google/android/location/i/c$a;

.field private final m:Lcom/google/android/location/os/i;

.field private final n:Lcom/google/android/location/a/b;

.field private final o:Lcom/google/android/location/i/a;

.field private final p:Lcom/google/android/location/os/h;

.field private final q:Ljava/util/Random;

.field private final r:Ljava/util/Calendar;

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/16 v5, 0x1e

    const/4 v4, 0x0

    .line 48
    new-instance v0, Lcom/google/common/collect/aw;

    invoke-direct {v0}, Lcom/google/common/collect/aw;-><init>()V

    new-instance v1, Lcom/google/android/location/k/b;

    const/4 v2, 0x7

    const/16 v3, 0xa

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/k/b;

    const/16 v2, 0xf

    const/16 v3, 0x13

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/d;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;Ljava/util/Random;)V

    .line 171
    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;Ljava/util/Random;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    .line 104
    sget-object v0, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    .line 106
    iput-boolean v1, p0, Lcom/google/android/location/d;->s:Z

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->t:Z

    .line 108
    iput-boolean v1, p0, Lcom/google/android/location/d;->u:Z

    .line 114
    iput-boolean v1, p0, Lcom/google/android/location/d;->b:Z

    .line 121
    iput-boolean v1, p0, Lcom/google/android/location/d;->c:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/android/location/d;->d:Z

    .line 126
    iput-object v4, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    .line 129
    iput-wide v2, p0, Lcom/google/android/location/d;->f:J

    .line 133
    iput-wide v2, p0, Lcom/google/android/location/d;->g:J

    .line 136
    iput-boolean v1, p0, Lcom/google/android/location/d;->h:Z

    .line 140
    iput-boolean v1, p0, Lcom/google/android/location/d;->i:Z

    .line 144
    iput-wide v2, p0, Lcom/google/android/location/d;->j:J

    .line 146
    iput-object v4, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    .line 161
    iput-object p4, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/a/b;

    .line 162
    iput-object p1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    .line 163
    iput-object p3, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/os/h;

    .line 164
    iput-object p2, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/i/a;

    .line 165
    iput-object p5, p0, Lcom/google/android/location/d;->q:Ljava/util/Random;

    .line 166
    return-void
.end method

.method private a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;
    .registers 5
    .parameter

    .prologue
    .line 179
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 180
    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 184
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private a(JJ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    add-long v1, p3, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 241
    const/4 v1, 0x0

    .line 242
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 243
    iget-object v3, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Lcom/google/android/location/k/b;->a(Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 248
    :goto_22
    iget-object v1, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/i/a;

    const-wide/32 v2, 0xe290

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/location/i/a;->e(JZ)Z

    move-result v1

    .line 250
    if-eqz v0, :cond_30

    if-nez v1, :cond_3a

    .line 253
    :cond_30
    if-nez v1, :cond_32

    .line 256
    :cond_32
    if-nez v0, :cond_34

    .line 259
    :cond_34
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;J)V

    .line 263
    :goto_39
    return-void

    .line 261
    :cond_3a
    iget-object v1, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-direct {p0, v1, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    goto :goto_39

    :cond_40
    move-object v0, v1

    goto :goto_22
.end method

.method private a(Ljava/util/Calendar;J)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 316
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 317
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 318
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    .line 319
    return-void
.end method

.method private a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 274
    invoke-virtual {p2, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 275
    iget-wide v0, p2, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 277
    :cond_b
    iget-wide v0, p0, Lcom/google/android/location/d;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_17

    .line 279
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    .line 296
    :goto_16
    return-void

    .line 281
    :cond_17
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    .line 282
    iget-wide v2, p0, Lcom/google/android/location/d;->g:J

    const-wide/32 v4, 0x927c0

    add-long/2addr v2, v4

    .line 283
    cmp-long v0, v2, v0

    if-lez v0, :cond_2b

    .line 285
    add-long v0, p3, v2

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 289
    :cond_2b
    invoke-virtual {p2, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 290
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;J)V

    goto :goto_16

    .line 293
    :cond_35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/d;->b(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    goto :goto_16
.end method

.method private a(J)Z
    .registers 7
    .parameter

    .prologue
    .line 192
    iget-wide v0, p0, Lcom/google/android/location/d;->f:J

    const-wide/16 v2, 0x7d0

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private a(JZ)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 467
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->g(J)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 468
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/d;->b(JZ)V

    .line 469
    const/4 v0, 0x1

    .line 471
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private b(JJ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 520
    iput-wide p1, p0, Lcom/google/android/location/d;->f:J

    .line 521
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 523
    return-void
.end method

.method private b(JZ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 475
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 476
    iput-wide v4, p0, Lcom/google/android/location/d;->f:J

    .line 477
    invoke-direct {p0}, Lcom/google/android/location/d;->f()V

    .line 478
    iput-boolean v2, p0, Lcom/google/android/location/d;->d:Z

    .line 479
    iput-object v3, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    .line 480
    iput-boolean v2, p0, Lcom/google/android/location/d;->b:Z

    .line 481
    iput-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    .line 482
    if-eqz p3, :cond_1e

    .line 483
    iput-wide v4, p0, Lcom/google/android/location/d;->j:J

    .line 484
    iput-object v3, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    .line 486
    :cond_1e
    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_2a

    .line 487
    sget-object v0, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    .line 488
    iput-wide p1, p0, Lcom/google/android/location/d;->g:J

    .line 490
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    .line 491
    return-void
.end method

.method private b(Ljava/util/Calendar;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 516
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/google/android/location/d;->b(JJ)V

    .line 517
    return-void
.end method

.method private b(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 304
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_23

    .line 305
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {p1, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 306
    sget-object v0, Lcom/google/android/location/d;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;Lcom/google/android/location/k/b;J)V

    .line 310
    :goto_22
    return-void

    .line 308
    :cond_23
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;J)V

    goto :goto_22
.end method

.method private b(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 196
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 233
    :goto_8
    return v0

    .line 202
    :cond_9
    iget-object v2, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    .line 203
    iget-wide v4, p0, Lcom/google/android/location/d;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5c

    .line 204
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v4

    if-eqz v4, :cond_52

    .line 207
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/google/android/location/d;->f:J

    add-long/2addr v4, v2

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 208
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-direct {p0, v0}, Lcom/google/android/location/d;->a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;

    move-result-object v0

    .line 209
    if-nez v0, :cond_32

    .line 210
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/d;->b(JZ)V

    move v0, v1

    .line 211
    goto :goto_8

    .line 216
    :cond_32
    invoke-direct {p0}, Lcom/google/android/location/d;->c()V

    .line 217
    invoke-direct {p0}, Lcom/google/android/location/d;->e()V

    .line 219
    iget-object v4, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    iget-wide v5, v0, Lcom/google/android/location/k/b;->b:J

    invoke-static {v4, v5, v6}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 220
    iget-object v0, p0, Lcom/google/android/location/d;->r:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/google/android/location/d;->b(JJ)V

    .line 221
    sget-object v0, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    iput-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    .line 222
    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    move v0, v1

    .line 223
    goto :goto_8

    .line 226
    :cond_52
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v2, 0xa

    iget-wide v3, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_8

    .line 232
    :cond_5c
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/location/d;->a(JJ)V

    goto :goto_8
.end method

.method private c()V
    .registers 3

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/google/android/location/d;->i:Z

    if-nez v0, :cond_e

    .line 424
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 425
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->i:Z

    .line 427
    :cond_e
    return-void
.end method

.method private c(J)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 326
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 345
    :goto_7
    return v0

    .line 329
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/location/d;->d:Z

    if-eqz v1, :cond_25

    .line 330
    iget-object v1, p0, Lcom/google/android/location/d;->q:Ljava/util/Random;

    const v2, 0x2bf20

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 332
    int-to-long v1, v1

    add-long/2addr v1, p1

    iget-object v3, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/location/d;->b(JJ)V

    .line 333
    sget-object v1, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_7

    .line 337
    :cond_25
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 340
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    goto :goto_7

    .line 344
    :cond_2f
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 345
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private d()V
    .registers 3

    .prologue
    .line 434
    iget-boolean v0, p0, Lcom/google/android/location/d;->i:Z

    if-eqz v0, :cond_e

    .line 435
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 436
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/d;->i:Z

    .line 438
    :cond_e
    return-void
.end method

.method private d(J)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 354
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 367
    :goto_7
    return v0

    .line 357
    :cond_8
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/d;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 360
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    sget-object v2, Lcom/google/android/location/os/i$b;->a:Lcom/google/android/location/os/i$b;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 361
    invoke-direct {p0}, Lcom/google/android/location/d;->f()V

    .line 362
    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_7

    .line 366
    :cond_1e
    iget-object v0, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 367
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private e()V
    .registers 3

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    if-nez v0, :cond_e

    .line 498
    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/a/b;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->b(I)V

    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    .line 502
    :cond_e
    return-void
.end method

.method private e(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 377
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 416
    :goto_8
    return v0

    .line 380
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    if-eqz v2, :cond_15

    iget-boolean v2, p0, Lcom/google/android/location/d;->t:Z

    if-eqz v2, :cond_15

    .line 384
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    goto :goto_8

    .line 387
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/location/d;->c:Z

    if-eqz v2, :cond_6f

    iget-boolean v2, p0, Lcom/google/android/location/d;->t:Z

    if-nez v2, :cond_6f

    .line 388
    invoke-direct {p0}, Lcom/google/android/location/d;->c()V

    .line 389
    iget-object v2, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/i/a;

    invoke-virtual {v2}, Lcom/google/android/location/i/a;->a()J

    move-result-wide v2

    .line 390
    const-wide/32 v4, 0x1d4c0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 391
    const-wide/32 v4, 0xe290

    cmp-long v4, v2, v4

    if-gez v4, :cond_3b

    .line 394
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    .line 395
    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    goto :goto_8

    .line 399
    :cond_3b
    iget-object v4, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/i/a;

    invoke-virtual {v4, v2, v3, v1}, Lcom/google/android/location/i/a;->c(JZ)Lcom/google/android/location/i/c$a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    .line 400
    iget-object v1, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    if-nez v1, :cond_4e

    .line 403
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->b(JZ)V

    .line 404
    invoke-direct {p0}, Lcom/google/android/location/d;->d()V

    goto :goto_8

    .line 407
    :cond_4e
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    add-long v1, v4, v2

    iput-wide v1, p0, Lcom/google/android/location/d;->j:J

    .line 409
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const-string v2, "BurstCollectionTrigger"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 410
    iget-wide v1, p0, Lcom/google/android/location/d;->j:J

    iget-object v3, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/location/d;->b(JJ)V

    .line 411
    sget-object v1, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    iput-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    goto :goto_8

    :cond_6f
    move v0, v1

    .line 416
    goto :goto_8
.end method

.method private f()V
    .registers 3

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    if-eqz v0, :cond_e

    .line 509
    iget-object v0, p0, Lcom/google/android/location/d;->n:Lcom/google/android/location/a/b;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->c(I)V

    .line 511
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/d;->h:Z

    .line 513
    :cond_e
    return-void
.end method

.method private f(J)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 445
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/d;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 447
    iget-wide v1, p0, Lcom/google/android/location/d;->j:J

    sub-long/2addr v1, p1

    .line 448
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_17

    .line 449
    iget-object v3, p0, Lcom/google/android/location/d;->o:Lcom/google/android/location/i/a;

    iget-object v4, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c$a;J)V

    .line 451
    :cond_17
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const-string v2, "BurstCollectionTrigger"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 452
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/d;->j:J

    .line 453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/d;->k:Lcom/google/android/location/i/c$a;

    .line 454
    const/4 v0, 0x1

    .line 459
    :goto_26
    return v0

    .line 458
    :cond_27
    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    const/16 v2, 0xa

    iget-wide v3, p0, Lcom/google/android/location/d;->f:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_26
.end method

.method private g()V
    .registers 7

    .prologue
    .line 526
    const/4 v0, 0x0

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    .line 529
    iget-object v2, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    .line 530
    sget-object v4, Lcom/google/android/location/d$1;->a:[I

    iget-object v5, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    invoke-virtual {v5}, Lcom/google/android/location/d$a;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_36

    .line 547
    :goto_16
    iget-object v2, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    if-eq v1, v2, :cond_1a

    .line 550
    :cond_1a
    if-nez v0, :cond_1

    .line 551
    return-void

    .line 532
    :pswitch_1d
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->b(J)Z

    move-result v0

    goto :goto_16

    .line 535
    :pswitch_22
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->c(J)Z

    move-result v0

    goto :goto_16

    .line 538
    :pswitch_27
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->d(J)Z

    move-result v0

    goto :goto_16

    .line 541
    :pswitch_2c
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->e(J)Z

    move-result v0

    goto :goto_16

    .line 544
    :pswitch_31
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/d;->f(J)Z

    move-result v0

    goto :goto_16

    .line 530
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_22
        :pswitch_27
        :pswitch_2c
        :pswitch_31
    .end packed-switch
.end method

.method private g(J)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 557
    iget-object v2, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    .line 558
    iget-object v2, p0, Lcom/google/android/location/d;->p:Lcom/google/android/location/os/h;

    invoke-virtual {v2}, Lcom/google/android/location/os/h;->i()Z

    .line 560
    iget-boolean v2, p0, Lcom/google/android/location/d;->s:Z

    if-eqz v2, :cond_10

    .line 566
    :cond_10
    sget-object v2, Lcom/google/android/location/d$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    invoke-virtual {v3}, Lcom/google/android/location/d$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_22

    move v0, v1

    .line 589
    :pswitch_1e
    if-eqz v0, :cond_20

    .line 597
    :cond_20
    return v0

    .line 566
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 650
    const/16 v0, 0xa

    if-ne p1, v0, :cond_7

    .line 651
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 653
    :cond_7
    return-void
.end method

.method public a(IIZ)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 621
    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    .line 622
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_24

    .line 623
    if-eqz p3, :cond_15

    float-to-double v1, v0

    const-wide v3, 0x3fc99999a0000000L

    cmpl-double v1, v1, v3

    if-gez v1, :cond_21

    :cond_15
    if-nez p3, :cond_28

    float-to-double v0, v0

    const-wide v2, 0x3fe3333340000000L

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_28

    :cond_21
    const/4 v0, 0x1

    :goto_22
    iput-boolean v0, p0, Lcom/google/android/location/d;->u:Z

    .line 626
    :cond_24
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 627
    return-void

    .line 623
    :cond_28
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_12

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_12

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-ne v0, v1, :cond_26

    .line 659
    :cond_12
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/d;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    .line 660
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v1, :cond_23

    .line 662
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->d:Z

    .line 664
    :cond_23
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 666
    :cond_26
    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 2
    .parameter

    .prologue
    .line 635
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 636
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 616
    iput-boolean p1, p0, Lcom/google/android/location/d;->t:Z

    .line 617
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 618
    return-void
.end method

.method public a()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 607
    iget-object v1, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v2, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-ne v1, v2, :cond_15

    iget-object v1, p0, Lcom/google/android/location/d;->m:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->m()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 608
    iput-boolean v0, p0, Lcom/google/android/location/d;->c:Z

    .line 609
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 612
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public b()V
    .registers 3

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/google/android/location/d;->a:Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    if-ne v0, v1, :cond_12

    .line 644
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/d;->b:Z

    .line 645
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 647
    :cond_12
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 630
    iput-boolean p1, p0, Lcom/google/android/location/d;->s:Z

    .line 631
    invoke-direct {p0}, Lcom/google/android/location/d;->g()V

    .line 632
    return-void
.end method
