.class final Lcom/google/android/location/e$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/os/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/os/g;",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/os/g;",
            "Lcom/google/android/location/e/E;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 600
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    .line 601
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    .line 602
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    .line 605
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    .line 629
    iput p1, p0, Lcom/google/android/location/e$a;->e:I

    .line 630
    iput p2, p0, Lcom/google/android/location/e$a;->f:I

    .line 631
    iput p3, p0, Lcom/google/android/location/e$a;->g:I

    .line 632
    return-void
.end method

.method private a(J)I
    .registers 7
    .parameter

    .prologue
    .line 847
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 848
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-ltz v0, :cond_1b

    .line 852
    :goto_1a
    return v1

    .line 847
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 852
    :cond_1f
    const/4 v1, -0x1

    goto :goto_1a
.end method

.method private a(ZIILcom/google/android/location/os/g;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x9

    .line 815
    if-ge p2, p3, :cond_7

    .line 816
    const/16 v0, 0x11

    .line 836
    :goto_6
    return v0

    .line 817
    :cond_7
    if-ne p2, p3, :cond_10

    .line 818
    if-eqz p1, :cond_e

    const/16 v0, 0x13

    goto :goto_6

    :cond_e
    const/4 v0, 0x6

    goto :goto_6

    .line 822
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_3b

    .line 824
    if-nez p4, :cond_1e

    move v0, v1

    .line 825
    goto :goto_6

    .line 827
    :cond_1e
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    .line 828
    const/16 v2, 0x19

    invoke-static {v0, p4, v2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 829
    const/16 v0, 0xa

    goto :goto_6

    :cond_39
    move v0, v1

    .line 831
    goto :goto_6

    .line 836
    :cond_3b
    const/16 v0, 0x8

    goto :goto_6
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 696
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_b

    .line 704
    :cond_a
    :goto_a
    return-void

    .line 699
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    .line 700
    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 701
    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a
.end method


# virtual methods
.method public a()Lcom/google/android/location/os/g;
    .registers 3

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 763
    const/4 v0, 0x0

    .line 765
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    goto :goto_9
.end method

.method public a(ZJJZLcom/google/android/location/os/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 782
    move-wide/from16 v0, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/e$a;->a(J)I

    move-result v11

    .line 784
    const/4 v2, -0x1

    if-ne v11, v2, :cond_b

    .line 785
    const/4 v2, 0x0

    .line 798
    :goto_a
    return-object v2

    .line 787
    :cond_b
    new-instance v10, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 789
    const/4 v2, 0x0

    move v9, v2

    :goto_14
    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_49

    .line 790
    iget-object v2, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/os/g;

    .line 791
    iget-object v2, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/e/e;

    .line 792
    iget-object v2, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/location/e/E;

    .line 793
    move-object/from16 v0, p7

    invoke-direct {p0, p1, v9, v11, v0}, Lcom/google/android/location/e$a;->a(ZIILcom/google/android/location/os/g;)I

    move-result v8

    move-wide v2, p2

    move/from16 v7, p6

    .line 794
    invoke-static/range {v2 .. v8}, Lcom/google/android/location/a;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 796
    const/4 v3, 0x4

    invoke-virtual {v10, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 789
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_14

    :cond_49
    move-object v2, v10

    .line 798
    goto :goto_a
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 13
    .parameter

    .prologue
    .line 656
    if-nez p1, :cond_3

    .line 687
    :cond_2
    :goto_2
    return-void

    .line 659
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_20

    .line 660
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 684
    :goto_11
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/e$a;->e:I

    if-le v0, v1, :cond_2

    .line 685
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/e$a;->a(I)V

    goto :goto_2

    .line 664
    :cond_20
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 665
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    .line 666
    iget-object v1, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    add-int/lit8 v3, v2, -0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/os/g;

    .line 667
    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    invoke-interface {v1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    .line 668
    invoke-interface {p1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v7

    sub-long/2addr v5, v7

    .line 669
    add-long v7, v3, v5

    .line 672
    iget v1, p0, Lcom/google/android/location/e$a;->f:I

    add-int/lit16 v1, v1, 0x1f4

    int-to-long v9, v1

    cmp-long v1, v7, v9

    if-lez v1, :cond_65

    iget v1, p0, Lcom/google/android/location/e$a;->g:I

    int-to-long v7, v1

    cmp-long v1, v3, v7

    if-ltz v1, :cond_7a

    iget v1, p0, Lcom/google/android/location/e$a;->g:I

    int-to-long v3, v1

    cmp-long v1, v5, v3

    if-ltz v1, :cond_7a

    .line 675
    :cond_65
    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7a

    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7a

    .line 677
    add-int/lit8 v0, v2, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/e$a;->a(I)V

    .line 680
    :cond_7a
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11
.end method

.method public a(JLcom/google/android/location/os/g;Lcom/google/android/location/a$b;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 716
    if-nez p3, :cond_4

    .line 717
    const/4 v0, 0x0

    .line 754
    :goto_3
    return v0

    .line 721
    :cond_4
    if-eqz p6, :cond_71

    invoke-static {p6, p3}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_71

    iget-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x2ee0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_71

    iget-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_71

    iget-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_71

    const-wide/32 v4, 0x927c0

    const-wide v6, 0x3fd3333333333333L

    move-object v0, p4

    move-object v1, p6

    move-wide v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/E;JJD)Z

    move-result v0

    if-eqz v0, :cond_71

    const/4 v0, 0x1

    .line 731
    :goto_38
    if-eqz p5, :cond_73

    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_73

    iget-wide v1, p0, Lcom/google/android/location/e$a;->a:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x2ee0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_73

    if-nez v0, :cond_54

    invoke-virtual {p4, p5, p3}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v1

    if-nez v1, :cond_73

    :cond_54
    const/4 v1, 0x1

    .line 735
    :goto_55
    if-eqz v1, :cond_75

    .line 736
    :goto_57
    if-eqz v0, :cond_77

    .line 738
    :goto_59
    if-eqz p5, :cond_60

    .line 739
    iget-object v1, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v1, p3, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    :cond_60
    if-eqz p6, :cond_67

    .line 743
    iget-object v1, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v1, p3, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 747
    :cond_67
    if-nez p6, :cond_6b

    if-eqz p5, :cond_6d

    .line 748
    :cond_6b
    iput-wide p1, p0, Lcom/google/android/location/e$a;->a:J

    .line 753
    :cond_6d
    invoke-virtual {p4, p3, p5, p6}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    goto :goto_3

    .line 721
    :cond_71
    const/4 v0, 0x0

    goto :goto_38

    .line 731
    :cond_73
    const/4 v1, 0x0

    goto :goto_55

    .line 735
    :cond_75
    const/4 p5, 0x0

    goto :goto_57

    .line 736
    :cond_77
    const/4 p6, 0x0

    goto :goto_59
.end method

.method public b()V
    .registers 3

    .prologue
    .line 859
    iget-object v0, p0, Lcom/google/android/location/e$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 860
    iget-object v0, p0, Lcom/google/android/location/e$a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 861
    iget-object v0, p0, Lcom/google/android/location/e$a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 862
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e$a;->a:J

    .line 863
    return-void
.end method
