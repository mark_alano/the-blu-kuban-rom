.class public Lcom/google/android/location/e;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e$1;,
        Lcom/google/android/location/e$a;
    }
.end annotation


# static fields
.field static g:Z


# instance fields
.field A:J

.field h:J

.field i:Lcom/google/android/location/os/g;

.field j:J

.field k:I

.field l:Z

.field m:Z

.field n:Z

.field o:Lcom/google/android/location/e/e;

.field p:Lcom/google/android/location/e/E;

.field q:Lcom/google/android/location/e$a;

.field r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field s:Z

.field t:J

.field u:Z

.field v:J

.field w:Z

.field x:Lcom/google/android/location/os/g;

.field y:Z

.field z:Lcom/google/android/location/d;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 99
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/e;->g:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 184
    const-string v1, "BurstCollector"

    sget-object v6, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e;->h:J

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    .line 120
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->j:J

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/e;->k:I

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->m:Z

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->n:Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    .line 138
    new-instance v0, Lcom/google/android/location/e$a;

    const/16 v1, 0x64

    const/16 v2, 0x2ee0

    const v3, 0xea60

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/e$a;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->s:Z

    .line 153
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->t:J

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    .line 162
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->v:J

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->w:Z

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->y:Z

    .line 179
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->A:J

    .line 185
    new-instance v0, Lcom/google/android/location/d;

    iget-object v1, p2, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-direct {v0, p1, p5, v1, p6}, Lcom/google/android/location/d;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;)V

    iput-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    .line 187
    return-void
.end method

.method private a(JLcom/google/android/location/os/g;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 269
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->j(J)Z

    move-result v0

    if-eqz v0, :cond_24

    if-eqz p3, :cond_24

    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_24

    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/e;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v2}, Lcom/google/android/location/a$b;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_24

    const/4 v0, 0x1

    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method private e()Z
    .registers 11

    .prologue
    const/4 v8, 0x1

    .line 342
    const/4 v9, 0x0

    .line 343
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    iget-boolean v1, p0, Lcom/google/android/location/e;->w:Z

    iget-object v2, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/e;->h:J

    iget-boolean v6, p0, Lcom/google/android/location/e;->n:Z

    iget-object v7, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/e$a;->a(ZJJZLcom/google/android/location/os/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 345
    iget-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_4f

    .line 346
    iget-object v0, p0, Lcom/google/android/location/e;->d:Lcom/google/android/location/x;

    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/x;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 350
    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    .line 351
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 352
    iput-boolean v8, p0, Lcom/google/android/location/e;->u:Z

    .line 353
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/e;->t:J

    .line 357
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    move v0, v8

    .line 359
    :goto_49
    iget-object v1, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v1}, Lcom/google/android/location/e$a;->b()V

    .line 360
    return v0

    :cond_4f
    move v0, v9

    goto :goto_49
.end method

.method private f()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 393
    iget-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    if-eqz v0, :cond_12

    .line 394
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    .line 396
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 398
    :cond_12
    return-void
.end method

.method private j(J)Z
    .registers 9
    .parameter

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/google/android/location/e;->m:Z

    if-eqz v0, :cond_19

    const-wide/32 v0, 0x75300

    .line 192
    :goto_7
    iget-wide v2, p0, Lcom/google/android/location/e;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_17

    iget-wide v2, p0, Lcom/google/android/location/e;->j:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_1d

    :cond_17
    const/4 v0, 0x1

    :goto_18
    return v0

    .line 190
    :cond_19
    const-wide/32 v0, 0xdbba0

    goto :goto_7

    .line 192
    :cond_1d
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private k(J)Z
    .registers 7
    .parameter

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/google/android/location/e;->A:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    iget-wide v0, p0, Lcom/google/android/location/e;->A:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_15

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private l(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 404
    sget-object v0, Lcom/google/android/location/e$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v3}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_3e

    move v1, v2

    .line 423
    :cond_10
    :goto_10
    :pswitch_10
    return v1

    .line 412
    :pswitch_11
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->m(J)Z

    move-result v3

    .line 413
    iget-wide v4, p0, Lcom/google/android/location/e;->h:J

    sub-long v4, p1, v4

    const-wide/32 v6, 0x222e0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2f

    move v0, v1

    .line 414
    :goto_21
    iget-object v4, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->m()Z

    move-result v4

    if-eqz v4, :cond_2d

    if-nez v3, :cond_2d

    if-eqz v0, :cond_31

    :cond_2d
    move v1, v2

    .line 417
    goto :goto_10

    :cond_2f
    move v0, v2

    .line 413
    goto :goto_21

    .line 421
    :cond_31
    :pswitch_31
    iget-boolean v0, p0, Lcom/google/android/location/e;->s:Z

    if-eqz v0, :cond_3b

    invoke-virtual {p0}, Lcom/google/android/location/e;->d()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_3b
    move v1, v2

    goto :goto_10

    .line 404
    nop

    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
        :pswitch_31
    .end packed-switch
.end method

.method private m(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 428
    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    .line 429
    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    sub-long v2, p1, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-ltz v2, :cond_16

    .line 436
    :cond_15
    :goto_15
    return v0

    :cond_16
    move v0, v1

    .line 429
    goto :goto_15

    .line 431
    :cond_18
    iget-object v2, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v2}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v2

    .line 432
    if-nez v2, :cond_22

    move v0, v1

    .line 434
    goto :goto_15

    .line 436
    :cond_22
    invoke-interface {v2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/16 v4, 0x55f0

    cmp-long v2, v2, v4

    if-gez v2, :cond_15

    move v0, v1

    goto :goto_15
.end method

.method private n(J)V
    .registers 11
    .parameter

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 442
    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_21

    .line 443
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_1f

    .line 447
    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    if-nez v0, :cond_3c

    .line 448
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    .line 449
    iput v4, p0, Lcom/google/android/location/e;->k:I

    .line 466
    :cond_1f
    :goto_1f
    iput-wide p1, p0, Lcom/google/android/location/e;->j:J

    .line 469
    :cond_21
    iput-object v5, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    .line 470
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e;->h:J

    .line 471
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    .line 472
    iput-object v5, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 473
    iput-boolean v3, p0, Lcom/google/android/location/e;->y:Z

    .line 474
    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    .line 475
    iput-wide v6, p0, Lcom/google/android/location/e;->v:J

    .line 476
    iput-boolean v3, p0, Lcom/google/android/location/e;->w:Z

    .line 477
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 478
    return-void

    .line 451
    :cond_3c
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iget-object v1, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lcom/google/android/location/e;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 455
    iget v0, p0, Lcom/google/android/location/e;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/e;->k:I

    goto :goto_1f

    .line 458
    :cond_4f
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    .line 459
    iput v4, p0, Lcom/google/android/location/e;->k:I

    goto :goto_1f
.end method


# virtual methods
.method a(I)V
    .registers 3
    .parameter

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(I)V

    .line 488
    return-void
.end method

.method a(IIZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 560
    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v3

    .line 561
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_28

    .line 562
    if-nez p3, :cond_17

    float-to-double v4, v3

    const-wide v6, 0x3fc999999999999aL

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_30

    :cond_17
    move v0, v2

    :goto_18
    iput-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    .line 563
    if-eqz p3, :cond_32

    float-to-double v3, v3

    const-wide v5, 0x3feccccccccccccdL

    cmpl-double v0, v3, v5

    if-ltz v0, :cond_32

    :goto_26
    iput-boolean v2, p0, Lcom/google/android/location/e;->m:Z

    .line 565
    :cond_28
    iput-boolean p3, p0, Lcom/google/android/location/e;->n:Z

    .line 566
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/d;->a(IIZ)V

    .line 567
    return-void

    :cond_30
    move v0, v1

    .line 562
    goto :goto_18

    :cond_32
    move v2, v1

    .line 563
    goto :goto_26
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 584
    invoke-super {p0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 586
    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .registers 2
    .parameter

    .prologue
    .line 550
    iput-object p1, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    .line 551
    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .registers 3
    .parameter

    .prologue
    .line 492
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 493
    iput-object p1, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    .line 497
    :goto_a
    return-void

    .line 495
    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    goto :goto_a
.end method

.method a(Lcom/google/android/location/os/g;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 506
    if-nez p1, :cond_4

    .line 529
    :cond_3
    :goto_3
    return-void

    .line 512
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/e;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->h()Z

    move-result v0

    if-eqz v0, :cond_43

    sget-boolean v0, Lcom/google/android/location/e;->g:Z

    if-eqz v0, :cond_43

    move v0, v1

    .line 514
    :goto_13
    if-eqz v0, :cond_45

    move v0, v1

    .line 515
    :goto_16
    iget-object v2, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v3, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v2, v3, :cond_1e

    if-nez v0, :cond_24

    :cond_1e
    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-ne v0, v2, :cond_3

    .line 516
    :cond_24
    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_37

    .line 518
    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    const/16 v2, 0x19

    invoke-static {v0, p1, v2}, Lcom/google/android/location/e;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_37

    iget v0, p0, Lcom/google/android/location/e;->k:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_3

    .line 523
    :cond_37
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e$a;->a(Lcom/google/android/location/os/g;)V

    .line 524
    iput-boolean v1, p0, Lcom/google/android/location/e;->y:Z

    .line 527
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->v:J

    goto :goto_3

    .line 512
    :cond_43
    const/4 v0, 0x0

    goto :goto_13

    .line 514
    :cond_45
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/e;->j(J)Z

    move-result v0

    goto :goto_16
.end method

.method a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Lcom/google/android/location/os/h;)V

    .line 580
    return-void
.end method

.method a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 533
    invoke-super {p0, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 534
    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_12

    .line 535
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/e;->A:J

    .line 541
    :goto_11
    return-void

    .line 539
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    goto :goto_11
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 501
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 502
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 556
    return-void
.end method

.method b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Z)V

    .line 546
    return-void
.end method

.method protected b(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const-wide/16 v6, -0x1

    .line 208
    .line 211
    iget-object v2, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v2}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v2

    .line 212
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->l(J)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->k(J)Z

    move-result v3

    if-eqz v3, :cond_4f

    .line 217
    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    .line 221
    iget-object v2, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v2}, Lcom/google/android/location/d;->a()Z

    move-result v2

    .line 222
    if-eqz v2, :cond_48

    .line 223
    iput-wide p1, p0, Lcom/google/android/location/e;->v:J

    .line 224
    iput-boolean v0, p0, Lcom/google/android/location/e;->w:Z

    :goto_24
    move v1, v0

    .line 238
    :cond_25
    :goto_25
    if-eqz v1, :cond_2d

    .line 239
    sget-object v0, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    .line 240
    iput-wide p1, p0, Lcom/google/android/location/e;->h:J

    .line 246
    :cond_2d
    iget-wide v2, p0, Lcom/google/android/location/e;->A:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_47

    iget-wide v2, p0, Lcom/google/android/location/e;->A:J

    sub-long v2, p1, v2

    const-wide/32 v4, 0xea60

    cmp-long v0, v2, v4

    if-lez v0, :cond_47

    if-nez v1, :cond_47

    .line 249
    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    .line 250
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    .line 252
    :cond_47
    return v1

    .line 229
    :cond_48
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    move v0, v1

    goto :goto_24

    .line 231
    :cond_4f
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/location/e;->a(JLcom/google/android/location/os/g;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 233
    iput-wide v6, p0, Lcom/google/android/location/e;->v:J

    .line 234
    iput-boolean v1, p0, Lcom/google/android/location/e;->w:Z

    move v1, v0

    goto :goto_25
.end method

.method public bridge synthetic c()V
    .registers 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 571
    iput-boolean p1, p0, Lcom/google/android/location/e;->s:Z

    .line 572
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->b(Z)V

    .line 573
    return-void
.end method

.method protected c(J)Z
    .registers 13
    .parameter

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x0

    .line 293
    .line 295
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v8

    .line 296
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->l(J)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 297
    const/4 v0, 0x1

    .line 300
    iget-object v1, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v1}, Lcom/google/android/location/d;->b()V

    .line 303
    invoke-direct {p0}, Lcom/google/android/location/e;->e()Z

    move-result v1

    .line 304
    if-nez v1, :cond_1d

    .line 305
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->n(J)V

    .line 320
    :cond_1d
    :goto_1d
    iget-object v1, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-ne v1, v2, :cond_37

    .line 321
    iget-wide v1, p0, Lcom/google/android/location/e;->v:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_58

    .line 323
    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v8}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x55f0

    add-long/2addr v2, v4

    invoke-interface {v1, v9, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 329
    :cond_37
    :goto_37
    iput-boolean v7, p0, Lcom/google/android/location/e;->y:Z

    .line 330
    return v0

    .line 309
    :cond_3a
    iget-boolean v0, p0, Lcom/google/android/location/e;->y:Z

    if-eqz v0, :cond_56

    .line 312
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    iget-object v1, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v1}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/e;->e:Lcom/google/android/location/a$b;

    iget-object v5, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    iget-object v6, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/e$a;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/a$b;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)Z

    move-result v0

    .line 314
    if-eqz v0, :cond_56

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    :cond_56
    move v0, v7

    goto :goto_1d

    .line 326
    :cond_58
    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    invoke-interface {v1, v9, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_37
.end method

.method d()Z
    .registers 2

    .prologue
    .line 481
    iget-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    return v0
.end method

.method protected f(J)Z
    .registers 10
    .parameter

    .prologue
    const-wide/16 v5, 0x3a98

    .line 371
    const/4 v0, 0x0

    .line 372
    iget-object v1, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_13

    .line 373
    const/4 v0, 0x1

    .line 375
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->n(J)V

    .line 376
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/e;->t:J

    .line 377
    invoke-direct {p0}, Lcom/google/android/location/e;->f()V

    .line 386
    :goto_12
    return v0

    .line 380
    :cond_13
    iget-wide v1, p0, Lcom/google/android/location/e;->t:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v5

    if-ltz v1, :cond_1f

    .line 381
    invoke-direct {p0}, Lcom/google/android/location/e;->f()V

    goto :goto_12

    .line 383
    :cond_1f
    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/google/android/location/e;->t:J

    add-long/2addr v3, v5

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_12
.end method
