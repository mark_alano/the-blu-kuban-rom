.class public Lcom/google/android/location/e/A;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final b:J

.field private volatile c:J


# direct methods
.method public constructor <init>(Ljava/lang/Object;J)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/location/e/A;->a:Ljava/lang/Object;

    .line 33
    iput-wide p2, p0, Lcom/google/android/location/e/A;->b:J

    .line 34
    iput-wide p2, p0, Lcom/google/android/location/e/A;->c:J

    .line 35
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/location/e/A;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)Ljava/lang/Object;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/e/A;->b(J)V

    .line 45
    iget-object v0, p0, Lcom/google/android/location/e/A;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public b()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/google/android/location/e/A;->b:J

    return-wide v0
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/google/android/location/e/A;->c:J

    .line 60
    return-void
.end method

.method public c()J
    .registers 3

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/google/android/location/e/A;->c:J

    return-wide v0
.end method
