.class public Lcom/google/android/location/e/C;
.super Lcom/google/android/location/e/w;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/C$a;
    }
.end annotation


# static fields
.field public static final k:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final i:I

.field public final j:Lcom/google/android/location/e/C$a;

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 173
    new-instance v0, Lcom/google/android/location/e/C$1;

    invoke-direct {v0}, Lcom/google/android/location/e/C$1;-><init>()V

    sput-object v0, Lcom/google/android/location/e/C;->k:Lcom/google/android/location/e/v;

    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/e/w;-><init>(IIII)V

    .line 49
    iput p5, p0, Lcom/google/android/location/e/C;->i:I

    .line 50
    invoke-virtual {p0, p4}, Lcom/google/android/location/e/C;->a(I)Lcom/google/android/location/e/C$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/e/C;)I
    .registers 2
    .parameter

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/location/e/C;->l:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/e/C;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 15
    iput p1, p0, Lcom/google/android/location/e/C;->l:I

    return p1
.end method

.method public static a(Ljava/io/PrintWriter;Lcom/google/android/location/e/C;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 160
    if-nez p1, :cond_8

    .line 161
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 171
    :goto_7
    return-void

    .line 164
    :cond_8
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 165
    invoke-static {p0, p1}, Lcom/google/android/location/e/w;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/w;)V

    .line 166
    const-string v0, ", Uncert="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 167
    iget v0, p1, Lcom/google/android/location/e/C;->i:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 168
    const-string v0, "mm, "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 169
    iget-object v0, p1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 170
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/C;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 146
    if-nez p1, :cond_8

    .line 147
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :goto_7
    return-void

    .line 150
    :cond_8
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-static {p0, p1}, Lcom/google/android/location/e/w;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/w;)V

    .line 152
    const-string v0, ", Uncert="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    iget v0, p1, Lcom/google/android/location/e/C;->i:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 154
    const-string v0, "mm, "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    iget-object v0, p1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method

.method public static final b()Lcom/google/android/location/e/C;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 71
    new-instance v0, Lcom/google/android/location/e/C;

    const/4 v3, -0x2

    const/16 v4, 0x55

    const v5, 0x9c40

    move v2, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/e/C;-><init>(IIIII)V

    .line 73
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/location/e/C;->l:I

    .line 74
    return-object v0
.end method


# virtual methods
.method protected a(I)Lcom/google/android/location/e/C$a;
    .registers 3
    .parameter

    .prologue
    .line 120
    const/16 v0, 0x50

    if-lt p1, v0, :cond_b

    const/16 v0, 0x54

    if-gt p1, v0, :cond_b

    .line 121
    sget-object v0, Lcom/google/android/location/e/C$a;->b:Lcom/google/android/location/e/C$a;

    .line 127
    :goto_a
    return-object v0

    .line 122
    :cond_b
    const/16 v0, 0x55

    if-lt p1, v0, :cond_16

    const/16 v0, 0x59

    if-gt p1, v0, :cond_16

    .line 123
    sget-object v0, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    goto :goto_a

    .line 124
    :cond_16
    const/16 v0, 0x5a

    if-lt p1, v0, :cond_21

    const/16 v0, 0x5e

    if-gt p1, v0, :cond_21

    .line 125
    sget-object v0, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    goto :goto_a

    .line 127
    :cond_21
    sget-object v0, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    goto :goto_a
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/location/e/C;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/e/C;->l:I

    .line 82
    return-void
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/location/e/C;->l:I

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/location/e/C;->l:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WifiApPosition ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/location/e/w;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", horizontalUncertaintyMm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/C;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", positionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
