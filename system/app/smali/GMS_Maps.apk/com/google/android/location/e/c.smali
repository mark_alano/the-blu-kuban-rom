.class public Lcom/google/android/location/e/c;
.super Lcom/google/android/location/e/o;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/e/f;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/f;Ljava/util/Map;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w;",
            "Lcom/google/android/location/e/o$a;",
            "J",
            "Lcom/google/android/location/e/f;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    .line 35
    iput-object p5, p0, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    .line 36
    iput-object p6, p0, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    .line 37
    return-void
.end method

.method public static a(Ljava/io/PrintWriter;Lcom/google/android/location/e/c;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 84
    if-nez p1, :cond_a

    .line 85
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 117
    :goto_9
    return-void

    .line 88
    :cond_a
    const-string v0, "CellLocatorResult [primary="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 89
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/e/e;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/e;)V

    .line 90
    const-string v0, ", History=["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 91
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_48

    .line 93
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_30
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e;

    .line 94
    if-nez v1, :cond_43

    .line 95
    const-string v1, ", "

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 98
    :cond_43
    invoke-static {p0, v0}, Lcom/google/android/location/e/e;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/e;)V

    move v1, v3

    goto :goto_30

    .line 101
    :cond_48
    const-string v0, "], Cache={"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 102
    iget-object v0, p1, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    if-eqz v0, :cond_87

    .line 104
    iget-object v0, p1, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 105
    if-nez v2, :cond_6e

    .line 106
    const-string v1, ", "

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 109
    :cond_6e
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 110
    const-string v1, "="

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 111
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-static {p0, v0}, Lcom/google/android/location/e/w;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/w;)V

    move v2, v3

    goto :goto_5b

    .line 114
    :cond_87
    const-string v0, "}, "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 115
    invoke-static {p0, p1}, Lcom/google/android/location/e/o;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/o;)V

    .line 116
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/16 :goto_9
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/c;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 47
    if-nez p1, :cond_a

    .line 48
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    :goto_9
    return-void

    .line 51
    :cond_a
    const-string v0, "CellLocatorResult [primary="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/e/e;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/e;)V

    .line 53
    const-string v0, ", History=["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 56
    iget-object v0, p1, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_30
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e;

    .line 57
    if-eqz v1, :cond_43

    move v1, v3

    .line 62
    :goto_3f
    invoke-static {p0, v0}, Lcom/google/android/location/e/e;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/e;)V

    goto :goto_30

    .line 60
    :cond_43
    const-string v5, ", "

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3f

    .line 65
    :cond_49
    const-string v0, "], Cache={"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    iget-object v0, p1, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    if-eqz v0, :cond_88

    .line 68
    iget-object v0, p1, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_88

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 69
    if-nez v2, :cond_6f

    .line 70
    const-string v1, ", "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_6f
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v1, "="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-static {p0, v0}, Lcom/google/android/location/e/w;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/w;)V

    move v2, v3

    goto :goto_5c

    .line 78
    :cond_88
    const-string v0, "}, "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-static {p0, p1}, Lcom/google/android/location/e/o;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/o;)V

    .line 80
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CellLocatorResult [primaryCell="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v1}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cellHistory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v1}, Lcom/google/android/location/e/f;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cellCacheEntries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/c;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/location/e/o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
