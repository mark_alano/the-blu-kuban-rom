.class public abstract Lcom/google/android/location/e/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/e$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final b:I

.field protected final c:I

.field protected final d:I

.field protected final e:I

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field protected final i:Ljava/lang/String;

.field protected final j:J

.field protected final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e$a;",
            ">;"
        }
    .end annotation
.end field

.field protected final l:I

.field protected final m:I

.field protected n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/e/e;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(JIIIILjava/lang/String;Ljava/util/List;IIIII)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIIII",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e$a;",
            ">;IIIII)V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-wide p1, p0, Lcom/google/android/location/e/e;->j:J

    .line 71
    iput p3, p0, Lcom/google/android/location/e/e;->l:I

    .line 72
    iput p4, p0, Lcom/google/android/location/e/e;->b:I

    .line 73
    iput p5, p0, Lcom/google/android/location/e/e;->c:I

    .line 74
    iput p6, p0, Lcom/google/android/location/e/e;->d:I

    .line 75
    iput-object p7, p0, Lcom/google/android/location/e/e;->i:Ljava/lang/String;

    .line 76
    iput-object p8, p0, Lcom/google/android/location/e/e;->k:Ljava/util/List;

    .line 77
    iput p13, p0, Lcom/google/android/location/e/e;->m:I

    .line 78
    iput p9, p0, Lcom/google/android/location/e/e;->e:I

    .line 79
    iput p10, p0, Lcom/google/android/location/e/e;->f:I

    .line 80
    iput p11, p0, Lcom/google/android/location/e/e;->g:I

    .line 81
    iput p12, p0, Lcom/google/android/location/e/e;->h:I

    .line 82
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 10
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 203
    if-nez p0, :cond_5

    .line 245
    :cond_4
    :goto_4
    return-object v0

    .line 206
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 207
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 208
    array-length v1, v2

    const/4 v4, 0x4

    if-lt v1, v4, :cond_4

    .line 212
    const/4 v1, 0x0

    :try_start_14
    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 213
    const/4 v1, 0x1

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 214
    const/4 v1, 0x2

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 216
    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 217
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v8, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 219
    if-eq v4, v3, :cond_69

    .line 220
    const/16 v8, 0xa

    invoke-virtual {v1, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 222
    const/4 v8, 0x6

    if-ne v4, v8, :cond_5b

    move v2, v3

    .line 233
    :goto_43
    if-eq v2, v3, :cond_49

    .line 234
    const/4 v4, 0x1

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 236
    :cond_49
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 237
    if-eq v6, v3, :cond_53

    .line 238
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 240
    :cond_53
    if-eq v5, v3, :cond_59

    .line 241
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_59
    move-object v0, v1

    .line 243
    goto :goto_4

    .line 225
    :cond_5b
    array-length v4, v2

    const/4 v8, 0x5

    if-ne v4, v8, :cond_4

    .line 229
    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_65
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_65} :catch_67

    move-result v2

    goto :goto_43

    .line 244
    :catch_67
    move-exception v1

    goto :goto_4

    :cond_69
    move v2, v3

    goto :goto_43
.end method

.method public static a(Ljava/io/PrintWriter;Lcom/google/android/location/e/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 287
    if-nez p1, :cond_8

    .line 288
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 308
    :goto_7
    return-void

    .line 291
    :cond_8
    const-string v0, "[cid: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/e/e;->b:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 292
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 293
    const-string v0, " mcc: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/e/e;->c:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 294
    const-string v0, " mnc: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/e/e;->d:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 295
    const-string v0, " radioType: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/e/e;->l:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 296
    const-string v0, " signalStrength: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/android/location/e/e;->m:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 297
    const-string v0, " neighbors["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 298
    const/4 v0, 0x1

    .line 299
    iget-object v1, p1, Lcom/google/android/location/e/e;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_4e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e$a;

    .line 300
    if-eqz v1, :cond_61

    .line 301
    const/4 v1, 0x0

    .line 305
    :goto_5d
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    goto :goto_4e

    .line 303
    :cond_61
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_5d

    .line 307
    :cond_67
    const-string v0, "]]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 263
    if-nez p1, :cond_8

    .line 264
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :goto_7
    return-void

    .line 267
    :cond_8
    const-string v0, "[cid: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/e/e;->b:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 268
    const-string v0, " mcc: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/e/e;->c:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 269
    const-string v0, " mnc: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/e/e;->d:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v0, " radioType: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/e/e;->l:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 272
    const-string v0, " signalStrength: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lcom/google/android/location/e/e;->m:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 273
    const-string v0, " neighbors["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const/4 v0, 0x1

    .line 275
    iget-object v1, p1, Lcom/google/android/location/e/e;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_4e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e$a;

    .line 276
    if-eqz v1, :cond_61

    .line 277
    const/4 v1, 0x0

    .line 281
    :goto_5d
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_4e

    .line 279
    :cond_61
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5d

    .line 283
    :cond_67
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method

.method static a(II)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    const v0, 0x7fffffff

    .line 448
    .line 452
    if-eq p0, v0, :cond_1d

    if-eq p1, v0, :cond_1d

    const v0, 0x13c680

    if-gt p0, v0, :cond_1d

    const v0, -0x13c680

    if-lt p0, v0, :cond_1d

    const v0, 0x278d00

    if-gt p1, v0, :cond_1d

    const v0, -0x278d00

    if-lt p1, v0, :cond_1d

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, -0x1

    .line 161
    .line 165
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 166
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 170
    :goto_f
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_a4

    .line 171
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 173
    :goto_19
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_a2

    .line 174
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 176
    :goto_23
    const/16 v4, 0xa

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_a0

    .line 177
    const/16 v4, 0xa

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    .line 182
    :goto_31
    const/4 v5, 0x6

    if-ne v4, v5, :cond_60

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_5f
    return-object v0

    .line 188
    :cond_60
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_6a

    .line 189
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 191
    :cond_6a
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5f

    :cond_a0
    move v4, v1

    goto :goto_31

    :cond_a2
    move v3, v1

    goto :goto_23

    :cond_a4
    move v2, v1

    goto/16 :goto_19

    :cond_a7
    move v0, v1

    goto/16 :goto_f
.end method


# virtual methods
.method public abstract a(JI)Lcom/google/android/location/e/e;
.end method

.method public a(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 11
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v3, -0x1

    const-wide v5, 0x4085b38e38e38e39L

    .line 313
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 315
    iget v1, p0, Lcom/google/android/location/e/e;->b:I

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 317
    iget v1, p0, Lcom/google/android/location/e/e;->d:I

    if-eq v1, v3, :cond_1d

    .line 318
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/location/e/e;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 321
    :cond_1d
    iget v1, p0, Lcom/google/android/location/e/e;->c:I

    if-eq v1, v3, :cond_27

    .line 322
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/location/e/e;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 325
    :cond_27
    iget v1, p0, Lcom/google/android/location/e/e;->m:I

    const/16 v2, -0x270f

    if-eq v1, v2, :cond_33

    .line 326
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/location/e/e;->m:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 329
    :cond_33
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-eqz v1, :cond_3e

    .line 330
    const/4 v1, 0x6

    long-to-int v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 333
    :cond_3e
    iget v1, p0, Lcom/google/android/location/e/e;->g:I

    iget v2, p0, Lcom/google/android/location/e/e;->h:I

    invoke-static {v1, v2}, Lcom/google/android/location/e/e;->a(II)Z

    move-result v1

    if-eqz v1, :cond_65

    .line 334
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 335
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/location/e/e;->g:I

    int-to-double v3, v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 336
    iget v2, p0, Lcom/google/android/location/e/e;->h:I

    int-to-double v2, v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    invoke-virtual {v1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 337
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 341
    :cond_65
    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->j()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 344
    invoke-virtual {p0, v0}, Lcom/google/android/location/e/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 345
    return-object v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method abstract a(Lcom/google/android/location/e/e;)Z
.end method

.method abstract b()Z
.end method

.method public b(Lcom/google/android/location/e/e;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 133
    if-nez p1, :cond_4

    .line 137
    :cond_3
    :goto_3
    return v0

    :cond_4
    iget v1, p0, Lcom/google/android/location/e/e;->b:I

    iget v2, p1, Lcom/google/android/location/e/e;->b:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/location/e/e;->c:I

    iget v2, p1, Lcom/google/android/location/e/e;->c:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/location/e/e;->d:I

    iget v2, p1, Lcom/google/android/location/e/e;->d:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/location/e/e;->l:I

    iget v2, p1, Lcom/google/android/location/e/e;->l:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/location/e/e;->a(Lcom/google/android/location/e/e;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    goto :goto_3
.end method

.method abstract c()Ljava/lang/String;
.end method

.method public d()I
    .registers 3

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/location/e/e;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    iget v0, p0, Lcom/google/android/location/e/e;->f:I

    :goto_7
    return v0

    :cond_8
    iget v0, p0, Lcom/google/android/location/e/e;->e:I

    goto :goto_7
.end method

.method public e()I
    .registers 3

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/location/e/e;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    iget v0, p0, Lcom/google/android/location/e/e;->d:I

    :goto_7
    return v0

    :cond_8
    iget v0, p0, Lcom/google/android/location/e/e;->c:I

    goto :goto_7
.end method

.method public f()J
    .registers 3

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/google/android/location/e/e;->j:J

    return-wide v0
.end method

.method public g()I
    .registers 2

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/location/e/e;->l:I

    return v0
.end method

.method public h()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/location/e/e;->k:Ljava/util/List;

    return-object v0
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 252
    iget v0, p0, Lcom/google/android/location/e/e;->b:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1c

    iget v0, p0, Lcom/google/android/location/e/e;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1c

    iget v0, p0, Lcom/google/android/location/e/e;->c:I

    if-ltz v0, :cond_1c

    iget v0, p0, Lcom/google/android/location/e/e;->d:I

    if-ltz v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->b()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public j()I
    .registers 6

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x3

    .line 353
    const/4 v2, -0x1

    .line 354
    iget v3, p0, Lcom/google/android/location/e/e;->l:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 363
    :goto_8
    return v0

    .line 356
    :cond_9
    iget v3, p0, Lcom/google/android/location/e/e;->l:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_10

    move v0, v1

    .line 357
    goto :goto_8

    .line 358
    :cond_10
    iget v3, p0, Lcom/google/android/location/e/e;->l:I

    if-ne v3, v0, :cond_16

    .line 359
    const/4 v0, 0x5

    goto :goto_8

    .line 360
    :cond_16
    iget v0, p0, Lcom/google/android/location/e/e;->l:I

    if-ne v0, v1, :cond_1c

    .line 361
    const/4 v0, 0x6

    goto :goto_8

    :cond_1c
    move v0, v2

    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    invoke-static {v0, p0}, Lcom/google/android/location/e/e;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/e;)V

    .line 259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
