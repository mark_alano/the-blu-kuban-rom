.class public Lcom/google/android/location/e/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:Lcom/google/android/location/e/e;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    .line 27
    return-void
.end method

.method public static a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 94
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-nez v0, :cond_a

    .line 95
    :cond_8
    const/4 v0, 0x0

    .line 111
    :goto_9
    return-object v0

    .line 98
    :cond_a
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 99
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 102
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/e/e;->a(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 105
    invoke-virtual {p0}, Lcom/google/android/location/e/e;->h()Ljava/util/List;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_43

    .line 107
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e$a;

    .line 108
    const/4 v3, 0x3

    invoke-virtual {v0, p0}, Lcom/google/android/location/e/e$a;->a(Lcom/google/android/location/e/e;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_2e

    :cond_43
    move-object v0, v1

    .line 111
    goto :goto_9
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/f;
    .registers 2

    .prologue
    .line 46
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/e/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/f;
    :try_end_6
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_6} :catch_7

    .line 48
    :goto_6
    return-object v0

    .line 47
    :catch_7
    move-exception v0

    .line 48
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 4
    .parameter

    .prologue
    .line 61
    if-nez p1, :cond_3

    .line 75
    :goto_2
    return-void

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 69
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_26

    .line 70
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 72
    :cond_26
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_2d
    iput-object p1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    goto :goto_2
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-static {v0, p2, p3}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_39

    .line 80
    const/4 v0, 0x5

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 83
    if-eqz p4, :cond_35

    .line 84
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e;

    .line 85
    iget-object v3, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v3}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/location/e/e;->a(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 86
    const/4 v3, 0x4

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_15

    .line 89
    :cond_35
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 91
    :cond_39
    return-void
.end method

.method public b()Lcom/google/android/location/e/e;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    return-object v0
.end method

.method protected clone()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 35
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/f;

    .line 38
    iget-object v1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    iput-object v1, v0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    .line 39
    new-instance v1, Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    .line 40
    return-object v0
.end method
