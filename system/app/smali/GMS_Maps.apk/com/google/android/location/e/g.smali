.class public Lcom/google/android/location/e/g;
.super Lcom/google/android/location/e/o;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/e/E;


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    .line 39
    iput-object p5, p0, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    .line 40
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/e/E;J)Lcom/google/android/location/e/g;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 44
    if-eqz p0, :cond_59

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_59

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 47
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-nez v1, :cond_59

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_59

    .line 50
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 51
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 52
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 54
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    .line 55
    const/4 v0, 0x0

    .line 56
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 57
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    move v2, v0

    .line 59
    :goto_45
    const v0, 0x4c4b40

    if-gt v2, v0, :cond_59

    .line 60
    new-instance v0, Lcom/google/android/location/e/g;

    new-instance v1, Lcom/google/android/location/e/w;

    invoke-direct {v1, v3, v4, v2}, Lcom/google/android/location/e/w;-><init>(III)V

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    move-wide v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/e/g;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;)V

    .line 66
    :goto_58
    return-object v0

    :cond_59
    const/4 v0, 0x0

    goto :goto_58

    :cond_5b
    move v2, v0

    goto :goto_45
.end method

.method public static a(Ljava/io/PrintWriter;Lcom/google/android/location/e/g;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 87
    if-nez p1, :cond_8

    .line 88
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 96
    :goto_7
    return-void

    .line 91
    :cond_8
    const-string v0, "GlsLocatorResult [wifiScan="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 92
    iget-object v0, p1, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    invoke-static {p0, v0}, Lcom/google/android/location/e/E;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/E;)V

    .line 93
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 94
    invoke-static {p0, p1}, Lcom/google/android/location/e/o;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/o;)V

    .line 95
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/g;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    if-nez p1, :cond_8

    .line 76
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :goto_7
    return-void

    .line 79
    :cond_8
    const-string v0, "GlsLocatorResult [wifiScan="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v0, p1, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    invoke-static {p0, v0}, Lcom/google/android/location/e/E;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/E;)V

    .line 81
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-static {p0, p1}, Lcom/google/android/location/e/o;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/o;)V

    .line 83
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GlsLocatorResult [wifiScan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/location/e/o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
