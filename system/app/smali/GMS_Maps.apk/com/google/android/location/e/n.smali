.class public Lcom/google/android/location/e/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/n$a;
    }
.end annotation


# instance fields
.field private final a:[I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    .line 36
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8

    .prologue
    .line 55
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 57
    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v3, :cond_23

    aget-object v4, v2, v0

    .line 58
    iget-object v5, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-virtual {v4}, Lcom/google/android/location/e/n$a;->ordinal()I

    move-result v6

    aget v5, v5, v6

    .line 60
    if-lez v5, :cond_20

    .line 61
    iget v4, v4, Lcom/google/android/location/e/n$a;->e:I

    invoke-virtual {v1, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 57
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 65
    :cond_23
    invoke-virtual {p0}, Lcom/google/android/location/e/n;->b()V

    .line 66
    return-object v1
.end method

.method public a(Lcom/google/android/location/e/n$a;)V
    .registers 5
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/e/n$a;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 40
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-static {}, Lcom/google/android/location/e/n$a;->values()[Lcom/google/android/location/e/n$a;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_7
    if-ge v0, v3, :cond_e

    aget-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 78
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/e/n;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 79
    return-void
.end method
