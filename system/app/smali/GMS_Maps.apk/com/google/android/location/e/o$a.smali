.class public final enum Lcom/google/android/location/e/o$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/e/o$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/e/o$a;

.field public static final enum b:Lcom/google/android/location/e/o$a;

.field public static final enum c:Lcom/google/android/location/e/o$a;

.field private static final synthetic d:[Lcom/google/android/location/e/o$a;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/location/e/o$a;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/e/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    .line 19
    new-instance v0, Lcom/google/android/location/e/o$a;

    const-string v1, "NO_LOCATION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/e/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    .line 20
    new-instance v0, Lcom/google/android/location/e/o$a;

    const-string v1, "CACHE_MISS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/e/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/location/e/o$a;->d:[Lcom/google/android/location/e/o$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/e/o$a;
    .registers 2
    .parameter

    .prologue
    .line 17
    const-class v0, Lcom/google/android/location/e/o$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/o$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/e/o$a;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/location/e/o$a;->d:[Lcom/google/android/location/e/o$a;

    invoke-virtual {v0}, [Lcom/google/android/location/e/o$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/e/o$a;

    return-object v0
.end method
