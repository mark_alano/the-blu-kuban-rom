.class public Lcom/google/android/location/e/s;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/s$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/e/s$a;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/location/e/s$a;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    .line 38
    iput-object p2, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/location/e/s;
    .registers 3
    .parameter

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/location/e/s;

    sget-object v1, Lcom/google/android/location/e/s$a;->a:Lcom/google/android/location/e/s$a;

    invoke-direct {v0, v1, p0}, Lcom/google/android/location/e/s;-><init>(Lcom/google/android/location/e/s$a;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/location/e/s;
    .registers 3
    .parameter

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/location/e/s;

    sget-object v1, Lcom/google/android/location/e/s$a;->b:Lcom/google/android/location/e/s$a;

    invoke-direct {v0, v1, p0}, Lcom/google/android/location/e/s;-><init>(Lcom/google/android/location/e/s$a;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/s$a;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    if-ne p0, p1, :cond_5

    .line 53
    :cond_4
    :goto_4
    return v0

    .line 47
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/e/s;

    if-nez v2, :cond_b

    move v0, v1

    .line 48
    goto :goto_4

    .line 51
    :cond_b
    check-cast p1, Lcom/google/android/location/e/s;

    .line 53
    iget-object v2, p0, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    iget-object v3, p1, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    if-ne v2, v3, :cond_1b

    iget-object v2, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    if-nez v2, :cond_1d

    iget-object v2, p1, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    if-eqz v2, :cond_4

    :cond_1b
    move v0, v1

    goto :goto_4

    :cond_1d
    iget-object v2, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 60
    .line 61
    iget-object v0, p0, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    invoke-virtual {v0}, Lcom/google/android/location/e/s$a;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 62
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    :goto_f
    add-int/2addr v0, v1

    .line 63
    return v0

    .line 62
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_f
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/s;->a:Lcom/google/android/location/e/s$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "modelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
