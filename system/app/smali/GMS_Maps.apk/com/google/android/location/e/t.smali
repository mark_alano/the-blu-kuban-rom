.class public Lcom/google/android/location/e/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/e/o;

.field public final b:Lcom/google/android/location/e/D;

.field public final c:Lcom/google/android/location/e/c;

.field public final d:Lcom/google/android/location/e/g;


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    .line 36
    iput-object p2, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    .line 37
    iput-object p3, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    .line 38
    iput-object p4, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    .line 39
    if-eqz p1, :cond_1b

    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v0, v1, :cond_1b

    .line 40
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid Args"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1b
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/E;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-nez v0, :cond_6

    .line 49
    const/4 v0, 0x0

    .line 51
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v0, v0, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    goto :goto_5
.end method

.method public a(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter

    .prologue
    .line 88
    const-string v0, "NetworkLocation [\n bestResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-nez v0, :cond_32

    .line 90
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 98
    :cond_e
    :goto_e
    const-string v0, "\n wifiResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    invoke-static {p1, v0}, Lcom/google/android/location/e/D;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/D;)V

    .line 100
    const-string v0, "\n cellResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    invoke-static {p1, v0}, Lcom/google/android/location/e/c;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/c;)V

    .line 102
    const-string v0, "\n glsResult="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    invoke-static {p1, v0}, Lcom/google/android/location/e/g;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/g;)V

    .line 104
    const-string v0, "\n]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 105
    return-void

    .line 91
    :cond_32
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v0, v1, :cond_3e

    .line 92
    const-string v0, "WIFI"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_e

    .line 93
    :cond_3e
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v0, v1, :cond_4a

    .line 94
    const-string v0, "CELL"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_e

    .line 95
    :cond_4a
    iget-object v0, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v1, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v0, v1, :cond_e

    .line 96
    const-string v0, "GLS"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_e
.end method

.method public b()Lcom/google/android/location/e/e;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-nez v0, :cond_6

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v0, v0, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    invoke-virtual {v0}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v0

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1388

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 67
    const-string v1, "NetworkLocation [\n bestResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-nez v1, :cond_3d

    .line 69
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_15
    :goto_15
    const-string v1, "\n wifiResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v1, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    invoke-static {v0, v1}, Lcom/google/android/location/e/D;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/D;)V

    .line 79
    const-string v1, "\n cellResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v1, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    invoke-static {v0, v1}, Lcom/google/android/location/e/c;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/c;)V

    .line 81
    const-string v1, "\n glsResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v1, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    invoke-static {v0, v1}, Lcom/google/android/location/e/g;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/g;)V

    .line 83
    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 70
    :cond_3d
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v1, v2, :cond_49

    .line 71
    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15

    .line 72
    :cond_49
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v1, v2, :cond_55

    .line 73
    const-string v1, "CELL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15

    .line 74
    :cond_55
    iget-object v1, p0, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v2, p0, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v1, v2, :cond_15

    .line 75
    const-string v1, "GLS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15
.end method
