.class public Lcom/google/android/location/e/u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TS;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    .line 25
    iput-object p2, p0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    .line 26
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(TA;TB;)",
            "Lcom/google/android/location/e/u",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/location/e/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    if-ne p1, p0, :cond_5

    .line 47
    :cond_4
    :goto_4
    return v0

    .line 40
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/e/u;

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_4

    .line 43
    :cond_b
    :try_start_b
    check-cast p1, Lcom/google/android/location/e/u;
    :try_end_d
    .catch Ljava/lang/ClassCastException; {:try_start_b .. :try_end_d} :catch_23

    .line 47
    iget-object v2, p0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_21
    move v0, v1

    goto :goto_4

    .line 44
    :catch_23
    move-exception v0

    move v0, v1

    .line 45
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 57
    .line 58
    iget-object v0, p0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 59
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    return v0
.end method
