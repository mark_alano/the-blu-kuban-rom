.class public Lcom/google/android/location/e/v$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/e/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/e/v$a;->d:I

    .line 33
    iput-object v1, p0, Lcom/google/android/location/e/v$a;->e:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/google/android/location/e/v$a;->f:Ljava/lang/String;

    .line 35
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/android/location/e/v$a;->g:I

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/e/v;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/e/v$a;->d:I

    .line 33
    iput-object v1, p0, Lcom/google/android/location/e/v$a;->e:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/google/android/location/e/v$a;->f:Ljava/lang/String;

    .line 35
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/android/location/e/v$a;->g:I

    .line 43
    iget v0, p1, Lcom/google/android/location/e/v;->a:I

    iput v0, p0, Lcom/google/android/location/e/v$a;->a:I

    .line 44
    iget v0, p1, Lcom/google/android/location/e/v;->b:I

    iput v0, p0, Lcom/google/android/location/e/v$a;->b:I

    .line 45
    iget v0, p1, Lcom/google/android/location/e/v;->c:I

    iput v0, p0, Lcom/google/android/location/e/v$a;->c:I

    .line 46
    iget v0, p1, Lcom/google/android/location/e/v;->d:I

    iput v0, p0, Lcom/google/android/location/e/v$a;->d:I

    .line 47
    iget-object v0, p1, Lcom/google/android/location/e/v;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/e/v$a;->e:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lcom/google/android/location/e/v;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/e/v$a;->f:Ljava/lang/String;

    .line 49
    iget v0, p1, Lcom/google/android/location/e/v;->g:I

    iput v0, p0, Lcom/google/android/location/e/v$a;->g:I

    .line 50
    return-void
.end method


# virtual methods
.method public a(II)Lcom/google/android/location/e/v$a;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 57
    iput p1, p0, Lcom/google/android/location/e/v$a;->a:I

    .line 58
    iput p2, p0, Lcom/google/android/location/e/v$a;->b:I

    .line 59
    return-object p0
.end method

.method public a(Lcom/google/android/location/e/h;)Lcom/google/android/location/e/v$a;
    .registers 4
    .parameter

    .prologue
    .line 63
    iget v0, p1, Lcom/google/android/location/e/h;->a:I

    iget v1, p1, Lcom/google/android/location/e/h;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/e/v$a;->a(II)Lcom/google/android/location/e/v$a;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/location/e/v;
    .registers 9

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/location/e/v;

    iget v1, p0, Lcom/google/android/location/e/v$a;->a:I

    iget v2, p0, Lcom/google/android/location/e/v$a;->b:I

    iget v3, p0, Lcom/google/android/location/e/v$a;->c:I

    iget v4, p0, Lcom/google/android/location/e/v$a;->d:I

    iget-object v5, p0, Lcom/google/android/location/e/v$a;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/location/e/v$a;->f:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/location/e/v$a;->g:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/e/v;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
