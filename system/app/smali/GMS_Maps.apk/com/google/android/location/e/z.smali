.class public Lcom/google/android/location/e/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/f/a;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/google/android/location/f/a;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/f/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/location/e/z;->a:Ljava/util/Map;

    .line 35
    iput-object p2, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    .line 36
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    invoke-virtual {v0}, Lcom/google/android/location/f/a;->a()I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/Map;)[F
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)[F"
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/location/e/z;->b(Ljava/util/Map;)[F

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    invoke-virtual {v1, v0}, Lcom/google/android/location/f/a;->a([F)[F

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/util/Map;)[F
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)[F"
        }
    .end annotation

    .prologue
    const/high16 v7, -0x3d38

    .line 72
    iget-object v0, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    iget v0, v0, Lcom/google/android/location/f/a;->a:I

    new-array v2, v0, [F

    .line 76
    iget-object v0, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    iget v0, v0, Lcom/google/android/location/f/a;->a:I

    new-array v3, v0, [I

    .line 77
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_16
    :goto_16
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 78
    iget-object v1, p0, Lcom/google/android/location/e/z;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 79
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 80
    if-eqz v1, :cond_16

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v5, v7

    if-eqz v5, :cond_16

    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aget v6, v2, v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v6

    aput v0, v2, v5

    .line 82
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget v1, v3, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, v3, v0

    goto :goto_16

    .line 85
    :cond_58
    const/4 v0, 0x0

    :goto_59
    array-length v1, v2

    if-ge v0, v1, :cond_6e

    .line 86
    aget v1, v3, v0

    if-nez v1, :cond_65

    .line 87
    aput v7, v2, v0

    .line 85
    :goto_62
    add-int/lit8 v0, v0, 0x1

    goto :goto_59

    .line 89
    :cond_65
    aget v1, v2, v0

    aget v4, v3, v0

    int-to-float v4, v4

    div-float/2addr v1, v4

    aput v1, v2, v0

    goto :goto_62

    .line 92
    :cond_6e
    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 97
    if-ne p0, p1, :cond_5

    .line 107
    :cond_4
    :goto_4
    return v0

    .line 101
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/e/z;

    if-nez v2, :cond_b

    move v0, v1

    .line 102
    goto :goto_4

    .line 105
    :cond_b
    check-cast p1, Lcom/google/android/location/e/z;

    .line 107
    iget-object v2, p0, Lcom/google/android/location/e/z;->a:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/location/e/z;->a:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    iget-object v3, p1, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    invoke-virtual {v2, v3}, Lcom/google/android/location/f/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_21
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 113
    .line 114
    iget-object v0, p0, Lcom/google/android/location/e/z;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 115
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/z;->b:Lcom/google/android/location/f/a;

    invoke-virtual {v1}, Lcom/google/android/location/f/a;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    return v0
.end method
