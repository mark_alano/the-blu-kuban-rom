.class public Lcom/google/android/location/f/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/b$a;
    }
.end annotation


# instance fields
.field a:I

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/f/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/f/b;->a:I

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    .line 77
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/f/b;->a:I

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    .line 87
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b;
    .registers 6
    .parameter

    .prologue
    .line 176
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 177
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 178
    new-instance v2, Lcom/google/android/location/f/b;

    invoke-direct {v2, v1}, Lcom/google/android/location/f/b;-><init>(I)V

    .line 179
    invoke-virtual {v2, v0}, Lcom/google/android/location/f/b;->a(I)V

    .line 180
    const/4 v0, 0x0

    :goto_11
    if-ge v0, v1, :cond_1f

    .line 181
    invoke-static {p0}, Lcom/google/android/location/f/b$a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b$a;

    move-result-object v3

    .line 182
    iget-object v4, v2, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 184
    :cond_1f
    return-object v2
.end method


# virtual methods
.method public a([F)F
    .registers 5
    .parameter

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not compute value of regression tree. No root of the tree was defined"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_d
    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    .line 129
    :goto_f
    iget-object v1, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/b$a;

    .line 130
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->a(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/b$a$a;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v1, v2, :cond_28

    .line 131
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->b(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/f/c;->a([F)F

    move-result v0

    return v0

    .line 133
    :cond_28
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->c(Lcom/google/android/location/f/b$a;)I

    move-result v1

    aget v1, p1, v1

    .line 134
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->d(Lcom/google/android/location/f/b$a;)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_3b

    .line 135
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->e(Lcom/google/android/location/f/b$a;)I

    move-result v0

    goto :goto_f

    .line 137
    :cond_3b
    invoke-static {v0}, Lcom/google/android/location/f/b$a;->f(Lcom/google/android/location/f/b$a;)I

    move-result v0

    goto :goto_f
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 95
    iput p1, p0, Lcom/google/android/location/f/b;->a:I

    .line 96
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 206
    if-ne p0, p1, :cond_5

    .line 216
    :cond_4
    :goto_4
    return v0

    .line 210
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/f/b;

    if-nez v2, :cond_b

    move v0, v1

    .line 211
    goto :goto_4

    .line 214
    :cond_b
    check-cast p1, Lcom/google/android/location/f/b;

    .line 216
    iget v2, p0, Lcom/google/android/location/f/b;->a:I

    iget v3, p1, Lcom/google/android/location/f/b;->a:I

    if-ne v2, v3, :cond_1d

    iget-object v2, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1d
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 222
    .line 223
    iget v0, p0, Lcom/google/android/location/f/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 224
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Root: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/f/b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 160
    :goto_25
    iget-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_52

    .line 161
    const-string v4, "(%d) %s\n"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/android/location/f/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/b$a;

    invoke-virtual {v0}, Lcom/google/android/location/f/b$a;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 163
    :cond_52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
