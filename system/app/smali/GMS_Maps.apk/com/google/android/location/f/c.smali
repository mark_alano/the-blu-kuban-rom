.class public Lcom/google/android/location/f/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/c$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/f/c$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    .line 61
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/c;
    .registers 5
    .parameter

    .prologue
    .line 108
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 109
    new-instance v2, Lcom/google/android/location/f/c;

    invoke-direct {v2, v1}, Lcom/google/android/location/f/c;-><init>(I)V

    .line 110
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v1, :cond_16

    .line 111
    invoke-static {p0}, Lcom/google/android/location/f/c$a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/c$a;

    move-result-object v3

    .line 112
    invoke-direct {v2, v3}, Lcom/google/android/location/f/c;->a(Lcom/google/android/location/f/c$a;)V

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 114
    :cond_16
    return-object v2
.end method

.method private a(Lcom/google/android/location/f/c$a;)V
    .registers 3
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    return-void
.end method


# virtual methods
.method public a([F)F
    .registers 8
    .parameter

    .prologue
    .line 66
    const-wide/16 v0, 0x0

    .line 67
    iget-object v2, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/c$a;

    .line 68
    iget v4, v0, Lcom/google/android/location/f/c$a;->a:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_21

    .line 69
    iget v0, v0, Lcom/google/android/location/f/c$a;->b:F

    float-to-double v4, v0

    add-double v0, v1, v4

    :goto_1f
    move-wide v1, v0

    .line 71
    goto :goto_9

    :cond_21
    iget v4, v0, Lcom/google/android/location/f/c$a;->b:F

    iget v0, v0, Lcom/google/android/location/f/c$a;->a:I

    aget v0, p1, v0

    mul-float/2addr v0, v4

    float-to-double v4, v0

    add-double v0, v1, v4

    goto :goto_1f

    .line 74
    :cond_2c
    double-to-float v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 119
    if-ne p0, p1, :cond_4

    .line 120
    const/4 v0, 0x1

    .line 129
    :goto_3
    return v0

    .line 123
    :cond_4
    instance-of v0, p1, Lcom/google/android/location/f/c;

    if-nez v0, :cond_a

    .line 124
    const/4 v0, 0x0

    goto :goto_3

    .line 127
    :cond_a
    check-cast p1, Lcom/google/android/location/f/c;

    .line 129
    iget-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 134
    .line 135
    iget-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 136
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v0, p0, Lcom/google/android/location/f/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/c$a;

    .line 95
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/location/f/c$a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    .line 97
    :cond_32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
