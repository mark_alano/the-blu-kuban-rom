.class public Lcom/google/android/location/f/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/d$a;
    }
.end annotation


# direct methods
.method public static a([F)Lcom/google/android/location/f/d$a;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 30
    .line 32
    array-length v4, p0

    move v1, v0

    move v3, v2

    .line 33
    :goto_5
    if-ge v1, v4, :cond_d

    .line 34
    aget v5, p0, v1

    add-float/2addr v3, v5

    .line 33
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 37
    :cond_d
    int-to-float v1, v4

    div-float/2addr v3, v1

    move v1, v2

    .line 40
    :goto_10
    if-ge v0, v4, :cond_1a

    .line 41
    aget v5, p0, v0

    sub-float/2addr v5, v3

    .line 42
    mul-float/2addr v5, v5

    add-float/2addr v1, v5

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 47
    :cond_1a
    const/4 v0, 0x1

    if-ne v4, v0, :cond_23

    .line 48
    new-instance v0, Lcom/google/android/location/f/d$a;

    invoke-direct {v0, v3, v2}, Lcom/google/android/location/f/d$a;-><init>(FF)V

    .line 51
    :goto_22
    return-object v0

    .line 50
    :cond_23
    add-int/lit8 v0, v4, -0x1

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v1, v0

    .line 51
    new-instance v0, Lcom/google/android/location/f/d$a;

    invoke-direct {v0, v3, v1}, Lcom/google/android/location/f/d$a;-><init>(FF)V

    goto :goto_22
.end method
