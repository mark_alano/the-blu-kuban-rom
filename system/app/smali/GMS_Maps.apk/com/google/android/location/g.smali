.class public Lcom/google/android/location/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/K",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/location/os/j;

.field b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/f;Ljava/io/File;Ljavax/crypto/SecretKey;[B)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->u:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 80
    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x1

    const/4 v3, 0x2

    sget-object v5, Lcom/google/android/location/j/a;->u:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-object v2, p3

    move-object v4, p4

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    iput-object v0, p0, Lcom/google/android/location/g;->a:Lcom/google/android/location/os/j;

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/f;Ljavax/crypto/SecretKey;[B)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-static {p1}, Lcom/google/android/location/g;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/location/g;-><init>(Lcom/google/android/location/os/f;Ljava/io/File;Ljavax/crypto/SecretKey;[B)V

    .line 75
    return-void
.end method

.method private static a(Lcom/google/android/location/os/f;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 94
    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/location/os/f;->h()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_clts"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Date;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 573
    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 577
    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private q()V
    .registers 6

    .prologue
    .line 311
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/location/g;->d()Ljava/util/List;

    move-result-object v0

    .line 313
    new-instance v2, Lcom/google/android/location/g$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/g$1;-><init>(Lcom/google/android/location/g;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 327
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 328
    const/16 v3, 0x14

    if-le v2, v3, :cond_1d

    .line 329
    add-int/lit8 v3, v2, -0x14

    invoke-interface {v0, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 331
    :cond_1d
    iget-object v2, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 332
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 333
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_27

    .line 335
    :catchall_3a
    move-exception v0

    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_3 .. :try_end_3c} :catchall_3a

    throw v0

    :cond_3d
    :try_start_3d
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3a

    .line 336
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->a:Lcom/google/android/location/os/j;

    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 107
    invoke-direct {p0}, Lcom/google/android/location/g;->q()V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_10
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_e} :catch_13

    .line 112
    :goto_e
    :try_start_e
    monitor-exit v1

    .line 113
    return-void

    .line 112
    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_e .. :try_end_12} :catchall_10

    throw v0

    .line 108
    :catch_13
    move-exception v0

    goto :goto_e
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 230
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 231
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 232
    monitor-exit v1

    .line 233
    return-void

    .line 232
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public a(J)V
    .registers 6
    .parameter

    .prologue
    .line 223
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 224
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 226
    monitor-exit v1

    .line 227
    return-void

    .line 226
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public a(JJILcom/google/android/location/b$a;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 286
    iget-object v2, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 287
    :try_start_5
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    .line 288
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    .line 289
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v8, Lcom/google/android/location/j/a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 290
    const/4 v8, 0x1

    invoke-virtual {v7, v8, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 291
    const/4 v3, 0x2

    invoke-virtual {v7, v3, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 292
    const/4 v3, 0x4

    invoke-virtual {v7, v3, p5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 293
    if-eqz p6, :cond_2a

    .line 294
    const/4 v3, 0x5

    sget-object v4, Lcom/google/android/location/b$a;->b:Lcom/google/android/location/b$a;

    if-ne p6, v4, :cond_38

    :goto_27
    invoke-virtual {v7, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 299
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 300
    invoke-direct {p0}, Lcom/google/android/location/g;->q()V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/location/g;->j()V

    .line 302
    monitor-exit v2

    .line 303
    return-void

    :cond_38
    move v0, v1

    .line 294
    goto :goto_27

    .line 302
    :catchall_3a
    move-exception v0

    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_5 .. :try_end_3c} :catchall_3a

    throw v0
.end method

.method public a(JLjava/util/Map;)V
    .registers 13
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 444
    iget-object v2, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 445
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 447
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 448
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 449
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ad:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 451
    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 452
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_40
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_69

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 453
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->al:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 454
    const/4 v6, 0x1

    iget-wide v7, v0, Lcom/google/android/location/k/b;->a:J

    long-to-int v7, v7

    invoke-virtual {v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 455
    const/4 v6, 0x2

    iget-wide v7, v0, Lcom/google/android/location/k/b;->b:J

    long-to-int v0, v7

    invoke-virtual {v5, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 456
    const/4 v0, 0x2

    invoke-virtual {v4, v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_40

    .line 462
    :catchall_66
    move-exception v0

    monitor-exit v2
    :try_end_68
    .catchall {:try_start_3 .. :try_end_68} :catchall_66

    throw v0

    .line 459
    :cond_69
    :try_start_69
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_19

    .line 462
    :cond_71
    monitor-exit v2
    :try_end_72
    .catchall {:try_start_69 .. :try_end_72} :catchall_66

    .line 463
    return-void
.end method

.method public a(Ljava/util/Calendar;)V
    .registers 7
    .parameter

    .prologue
    .line 396
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 397
    if-eqz p1, :cond_16

    .line 398
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 400
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 402
    :cond_16
    monitor-exit v1

    .line 403
    return-void

    .line 402
    :catchall_18
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 54
    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 120
    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_15

    .line 123
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->a:Lcom/google/android/location/os/j;

    iget-object v2, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[B

    move-result-object v0

    .line 124
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_12

    .line 125
    :try_start_c
    iget-object v1, p0, Lcom/google/android/location/g;->a:Lcom/google/android/location/os/j;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/j;->a([B)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_11} :catch_15

    .line 129
    :goto_11
    return-void

    .line 124
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    :try_start_14
    throw v0
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_15} :catch_15

    .line 126
    :catch_15
    move-exception v0

    goto :goto_11
.end method

.method public b(I)V
    .registers 5
    .parameter

    .prologue
    .line 236
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 238
    monitor-exit v1

    .line 239
    return-void

    .line 238
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public b(J)V
    .registers 6
    .parameter

    .prologue
    .line 360
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 361
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x8

    invoke-virtual {v0, v2, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 363
    monitor-exit v1

    .line 364
    return-void

    .line 363
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 145
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 147
    if-lez v0, :cond_17

    .line 148
    iget-object v2, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    monitor-exit v1

    .line 150
    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_16

    .line 151
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method public c(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 13
    .parameter

    .prologue
    .line 514
    iget-object v2, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 515
    :try_start_3
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->as:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 516
    const/4 v1, 0x0

    :goto_b
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v1, v3, :cond_5c

    .line 517
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 519
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_59

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_59

    .line 526
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    .line 528
    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 529
    cmp-long v7, v4, p1

    if-ltz v7, :cond_59

    .line 530
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v8, Lcom/google/android/location/j/a;->Z:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 533
    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-static {v3, v8, v9}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    .line 535
    const/4 v8, 0x2

    invoke-virtual {v7, v8, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 538
    const/4 v3, 0x1

    invoke-virtual {v7, v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 539
    invoke-static {v4, v5}, Lcom/google/android/location/k/c;->a(J)J

    move-result-wide v3

    .line 540
    const/4 v5, 0x3

    long-to-int v3, v3

    invoke-virtual {v7, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 542
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 516
    :cond_59
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 547
    :cond_5c
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-nez v1, :cond_64

    const/4 v0, 0x0

    :cond_64
    monitor-exit v2

    return-object v0

    .line 549
    :catchall_66
    move-exception v0

    monitor-exit v2
    :try_end_68
    .catchall {:try_start_3 .. :try_end_68} :catchall_66

    throw v0
.end method

.method public d()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    const/4 v0, 0x0

    :goto_9
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_1f

    .line 163
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 165
    :cond_1f
    monitor-exit v1

    return-object v2

    .line 166
    :catchall_21
    move-exception v0

    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    throw v0
.end method

.method public e()J
    .registers 4

    .prologue
    .line 212
    iget-object v2, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 213
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    :goto_13
    monitor-exit v2

    return-wide v0

    :cond_15
    const-wide/16 v0, 0x0

    goto :goto_13

    .line 216
    :catchall_18
    move-exception v0

    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public f()I
    .registers 4

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 244
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public g()I
    .registers 4

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 249
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 250
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public h()Z
    .registers 4

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 256
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_f

    const/4 v0, 0x1

    :goto_d
    monitor-exit v1

    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_d

    .line 257
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public i()Z
    .registers 4

    .prologue
    .line 261
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 262
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_f

    const/4 v0, 0x1

    :goto_d
    monitor-exit v1

    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_d

    .line 263
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method j()V
    .registers 4

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 269
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xb

    invoke-static {v0, v2}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 271
    monitor-exit v1

    .line 272
    return-void

    .line 271
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public k()J
    .registers 4

    .prologue
    .line 347
    iget-object v2, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 348
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    :goto_15
    monitor-exit v2

    return-wide v0

    :cond_17
    const-wide/16 v0, 0x0

    goto :goto_15

    .line 351
    :catchall_1a
    move-exception v0

    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method public l()V
    .registers 4

    .prologue
    .line 370
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x8

    invoke-static {v0, v2}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 373
    monitor-exit v1

    .line 374
    return-void

    .line 373
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public m()Ljava/util/Date;
    .registers 7

    .prologue
    .line 381
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 382
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    .line 384
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_13

    .line 385
    const/4 v0, 0x0

    monitor-exit v1

    .line 387
    :goto_12
    return-object v0

    :cond_13
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    monitor-exit v1

    goto :goto_12

    .line 388
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method public n()V
    .registers 5

    .prologue
    .line 409
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 410
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/location/g;->o()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 412
    monitor-exit v1

    .line 413
    return-void

    .line 412
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public o()I
    .registers 4

    .prologue
    .line 420
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 421
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x7

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 423
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public p()Ljava/util/Map;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 473
    iget-object v4, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 474
    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_11

    .line 475
    const/4 v0, 0x0

    monitor-exit v4

    .line 503
    :goto_10
    return-object v0

    .line 477
    :cond_11
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    move v3, v2

    .line 478
    :goto_16
    iget-object v1, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-ge v3, v1, :cond_75

    .line 480
    iget-object v1, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v5, 0xa

    invoke-virtual {v1, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 482
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_71

    .line 483
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 485
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 486
    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .line 488
    :goto_3c
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    if-ge v1, v7, :cond_71

    .line 490
    const/4 v7, 0x2

    invoke-virtual {v5, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 492
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_6e

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_6e

    .line 494
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    int-to-long v8, v8

    .line 495
    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    int-to-long v10, v7

    .line 496
    cmp-long v7, v8, v10

    if-gez v7, :cond_6e

    .line 497
    new-instance v7, Lcom/google/android/location/k/b;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    :cond_6e
    add-int/lit8 v1, v1, 0x1

    goto :goto_3c

    .line 479
    :cond_71
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_16

    .line 503
    :cond_75
    monitor-exit v4

    goto :goto_10

    .line 504
    :catchall_77
    move-exception v0

    monitor-exit v4
    :try_end_79
    .catchall {:try_start_4 .. :try_end_79} :catchall_77

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 554
    iget-object v1, p0, Lcom/google/android/location/g;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 555
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 556
    const-string v3, "NextSensorCollectionTimeSinceEpoch: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v7, 0x2

    invoke-direct {p0, v6, v7}, Lcom/google/android/location/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Date;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :goto_1f
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_60

    .line 559
    iget-object v3, p0, Lcom/google/android/location/g;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 561
    const-string v4, ", ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v4, "start: %s, end: %s, success: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {p0, v3, v7}, Lcom/google/android/location/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Date;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x2

    invoke-direct {p0, v3, v7}, Lcom/google/android/location/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/Date;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const/4 v7, 0x3

    invoke-direct {p0, v3, v7}, Lcom/google/android/location/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 568
    :cond_60
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 569
    :catchall_66
    move-exception v0

    monitor-exit v1
    :try_end_68
    .catchall {:try_start_4 .. :try_end_68} :catchall_66

    throw v0
.end method
