.class public Lcom/google/android/location/g/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(D)D
    .registers 6
    .parameter

    .prologue
    .line 40
    const-wide v0, 0x400921fb54442d18L

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(DDDD)D
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-static {p0, p1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    .line 62
    invoke-static {p4, p5}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v4

    .line 63
    invoke-static {p2, p3}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v2

    .line 64
    invoke-static {p6, p7}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v6

    .line 65
    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(I)D
    .registers 5
    .parameter

    .prologue
    .line 48
    int-to-double v0, p0

    const-wide v2, 0x3e7ad7f29abcaf48L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/h;)D
    .registers 3
    .parameter

    .prologue
    .line 111
    iget v0, p0, Lcom/google/android/location/e/h;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 199
    invoke-static {p0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/h;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 200
    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v4

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000

    div-double/2addr v2, v4

    .line 201
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/w;)D
    .registers 3
    .parameter

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;)D
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 189
    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v0

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)I
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/location/e/w$a;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w$a;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 88
    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(IILcom/google/android/location/e/w;)Lcom/google/android/location/e/h;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide v10, 0x3f847ae147ae147bL

    .line 257
    iget v0, p2, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    .line 258
    iget v2, p2, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    .line 259
    add-double v4, v0, v10

    move-wide v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v4

    div-double v8, v10, v4

    .line 260
    add-double v6, v2, v10

    move-wide v4, v0

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v4

    div-double v4, v10, v4

    .line 261
    new-instance v6, Lcom/google/android/location/e/h;

    int-to-double v10, p0

    mul-double v7, v10, v8

    add-double/2addr v0, v7

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(D)I

    move-result v0

    int-to-double v7, p1

    mul-double/2addr v4, v7

    add-double v1, v4, v2

    invoke-static {v1, v2}, Lcom/google/android/location/g/c;->b(D)I

    move-result v1

    invoke-direct {v6, v0, v1}, Lcom/google/android/location/e/h;-><init>(II)V

    return-object v6
.end method

.method public static b(DDDD)D
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    sub-double v0, p4, p0

    const-wide/high16 v2, 0x3fe0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    .line 75
    sub-double v2, p6, p2

    const-wide/high16 v4, 0x3fe0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 76
    mul-double/2addr v0, v0

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    invoke-static {p4, p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v2

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 77
    const-wide/high16 v2, 0x3ff0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_27

    .line 78
    const-wide/high16 v0, 0x3ff0

    .line 80
    :cond_27
    const-wide/high16 v2, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0

    sub-double v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    mul-double/2addr v0, v2

    .line 81
    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/h;)D
    .registers 3
    .parameter

    .prologue
    .line 125
    iget v0, p0, Lcom/google/android/location/e/h;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 232
    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 233
    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)D
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 184
    iget v0, p0, Lcom/google/android/location/e/w$a;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w$a;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w;)D
    .registers 3
    .parameter

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 179
    iget v0, p0, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/location/e/w;->b:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v4}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v4

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static b(D)I
    .registers 4
    .parameter

    .prologue
    .line 52
    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method

.method public static b(I)I
    .registers 2
    .parameter

    .prologue
    .line 269
    div-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static c(D)D
    .registers 6
    .parameter

    .prologue
    .line 217
    const-wide v0, -0x4006de04abbbd2e8L

    const-wide v2, 0x3ff921fb54442d18L

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static c(DDDD)D
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    invoke-static {p2, p3}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v0

    .line 143
    invoke-static/range {p6 .. p7}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v2

    .line 144
    invoke-static {p0, p1}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v4

    .line 145
    invoke-static/range {p4 .. p5}, Lcom/google/android/location/g/c;->a(D)D

    move-result-wide v6

    .line 147
    sub-double v8, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_2e

    sub-double v8, v0, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L

    cmpl-double v8, v8, v10

    if-lez v8, :cond_33

    .line 149
    :cond_2e
    invoke-static/range {p0 .. p7}, Lcom/google/android/location/g/c;->a(DDDD)D

    move-result-wide v0

    .line 165
    :goto_32
    return-wide v0

    .line 152
    :cond_33
    sub-double v8, v4, v6

    .line 155
    sub-double/2addr v0, v2

    .line 159
    add-double v2, v4, v6

    const-wide/high16 v4, 0x4000

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 160
    mul-double/2addr v2, v2

    .line 161
    mul-double v4, v8, v8

    mul-double/2addr v2, v0

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    .line 162
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 165
    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    goto :goto_32
.end method

.method public static c(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 211
    invoke-static {p0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 212
    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v4

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000

    div-double/2addr v2, v4

    .line 213
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static c(I)I
    .registers 2
    .parameter

    .prologue
    .line 276
    const v0, 0x20c49b

    if-le p0, v0, :cond_9

    .line 278
    const v0, 0x7fffffff

    .line 280
    :goto_8
    return v0

    :cond_9
    mul-int/lit16 v0, p0, 0x3e8

    goto :goto_8
.end method

.method public static c(Lcom/google/android/location/e/w;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 310
    iget v1, p0, Lcom/google/android/location/e/w;->c:I

    const v2, 0x989680

    if-le v1, v2, :cond_9

    .line 318
    :cond_8
    :goto_8
    return v0

    .line 312
    :cond_9
    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    if-nez v1, :cond_11

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    if-eqz v1, :cond_8

    .line 314
    :cond_11
    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    const v2, 0x35a4e900

    if-gt v1, v2, :cond_8

    iget v1, p0, Lcom/google/android/location/e/w;->a:I

    const v2, -0x35a4e900

    if-lt v1, v2, :cond_8

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    const v2, 0x6b49d200

    if-gt v1, v2, :cond_8

    iget v1, p0, Lcom/google/android/location/e/w;->b:I

    const v2, -0x6b49d200

    if-lt v1, v2, :cond_8

    .line 318
    const/4 v0, 0x1

    goto :goto_8
.end method

.method public static d(D)D
    .registers 4
    .parameter

    .prologue
    .line 221
    const-wide v0, 0x401921fb54442d18L

    invoke-static {p0, p1, v0, v1}, Ljava/lang/Math;->IEEEremainder(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static d(I)D
    .registers 5
    .parameter

    .prologue
    .line 290
    int-to-double v0, p0

    const-wide v2, 0x416312d000000000L

    div-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static d(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 243
    invoke-static {p0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 244
    const-wide v2, 0x415849c600000000L

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static e(D)I
    .registers 6
    .parameter

    .prologue
    .line 299
    const-wide v0, 0x4066800000000000L

    mul-double/2addr v0, p0

    const-wide v2, 0x400921fb54442d18L

    div-double/2addr v0, v2

    const-wide v2, 0x416312d000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method
