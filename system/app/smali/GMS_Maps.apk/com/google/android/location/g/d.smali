.class public Lcom/google/android/location/g/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:D

.field private b:D

.field private c:I

.field private d:I

.field private e:D

.field private f:D

.field private g:[D

.field private h:[D

.field private i:[I


# direct methods
.method constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xa

    const-wide/16 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    .line 21
    iput-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    .line 22
    iput v3, p0, Lcom/google/android/location/g/d;->c:I

    .line 23
    iput v3, p0, Lcom/google/android/location/g/d;->d:I

    .line 25
    iput-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    .line 26
    iput-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    .line 29
    new-array v0, v2, [D

    iput-object v0, p0, Lcom/google/android/location/g/d;->g:[D

    .line 30
    new-array v0, v2, [D

    iput-object v0, p0, Lcom/google/android/location/g/d;->h:[D

    .line 31
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    .line 34
    invoke-virtual {p0}, Lcom/google/android/location/g/d;->a()V

    .line 35
    return-void
.end method


# virtual methods
.method public a()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v3, 0x0

    .line 38
    iput-wide v3, p0, Lcom/google/android/location/g/d;->a:D

    .line 39
    iput-wide v3, p0, Lcom/google/android/location/g/d;->b:D

    .line 40
    iput v1, p0, Lcom/google/android/location/g/d;->c:I

    .line 41
    iput v1, p0, Lcom/google/android/location/g/d;->d:I

    .line 43
    iput-wide v3, p0, Lcom/google/android/location/g/d;->e:D

    .line 44
    iput-wide v3, p0, Lcom/google/android/location/g/d;->f:D

    move v0, v1

    .line 46
    :goto_10
    const/16 v2, 0xa

    if-ge v0, v2, :cond_23

    .line 47
    iget-object v2, p0, Lcom/google/android/location/g/d;->g:[D

    aput-wide v3, v2, v0

    .line 48
    iget-object v2, p0, Lcom/google/android/location/g/d;->h:[D

    aput-wide v3, v2, v0

    .line 49
    iget-object v2, p0, Lcom/google/android/location/g/d;->i:[I

    aput v1, v2, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 51
    :cond_23
    return-void
.end method

.method public a(DDII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_32

    const/16 v0, 0x1388

    if-gt p5, v0, :cond_32

    .line 55
    iget-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    .line 56
    iget-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    add-double/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    .line 57
    iget v0, p0, Lcom/google/android/location/g/d;->d:I

    if-le p6, v0, :cond_1a

    .line 58
    iput p6, p0, Lcom/google/android/location/g/d;->d:I

    .line 61
    :cond_1a
    iget-object v0, p0, Lcom/google/android/location/g/d;->g:[D

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput-wide p1, v0, v1

    .line 62
    iget-object v0, p0, Lcom/google/android/location/g/d;->h:[D

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput-wide p3, v0, v1

    .line 63
    iget-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    iget v1, p0, Lcom/google/android/location/g/d;->c:I

    aput p5, v0, v1

    .line 64
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/g/d;->c:I

    .line 66
    :cond_32
    return-void
.end method

.method public a(Lcom/google/android/location/e/w;)V
    .registers 9
    .parameter

    .prologue
    .line 69
    iget v0, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v1

    iget v0, p1, Lcom/google/android/location/e/w;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v3

    iget v0, p1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v5

    iget v6, p1, Lcom/google/android/location/e/w;->d:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/g/d;->a(DDII)V

    .line 72
    return-void
.end method

.method public b()D
    .registers 5

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_14

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-eqz v0, :cond_14

    .line 80
    iget-wide v0, p0, Lcom/google/android/location/g/d;->a:D

    iget v2, p0, Lcom/google/android/location/g/d;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    .line 82
    :cond_14
    iget-wide v0, p0, Lcom/google/android/location/g/d;->e:D

    return-wide v0
.end method

.method public c()D
    .registers 5

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_14

    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-eqz v0, :cond_14

    .line 87
    iget-wide v0, p0, Lcom/google/android/location/g/d;->b:D

    iget v2, p0, Lcom/google/android/location/g/d;->c:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    .line 89
    :cond_14
    iget-wide v0, p0, Lcom/google/android/location/g/d;->f:D

    return-wide v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/location/g/d;->d:I

    return v0
.end method

.method public e()I
    .registers 15

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    if-nez v0, :cond_6

    .line 98
    const/4 v0, 0x0

    .line 129
    :goto_5
    return v0

    .line 101
    :cond_6
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    .line 102
    iget-object v0, p0, Lcom/google/android/location/g/d;->i:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_5

    .line 105
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/location/g/d;->b()D

    move-result-wide v0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/location/g/d;->c()D

    move-result-wide v2

    .line 108
    const/4 v8, 0x0

    .line 109
    const/16 v7, 0x1388

    .line 110
    const/16 v6, 0x1388

    .line 111
    const/4 v4, 0x1

    new-array v13, v4, [F

    .line 112
    const/4 v5, 0x0

    .line 114
    const/4 v4, 0x0

    move v9, v5

    move v10, v6

    move v11, v7

    move v12, v8

    move v8, v4

    :goto_28
    iget v4, p0, Lcom/google/android/location/g/d;->c:I

    if-ge v8, v4, :cond_5e

    .line 115
    iget-object v4, p0, Lcom/google/android/location/g/d;->g:[D

    aget-wide v4, v4, v8

    iget-object v6, p0, Lcom/google/android/location/g/d;->h:[D

    aget-wide v6, v6, v8

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->a(DDDD)D

    .line 116
    const/4 v4, 0x0

    aget v4, v13, v4

    float-to-int v4, v4

    add-int/2addr v12, v4

    .line 117
    const/4 v4, 0x0

    aget v4, v13, v4

    iget-object v5, p0, Lcom/google/android/location/g/d;->i:[I

    aget v5, v5, v8

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6d

    .line 118
    const/4 v5, 0x1

    .line 120
    :goto_49
    iget-object v4, p0, Lcom/google/android/location/g/d;->i:[I

    aget v4, v4, v8

    if-ge v4, v11, :cond_6a

    .line 121
    iget-object v4, p0, Lcom/google/android/location/g/d;->i:[I

    aget v7, v4, v8

    .line 122
    const/4 v4, 0x0

    aget v4, v13, v4

    float-to-int v6, v4

    .line 114
    :goto_57
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v9, v5

    move v10, v6

    move v11, v7

    goto :goto_28

    .line 126
    :cond_5e
    if-eqz v9, :cond_65

    .line 127
    iget v0, p0, Lcom/google/android/location/g/d;->c:I

    div-int v0, v12, v0

    goto :goto_5

    .line 129
    :cond_65
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_5

    :cond_6a
    move v6, v10

    move v7, v11

    goto :goto_57

    :cond_6d
    move v5, v9

    goto :goto_49
.end method
