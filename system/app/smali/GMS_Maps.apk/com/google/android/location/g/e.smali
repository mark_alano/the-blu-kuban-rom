.class public Lcom/google/android/location/g/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/g/p;

.field private final b:Lcom/google/android/location/g/a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;Lcom/google/android/location/g/l;Lcom/google/android/location/os/i;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/g/l;",
            "Lcom/google/android/location/os/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p3, p2, p4}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/g/l;Lcom/google/android/location/b/i;Lcom/google/android/location/os/i;)Lcom/google/android/location/g/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    .line 37
    new-instance v0, Lcom/google/android/location/g/a;

    invoke-direct {v0, p1, p4}, Lcom/google/android/location/g/a;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V

    iput-object v0, p0, Lcom/google/android/location/g/e;->b:Lcom/google/android/location/g/a;

    .line 38
    return-void
.end method

.method private a(Lcom/google/android/location/e/E;Lcom/google/android/location/e/E;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v0

    .line 91
    iget-object v1, v0, Lcom/google/android/location/e/D;->d:Lcom/google/android/location/e/o$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v1, v2, :cond_1e

    if-eq p1, p2, :cond_1e

    if-eqz p2, :cond_1e

    .line 96
    iget-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v0

    .line 98
    :cond_1e
    return-object v0
.end method

.method private a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)Lcom/google/android/location/e/o;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v3, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v0, v3, :cond_16

    move v0, v1

    .line 104
    :goto_9
    iget-object v3, p2, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_18

    move v3, v1

    .line 107
    :goto_10
    if-nez v0, :cond_1a

    if-nez v3, :cond_1a

    .line 108
    const/4 p2, 0x0

    .line 136
    :cond_15
    :goto_15
    return-object p2

    :cond_16
    move v0, v2

    .line 103
    goto :goto_9

    :cond_18
    move v3, v2

    .line 104
    goto :goto_10

    .line 109
    :cond_1a
    if-eqz v0, :cond_15

    .line 111
    if-nez v3, :cond_20

    move-object p2, p1

    .line 112
    goto :goto_15

    .line 115
    :cond_20
    iget-object v3, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    .line 116
    iget-object v4, p2, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    .line 117
    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    .line 118
    iget v5, v3, Lcom/google/android/location/e/w;->c:I

    iget v6, v4, Lcom/google/android/location/e/w;->c:I

    add-int/2addr v5, v6

    const v6, 0x3567e0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    .line 121
    if-gt v0, v5, :cond_45

    move v0, v1

    .line 127
    :goto_39
    if-eqz v0, :cond_49

    .line 128
    iget v0, v3, Lcom/google/android/location/e/w;->c:I

    iget v3, v4, Lcom/google/android/location/e/w;->c:I

    if-le v0, v3, :cond_47

    .line 133
    :cond_41
    :goto_41
    if-nez v1, :cond_15

    move-object p2, p1

    .line 136
    goto :goto_15

    :cond_45
    move v0, v2

    .line 121
    goto :goto_39

    :cond_47
    move v1, v2

    .line 128
    goto :goto_41

    .line 131
    :cond_49
    iget v0, v3, Lcom/google/android/location/e/w;->d:I

    iget v3, v4, Lcom/google/android/location/e/w;->d:I

    if-lt v0, v3, :cond_41

    move v1, v2

    goto :goto_41
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;Lcom/google/android/location/e/g;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/t;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/location/g/e;->b:Lcom/google/android/location/g/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/a;->a(Lcom/google/android/location/e/f;)Lcom/google/android/location/e/c;

    move-result-object v1

    .line 62
    if-nez p4, :cond_1f

    if-eqz v1, :cond_1f

    .line 63
    iget-object v0, v1, Lcom/google/android/location/e/c;->d:Lcom/google/android/location/e/o$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v0, v2, :cond_1f

    .line 64
    iget-object v0, v1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    .line 65
    new-instance p4, Lcom/google/android/location/e/w;

    iget v2, v0, Lcom/google/android/location/e/w;->a:I

    iget v3, v0, Lcom/google/android/location/e/w;->b:I

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    mul-int/lit8 v0, v0, 0x4

    invoke-direct {p4, v2, v3, v0}, Lcom/google/android/location/e/w;-><init>(III)V

    .line 71
    :cond_1f
    if-nez p3, :cond_3b

    const/4 v0, 0x0

    .line 72
    :goto_22
    invoke-direct {p0, p2, v0, p4}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/e/E;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v2

    .line 75
    invoke-direct {p0, v2, v1}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)Lcom/google/android/location/e/o;

    move-result-object v0

    .line 77
    if-nez v0, :cond_35

    if-eqz p3, :cond_35

    iget-object v3, p3, Lcom/google/android/location/e/g;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_35

    move-object v0, p3

    .line 82
    :cond_35
    new-instance v3, Lcom/google/android/location/e/t;

    invoke-direct {v3, v0, v2, v1, p3}, Lcom/google/android/location/e/t;-><init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V

    return-object v3

    .line 71
    :cond_3b
    iget-object v0, p3, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    goto :goto_22
.end method
