.class Lcom/google/android/location/g/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/f$b;,
        Lcom/google/android/location/g/f$a;
    }
.end annotation


# static fields
.field static final a:[[I

.field private static final b:Ljava/util/logging/Logger;

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/android/location/g/n;

.field private static final g:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/location/g/g;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x78

    .line 55
    const-class v0, Lcom/google/android/location/g/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/f;->c:Ljava/util/Set;

    .line 108
    filled-new-array {v5, v5}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    sput-object v0, Lcom/google/android/location/g/f;->a:[[I

    .line 110
    new-instance v0, Lcom/google/android/location/g/o;

    invoke-direct {v0}, Lcom/google/android/location/g/o;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->d:Lcom/google/android/location/g/n;

    move v2, v1

    .line 119
    :goto_2b
    if-ge v2, v5, :cond_41

    move v0, v1

    .line 120
    :goto_2e
    if-ge v0, v5, :cond_3d

    .line 121
    sget-object v3, Lcom/google/android/location/g/f;->a:[[I

    aget-object v3, v3, v2

    invoke-static {v2, v0}, Lcom/google/android/location/g/f;->a(II)I

    move-result v4

    aput v4, v3, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    .line 119
    :cond_3d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2b

    .line 260
    :cond_41
    new-instance v0, Lcom/google/android/location/g/f$1;

    invoke-direct {v0}, Lcom/google/android/location/g/f$1;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->g:Ljava/util/Comparator;

    .line 373
    new-instance v0, Lcom/google/android/location/g/f$2;

    invoke-direct {v0}, Lcom/google/android/location/g/f$2;-><init>()V

    sput-object v0, Lcom/google/android/location/g/f;->h:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Lcom/google/android/location/g/g;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Lcom/google/android/location/g/g;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    .line 142
    iput-object p1, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    .line 143
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/e/C;)D
    .registers 3
    .parameter

    .prologue
    .line 53
    invoke-static {p0}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v0

    return-wide v0
.end method

.method static a(II)I
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/high16 v4, 0x400e

    .line 130
    int-to-double v0, p0

    mul-double/2addr v0, v4

    .line 131
    int-to-double v2, p1

    mul-double/2addr v2, v4

    .line 132
    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/location/e/w;Lcom/google/android/location/g/g;)Lcom/google/android/location/e/u;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/e/w;",
            "Lcom/google/android/location/g/g;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/e/w;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 203
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 204
    const-wide/16 v0, 0x0

    .line 205
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_15
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 206
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 207
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    .line 208
    iget-object v5, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    .line 210
    sget-object v6, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v5, v6, :cond_43

    .line 211
    new-instance v5, Lcom/google/android/location/e/u;

    invoke-direct {v5, v1, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-static {v0}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v0

    add-double/2addr v2, v0

    move-wide v0, v2

    :goto_41
    move-wide v2, v0

    .line 216
    goto :goto_15

    .line 213
    :cond_43
    sget-object v6, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-ne v5, v6, :cond_4f

    .line 214
    new-instance v5, Lcom/google/android/location/e/u;

    invoke-direct {v5, v1, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4f
    move-wide v0, v2

    goto :goto_41

    .line 217
    :cond_51
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_70

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 218
    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "No lre nor minK results found. Returning matrixCenter"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 219
    new-instance v0, Lcom/google/android/location/e/u;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 257
    :goto_6f
    return-object v0

    .line 221
    :cond_70
    sget-object v0, Lcom/google/android/location/g/f;->h:Ljava/util/Comparator;

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 222
    const-wide/16 v0, 0x0

    .line 223
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v6, v0

    :goto_7c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    .line 224
    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/location/e/C;

    .line 225
    invoke-static {v2}, Lcom/google/android/location/g/f;->b(Lcom/google/android/location/e/C;)D

    move-result-wide v3

    add-double/2addr v6, v3

    .line 226
    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    .line 227
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/g/f;->a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V

    goto :goto_7c

    .line 229
    :cond_a6
    const/4 v0, 0x0

    .line 231
    const-wide/high16 v1, 0x4020

    cmpg-double v1, v6, v1

    if-gez v1, :cond_d8

    .line 232
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_b1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    .line 233
    const/4 v8, 0x1

    .line 234
    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/location/e/C;

    .line 235
    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    .line 236
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/g/f;->a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V

    move v0, v8

    .line 237
    goto :goto_b1

    :cond_d8
    move v8, v0

    .line 239
    const-wide v1, 0x3fd999999999999aL

    const-wide/high16 v4, 0x400e

    move-object v0, p4

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/g;->a(DLcom/google/android/location/e/w;D)Lcom/google/android/location/e/w$a;

    move-result-object v0

    .line 242
    if-nez v0, :cond_f2

    .line 243
    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as unable to find dominant circle."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 244
    const/4 v0, 0x0

    goto/16 :goto_6f

    .line 246
    :cond_f2
    invoke-direct {p0, v0, v9}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/w$a;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_103

    .line 247
    if-eqz v8, :cond_103

    .line 248
    iget v1, v0, Lcom/google/android/location/e/w$a;->c:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8

    mul-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Lcom/google/android/location/e/w$a;->c:I

    .line 251
    :cond_103
    invoke-virtual {v0}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    .line 252
    invoke-direct {p0, v0, p1}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/w;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_117

    .line 253
    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "Not returning location as no APs within 75 meters of location."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 254
    const/4 v0, 0x0

    goto/16 :goto_6f

    .line 256
    :cond_117
    const-wide v1, 0x3fd3333333333333L

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    .line 257
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto/16 :goto_6f
.end method

.method private a(Ljava/util/Map;)Lcom/google/android/location/e/w;
    .registers 12
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 427
    .line 429
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v3, v0

    move-wide v8, v0

    move-wide v1, v8

    :goto_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    .line 430
    iget v6, v0, Lcom/google/android/location/e/C;->a:I

    invoke-static {v6}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    add-double/2addr v3, v6

    .line 431
    iget v0, v0, Lcom/google/android/location/e/C;->b:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->a(I)D

    move-result-wide v6

    add-double v0, v1, v6

    move-wide v1, v0

    goto :goto_d

    .line 433
    :cond_2a
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double/2addr v3, v5

    .line 434
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-double v5, v0

    div-double v0, v1, v5

    .line 436
    new-instance v2, Lcom/google/android/location/e/w;

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->b(D)I

    move-result v3

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(D)I

    move-result v0

    const v1, 0x124f8

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/location/e/w;-><init>(III)V

    return-object v2
.end method

.method private a(I)Lcom/google/android/location/g/b;
    .registers 6
    .parameter

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 281
    new-instance v0, Lcom/google/android/location/g/b;

    invoke-direct {v0, p1, v2, v2, v1}, Lcom/google/android/location/g/b;-><init>(III[F)V

    .line 282
    iget-object v2, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    sget-object v3, Lcom/google/android/location/g/f;->g:Ljava/util/Comparator;

    invoke-static {v2, v0, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 283
    if-gez v0, :cond_13

    move-object v0, v1

    .line 290
    :cond_12
    :goto_12
    return-object v0

    .line 286
    :cond_13
    iget-object v2, p0, Lcom/google/android/location/g/f;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/b;

    .line 287
    iget v2, v0, Lcom/google/android/location/g/b;->b:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_12

    move-object v0, v1

    .line 288
    goto :goto_12
.end method

.method private a(Ljava/util/List;)Lcom/google/android/location/g/f$a;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/f$a;",
            ">;)",
            "Lcom/google/android/location/g/f$a;"
        }
    .end annotation

    .prologue
    .line 526
    new-instance v1, Lcom/google/android/location/g/f$a;

    invoke-direct {v1}, Lcom/google/android/location/g/f$a;-><init>()V

    .line 527
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    .line 528
    invoke-virtual {v1}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    .line 530
    :cond_21
    return-object v1
.end method

.method private a()Lcom/google/android/location/g/n$a;
    .registers 5

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/location/g/n$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/g/f;->c:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    return-object v0
.end method

.method private a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 443
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 444
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 445
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 448
    :cond_2f
    return-object v1
.end method

.method private a(Ljava/lang/Long;Lcom/google/android/location/e/C;ILcom/google/android/location/e/w;Lcom/google/android/location/g/g;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 307
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->c(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v4, 0x400e

    div-double/2addr v2, v4

    double-to-int v10, v2

    .line 308
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->d(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v4, 0x400e

    div-double/2addr v2, v4

    double-to-int v11, v2

    .line 310
    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/location/g/f;->a(I)Lcom/google/android/location/g/b;

    move-result-object v12

    .line 312
    const/4 v4, 0x0

    .line 313
    const/4 v3, 0x0

    .line 314
    const/4 v2, 0x1

    .line 316
    if-eqz v12, :cond_2e

    iget-object v5, v12, Lcom/google/android/location/g/b;->d:[F

    array-length v5, v5

    if-lez v5, :cond_2e

    .line 317
    iget-object v2, v12, Lcom/google/android/location/g/b;->d:[F

    array-length v4, v2

    .line 318
    iget v3, v12, Lcom/google/android/location/g/b;->c:I

    .line 319
    const/4 v2, 0x0

    .line 322
    :cond_2e
    const/16 v5, -0x28

    move v9, v5

    :goto_31
    const/16 v5, 0x28

    if-gt v9, v5, :cond_96

    .line 323
    sub-int v5, v9, v10

    .line 324
    if-gez v5, :cond_3a

    .line 325
    neg-int v5, v5

    .line 327
    :cond_3a
    const/16 v6, -0x28

    move v8, v6

    :goto_3d
    const/16 v6, 0x28

    if-gt v8, v6, :cond_92

    .line 328
    sub-int v6, v8, v11

    .line 329
    if-gez v6, :cond_46

    .line 330
    neg-int v6, v6

    .line 334
    :cond_46
    const/16 v7, 0x78

    if-ge v6, v7, :cond_4e

    const/16 v7, 0x78

    if-lt v5, v7, :cond_69

    .line 335
    :cond_4e
    invoke-static {v5, v6}, Lcom/google/android/location/g/f;->a(II)I

    move-result v6

    .line 342
    :goto_52
    if-eqz v2, :cond_76

    .line 343
    int-to-double v6, v6

    const-wide v13, 0x4052c00000000000L

    cmpg-double v6, v6, v13

    if-gez v6, :cond_70

    const-wide/high16 v6, 0x3fe0

    .line 357
    :goto_60
    move-object/from16 v0, p5

    invoke-virtual {v0, v9, v8, v6, v7}, Lcom/google/android/location/g/g;->a(IID)V

    .line 327
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_3d

    .line 337
    :cond_69
    sget-object v7, Lcom/google/android/location/g/f;->a:[[I

    aget-object v7, v7, v5

    aget v6, v7, v6

    goto :goto_52

    .line 343
    :cond_70
    const-wide v6, 0x3fa999999999999aL

    goto :goto_60

    .line 347
    :cond_76
    if-ge v6, v3, :cond_7f

    .line 348
    iget-object v6, v12, Lcom/google/android/location/g/b;->d:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    float-to-double v6, v6

    goto :goto_60

    .line 350
    :cond_7f
    add-int v7, v3, v4

    if-lt v6, v7, :cond_8b

    .line 351
    iget-object v6, v12, Lcom/google/android/location/g/b;->d:[F

    add-int/lit8 v7, v4, -0x1

    aget v6, v6, v7

    float-to-double v6, v6

    goto :goto_60

    .line 353
    :cond_8b
    iget-object v7, v12, Lcom/google/android/location/g/b;->d:[F

    sub-int/2addr v6, v3

    aget v6, v7, v6

    float-to-double v6, v6

    goto :goto_60

    .line 322
    :cond_92
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_31

    .line 360
    :cond_96
    return-void
.end method

.method private a(Lcom/google/android/location/e/C;Lcom/google/android/location/e/C;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 551
    invoke-static {p1, p2}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    const/16 v1, 0xc8

    if-gt v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private a(Lcom/google/android/location/e/C;Lcom/google/android/location/g/f$a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 534
    invoke-virtual {p2}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    .line 535
    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/C;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/C;Lcom/google/android/location/e/C;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 536
    const/4 v0, 0x1

    .line 539
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private a(Lcom/google/android/location/e/w$a;Ljava/util/List;)Z
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w$a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 409
    const-wide/16 v0, 0x0

    .line 410
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    .line 411
    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/C;

    .line 413
    iget v4, v0, Lcom/google/android/location/e/C;->i:I

    const v5, 0x9c40

    if-ge v4, v5, :cond_40

    .line 414
    invoke-static {p1, v0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    :goto_26
    move-wide v1, v0

    .line 416
    goto :goto_7

    .line 418
    :cond_28
    const-wide v3, 0x408f400000000000L

    mul-double v0, v3, v1

    const-wide v2, 0x3ff3333333333333L

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 419
    iget v1, p1, Lcom/google/android/location/e/w$a;->c:I

    if-le v0, v1, :cond_3e

    .line 420
    iput v0, p1, Lcom/google/android/location/e/w$a;->c:I

    .line 421
    const/4 v0, 0x1

    .line 423
    :goto_3d
    return v0

    :cond_3e
    const/4 v0, 0x0

    goto :goto_3d

    :cond_40
    move-wide v0, v1

    goto :goto_26
.end method

.method private a(Lcom/google/android/location/e/w;Ljava/util/Map;)Z
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 392
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    .line 393
    invoke-static {p1, v0}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide v4, 0x4052c00000000000L

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_8

    .line 394
    const/4 v0, 0x0

    .line 397
    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x1

    goto :goto_22
.end method

.method private static b(Lcom/google/android/location/e/C;)D
    .registers 5
    .parameter

    .prologue
    .line 368
    iget v0, p0, Lcom/google/android/location/e/C;->i:I

    int-to-double v0, v0

    const-wide v2, 0x408f400000000000L

    div-double/2addr v0, v2

    .line 369
    const-wide/high16 v2, 0x4024

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 370
    const-wide/high16 v2, 0x4034

    div-double v0, v2, v0

    return-wide v0
.end method

.method private b(Ljava/util/Map;)Ljava/util/Set;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->c(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 455
    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->d(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 456
    return-object v0
.end method

.method private c(Ljava/util/Map;)Ljava/util/Set;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    sget-object v0, Lcom/google/android/location/g/f;->d:Lcom/google/android/location/g/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    .line 463
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/google/android/location/g/n$a;->c()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private d(Ljava/util/Map;)Ljava/util/Set;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 473
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 474
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 475
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 476
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_17
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 477
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 478
    new-instance v2, Lcom/google/android/location/g/f$a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/location/g/f$a;-><init>(Lcom/google/android/location/e/u;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/f$a;

    .line 480
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/C;

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/g/f;->a(Lcom/google/android/location/e/C;Lcom/google/android/location/g/f$a;)Z

    move-result v2

    if-eqz v2, :cond_59

    .line 481
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3d

    .line 483
    :cond_59
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3d

    .line 486
    :cond_5d
    invoke-direct {p0, v5}, Lcom/google/android/location/g/f;->a(Ljava/util/List;)Lcom/google/android/location/g/f$a;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 488
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 489
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 490
    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_17

    .line 492
    :cond_71
    new-instance v0, Lcom/google/android/location/g/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/g/f$b;-><init>(Lcom/google/android/location/g/f;Lcom/google/android/location/g/f$1;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 493
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 494
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_c6

    .line 495
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v4

    .line 496
    const/4 v0, 0x1

    move v1, v0

    :goto_92
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c6

    .line 497
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/f$a;->a()Ljava/util/List;

    move-result-object v0

    .line 498
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_c2

    .line 499
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_b0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    .line 500
    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_b0

    .line 496
    :cond_c2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_92

    .line 505
    :cond_c6
    return-object v2
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .registers 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/location/g/f;->b(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v3

    .line 150
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_13

    .line 151
    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    .line 176
    :goto_12
    return-object v0

    .line 153
    :cond_13
    invoke-direct {p0, p1, v3}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    invoke-virtual {v1}, Lcom/google/android/location/g/g;->a()V

    .line 156
    invoke-direct {p0, v0}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;)Lcom/google/android/location/e/w;

    move-result-object v1

    .line 158
    iget-object v2, p0, Lcom/google/android/location/g/f;->f:Lcom/google/android/location/g/g;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/location/g/f;->a(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/location/e/w;Lcom/google/android/location/g/g;)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 161
    if-nez v1, :cond_2d

    .line 162
    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_12

    .line 165
    :cond_2d
    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/w;

    .line 166
    if-nez v0, :cond_3f

    .line 167
    sget-object v0, Lcom/google/android/location/g/f;->b:Ljava/util/logging/Logger;

    const-string v1, "No location found by lre localizer"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 168
    invoke-direct {p0}, Lcom/google/android/location/g/f;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_12

    .line 171
    :cond_3f
    iget-object v1, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 172
    const/16 v1, 0x50

    .line 173
    const-wide v6, 0x3fd3333333333333L

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_54

    .line 174
    const/16 v1, 0x4e

    .line 176
    :cond_54
    new-instance v2, Lcom/google/android/location/g/n$a;

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    move-object v0, v2

    goto :goto_12
.end method
