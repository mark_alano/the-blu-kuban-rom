.class Lcom/google/android/location/g/k$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/g/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/g/k$a;->c:I

    .line 58
    iput p1, p0, Lcom/google/android/location/g/k$a;->a:I

    .line 59
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    .line 60
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 91
    iget v1, p0, Lcom/google/android/location/g/k$a;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/g/k$a;->c:I

    .line 92
    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 74
    iget-object v1, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 76
    if-nez v0, :cond_19

    .line 77
    iget v0, p0, Lcom/google/android/location/g/k$a;->c:I

    iget v1, p0, Lcom/google/android/location/g/k$a;->a:I

    if-ge v0, v1, :cond_1a

    .line 78
    iget v0, p0, Lcom/google/android/location/g/k$a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/g/k$a;->c:I

    .line 83
    :cond_19
    :goto_19
    return-void

    .line 80
    :cond_1a
    iget-object v0, p0, Lcom/google/android/location/g/k$a;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_19
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/location/g/k$a;->c:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method
