.class public Lcom/google/android/location/g/k;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/k$1;,
        Lcom/google/android/location/g/k$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/location/g/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/g/k$a",
            "<",
            "Lcom/google/android/location/e/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/location/g/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/g/k$a",
            "<",
            "Lcom/google/android/location/e/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x3

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Lcom/google/android/location/g/k$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/g/k$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    .line 102
    new-instance v0, Lcom/google/android/location/g/k$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/g/k$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/s;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_11

    .line 130
    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/s;

    .line 135
    :goto_10
    return-object v0

    .line 131
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_22

    .line 132
    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/s;

    goto :goto_10

    .line 135
    :cond_22
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public a(Lcom/google/android/location/e/s;)V
    .registers 4
    .parameter

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/location/g/k$1;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/e/s$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1c

    .line 122
    :goto_f
    return-void

    .line 114
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/k$a;->a(Ljava/lang/Object;)V

    goto :goto_f

    .line 117
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/k$a;->a(Ljava/lang/Object;)V

    goto :goto_f

    .line 112
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_10
        :pswitch_16
    .end packed-switch
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
