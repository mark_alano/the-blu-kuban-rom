.class public Lcom/google/android/location/g/p;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/p$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/d/d;

.field private final b:Lcom/google/android/location/g/n;

.field private final c:Lcom/google/android/location/g/n;

.field private final d:Lcom/google/android/location/g/i;

.field private final e:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/location/g/q;

.field private final g:Lcom/google/android/location/g/r;

.field private final h:Lcom/google/android/location/os/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/f;Lcom/google/android/location/b/i;Lcom/google/android/location/g/i;Lcom/google/android/location/g/q;Lcom/google/android/location/g/r;Lcom/google/android/location/os/c;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/g/o;",
            "Lcom/google/android/location/g/f;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/g/i;",
            "Lcom/google/android/location/g/q;",
            "Lcom/google/android/location/g/r;",
            "Lcom/google/android/location/os/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {}, Lcom/google/android/location/d/d;->a()Lcom/google/android/location/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    .line 120
    iput-object p1, p0, Lcom/google/android/location/g/p;->b:Lcom/google/android/location/g/n;

    .line 121
    iput-object p2, p0, Lcom/google/android/location/g/p;->c:Lcom/google/android/location/g/n;

    .line 122
    iput-object p4, p0, Lcom/google/android/location/g/p;->d:Lcom/google/android/location/g/i;

    .line 123
    iput-object p3, p0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    .line 124
    iput-object p5, p0, Lcom/google/android/location/g/p;->f:Lcom/google/android/location/g/q;

    .line 125
    iput-object p6, p0, Lcom/google/android/location/g/p;->g:Lcom/google/android/location/g/r;

    .line 126
    iput-object p7, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    .line 127
    return-void
.end method

.method private a(Ljava/util/List;)I
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 348
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 349
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    .line 350
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_2b

    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    :goto_2a
    return v0

    :cond_2b
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2a
.end method

.method private a(Ljava/util/Map;)Lcom/google/android/location/e/C$a;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Lcom/google/android/location/e/C$a;"
        }
    .end annotation

    .prologue
    .line 298
    const/4 v1, -0x1

    .line 299
    sget-object v0, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    .line 300
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move-object v1, v0

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 301
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget-object v0, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    .line 302
    invoke-virtual {v0}, Lcom/google/android/location/e/C$a;->ordinal()I

    move-result v4

    if-le v4, v2, :cond_37

    .line 303
    invoke-static {}, Lcom/google/android/location/e/C$a;->a()Lcom/google/android/location/e/C$a;

    move-result-object v1

    if-ne v0, v1, :cond_2e

    .line 311
    :goto_2d
    return-object v0

    .line 307
    :cond_2e
    invoke-virtual {v0}, Lcom/google/android/location/e/C$a;->ordinal()I

    move-result v1

    :goto_32
    move v2, v1

    move-object v1, v0

    .line 310
    goto :goto_d

    :cond_35
    move-object v0, v1

    .line 311
    goto :goto_2d

    :cond_37
    move-object v0, v1

    move v1, v2

    goto :goto_32
.end method

.method private a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 229
    if-eqz p1, :cond_6

    iget-object v0, p1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-nez v0, :cond_8

    :cond_6
    move-object p1, p2

    .line 238
    :cond_7
    :goto_7
    return-object p1

    .line 232
    :cond_8
    if-eqz p2, :cond_7

    iget-object v0, p2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v0, :cond_7

    .line 235
    iget-object v0, p1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    iget-object v1, p2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    if-le v0, v1, :cond_7

    move-object p1, p2

    .line 238
    goto :goto_7
.end method

.method private a(Lcom/google/android/location/e/E;)Lcom/google/android/location/e/E;
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 268
    if-nez p1, :cond_5

    .line 269
    const/4 p1, 0x0

    .line 294
    :cond_4
    :goto_4
    return-object p1

    .line 272
    :cond_5
    const/4 v2, 0x1

    move v0, v1

    .line 273
    :goto_7
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_49

    .line 274
    iget-object v3, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/location/d/d;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-nez v3, :cond_3d

    move v0, v1

    .line 280
    :goto_1a
    if-nez v0, :cond_4

    .line 286
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 287
    :goto_25
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-ge v1, v0, :cond_40

    .line 288
    invoke-virtual {p1, v1}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v0

    .line 289
    iget-object v3, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {v3, v0}, Lcom/google/android/location/d/d;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 290
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_3a
    add-int/lit8 v1, v1, 0x1

    goto :goto_25

    .line 273
    :cond_3d
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 294
    :cond_40
    new-instance v0, Lcom/google/android/location/e/E;

    iget-wide v3, p1, Lcom/google/android/location/e/E;->a:J

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/location/e/E;-><init>(JLjava/util/ArrayList;)V

    move-object p1, v0

    goto :goto_4

    :cond_49
    move v0, v2

    goto :goto_1a
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    .prologue
    .line 244
    invoke-direct {p0, p1}, Lcom/google/android/location/g/p;->a(Ljava/util/Map;)Lcom/google/android/location/e/C$a;

    move-result-object v0

    .line 246
    sget-object v1, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    if-ne v0, v1, :cond_a

    .line 248
    const/4 v0, 0x0

    .line 259
    :goto_9
    return-object v0

    .line 249
    :cond_a
    sget-object v1, Lcom/google/android/location/e/C$a;->b:Lcom/google/android/location/e/C$a;

    if-ne v0, v1, :cond_15

    .line 252
    iget-object v0, p0, Lcom/google/android/location/g/p;->b:Lcom/google/android/location/g/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_9

    .line 257
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/g/p;->c:Lcom/google/android/location/g/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/g/n;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto :goto_9
.end method

.method public static a(Lcom/google/android/location/g/l;Lcom/google/android/location/b/i;Lcom/google/android/location/os/i;)Lcom/google/android/location/g/p;
    .registers 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/g/l;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/os/i;",
            ")",
            "Lcom/google/android/location/g/p;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v1, Lcom/google/android/location/g/o;

    invoke-direct {v1}, Lcom/google/android/location/g/o;-><init>()V

    .line 91
    invoke-interface {p2}, Lcom/google/android/location/os/i;->o()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/g/h;->a(Ljava/io/InputStream;)Lcom/google/android/location/g/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/g/h;->a()Ljava/util/List;

    move-result-object v0

    .line 93
    new-instance v2, Lcom/google/android/location/g/f;

    invoke-direct {v2, v0}, Lcom/google/android/location/g/f;-><init>(Ljava/util/List;)V

    .line 96
    new-instance v4, Lcom/google/android/location/g/i;

    invoke-direct {v4, p0}, Lcom/google/android/location/g/i;-><init>(Lcom/google/android/location/g/j;)V

    .line 97
    invoke-static {p2}, Lcom/google/android/location/g/q;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/g/q;

    move-result-object v5

    .line 98
    new-instance v6, Lcom/google/android/location/g/r;

    invoke-direct {v6, p2}, Lcom/google/android/location/g/r;-><init>(Lcom/google/android/location/os/c;)V

    .line 100
    new-instance v0, Lcom/google/android/location/g/p;

    move-object v3, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/g/p;-><init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/f;Lcom/google/android/location/b/i;Lcom/google/android/location/g/i;Lcom/google/android/location/g/q;Lcom/google/android/location/g/r;Lcom/google/android/location/os/c;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;III)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 589
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 590
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    const-string v1, " hasLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 592
    add-int v1, p2, p3

    sub-int v1, p4, v1

    .line 593
    const-string v2, " noLocation="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 594
    const-string v1, " cacheMiss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 596
    return-void
.end method

.method static a(Ljava/util/Collection;D)Z
    .registers 15
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/location/e/C;",
            ">;D)Z"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 523
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_a

    move v0, v9

    .line 538
    :goto_9
    return v0

    .line 527
    :cond_a
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/location/e/C;

    .line 528
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/location/e/C;

    .line 529
    if-eq v8, v6, :cond_1f

    .line 530
    iget v0, v8, Lcom/google/android/location/e/w;->a:I

    int-to-double v0, v0

    iget v2, v8, Lcom/google/android/location/e/w;->b:I

    int-to-double v2, v2

    iget v4, v6, Lcom/google/android/location/e/w;->a:I

    int-to-double v4, v4

    iget v6, v6, Lcom/google/android/location/e/w;->b:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/e/i;->a(DDDD)D

    move-result-wide v0

    .line 532
    cmpg-double v0, v0, p1

    if-gez v0, :cond_1f

    move v0, v9

    .line 533
    goto :goto_9

    .line 538
    :cond_44
    const/4 v0, 0x1

    goto :goto_9
.end method

.method static a(Ljava/util/Set;)Z
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 548
    .line 550
    const-wide/16 v0, 0x0

    .line 552
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v4

    move-wide v11, v0

    move-wide v1, v11

    :goto_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 554
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide v9, 0xfcffff000000L

    and-long/2addr v5, v9

    .line 556
    if-eqz v3, :cond_29

    move-wide v0, v5

    move v2, v7

    :goto_25
    move v3, v2

    move-wide v11, v0

    move-wide v1, v11

    .line 566
    goto :goto_b

    .line 560
    :cond_29
    cmp-long v0, v5, v1

    if-eqz v0, :cond_31

    move v0, v4

    .line 562
    :goto_2e
    if-eqz v0, :cond_35

    .line 568
    :goto_30
    return v4

    :cond_31
    move v0, v7

    .line 560
    goto :goto_2e

    :cond_33
    move v4, v7

    .line 568
    goto :goto_30

    :cond_35
    move-wide v11, v1

    move-wide v0, v11

    move v2, v3

    goto :goto_25
.end method


# virtual methods
.method public a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;
    .registers 14
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/E;",
            ">;",
            "Lcom/google/android/location/e/w;",
            ")",
            "Lcom/google/android/location/e/D;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/location/g/p;->a:Lcom/google/android/location/d/d;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/d/e;)Ljava/util/Map;

    move-result-object v9

    .line 143
    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/location/g/p;->a(Ljava/util/Set;Lcom/google/android/location/e/w;)Lcom/google/android/location/g/p$a;

    move-result-object v0

    .line 146
    const/high16 v1, -0x8000

    .line 147
    iget-object v6, v0, Lcom/google/android/location/g/p$a;->b:Ljava/util/Map;

    .line 148
    iget-object v10, v0, Lcom/google/android/location/g/p$a;->a:Lcom/google/android/location/e/o$a;

    .line 151
    iget-object v0, v0, Lcom/google/android/location/g/p$a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 152
    invoke-interface {v9, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1f

    .line 155
    :cond_2f
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E;

    invoke-direct {p0, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/E;)Lcom/google/android/location/e/E;

    move-result-object v5

    .line 160
    iget-object v0, p0, Lcom/google/android/location/g/p;->f:Lcom/google/android/location/g/q;

    invoke-virtual {v0, v9}, Lcom/google/android/location/g/q;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 161
    iget-object v2, p0, Lcom/google/android/location/g/p;->g:Lcom/google/android/location/g/r;

    invoke-virtual {v2, v0}, Lcom/google/android/location/g/r;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 166
    iget-object v2, p0, Lcom/google/android/location/g/p;->d:Lcom/google/android/location/g/i;

    invoke-virtual {v2, v7, v0}, Lcom/google/android/location/g/i;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v8

    .line 168
    if-eqz v8, :cond_ff

    .line 169
    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_73

    .line 171
    new-instance v0, Lcom/google/android/location/e/D;

    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    .line 174
    iget-object v1, v0, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_74

    iget-object v1, v0, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    const/16 v2, 0x7530

    if-ge v1, v2, :cond_74

    .line 220
    :goto_72
    return-object v0

    :cond_73
    move-object v0, v7

    .line 186
    :cond_74
    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    .line 187
    invoke-virtual {v8}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    iget v1, v1, Lcom/google/android/location/e/w;->g:I

    move-object v8, v0

    move v0, v1

    move-object v1, v2

    .line 192
    :goto_83
    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v10, v2, :cond_99

    .line 193
    new-instance v0, Lcom/google/android/location/e/D;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    move-object v2, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    .line 195
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_72

    .line 198
    :cond_99
    invoke-direct {p0, v6, v9}, Lcom/google/android/location/g/p;->a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;

    move-result-object v2

    .line 200
    if-eqz v2, :cond_a5

    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v3

    if-nez v3, :cond_b8

    .line 202
    :cond_a5
    new-instance v0, Lcom/google/android/location/e/D;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    .line 204
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_72

    .line 205
    :cond_b8
    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/g/c;->c(Lcom/google/android/location/e/w;)Z

    move-result v3

    if-nez v3, :cond_d5

    .line 207
    new-instance v0, Lcom/google/android/location/e/D;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    iget-object v1, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v1}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    move-object v1, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    .line 209
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto :goto_72

    .line 214
    :cond_d5
    new-instance v3, Lcom/google/android/location/e/w$a;

    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->a()Lcom/google/android/location/e/w;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/location/e/w$a;-><init>(Lcom/google/android/location/e/w;)V

    .line 215
    invoke-virtual {v2}, Lcom/google/android/location/g/n$a;->b()I

    move-result v2

    iput v2, v3, Lcom/google/android/location/e/w$a;->d:I

    .line 216
    iput-object v1, v3, Lcom/google/android/location/e/w$a;->f:Ljava/lang/String;

    .line 217
    iput v0, v3, Lcom/google/android/location/e/w$a;->g:I

    .line 218
    new-instance v0, Lcom/google/android/location/e/D;

    invoke-virtual {v3}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    iget-object v3, p0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/e/D;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;JLcom/google/android/location/e/E;Ljava/util/Map;)V

    .line 220
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/e/D;Lcom/google/android/location/e/D;)Lcom/google/android/location/e/D;

    move-result-object v0

    goto/16 :goto_72

    :cond_ff
    move-object v8, v7

    move v0, v1

    move-object v1, v7

    goto :goto_83
.end method

.method a(Ljava/util/Set;Lcom/google/android/location/e/w;)Lcom/google/android/location/g/p$a;
    .registers 23
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/location/e/w;",
            ")",
            "Lcom/google/android/location/g/p$a;"
        }
    .end annotation

    .prologue
    .line 364
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 365
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 366
    const/4 v1, 0x0

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/g/p;->h:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->b()J

    move-result-wide v14

    .line 369
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v10, v1

    :goto_18
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/lang/Long;

    .line 370
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v1, v9, v14, v15}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v17

    .line 372
    if-eqz v17, :cond_88

    .line 373
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/google/android/location/e/C;

    .line 374
    invoke-virtual {v3}, Lcom/google/android/location/e/C;->a()Z

    move-result v1

    if-eqz v1, :cond_7e

    iget-object v1, v3, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v2, Lcom/google/android/location/e/C$a;->a:Lcom/google/android/location/e/C$a;

    if-eq v1, v2, :cond_7e

    .line 375
    const/4 v11, 0x1

    .line 377
    if-eqz p2, :cond_191

    .line 378
    move-object/from16 v0, p2

    iget v1, v0, Lcom/google/android/location/e/w;->c:I

    int-to-double v1, v1

    const-wide v4, 0x408f400000000000L

    div-double v18, v1, v4

    .line 379
    iget v1, v3, Lcom/google/android/location/e/C;->a:I

    int-to-double v1, v1

    iget v3, v3, Lcom/google/android/location/e/C;->b:I

    int-to-double v3, v3

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/location/e/w;->a:I

    int-to-double v5, v5

    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/location/e/w;->b:I

    int-to-double v7, v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/e/i;->a(DDDD)D

    move-result-wide v1

    .line 382
    cmpl-double v1, v1, v18

    if-lez v1, :cond_191

    .line 383
    const/4 v1, 0x0

    .line 395
    :goto_6a
    if-eqz v1, :cond_76

    .line 396
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v12, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_73
    :goto_73
    move v1, v10

    :goto_74
    move v10, v1

    .line 411
    goto :goto_18

    .line 398
    :cond_76
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v13, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_73

    .line 400
    :cond_7e
    invoke-virtual {v3}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-eqz v1, :cond_73

    .line 406
    invoke-virtual {v3}, Lcom/google/android/location/e/C;->c()V

    goto :goto_73

    .line 409
    :cond_88
    add-int/lit8 v1, v10, 0x1

    goto :goto_74

    .line 420
    :cond_8b
    if-nez p2, :cond_a7

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    const-wide v2, 0x40b3880000000000L

    invoke-static {v1, v2, v3}, Lcom/google/android/location/g/p;->a(Ljava/util/Collection;D)Z

    move-result v1

    if-eqz v1, :cond_a7

    .line 422
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    .line 511
    :goto_a6
    return-object v1

    .line 434
    :cond_a7
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_117

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_117

    .line 435
    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/g/p;->a(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_e5

    .line 443
    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_df

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    const-wide v2, 0x40b3880000000000L

    invoke-static {v1, v2, v3}, Lcom/google/android/location/g/p;->a(Ljava/util/Collection;D)Z

    move-result v1

    if-eqz v1, :cond_df

    .line 445
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_a6

    .line 448
    :cond_df
    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 449
    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 465
    :cond_e5
    :goto_e5
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v8

    .line 471
    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_f1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 476
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v1, v3, v14, v15}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;

    move-result-object v4

    .line 479
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/g/p;->e:Lcom/google/android/location/b/i;

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/google/android/location/b/a;->c()I

    move-result v4

    invoke-static {}, Lcom/google/android/location/e/C;->b()Lcom/google/android/location/e/C;

    move-result-object v5

    move-wide v6, v14

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    goto :goto_f1

    .line 451
    :cond_117
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e5

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_e5

    .line 461
    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 462
    invoke-interface {v13}, Ljava/util/Map;->clear()V

    goto :goto_e5

    .line 485
    :cond_12b
    if-lez v8, :cond_161

    .line 488
    const/4 v1, 0x5

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v8, v1, :cond_14a

    .line 490
    const-string v1, "Good cache hits. Computing WiFi location locally"

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    .line 492
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v12, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_a6

    .line 499
    :cond_14a
    const-string v1, "Not enough positive cache hits compared to misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    .line 501
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_a6

    .line 503
    :cond_161
    if-lez v10, :cond_17a

    .line 505
    const-string v1, "Too many cache  misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    .line 506
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_a6

    .line 509
    :cond_17a
    const-string v1, "Too many no-location APs. Will not compute a location nor go to the server."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v8, v10, v2}, Lcom/google/android/location/g/p;->a(Ljava/lang/String;III)V

    .line 511
    new-instance v1, Lcom/google/android/location/g/p$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v3, v13}, Lcom/google/android/location/g/p$a;-><init>(Lcom/google/android/location/g/p;Lcom/google/android/location/e/o$a;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_a6

    :cond_191
    move v1, v11

    goto/16 :goto_6a
.end method

.method a(Ljava/util/List;Lcom/google/android/location/d/e;)Ljava/util/Map;
    .registers 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/E;",
            ">;",
            "Lcom/google/android/location/d/e;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 316
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 318
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E;

    .line 319
    if-eqz v0, :cond_9

    .line 322
    invoke-virtual {v0}, Lcom/google/android/location/e/E;->a()I

    move-result v5

    .line 323
    const/4 v1, 0x0

    move v2, v1

    :goto_1d
    if-ge v2, v5, :cond_9

    .line 324
    invoke-virtual {v0, v2}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v6

    .line 325
    invoke-interface {p2, v6}, Lcom/google/android/location/d/e;->a(Lcom/google/android/location/e/E$a;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 323
    :goto_29
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1d

    .line 328
    :cond_2d
    iget-object v1, v6, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 329
    if-nez v1, :cond_41

    .line 330
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 331
    iget-object v7, v6, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v3, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    :cond_41
    iget v6, v6, Lcom/google/android/location/e/E$a;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_29

    .line 338
    :cond_4b
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 339
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_58
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 340
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 341
    invoke-direct {p0, v1}, Lcom/google/android/location/g/p;->a(Ljava/util/List;)I

    move-result v1

    .line 342
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_58

    .line 344
    :cond_7a
    return-object v2
.end method
