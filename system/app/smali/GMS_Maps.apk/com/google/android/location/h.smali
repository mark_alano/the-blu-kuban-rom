.class public Lcom/google/android/location/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/h;->b:J

    .line 51
    iput-boolean v2, p0, Lcom/google/android/location/h;->c:Z

    .line 52
    iput-boolean v2, p0, Lcom/google/android/location/h;->d:Z

    .line 53
    iput-boolean v2, p0, Lcom/google/android/location/h;->e:Z

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/h;->f:I

    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/h;->c:Z

    .line 97
    return-void
.end method


# virtual methods
.method public a(JZ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x3

    .line 66
    if-eqz p3, :cond_b

    .line 67
    invoke-direct {p0}, Lcom/google/android/location/h;->a()V

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/h;->b:J

    .line 91
    :cond_a
    :goto_a
    return-void

    .line 70
    :cond_b
    iput-wide p1, p0, Lcom/google/android/location/h;->b:J

    .line 71
    iget-boolean v0, p0, Lcom/google/android/location/h;->e:Z

    if-eqz v0, :cond_51

    .line 74
    iget-boolean v0, p0, Lcom/google/android/location/h;->c:Z

    if-nez v0, :cond_a

    .line 75
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v2, :cond_22

    .line 76
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 78
    :cond_22
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt v0, v2, :cond_a

    iget-object v0, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_a

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h;->c:Z

    goto :goto_a

    .line 88
    :cond_51
    invoke-direct {p0}, Lcom/google/android/location/h;->a()V

    goto :goto_a
.end method

.method public a(ZZI)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/location/h;->e:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/location/h;->f:I

    if-eq v0, p3, :cond_b

    .line 105
    :cond_8
    invoke-direct {p0}, Lcom/google/android/location/h;->a()V

    .line 107
    :cond_b
    iput-boolean p1, p0, Lcom/google/android/location/h;->e:Z

    .line 108
    iput-boolean p2, p0, Lcom/google/android/location/h;->d:Z

    .line 109
    if-eqz p1, :cond_13

    .line 110
    iput p3, p0, Lcom/google/android/location/h;->f:I

    .line 113
    :cond_13
    return-void
.end method

.method public a(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 129
    iget-boolean v2, p0, Lcom/google/android/location/h;->d:Z

    if-eqz v2, :cond_8

    move v0, v1

    .line 138
    :cond_7
    :goto_7
    return v0

    .line 131
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/location/h;->e:Z

    if-eqz v2, :cond_7

    .line 132
    iget-wide v2, p0, Lcom/google/android/location/h;->b:J

    sub-long v2, p1, v2

    .line 135
    iget-boolean v4, p0, Lcom/google/android/location/h;->c:Z

    if-eqz v4, :cond_1b

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_7

    :cond_1b
    move v0, v1

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GlsFailureTracker [currentOnCellNetwork="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/h;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentOnWifiNetwork="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/h;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentWifiNetworkId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/h;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inBackOffMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/h;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastGlsFailureMillisSinceBoot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/h;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", glsFailureTimestamps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
