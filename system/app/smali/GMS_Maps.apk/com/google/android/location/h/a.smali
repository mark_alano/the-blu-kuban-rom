.class public abstract Lcom/google/android/location/h/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/util/Observable;


# instance fields
.field protected final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    .line 26
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/common/util/Observer;)I
    .registers 6
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    .line 51
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljava/lang/ref/WeakReference;

    if-ne v2, v3, :cond_1e

    .line 53
    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 55
    :cond_1e
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 59
    :goto_24
    return v1

    .line 50
    :cond_25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 59
    :cond_29
    const/4 v1, -0x1

    goto :goto_24
.end method

.method public addObserver(Lcom/google/googlenav/common/util/Observer;)V
    .registers 5
    .parameter

    .prologue
    .line 37
    iget-object v1, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    monitor-enter v1

    .line 38
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/google/android/location/h/a;->a(Lcom/google/googlenav/common/util/Observer;)I

    move-result v0

    .line 39
    const/4 v2, -0x1

    if-ne v0, v2, :cond_11

    .line 40
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 46
    :goto_f
    monitor-exit v1

    .line 47
    return-void

    .line 44
    :cond_11
    iget-object v2, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v2, p1, v0}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_f

    .line 46
    :catchall_17
    move-exception v0

    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    throw v0
.end method

.method public getObservers()[Lcom/google/googlenav/common/util/Observer;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v4, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    monitor-enter v4

    .line 69
    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/googlenav/common/util/Observer;

    .line 71
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_15
    if-ltz v3, :cond_49

    .line 72
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Ljava/lang/ref/WeakReference;

    if-ne v5, v6, :cond_41

    .line 74
    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_39

    .line 76
    add-int v5, v3, v1

    check-cast v0, Lcom/google/googlenav/common/util/Observer;

    aput-object v0, v2, v5

    move v0, v1

    .line 71
    :goto_34
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v1, v0

    goto :goto_15

    .line 78
    :cond_39
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->removeElementAt(I)V

    .line 79
    add-int/lit8 v0, v1, 0x1

    goto :goto_34

    .line 82
    :cond_41
    add-int v5, v3, v1

    check-cast v0, Lcom/google/googlenav/common/util/Observer;

    aput-object v0, v2, v5

    move v0, v1

    goto :goto_34

    .line 86
    :cond_49
    if-lez v1, :cond_59

    .line 87
    array-length v0, v2

    sub-int/2addr v0, v1

    new-array v0, v0, [Lcom/google/googlenav/common/util/Observer;

    .line 88
    const/4 v3, 0x0

    array-length v5, v0

    invoke-static {v2, v1, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    :goto_54
    monitor-exit v4

    .line 93
    return-object v0

    .line 91
    :catchall_56
    move-exception v0

    monitor-exit v4
    :try_end_58
    .catchall {:try_start_4 .. :try_end_58} :catchall_56

    throw v0

    :cond_59
    move-object v0, v2

    goto :goto_54
.end method

.method public notifyObservers()V
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/a;->notifyObservers(Ljava/lang/Object;)V

    .line 123
    return-void
.end method

.method public notifyObservers(Ljava/lang/Object;)V
    .registers 5
    .parameter

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/location/h/a;->getObservers()[Lcom/google/googlenav/common/util/Observer;

    move-result-object v1

    .line 128
    const/4 v0, 0x0

    :goto_5
    array-length v2, v1

    if-ge v0, v2, :cond_10

    .line 129
    aget-object v2, v1, v0

    invoke-interface {v2, p0, p1}, Lcom/google/googlenav/common/util/Observer;->update(Lcom/google/googlenav/common/util/Observable;Ljava/lang/Object;)V

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 131
    :cond_10
    return-void
.end method

.method public removeAllObservers()V
    .registers 3

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    monitor-enter v1

    .line 116
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 117
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public removeObserver(Lcom/google/googlenav/common/util/Observer;)Z
    .registers 7
    .parameter

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    monitor-enter v2

    .line 99
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_c
    if-ltz v1, :cond_34

    .line 100
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Ljava/lang/ref/WeakReference;

    if-ne v3, v4, :cond_22

    .line 102
    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 104
    :cond_22
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 105
    iget-object v0, p0, Lcom/google/android/location/h/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    .line 106
    const/4 v0, 0x1

    monitor-exit v2

    .line 110
    :goto_2f
    return v0

    .line 99
    :cond_30
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_c

    .line 109
    :cond_34
    monitor-exit v2

    .line 110
    const/4 v0, 0x0

    goto :goto_2f

    .line 109
    :catchall_37
    move-exception v0

    monitor-exit v2
    :try_end_39
    .catchall {:try_start_3 .. :try_end_39} :catchall_37

    throw v0
.end method
