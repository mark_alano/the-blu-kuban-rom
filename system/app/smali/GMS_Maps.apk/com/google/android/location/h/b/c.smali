.class public Lcom/google/android/location/h/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# instance fields
.field private a:Lcom/google/android/location/h/f;

.field private b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/google/android/location/h/f;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-lez p2, :cond_2c

    .line 35
    iput p2, p0, Lcom/google/android/location/h/b/c;->b:I

    .line 42
    :goto_7
    if-eqz p1, :cond_5c

    .line 50
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 53
    :cond_d
    :goto_d
    if-lez v0, :cond_51

    .line 54
    add-int/lit8 v1, v0, -0x1

    .line 55
    const/16 v0, 0x2c

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    .line 57
    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 59
    const-string v2, "g"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 60
    invoke-static {p3}, Lcom/google/googlenav/common/io/e;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p3

    goto :goto_d

    .line 37
    :cond_2c
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/h/b/c;->b:I

    goto :goto_7

    .line 61
    :cond_30
    const-string v2, "n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unrecognised encoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_51
    if-lez p2, :cond_5d

    .line 70
    new-instance v0, Lcom/google/android/location/h/f;

    iget v1, p0, Lcom/google/android/location/h/b/c;->b:I

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/c;->a:Lcom/google/android/location/h/f;

    .line 75
    :cond_5c
    :goto_5c
    return-void

    .line 72
    :cond_5d
    new-instance v0, Lcom/google/android/location/h/f;

    const v1, 0x7fffffff

    invoke-direct {v0, p3, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/c;->a:Lcom/google/android/location/h/f;

    goto :goto_5c
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 103
    return-void
.end method

.method public b_()I
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/location/h/b/c;->b:I

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/location/h/b/c;->a:Lcom/google/android/location/h/f;

    return-object v0
.end method
