.class public Lcom/google/android/location/h/b/d;
.super Lcom/google/android/location/h/b/m;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private f:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x2c

    .line 36
    invoke-direct {p0}, Lcom/google/android/location/h/b/m;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 44
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 46
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 48
    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 50
    invoke-virtual {v0, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 52
    const-string v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/d;->b:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Lcom/google/android/location/h/b/d;->a:Ljava/lang/String;

    .line 56
    return-void
.end method

.method private e(J)[B
    .registers 6
    .parameter

    .prologue
    .line 126
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 127
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 130
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 133
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 136
    iget-object v2, p0, Lcom/google/android/location/h/b/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v1, p1, p2}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 142
    iget-object v2, p0, Lcom/google/android/location/h/b/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 145
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 147
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private f()[B
    .registers 6

    .prologue
    .line 98
    monitor-enter p0

    .line 99
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/b/d;->f:[B

    .line 100
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_21

    .line 102
    if-nez v0, :cond_20

    .line 103
    invoke-static {}, Lcom/google/android/location/h/c/a;->a()Lcom/google/android/location/h/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/h/c/a;->c()J

    move-result-wide v1

    .line 104
    invoke-direct {p0, v1, v2}, Lcom/google/android/location/h/b/d;->e(J)[B

    move-result-object v0

    .line 106
    monitor-enter p0

    .line 107
    :try_start_13
    iget-object v3, p0, Lcom/google/android/location/h/b/d;->f:[B

    if-nez v3, :cond_24

    .line 109
    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1f

    .line 110
    iput-object v0, p0, Lcom/google/android/location/h/b/d;->f:[B

    .line 116
    :cond_1f
    :goto_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_13 .. :try_end_20} :catchall_27

    .line 119
    :cond_20
    return-object v0

    .line 100
    :catchall_21
    move-exception v0

    :try_start_22
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    throw v0

    .line 114
    :cond_24
    :try_start_24
    iget-object v0, p0, Lcom/google/android/location/h/b/d;->f:[B

    goto :goto_1f

    .line 116
    :catchall_27
    move-exception v0

    monitor-exit p0
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_27

    throw v0
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 72
    return-void
.end method

.method public b_()I
    .registers 2

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/location/h/b/d;->f()[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 89
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {p0}, Lcom/google/android/location/h/b/d;->f()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method
