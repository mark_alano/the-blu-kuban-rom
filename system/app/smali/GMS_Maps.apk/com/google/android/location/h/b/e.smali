.class public Lcom/google/android/location/h/b/e;
.super Lcom/google/android/location/h/b/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/location/h/g;

.field private b:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private i:[B

.field private j:[B

.field private k:[B


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/location/h/b/o;-><init>()V

    .line 28
    const-string v0, "GET"

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->b:Ljava/lang/String;

    .line 39
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/b/e;->c(I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/o;-><init>(Ljava/lang/String;I)V

    .line 28
    const-string v0, "GET"

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->b:Ljava/lang/String;

    .line 39
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/b/e;->c(I)V

    .line 73
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 115
    if-eqz p0, :cond_c

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private x()V
    .registers 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v0, :cond_b

    .line 209
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->k:[B

    if-nez v0, :cond_b

    .line 210
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->h()[B

    .line 213
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->j:[B

    if-nez v0, :cond_12

    .line 214
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->g()[B

    .line 216
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B

    if-nez v0, :cond_19

    .line 217
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->f()[B

    .line 219
    :cond_19
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 3

    .prologue
    .line 312
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->j:[B

    .line 314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->k:[B

    .line 316
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v0, :cond_27

    .line 317
    const/4 v0, 0x0

    move v1, v0

    :goto_10
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_27

    .line 318
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->a()V
    :try_end_23
    .catchall {:try_start_2 .. :try_end_23} :catchall_29

    .line 317
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 321
    :cond_27
    monitor-exit p0

    return-void

    .line 312
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 3
    .parameter

    .prologue
    .line 128
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/location/h/b/o;->a(I)V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 133
    monitor-exit p0

    return-void

    .line 128
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 108
    monitor-enter p0

    :try_start_1
    invoke-static {p1}, Lcom/google/android/location/h/b/e;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 109
    invoke-super {p0}, Lcom/google/android/location/h/b/o;->e()V

    .line 111
    :cond_a
    invoke-super {p0, p1}, Lcom/google/android/location/h/b/o;->a(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 112
    monitor-exit p0

    return-void

    .line 108
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 145
    monitor-enter p0

    :try_start_1
    const-string v0, "Content-Type"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 146
    iput-object p2, p0, Lcom/google/android/location/h/b/e;->f:Ljava/lang/String;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_26

    .line 153
    :goto_13
    monitor-exit p0

    return-void

    .line 148
    :cond_15
    :try_start_15
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->g:Ljava/util/Hashtable;

    if-nez v0, :cond_20

    .line 149
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->g:Ljava/util/Hashtable;

    .line 151
    :cond_20
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->g:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_15 .. :try_end_25} :catchall_26

    goto :goto_13

    .line 145
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([B)V
    .registers 3
    .parameter

    .prologue
    .line 185
    monitor-enter p0

    :try_start_1
    new-instance v0, Lcom/google/android/location/h/d;

    invoke-direct {v0, p1}, Lcom/google/android/location/h/d;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    .line 186
    monitor-exit p0

    return-void

    .line 185
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 365
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/h/b/e;->x()V

    .line 372
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 374
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/h/b/e;->i:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 375
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/h/b/e;->j:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_2a

    .line 378
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 380
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v0, :cond_55

    .line 386
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/h/b/e;->k:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 387
    const/4 v0, 0x0

    move v1, v0

    :goto_3a
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_55

    .line 388
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->c_()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3a

    .line 392
    :cond_55
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Ljava/io/InputStream;

    .line 393
    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 395
    new-instance v1, Lcom/google/googlenav/common/io/SequenceInputStream;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>([Ljava/io/InputStream;)V
    :try_end_63
    .catchall {:try_start_1 .. :try_end_63} :catchall_65

    monitor-exit p0

    return-object v1

    .line 365
    :catchall_65
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 329
    monitor-enter p0

    .line 331
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/location/h/b/e;->x()V

    .line 338
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B

    array-length v0, v0

    add-int/2addr v0, v1

    .line 339
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->j:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 340
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    if-eqz v2, :cond_18

    .line 341
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    add-int/2addr v0, v2

    .line 343
    :cond_18
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v2, :cond_3c

    .line 349
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->k:[B

    array-length v2, v2

    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 350
    :goto_22
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_3b

    .line 351
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->b_()I
    :try_end_35
    .catchall {:try_start_2 .. :try_end_35} :catchall_3e

    move-result v0

    add-int/2addr v1, v0

    .line 350
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_22

    :cond_3b
    move v0, v1

    .line 355
    :cond_3c
    monitor-exit p0

    return v0

    .line 329
    :catchall_3e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 163
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/b/e;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 164
    monitor-exit p0

    return-void

    .line 163
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .registers 3

    .prologue
    .line 119
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The secure flag is set  based on the service uri"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected f()[B
    .registers 5

    .prologue
    .line 230
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 231
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->d()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/location/h/b/e;->w()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 237
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-nez v0, :cond_34

    .line 238
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->j:[B

    array-length v0, v0

    .line 239
    iget-object v3, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    if-eqz v3, :cond_31

    .line 240
    iget-object v3, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    invoke-interface {v3}, Lcom/google/android/location/h/g;->b_()I

    move-result v3

    add-int/2addr v0, v3

    .line 242
    :cond_31
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 245
    :cond_34
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 246
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 248
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B

    .line 249
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->i:[B

    return-object v0
.end method

.method protected g()[B
    .registers 5

    .prologue
    .line 259
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 260
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v0, :cond_21

    .line 265
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->f:Ljava/lang/String;

    if-eqz v0, :cond_5d

    iget-object v0, p0, Lcom/google/android/location/h/b/e;->f:Ljava/lang/String;

    :goto_19
    invoke-static {v2, v0}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 266
    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 268
    :cond_21
    iget-object v3, p0, Lcom/google/android/location/h/b/e;->g:Ljava/util/Hashtable;

    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-nez v0, :cond_60

    iget-object v0, p0, Lcom/google/android/location/h/b/e;->f:Ljava/lang/String;

    :goto_29
    invoke-static {v2, v3, v0}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_62

    .line 270
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 274
    :goto_39
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    if-eqz v0, :cond_4e

    .line 275
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/location/h/b/e;->a:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    if-lez v0, :cond_4e

    .line 276
    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 280
    :cond_4e
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 281
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 283
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->j:[B

    .line 284
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->j:[B

    return-object v0

    .line 265
    :cond_5d
    const-string v0, "multipart/related"

    goto :goto_19

    .line 268
    :cond_60
    const/4 v0, 0x0

    goto :goto_29

    .line 272
    :cond_62
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_39
.end method

.method protected h()[B
    .registers 4

    .prologue
    .line 296
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 297
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 299
    iget-object v2, p0, Lcom/google/android/location/h/b/e;->h:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 301
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 302
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 304
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/e;->k:[B

    .line 305
    iget-object v0, p0, Lcom/google/android/location/h/b/e;->k:[B

    return-object v0
.end method
