.class public Lcom/google/android/location/h/b/j;
.super Lcom/google/android/location/h/b/o;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/location/h/g;

.field b:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/o;-><init>(Ljava/lang/String;I)V

    .line 27
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/b/j;->c(I)V

    .line 47
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/location/h/b/j;->b:[B

    if-nez v0, :cond_7

    .line 125
    invoke-direct {p0}, Lcom/google/android/location/h/b/j;->g()V

    .line 127
    :cond_7
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 136
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 137
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/location/h/b/j;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/location/h/b/j;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/location/h/b/j;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 143
    iget-object v2, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    if-eqz v2, :cond_39

    .line 144
    iget-object v2, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 149
    :goto_2c
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 150
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 152
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/j;->b:[B

    .line 153
    return-void

    .line 146
    :cond_39
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_2c
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Lcom/google/android/location/h/b/o;->a()V

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b/j;->b:[B
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 162
    monitor-exit p0

    return-void

    .line 159
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 3
    .parameter

    .prologue
    .line 88
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/location/h/b/o;->a(I)V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b/j;->b:[B
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 93
    monitor-exit p0

    return-void

    .line 88
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized b()Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 187
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/h/b/j;->f()V

    .line 189
    iget-object v0, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    if-nez v0, :cond_11

    .line 190
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/h/b/j;->b:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_24

    .line 192
    :goto_f
    monitor-exit p0

    return-object v0

    :cond_11
    :try_start_11
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/b/j;->b:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_23
    .catchall {:try_start_11 .. :try_end_23} :catchall_24

    goto :goto_f

    .line 187
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized c()I
    .registers 3

    .prologue
    .line 170
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/h/b/j;->f()V

    .line 172
    iget-object v0, p0, Lcom/google/android/location/h/b/j;->b:[B

    array-length v0, v0

    .line 174
    iget-object v1, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    if-eqz v1, :cond_12

    .line 175
    iget-object v1, p0, Lcom/google/android/location/h/b/j;->a:Lcom/google/android/location/h/g;

    invoke-interface {v1}, Lcom/google/android/location/h/g;->b_()I
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_14

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_12
    monitor-exit p0

    return v0

    .line 170
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method
