.class public Lcom/google/android/location/h/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/io/DataInputStream;

.field private b:Lcom/google/android/location/h/f;


# direct methods
.method public constructor <init>(Ljava/io/DataInputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    .line 39
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 41
    const/4 v1, 0x2

    if-eq v0, v1, :cond_14

    .line 42
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unrecognised protocol version"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_14
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    if-eqz v0, :cond_7

    .line 58
    iput-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    .line 61
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 62
    iput-object v1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    .line 63
    return-void
.end method

.method public b()Lcom/google/android/location/h/b/n;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 76
    .line 78
    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    if-eqz v1, :cond_c

    .line 79
    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-virtual {v1}, Lcom/google/android/location/h/f;->a()V

    .line 80
    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    .line 83
    :cond_c
    iget-object v1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 84
    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v2

    .line 86
    const v3, 0x8100

    if-ne v2, v3, :cond_2e

    .line 87
    new-instance v0, Lcom/google/android/location/h/f;

    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    .line 88
    new-instance v0, Lcom/google/android/location/h/b/k;

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-direct {v0, v1}, Lcom/google/android/location/h/b/k;-><init>(Lcom/google/android/location/h/f;)V

    .line 97
    :goto_2d
    return-object v0

    .line 89
    :cond_2e
    const v3, 0x8101

    if-ne v2, v3, :cond_44

    .line 90
    new-instance v0, Lcom/google/android/location/h/f;

    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    .line 91
    new-instance v0, Lcom/google/android/location/h/b/h;

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-direct {v0, v1}, Lcom/google/android/location/h/b/h;-><init>(Lcom/google/android/location/h/f;)V

    goto :goto_2d

    .line 94
    :cond_44
    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->skipBytes(I)I

    goto :goto_2d
.end method
