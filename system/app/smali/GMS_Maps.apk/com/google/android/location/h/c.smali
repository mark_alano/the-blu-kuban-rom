.class Lcom/google/android/location/h/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:[B

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(I)V
    .registers 5
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/h/c;->c:I

    .line 33
    if-gtz p1, :cond_28

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bufferSize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_28
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/location/h/c;->b:[B

    .line 37
    return-void
.end method


# virtual methods
.method public a()I
    .registers 5

    .prologue
    const/4 v0, -0x1

    .line 123
    iget-object v1, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :goto_4
    :try_start_4
    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    if-gez v2, :cond_16

    .line 126
    iget-boolean v2, p0, Lcom/google/android/location/h/c;->e:Z

    if-eqz v2, :cond_e

    .line 127
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_3f

    .line 146
    :goto_d
    return v0

    .line 130
    :cond_e
    :try_start_e
    iget-object v2, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_3f
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_13} :catch_14

    goto :goto_4

    .line 131
    :catch_14
    move-exception v2

    goto :goto_4

    .line 135
    :cond_16
    :try_start_16
    iget-object v0, p0, Lcom/google/android/location/h/c;->b:[B

    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/location/h/c;->c:I

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 136
    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    iget-object v3, p0, Lcom/google/android/location/h/c;->b:[B

    array-length v3, v3

    if-ne v2, v3, :cond_2c

    .line 137
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/location/h/c;->c:I

    .line 140
    :cond_2c
    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    iget v3, p0, Lcom/google/android/location/h/c;->d:I

    if-ne v2, v3, :cond_38

    .line 142
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/location/h/c;->c:I

    .line 143
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/location/h/c;->d:I

    .line 145
    :cond_38
    iget-object v2, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 146
    monitor-exit v1

    goto :goto_d

    .line 147
    :catchall_3f
    move-exception v0

    monitor-exit v1
    :try_end_41
    .catchall {:try_start_16 .. :try_end_41} :catchall_3f

    throw v0
.end method

.method public a([BII)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 160
    if-nez p3, :cond_5

    .line 210
    :goto_4
    return v0

    .line 163
    :cond_5
    iget-object v3, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 164
    :goto_8
    :try_start_8
    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    if-gez v2, :cond_1b

    .line 166
    iget-boolean v2, p0, Lcom/google/android/location/h/c;->e:Z

    if-eqz v2, :cond_13

    .line 167
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_56

    move v0, v1

    goto :goto_4

    .line 170
    :cond_13
    :try_start_13
    iget-object v2, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_18
    .catchall {:try_start_13 .. :try_end_18} :catchall_56
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_18} :catch_19

    goto :goto_8

    .line 171
    :catch_19
    move-exception v2

    goto :goto_8

    .line 178
    :cond_1b
    :try_start_1b
    iget v1, p0, Lcom/google/android/location/h/c;->c:I

    iget v2, p0, Lcom/google/android/location/h/c;->d:I

    if-ge v1, v2, :cond_59

    .line 179
    iget v1, p0, Lcom/google/android/location/h/c;->d:I

    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    sub-int/2addr v1, v2

    move v2, v0

    move v0, p3

    .line 193
    :goto_28
    if-le v1, v0, :cond_2b

    move v1, v0

    .line 196
    :cond_2b
    iget-object v0, p0, Lcom/google/android/location/h/c;->b:[B

    iget v4, p0, Lcom/google/android/location/h/c;->c:I

    invoke-static {v0, v4, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    add-int v0, v2, v1

    .line 198
    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/location/h/c;->c:I

    .line 199
    iget v1, p0, Lcom/google/android/location/h/c;->c:I

    iget-object v2, p0, Lcom/google/android/location/h/c;->b:[B

    array-length v2, v2

    if-ne v1, v2, :cond_43

    .line 200
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/location/h/c;->c:I

    .line 203
    :cond_43
    iget v1, p0, Lcom/google/android/location/h/c;->c:I

    iget v2, p0, Lcom/google/android/location/h/c;->d:I

    if-ne v1, v2, :cond_4f

    .line 205
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/location/h/c;->c:I

    .line 206
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/location/h/c;->d:I

    .line 209
    :cond_4f
    iget-object v1, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 210
    monitor-exit v3

    goto :goto_4

    .line 211
    :catchall_56
    move-exception v0

    monitor-exit v3
    :try_end_58
    .catchall {:try_start_1b .. :try_end_58} :catchall_56

    throw v0

    .line 181
    :cond_59
    :try_start_59
    iget-object v1, p0, Lcom/google/android/location/h/c;->b:[B

    array-length v1, v1

    iget v2, p0, Lcom/google/android/location/h/c;->c:I

    sub-int v2, v1, v2

    .line 182
    if-ge v2, p3, :cond_72

    .line 184
    iget-object v0, p0, Lcom/google/android/location/h/c;->b:[B

    iget v1, p0, Lcom/google/android/location/h/c;->c:I

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 187
    add-int/2addr p2, v2

    .line 188
    sub-int/2addr p3, v2

    .line 189
    iget v1, p0, Lcom/google/android/location/h/c;->d:I

    .line 190
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/h/c;->c:I
    :try_end_70
    .catchall {:try_start_59 .. :try_end_70} :catchall_56

    move v0, p3

    goto :goto_28

    :cond_72
    move v1, v2

    move v2, v0

    move v0, p3

    goto :goto_28
.end method

.method public a([BI)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 75
    .line 76
    iget-object v3, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    monitor-enter v3

    move v2, v0

    move v1, p2

    .line 77
    :cond_6
    :goto_6
    if-lez v1, :cond_53

    .line 80
    :goto_8
    :try_start_8
    iget v0, p0, Lcom/google/android/location/h/c;->c:I

    iget v4, p0, Lcom/google/android/location/h/c;->d:I
    :try_end_c
    .catchall {:try_start_8 .. :try_end_c} :catchall_49

    if-ne v0, v4, :cond_1b

    .line 85
    :try_start_e
    iget-object v0, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 86
    iget-object v0, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_49
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_18} :catch_19

    goto :goto_8

    .line 87
    :catch_19
    move-exception v0

    goto :goto_8

    .line 91
    :cond_1b
    :try_start_1b
    iget v0, p0, Lcom/google/android/location/h/c;->c:I

    if-gez v0, :cond_22

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/h/c;->c:I

    .line 96
    :cond_22
    iget v0, p0, Lcom/google/android/location/h/c;->d:I

    iget v4, p0, Lcom/google/android/location/h/c;->c:I

    if-ge v0, v4, :cond_4c

    .line 97
    iget v0, p0, Lcom/google/android/location/h/c;->c:I

    iget v4, p0, Lcom/google/android/location/h/c;->d:I

    sub-int/2addr v0, v4

    .line 101
    :goto_2d
    if-le v0, v1, :cond_30

    move v0, v1

    .line 104
    :cond_30
    iget-object v4, p0, Lcom/google/android/location/h/c;->b:[B

    iget v5, p0, Lcom/google/android/location/h/c;->d:I

    invoke-static {p1, v2, v4, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    add-int/2addr v2, v0

    .line 106
    sub-int/2addr v1, v0

    .line 107
    iget v4, p0, Lcom/google/android/location/h/c;->d:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/google/android/location/h/c;->d:I

    .line 108
    iget v0, p0, Lcom/google/android/location/h/c;->d:I

    iget-object v4, p0, Lcom/google/android/location/h/c;->b:[B

    array-length v4, v4

    if-ne v0, v4, :cond_6

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/h/c;->d:I

    goto :goto_6

    .line 113
    :catchall_49
    move-exception v0

    monitor-exit v3
    :try_end_4b
    .catchall {:try_start_1b .. :try_end_4b} :catchall_49

    throw v0

    .line 99
    :cond_4c
    :try_start_4c
    iget-object v0, p0, Lcom/google/android/location/h/c;->b:[B

    array-length v0, v0

    iget v4, p0, Lcom/google/android/location/h/c;->d:I

    sub-int/2addr v0, v4

    goto :goto_2d

    .line 112
    :cond_53
    iget-object v0, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 113
    monitor-exit v3
    :try_end_59
    .catchall {:try_start_4c .. :try_end_59} :catchall_49

    .line 114
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 231
    iget-object v1, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 232
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/location/h/c;->e:Z

    .line 233
    iget-object v0, p0, Lcom/google/android/location/h/c;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 234
    monitor-exit v1

    .line 235
    return-void

    .line 234
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_d

    throw v0
.end method
