.class public Lcom/google/android/location/h/c/a;
.super Lcom/google/android/location/h/a;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/location/h/c/a;


# instance fields
.field private b:Z

.field private c:J

.field private d:Z


# direct methods
.method private constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/android/location/h/a;-><init>()V

    .line 77
    iput-boolean v2, p0, Lcom/google/android/location/h/c/a;->b:Z

    .line 78
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    .line 79
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "RequestNewInstallId"

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    .line 81
    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    if-eqz v0, :cond_21

    .line 82
    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    .line 84
    :cond_21
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/google/android/location/h/c/a;->c:J

    return-wide p1
.end method

.method public static declared-synchronized a()Lcom/google/android/location/h/c/a;
    .registers 2

    .prologue
    .line 92
    const-class v1, Lcom/google/android/location/h/c/a;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;

    if-nez v0, :cond_e

    .line 93
    new-instance v0, Lcom/google/android/location/h/c/a;

    invoke-direct {v0}, Lcom/google/android/location/h/c/a;-><init>()V

    sput-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;

    .line 95
    :cond_e
    sget-object v0, Lcom/google/android/location/h/c/a;->e:Lcom/google/android/location/h/c/a;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 92
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;)V
    .registers 1
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/location/h/c/a;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/c/a;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/location/h/c/a;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/h/c/a;)J
    .registers 3
    .parameter

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/location/h/c/a;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/location/h/c/a;->b:Z

    return p1
.end method

.method private d()V
    .registers 5

    .prologue
    .line 163
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "InstallId"

    iget-wide v2, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    .line 164
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "RequestNewInstallId"

    iget-boolean v2, p0, Lcom/google/android/location/h/c/a;->d:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Z)V

    .line 166
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 167
    return-void
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/google/googlenav/common/util/Observer;)V
    .registers 5
    .parameter

    .prologue
    .line 211
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/Long;

    iget-wide v1, p0, Lcom/google/android/location/h/c/a;->c:J

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    invoke-interface {p1, p0, v0}, Lcom/google/googlenav/common/util/Observer;->update(Lcom/google/googlenav/common/util/Observable;Ljava/lang/Object;)V

    .line 212
    invoke-super {p0, p1}, Lcom/google/android/location/h/a;->addObserver(Lcom/google/googlenav/common/util/Observer;)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 213
    monitor-exit p0

    return-void

    .line 211
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .registers 5

    .prologue
    .line 104
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->b:Z

    if-nez v0, :cond_2c

    .line 105
    invoke-static {}, Lcom/google/android/location/h/h;->g()Lcom/google/android/location/h/h;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_2c

    .line 108
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/h/c/a;->b:Z

    .line 109
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/h/c/a;->d:Z

    .line 110
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/location/h/c/a;->c:J

    .line 111
    invoke-direct {p0}, Lcom/google/android/location/h/c/a;->d()V

    .line 114
    new-instance v1, Lcom/google/android/location/h/b/j;

    const-string v2, "g:c"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/h/b/j;-><init>(Ljava/lang/String;I)V

    .line 115
    new-instance v2, Lcom/google/android/location/h/c/a$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/h/c/a$1;-><init>(Lcom/google/android/location/h/c/a;)V

    invoke-virtual {v1, v2}, Lcom/google/android/location/h/b/m;->a(Lcom/google/android/location/h/b/m$a;)V

    .line 154
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2e

    .line 157
    :cond_2c
    monitor-exit p0

    return-void

    .line 104
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()J
    .registers 7

    .prologue
    const-wide/16 v4, 0x0

    .line 184
    monitor-enter p0

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/h/c/a;->d:Z

    if-eqz v0, :cond_e

    .line 185
    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    .line 186
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_32

    .line 198
    :goto_c
    monitor-exit p0

    return-wide v0

    .line 189
    :cond_e
    :try_start_e
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_26

    .line 190
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "InstallId"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    .line 194
    :cond_26
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2f

    .line 195
    invoke-virtual {p0}, Lcom/google/android/location/h/c/a;->b()V

    .line 198
    :cond_2f
    iget-wide v0, p0, Lcom/google/android/location/h/c/a;->c:J
    :try_end_31
    .catchall {:try_start_e .. :try_end_31} :catchall_32

    goto :goto_c

    .line 184
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0
.end method
