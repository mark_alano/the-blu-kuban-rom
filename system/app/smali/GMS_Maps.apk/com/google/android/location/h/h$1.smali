.class Lcom/google/android/location/h/h$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/h/h;-><init>(Lcom/google/android/location/h/h$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/h/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/h/h;)V
    .registers 2
    .parameter

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 260
    :try_start_7
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    iget-wide v4, v0, Lcom/google/android/location/h/h;->i:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_2f

    .line 261
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    iget-object v0, v0, Lcom/google/android/location/h/h;->j:Las/d;

    iget-object v2, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    iget-wide v2, v2, Lcom/google/android/location/h/h;->i:J

    invoke-virtual {v0, v2, v3}, Las/d;->b(J)V

    .line 262
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    iget-object v0, v0, Lcom/google/android/location/h/h;->j:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    .line 263
    monitor-exit v1

    .line 271
    :goto_2e
    return-void

    .line 266
    :cond_2f
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/location/h/h;->i:J

    .line 267
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/location/h/h;->h:J

    .line 268
    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_7 .. :try_end_3c} :catchall_42

    .line 270
    iget-object v0, p0, Lcom/google/android/location/h/h$1;->a:Lcom/google/android/location/h/h;

    invoke-virtual {v0}, Lcom/google/android/location/h/h;->j()V

    goto :goto_2e

    .line 268
    :catchall_42
    move-exception v0

    :try_start_43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_42

    throw v0
.end method
