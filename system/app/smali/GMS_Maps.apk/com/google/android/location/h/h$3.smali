.class Lcom/google/android/location/h/h$3;
.super Las/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/h/a/b;

.field final synthetic b:[Lcom/google/android/location/h/b/m;

.field final synthetic f:I

.field final synthetic g:Lcom/google/android/location/h/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/h/h;Las/c;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 943
    iput-object p1, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iput-object p3, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    iput-object p4, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    iput p5, p0, Lcom/google/android/location/h/h$3;->f:I

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 6

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->f()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 947
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 948
    const/4 v0, 0x0

    :goto_10
    :try_start_10
    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    array-length v2, v2

    if-ge v0, v2, :cond_25

    .line 949
    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v3}, Lcom/google/android/location/h/a/b;->k()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/location/h/b/m;->d(J)V

    .line 948
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 951
    :cond_25
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    .line 952
    monitor-exit v1

    .line 971
    :cond_2b
    :goto_2b
    return-void

    .line 952
    :catchall_2c
    move-exception v0

    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_10 .. :try_end_2e} :catchall_2c

    throw v0

    .line 953
    :cond_2f
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 955
    :try_start_37
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 956
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    iget-object v2, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v2}, Lcom/google/android/location/h/a/b;->c()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;[Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    .line 961
    :goto_4c
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    invoke-static {v0, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;[Ljava/lang/Object;)V

    .line 962
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget v1, p0, Lcom/google/android/location/h/h$3;->f:I

    invoke-static {v0, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;I)I
    :try_end_5a
    .catchall {:try_start_37 .. :try_end_5a} :catchall_6f

    .line 967
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    .line 968
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->b()V

    goto :goto_2b

    .line 958
    :cond_65
    :try_start_65
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    invoke-static {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    :try_end_6e
    .catchall {:try_start_65 .. :try_end_6e} :catchall_6f

    goto :goto_4c

    .line 967
    :catchall_6f
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v1}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    .line 968
    iget-object v1, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v1}, Lcom/google/android/location/h/a/b;->b()V

    throw v0
.end method
