.class public Lcom/google/android/location/h/h$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/h/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 1199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1197
    const-wide/16 v0, 0x4e20

    iput-wide v0, p0, Lcom/google/android/location/h/h$a;->g:J

    .line 1201
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h$a;)J
    .registers 3
    .parameter

    .prologue
    .line 1190
    iget-wide v0, p0, Lcom/google/android/location/h/h$a;->g:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/h/h$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/location/h/h$a;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1204
    iput-object p1, p0, Lcom/google/android/location/h/h$a;->a:Ljava/lang/String;

    .line 1205
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1212
    iput-object p1, p0, Lcom/google/android/location/h/h$a;->c:Ljava/lang/String;

    .line 1213
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1216
    iput-object p1, p0, Lcom/google/android/location/h/h$a;->d:Ljava/lang/String;

    .line 1217
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1220
    iput-object p1, p0, Lcom/google/android/location/h/h$a;->e:Ljava/lang/String;

    .line 1221
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1224
    iput-object p1, p0, Lcom/google/android/location/h/h$a;->f:Ljava/lang/String;

    .line 1225
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Configuration[serverUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "secureServerUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "applicationName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "applicationVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "platformId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "distributionChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/h/h$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
