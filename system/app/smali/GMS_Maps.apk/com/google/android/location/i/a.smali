.class public Lcom/google/android/location/i/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/i/b;

.field final b:Lcom/google/android/location/i/c;

.field final c:Lcom/google/android/location/i/c;

.field final d:Lcom/google/android/location/i/c;

.field final e:Lcom/google/android/location/i/c;

.field final f:Lcom/google/android/location/os/i;

.field volatile g:Z


# direct methods
.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/b;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z

    .line 46
    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 47
    iput-object p1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    .line 48
    iput-object p2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    .line 49
    invoke-virtual {p2, v0, v1}, Lcom/google/android/location/i/b;->a(J)V

    .line 53
    invoke-virtual {p2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V

    .line 54
    invoke-virtual {p2}, Lcom/google/android/location/i/b;->a()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    .line 55
    invoke-virtual {p2}, Lcom/google/android/location/i/b;->b()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    .line 56
    invoke-virtual {p2}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    .line 57
    invoke-virtual {p2}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/location/i/b;

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/i/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;JJ)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/i/a;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/b;)V

    .line 42
    return-void
.end method

.method private a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-object v0, p1

    move-wide v1, p2

    move v5, p4

    .line 160
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->b(JJZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_14

    .line 166
    iget-object v1, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/location/i/b;->b(J)V

    .line 168
    :cond_14
    return-object v0
.end method


# virtual methods
.method public a()J
    .registers 4

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/c;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized a(J)V
    .registers 6
    .parameter

    .prologue
    .line 92
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_18

    if-nez v0, :cond_7

    .line 104
    :goto_5
    monitor-exit p0

    return-void

    .line 97
    :cond_7
    :try_start_7
    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 98
    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/location/i/c;->a(JJ)Z

    .line 103
    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_18

    goto :goto_5

    .line 92
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/location/i/c$a;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 178
    if-nez p1, :cond_3

    .line 187
    :cond_2
    :goto_2
    return-void

    .line 181
    :cond_3
    invoke-virtual {p1, p2, p3}, Lcom/google/android/location/i/c$a;->a(J)J

    move-result-wide v0

    .line 184
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/b;->b(J)V

    goto :goto_2
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 6
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 238
    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_13

    .line 239
    iget-object v2, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->k()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    .line 242
    :cond_13
    iget-object v2, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_20

    .line 243
    iget-object v2, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->l()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    .line 246
    :cond_20
    iget-object v2, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_2d

    .line 247
    iget-object v2, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->m()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    .line 251
    :cond_2d
    iget-object v2, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    if-eqz v2, :cond_3a

    .line 252
    iget-object v2, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    invoke-virtual {p1}, Lcom/google/android/location/os/h;->n()Lcom/google/android/location/os/h$a;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/os/h$a;J)V

    .line 255
    :cond_3a
    return-void
.end method

.method public a(ZZI)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 232
    iput-boolean p2, p0, Lcom/google/android/location/i/a;->g:Z

    .line 233
    return-void
.end method

.method public declared-synchronized a(JZ)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 74
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/i/a;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_17

    if-nez v0, :cond_8

    .line 77
    const/4 v0, 0x1

    .line 83
    :goto_6
    monitor-exit p0

    return v0

    .line 79
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z
    :try_end_15
    .catchall {:try_start_8 .. :try_end_15} :catchall_17

    move-result v0

    goto :goto_6

    .line 74
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(JZ)Lcom/google/android/location/i/c$a;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/location/i/a;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 264
    iget-object v0, p0, Lcom/google/android/location/i/a;->c:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 265
    iget-object v0, p0, Lcom/google/android/location/i/a;->d:Lcom/google/android/location/i/c;

    invoke-virtual {v0}, Lcom/google/android/location/i/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 268
    if-nez v1, :cond_1a

    if-nez v2, :cond_1a

    if-nez v3, :cond_1a

    .line 269
    const/4 v0, 0x0

    .line 283
    :cond_19
    :goto_19
    return-object v0

    .line 271
    :cond_1a
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 272
    if-eqz v1, :cond_27

    .line 273
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 275
    :cond_27
    if-eqz v2, :cond_2d

    .line 276
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 278
    :cond_2d
    if-eqz v3, :cond_19

    .line 279
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_19
.end method

.method public b(J)V
    .registers 6
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 203
    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/location/i/c;->a(JJ)Z

    .line 206
    iget-object v2, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/b;->b(J)V

    .line 207
    return-void
.end method

.method public c(JZ)Lcom/google/android/location/i/c$a;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/location/i/a;->e:Lcom/google/android/location/i/c;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c;JZ)Lcom/google/android/location/i/c$a;

    move-result-object v0

    return-object v0
.end method

.method public d(JZ)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->c()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v0

    return v0
.end method

.method public e(JZ)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/location/i/a;->a:Lcom/google/android/location/i/b;

    invoke-virtual {v0}, Lcom/google/android/location/i/b;->d()Lcom/google/android/location/i/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/i/a;->f:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-wide v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v0

    return v0
.end method
