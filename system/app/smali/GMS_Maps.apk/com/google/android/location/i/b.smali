.class Lcom/google/android/location/i/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i/b$a;
    }
.end annotation


# instance fields
.field final a:J

.field private final b:Lcom/google/android/location/i/c;

.field private final c:Lcom/google/android/location/i/c;

.field private final d:Lcom/google/android/location/i/c;

.field private final e:Lcom/google/android/location/i/c;

.field private final f:Lcom/google/android/location/os/i;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;JJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    .line 91
    iput-wide p6, p0, Lcom/google/android/location/i/b;->a:J

    .line 92
    iput-object p2, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    .line 93
    iput-object p3, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    .line 94
    iput-object p4, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    .line 95
    iput-object p5, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    .line 96
    invoke-direct {p0, p8, p9}, Lcom/google/android/location/i/b;->c(J)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;JJ)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/location/i/c;

    const-string v1, "bandwidth"

    const-wide/16 v2, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->k()Lcom/google/android/location/os/h$a;

    move-result-object v4

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v1, Lcom/google/android/location/i/c;

    const-string v2, "general-gps"

    const-wide/16 v3, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->l()Lcom/google/android/location/os/h$a;

    move-result-object v5

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v2, Lcom/google/android/location/i/c;

    const-string v3, "sensor-gps"

    const-wide/16 v4, -0x1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->m()Lcom/google/android/location/os/h$a;

    move-result-object v6

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    invoke-direct/range {v2 .. v10}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    new-instance v3, Lcom/google/android/location/i/c;

    const-string v4, "burst-gps"

    const-wide/16 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/os/h;->n()Lcom/google/android/location/os/h$a;

    move-result-object v7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    invoke-direct/range {v3 .. v11}, Lcom/google/android/location/i/c;-><init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V

    move-object v4, p0

    move-object v5, p1

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    invoke-direct/range {v4 .. v13}, Lcom/google/android/location/i/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;Lcom/google/android/location/i/c;JJ)V

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    return-object v0
.end method

.method private declared-synchronized a(JLjava/io/ByteArrayOutputStream;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 228
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 229
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 230
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 231
    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/location/i/b;->a:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 232
    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 234
    iget-object v2, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    const/4 v3, 0x3

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 236
    iget-object v2, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    const/4 v3, 0x4

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 238
    iget-object v2, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    const/4 v3, 0x5

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 240
    iget-object v2, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    const/4 v3, 0x6

    invoke-static {v2, v1, v3}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 242
    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 243
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_3d
    .catchall {:try_start_1 .. :try_end_3d} :catchall_3f

    .line 244
    monitor-exit p0

    return-void

    .line 228
    :catchall_3f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/i/b;JLjava/io/ByteArrayOutputStream;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/i/b;->a(JLjava/io/ByteArrayOutputStream;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    return-void
.end method

.method private static a(Lcom/google/android/location/i/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 248
    invoke-virtual {p0, v0}, Lcom/google/android/location/i/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 249
    invoke-virtual {p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 250
    return-void
.end method

.method private a(Ljava/io/Closeable;)V
    .registers 3
    .parameter

    .prologue
    .line 122
    if-eqz p1, :cond_5

    .line 124
    :try_start_2
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 129
    :cond_5
    :goto_5
    return-void

    .line 125
    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method private declared-synchronized c(J)V
    .registers 4
    .parameter

    .prologue
    .line 220
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    .line 221
    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    .line 222
    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V

    .line 223
    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 224
    monitor-exit p0

    return-void

    .line 220
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/android/location/i/c;
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method a(J)V
    .registers 9
    .parameter

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 108
    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->e()Ljava/io/File;

    move-result-object v3

    const-string v4, "cp_state"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_23
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_13} :catch_1a

    .line 110
    :try_start_13
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/location/i/b;->a(Ljava/io/InputStream;J)V
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_28
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_16} :catch_2d

    .line 114
    invoke-direct {p0, v0}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    .line 116
    :goto_19
    return-void

    .line 111
    :catch_1a
    move-exception v0

    move-object v0, v1

    .line 112
    :goto_1c
    :try_start_1c
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_28

    .line 114
    invoke-direct {p0, v0}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_19

    :catchall_23
    move-exception v0

    :goto_24
    invoke-direct {p0, v1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_28
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_24

    .line 111
    :catch_2d
    move-exception v1

    goto :goto_1c
.end method

.method a(Ljava/io/InputStream;J)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 134
    :try_start_1
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 135
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    .line 136
    if-eq v1, v2, :cond_13

    .line 139
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_73
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_f} :catch_63
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_f} :catch_6b

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    .line 174
    :goto_12
    return-void

    .line 142
    :cond_13
    :try_start_13
    sget-object v1, Lcom/google/android/location/j/a;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 143
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 144
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    .line 145
    const-wide/16 v1, 0x0

    cmp-long v1, v5, v1

    if-gtz v1, :cond_30

    .line 147
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_73
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_2c} :catch_63
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_2c} :catch_6b

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_12

    .line 150
    :cond_30
    const/4 v1, 0x3

    :try_start_31
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 152
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 154
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 156
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    .line 158
    iget-object v0, p0, Lcom/google/android/location/i/b;->b:Lcom/google/android/location/i/c;

    move-wide v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v9

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    move-wide v1, p2

    move-object v7, v10

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/i/c;->a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_5f
    .catchall {:try_start_31 .. :try_end_5f} :catchall_73
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_5f} :catch_63
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_5f} :catch_6b

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_12

    .line 164
    :catch_63
    move-exception v0

    .line 165
    :try_start_64
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_67
    .catchall {:try_start_64 .. :try_end_67} :catchall_73

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_12

    .line 166
    :catch_6b
    move-exception v0

    .line 170
    :try_start_6c
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/i/b;->c(J)V
    :try_end_6f
    .catchall {:try_start_6c .. :try_end_6f} :catchall_73

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    goto :goto_12

    :catchall_73
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/location/i/b;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public b()Lcom/google/android/location/i/c;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/location/i/b;->c:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method public declared-synchronized b(J)V
    .registers 5
    .parameter

    .prologue
    .line 182
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/i/b;->f:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/i/b$a;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/i/b$a;-><init>(Lcom/google/android/location/i/b;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 183
    monitor-exit p0

    return-void

    .line 182
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lcom/google/android/location/i/c;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/location/i/b;->d:Lcom/google/android/location/i/c;

    return-object v0
.end method

.method public d()Lcom/google/android/location/i/c;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/location/i/b;->e:Lcom/google/android/location/i/c;

    return-object v0
.end method
