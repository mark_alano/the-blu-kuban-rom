.class public Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/internal/server/GoogleLocationService$NetworkServiceInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/location/os/e$b;

.field private b:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

.field private c:Lcom/google/android/location/os/e;

.field private final d:Landroid/os/HandlerThread;

.field private e:Lcom/google/android/location/internal/server/c;

.field private f:Landroid/app/Service;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$1;-><init>(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a:Lcom/google/android/location/os/e$b;

    .line 72
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NetworkLocationServiceImpl"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->d:Landroid/os/HandlerThread;

    .line 231
    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;I)J
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 189
    invoke-virtual {p1, p2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 190
    int-to-long v0, p3

    .line 198
    :goto_7
    return-wide v0

    .line 192
    :cond_8
    int-to-long v0, p3

    invoke-virtual {p1, p2, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 194
    int-to-long v2, p3

    cmp-long v2, v0, v2

    if-nez v2, :cond_17

    .line 195
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 197
    :cond_17
    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_7
.end method

.method static synthetic a(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Landroid/app/Service;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->f:Landroid/app/Service;

    return-object v0
.end method

.method private a()Ljava/io/FileOutputStream;
    .registers 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->f:Landroid/app/Service;

    const-string v1, "nlp_debug_log"

    const v2, 0x8000

    invoke-virtual {v0, v1, v2}, Landroid/app/Service;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)Lcom/google/android/location/internal/server/c;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 204
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/c;->c()V

    .line 205
    iput-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    .line 208
    invoke-static {v1}, Lcom/google/android/location/k/a/a;->a(Lcom/google/android/location/k/a/b;)V

    .line 209
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .registers 9
    .parameter

    .prologue
    .line 213
    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v4

    .line 214
    invoke-static {}, Lcom/google/android/location/os/real/h;->G()J

    move-result-wide v2

    .line 215
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "MM-dd HH:mm:ss.SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v0, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 216
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 217
    const-string v6, "elapsedRealtime "

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    .line 219
    const-string v6, " is time "

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/c;->a(Ljava/io/PrintWriter;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/internal/server/c;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;

    if-eqz v0, :cond_3f

    .line 224
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;

    sub-long/2addr v2, v4

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/os/e;->a(Ljava/text/Format;JJLjava/io/PrintWriter;)V

    .line 226
    :cond_3f
    return-void
.end method

.method public getBinder()Landroid/os/IBinder;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;)V
    .registers 13
    .parameter

    .prologue
    const-wide/16 v9, 0x3e8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 140
    const-string v0, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 141
    const-string v0, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 143
    const-string v3, "com.google.android.location.internal.EXTRA_DEBUG_INFO"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 144
    const-string v4, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    const/4 v5, -0x1

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v5

    .line 145
    if-eqz v0, :cond_4a

    const-wide/16 v7, 0x0

    cmp-long v4, v5, v7

    if-ltz v4, :cond_4a

    .line 146
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v4

    .line 155
    if-eqz v3, :cond_7b

    .line 156
    iget-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->f:Landroid/app/Service;

    invoke-virtual {v7}, Landroid/app/Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 158
    const-string v8, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v7, v8, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_77

    move v4, v1

    .line 161
    :goto_3e
    if-eqz v3, :cond_79

    if-eqz v4, :cond_79

    .line 165
    :goto_42
    iget-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    div-long v3, v5, v9

    long-to-int v3, v3

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/location/internal/server/c;->a(Landroid/app/PendingIntent;IZ)V

    .line 169
    :cond_4a
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 170
    const-string v0, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 172
    if-eqz v0, :cond_76

    .line 173
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {v1}, Lcom/google/android/location/internal/server/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_76

    .line 177
    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_PERIOD_MILLIS"

    const v2, 0x2bf20

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a(Landroid/content/Intent;Ljava/lang/String;I)J

    move-result-wide v1

    .line 179
    iget-object v3, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    div-long/2addr v1, v9

    long-to-int v1, v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/location/internal/server/c;->a(Landroid/app/PendingIntent;I)V

    .line 186
    :cond_76
    return-void

    :cond_77
    move v4, v2

    .line 158
    goto :goto_3e

    :cond_79
    move v1, v2

    .line 161
    goto :goto_42

    :cond_7b
    move v1, v3

    goto :goto_42
.end method

.method public declared-synchronized init(Landroid/app/Service;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 78
    monitor-enter p0

    :try_start_3
    iput-object p1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->f:Landroid/app/Service;

    .line 80
    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;
    :try_end_7
    .catchall {:try_start_3 .. :try_end_7} :catchall_62

    if-eqz v1, :cond_b

    .line 118
    :goto_9
    monitor-exit p0

    return-void

    .line 84
    :cond_b
    :try_start_b
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/os/real/j;->c()V

    .line 86
    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 87
    new-instance v1, Lcom/google/android/location/internal/server/c;

    iget-object v3, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->d:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p1, v3}, Lcom/google/android/location/internal/server/c;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;
    :try_end_24
    .catchall {:try_start_b .. :try_end_24} :catchall_62

    .line 92
    :try_start_24
    const-string v1, "nlp_debug_log"

    invoke-virtual {p1, v1}, Landroid/app/Service;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 93
    const/4 v1, 0x1

    .line 94
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2e
    .catchall {:try_start_24 .. :try_end_2e} :catchall_62
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_2e} :catch_65
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_2e} :catch_68

    .line 101
    :goto_2e
    if-nez v1, :cond_6b

    move-object v1, v0

    .line 109
    :goto_31
    :try_start_31
    new-instance v2, Lcom/google/android/location/os/e;

    iget-object v3, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a:Lcom/google/android/location/os/e$b;

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/location/os/real/j;->b()Z

    move-result v4

    if-eqz v4, :cond_7d

    :goto_3f
    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/location/os/e;-><init>(Lcom/google/android/location/os/e$b;Lcom/google/android/location/k/a/b;Ljava/io/PrintWriter;)V

    iput-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;

    .line 113
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/os/e;)V

    .line 114
    new-instance v0, Lcom/google/android/location/os/real/a;

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->c:Lcom/google/android/location/os/e;

    invoke-direct {v0, v1}, Lcom/google/android/location/os/real/a;-><init>(Lcom/google/android/location/os/e;)V

    invoke-static {v0}, Lcom/google/android/location/k/a/a;->a(Lcom/google/android/location/k/a/b;)V

    .line 116
    new-instance v0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;

    invoke-direct {v0, p0, p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl$a;-><init>(Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->b:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    .line 117
    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->e:Lcom/google/android/location/internal/server/c;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/c;->b()V
    :try_end_61
    .catchall {:try_start_31 .. :try_end_61} :catchall_62

    goto :goto_9

    .line 78
    :catchall_62
    move-exception v0

    monitor-exit p0

    throw v0

    .line 95
    :catch_65
    move-exception v1

    move v1, v2

    .line 99
    goto :goto_2e

    .line 97
    :catch_68
    move-exception v1

    move v1, v2

    .line 98
    goto :goto_2e

    .line 103
    :cond_6b
    :try_start_6b
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationServiceImpl;->a()Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 104
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_79
    .catchall {:try_start_6b .. :try_end_79} :catchall_62
    .catch Ljava/io/FileNotFoundException; {:try_start_6b .. :try_end_79} :catch_7a

    goto :goto_31

    .line 105
    :catch_7a
    move-exception v1

    move-object v1, v0

    goto :goto_31

    .line 109
    :cond_7d
    :try_start_7d
    new-instance v0, Lcom/google/android/location/os/real/a;

    invoke-direct {v0}, Lcom/google/android/location/os/real/a;-><init>()V
    :try_end_82
    .catchall {:try_start_7d .. :try_end_82} :catchall_62

    goto :goto_3f
.end method
