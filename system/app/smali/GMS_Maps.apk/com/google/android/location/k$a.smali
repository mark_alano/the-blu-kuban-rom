.class Lcom/google/android/location/k$a;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/k;


# direct methods
.method private constructor <init>(Lcom/google/android/location/k;)V
    .registers 2
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/k;Lcom/google/android/location/k$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/google/android/location/k$a;-><init>(Lcom/google/android/location/k;)V

    return-void
.end method

.method private j()V
    .registers 5

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/location/k;->i:Lcom/google/android/location/c/r;

    .line 200
    iget-object v0, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/google/android/location/k;->j:J

    .line 201
    iget-object v0, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    iget-object v0, v0, Lcom/google/android/location/k;->b:Lcom/google/android/location/os/i;

    const/16 v1, 0x9

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 202
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    iget-object v0, v0, Lcom/google/android/location/k;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 187
    iget-object v2, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    iget-wide v2, v2, Lcom/google/android/location/k;->j:J

    sub-long/2addr v0, v2

    .line 188
    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2a

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_2a

    .line 190
    iget-object v0, p0, Lcom/google/android/location/k$a;->a:Lcom/google/android/location/k;

    invoke-static {v0}, Lcom/google/android/location/k;->a(Lcom/google/android/location/k;)Lcom/google/android/location/l;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/l;->a(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 195
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/location/k$a;->j()V

    .line 196
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/location/k$a;->j()V

    .line 179
    return-void
.end method
