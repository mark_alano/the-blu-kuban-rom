.class public Lcom/google/android/location/k/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 190
    new-instance v0, Lcom/google/android/location/k/b$1;

    invoke-direct {v0}, Lcom/google/android/location/k/b$1;-><init>()V

    sput-object v0, Lcom/google/android/location/k/b;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(IIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-static {p1, p2, v2, v2}, Lcom/google/android/location/k/c;->a(IIII)J

    move-result-wide v0

    invoke-static {p3, p4, v2, v2}, Lcom/google/android/location/k/c;->a(IIII)J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/location/k/b;-><init>(JJ)V

    .line 25
    return-void
.end method

.method public constructor <init>(JJ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/google/android/location/k/b;->a:J

    .line 35
    iput-wide p3, p0, Lcom/google/android/location/k/b;->b:J

    .line 36
    cmp-long v0, p3, p1

    if-gtz v0, :cond_13

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid time span."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_13
    return-void
.end method

.method private e(J)Z
    .registers 5
    .parameter

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public a()J
    .registers 5

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;
    .registers 9
    .parameter

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    .line 165
    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v5, p1, Lcom/google/android/location/k/b;->b:J

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    .line 166
    cmp-long v0, v3, v1

    if-gtz v0, :cond_16

    .line 167
    const/4 v0, 0x0

    .line 169
    :goto_15
    return-object v0

    :cond_16
    new-instance v0, Lcom/google/android/location/k/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    goto :goto_15
.end method

.method public a(Lcom/google/android/location/k/b;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/k/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    iget-wide v0, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_10

    iget-wide v0, p1, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    .line 140
    :cond_10
    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_13
    :goto_13
    return-void

    .line 143
    :cond_14
    iget-wide v0, p1, Lcom/google/android/location/k/b;->a:J

    iget-wide v2, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_31

    .line 144
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_13

    .line 145
    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 148
    :cond_31
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v2, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_52

    .line 150
    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p1, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 153
    :cond_52
    new-instance v0, Lcom/google/android/location/k/b;

    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13
.end method

.method public a(J)Z
    .registers 7
    .parameter

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(Ljava/util/Calendar;)Z
    .registers 5
    .parameter

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 47
    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v2

    if-nez v2, :cond_10

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/k/b;->e(J)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public b(J)Z
    .registers 5
    .parameter

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public b(Ljava/util/Calendar;)Z
    .registers 4
    .parameter

    .prologue
    .line 69
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->a(J)Z

    move-result v0

    return v0
.end method

.method public c(J)Z
    .registers 5
    .parameter

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_e

    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public c(Ljava/util/Calendar;)Z
    .registers 4
    .parameter

    .prologue
    .line 92
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v0

    return v0
.end method

.method public d(J)J
    .registers 5
    .parameter

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/google/android/location/k/b;->a:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_b

    .line 124
    invoke-virtual {p0}, Lcom/google/android/location/k/b;->a()J

    move-result-wide v0

    .line 128
    :goto_a
    return-wide v0

    .line 125
    :cond_b
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_15

    .line 126
    iget-wide v0, p0, Lcom/google/android/location/k/b;->b:J

    sub-long/2addr v0, p1

    goto :goto_a

    .line 128
    :cond_15
    const-wide/16 v0, 0x0

    goto :goto_a
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 174
    if-eqz p1, :cond_7

    instance-of v1, p1, Lcom/google/android/location/k/b;

    if-nez v1, :cond_8

    .line 178
    :cond_7
    :goto_7
    return v0

    .line 177
    :cond_8
    check-cast p1, Lcom/google/android/location/k/b;

    .line 178
    iget-wide v1, p0, Lcom/google/android/location/k/b;->a:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_7

    iget-wide v1, p0, Lcom/google/android/location/k/b;->b:J

    iget-wide v3, p1, Lcom/google/android/location/k/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 183
    const-string v0, "TimeSpan: [%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/location/k/b;->a:J

    invoke-static {v3, v4}, Lcom/google/android/location/k/c;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/location/k/b;->b:J

    invoke-static {v3, v4}, Lcom/google/android/location/k/c;->b(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
