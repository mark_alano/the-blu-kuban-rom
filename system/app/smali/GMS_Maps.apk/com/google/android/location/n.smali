.class public Lcom/google/android/location/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/n$1;,
        Lcom/google/android/location/n$a;,
        Lcom/google/android/location/n$b;,
        Lcom/google/android/location/n$c;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/google/android/location/k/b;

.field private final c:J

.field private final d:I

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/k/b;)V
    .registers 10
    .parameter

    .prologue
    .line 87
    const/4 v2, 0x3

    const-wide/32 v3, 0x927c0

    const-wide/32 v5, 0xdbba0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/n;-><init>(Lcom/google/android/location/k/b;IJJZ)V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/k/b;IJJZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    .line 97
    iput p2, p0, Lcom/google/android/location/n;->d:I

    .line 98
    iput-wide p3, p0, Lcom/google/android/location/n;->c:J

    .line 99
    iput-wide p5, p0, Lcom/google/android/location/n;->e:J

    .line 100
    iput-boolean p7, p0, Lcom/google/android/location/n;->a:Z

    .line 101
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Map;)Ljava/util/Calendar;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 221
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_11

    :cond_f
    move-object v0, v1

    .line 234
    :goto_10
    return-object v0

    .line 225
    :cond_11
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    if-nez v0, :cond_23

    move-object v0, v1

    .line 229
    goto :goto_10

    .line 231
    :cond_23
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 232
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_10
.end method

.method private a(Ljava/util/List;Lcom/google/android/location/k/b;)Ljava/util/List;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/k/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 436
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 437
    invoke-virtual {v0, p2, v1}, Lcom/google/android/location/k/b;->a(Lcom/google/android/location/k/b;Ljava/util/List;)V

    goto :goto_8

    .line 439
    :cond_18
    return-object v1
.end method

.method private b(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$b;

    goto :goto_4

    .line 472
    :cond_11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_17

    .line 475
    :cond_17
    return-void
.end method

.method private b(Ljava/util/SortedMap;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 479
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_8

    .line 483
    :cond_15
    return-void
.end method

.method private b(Ljava/util/TreeMap;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 323
    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_78

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 324
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->b:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_46

    .line 325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 326
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    .line 327
    if-eqz v2, :cond_36

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-ne v2, v4, :cond_8

    :cond_36
    if-eqz v3, :cond_40

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_8

    .line 329
    :cond_40
    sget-object v2, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 331
    :cond_46
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->d:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_8

    .line 332
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 333
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    .line 334
    if-eqz v2, :cond_68

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v2, v4, :cond_8

    :cond_68
    if-eqz v3, :cond_72

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v2, v3, :cond_8

    .line 336
    :cond_72
    sget-object v2, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 340
    :cond_78
    return-void
.end method

.method private c(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 499
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    goto :goto_4

    .line 502
    :cond_11
    return-void
.end method

.method private c(Ljava/util/SortedMap;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 487
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 488
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 489
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 490
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    goto :goto_8

    .line 495
    :cond_38
    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    .line 242
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 243
    const/4 v0, 0x1

    .line 250
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    goto :goto_8
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/List;
    .registers 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 413
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    :cond_e
    :goto_e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$b;

    .line 416
    iget-wide v2, p0, Lcom/google/android/location/n;->c:J

    const-wide/16 v4, 0x4

    div-long v4, v2, v4

    .line 417
    iget-object v0, v0, Lcom/google/android/location/n$b;->a:Lcom/google/android/location/k/b;

    .line 418
    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    iget-object v7, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v7, v7, Lcom/google/android/location/k/b;->a:J

    cmp-long v2, v2, v7

    if-nez v2, :cond_49

    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    .line 421
    :goto_2e
    iget-wide v7, v0, Lcom/google/android/location/k/b;->b:J

    iget-object v9, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v9, v9, Lcom/google/android/location/k/b;->b:J

    cmp-long v7, v7, v9

    if-nez v7, :cond_4d

    iget-wide v4, v0, Lcom/google/android/location/k/b;->b:J

    .line 423
    :goto_3a
    cmp-long v0, v4, v2

    if-lez v0, :cond_e

    .line 428
    new-instance v0, Lcom/google/android/location/k/b;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/n;->a(Ljava/util/List;Lcom/google/android/location/k/b;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 429
    goto :goto_e

    .line 418
    :cond_49
    iget-wide v2, v0, Lcom/google/android/location/k/b;->a:J

    add-long/2addr v2, v4

    goto :goto_2e

    .line 421
    :cond_4d
    iget-wide v7, v0, Lcom/google/android/location/k/b;->b:J

    sub-long v4, v7, v4

    goto :goto_3a

    .line 430
    :cond_52
    invoke-direct {p0, v1}, Lcom/google/android/location/n;->c(Ljava/util/List;)V

    .line 431
    return-object v1
.end method

.method a(Ljava/util/SortedMap;)Ljava/util/List;
    .registers 16
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/n$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 349
    invoke-interface {p1}, Ljava/util/SortedMap;->size()I

    move-result v0

    if-gtz v0, :cond_13

    .line 350
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_11

    .line 351
    invoke-direct {p0, v8}, Lcom/google/android/location/n;->b(Ljava/util/List;)V

    :cond_11
    move-object v0, v8

    .line 402
    :goto_12
    return-object v0

    .line 355
    :cond_13
    const-wide/16 v1, -0x1

    .line 356
    const/4 v5, 0x0

    .line 357
    const/4 v0, 0x0

    .line 358
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v3, v0

    :goto_20
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 359
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/location/n$a;

    .line 360
    const-wide/16 v10, -0x1

    cmp-long v0, v1, v10

    if-nez v0, :cond_5b

    .line 361
    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-eq v7, v0, :cond_42

    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v7, v0, :cond_53

    .line 364
    :cond_42
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 365
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$a;

    move-object v5, v0

    .line 386
    :cond_53
    :goto_53
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v3, v0

    .line 387
    goto :goto_20

    .line 368
    :cond_5b
    if-eq v7, v5, :cond_53

    .line 370
    invoke-static {v3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v10, v1

    iget-wide v12, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v10, v12

    iget-wide v12, p0, Lcom/google/android/location/n;->e:J

    cmp-long v0, v10, v12

    if-ltz v0, :cond_85

    .line 373
    new-instance v0, Lcom/google/android/location/n$b;

    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v10, v4, Lcom/google/android/location/k/b;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v12, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v3, v12

    invoke-static {v10, v11, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/n$b;-><init>(JJLcom/google/android/location/n$a;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    :cond_85
    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    if-eq v7, v0, :cond_8d

    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    if-ne v7, v0, :cond_9f

    .line 378
    :cond_8d
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 379
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$a;

    move-object v5, v0

    goto :goto_53

    .line 381
    :cond_9f
    const-wide/16 v1, -0x1

    .line 382
    const/4 v5, 0x0

    goto :goto_53

    .line 390
    :cond_a3
    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_d7

    .line 391
    invoke-interface {p1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Long;

    .line 392
    if-eqz v3, :cond_d7

    .line 393
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, v1

    iget-wide v9, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v6, v9

    iget-wide v9, p0, Lcom/google/android/location/n;->e:J

    cmp-long v0, v6, v9

    if-ltz v0, :cond_d7

    .line 394
    new-instance v0, Lcom/google/android/location/n$b;

    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v6, v4, Lcom/google/android/location/k/b;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v9, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v3, v9

    invoke-static {v6, v7, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/n$b;-><init>(JJLcom/google/android/location/n$a;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_d7
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_de

    .line 400
    invoke-direct {p0, v8}, Lcom/google/android/location/n;->b(Ljava/util/List;)V

    :cond_de
    move-object v0, v8

    .line 402
    goto/16 :goto_12
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/google/android/location/n;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v2

    .line 112
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 113
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 115
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {p0, v2, v1, p1}, Lcom/google/android/location/n;->a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/TreeMap;

    move-result-object v1

    .line 117
    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/TreeMap;)Ljava/util/TreeMap;

    move-result-object v1

    .line 119
    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v1

    .line 121
    invoke-virtual {p0, v1}, Lcom/google/android/location/n;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 122
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    .line 124
    :cond_3a
    return-object v3
.end method

.method a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/TreeMap;
    .registers 14
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v1, v0, :cond_93

    .line 154
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 156
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v0, v4, :cond_21

    .line 153
    :cond_1d
    :goto_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 159
    :cond_21
    invoke-direct {p0, v3, p1}, Lcom/google/android/location/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Map;)Ljava/util/Calendar;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_1d

    .line 166
    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_37

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3c

    .line 168
    :cond_37
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_1d

    goto :goto_1d

    .line 173
    :cond_3c
    iget-object v4, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    invoke-virtual {v4, v0}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v4

    if-nez v4, :cond_49

    .line 174
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_1d

    goto :goto_1d

    .line 179
    :cond_49
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v4

    .line 180
    iget-object v0, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v6, v0, Lcom/google/android/location/k/b;->a:J

    sub-long v6, v4, v6

    iget-wide v8, p0, Lcom/google/android/location/n;->c:J

    rem-long/2addr v6, v8

    sub-long/2addr v4, v6

    .line 182
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$c;

    .line 183
    invoke-direct {p0, v3}, Lcom/google/android/location/n;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v6

    .line 184
    if-nez v0, :cond_8a

    .line 185
    new-instance v7, Lcom/google/android/location/n$c;

    if-eqz v6, :cond_88

    const/4 v0, 0x1

    :goto_6c
    const/4 v6, 0x1

    const/4 v8, 0x0

    invoke-direct {v7, p0, v0, v6, v8}, Lcom/google/android/location/n$c;-><init>(Lcom/google/android/location/n;IILcom/google/android/location/n$1;)V

    .line 186
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :goto_78
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_1d

    .line 194
    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    goto :goto_1d

    .line 185
    :cond_88
    const/4 v0, 0x0

    goto :goto_6c

    .line 188
    :cond_8a
    if-eqz v6, :cond_8f

    .line 189
    invoke-virtual {v0}, Lcom/google/android/location/n$c;->a()V

    .line 191
    :cond_8f
    invoke-virtual {v0}, Lcom/google/android/location/n$c;->b()V

    goto :goto_78

    .line 203
    :cond_93
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 204
    invoke-virtual {v2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_be

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 205
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/n$c;

    invoke-virtual {v0}, Lcom/google/android/location/n$c;->c()Lcom/google/android/location/e/u;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a0

    .line 207
    :cond_be
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_c5

    .line 208
    invoke-direct {p0, v1}, Lcom/google/android/location/n;->c(Ljava/util/SortedMap;)V

    .line 210
    :cond_c5
    return-object v1
.end method

.method a(Ljava/util/TreeMap;)Ljava/util/TreeMap;
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/n$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    .line 274
    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_82

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 275
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 276
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/u;

    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v6, v1

    .line 277
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/u;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    .line 278
    sget-object v0, Lcom/google/android/location/n$a;->f:Lcom/google/android/location/n$a;

    .line 279
    iget v1, p0, Lcom/google/android/location/n;->d:I

    int-to-long v10, v1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_6b

    .line 280
    long-to-double v0, v8

    div-double v0, v6, v0

    double-to-float v0, v0

    .line 281
    const v1, 0x3f4ccccd

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_5e

    .line 282
    sget-object v0, Lcom/google/android/location/n$a;->a:Lcom/google/android/location/n$a;

    .line 296
    :cond_56
    :goto_56
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 283
    :cond_5e
    const v1, 0x3e4ccccc

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_68

    .line 284
    sget-object v0, Lcom/google/android/location/n$a;->c:Lcom/google/android/location/n$a;

    goto :goto_56

    .line 286
    :cond_68
    sget-object v0, Lcom/google/android/location/n$a;->e:Lcom/google/android/location/n$a;

    goto :goto_56

    .line 288
    :cond_6b
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_56

    .line 289
    long-to-double v8, v8

    cmpl-double v1, v6, v8

    if-nez v1, :cond_79

    .line 291
    sget-object v0, Lcom/google/android/location/n$a;->b:Lcom/google/android/location/n$a;

    goto :goto_56

    .line 292
    :cond_79
    const-wide/16 v8, 0x0

    cmpl-double v1, v6, v8

    if-nez v1, :cond_56

    .line 293
    sget-object v0, Lcom/google/android/location/n$a;->d:Lcom/google/android/location/n$a;

    goto :goto_56

    .line 300
    :cond_82
    iget-object v0, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    :goto_86
    iget-object v3, p0, Lcom/google/android/location/n;->b:Lcom/google/android/location/k/b;

    iget-wide v3, v3, Lcom/google/android/location/k/b;->b:J

    cmp-long v3, v0, v3

    if-gez v3, :cond_a5

    .line 302
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a1

    .line 303
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/n$a;->f:Lcom/google/android/location/n$a;

    invoke-virtual {v2, v3, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    :cond_a1
    iget-wide v3, p0, Lcom/google/android/location/n;->c:J

    add-long/2addr v0, v3

    goto :goto_86

    .line 306
    :cond_a5
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_ac

    .line 307
    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/SortedMap;)V

    .line 309
    :cond_ac
    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/TreeMap;)V

    .line 310
    iget-boolean v0, p0, Lcom/google/android/location/n;->a:Z

    if-eqz v0, :cond_b6

    .line 312
    invoke-direct {p0, v2}, Lcom/google/android/location/n;->b(Ljava/util/SortedMap;)V

    .line 314
    :cond_b6
    return-object v2
.end method

.method b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 132
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 133
    const/4 v0, 0x0

    :goto_7
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_2f

    .line 134
    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 135
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_2c

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 136
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 140
    :cond_2f
    return-object v1
.end method
