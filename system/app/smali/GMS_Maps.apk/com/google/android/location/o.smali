.class public Lcom/google/android/location/o;
.super Lcom/google/android/location/a;
.source "SourceFile"


# instance fields
.field final g:[Lcom/google/android/location/a;

.field h:Lcom/google/android/location/a;

.field final i:Lcom/google/android/location/b/g;

.field final j:Lcom/google/android/location/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/x;Lcom/google/android/location/t;Lcom/google/android/location/i/a;Lcom/google/android/location/e/d;Lcom/google/android/location/a/b;Lcom/google/android/location/g/l;)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    const-string v2, "NetworkCollector"

    new-instance v6, Lcom/google/android/location/a$b;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Lcom/google/android/location/a$b;-><init>(Lcom/google/android/location/b/g;)V

    sget-object v7, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 54
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    .line 55
    new-instance v1, Lcom/google/android/location/f;

    iget-object v5, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    invoke-static {p1}, Lcom/google/android/location/f;->a(Lcom/google/android/location/os/i;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/f;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/t;Lcom/google/android/location/g;)V

    .line 58
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/o;->j:Lcom/google/android/location/e/d;

    .line 59
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v12

    .line 63
    new-instance v2, Lcom/google/android/location/c;

    iget-object v3, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object/from16 v0, p5

    invoke-direct {v2, p1, p2, v0, v3}, Lcom/google/android/location/c;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v2, Lcom/google/android/location/l;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/l;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v2, Lcom/google/android/location/e;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/e;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v2, Lcom/google/android/location/v;

    iget-object v6, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v7, p4

    move-object v8, v1

    move-object/from16 v9, p7

    move-object/from16 v10, p9

    invoke-direct/range {v2 .. v11}, Lcom/google/android/location/v;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v1, Lcom/google/android/location/r;

    iget-object v5, p0, Lcom/google/android/location/o;->e:Lcom/google/android/location/a$b;

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/location/r;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/location/a;

    invoke-interface {v12, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/location/a;

    iput-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    .line 77
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    .line 79
    return-void
.end method

.method private a(Lcom/google/android/location/a;)V
    .registers 3
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    if-eq v0, p1, :cond_4

    .line 135
    :cond_4
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->a()V

    .line 110
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 112
    iget-object v2, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v3, v2

    move v0, v1

    :goto_12
    if-ge v0, v3, :cond_28

    aget-object v4, v2, v0

    .line 113
    iget-object v5, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    if-eq v4, v5, :cond_46

    .line 114
    invoke-virtual {v4}, Lcom/google/android/location/a;->a()V

    .line 115
    invoke-virtual {v4}, Lcom/google/android/location/a;->b()Z

    move-result v5

    if-nez v5, :cond_46

    .line 116
    invoke-direct {p0, v4}, Lcom/google/android/location/o;->a(Lcom/google/android/location/a;)V

    .line 117
    iput-object v4, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    .line 122
    :cond_28
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    iget-object v2, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v2, v2, v1

    if-eq v0, v2, :cond_45

    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 124
    iget-object v0, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/location/o;->a(Lcom/google/android/location/a;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    .line 128
    :cond_45
    return-void

    .line 112
    :cond_46
    add-int/lit8 v0, v0, 0x1

    goto :goto_12
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    .line 156
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 157
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(I)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 159
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 160
    return-void
.end method

.method public a(IIZ)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 211
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 212
    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/location/a;->a(IIZ)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 214
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 215
    return-void
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 6
    .parameter

    .prologue
    .line 243
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 244
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 246
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 247
    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 6
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_9

    .line 193
    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->a(Lcom/google/android/location/e/E;)V

    .line 195
    :cond_9
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v2, :cond_17

    aget-object v3, v1, v0

    .line 196
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/E;)V

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 198
    :cond_17
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 199
    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 6
    .parameter

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/location/o;->j:Lcom/google/android/location/e/d;

    invoke-virtual {v0}, Lcom/google/android/location/e/d;->b()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 167
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 168
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/e;)V

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 170
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 172
    :cond_19
    return-void
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 6
    .parameter

    .prologue
    .line 176
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 177
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 179
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 180
    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 6
    .parameter

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 228
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/h;)V

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 230
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 231
    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 252
    invoke-virtual {v3, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 254
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 255
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 150
    return-void
.end method

.method public a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 203
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 204
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->a(Z)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 206
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 207
    return-void
.end method

.method b(Lcom/google/android/location/e/E;)V
    .registers 6
    .parameter

    .prologue
    .line 98
    if-eqz p1, :cond_17

    .line 99
    iget-object v0, p0, Lcom/google/android/location/o;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/google/android/location/e/E;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gez v0, :cond_17

    .line 100
    iget-object v0, p0, Lcom/google/android/location/o;->i:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->b(Lcom/google/android/location/e/E;)V

    .line 103
    :cond_17
    return-void
.end method

.method public b(Z)V
    .registers 6
    .parameter

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 185
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->b(Z)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 187
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 188
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/location/o;->h:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .registers 5

    .prologue
    .line 235
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 236
    invoke-virtual {v3}, Lcom/google/android/location/a;->c()V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 238
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 239
    return-void
.end method

.method public c(Z)V
    .registers 6
    .parameter

    .prologue
    .line 219
    iget-object v1, p0, Lcom/google/android/location/o;->g:[Lcom/google/android/location/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 220
    invoke-virtual {v3, p1}, Lcom/google/android/location/a;->c(Z)V

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 222
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/location/o;->a()V

    .line 223
    return-void
.end method
