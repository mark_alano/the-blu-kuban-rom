.class Lcom/google/android/location/os/e$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field final e:Lcom/google/android/location/os/d;

.field final f:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/d;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    .line 47
    iput-wide p2, p0, Lcom/google/android/location/os/e$a;->f:J

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/PrintWriter;)V
    .registers 2
    .parameter

    .prologue
    .line 73
    return-void
.end method

.method public a(Ljava/text/Format;Ljava/util/Date;Ljava/io/PrintWriter;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/os/e$a;->f:J

    add-long/2addr v0, v2

    invoke-virtual {p2, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 67
    invoke-virtual {p1, p2}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 68
    const/16 v0, 0x20

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V

    .line 69
    invoke-virtual {p0, p3}, Lcom/google/android/location/os/e$a;->b(Ljava/io/PrintWriter;)V

    .line 70
    return-void
.end method

.method public b(Ljava/io/PrintWriter;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x20

    .line 51
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    .line 52
    iget-wide v0, p0, Lcom/google/android/location/os/e$a;->f:J

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    .line 53
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    .line 54
    iget-object v0, p0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    invoke-virtual {v0}, Lcom/google/android/location/os/d;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    .line 56
    invoke-virtual {p0, p1}, Lcom/google/android/location/os/e$a;->a(Ljava/io/PrintWriter;)V

    .line 57
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    .line 58
    return-void
.end method
