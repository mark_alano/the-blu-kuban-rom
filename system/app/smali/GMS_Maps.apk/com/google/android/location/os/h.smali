.class public Lcom/google/android/location/os/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/h$a;
    }
.end annotation


# static fields
.field static final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# instance fields
.field final b:Lcom/google/android/location/os/c;

.field final c:Lcom/google/android/location/os/f;

.field volatile d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field volatile e:J

.field private final f:Lcom/google/android/location/k/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/os/h;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 44
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->R:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/c;Lcom/google/android/location/os/f;Lcom/google/android/location/k/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/os/c;",
            "Lcom/google/android/location/os/f;",
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/os/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-object v0, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 64
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/os/h;->e:J

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    .line 75
    iput-object p1, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    .line 76
    iput-object p2, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    .line 77
    iput-object p3, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    .line 78
    return-void
.end method

.method private a(J)J
    .registers 8
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->b()J

    move-result-wide v0

    .line 132
    iget-object v2, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v2

    .line 135
    sub-long v2, v0, v2

    .line 138
    cmp-long v4, p1, v0

    if-lez v4, :cond_13

    move-wide p1, v0

    .line 140
    :cond_13
    sub-long v0, p1, v2

    .line 141
    return-wide v0
.end method

.method private a(Lcom/google/android/location/os/f;)Ljava/io/File;
    .registers 5
    .parameter

    .prologue
    .line 145
    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lcom/google/android/location/os/f;->g()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_params"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 146
    return-object v0
.end method

.method private a(Ljava/io/Closeable;)V
    .registers 3
    .parameter

    .prologue
    .line 350
    if-eqz p1, :cond_5

    .line 352
    :try_start_2
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 357
    :cond_5
    :goto_5
    return-void

    .line 353
    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method private a(IZ)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 258
    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 259
    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result p2

    .line 261
    :cond_e
    return p2
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 154
    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne p1, v1, :cond_6

    .line 158
    :cond_5
    :goto_5
    return v0

    .line 157
    :cond_6
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    .line 158
    add-long/2addr v1, p2

    iget-object v3, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v3}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 2
    .parameter

    .prologue
    .line 126
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private q()V
    .registers 9

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v2

    .line 163
    const/4 v0, 0x0

    .line 165
    :try_start_7
    iget-object v1, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne v1, v3, :cond_1a

    .line 166
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 167
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_5a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_16} :catch_64

    .line 186
    :cond_16
    :goto_16
    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    .line 188
    :goto_19
    return-void

    .line 170
    :cond_1a
    :try_start_1a
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_23

    .line 171
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 173
    :cond_23
    iget-object v1, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    .line 174
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_32
    .catchall {:try_start_1a .. :try_end_32} :catchall_5a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_32} :catch_64

    .line 175
    const/4 v0, 0x2

    :try_start_33
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 177
    iget-object v2, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v2
    :try_end_39
    .catchall {:try_start_33 .. :try_end_39} :catchall_62
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_39} :catch_54

    .line 178
    :try_start_39
    iget-wide v3, p0, Lcom/google/android/location/os/h;->e:J

    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->c()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 179
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 180
    monitor-exit v2

    move-object v0, v1

    goto :goto_16

    :catchall_51
    move-exception v0

    monitor-exit v2
    :try_end_53
    .catchall {:try_start_39 .. :try_end_53} :catchall_51

    :try_start_53
    throw v0
    :try_end_54
    .catchall {:try_start_53 .. :try_end_54} :catchall_62
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_54} :catch_54

    .line 182
    :catch_54
    move-exception v0

    move-object v0, v1

    .line 186
    :goto_56
    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_19

    :catchall_5a
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_5e
    invoke-direct {p0, v1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_62
    move-exception v0

    goto :goto_5e

    .line 182
    :catch_64
    move-exception v1

    goto :goto_56
.end method

.method private r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 337
    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 338
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eq v0, v2, :cond_16

    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v2, p0, Lcom/google/android/location/os/h;->e:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 340
    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V

    .line 342
    :cond_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_1a

    .line 343
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0

    .line 342
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private declared-synchronized s()V
    .registers 6

    .prologue
    .line 382
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_29

    .line 383
    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    .line 384
    sget-object v2, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v2

    .line 385
    sget-object v3, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v3, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 386
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/google/android/location/os/h;->e:J

    .line 387
    if-eq v0, v2, :cond_23

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_23

    .line 388
    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    .line 390
    :cond_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_4 .. :try_end_24} :catchall_26

    .line 391
    monitor-exit p0

    return-void

    .line 390
    :catchall_26
    move-exception v0

    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    :try_start_28
    throw v0
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_29

    .line 382
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/location/os/h;->c:Lcom/google/android/location/os/f;

    invoke-direct {p0, v0}, Lcom/google/android/location/os/h;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 84
    :try_start_c
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/google/android/location/os/h;->a(Ljava/io/DataInputStream;)V
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_19} :catch_1a

    .line 91
    :cond_19
    :goto_19
    return-void

    .line 86
    :catch_1a
    move-exception v0

    .line 87
    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V

    goto :goto_19
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    .line 360
    iget-object v1, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 361
    if-eqz p1, :cond_2b

    .line 364
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    int-to-long v2, v0

    .line 365
    invoke-static {p1}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    int-to-long v4, v0

    .line 366
    iput-object p1, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 367
    iget-object v0, p0, Lcom/google/android/location/os/h;->b:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/os/h;->e:J

    .line 368
    cmp-long v0, v4, v2

    if-eqz v0, :cond_2b

    .line 372
    invoke-direct {p0}, Lcom/google/android/location/os/h;->q()V

    .line 373
    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_2b

    .line 374
    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    .line 378
    :cond_2b
    monitor-exit v1

    .line 379
    return-void

    .line 378
    :catchall_2d
    move-exception v0

    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_5 .. :try_end_2f} :catchall_2d

    throw v0
.end method

.method a(Ljava/io/DataInputStream;)V
    .registers 6
    .parameter

    .prologue
    .line 95
    :try_start_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 96
    const/4 v1, 0x2

    if-eq v0, v1, :cond_e

    .line 100
    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V
    :try_end_a
    .catchall {:try_start_0 .. :try_end_a} :catchall_54
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_a} :catch_48

    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    .line 123
    :goto_d
    return-void

    .line 103
    :cond_e
    :try_start_e
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(J)J

    move-result-wide v0

    .line 104
    sget-object v2, Lcom/google/android/location/j/a;->R:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p1, v2}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 105
    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v3

    if-nez v3, :cond_50

    .line 106
    iget-object v3, p0, Lcom/google/android/location/os/h;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_25
    .catchall {:try_start_e .. :try_end_25} :catchall_54
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_25} :catch_48

    .line 107
    :try_start_25
    iput-object v2, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 108
    iput-wide v0, p0, Lcom/google/android/location/os/h;->e:J

    .line 109
    iget-object v0, p0, Lcom/google/android/location/os/h;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    if-eq v0, v1, :cond_40

    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    if-eqz v0, :cond_40

    .line 111
    iget-object v0, p0, Lcom/google/android/location/os/h;->f:Lcom/google/android/location/k/a;

    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    .line 113
    :cond_40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_25 .. :try_end_41} :catchall_45

    .line 121
    :goto_41
    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_d

    .line 113
    :catchall_45
    move-exception v0

    :try_start_46
    monitor-exit v3
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    :try_start_47
    throw v0
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_54
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_48} :catch_48

    .line 118
    :catch_48
    move-exception v0

    .line 119
    :try_start_49
    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_54

    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    goto :goto_d

    .line 116
    :cond_50
    :try_start_50
    invoke-direct {p0}, Lcom/google/android/location/os/h;->s()V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_54
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_53} :catch_48

    goto :goto_41

    .line 121
    :catchall_54
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/location/os/h;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public c()I
    .registers 2

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v0

    return v0
.end method

.method public d()I
    .registers 3

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public e()I
    .registers 3

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public f()Z
    .registers 3

    .prologue
    .line 226
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .registers 3

    .prologue
    .line 233
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 240
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 247
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/h;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public j()I
    .registers 3

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Lcom/google/android/location/os/h$a;
    .registers 10

    .prologue
    .line 277
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 278
    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x16

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-long v1, v1

    const/16 v3, 0x22

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    const/16 v6, 0x23

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public l()Lcom/google/android/location/os/h$a;
    .registers 10

    .prologue
    const-wide/16 v7, 0x3e8

    .line 289
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 290
    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x15

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-long v1, v1

    mul-long/2addr v1, v7

    const/16 v3, 0x1f

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v3, v7

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public m()Lcom/google/android/location/os/h$a;
    .registers 8

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 301
    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x17

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x24

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public n()Lcom/google/android/location/os/h$a;
    .registers 8

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 312
    new-instance v0, Lcom/google/android/location/os/h$a;

    const/16 v1, 0x18

    invoke-virtual {v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const/16 v3, 0x26

    invoke-virtual {v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    const/16 v6, 0x27

    invoke-virtual {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/h$a;-><init>(JJJ)V

    return-object v0
.end method

.method public o()J
    .registers 3

    .prologue
    .line 322
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method

.method public p()J
    .registers 3

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/google/android/location/os/h;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0
.end method
