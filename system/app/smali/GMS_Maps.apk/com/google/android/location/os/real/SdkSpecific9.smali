.class public Lcom/google/android/location/os/real/SdkSpecific9;
.super Lcom/google/android/location/os/real/SdkSpecific8;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/location/os/real/SdkSpecific8;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/telephony/gsm/GsmCellLocation;)I
    .registers 3
    .parameter

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getPsc()I

    move-result v0

    return v0
.end method

.method public a(Ljava/io/File;)V
    .registers 3
    .parameter

    .prologue
    .line 23
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p1, v0}, Ljava/io/File;->setReadable(Z)Z
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_4} :catch_f

    move-result v0

    if-nez v0, :cond_7

    .line 30
    :cond_7
    :goto_7
    const/4 v0, 0x1

    :try_start_8
    invoke-virtual {p1, v0}, Ljava/io/File;->setWritable(Z)Z
    :try_end_b
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_b} :catch_11

    move-result v0

    if-nez v0, :cond_e

    .line 36
    :cond_e
    :goto_e
    return-void

    .line 26
    :catch_f
    move-exception v0

    goto :goto_7

    .line 33
    :catch_11
    move-exception v0

    goto :goto_e
.end method

.method public c()V
    .registers 1

    .prologue
    .line 45
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 46
    return-void
.end method
