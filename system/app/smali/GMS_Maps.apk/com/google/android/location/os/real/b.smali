.class Lcom/google/android/location/os/real/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/l;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/google/android/location/c/l;

.field private final c:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    .line 64
    iput-object p2, p0, Lcom/google/android/location/os/real/b;->b:Lcom/google/android/location/c/l;

    .line 66
    const-string v0, "power"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 67
    const-string v1, "AsyncCollectorListener"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    .line 71
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/b;)Lcom/google/android/location/c/l;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->b:Lcom/google/android/location/c/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/os/real/b;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 319
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$10;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$10;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 326
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 114
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$13;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/os/real/b$13;-><init>(Lcom/google/android/location/os/real/b;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 121
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 127
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$14;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$14;-><init>(Lcom/google/android/location/os/real/b;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 134
    return-void
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .registers 4
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 139
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$15;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$15;-><init>(Lcom/google/android/location/os/real/b;Lcom/google/android/location/c/H;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 146
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 163
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$17;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$17;-><init>(Lcom/google/android/location/os/real/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 170
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 151
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$16;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$16;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 158
    return-void
.end method

.method public a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 77
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$1;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 90
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/location/os/real/b$8;-><init>(Lcom/google/android/location/os/real/b;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    return-void
.end method

.method public a(ZZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 331
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$11;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/os/real/b$11;-><init>(Lcom/google/android/location/os/real/b;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 338
    return-void
.end method

.method public a_(I)V
    .registers 4
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 175
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$18;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/os/real/b$18;-><init>(Lcom/google/android/location/os/real/b;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 182
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 211
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$4;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$4;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 218
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 199
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$3;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$3;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 206
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 283
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$6;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$6;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 290
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 271
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$5;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$5;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 278
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 187
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$2;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$2;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 194
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 295
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$7;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$7;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 302
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 307
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$9;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$9;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 314
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 102
    iget-object v0, p0, Lcom/google/android/location/os/real/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/os/real/b$12;

    invoke-direct {v1, p0}, Lcom/google/android/location/os/real/b$12;-><init>(Lcom/google/android/location/os/real/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 109
    return-void
.end method
