.class final Lcom/google/android/location/os/real/c$a;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/c;

.field private final b:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/c;)V
    .registers 4
    .parameter

    .prologue
    .line 559
    iput-object p1, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 561
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$a;->b:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 559
    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c$a;-><init>(Lcom/google/android/location/os/real/c;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x16

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    .line 569
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 571
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 572
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0, v4, v2, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    .line 669
    :cond_16
    :goto_16
    return-void

    .line 573
    :cond_17
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 574
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0, v4, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto :goto_16

    .line 575
    :cond_25
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 579
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 580
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_16

    .line 581
    :cond_3f
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 585
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 586
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_16

    .line 587
    :cond_59
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_74

    .line 591
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 592
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_16

    .line 593
    :cond_74
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8f

    .line 597
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 598
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x9

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto :goto_16

    .line 599
    :cond_8f
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ab

    .line 603
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 604
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 605
    :cond_ab
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c7

    .line 609
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 610
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 611
    :cond_c7
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e3

    .line 615
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 616
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 617
    :cond_e3
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ff

    .line 621
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 622
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 623
    :cond_ff
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11b

    .line 627
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 628
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 629
    :cond_11b
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_137

    .line 633
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 634
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 635
    :cond_137
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_153

    .line 639
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 640
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16

    .line 641
    :cond_153
    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_174

    .line 642
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 643
    iget-object v2, p0, Lcom/google/android/location/os/real/c$a;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2

    .line 644
    if-eqz v2, :cond_16

    .line 645
    invoke-static {v0, v1, v2}, Lcom/google/android/location/os/real/i;->a(JLjava/util/List;)Lcom/google/android/location/os/real/i;

    move-result-object v0

    .line 646
    iget-object v1, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v2, 0x12

    invoke-static {v1, v2, v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V

    goto/16 :goto_16

    .line 648
    :cond_174
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_198

    .line 649
    const-string v0, "wifi_state"

    const/4 v3, 0x4

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 652
    const/4 v3, 0x3

    if-ne v0, v3, :cond_192

    move v0, v1

    .line 659
    :goto_187
    iget-object v3, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v4, 0x13

    if-eqz v0, :cond_196

    :goto_18d
    invoke-static {v3, v4, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto/16 :goto_16

    .line 654
    :cond_192
    if-ne v0, v1, :cond_16

    move v0, v2

    .line 655
    goto :goto_187

    :cond_196
    move v1, v2

    .line 659
    goto :goto_18d

    .line 660
    :cond_198
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1ad

    .line 661
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x14

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V

    goto/16 :goto_16

    .line 662
    :cond_1ad
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c6

    .line 663
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v3, 0x18

    invoke-static {p1}, Lcom/google/android/location/os/real/c;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1c4

    :goto_1bf
    invoke-static {v0, v3, v1, v5}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;III)V

    goto/16 :goto_16

    :cond_1c4
    move v1, v2

    goto :goto_1bf

    .line 664
    :cond_1c6
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 665
    iget-object v0, p0, Lcom/google/android/location/os/real/c$a;->a:Lcom/google/android/location/os/real/c;

    const/16 v1, 0x1b

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;I)V

    goto/16 :goto_16
.end method
