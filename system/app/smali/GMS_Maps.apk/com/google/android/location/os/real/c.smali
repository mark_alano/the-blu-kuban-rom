.class Lcom/google/android/location/os/real/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/c$1;,
        Lcom/google/android/location/os/real/c$f;,
        Lcom/google/android/location/os/real/c$a;,
        Lcom/google/android/location/os/real/c$b;,
        Lcom/google/android/location/os/real/c$e;,
        Lcom/google/android/location/os/real/c$d;,
        Lcom/google/android/location/os/real/c$c;
    }
.end annotation


# instance fields
.field private A:Z

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field private final l:Landroid/content/Context;

.field private final m:Lcom/google/android/location/os/e;

.field private final n:Lcom/google/android/location/os/real/c$d;

.field private final o:Lcom/google/android/location/os/real/c$d;

.field private final p:Lcom/google/android/location/os/real/c$e;

.field private final q:Lcom/google/android/location/os/real/c$b;

.field private final r:Lcom/google/android/location/os/real/c$f;

.field private final s:Ljava/lang/Thread;

.field private final t:Landroid/os/PowerManager$WakeLock;

.field private final u:Lcom/google/android/location/os/h;

.field private final v:Ljava/lang/Object;

.field private w:Z

.field private x:Landroid/os/Handler;

.field private y:Lcom/google/android/location/os/real/c$a;

.field private z:Lcom/google/android/location/os/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/os/e;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    .line 130
    iput-boolean v1, p0, Lcom/google/android/location/os/real/c;->w:Z

    .line 135
    iput-boolean v1, p0, Lcom/google/android/location/os/real/c;->A:Z

    .line 784
    iput-object p1, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    .line 785
    iput-object p2, p0, Lcom/google/android/location/os/real/c;->u:Lcom/google/android/location/os/h;

    .line 786
    iput-object p3, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    .line 793
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nlp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_LOCATOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    .line 795
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_ACTIVE_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    .line 796
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_BURST_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    .line 797
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_PASSIVE_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    .line 798
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_CACHE_UPDATER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    .line 799
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_CALIBRATION_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    .line 800
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_S_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    .line 801
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_SENSOR_UPLOADER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    .line 802
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_ACTIVITY_DETECTION"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    .line 803
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".ALARM_WAKEUP_IN_OUT_DOOR_COLLECTOR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    .line 804
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ALARM_WAKEUP_BURST_COLLECTION_TRIGGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    .line 806
    new-instance v0, Lcom/google/android/location/os/real/c$d;

    const/16 v1, 0x11

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/c$d;-><init>(Lcom/google/android/location/os/real/c;I)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    .line 807
    new-instance v0, Lcom/google/android/location/os/real/c$d;

    const/16 v1, 0x15

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/c$d;-><init>(Lcom/google/android/location/os/real/c;I)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    .line 808
    new-instance v0, Lcom/google/android/location/os/real/c$e;

    invoke-direct {v0, p0, v3}, Lcom/google/android/location/os/real/c$e;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->p:Lcom/google/android/location/os/real/c$e;

    .line 809
    new-instance v0, Lcom/google/android/location/os/real/c$f;

    invoke-direct {v0, p0, v3}, Lcom/google/android/location/os/real/c$f;-><init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    .line 810
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    const-string v2, "NetworkLocationCallbackRunner"

    invoke-direct {v0, v3, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    .line 811
    new-instance v1, Lcom/google/android/location/os/real/c$b;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    invoke-direct {v1, v0}, Lcom/google/android/location/os/real/c$b;-><init>(Landroid/location/LocationManager;)V

    iput-object v1, p0, Lcom/google/android/location/os/real/c;->q:Lcom/google/android/location/os/real/c$b;

    .line 814
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 815
    const-string v1, "NetworkLocationCallbackRunner"

    invoke-virtual {v0, v4, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    .line 819
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 820
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$a;)Lcom/google/android/location/os/real/c$a;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    return-object p1
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 1047
    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1048
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_9

    .line 1049
    monitor-exit v1

    .line 1053
    :goto_8
    return-void

    .line 1051
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1052
    monitor-exit v1

    goto :goto_8

    :catchall_14
    move-exception v0

    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    throw v0
.end method

.method private a(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1056
    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1057
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_9

    .line 1058
    monitor-exit v1

    .line 1062
    :goto_8
    return-void

    .line 1060
    :cond_9
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1061
    monitor-exit v1

    goto :goto_8

    :catchall_14
    move-exception v0

    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    throw v0
.end method

.method private a(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1065
    iget-object v1, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1066
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v0, :cond_d

    .line 1067
    :cond_b
    monitor-exit v1

    .line 1071
    :goto_c
    return-void

    .line 1069
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1070
    monitor-exit v1

    goto :goto_c

    :catchall_18
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method private a(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 426
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-nez v2, :cond_18

    .line 429
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    invoke-virtual {v0, v4, v4, v1}, Lcom/google/android/location/os/e;->a(ZZI)V

    .line 430
    invoke-interface {p3, v4, v4, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    .line 441
    :cond_17
    :goto_17
    return-void

    .line 431
    :cond_18
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_27

    .line 433
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/location/os/e;->a(ZZI)V

    .line 434
    invoke-interface {p3, v4, v5, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    goto :goto_17

    .line 435
    :cond_27
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v5, :cond_17

    .line 437
    invoke-virtual {p2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 438
    iget-object v3, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    if-nez v2, :cond_3f

    move v0, v1

    :goto_36
    invoke-virtual {v3, v5, v4, v0}, Lcom/google/android/location/os/e;->a(ZZI)V

    .line 439
    if-nez v2, :cond_44

    :goto_3b
    invoke-interface {p3, v5, v4, v1}, Lcom/google/android/location/os/a;->a(ZZI)V

    goto :goto_17

    .line 438
    :cond_3f
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    goto :goto_36

    .line 439
    :cond_44
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v1

    goto :goto_3b
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;III)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/os/real/c;->a(III)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;ILjava/lang/Object;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/c;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/os/real/c;->a(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;)Z
    .registers 2
    .parameter

    .prologue
    .line 60
    invoke-static {p0}, Lcom/google/android/location/os/real/c;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->m:Lcom/google/android/location/os/e;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1037
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1038
    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_f

    :goto_e
    return v0

    :cond_f
    move v0, v1

    goto :goto_e
.end method

.method static synthetic c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->z:Lcom/google/android/location/os/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$e;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->p:Lcom/google/android/location/os/real/c$e;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/os/real/c;)Z
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->A:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->q:Lcom/google/android/location/os/real/c$b;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/os/real/c;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/os/real/c;)Z
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->w:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$a;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/h;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->u:Lcom/google/android/location/os/h;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 877
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_5} :catch_14

    .line 883
    :goto_5
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 884
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_5

    .line 886
    :cond_13
    return-void

    .line 879
    :catch_14
    move-exception v0

    goto :goto_0
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 994
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(III)V

    .line 995
    return-void
.end method

.method public a(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 849
    const/4 v1, 0x3

    if-eqz p2, :cond_8

    const/4 v0, 0x1

    :goto_4
    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/location/os/real/c;->a(III)V

    .line 850
    return-void

    .line 849
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .registers 3
    .parameter

    .prologue
    .line 978
    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 979
    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 986
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 987
    return-void
.end method

.method public a(Lcom/google/android/location/d/a;Ljava/lang/String;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    .line 944
    iget-boolean v0, p0, Lcom/google/android/location/os/real/c;->A:Z

    if-ne v0, p3, :cond_8

    .line 958
    :goto_7
    return-void

    .line 947
    :cond_8
    iput-boolean p3, p0, Lcom/google/android/location/os/real/c;->A:Z

    .line 948
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    .line 949
    if-eqz p3, :cond_20

    .line 950
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    invoke-virtual {p1, p2, v0, v1}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    .line 951
    const-string v2, "gps"

    iget-object v6, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_7

    .line 954
    :cond_20
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/os/real/c;->o:Lcom/google/android/location/os/real/c$d;

    invoke-virtual {p1, p2, v0, v1}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    .line 955
    const-string v2, "passive"

    iget-object v6, p0, Lcom/google/android/location/os/real/c;->n:Lcom/google/android/location/os/real/c$d;

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_7
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .registers 3
    .parameter

    .prologue
    .line 1003
    const/16 v0, 0x1e

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 1004
    return-void
.end method

.method public a(Lcom/google/android/location/os/a;)V
    .registers 4
    .parameter

    .prologue
    .line 826
    iput-object p1, p0, Lcom/google/android/location/os/real/c;->z:Lcom/google/android/location/os/a;

    .line 827
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->s:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 831
    iget-object v1, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    monitor-enter v1

    .line 832
    :goto_a
    :try_start_a
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    iget-boolean v0, v0, Lcom/google/android/location/os/real/c$f;->a:Z
    :try_end_e
    .catchall {:try_start_a .. :try_end_e} :catchall_1a

    if-nez v0, :cond_18

    .line 834
    :try_start_10
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->r:Lcom/google/android/location/os/real/c$f;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_1a
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_15} :catch_16

    goto :goto_a

    .line 835
    :catch_16
    move-exception v0

    goto :goto_a

    .line 839
    :cond_18
    :try_start_18
    monitor-exit v1

    .line 840
    return-void

    .line 839
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_18 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 971
    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 972
    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1018
    const/16 v0, 0x23

    invoke-static {p1, p2}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 1019
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 910
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 911
    return-void
.end method

.method public a(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 858
    iget-object v2, p0, Lcom/google/android/location/os/real/c;->v:Ljava/lang/Object;

    monitor-enter v2

    .line 859
    :try_start_5
    iget-boolean v3, p0, Lcom/google/android/location/os/real/c;->w:Z

    if-eqz v3, :cond_b

    .line 860
    monitor-exit v2

    .line 869
    :goto_a
    return-void

    .line 863
    :cond_b
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/location/os/real/c;->w:Z

    .line 864
    iget-object v3, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    if-eqz v3, :cond_19

    .line 865
    iget-object v3, p0, Lcom/google/android/location/os/real/c;->l:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/c;->y:Lcom/google/android/location/os/real/c$a;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 867
    :cond_19
    iget-object v3, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    const/4 v4, 0x1

    if-eqz p1, :cond_2b

    :goto_1e
    const/4 v1, 0x0

    invoke-static {v3, v4, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 868
    monitor-exit v2

    goto :goto_a

    :catchall_28
    move-exception v0

    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_5 .. :try_end_2a} :catchall_28

    throw v0

    :cond_2b
    move v0, v1

    .line 867
    goto :goto_1e
.end method

.method public a(ZLjava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1033
    const/16 v0, 0x22

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 1034
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 901
    const/4 v0, 0x4

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/location/os/real/c;->a(III)V

    .line 902
    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 920
    const/16 v0, 0x1a

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 921
    return-void
.end method

.method public b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 894
    const/16 v1, 0x19

    if-eqz p1, :cond_a

    const/4 v0, 0x1

    :goto_5
    const/4 v2, -0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/location/os/real/c;->a(III)V

    .line 895
    return-void

    .line 894
    :cond_a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public c()Landroid/os/Handler;
    .registers 2

    .prologue
    .line 964
    iget-object v0, p0, Lcom/google/android/location/os/real/c;->x:Landroid/os/Handler;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 931
    const/16 v0, 0x1c

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 932
    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 940
    const/16 v0, 0x17

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/real/c;->a(ILjava/lang/Object;)V

    .line 941
    return-void
.end method
