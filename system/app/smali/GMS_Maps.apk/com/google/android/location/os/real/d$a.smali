.class Lcom/google/android/location/os/real/d$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/d;

.field private final b:Lcom/google/android/location/os/real/d$b;

.field private final c:Ljava/lang/String;

.field private d:Lcom/google/android/location/os/real/e;

.field private volatile e:J


# direct methods
.method constructor <init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    .line 350
    iput-object p2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    .line 351
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->c:Ljava/lang/String;

    .line 352
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 355
    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1c

    .line 365
    const-string v0, "GlsClient"

    :goto_f
    return-object v0

    .line 357
    :pswitch_10
    const-string v0, "GlsClient-query"

    goto :goto_f

    .line 359
    :pswitch_13
    const-string v0, "GlsClient-upload"

    goto :goto_f

    .line 361
    :pswitch_16
    const-string v0, "GlsClient-model-query"

    goto :goto_f

    .line 363
    :pswitch_19
    const-string v0, "GlsClient-device-location-query"

    goto :goto_f

    .line 355
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_19
    .end packed-switch
.end method

.method private b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 434
    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_38

    .line 443
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :pswitch_2e
    const-string v0, "g:loc/ql"

    .line 441
    :goto_30
    return-object v0

    .line 439
    :pswitch_31
    const-string v0, "g:loc/ul"

    goto :goto_30

    .line 441
    :pswitch_34
    const-string v0, "g:loc/dl"

    goto :goto_30

    .line 434
    nop

    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_31
        :pswitch_2e
        :pswitch_34
    .end packed-switch
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 462
    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_58

    .line 476
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :pswitch_2f
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 478
    :goto_38
    return-void

    .line 467
    :pswitch_39
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_38

    .line 470
    :pswitch_43
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_38

    .line 473
    :pswitch_4d
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_38

    .line 462
    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_39
        :pswitch_43
        :pswitch_4d
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 484
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eq p1, v1, :cond_11

    .line 485
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_e
    .catchall {:try_start_2 .. :try_end_e} :catchall_e

    .line 484
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 489
    :cond_11
    if-eqz p2, :cond_43

    .line 490
    :try_start_13
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v1

    .line 491
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_43

    .line 492
    sget-object v2, Lcom/google/android/location/j/a;->ai:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 493
    invoke-static {v0}, Lcom/google/android/location/os/real/d;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 496
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->c()Z

    move-result v1

    if-eqz v1, :cond_43

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 497
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_43
    .catchall {:try_start_13 .. :try_end_43} :catchall_e
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_43} :catch_b0

    .line 504
    :cond_43
    :goto_43
    :try_start_43
    sget-object v1, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_b2

    .line 521
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :pswitch_71
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 523
    :cond_7a
    :goto_7a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;
    :try_end_7d
    .catchall {:try_start_43 .. :try_end_7d} :catchall_e

    .line 524
    monitor-exit p0

    return-void

    .line 509
    :pswitch_7f
    :try_start_7f
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 510
    iget-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7a

    .line 511
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/a;->a(J)V

    goto :goto_7a

    .line 515
    :pswitch_9c
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_7a

    .line 518
    :pswitch_a6
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_af
    .catchall {:try_start_7f .. :try_end_af} :catchall_e

    goto :goto_7a

    .line 501
    :catch_b0
    move-exception v1

    goto :goto_43

    .line 504
    :pswitch_data_b2
    .packed-switch 0x1
        :pswitch_71
        :pswitch_7f
        :pswitch_9c
        :pswitch_a6
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 528
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eq p1, v0, :cond_10

    .line 529
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    .line 528
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 532
    :cond_10
    :try_start_10
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_2d

    instance-of v0, p2, Lcom/google/android/location/h/e;

    if-eqz v0, :cond_2d

    .line 536
    iget-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2d

    .line 537
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/a;->a(J)V

    .line 540
    :cond_2d
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V

    .line 541
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;
    :try_end_33
    .catchall {:try_start_10 .. :try_end_33} :catchall_d

    .line 542
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 370
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eqz v0, :cond_10

    .line 371
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Gls request still outstanding."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    .line 370
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 374
    :cond_10
    const/4 v0, 0x1

    :try_start_11
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 378
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->c()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 379
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 381
    :cond_30
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_60

    .line 382
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_60

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->b(Lcom/google/android/location/os/real/d;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 384
    const/16 v0, 0x63

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 385
    if-nez v0, :cond_5b

    .line 386
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->U:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 387
    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 389
    :cond_5b
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1, v0}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 394
    :cond_60
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_65
    .catchall {:try_start_11 .. :try_end_65} :catchall_d

    .line 396
    :try_start_65
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_d
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_68} :catch_ad

    .line 403
    :try_start_68
    new-instance v1, Lcom/google/android/location/os/real/e;

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/os/real/e;-><init>(Ljava/lang/String;I[B)V

    iput-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    .line 405
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    invoke-virtual {v0, p0}, Lcom/google/android/location/os/real/e;->a(Lcom/google/android/location/h/b/m$a;)V

    .line 406
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/e;->b(I)V
    :try_end_83
    .catchall {:try_start_68 .. :try_end_83} :catchall_d

    .line 408
    :try_start_83
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    monitor-enter v1
    :try_end_86
    .catchall {:try_start_83 .. :try_end_86} :catchall_d
    .catch Ljava/io/IOException; {:try_start_83 .. :try_end_86} :catch_b5

    .line 410
    :try_start_86
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/e;->c()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/location/os/real/d$a;->e:J

    .line 411
    monitor-exit v1
    :try_end_90
    .catchall {:try_start_86 .. :try_end_90} :catchall_b2

    .line 419
    :goto_90
    :try_start_90
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_bb

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/i/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_bb

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    .line 422
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V
    :try_end_ab
    .catchall {:try_start_90 .. :try_end_ab} :catchall_d

    .line 431
    :goto_ab
    monitor-exit p0

    return-void

    .line 397
    :catch_ad
    move-exception v0

    .line 399
    :try_start_ae
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V
    :try_end_b1
    .catchall {:try_start_ae .. :try_end_b1} :catchall_d

    goto :goto_ab

    .line 411
    :catchall_b2
    move-exception v0

    :try_start_b3
    monitor-exit v1
    :try_end_b4
    .catchall {:try_start_b3 .. :try_end_b4} :catchall_b2

    :try_start_b4
    throw v0
    :try_end_b5
    .catchall {:try_start_b4 .. :try_end_b5} :catchall_d
    .catch Ljava/io/IOException; {:try_start_b4 .. :try_end_b5} :catch_b5

    .line 412
    :catch_b5
    move-exception v0

    .line 414
    const-wide/16 v0, 0x0

    :try_start_b8
    iput-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    goto :goto_90

    .line 430
    :cond_bb
    invoke-static {}, Lcom/google/android/location/d/c;->a()Lcom/google/android/location/d/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/d/c;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_c5
    .catchall {:try_start_b8 .. :try_end_c5} :catchall_d

    goto :goto_ab
.end method
