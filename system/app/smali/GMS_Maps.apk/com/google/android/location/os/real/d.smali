.class public Lcom/google/android/location/os/real/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/d$1;,
        Lcom/google/android/location/os/real/d$a;,
        Lcom/google/android/location/os/real/d$b;
    }
.end annotation


# static fields
.field static a:Z

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static f:Ljava/io/File;


# instance fields
.field private e:Z

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/location/os/real/c;

.field private final i:Lcom/google/android/location/os/h;

.field private final j:Lcom/google/android/location/os/real/d$a;

.field private final k:Lcom/google/android/location/os/real/d$a;

.field private final l:Lcom/google/android/location/os/real/d$a;

.field private final m:Lcom/google/android/location/os/real/d$a;

.field private final n:Lcom/google/android/location/i/a;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 68
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/location/os/real/d;->a:Z

    .line 79
    const-string v0, ""

    sput-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/os/real/c;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/os/real/d;->e:Z

    .line 110
    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->j:Lcom/google/android/location/os/real/d$a;

    .line 111
    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->c:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->k:Lcom/google/android/location/os/real/d$a;

    .line 113
    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->l:Lcom/google/android/location/os/real/d$a;

    .line 114
    new-instance v0, Lcom/google/android/location/os/real/d$a;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->d:Lcom/google/android/location/os/real/d$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/d$a;-><init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/d;->m:Lcom/google/android/location/os/real/d$a;

    .line 169
    iput-object p1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    .line 170
    iput-object p2, p0, Lcom/google/android/location/os/real/d;->i:Lcom/google/android/location/os/h;

    .line 171
    iput-object p4, p0, Lcom/google/android/location/os/real/d;->h:Lcom/google/android/location/os/real/c;

    .line 172
    iput-object p3, p0, Lcom/google/android/location/os/real/d;->n:Lcom/google/android/location/i/a;

    .line 173
    invoke-static {p1}, Lcom/google/android/location/os/real/d;->a(Landroid/content/Context;)V

    .line 174
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->i:Lcom/google/android/location/os/h;

    return-object v0
.end method

.method static synthetic a(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/location/os/real/d;->b(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a()V
    .registers 4

    .prologue
    .line 217
    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_3
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_13

    .line 219
    :try_start_c
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_f} :catch_11

    .line 223
    :goto_f
    monitor-exit v1

    return-void

    .line 220
    :catch_11
    move-exception v0

    goto :goto_f

    .line 217
    :catchall_13
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 124
    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/internal/e;

    invoke-static {v0, p0}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    .line 125
    iget v0, v0, Lcom/google/android/location/internal/d;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    .line 128
    invoke-static {p0}, Lcom/google/android/location/os/real/d;->b(Landroid/content/Context;)Lcom/google/android/location/h/h$a;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/d/c;->a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V

    .line 130
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    if-eqz v0, :cond_3b

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    .line 136
    :goto_33
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;
    :try_end_39
    .catchall {:try_start_3 .. :try_end_39} :catchall_40

    .line 137
    monitor-exit v1

    return-void

    .line 133
    :cond_3b
    :try_start_3b
    const-string v0, "android"

    sput-object v0, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;
    :try_end_3f
    .catchall {:try_start_3b .. :try_end_3f} :catchall_40

    goto :goto_33

    .line 124
    :catchall_40
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/os/real/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/d;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static declared-synchronized a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 247
    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_15

    .line 248
    sget-object v0, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_38

    move-result v0

    if-nez v0, :cond_15

    .line 265
    :goto_13
    monitor-exit v1

    return-void

    .line 254
    :cond_15
    :try_start_15
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 255
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 256
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 257
    invoke-virtual {v0, p0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 258
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 259
    sput-object p0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_35
    .catchall {:try_start_15 .. :try_end_35} :catchall_38
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_35} :catch_36
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_35} :catch_3b

    goto :goto_13

    .line 260
    :catch_36
    move-exception v0

    goto :goto_13

    .line 247
    :catchall_38
    move-exception v0

    monitor-exit v1

    throw v0

    .line 262
    :catch_3b
    move-exception v0

    goto :goto_13
.end method

.method private static b(Landroid/content/Context;)Lcom/google/android/location/h/h$a;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    .line 145
    const-string v0, "https://www.google.com/loc/m/api"

    .line 146
    sget-boolean v2, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v2, :cond_14

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "url:google_location_server"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    :cond_14
    if-eqz v1, :cond_38

    .line 149
    const-string v2, " "

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 150
    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_38

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_38

    const/4 v2, 0x1

    aget-object v2, v1, v2

    const-string v3, "rewrite"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 151
    const/4 v0, 0x2

    aget-object v0, v1, v0

    .line 155
    :cond_38
    new-instance v1, Lcom/google/android/location/h/h$a;

    invoke-direct {v1}, Lcom/google/android/location/h/h$a;-><init>()V

    .line 156
    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->a(Ljava/lang/String;)V

    .line 157
    const-string v0, "location"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->b(Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->c(Ljava/lang/String;)V

    .line 159
    const-string v0, "android"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->d(Ljava/lang/String;)V

    .line 160
    const-string v0, "gmm"

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/h$a;->e(Ljava/lang/String;)V

    .line 161
    return-object v1
.end method

.method private static b(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 279
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->W:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 280
    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/location/os/real/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 281
    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/os/real/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 284
    if-eqz p0, :cond_23

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_23

    .line 285
    const/4 v1, 0x5

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 289
    :cond_23
    invoke-static {}, Lcom/google/android/location/os/real/d;->b()Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    .line 291
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 294
    :cond_31
    return-object v0
.end method

.method static declared-synchronized b()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 226
    const-class v1, Lcom/google/android/location/os/real/d;

    monitor-enter v1

    :try_start_4
    sget-object v2, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 227
    sget-object v0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_30

    .line 242
    :goto_a
    monitor-exit v1

    return-object v0

    .line 230
    :cond_c
    :try_start_c
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/google/android/location/os/real/d;->f:Ljava/io/File;

    const-string v4, "nlp_GlsPlatformKey"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 231
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 232
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 233
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 234
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    .line 235
    sput-object v3, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;

    .line 236
    sget-object v0, Lcom/google/android/location/os/real/d;->d:Ljava/lang/String;
    :try_end_2f
    .catchall {:try_start_c .. :try_end_2f} :catchall_30
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_2f} :catch_35
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_2f} :catch_33

    goto :goto_a

    .line 226
    :catchall_30
    move-exception v0

    monitor-exit v1

    throw v0

    .line 240
    :catch_33
    move-exception v2

    goto :goto_a

    .line 237
    :catch_35
    move-exception v2

    goto :goto_a
.end method

.method static synthetic b(Lcom/google/android/location/os/real/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/location/os/real/d;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->n:Lcom/google/android/location/i/a;

    return-object v0
.end method

.method private c()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 303
    sget-boolean v1, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_provider_debug"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    :cond_11
    const-string v1, "verbose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_21
    const/4 v0, 0x1

    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method static synthetic d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->h:Lcom/google/android/location/os/real/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/location/os/real/d;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x3

    .line 268
    if-eqz p0, :cond_1d

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 271
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 272
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 273
    invoke-static {v0}, Lcom/google/android/location/os/real/d;->a(Ljava/lang/String;)V

    .line 276
    :cond_1d
    return-void
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 320
    iget-boolean v2, p0, Lcom/google/android/location/os/real/d;->e:Z

    if-eqz v2, :cond_d

    .line 321
    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 322
    iput-boolean v0, p0, Lcom/google/android/location/os/real/d;->e:Z

    .line 325
    :cond_d
    sget-boolean v2, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network_location_provider_debug"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327
    :cond_1d
    if-eqz v1, :cond_3d

    .line 328
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 329
    iget-object v1, p0, Lcom/google/android/location/os/real/d;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 330
    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    :goto_30
    if-ge v0, v2, :cond_3d

    aget-object v3, v1, v0

    .line 331
    const/4 v4, 0x5

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_30

    .line 335
    :cond_3d
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->j:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 184
    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->l:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 194
    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->k:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 204
    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/location/os/real/d;->m:Lcom/google/android/location/os/real/d$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 214
    return-void
.end method
