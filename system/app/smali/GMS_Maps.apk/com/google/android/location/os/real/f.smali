.class public Lcom/google/android/location/os/real/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static final a(Landroid/telephony/TelephonyManager;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 195
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v4

    .line 196
    if-eq v4, v3, :cond_c

    if-ne v4, v1, :cond_e

    :cond_c
    move v0, v3

    .line 214
    :cond_d
    :goto_d
    return v0

    .line 199
    :cond_e
    if-eq v4, v2, :cond_20

    const/16 v3, 0x8

    if-eq v4, v3, :cond_20

    const/16 v3, 0x9

    if-eq v4, v3, :cond_20

    const/16 v3, 0xa

    if-eq v4, v3, :cond_20

    const/16 v3, 0xf

    if-ne v4, v3, :cond_22

    :cond_20
    move v0, v2

    .line 204
    goto :goto_d

    .line 205
    :cond_22
    if-eq v4, v0, :cond_31

    const/4 v2, 0x5

    if-eq v4, v2, :cond_31

    const/4 v2, 0x6

    if-eq v4, v2, :cond_31

    const/16 v2, 0xc

    if-eq v4, v2, :cond_31

    const/4 v2, 0x7

    if-ne v4, v2, :cond_33

    :cond_31
    move v0, v1

    .line 210
    goto :goto_d

    .line 211
    :cond_33
    const/16 v1, 0xd

    if-eq v4, v1, :cond_d

    .line 214
    const/4 v0, -0x1

    goto :goto_d
.end method

.method public static a(Landroid/telephony/TelephonyManager;Landroid/telephony/CellLocation;IJ)Lcom/google/android/location/e/e;
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    const/4 v10, -0x1

    .line 51
    invoke-static/range {p0 .. p0}, Lcom/google/android/location/os/real/f;->a(Landroid/telephony/TelephonyManager;)I

    move-result v2

    .line 52
    const/4 v9, -0x1

    .line 53
    const/4 v6, -0x1

    .line 54
    const/4 v4, -0x1

    .line 55
    const/4 v3, -0x1

    .line 56
    const v16, 0x7fffffff

    .line 57
    const v17, 0x7fffffff

    .line 58
    const/4 v7, -0x1

    .line 59
    const/4 v15, -0x1

    .line 60
    const/4 v11, 0x0

    .line 62
    move-object/from16 v0, p1

    instance-of v5, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v5, :cond_d2

    .line 63
    check-cast p1, Landroid/telephony/gsm/GsmCellLocation;

    .line 64
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v9

    .line 65
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v6

    .line 66
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/location/os/real/j;->a(Landroid/telephony/gsm/GsmCellLocation;)I

    move-result v10

    .line 71
    const/4 v5, 0x4

    if-ne v2, v5, :cond_39

    .line 72
    const/high16 v2, 0x1

    if-lt v6, v2, :cond_cf

    const v2, 0xfffffff

    if-gt v6, v2, :cond_cf

    .line 73
    const/4 v2, 0x3

    :cond_39
    :goto_39
    move v5, v2

    move v2, v3

    .line 93
    :goto_3b
    const/4 v3, 0x2

    if-eq v5, v3, :cond_142

    .line 95
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    .line 96
    if-eqz v3, :cond_142

    const-string v8, ""

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_142

    .line 100
    const/4 v8, 0x0

    const/4 v12, 0x3

    :try_start_4e
    invoke-virtual {v3, v8, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 101
    const/4 v12, 0x3

    invoke-virtual {v3, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 102
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 103
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_5e} :catch_f2

    move-result v2

    .line 113
    :goto_5f
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v4

    .line 114
    if-eqz v4, :cond_13d

    const-string v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_13d

    .line 119
    const/4 v8, 0x0

    const/4 v12, 0x3

    :try_start_6f
    invoke-virtual {v4, v8, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 120
    const/4 v12, 0x3

    invoke-virtual {v4, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 121
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 122
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_6f .. :try_end_7f} :catch_f6

    move-result v15

    move v8, v2

    move v7, v3

    .line 139
    :goto_82
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v2

    .line 140
    sget-object v12, Lcom/google/android/location/e/e;->a:Ljava/util/List;

    .line 142
    if-eqz v2, :cond_11a

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_11a

    .line 143
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 144
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 146
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9d
    :goto_9d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_116

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/NeighboringCellInfo;

    .line 147
    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getNetworkType()I

    move-result v12

    .line 148
    packed-switch v12, :pswitch_data_14c

    :pswitch_b0
    goto :goto_9d

    .line 151
    :pswitch_b1
    const/4 v12, 0x1

    if-ne v5, v12, :cond_9d

    .line 154
    new-instance v12, Lcom/google/android/location/e/e$a;

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v13

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v18

    const/16 v19, -0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v12, v13, v0, v1, v2}, Lcom/google/android/location/e/e$a;-><init>(IIII)V

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9d

    .line 75
    :cond_cf
    const/4 v2, 0x1

    goto/16 :goto_39

    .line 78
    :cond_d2
    move-object/from16 v0, p1

    instance-of v5, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v5, :cond_147

    .line 79
    check-cast p1, Landroid/telephony/cdma/CdmaCellLocation;

    .line 80
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v9

    .line 81
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v6

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v2

    .line 84
    const/4 v4, 0x0

    .line 85
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v16

    .line 86
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v17

    .line 89
    const/4 v5, 0x2

    goto/16 :goto_3b

    .line 108
    :catch_f2
    move-exception v3

    move v3, v4

    goto/16 :goto_5f

    .line 127
    :catch_f6
    move-exception v4

    move v14, v7

    move v8, v2

    move v7, v3

    goto :goto_82

    .line 161
    :pswitch_fb
    const/4 v12, 0x3

    if-ne v5, v12, :cond_9d

    .line 164
    new-instance v12, Lcom/google/android/location/e/e$a;

    const/4 v13, -0x1

    const/16 v18, -0x1

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v19

    invoke-virtual {v2}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v12, v13, v0, v1, v2}, Lcom/google/android/location/e/e$a;-><init>(IIII)V

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9d

    .line 170
    :cond_116
    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v12

    .line 174
    :cond_11a
    invoke-virtual/range {p0 .. p0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v13

    .line 175
    if-eqz v13, :cond_13b

    const-string v2, ""

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13b

    .line 179
    :goto_128
    new-instance v2, Lcom/google/android/location/e/b;

    move-wide/from16 v3, p3

    move/from16 v11, p2

    invoke-direct/range {v2 .. v17}, Lcom/google/android/location/e/b;-><init>(JIIIIIIILjava/util/List;Ljava/lang/String;IIII)V

    .line 183
    const-string v3, "RealCellState"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/location/k/a/a;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_13a

    .line 187
    :cond_13a
    return-object v2

    :cond_13b
    move-object v13, v11

    goto :goto_128

    :cond_13d
    move v14, v7

    move v8, v2

    move v7, v3

    goto/16 :goto_82

    :cond_142
    move v14, v7

    move v8, v2

    move v7, v4

    goto/16 :goto_82

    :cond_147
    move v5, v2

    move v2, v3

    goto/16 :goto_3b

    .line 148
    nop

    :pswitch_data_14c
    .packed-switch 0x1
        :pswitch_b1
        :pswitch_b1
        :pswitch_fb
        :pswitch_b0
        :pswitch_b0
        :pswitch_b0
        :pswitch_b0
        :pswitch_fb
        :pswitch_fb
        :pswitch_fb
    .end packed-switch
.end method
