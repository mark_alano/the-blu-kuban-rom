.class public Lcom/google/android/location/os/real/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/g;


# instance fields
.field private final a:Landroid/location/Location;

.field private final b:J

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/location/Location;JI)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-nez p1, :cond_d

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null location in RealLocation constructor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_d
    iput-object p1, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    .line 37
    iput-wide p2, p0, Lcom/google/android/location/os/real/g;->b:J

    .line 38
    iput p4, p0, Lcom/google/android/location/os/real/g;->c:I

    .line 39
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    return v0
.end method

.method public b()D
    .registers 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public c()D
    .registers 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/location/os/real/g;->c:I

    return v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    return v0
.end method

.method public f()J
    .registers 3

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/google/android/location/os/real/g;->b:J

    return-wide v0
.end method

.method public g()J
    .registers 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/location/os/real/g;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    return v0
.end method

.method public k()F
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    return v0
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    return v0
.end method

.method public m()D
    .registers 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public n()Landroid/location/Location;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    const-string v1, "RealLocation [location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    iget-object v1, p0, Lcom/google/android/location/os/real/g;->a:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, " satellites="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {p0}, Lcom/google/android/location/os/real/g;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/location/os/real/g;->j()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 119
    const-string v1, " bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-virtual {p0}, Lcom/google/android/location/os/real/g;->k()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 122
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/location/os/real/g;->l()Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 123
    const-string v1, " altitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lcom/google/android/location/os/real/g;->m()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 126
    :cond_3f
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
