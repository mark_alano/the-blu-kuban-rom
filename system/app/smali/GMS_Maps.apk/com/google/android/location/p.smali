.class public Lcom/google/android/location/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final E:Lcom/google/android/location/e/f;


# instance fields
.field A:J

.field final B:Lcom/google/android/location/g;

.field final C:Lcom/google/android/location/e/d;

.field final D:Lcom/google/android/location/h;

.field private F:Lcom/google/android/location/os/g;

.field final a:Lcom/google/android/location/os/i;

.field final b:Lcom/google/android/location/b/f;

.field final c:Lcom/google/android/location/x;

.field final d:Lcom/google/android/location/e/n;

.field final e:Lcom/google/android/location/b/g;

.field final f:Lcom/google/android/location/a/n;

.field g:J

.field h:J

.field final i:Lcom/google/android/location/g/e;

.field j:Lcom/google/android/location/e/t;

.field k:Z

.field l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field m:J

.field n:J

.field o:Lcom/google/android/location/e/E;

.field p:Z

.field q:J

.field r:J

.field s:Lcom/google/android/location/e/f;

.field t:J

.field u:Z

.field v:Z

.field w:J

.field x:Lcom/google/android/location/e/E;

.field y:Lcom/google/android/location/g/l;

.field z:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/location/e/f;

    invoke-direct {v0}, Lcom/google/android/location/e/f;-><init>()V

    sput-object v0, Lcom/google/android/location/p;->E:Lcom/google/android/location/e/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g/l;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/x;Lcom/google/android/location/e/d;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lcom/google/android/location/p;->g:J

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    .line 109
    new-instance v0, Lcom/google/android/location/e/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/e/t;-><init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V

    iput-object v0, p0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->k:Z

    .line 115
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->m:J

    .line 116
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->n:J

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->p:Z

    .line 121
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    .line 122
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->r:J

    .line 126
    new-instance v0, Lcom/google/android/location/e/f;

    invoke-direct {v0}, Lcom/google/android/location/e/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    .line 127
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->t:J

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->u:Z

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/p;->v:Z

    .line 132
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->w:J

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->z:J

    .line 140
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/p;->A:J

    .line 155
    iput-object p1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    .line 156
    iput-object p2, p0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    .line 157
    iput-object p3, p0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    .line 158
    iput-object p4, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    .line 159
    new-instance v0, Lcom/google/android/location/g/e;

    iget-object v1, p2, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v2, p2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-direct {v0, v1, v2, p4, p1}, Lcom/google/android/location/g/e;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;Lcom/google/android/location/g/l;Lcom/google/android/location/os/i;)V

    iput-object v0, p0, Lcom/google/android/location/p;->i:Lcom/google/android/location/g/e;

    .line 160
    iput-object p6, p0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    .line 161
    iput-object p7, p0, Lcom/google/android/location/p;->c:Lcom/google/android/location/x;

    .line 162
    iput-object p5, p0, Lcom/google/android/location/p;->B:Lcom/google/android/location/g;

    .line 163
    iput-object p8, p0, Lcom/google/android/location/p;->C:Lcom/google/android/location/e/d;

    .line 164
    new-instance v0, Lcom/google/android/location/a/n;

    iget-object v1, p2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-direct {v0, v1, p1}, Lcom/google/android/location/a/n;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V

    iput-object v0, p0, Lcom/google/android/location/p;->f:Lcom/google/android/location/a/n;

    .line 165
    new-instance v0, Lcom/google/android/location/h;

    invoke-direct {v0}, Lcom/google/android/location/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    .line 168
    return-void
.end method

.method static a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)D
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    .line 555
    iget-object v1, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    .line 556
    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L

    mul-double/2addr v2, v4

    .line 557
    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    int-to-double v4, v0

    sub-double/2addr v2, v4

    iget v0, v1, Lcom/google/android/location/e/w;->c:I

    int-to-double v0, v0

    sub-double v0, v2, v0

    .line 558
    iget-wide v2, p0, Lcom/google/android/location/e/o;->e:J

    iget-wide v4, p1, Lcom/google/android/location/e/o;->e:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 560
    const-wide/high16 v4, 0x3ff0

    cmpg-double v4, v0, v4

    if-ltz v4, :cond_2c

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_2f

    .line 562
    :cond_2c
    const-wide/16 v0, 0x0

    .line 565
    :goto_2e
    return-wide v0

    :cond_2f
    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_2e
.end method

.method static a(JLcom/google/android/location/os/g;Lcom/google/android/location/os/g;)Lcom/google/android/location/e/w;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/32 v4, 0x15f90

    const/4 v1, 0x0

    const-wide v8, 0x416312d000000000L

    .line 585
    .line 590
    if-eqz p2, :cond_77

    invoke-interface {p2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p0, v2

    cmp-long v0, v2, v4

    if-gez v0, :cond_77

    move-object v0, p2

    .line 595
    :goto_16
    if-eqz p3, :cond_75

    .line 596
    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p0, v2

    .line 598
    cmp-long v2, v2, v4

    if-gez v2, :cond_75

    .line 600
    invoke-interface {p3}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    .line 602
    if-eqz v0, :cond_38

    .line 604
    invoke-interface {v0}, Lcom/google/android/location/os/g;->a()F

    move-result v3

    .line 606
    float-to-double v4, v2

    float-to-double v2, v3

    const-wide v6, 0x3feccccccccccccdL

    mul-double/2addr v2, v6

    cmpg-double v2, v4, v2

    if-gez v2, :cond_75

    .line 617
    :cond_38
    :goto_38
    if-nez p3, :cond_3c

    move-object v0, v1

    .line 637
    :goto_3b
    return-object v0

    .line 621
    :cond_3c
    invoke-interface {p3}, Lcom/google/android/location/os/g;->a()F

    move-result v0

    .line 626
    float-to-double v1, v0

    const-wide v3, 0x408f400000000000L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_63

    .line 627
    const/high16 v0, 0x457a

    .line 634
    :goto_4c
    new-instance v1, Lcom/google/android/location/e/w;

    invoke-interface {p3}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v2

    mul-double/2addr v2, v8

    double-to-int v2, v2

    invoke-interface {p3}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    const/high16 v4, 0x447a

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/e/w;-><init>(III)V

    move-object v0, v1

    .line 637
    goto :goto_3b

    .line 628
    :cond_63
    float-to-double v0, v0

    const-wide v2, 0x40c3880000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_71

    .line 629
    const v0, 0x47435000

    goto :goto_4c

    .line 631
    :cond_71
    const v0, 0x47c35000

    goto :goto_4c

    :cond_75
    move-object p3, v0

    goto :goto_38

    :cond_77
    move-object v0, v1

    goto :goto_16
.end method

.method private a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 196
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v1

    .line 198
    if-eqz p1, :cond_14

    .line 199
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/location/e/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V

    .line 201
    :cond_14
    if-eqz p2, :cond_30

    .line 202
    invoke-virtual {p2, v1, v2, v3}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 203
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 205
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 208
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->w:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 210
    invoke-virtual {v1, v3, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 212
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 217
    :cond_30
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 218
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 219
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/location/p;->c:Lcom/google/android/location/x;

    iget-object v2, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/x;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 221
    return-object v1
.end method

.method private a(J)V
    .registers 7
    .parameter

    .prologue
    .line 190
    iput-wide p1, p0, Lcom/google/android/location/p;->r:J

    .line 191
    iget-wide v0, p0, Lcom/google/android/location/p;->q:J

    const-wide/16 v2, 0x1

    sub-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    .line 192
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 193
    return-void
.end method

.method private a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V
    .registers 33
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 238
    if-eqz p3, :cond_8

    .line 239
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    .line 241
    :cond_8
    if-eqz p5, :cond_f

    .line 242
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 247
    :cond_f
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->A:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-lez v3, :cond_4d

    .line 248
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/b/f;->a(J)V

    .line 249
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/g/l;->a(J)V

    .line 250
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    if-eqz v3, :cond_47

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v3}, Lcom/google/android/location/b/g;->g()V

    .line 253
    :cond_47
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->A:J

    .line 267
    :cond_4d
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->q:J

    sub-long v20, p1, v3

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-nez v3, :cond_3dd

    const-wide/16 v3, 0x0

    .line 273
    :goto_5b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v5}, Lcom/google/android/location/e/f;->b()Lcom/google/android/location/e/e;

    move-result-object v7

    .line 275
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->h:J

    cmp-long v5, p1, v5

    if-ltz v5, :cond_3e7

    const/4 v5, 0x1

    move/from16 v19, v5

    .line 277
    :goto_6e
    if-eqz v7, :cond_3ec

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->i()Z

    move-result v5

    if-eqz v5, :cond_3ec

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v5

    sub-long v5, p1, v5

    const-wide/32 v8, 0xafc8

    cmp-long v5, v5, v8

    if-gez v5, :cond_3ec

    const/4 v5, 0x1

    .line 279
    :goto_84
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-eqz v6, :cond_3ef

    const-wide/32 v8, 0xafc8

    cmp-long v6, v3, v8

    if-gez v6, :cond_3ef

    const/4 v6, 0x1

    move/from16 v18, v6

    .line 284
    :goto_94
    if-eqz v5, :cond_3f4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->C:Lcom/google/android/location/e/d;

    invoke-virtual {v6}, Lcom/google/android/location/e/d;->b()Z

    move-result v6

    if-eqz v6, :cond_3f4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    invoke-virtual {v6}, Lcom/google/android/location/e/t;->b()Lcom/google/android/location/e/e;

    move-result-object v6

    if-eq v7, v6, :cond_3f4

    const/4 v6, 0x1

    move/from16 v17, v6

    .line 286
    :goto_ad
    if-eqz v18, :cond_3f9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    invoke-virtual {v8}, Lcom/google/android/location/e/t;->a()Lcom/google/android/location/e/E;

    move-result-object v8

    if-eq v6, v8, :cond_3f9

    const/4 v6, 0x1

    move/from16 v16, v6

    .line 292
    :goto_c0
    if-eqz v19, :cond_3fe

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->u:Z

    if-eqz v6, :cond_3fe

    const/4 v6, 0x1

    move v15, v6

    .line 293
    :goto_ca
    if-eqz v19, :cond_402

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->v:Z

    if-eqz v6, :cond_402

    if-eqz v16, :cond_df

    const-wide/32 v8, 0xafc8

    sub-long v3, v8, v3

    const-wide/16 v8, 0xc8

    cmp-long v3, v3, v8

    if-gez v3, :cond_402

    :cond_df
    const/4 v3, 0x1

    move v14, v3

    .line 298
    :goto_e1
    if-nez v15, :cond_fb

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    const-wide/16 v8, -0x1

    cmp-long v3, v3, v8

    if-eqz v3, :cond_406

    if-eqz v7, :cond_fb

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    invoke-virtual {v7}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v6

    cmp-long v3, v3, v6

    if-lez v3, :cond_406

    :cond_fb
    const/4 v3, 0x1

    move v13, v3

    .line 302
    :goto_fd
    if-nez v14, :cond_123

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->w:J

    const-wide/16 v6, -0x1

    cmp-long v3, v3, v6

    if-eqz v3, :cond_40a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/p;->v:Z

    if-eqz v3, :cond_40a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    if-eqz v3, :cond_123

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->w:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    iget-wide v6, v6, Lcom/google/android/location/e/E;->a:J

    cmp-long v3, v3, v6

    if-lez v3, :cond_40a

    :cond_123
    const/4 v3, 0x1

    move v12, v3

    .line 307
    :goto_125
    if-eqz v13, :cond_40e

    if-nez v15, :cond_135

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->t:J

    sub-long v3, p1, v3

    const-wide/16 v6, 0x1388

    cmp-long v3, v3, v6

    if-gez v3, :cond_40e

    :cond_135
    const/4 v3, 0x1

    move v4, v3

    .line 310
    :goto_137
    if-eqz v12, :cond_412

    if-nez v14, :cond_147

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/location/p;->w:J

    sub-long v6, p1, v6

    const-wide/16 v8, 0x1388

    cmp-long v3, v6, v8

    if-gez v3, :cond_412

    :cond_147
    const/4 v3, 0x1

    .line 313
    :goto_148
    if-nez v4, :cond_14c

    if-eqz v3, :cond_415

    :cond_14c
    const/4 v3, 0x1

    .line 319
    :goto_14d
    if-nez v17, :cond_151

    if-eqz v16, :cond_153

    :cond_151
    if-eqz v3, :cond_15d

    :cond_153
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/p;->v:Z

    if-nez v3, :cond_418

    if-eqz v5, :cond_418

    if-nez v4, :cond_418

    :cond_15d
    const/4 v3, 0x1

    .line 321
    :goto_15e
    if-nez p5, :cond_16c

    if-nez p6, :cond_16c

    if-nez v19, :cond_16a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/location/p;->k:Z

    if-eqz v4, :cond_41b

    :cond_16a
    if-eqz v3, :cond_41b

    :cond_16c
    const/4 v3, 0x1

    move v11, v3

    .line 324
    :goto_16e
    const/4 v3, 0x0

    .line 326
    if-eqz v11, :cond_46e

    .line 327
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->F:Lcom/google/android/location/os/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/location/p;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/os/g;)Lcom/google/android/location/e/w;

    move-result-object v5

    .line 329
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->i:Lcom/google/android/location/g/e;

    if-eqz v17, :cond_41f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v3}, Lcom/google/android/location/e/f;->a()Lcom/google/android/location/e/f;

    move-result-object v3

    move-object v4, v3

    :goto_192
    if-eqz v18, :cond_424

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :goto_198
    move-object/from16 v0, p4

    invoke-virtual {v6, v4, v3, v0, v5}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;Lcom/google/android/location/e/g;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/t;

    move-result-object v3

    move-object v10, v3

    .line 333
    :goto_19f
    if-eqz v10, :cond_427

    iget-object v3, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v3, :cond_427

    const/4 v3, 0x1

    move v5, v3

    .line 334
    :goto_1a7
    if-eqz v10, :cond_42b

    iget-object v3, v10, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v3, v3, Lcom/google/android/location/e/c;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_42b

    const/4 v3, 0x1

    move v4, v3

    .line 336
    :goto_1b3
    if-eqz v10, :cond_42f

    iget-object v3, v10, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v3, v3, Lcom/google/android/location/e/D;->d:Lcom/google/android/location/e/o$a;

    sget-object v6, Lcom/google/android/location/e/o$a;->c:Lcom/google/android/location/e/o$a;

    if-ne v3, v6, :cond_42f

    const/4 v3, 0x1

    .line 340
    :goto_1be
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v6, :cond_432

    if-nez v19, :cond_1cc

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    if-eqz v6, :cond_432

    :cond_1cc
    if-eqz v4, :cond_1d0

    if-nez v17, :cond_1de

    :cond_1d0
    if-eqz v3, :cond_432

    if-eqz v16, :cond_432

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    invoke-virtual {v3}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-lez v3, :cond_432

    :cond_1de
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/location/h;->a(J)Z

    move-result v3

    if-eqz v3, :cond_432

    const/4 v3, 0x1

    move v9, v3

    .line 344
    :goto_1ec
    if-nez v9, :cond_1f0

    if-eqz v5, :cond_436

    :cond_1f0
    const/4 v3, 0x1

    move v8, v3

    .line 346
    :goto_1f2
    if-eqz v5, :cond_43a

    if-eqz v9, :cond_202

    iget-object v3, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/o;J)Z

    move-result v3

    if-eqz v3, :cond_43a

    :cond_202
    const/4 v3, 0x1

    .line 349
    :goto_203
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v6

    .line 350
    if-nez v6, :cond_43d

    if-eqz v19, :cond_43d

    const/4 v4, 0x1

    .line 354
    :goto_20c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v5, :cond_222

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/p;->m:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/p;->q:J

    move-wide/from16 v24, v0

    cmp-long v5, v22, v24

    if-gez v5, :cond_224

    :cond_222
    if-eqz v9, :cond_440

    :cond_224
    const/4 v5, 0x1

    .line 356
    :goto_225
    if-nez v13, :cond_22b

    if-nez v12, :cond_22b

    if-eqz v5, :cond_443

    :cond_22b
    const/4 v5, 0x1

    .line 357
    :goto_22c
    if-eqz v6, :cond_446

    if-nez v19, :cond_446

    const-wide/16 v22, 0x1388

    cmp-long v7, v20, v22

    if-gez v7, :cond_238

    if-nez v5, :cond_446

    :cond_238
    const/4 v5, 0x1

    move v7, v5

    .line 361
    :goto_23a
    if-eqz v6, :cond_44a

    if-eqz v19, :cond_44a

    if-nez v7, :cond_44a

    const/4 v5, 0x1

    .line 394
    :goto_241
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->k:Z

    if-nez v6, :cond_249

    if-eqz v19, :cond_44d

    :cond_249
    if-nez v11, :cond_44d

    const/4 v6, 0x1

    :goto_24c
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/location/p;->k:Z

    .line 395
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    if-nez v6, :cond_258

    if-eqz v19, :cond_450

    :cond_258
    if-nez v8, :cond_450

    const/4 v6, 0x1

    :goto_25b
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/location/p;->p:Z

    .line 397
    if-eqz v4, :cond_265

    .line 398
    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/p;->c()J

    move-result-wide p1

    .line 400
    :cond_265
    if-eqz v5, :cond_26d

    .line 401
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->q:J

    .line 403
    :cond_26d
    if-eqz v19, :cond_282

    .line 404
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->a:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    .line 405
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/p;->g:J

    add-long v4, v4, p1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/location/p;->h:J

    .line 407
    :cond_282
    if-eqz v15, :cond_291

    .line 408
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->i()V

    .line 409
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->t:J

    .line 411
    :cond_291
    if-eqz v14, :cond_2a0

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->n()V

    .line 413
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->w:J

    .line 415
    :cond_2a0
    if-eqz v9, :cond_2d8

    .line 416
    if-eqz v17, :cond_453

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    move-object v5, v4

    .line 417
    :goto_2a9
    if-eqz v16, :cond_457

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    invoke-virtual {v4}, Lcom/google/android/location/e/E;->a()I

    move-result v4

    if-lez v4, :cond_457

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    .line 418
    :goto_2b9
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 419
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->m:J

    .line 420
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/location/p;->o:Lcom/google/android/location/e/E;

    .line 421
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v4, v5}, Lcom/google/android/location/os/i;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 424
    :cond_2d8
    if-eqz v11, :cond_2e1

    .line 425
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    invoke-virtual {v4}, Lcom/google/android/location/g/l;->a()V

    .line 427
    :cond_2e1
    if-eqz v3, :cond_357

    .line 428
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->f:Lcom/google/android/location/a/n;

    if-eqz v18, :cond_45a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    :goto_2ed
    invoke-virtual {v4, v10, v3}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;

    move-result-object v3

    .line 430
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/a/n$b;)V

    .line 431
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v4

    .line 440
    if-eqz v4, :cond_30e

    .line 441
    invoke-interface {v4}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v4

    .line 443
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/location/p;->n:J

    cmp-long v4, v8, v4

    if-lez v4, :cond_45d

    .line 453
    :cond_30e
    :goto_30e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    iget-object v4, v4, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v4, :cond_334

    .line 454
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    iget-object v4, v4, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v5, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    invoke-static {v4, v5}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)D

    move-result-wide v4

    .line 459
    const-wide v8, 0x407544a3d70a3d71L

    cmpl-double v4, v4, v8

    if-lez v4, :cond_334

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->d:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    .line 465
    :cond_334
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    .line 466
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    iget-object v5, v10, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iput-object v5, v4, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    .line 467
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->b:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    .line 468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->j:Lcom/google/android/location/e/t;

    if-nez v3, :cond_468

    const/4 v3, 0x0

    :goto_354
    invoke-interface {v4, v5, v3}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V

    .line 473
    :cond_357
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/p;->h:J

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v5

    if-eqz v5, :cond_36e

    if-nez v7, :cond_36e

    .line 475
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->q:J

    const-wide/16 v8, 0x1388

    add-long/2addr v5, v8

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    .line 477
    :cond_36e
    const-wide v5, 0x7fffffffffffffffL

    cmp-long v5, v3, v5

    if-gez v5, :cond_37f

    .line 478
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v6, 0x0

    invoke-interface {v5, v6, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 484
    :cond_37f
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/p;->z:J

    sub-long v5, p1, v5

    .line 485
    const-wide/32 v8, 0x5265c0

    cmp-long v8, v5, v8

    if-gtz v8, :cond_3ab

    const-wide/32 v8, 0x2932e0

    cmp-long v5, v5, v8

    if-lez v5, :cond_3d7

    sub-long v3, v3, p1

    const-wide/16 v5, 0x2710

    cmp-long v3, v3, v5

    if-lez v3, :cond_3d7

    if-nez v13, :cond_3d7

    if-nez v12, :cond_3d7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v3, :cond_3d7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/p;->a()Z

    move-result v3

    if-eqz v3, :cond_3d7

    .line 490
    :cond_3ab
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v3, v4}, Lcom/google/android/location/b/f;->b(Lcom/google/android/location/os/i;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    invoke-virtual {v3}, Lcom/google/android/location/g/l;->b()V

    .line 492
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    if-eqz v3, :cond_3ca

    .line 493
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v3}, Lcom/google/android/location/b/g;->c()V

    .line 495
    :cond_3ca
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->B:Lcom/google/android/location/g;

    invoke-virtual {v3}, Lcom/google/android/location/g;->b()V

    .line 496
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/p;->z:J

    .line 499
    :cond_3d7
    if-eqz v7, :cond_3dc

    .line 500
    invoke-direct/range {p0 .. p2}, Lcom/google/android/location/p;->a(J)V

    .line 502
    :cond_3dc
    return-void

    .line 272
    :cond_3dd
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/p;->x:Lcom/google/android/location/e/E;

    iget-wide v3, v3, Lcom/google/android/location/e/E;->a:J

    sub-long v3, p1, v3

    goto/16 :goto_5b

    .line 275
    :cond_3e7
    const/4 v5, 0x0

    move/from16 v19, v5

    goto/16 :goto_6e

    .line 277
    :cond_3ec
    const/4 v5, 0x0

    goto/16 :goto_84

    .line 279
    :cond_3ef
    const/4 v6, 0x0

    move/from16 v18, v6

    goto/16 :goto_94

    .line 284
    :cond_3f4
    const/4 v6, 0x0

    move/from16 v17, v6

    goto/16 :goto_ad

    .line 286
    :cond_3f9
    const/4 v6, 0x0

    move/from16 v16, v6

    goto/16 :goto_c0

    .line 292
    :cond_3fe
    const/4 v6, 0x0

    move v15, v6

    goto/16 :goto_ca

    .line 293
    :cond_402
    const/4 v3, 0x0

    move v14, v3

    goto/16 :goto_e1

    .line 298
    :cond_406
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_fd

    .line 302
    :cond_40a
    const/4 v3, 0x0

    move v12, v3

    goto/16 :goto_125

    .line 307
    :cond_40e
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_137

    .line 310
    :cond_412
    const/4 v3, 0x0

    goto/16 :goto_148

    .line 313
    :cond_415
    const/4 v3, 0x0

    goto/16 :goto_14d

    .line 319
    :cond_418
    const/4 v3, 0x0

    goto/16 :goto_15e

    .line 321
    :cond_41b
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_16e

    .line 329
    :cond_41f
    sget-object v3, Lcom/google/android/location/p;->E:Lcom/google/android/location/e/f;

    move-object v4, v3

    goto/16 :goto_192

    :cond_424
    const/4 v3, 0x0

    goto/16 :goto_198

    .line 333
    :cond_427
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_1a7

    .line 334
    :cond_42b
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_1b3

    .line 336
    :cond_42f
    const/4 v3, 0x0

    goto/16 :goto_1be

    .line 340
    :cond_432
    const/4 v3, 0x0

    move v9, v3

    goto/16 :goto_1ec

    .line 344
    :cond_436
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_1f2

    .line 346
    :cond_43a
    const/4 v3, 0x0

    goto/16 :goto_203

    .line 350
    :cond_43d
    const/4 v4, 0x0

    goto/16 :goto_20c

    .line 354
    :cond_440
    const/4 v5, 0x0

    goto/16 :goto_225

    .line 356
    :cond_443
    const/4 v5, 0x0

    goto/16 :goto_22c

    .line 357
    :cond_446
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_23a

    .line 361
    :cond_44a
    const/4 v5, 0x0

    goto/16 :goto_241

    .line 394
    :cond_44d
    const/4 v6, 0x0

    goto/16 :goto_24c

    .line 395
    :cond_450
    const/4 v6, 0x0

    goto/16 :goto_25b

    .line 416
    :cond_453
    const/4 v4, 0x0

    move-object v5, v4

    goto/16 :goto_2a9

    .line 417
    :cond_457
    const/4 v4, 0x0

    goto/16 :goto_2b9

    .line 428
    :cond_45a
    const/4 v3, 0x0

    goto/16 :goto_2ed

    .line 447
    :cond_45d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/p;->d:Lcom/google/android/location/e/n;

    sget-object v5, Lcom/google/android/location/e/n$a;->c:Lcom/google/android/location/e/n$a;

    invoke-virtual {v4, v5}, Lcom/google/android/location/e/n;->a(Lcom/google/android/location/e/n$a;)V

    goto/16 :goto_30e

    .line 468
    :cond_468
    invoke-virtual {v3}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v3

    goto/16 :goto_354

    :cond_46e
    move-object v10, v3

    goto/16 :goto_19f
.end method

.method private a(JZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 655
    iget-boolean v0, p0, Lcom/google/android/location/p;->u:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v0, :cond_2a

    :cond_b
    move v0, v1

    .line 656
    :goto_c
    iput-boolean p3, p0, Lcom/google/android/location/p;->u:Z

    .line 657
    iput-boolean p4, p0, Lcom/google/android/location/p;->v:Z

    .line 658
    iget-boolean v2, p0, Lcom/google/android/location/p;->u:Z

    if-nez v2, :cond_18

    iget-boolean v2, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v2, :cond_2c

    .line 660
    :cond_18
    :goto_18
    if-eq v0, v1, :cond_22

    .line 661
    if-eqz v1, :cond_2e

    .line 662
    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    :cond_22
    :goto_22
    move-object v0, p0

    move-wide v1, p1

    move-object v4, v3

    move v6, v5

    .line 667
    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 668
    return-void

    :cond_2a
    move v0, v5

    .line 655
    goto :goto_c

    :cond_2c
    move v1, v5

    .line 658
    goto :goto_18

    .line 664
    :cond_2e
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/p;->h:J

    goto :goto_22
.end method

.method private a(Lcom/google/android/location/e/o;J)Z
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 515
    iget-object v2, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->p()Lcom/google/android/location/os/g;

    move-result-object v2

    .line 516
    if-nez v2, :cond_16

    .line 517
    iget-object v2, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget v2, v2, Lcom/google/android/location/e/w;->c:I

    const v3, 0x30d40

    if-ge v2, v3, :cond_14

    .line 535
    :cond_13
    :goto_13
    return v0

    :cond_14
    move v0, v1

    .line 523
    goto :goto_13

    .line 525
    :cond_16
    iget-wide v3, p1, Lcom/google/android/location/e/o;->e:J

    invoke-interface {v2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/location/p;->g:J

    const-wide/32 v7, 0xdbba0

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_13

    .line 529
    iget-object v3, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget v3, v3, Lcom/google/android/location/e/w;->c:I

    int-to-float v3, v3

    invoke-interface {v2}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    const/high16 v4, 0x447a

    mul-float/2addr v2, v4

    const v4, 0x47c35000

    add-float/2addr v2, v4

    cmpl-float v2, v3, v2

    if-lez v2, :cond_13

    move v0, v1

    .line 533
    goto :goto_13
.end method

.method private c()J
    .registers 5

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 183
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 184
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/p;->r:J

    .line 185
    iput-wide v0, p0, Lcom/google/android/location/p;->q:J

    .line 186
    return-wide v0
.end method


# virtual methods
.method public a(IZ)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 674
    int-to-long v1, p1

    const-wide/16 v6, 0x3e8

    mul-long/2addr v1, v6

    iput-wide v1, p0, Lcom/google/android/location/p;->g:J

    .line 675
    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    .line 676
    iget-boolean v4, p0, Lcom/google/android/location/p;->u:Z

    if-nez v4, :cond_17

    iget-boolean v4, p0, Lcom/google/android/location/p;->v:Z

    if-eqz v4, :cond_3a

    :cond_17
    move v4, v0

    .line 677
    :goto_18
    if-eqz v4, :cond_26

    .line 678
    if-eqz p2, :cond_3c

    const-wide/16 v6, 0x0

    .line 679
    :goto_1e
    iget-wide v8, p0, Lcom/google/android/location/p;->h:J

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/location/p;->h:J

    .line 682
    :cond_26
    iget-object v4, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-wide v6, p0, Lcom/google/android/location/p;->g:J

    const-wide/16 v8, 0x4e20

    cmp-long v6, v6, v8

    if-gtz v6, :cond_40

    :goto_30
    invoke-virtual {v4, v0}, Lcom/google/android/location/g/l;->a(Z)V

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    .line 684
    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 685
    return-void

    :cond_3a
    move v4, v5

    .line 676
    goto :goto_18

    .line 678
    :cond_3c
    iget-wide v6, p0, Lcom/google/android/location/p;->g:J

    add-long/2addr v6, v1

    goto :goto_1e

    :cond_40
    move v0, v5

    .line 682
    goto :goto_30
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 9
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 724
    if-nez p1, :cond_4

    .line 728
    :goto_3
    return-void

    .line 727
    :cond_4
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    goto :goto_3
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 700
    if-nez p1, :cond_15

    .line 703
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/p;->t:J

    .line 707
    :goto_8
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 708
    return-void

    .line 705
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/p;->s:Lcom/google/android/location/e/f;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;)V

    goto :goto_8
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 2
    .parameter

    .prologue
    .line 696
    iput-object p1, p0, Lcom/google/android/location/p;->F:Lcom/google/android/location/os/g;

    .line 697
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 11
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 711
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    .line 712
    iget-object v0, p0, Lcom/google/android/location/p;->o:Lcom/google/android/location/e/E;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/location/e/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/e/E;J)Lcom/google/android/location/e/g;

    move-result-object v4

    .line 713
    iget-object v0, p0, Lcom/google/android/location/p;->b:Lcom/google/android/location/b/f;

    iget-object v3, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    invoke-virtual {v0, p1, v5, v7, v8}, Lcom/google/android/location/b/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    .line 714
    iput-wide v1, p0, Lcom/google/android/location/p;->n:J

    .line 717
    iget-object v0, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-object v3, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    invoke-virtual {v0, p1, v7, v8}, Lcom/google/android/location/g/l;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    .line 719
    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 720
    iget-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    if-eqz p1, :cond_33

    :goto_2f
    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/location/h;->a(JZ)V

    .line 721
    return-void

    :cond_33
    move v5, v6

    .line 720
    goto :goto_2f
.end method

.method public a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    if-nez p1, :cond_f

    const/4 v0, 0x1

    :goto_9
    iget-boolean v3, p0, Lcom/google/android/location/p;->v:Z

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/location/p;->a(JZZ)V

    .line 689
    return-void

    .line 688
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a(ZZI)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 751
    iget-object v0, p0, Lcom/google/android/location/p;->D:Lcom/google/android/location/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/h;->a(ZZI)V

    .line 752
    return-void
.end method

.method a()Z
    .registers 5

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/google/android/location/p;->q:J

    iget-wide v2, p0, Lcom/google/android/location/p;->r:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public b()V
    .registers 8

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 692
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 693
    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 736
    iget-object v0, p0, Lcom/google/android/location/p;->y:Lcom/google/android/location/g/l;

    iget-object v1, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/g/l;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    .line 741
    if-eqz v0, :cond_1c

    .line 745
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/p;->a(JLcom/google/android/location/e/E;Lcom/google/android/location/e/g;ZZ)V

    .line 748
    :cond_1c
    return-void
.end method

.method public b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 731
    iget-object v0, p0, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/location/p;->u:Z

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/location/p;->a(JZZ)V

    .line 732
    return-void
.end method
