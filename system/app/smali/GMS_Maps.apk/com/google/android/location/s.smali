.class public Lcom/google/android/location/s;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/s$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/g;

.field private final d:Z


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/os/i;",
            "Lcom/google/android/location/g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/s;-><init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;Z)V

    .line 67
    return-void
.end method

.method constructor <init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/os/i;",
            "Lcom/google/android/location/g;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/android/location/s;->a:Ljava/util/List;

    .line 73
    iput-object p2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    .line 74
    iput-object p3, p0, Lcom/google/android/location/s;->c:Lcom/google/android/location/g;

    .line 75
    iput-boolean p4, p0, Lcom/google/android/location/s;->d:Z

    .line 76
    return-void
.end method

.method private a(Ljava/util/Calendar;JZ)J
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 126
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/s;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 128
    const/4 v0, 0x0

    .line 129
    if-nez v1, :cond_31

    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/location/s;->a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v0

    move v1, v2

    move-object v3, v0

    .line 134
    :goto_e
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 135
    if-eqz v1, :cond_1a

    .line 136
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 138
    :cond_1a
    if-eqz p4, :cond_2a

    iget-object v1, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->a:J

    :goto_22
    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 140
    invoke-direct {p0, v0}, Lcom/google/android/location/s;->c(Ljava/util/Calendar;)J

    move-result-wide v0

    return-wide v0

    .line 138
    :cond_2a
    iget-object v1, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->b:J

    goto :goto_22

    :cond_31
    move-object v3, v1

    move v1, v0

    goto :goto_e
.end method

.method private a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/k/b;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 231
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 232
    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 238
    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/s;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/location/k/b;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/google/android/location/u;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x7

    const/4 v2, 0x0

    .line 267
    iget-object v0, p0, Lcom/google/android/location/s;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 269
    iget-object v0, p0, Lcom/google/android/location/s;->c:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->p()Ljava/util/Map;

    move-result-object v3

    .line 272
    if-nez v3, :cond_3a

    .line 297
    :cond_17
    :goto_17
    if-eqz v2, :cond_73

    .line 298
    new-instance v0, Lcom/google/android/location/u;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    sget-object v3, Lcom/google/android/location/u$a;->b:Lcom/google/android/location/u$a;

    iget-boolean v4, p0, Lcom/google/android/location/s;->d:Z

    iget-object v1, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v7

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/u;-><init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/u$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    .line 303
    :goto_39
    return-object v0

    .line 277
    :cond_3a
    invoke-virtual {p2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, v4, :cond_17

    invoke-virtual {p2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, v1, :cond_17

    .line 287
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 288
    iget-wide v4, p1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v4, v5}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 289
    new-instance v4, Ljava/util/Random;

    iget-object v5, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    xor-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Random;-><init>(J)V

    .line 291
    invoke-virtual {v4}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    .line 295
    const-wide v6, 0x3fe999999999999aL

    cmpg-double v0, v4, v6

    if-gtz v0, :cond_71

    move v0, v1

    :goto_6f
    move v2, v0

    goto :goto_17

    :cond_71
    move v0, v2

    goto :goto_6f

    .line 303
    :cond_73
    new-instance v0, Lcom/google/android/location/u;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/u$a;->a:Lcom/google/android/location/u$a;

    iget-boolean v4, p0, Lcom/google/android/location/s;->d:Z

    iget-object v1, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v7

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/u;-><init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/u$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    goto :goto_39
.end method

.method private b(Ljava/util/Calendar;)Ljava/util/Calendar;
    .registers 8
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 242
    iget-object v0, p0, Lcom/google/android/location/s;->c:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 243
    if-nez v0, :cond_2f

    move-wide v0, v2

    .line 247
    :goto_b
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_12

    move-wide v0, v2

    .line 250
    :cond_12
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 251
    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 253
    invoke-virtual {v4, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 254
    const/16 v0, 0xc

    const/4 v1, -0x5

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 255
    invoke-virtual {v4, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 259
    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 262
    :cond_2e
    return-object v4

    .line 243
    :cond_2f
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    goto :goto_b
.end method

.method private c(Ljava/util/Calendar;)J
    .registers 6
    .parameter

    .prologue
    .line 309
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;
    .registers 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "J)",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/k/b;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 179
    .line 180
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_5a

    .line 181
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/google/android/location/s;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    add-long/2addr v3, p2

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object v1, v0

    .line 185
    :goto_16
    invoke-direct {p0, p1}, Lcom/google/android/location/s;->b(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v3

    .line 186
    invoke-static {p1, v3}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v4

    .line 188
    iget-object v0, p0, Lcom/google/android/location/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_24
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 189
    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->b(Ljava/util/Calendar;)Z

    move-result v6

    if-nez v6, :cond_24

    .line 190
    if-eqz v4, :cond_3e

    invoke-virtual {v0, v3}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v6

    if-nez v6, :cond_24

    .line 191
    :cond_3e
    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/location/s;->a(Lcom/google/android/location/k/b;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/google/android/location/u;

    move-result-object v0

    .line 193
    invoke-virtual {v0, p1}, Lcom/google/android/location/u;->a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;

    move-result-object v6

    .line 194
    if-eqz v6, :cond_24

    .line 195
    invoke-virtual {v0}, Lcom/google/android/location/u;->a()Lcom/google/android/location/u$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/u$a;->a:Lcom/google/android/location/u$a;

    if-ne v0, v1, :cond_57

    sget-object v0, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    .line 201
    :goto_52
    invoke-static {v6, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v2

    .line 210
    :cond_56
    return-object v2

    .line 195
    :cond_57
    sget-object v0, Lcom/google/android/location/b$a;->b:Lcom/google/android/location/b$a;

    goto :goto_52

    :cond_5a
    move-object v1, v2

    goto :goto_16
.end method


# virtual methods
.method public a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;
    .registers 6
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/location/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 222
    iget-wide v2, p1, Lcom/google/android/location/k/b;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 223
    return-object v0

    .line 226
    :cond_1b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not find parent of subtimespan: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/util/Calendar;J)Lcom/google/android/location/s$a;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/s;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v4

    .line 84
    if-eqz v4, :cond_22

    iget-object v0, v4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    move v3, v0

    .line 85
    :goto_13
    if-eqz v3, :cond_25

    iget-object v0, v4, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/b$a;

    move-object v1, v0

    .line 86
    :goto_1a
    new-instance v5, Lcom/google/android/location/s$a;

    if-nez v4, :cond_27

    :goto_1e
    invoke-direct {v5, v3, v1, v2}, Lcom/google/android/location/s$a;-><init>(ZLcom/google/android/location/b$a;Lcom/google/android/location/k/b;)V

    return-object v5

    .line 84
    :cond_22
    const/4 v0, 0x0

    move v3, v0

    goto :goto_13

    :cond_25
    move-object v1, v2

    .line 85
    goto :goto_1a

    .line 86
    :cond_27
    iget-object v0, v4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/k/b;

    move-object v2, v0

    goto :goto_1e
.end method

.method b(Ljava/util/Calendar;J)J
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 104
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/s;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    return-wide v0
.end method

.method c(Ljava/util/Calendar;J)J
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/s;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    return-wide v0
.end method
