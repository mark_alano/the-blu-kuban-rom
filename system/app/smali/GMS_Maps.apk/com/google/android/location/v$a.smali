.class final Lcom/google/android/location/v$a;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/v;


# direct methods
.method private constructor <init>(Lcom/google/android/location/v;)V
    .registers 2
    .parameter

    .prologue
    .line 577
    iput-object p1, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/v;Lcom/google/android/location/v$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 577
    invoke-direct {p0, p1}, Lcom/google/android/location/v$a;-><init>(Lcom/google/android/location/v;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 605
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-boolean v0, v0, Lcom/google/android/location/v;->o:Z

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_31

    .line 606
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 607
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v2, v2, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    .line 608
    const/4 v1, 0x6

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 609
    iget-object v1, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v1, v1, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    invoke-interface {v1, v0}, Lcom/google/android/location/c/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_31} :catch_39

    .line 614
    :cond_31
    :goto_31
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->c()V

    .line 615
    return-void

    .line 611
    :catch_39
    move-exception v0

    goto :goto_31
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v0, v0, Lcom/google/android/location/v;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    :goto_11
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 596
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_21

    .line 597
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    .line 600
    :cond_21
    return-void

    .line 592
    :cond_22
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    .line 619
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v0, v0, Lcom/google/android/location/v;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_34

    move v0, v8

    :goto_12
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 623
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    .line 628
    if-eqz p1, :cond_36

    invoke-virtual {p1}, Lcom/google/android/location/c/H;->a()I

    move-result v0

    if-nez v0, :cond_36

    .line 629
    const/4 v7, 0x3

    .line 635
    :goto_26
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v1, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v1, v1, Lcom/google/android/location/v;->m:J

    iget-object v3, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v3, v3, Lcom/google/android/location/v;->m:J

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/v;->a(Lcom/google/android/location/v;JJJIZ)V

    .line 638
    return-void

    .line 619
    :cond_34
    const/4 v0, 0x0

    goto :goto_12

    .line 630
    :cond_36
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-boolean v0, v0, Lcom/google/android/location/v;->q:Z

    if-eqz v0, :cond_3e

    .line 631
    const/4 v7, 0x2

    goto :goto_26

    :cond_3e
    move v7, v8

    .line 633
    goto :goto_26
.end method

.method public a(Ljava/lang/String;)V
    .registers 11
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/4 v8, 0x1

    .line 582
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v0, v0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v0, v0, Lcom/google/android/location/v;->m:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_29

    move v0, v8

    :goto_12
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 585
    iget-object v0, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v1, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-wide v1, v1, Lcom/google/android/location/v;->m:J

    iget-object v5, p0, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v;

    iget-object v5, v5, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    const/16 v7, 0x1d

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/v;->a(Lcom/google/android/location/v;JJJIZ)V

    .line 588
    return-void

    .line 582
    :cond_29
    const/4 v0, 0x0

    goto :goto_12
.end method
