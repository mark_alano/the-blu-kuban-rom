.class public Lcom/google/android/location/v;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/v$1;,
        Lcom/google/android/location/v$a;
    }
.end annotation


# static fields
.field static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Ljava/util/Random;

.field private B:Z

.field private C:Ljava/lang/Integer;

.field private D:Lcom/google/android/location/k/b;

.field private E:Z

.field private F:Z

.field j:J

.field k:Lcom/google/android/location/b$a;

.field l:Z

.field m:J

.field n:Lcom/google/android/location/c/r;

.field o:Z

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:Z

.field r:Lcom/google/android/location/c/J;

.field s:J

.field t:J

.field private final u:Lcom/google/android/location/g;

.field private final v:Lcom/google/android/location/t;

.field private final w:Lcom/google/android/location/f;

.field private final x:Lcom/google/android/location/i/a;

.field private final y:Lcom/google/android/location/a/b;

.field private final z:Lcom/google/android/location/s;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0xb

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    new-array v0, v4, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/v;->g:Ljava/util/Set;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/v;->h:Ljava/util/Set;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    .line 87
    sget-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    const/4 v2, 0x6

    invoke-direct {v1, v2, v3, v5, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    invoke-direct {v1, v5, v4, v6, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    const/16 v2, 0x17

    invoke-direct {v1, v6, v4, v2, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 166
    invoke-static {}, Lcom/google/android/location/t;->a()Lcom/google/android/location/t;

    move-result-object v8

    new-instance v12, Lcom/google/android/location/s;

    sget-object v1, Lcom/google/android/location/v;->i:Ljava/util/List;

    move-object/from16 v0, p5

    invoke-direct {v12, v1, p1, v0}, Lcom/google/android/location/s;-><init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v12}, Lcom/google/android/location/v;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/t;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;Lcom/google/android/location/s;)V

    .line 177
    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/t;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;Lcom/google/android/location/s;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    const-string v2, "SCollector"

    sget-object v7, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 114
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/v;->j:J

    .line 116
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/v;->k:Lcom/google/android/location/b$a;

    .line 118
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->B:Z

    .line 119
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->l:Z

    .line 127
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/v;->m:J

    .line 129
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    .line 134
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->o:Z

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 142
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->q:Z

    .line 144
    new-instance v1, Lcom/google/android/location/v$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/v$a;-><init>(Lcom/google/android/location/v;Lcom/google/android/location/v$1;)V

    iput-object v1, p0, Lcom/google/android/location/v;->r:Lcom/google/android/location/c/J;

    .line 147
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/v;->s:J

    .line 149
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/v;->t:J

    .line 152
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->E:Z

    .line 155
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/v;->F:Z

    .line 192
    iput-object p5, p0, Lcom/google/android/location/v;->u:Lcom/google/android/location/g;

    .line 193
    iput-object p7, p0, Lcom/google/android/location/v;->v:Lcom/google/android/location/t;

    .line 194
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/v;->x:Lcom/google/android/location/i/a;

    .line 195
    iput-object p6, p0, Lcom/google/android/location/v;->w:Lcom/google/android/location/f;

    .line 196
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/location/v;->y:Lcom/google/android/location/a/b;

    .line 197
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    .line 198
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/location/v;->A:Ljava/util/Random;

    .line 199
    return-void
.end method

.method private a(JJJIZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    .line 548
    add-long v1, p1, v3

    .line 549
    add-long/2addr v3, p5

    .line 550
    iget-object v0, p0, Lcom/google/android/location/v;->u:Lcom/google/android/location/g;

    iget-object v6, p0, Lcom/google/android/location/v;->k:Lcom/google/android/location/b$a;

    move v5, p7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/g;->a(JJILcom/google/android/location/b$a;)V

    .line 551
    iget-object v0, p0, Lcom/google/android/location/v;->u:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->b()V

    .line 554
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-eqz v0, :cond_29

    .line 555
    sub-long v0, p5, p3

    .line 556
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_29

    .line 557
    iget-object v2, p0, Lcom/google/android/location/v;->x:Lcom/google/android/location/i/a;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/a;->b(J)V

    .line 561
    :cond_29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    .line 562
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/v;->q:Z

    .line 563
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/v;->m:J

    .line 564
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/v;->o:Z

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 566
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/v;->C:Ljava/lang/Integer;

    .line 567
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/v;->D:Lcom/google/android/location/k/b;

    .line 568
    if-eqz p8, :cond_49

    .line 569
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 571
    :cond_49
    return-void
.end method

.method private a(JLcom/google/android/location/s$a;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p3, Lcom/google/android/location/s$a;->c:Lcom/google/android/location/k/b;

    if-nez v0, :cond_28

    const/4 v0, 0x0

    move-object v9, v0

    .line 278
    :goto_6
    iget-object v0, p0, Lcom/google/android/location/v;->D:Lcom/google/android/location/k/b;

    if-eqz v0, :cond_25

    .line 279
    iget-object v0, p0, Lcom/google/android/location/v;->D:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v9}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 280
    iget-object v0, p0, Lcom/google/android/location/v;->C:Ljava/lang/Integer;

    if-eqz v0, :cond_25

    .line 284
    const-wide/16 v3, -0x1

    iget-object v0, p0, Lcom/google/android/location/v;->C:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/v;->a(JJJIZ)V

    .line 290
    :cond_25
    iput-object v9, p0, Lcom/google/android/location/v;->D:Lcom/google/android/location/k/b;

    .line 291
    return-void

    .line 276
    :cond_28
    iget-object v0, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-object v1, p3, Lcom/google/android/location/s$a;->c:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/s;->a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;

    move-result-object v0

    move-object v9, v0

    goto :goto_6
.end method

.method static synthetic a(Lcom/google/android/location/v;JJJIZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct/range {p0 .. p8}, Lcom/google/android/location/v;->a(JJJIZ)V

    return-void
.end method

.method private a(Ljava/util/Calendar;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 445
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/v;->j(J)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/v;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    .line 446
    iget-object v1, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-wide v2, p0, Lcom/google/android/location/v;->t:J

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/location/s;->a(Ljava/util/Calendar;J)Lcom/google/android/location/s$a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/location/s$a;->a:Z

    .line 448
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_34

    if-eqz v1, :cond_34

    iget-object v0, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v2, :cond_34

    .line 449
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/v;->e(Z)V

    .line 450
    iget-object v0, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-wide v1, p0, Lcom/google/android/location/v;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/s;->c(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/v;->k(J)V

    .line 466
    :cond_33
    :goto_33
    return-void

    .line 452
    :cond_34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/v;->e(Z)V

    .line 454
    invoke-virtual {p0}, Lcom/google/android/location/v;->d()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 457
    if-eqz v1, :cond_4c

    .line 458
    iget-object v0, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-wide v1, p0, Lcom/google/android/location/v;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/s;->c(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/v;->k(J)V

    goto :goto_33

    .line 461
    :cond_4c
    iget-object v0, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-wide v1, p0, Lcom/google/android/location/v;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/s;->b(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/v;->k(J)V

    goto :goto_33
.end method

.method private a(JZ)Z
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 412
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 413
    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    iget-object v1, p0, Lcom/google/android/location/v;->u:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    iget-object v1, p0, Lcom/google/android/location/v;->u:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    sget-object v1, Lcom/google/android/location/v;->h:Ljava/util/Set;

    const-wide/32 v3, 0x493e0

    iget-object v5, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    if-eqz p3, :cond_68

    const/16 v6, 0x12

    :goto_39
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/v;->w:Lcom/google/android/location/f;

    invoke-virtual {v7}, Lcom/google/android/location/f;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/location/v;->r:Lcom/google/android/location/c/J;

    iget-object v11, p0, Lcom/google/android/location/v;->a:Ljava/lang/String;

    move v7, p3

    invoke-interface/range {v0 .. v11}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;ZLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    .line 430
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_62

    .line 431
    iput-wide p1, p0, Lcom/google/android/location/v;->m:J

    .line 432
    iput-boolean v9, p0, Lcom/google/android/location/v;->q:Z

    .line 433
    iput-boolean p3, p0, Lcom/google/android/location/v;->o:Z

    .line 434
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    .line 435
    iget-boolean v0, p0, Lcom/google/android/location/v;->E:Z

    invoke-direct {p0, v0}, Lcom/google/android/location/v;->f(Z)V

    .line 441
    :cond_62
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_67

    const/4 v9, 0x1

    :cond_67
    return v9

    .line 415
    :cond_68
    const/16 v6, 0xe

    goto :goto_39
.end method

.method private a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E$a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 674
    iget-object v0, p2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/android/location/b/g;->b(Ljava/lang/Long;)F

    move-result v0

    .line 675
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_17

    iget-object v1, p0, Lcom/google/android/location/v;->A:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_17

    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private e()V
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    .line 319
    iget-wide v0, p0, Lcom/google/android/location/v;->j:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_10

    .line 320
    iput-wide v2, p0, Lcom/google/android/location/v;->j:J

    .line 321
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 323
    :cond_10
    return-void
.end method

.method private e(Z)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0xb4

    .line 469
    iget-boolean v0, p0, Lcom/google/android/location/v;->l:Z

    if-eq p1, v0, :cond_f

    .line 470
    if-eqz p1, :cond_10

    .line 471
    iget-object v0, p0, Lcom/google/android/location/v;->y:Lcom/google/android/location/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->b(I)V

    .line 477
    :goto_d
    iput-boolean p1, p0, Lcom/google/android/location/v;->l:Z

    .line 479
    :cond_f
    return-void

    .line 474
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/v;->y:Lcom/google/android/location/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->c(I)V

    goto :goto_d
.end method

.method private f()I
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 393
    .line 394
    iget-object v1, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    .line 395
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 396
    if-eqz v2, :cond_1e

    .line 397
    array-length v3, v2

    move v1, v0

    :goto_f
    if-ge v1, v3, :cond_1e

    aget-object v4, v2, v1

    .line 398
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 399
    add-int/lit8 v0, v0, 0x1

    .line 397
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 403
    :cond_1e
    return v0
.end method

.method private f(Z)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 500
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_33

    iget-boolean v0, p0, Lcom/google/android/location/v;->o:Z

    if-eqz v0, :cond_33

    .line 501
    iget-object v0, p0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_16

    .line 502
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->K:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 504
    :cond_16
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->S:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 505
    invoke-virtual {v0, v6, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 506
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/v;->m:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 508
    iget-object v1, p0, Lcom/google/android/location/v;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 510
    :cond_33
    return-void
.end method

.method private g()Ljava/util/Calendar;
    .registers 4

    .prologue
    .line 482
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 484
    iget-object v1, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 485
    return-object v0
.end method

.method private j(J)Z
    .registers 7
    .parameter

    .prologue
    .line 266
    iget-wide v0, p0, Lcom/google/android/location/v;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    iget-wide v0, p0, Lcom/google/android/location/v;->t:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_15

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private k(J)V
    .registers 7
    .parameter

    .prologue
    .line 309
    iget-wide v0, p0, Lcom/google/android/location/v;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_16

    iget-wide v0, p0, Lcom/google/android/location/v;->j:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1e

    .line 311
    :cond_16
    iput-wide p1, p0, Lcom/google/android/location/v;->j:J

    .line 312
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 316
    :cond_1e
    return-void
.end method


# virtual methods
.method a(I)V
    .registers 5
    .parameter

    .prologue
    .line 295
    const/4 v0, 0x6

    if-ne p1, v0, :cond_14

    .line 296
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/v;->j:J

    .line 297
    invoke-direct {p0}, Lcom/google/android/location/v;->g()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;J)V

    .line 299
    :cond_14
    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 644
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v1, :cond_10

    .line 645
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/v;->s:J

    .line 647
    :cond_10
    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 651
    invoke-super {p0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/E;)V

    .line 653
    iget-object v1, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v1, v2, :cond_c

    if-nez p1, :cond_d

    .line 667
    :cond_c
    :goto_c
    return-void

    .line 657
    :cond_d
    iget-object v1, p0, Lcom/google/android/location/v;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v1}, Lcom/google/android/location/a$b;->a()Lcom/google/android/location/b/g;

    move-result-object v2

    move v1, v0

    .line 658
    :goto_14
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_29

    .line 659
    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/v;->a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 660
    add-int/lit8 v1, v1, 0x1

    .line 658
    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 663
    :cond_29
    int-to-long v0, v1

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-ltz v0, :cond_c

    .line 665
    iget-wide v0, p1, Lcom/google/android/location/e/E;->a:J

    iput-wide v0, p0, Lcom/google/android/location/v;->t:J

    goto :goto_c
.end method

.method a(Lcom/google/android/location/e/e;)V
    .registers 3
    .parameter

    .prologue
    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/v;->F:Z

    .line 682
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/google/android/location/v;->B:Z

    .line 204
    return-void
.end method

.method b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/google/android/location/v;->f(Z)V

    .line 491
    iput-boolean p1, p0, Lcom/google/android/location/v;->E:Z

    .line 492
    return-void
.end method

.method protected b(J)Z
    .registers 15
    .parameter

    .prologue
    const/4 v9, 0x1

    const-wide/16 v3, -0x1

    const/4 v8, 0x0

    .line 213
    iget-boolean v0, p0, Lcom/google/android/location/v;->F:Z

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/location/v;->d()Z

    move-result v0

    if-nez v0, :cond_11

    .line 214
    :cond_e
    iput-boolean v8, p0, Lcom/google/android/location/v;->F:Z

    .line 259
    :cond_10
    :goto_10
    return v8

    .line 218
    :cond_11
    invoke-direct {p0}, Lcom/google/android/location/v;->g()Ljava/util/Calendar;

    move-result-object v0

    .line 222
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/v;->j(J)Z

    move-result v1

    .line 223
    iget-wide v5, p0, Lcom/google/android/location/v;->j:J

    cmp-long v2, v5, v3

    if-eqz v2, :cond_25

    if-eqz v1, :cond_28

    iget-boolean v2, p0, Lcom/google/android/location/v;->l:Z

    if-nez v2, :cond_28

    .line 225
    :cond_25
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;J)V

    .line 227
    :cond_28
    iget-object v2, p0, Lcom/google/android/location/v;->z:Lcom/google/android/location/s;

    iget-wide v5, p0, Lcom/google/android/location/v;->t:J

    invoke-virtual {v2, v0, v5, v6}, Lcom/google/android/location/s;->a(Ljava/util/Calendar;J)Lcom/google/android/location/s$a;

    move-result-object v0

    .line 229
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/v;->a(JLcom/google/android/location/s$a;)V

    .line 230
    iget-boolean v2, v0, Lcom/google/android/location/s$a;->a:Z

    if-eqz v2, :cond_78

    .line 232
    iget-object v0, v0, Lcom/google/android/location/s$a;->b:Lcom/google/android/location/b$a;

    iput-object v0, p0, Lcom/google/android/location/v;->k:Lcom/google/android/location/b$a;

    .line 235
    invoke-virtual {p0, v1}, Lcom/google/android/location/v;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v2

    .line 236
    iget-object v0, v2, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 238
    iget-wide v5, p0, Lcom/google/android/location/v;->s:J

    cmp-long v0, v5, v3

    if-eqz v0, :cond_80

    iget-wide v5, p0, Lcom/google/android/location/v;->s:J

    sub-long v5, p1, v5

    const-wide/16 v10, 0x7530

    cmp-long v0, v5, v10

    if-gez v0, :cond_80

    move v0, v9

    .line 240
    :goto_5a
    if-eqz v0, :cond_82

    .line 243
    invoke-direct {p0}, Lcom/google/android/location/v;->e()V

    .line 244
    sget-object v0, Lcom/google/android/location/a$c;->h:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    .line 245
    invoke-direct {p0, v8}, Lcom/google/android/location/v;->e(Z)V

    .line 246
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/v;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_78

    .line 248
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    .line 249
    const/16 v7, 0x1d

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/v;->a(JJJIZ)V

    .line 259
    :cond_78
    :goto_78
    iget-object v0, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_10

    move v8, v9

    goto :goto_10

    :cond_80
    move v0, v8

    .line 238
    goto :goto_5a

    .line 253
    :cond_82
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/v;->C:Ljava/lang/Integer;

    goto :goto_78

    .line 256
    :cond_8b
    iget-object v0, v2, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/v;->C:Ljava/lang/Integer;

    goto :goto_78
.end method

.method public c()V
    .registers 4

    .prologue
    .line 303
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    .line 305
    invoke-direct {p0}, Lcom/google/android/location/v;->g()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;J)V

    .line 306
    return-void
.end method

.method d(Z)Lcom/google/android/location/e/u;
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/android/location/v;->v:Lcom/google/android/location/t;

    invoke-virtual {v0, p1}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v5

    .line 348
    iget-object v0, p0, Lcom/google/android/location/v;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->m()Z

    move-result v6

    .line 349
    iget-object v0, p0, Lcom/google/android/location/v;->w:Lcom/google/android/location/f;

    invoke-virtual {v0}, Lcom/google/android/location/f;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_5d

    move v1, v2

    .line 350
    :goto_17
    iget-object v0, p0, Lcom/google/android/location/v;->x:Lcom/google/android/location/i/a;

    const-wide/32 v7, 0x493e0

    invoke-virtual {v0, v7, v8, v2}, Lcom/google/android/location/i/a;->d(JZ)Z

    move-result v7

    .line 353
    iget-object v0, v5, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5f

    iget-boolean v0, p0, Lcom/google/android/location/v;->B:Z

    if-eqz v0, :cond_5f

    if-eqz v6, :cond_5f

    if-eqz v1, :cond_5f

    if-eqz v7, :cond_5f

    move v4, v2

    .line 357
    :goto_35
    if-eqz v4, :cond_92

    .line 359
    invoke-direct {p0}, Lcom/google/android/location/v;->f()I

    move-result v0

    .line 360
    const/4 v8, 0x3

    if-lt v0, v8, :cond_61

    move v0, v2

    .line 361
    :goto_3f
    if-eqz v4, :cond_63

    if-nez v0, :cond_63

    :goto_43
    move v3, v0

    .line 364
    :goto_44
    if-nez v2, :cond_90

    .line 368
    iget-object v0, v5, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_65

    .line 369
    iget-object v0, v5, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 384
    :goto_54
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0

    :cond_5d
    move v1, v3

    .line 349
    goto :goto_17

    :cond_5f
    move v4, v3

    .line 353
    goto :goto_35

    :cond_61
    move v0, v3

    .line 360
    goto :goto_3f

    :cond_63
    move v2, v3

    .line 361
    goto :goto_43

    .line 370
    :cond_65
    if-nez v6, :cond_6e

    .line 371
    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_54

    .line 372
    :cond_6e
    if-nez v1, :cond_77

    .line 373
    const/16 v0, 0x1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_54

    .line 374
    :cond_77
    if-nez v7, :cond_80

    .line 375
    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_54

    .line 376
    :cond_80
    if-eqz v3, :cond_89

    .line 377
    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_54

    .line 379
    :cond_89
    const/16 v0, 0x63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_54

    .line 382
    :cond_90
    const/4 v0, 0x0

    goto :goto_54

    :cond_92
    move v2, v4

    goto :goto_44
.end method

.method d()Z
    .registers 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/location/v;->v:Lcom/google/android/location/t;

    invoke-virtual {v0}, Lcom/google/android/location/t;->b()Z

    move-result v0

    .line 330
    if-nez v0, :cond_8

    .line 333
    :cond_8
    return v0
.end method

.method protected h(J)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 514
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    if-nez v0, :cond_b

    .line 517
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/v;->f:Lcom/google/android/location/a$c;

    move v0, v1

    .line 530
    :goto_a
    return v0

    .line 521
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/v;->v:Lcom/google/android/location/t;

    iget-boolean v2, p0, Lcom/google/android/location/v;->o:Z

    invoke-virtual {v0, v2}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_24

    .line 527
    iput-boolean v1, p0, Lcom/google/android/location/v;->q:Z

    .line 528
    iget-object v0, p0, Lcom/google/android/location/v;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    .line 530
    :cond_24
    const/4 v0, 0x0

    goto :goto_a
.end method
