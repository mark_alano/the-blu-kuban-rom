.class public Lcom/google/android/location/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/w$1;,
        Lcom/google/android/location/w$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/os/i;

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lcom/google/android/location/w$a;

.field private g:J

.field private h:J

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private j:J

.field private k:Lcom/google/android/location/c/q;

.field private final l:Lcom/google/android/location/g;

.field private final m:Lcom/google/android/location/t;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/g;Lcom/google/android/location/t;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x0

    const-wide/16 v1, -0x1

    const/4 v8, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-wide v1, p0, Lcom/google/android/location/w;->b:J

    .line 65
    iput-boolean v8, p0, Lcom/google/android/location/w;->c:Z

    .line 69
    iput-boolean v8, p0, Lcom/google/android/location/w;->d:Z

    .line 72
    iput-boolean v8, p0, Lcom/google/android/location/w;->e:Z

    .line 74
    sget-object v0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w$a;

    iput-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    .line 77
    iput-wide v1, p0, Lcom/google/android/location/w;->g:J

    .line 81
    iput-wide v3, p0, Lcom/google/android/location/w;->h:J

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    .line 85
    iput-wide v3, p0, Lcom/google/android/location/w;->j:J

    .line 101
    iput-object p1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    .line 102
    iput-object p2, p0, Lcom/google/android/location/w;->l:Lcom/google/android/location/g;

    .line 103
    iput-object p3, p0, Lcom/google/android/location/w;->m:Lcom/google/android/location/t;

    .line 104
    invoke-virtual {p2}, Lcom/google/android/location/g;->e()J

    move-result-wide v0

    .line 106
    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 107
    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    .line 108
    sub-long v4, v2, v0

    const-wide/32 v6, 0x6cf2a0

    cmp-long v4, v4, v6

    if-lez v4, :cond_46

    .line 111
    const-wide/32 v0, 0x6ddd00

    sub-long v0, v2, v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    .line 113
    :cond_46
    invoke-direct {p0, v0, v1, v8}, Lcom/google/android/location/w;->a(JZ)V

    .line 114
    return-void
.end method

.method private a(J)V
    .registers 8
    .parameter

    .prologue
    .line 268
    iget-wide v0, p0, Lcom/google/android/location/w;->j:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_c

    .line 282
    :cond_b
    return-void

    .line 273
    :cond_c
    iput-wide p1, p0, Lcom/google/android/location/w;->j:J

    .line 274
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 276
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_20
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 277
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 278
    invoke-direct {p0, v3}, Lcom/google/android/location/w;->a(Ljava/io/File;)V

    .line 276
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    goto :goto_20
.end method

.method private a(JZ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 196
    iput-wide p1, p0, Lcom/google/android/location/w;->g:J

    .line 197
    iget-wide v0, p0, Lcom/google/android/location/w;->g:J

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    .line 198
    if-eqz p3, :cond_16

    .line 199
    iget-object v0, p0, Lcom/google/android/location/w;->l:Lcom/google/android/location/g;

    iget-object v1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v1

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/g;->a(J)V

    .line 204
    :cond_16
    return-void
.end method

.method private a(Ljava/io/File;)V
    .registers 5
    .parameter

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v0

    .line 289
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/w;->b(Ljava/io/File;J)Z

    move-result v2

    if-nez v2, :cond_12

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/w;->a(Ljava/io/File;J)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 291
    :cond_12
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/io/File;)Z

    .line 293
    :cond_15
    return-void
.end method

.method private a(Ljava/io/File;J)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 296
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_a

    .line 308
    :cond_9
    :goto_9
    return v0

    .line 299
    :cond_a
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 300
    if-eqz v2, :cond_9

    .line 303
    array-length v3, v2

    move v1, v0

    :goto_12
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 304
    invoke-direct {p0, v4, p2, p3}, Lcom/google/android/location/w;->b(Ljava/io/File;J)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 305
    const/4 v0, 0x1

    goto :goto_9

    .line 303
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_12
.end method

.method private b()V
    .registers 5

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/location/w;->m:Lcom/google/android/location/t;

    invoke-virtual {v0}, Lcom/google/android/location/t;->b()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 118
    iget-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    sget-object v1, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w$a;

    if-eq v0, v1, :cond_12

    .line 121
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    .line 145
    :goto_11
    return-void

    .line 123
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/w;->a(J)V

    goto :goto_11

    .line 127
    :cond_1c
    const/4 v0, 0x0

    .line 129
    :cond_1d
    iget-object v1, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    .line 130
    sget-object v2, Lcom/google/android/location/w$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    invoke-virtual {v3}, Lcom/google/android/location/w$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_42

    .line 141
    :goto_2c
    iget-object v2, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    if-eq v1, v2, :cond_30

    .line 144
    :cond_30
    if-nez v0, :cond_1d

    goto :goto_11

    .line 132
    :pswitch_33
    invoke-direct {p0}, Lcom/google/android/location/w;->c()Z

    move-result v0

    goto :goto_2c

    .line 135
    :pswitch_38
    invoke-direct {p0}, Lcom/google/android/location/w;->d()Z

    move-result v0

    goto :goto_2c

    .line 138
    :pswitch_3d
    invoke-direct {p0}, Lcom/google/android/location/w;->e()Z

    move-result v0

    goto :goto_2c

    .line 130
    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_33
        :pswitch_38
        :pswitch_3d
    .end packed-switch
.end method

.method private b(J)Z
    .registers 7
    .parameter

    .prologue
    .line 369
    iget-wide v0, p0, Lcom/google/android/location/w;->h:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private b(Ljava/io/File;J)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 317
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .line 318
    sub-long v2, p2, v0

    .line 319
    const-wide/32 v4, 0x240c8400

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1b

    const-wide/32 v4, 0x36ee80

    add-long/2addr v4, p2

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1b

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/location/w;->c(Ljava/io/File;J)Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_1b
    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private c(J)V
    .registers 7
    .parameter

    .prologue
    .line 386
    iget-wide v0, p0, Lcom/google/android/location/w;->b:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_10

    .line 388
    iput-wide p1, p0, Lcom/google/android/location/w;->b:J

    .line 389
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/location/w;->b:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 391
    :cond_10
    return-void
.end method

.method private c()Z
    .registers 10

    .prologue
    const-wide/32 v7, 0x6ddd00

    const/4 v0, 0x1

    .line 148
    iget-object v1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    .line 149
    iget-wide v3, p0, Lcom/google/android/location/w;->g:J

    add-long/2addr v3, v7

    .line 150
    invoke-direct {p0}, Lcom/google/android/location/w;->h()Z

    move-result v5

    if-eqz v5, :cond_43

    cmp-long v5, v1, v3

    if-ltz v5, :cond_43

    .line 151
    invoke-direct {p0}, Lcom/google/android/location/w;->i()Z

    move-result v3

    if-nez v3, :cond_21

    .line 154
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/w;->a(JZ)V

    .line 192
    :goto_20
    return v0

    .line 159
    :cond_21
    invoke-direct {p0, v1, v2}, Lcom/google/android/location/w;->b(J)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 165
    const-wide/16 v3, 0x1388

    add-long/2addr v3, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/location/w;->c(J)V

    .line 191
    :cond_2d
    :goto_2d
    invoke-direct {p0, v1, v2}, Lcom/google/android/location/w;->a(J)V

    .line 192
    const/4 v0, 0x0

    goto :goto_20

    .line 170
    :cond_32
    invoke-direct {p0}, Lcom/google/android/location/w;->k()V

    .line 171
    sget-object v1, Lcom/google/android/location/w$a;->b:Lcom/google/android/location/w$a;

    iput-object v1, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    .line 175
    iget-object v1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/location/w;->a(JZ)V

    goto :goto_20

    .line 179
    :cond_43
    cmp-long v0, v3, v1

    if-gtz v0, :cond_5b

    .line 180
    iget-wide v3, p0, Lcom/google/android/location/w;->b:J

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_55

    iget-wide v3, p0, Lcom/google/android/location/w;->b:J

    cmp-long v0, v3, v1

    if-gez v0, :cond_2d

    .line 184
    :cond_55
    add-long v3, v1, v7

    invoke-direct {p0, v3, v4}, Lcom/google/android/location/w;->c(J)V

    goto :goto_2d

    .line 188
    :cond_5b
    invoke-direct {p0, v3, v4}, Lcom/google/android/location/w;->c(J)V

    goto :goto_2d
.end method

.method private c(Ljava/io/File;J)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".lck"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-wide/32 v0, 0x36ee80

    cmp-long v0, p2, v0

    if-lez v0, :cond_17

    const/4 v0, 0x1

    .line 330
    :goto_14
    if-eqz v0, :cond_16

    .line 333
    :cond_16
    return v0

    .line 329
    :cond_17
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private d()Z
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 213
    invoke-direct {p0}, Lcom/google/android/location/w;->h()Z

    move-result v2

    if-nez v2, :cond_c

    .line 216
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    .line 245
    :goto_b
    return v0

    .line 220
    :cond_c
    iget-object v2, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    .line 221
    iget-wide v4, p0, Lcom/google/android/location/w;->g:J

    const-wide/16 v6, 0x3a98

    add-long/2addr v4, v6

    .line 222
    cmp-long v2, v2, v4

    if-ltz v2, :cond_51

    .line 223
    invoke-direct {p0}, Lcom/google/android/location/w;->j()Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 225
    sget-object v2, Lcom/google/android/location/w$a;->c:Lcom/google/android/location/w$a;

    iput-object v2, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    .line 226
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    .line 228
    iget-object v2, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    :goto_37
    if-ge v1, v3, :cond_49

    aget-object v4, v2, v1

    .line 229
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_46

    .line 230
    iget-object v5, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_46
    add-int/lit8 v1, v1, 0x1

    goto :goto_37

    .line 233
    :cond_49
    invoke-direct {p0}, Lcom/google/android/location/w;->f()V

    goto :goto_b

    .line 237
    :cond_4d
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    goto :goto_b

    .line 243
    :cond_51
    invoke-direct {p0, v4, v5}, Lcom/google/android/location/w;->c(J)V

    move v0, v1

    .line 245
    goto :goto_b
.end method

.method private e()Z
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 249
    iget-wide v3, p0, Lcom/google/android/location/w;->g:J

    const-wide/32 v5, 0x124f80

    add-long/2addr v3, v5

    .line 250
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    .line 252
    cmp-long v0, v5, v3

    if-ltz v0, :cond_26

    move v0, v1

    .line 253
    :goto_13
    invoke-direct {p0}, Lcom/google/android/location/w;->h()Z

    move-result v5

    .line 254
    invoke-direct {p0}, Lcom/google/android/location/w;->j()Z

    move-result v6

    .line 255
    if-nez v0, :cond_21

    if-eqz v5, :cond_21

    if-nez v6, :cond_28

    .line 258
    :cond_21
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    move v2, v1

    .line 264
    :goto_25
    return v2

    :cond_26
    move v0, v2

    .line 252
    goto :goto_13

    .line 262
    :cond_28
    invoke-direct {p0, v3, v4}, Lcom/google/android/location/w;->c(J)V

    goto :goto_25
.end method

.method private f()V
    .registers 4

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/google/android/location/w;->h()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-direct {p0}, Lcom/google/android/location/w;->j()Z

    move-result v0

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    sget-object v1, Lcom/google/android/location/w$a;->c:Lcom/google/android/location/w$a;

    if-ne v0, v1, :cond_4c

    .line 339
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4c

    .line 340
    iget-object v0, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 342
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/c/C;->c(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 358
    iget-object v1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SensorUploader"

    invoke-interface {v1, v0, p0, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Lcom/google/android/location/c/g;Ljava/lang/String;)Lcom/google/android/location/c/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    .line 359
    iget-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    if-eqz v0, :cond_12

    .line 360
    iget-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    invoke-interface {v0}, Lcom/google/android/location/c/q;->a()V

    .line 366
    :goto_4b
    return-void

    .line 365
    :cond_4c
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    goto :goto_4b
.end method

.method private g()V
    .registers 3

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    if-eqz v0, :cond_c

    .line 375
    iget-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    invoke-interface {v0}, Lcom/google/android/location/c/q;->b()V

    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/c/q;

    .line 378
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/w;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 379
    invoke-direct {p0}, Lcom/google/android/location/w;->l()V

    .line 380
    sget-object v0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w$a;

    iput-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/w$a;

    .line 382
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/w;->c(J)V

    .line 383
    return-void
.end method

.method private h()Z
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 394
    iget-boolean v0, p0, Lcom/google/android/location/w;->c:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/location/w;->m:Lcom/google/android/location/t;

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    move v0, v1

    goto :goto_16
.end method

.method private i()Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 398
    iget-object v1, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    .line 399
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_e

    .line 407
    :cond_d
    :goto_d
    return v0

    .line 402
    :cond_e
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_14
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 403
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_20

    .line 404
    const/4 v0, 0x1

    goto :goto_d

    .line 402
    :cond_20
    add-int/lit8 v1, v1, 0x1

    goto :goto_14
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/google/android/location/w;->c:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/location/w;->d:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private k()V
    .registers 3

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/google/android/location/w;->e:Z

    if-nez v0, :cond_16

    .line 421
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 426
    iget-boolean v0, p0, Lcom/google/android/location/w;->d:Z

    if-nez v0, :cond_13

    .line 427
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->x()Z

    .line 432
    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/w;->e:Z

    .line 434
    :cond_16
    return-void
.end method

.method private l()V
    .registers 3

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/google/android/location/w;->e:Z

    if-eqz v0, :cond_d

    .line 441
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 442
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/w;->e:Z

    .line 444
    :cond_d
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/google/android/location/w;->h()Z

    move-result v0

    .line 525
    invoke-direct {p0}, Lcom/google/android/location/w;->j()Z

    move-result v1

    .line 526
    if-eqz v0, :cond_c

    if-nez v1, :cond_f

    .line 529
    :cond_c
    invoke-direct {p0}, Lcom/google/android/location/w;->g()V

    .line 531
    :cond_f
    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/google/android/location/w;->b()V

    .line 475
    return-void
.end method

.method a(I)V
    .registers 4
    .parameter

    .prologue
    .line 452
    const/4 v0, 0x7

    if-ne p1, v0, :cond_a

    .line 454
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/w;->b:J

    .line 455
    invoke-direct {p0}, Lcom/google/android/location/w;->b()V

    .line 457
    :cond_a
    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/google/android/location/w;->m()V

    .line 521
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/google/android/location/w;->m()V

    .line 516
    return-void
.end method

.method public a(ILjava/lang/String;Lcom/google/android/location/c/K;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 511
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/android/location/w;->f()V

    .line 496
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/location/c/K;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 500
    iget v0, p2, Lcom/google/android/location/c/K;->b:I

    if-nez v0, :cond_8

    iget v0, p2, Lcom/google/android/location/c/K;->f:I

    if-nez v0, :cond_8

    .line 505
    :cond_8
    invoke-direct {p0}, Lcom/google/android/location/w;->f()V

    .line 506
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 466
    iput-boolean p1, p0, Lcom/google/android/location/w;->c:Z

    .line 467
    invoke-direct {p0}, Lcom/google/android/location/w;->b()V

    .line 468
    return-void
.end method

.method a(ZZI)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 487
    iput-boolean p1, p0, Lcom/google/android/location/w;->d:Z

    .line 488
    iget-object v0, p0, Lcom/google/android/location/w;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/w;->h:J

    .line 489
    invoke-direct {p0}, Lcom/google/android/location/w;->b()V

    .line 490
    return-void
.end method
