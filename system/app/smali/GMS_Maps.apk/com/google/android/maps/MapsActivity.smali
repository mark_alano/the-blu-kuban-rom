.class public Lcom/google/android/maps/MapsActivity;
.super Lcom/google/googlenav/android/BaseMapsActivity;
.source "SourceFile"

# interfaces
.implements LaT/m;
.implements Lcom/google/googlenav/android/R;
.implements Lcom/google/googlenav/android/Y;
.implements Lcom/google/googlenav/android/ag;


# static fields
.field private static final ACTION_ENTER_CAR_MODE:Ljava/lang/String; = "android.app.action.ENTER_CAR_MODE"

.field private static final ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter; = null

.field private static final ENTRY_POINT_RESET_TIMEOUT_MS:J = 0xdbba0L

.field public static final INPUT_FOCUS_STATE_DIALOG:I = 0x2

.field public static final INPUT_FOCUS_STATE_INITIAL:I = -0x1

.field public static final INPUT_FOCUS_STATE_MAP:I = 0x1

.field public static final INPUT_FOCUS_STATE_WAIT:I = 0x0

.field private static final MAX_MS_BEFORE_ON_CREATE:I = 0x1388

.field private static final MINIMUM_HEAP_SIZE:J = 0x600000L

.field private static final NO_TIME:I = -0x1

.field private static coldStartProfile:Lcom/google/googlenav/common/util/o;

.field public static volatile featureTestUiHandler:Landroid/os/Handler;

.field private static inputFocusStateForTesting:I

.field private static final startupClock:Lcom/google/googlenav/common/a;

.field private static stopwatchStatsLifecycleOnPause:Lbm/i;

.field private static stopwatchStatsLifecycleOnPauseVm:Lbm/i;

.field private static stopwatchStatsMenuOpen:Lbm/i;

.field private static final stopwatchStatsStartupAfterBack:Lbm/i;

.field private static final stopwatchStatsStartupAfterBackVm:Lbm/i;

.field private static final stopwatchStatsStartupAfterBriefPause:Lbm/i;

.field private static final stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

.field private static final stopwatchStatsStartupCold:Lbm/i;

.field private static final stopwatchStatsStartupColdVm:Lbm/i;

.field private static final stopwatchStatsStartupHot:Lbm/i;

.field private static final stopwatchStatsStartupHotVm:Lbm/i;

.field private static final stopwatchStatsStartupRemoteStrings:Lbm/i;

.field private static final stopwatchStatsStartupRemoteStringsVm:Lbm/i;

.field private static final stopwatchStatsStartupScreenOn:Lbm/i;

.field private static final stopwatchStatsStartupScreenOnVm:Lbm/i;


# instance fields
.field private arePowerConsumersRunning:Z

.field private buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

.field private connReceiver:Landroid/content/BroadcastReceiver;

.field private final dockReceiver:Landroid/content/BroadcastReceiver;

.field private entryPointType:Lcom/google/android/maps/A;

.field private gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

.field private hasDataConnection:Z

.field private intentProcessor:Lcom/google/googlenav/android/M;

.field private final latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

.field private mapViewMenuController:Lcom/google/googlenav/ui/as;

.field private orientationProvider:LaV/h;

.field private final screenReceiver:Landroid/content/BroadcastReceiver;

.field private shouldSetupEntryPointOnResume:Z

.field private startupStartTime:J

.field private startupType:Lcom/google/android/maps/B;

.field private stopPowerConsumersTask:Las/d;

.field private tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

.field private voiceRecognizer:Lcom/google/googlenav/android/ad;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/16 v4, 0x16

    .line 257
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.app.action.ENTER_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter;

    .line 322
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    .line 334
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup remote strings"

    const-string v3, "guid"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStrings:Lbm/i;

    .line 339
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup remote strings vm"

    const-string v3, "guid_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStringsVm:Lbm/i;

    .line 349
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup cold"

    const-string v3, "guif"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupCold:Lbm/i;

    .line 353
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup cold vm"

    const-string v3, "guif_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupColdVm:Lbm/i;

    .line 359
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after back"

    const-string v3, "guir"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBack:Lbm/i;

    .line 363
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after back vm"

    const-string v3, "guir_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBackVm:Lbm/i;

    .line 369
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup hot"

    const-string v3, "guis"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHot:Lbm/i;

    .line 374
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup hot vm"

    const-string v3, "guis_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHotVm:Lbm/i;

    .line 381
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup screen on"

    const-string v3, "guip"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOn:Lbm/i;

    .line 386
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup screen on vm"

    const-string v3, "guip_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOnVm:Lbm/i;

    .line 396
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after brief pause"

    const-string v3, "guib"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPause:Lbm/i;

    .line 401
    new-instance v0, Lbm/i;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    const-string v2, "startup after brief pause vm"

    const-string v3, "guib_vm"

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

    .line 448
    const/4 v0, -0x1

    sput v0, Lcom/google/android/maps/MapsActivity;->inputFocusStateForTesting:I

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/googlenav/android/BaseMapsActivity;-><init>()V

    .line 248
    new-instance v0, Lcom/google/android/maps/a;

    invoke-direct {v0, p0}, Lcom/google/android/maps/a;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    .line 263
    new-instance v0, Lcom/google/android/maps/p;

    invoke-direct {v0, p0}, Lcom/google/android/maps/p;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    .line 288
    new-instance v0, Lcom/google/android/maps/t;

    invoke-direct {v0, p0}, Lcom/google/android/maps/t;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    .line 301
    new-instance v0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-direct {v0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    .line 331
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 415
    sget-object v0, Lcom/google/android/maps/B;->a:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    .line 481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/maps/MapsActivity;Lcom/google/android/maps/B;)Lcom/google/android/maps/B;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/maps/MapsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->stopPowerConsumers()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/maps/MapsActivity;)LaV/h;
    .registers 2
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/android/M;
    .registers 2
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/maps/MapsActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/maps/MapsActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/maps/MapsActivity;)Las/d;
    .registers 2
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/maps/MapsActivity;Las/d;)Las/d;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/maps/MapsActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/ui/as;
    .registers 2
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/maps/MapsActivity;Landroid/os/Bundle;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/google/android/maps/MapsActivity;->onCreateInternal(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/maps/MapsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onResumeInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/maps/MapsActivity;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/maps/MapsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onNewFeaturesContentHintClick()V

    return-void
.end method

.method private createTabletDialog()V
    .registers 4

    .prologue
    .line 990
    new-instance v0, Lcom/google/googlenav/ui/view/android/bC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bC;-><init>(Lcom/google/googlenav/android/BaseMapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    .line 991
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->show()V

    .line 994
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    .line 995
    return-void
.end method

.method private determineEntryPointType()Lcom/google/android/maps/A;
    .registers 5

    .prologue
    .line 674
    sget-object v0, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    .line 676
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 677
    if-eqz v1, :cond_17

    sget-object v2, Lcom/google/googlenav/android/M;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 678
    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    .line 683
    :cond_16
    :goto_16
    return-object v0

    .line 679
    :cond_17
    if-eqz v1, :cond_16

    sget-object v2, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 680
    sget-object v0, Lcom/google/android/maps/A;->c:Lcom/google/android/maps/A;

    goto :goto_16
.end method

.method private getController()Lcom/google/googlenav/ui/s;
    .registers 2

    .prologue
    .line 1670
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    return-object v0
.end method

.method private getGmmApplication()Lcom/google/googlenav/android/AndroidGmmApplication;
    .registers 2

    .prologue
    .line 1019
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method public static getInputFocusForTesting()I
    .registers 1

    .prologue
    .line 2299
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 2300
    sget v0, Lcom/google/android/maps/MapsActivity;->inputFocusStateForTesting:I

    return v0
.end method

.method private getMapController()LaN/u;
    .registers 2

    .prologue
    .line 1674
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    return-object v0
.end method

.method public static getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;
    .registers 2
    .parameter

    .prologue
    .line 1683
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    .line 1684
    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    .line 1686
    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method

.method private initTransitNavigation()V
    .registers 5

    .prologue
    .line 972
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    .line 973
    new-instance v1, Lcom/google/android/maps/rideabout/view/a;

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/rideabout/view/a;-><init>(Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/r;->a(Lcom/google/googlenav/ui/view/r;)V

    .line 975
    new-instance v1, Lcom/google/android/maps/rideabout/app/q;

    invoke-direct {v1, p0, v0}, Lcom/google/android/maps/rideabout/app/q;-><init>(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {v1}, LaS/a;->a(LaS/a;)V

    .line 976
    return-void
.end method

.method private isIntentAvailable(Ljava/lang/String;)Z
    .registers 5
    .parameter

    .prologue
    .line 2272
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2273
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2274
    const/high16 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2276
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_17

    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private onCreateInternal(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 795
    const-string v0, "MapsActivity.onCreateInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 796
    invoke-static {p0}, Lcom/google/googlenav/android/J;->a(Landroid/app/Activity;)V

    .line 802
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->c()V

    .line 805
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setDefaultKeyMode(I)V

    .line 807
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->initMapUi()V

    .line 811
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/s;->a(Lcom/google/android/maps/A;)V

    .line 813
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    new-instance v3, Lcom/google/googlenav/android/D;

    invoke-direct {v3}, Lcom/google/googlenav/android/D;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/android/A;->a(Lcom/google/googlenav/android/C;)V

    .line 814
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->d()V

    .line 818
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/android/A;->a(Lcom/google/googlenav/android/B;)V

    .line 820
    new-instance v0, Lcom/google/googlenav/android/ad;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/ad;-><init>(Lcom/google/googlenav/android/ag;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->voiceRecognizer:Lcom/google/googlenav/android/ad;

    .line 821
    new-instance v0, Lcom/google/googlenav/android/M;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getMapController()LaN/u;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Lcom/google/googlenav/android/M;-><init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    .line 825
    if-nez p1, :cond_65

    .line 828
    new-instance v0, Lcom/google/android/maps/w;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/w;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    .line 843
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v3

    if-eqz v3, :cond_a7

    .line 844
    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v3, v0, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    .line 850
    :cond_65
    :goto_65
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v3, Lcom/google/android/maps/x;

    invoke-direct {v3, p0}, Lcom/google/android/maps/x;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-static {v0, v3}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 862
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_97

    .line 864
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 866
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_ad

    move v0, v1

    :goto_84
    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    .line 869
    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    if-nez v0, :cond_97

    .line 870
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0x32d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 877
    :cond_97
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/y;

    invoke-direct {v1, p0}, Lcom/google/android/maps/y;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 884
    const-string v0, "MapsActivity.onCreateInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 885
    return-void

    .line 847
    :cond_a7
    sget-object v3, Lcom/google/googlenav/z;->d:Lcom/google/googlenav/z;

    invoke-static {v3, v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    goto :goto_65

    :cond_ad
    move v0, v2

    .line 866
    goto :goto_84
.end method

.method private onNewFeaturesContentHintClick()V
    .registers 6

    .prologue
    .line 1280
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1281
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->m()V

    .line 1289
    :cond_11
    :goto_11
    const/4 v0, 0x6

    const-string v1, "ch"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "c"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1293
    return-void

    .line 1282
    :cond_24
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 1283
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->openOptionsMenu()V

    goto :goto_11

    .line 1284
    :cond_36
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1285
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->b()Landroid/view/View;

    move-result-object v0

    .line 1286
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LaA/h;->a(Landroid/content/Context;Landroid/view/View;LaA/g;)V

    goto :goto_11
.end method

.method private onResumeInternal()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1132
    const-string v0, "MapsActivity.onResumeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 1134
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aD()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1135
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aE()Z

    .line 1136
    invoke-static {}, LaM/j;->a()LaM/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/j;->a(Landroid/content/Context;)V

    .line 1140
    :cond_23
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 1141
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->j()Lcom/google/googlenav/ui/android/L;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/L;->a(Landroid/view/View;)V

    .line 1146
    :cond_38
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    .line 1147
    iget-boolean v1, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->h(Z)V

    .line 1149
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v1, :cond_4f

    .line 1150
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->requestFocus()Z

    .line 1151
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->c()V

    .line 1154
    :cond_4f
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->resumeTransitNavigationView()V

    .line 1157
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->setUpForEntryPoint()V

    .line 1161
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->u()V

    .line 1164
    const-string v1, "settings_preference"

    invoke-virtual {p0, v1, v5}, Lcom/google/android/maps/MapsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1168
    :try_start_5e
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1171
    const-string v3, "lastRunVersionCode"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ge v3, v4, :cond_8e

    .line 1173
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    const/16 v4, 0x618

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1177
    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v4, Lcom/google/android/maps/b;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/google/android/maps/b;-><init>(Lcom/google/android/maps/MapsActivity;Landroid/content/pm/PackageInfo;Lcom/google/googlenav/ui/s;Landroid/content/SharedPreferences;)V

    invoke-static {v3, v4}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V
    :try_end_8e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5e .. :try_end_8e} :catch_d6

    .line 1236
    :cond_8e
    :goto_8e
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_ae

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_ae

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b1

    .line 1238
    :cond_ae
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->queueStarSync()V

    .line 1247
    :cond_b1
    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    if-nez v0, :cond_c9

    .line 1248
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/e;

    sget-object v2, Lcom/google/googlenav/y;->a:Lcom/google/googlenav/y;

    invoke-direct {v1, p0, v5, v6, v2}, Lcom/google/android/maps/e;-><init>(Lcom/google/android/maps/MapsActivity;ZZLcom/google/googlenav/y;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1267
    iput-boolean v6, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    .line 1272
    :cond_c3
    :goto_c3
    const-string v0, "MapsActivity.onResumeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 1273
    return-void

    .line 1268
    :cond_c9
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    if-eqz v0, :cond_c3

    .line 1269
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    .line 1270
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    goto :goto_c3

    .line 1232
    :catch_d6
    move-exception v0

    goto :goto_8e
.end method

.method private pauseTransitNavigationView()V
    .registers 2

    .prologue
    .line 979
    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->d()V

    .line 980
    return-void
.end method

.method private queueStarSync()V
    .registers 5

    .prologue
    .line 2467
    .line 2469
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/q;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/maps/q;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    .line 2476
    return-void
.end method

.method private resumeTransitNavigationView()V
    .registers 2

    .prologue
    .line 983
    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->c()V

    .line 984
    return-void
.end method

.method public static setInputFocusForTesting(I)V
    .registers 1
    .parameter

    .prologue
    .line 2296
    return-void
.end method

.method private setLastMenuForTest(Landroid/view/Menu;)V
    .registers 2
    .parameter

    .prologue
    .line 1661
    return-void
.end method

.method private setMinimumHeapSize(J)V
    .registers 9
    .parameter

    .prologue
    .line 2435
    :try_start_0
    const-string v0, "dalvik.system.VMRuntime"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2436
    const-string v1, "getRuntime"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2437
    const-string v2, "setMinimumHeapSize"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2438
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2439
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_32} :catch_33

    .line 2443
    :goto_32
    return-void

    .line 2440
    :catch_33
    move-exception v0

    .line 2441
    const-string v1, "setMinimumHeapSize reflection failed"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_32
.end method

.method private setUpForEntryPoint()V
    .registers 13

    .prologue
    const/16 v11, 0x12

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1300
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    .line 1301
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v4

    .line 1303
    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    if-ne v4, v0, :cond_69

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_69

    move v0, v1

    .line 1306
    :goto_1f
    iget-boolean v5, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    if-eqz v5, :cond_41

    if-eqz v0, :cond_6b

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/googlenav/ui/s;->M()J

    move-result-wide v7

    const-wide/32 v9, 0xdbba0

    add-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-gez v5, :cond_6b

    .line 1309
    :cond_41
    if-eqz v0, :cond_68

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_68

    .line 1310
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 1311
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v1, v11}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1367
    :cond_68
    :goto_68
    return-void

    :cond_69
    move v0, v2

    .line 1303
    goto :goto_1f

    .line 1317
    :cond_6b
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1318
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 1319
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v6

    .line 1322
    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_84

    const/high16 v5, 0x10

    and-int/2addr v5, v6

    if-eqz v5, :cond_68

    .line 1326
    :cond_84
    iput-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    .line 1328
    iget-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    sget-object v5, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    if-ne v4, v5, :cond_9a

    .line 1331
    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 1332
    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    goto :goto_68

    .line 1338
    :cond_9a
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->resetForInvocation()V

    .line 1340
    sget-object v3, Lcom/google/android/maps/s;->a:[I

    iget-object v4, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    invoke-virtual {v4}, Lcom/google/android/maps/A;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_112

    goto :goto_68

    .line 1342
    :pswitch_ab
    const-string v3, "source"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1343
    const-string v0, ""

    .line 1344
    if-eqz v3, :cond_c8

    .line 1345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1347
    :cond_c8
    const/16 v3, 0x57

    const-string v4, "o"

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1349
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/ak;->a(Z)V

    .line 1350
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_f4

    .line 1353
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v11}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1356
    :cond_f4
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    goto/16 :goto_68

    .line 1360
    :pswitch_fd
    const/16 v0, 0x3d

    const-string v1, "li"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 1362
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->startFriendsListView(Lbf/am;)V

    goto/16 :goto_68

    .line 1340
    nop

    :pswitch_data_112
    .packed-switch 0x1
        :pswitch_ab
        :pswitch_fd
    .end packed-switch
.end method

.method private setupMapViewMenuController()V
    .registers 3

    .prologue
    .line 662
    new-instance v0, Lcom/google/googlenav/ui/as;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/as;-><init>(Lcom/google/android/maps/MapsActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    .line 663
    new-instance v0, Lbf/av;

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbf/av;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V

    .line 667
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    .line 668
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/as;)V

    .line 669
    return-void
.end method

.method private stopPowerConsumers()V
    .registers 2

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 776
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->Z()V

    .line 780
    :cond_d
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    if-eqz v0, :cond_16

    .line 781
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    invoke-virtual {v0}, LaV/h;->h()V

    .line 785
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_1f

    .line 786
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->e()V

    .line 788
    :cond_1f
    return-void
.end method


# virtual methods
.method public activateAreaSelector(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)Lcom/google/googlenav/ui/android/r;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1725
    new-instance v0, Lcom/google/googlenav/ui/android/r;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/r;-><init>(Lcom/google/android/maps/MapsActivity;)V

    .line 1726
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/android/r;->a(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)V

    .line 1727
    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2550
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/android/BaseMapsActivity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2554
    return-void
.end method

.method public getActivity()Lcom/google/android/maps/MapsActivity;
    .registers 1

    .prologue
    .line 1821
    return-object p0
.end method

.method public getBaseAndroidView()Lcom/google/googlenav/ui/android/BaseAndroidView;
    .registers 2

    .prologue
    .line 1731
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    return-object v0
.end method

.method public getBottomBar()Landroid/view/View;
    .registers 3

    .prologue
    .line 1703
    const v0, 0x7f1002d2

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1705
    if-eqz v0, :cond_16

    invoke-virtual {v0}, Landroid/view/ViewStub;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_16

    .line 1706
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1710
    :goto_15
    return-object v0

    .line 1708
    :cond_16
    const v0, 0x7f10003d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_15
.end method

.method public getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;
    .registers 2

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-object v0
.end method

.method public getCurrentViewportDetails()Landroid/os/Bundle;
    .registers 6

    .prologue
    .line 2211
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2212
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getMapController()LaN/u;

    move-result-object v1

    .line 2213
    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v2

    .line 2214
    const-string v3, "centerLatitude"

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2215
    const-string v3, "centerLongitude"

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2216
    const-string v2, "latitudeSpan"

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2217
    const-string v2, "longitudeSpan"

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2219
    const-string v2, "zoomLevel"

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2221
    return-object v0
.end method

.method public getMapViewMenuController()Lcom/google/googlenav/ui/as;
    .registers 2

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method public getNfcUrl()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2480
    const/4 v0, 0x0

    .line 2481
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    .line 2482
    if-eqz v1, :cond_10

    .line 2483
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/C;->x()Ljava/lang/String;

    move-result-object v0

    .line 2490
    :cond_f
    :goto_f
    return-object v0

    .line 2485
    :cond_10
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    .line 2486
    if-eqz v1, :cond_f

    .line 2487
    invoke-virtual {v1}, Lbf/i;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_f
.end method

.method public getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 2452
    const/4 v0, 0x0

    .line 2453
    invoke-static {}, Lcom/google/googlenav/friend/android/e;->a()Lcom/google/googlenav/friend/android/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/friend/android/e;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v1

    .line 2455
    if-eqz v1, :cond_22

    .line 2456
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2457
    const-string v0, "contacts_accessor_formatted_address"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2460
    :cond_1f
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2463
    :cond_22
    return-object v0
.end method

.method public getState()Lcom/google/googlenav/android/i;
    .registers 2

    .prologue
    .line 1691
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getGmmApplication()Lcom/google/googlenav/android/AndroidGmmApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/c;

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    return-object v0
.end method

.method public getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;
    .registers 2

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    return-object v0
.end method

.method public getUiThreadHandler()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 1816
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->k()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    .prologue
    .line 1696
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    return-object v0
.end method

.method public hasDataConnection()Z
    .registers 2

    .prologue
    .line 2541
    iget-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->hasDataConnection:Z

    return v0
.end method

.method public initClickableView(Lcom/google/googlenav/ui/s;)V
    .registers 6
    .parameter

    .prologue
    .line 998
    const-string v0, "MapsActivity.initClickableView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 1000
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1009
    :goto_1d
    if-nez v0, :cond_3e

    .line 1010
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bubble not found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1005
    :cond_27
    const v0, 0x7f1002d0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1006
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1007
    const v1, 0x7f100050

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_1d

    .line 1012
    :cond_3e
    new-instance v1, Lcom/google/googlenav/ui/view/e;

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    iget-object v3, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/googlenav/ui/view/e;-><init>(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    invoke-static {v1}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/view/e;)V

    .line 1013
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-static {v0, v1}, Lbf/h;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;)V

    .line 1014
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/s;)V

    .line 1015
    const-string v0, "MapsActivity.initClickableView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 1016
    return-void
.end method

.method public initMapUi()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 891
    const-string v0, "MapsActivity.initMapUi"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 894
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 895
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 899
    invoke-static {p0}, Lcom/google/googlenav/ui/view/android/ad;->a(Lcom/google/googlenav/android/BaseMapsActivity;)V

    .line 900
    invoke-static {p0}, Lcom/google/googlenav/ui/view/android/S;->a(Lcom/google/googlenav/android/BaseMapsActivity;)V

    .line 901
    invoke-static {p0}, Lcom/google/googlenav/mylocationnotifier/a;->a(Lcom/google/android/maps/MapsActivity;)V

    .line 903
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_f5

    .line 904
    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 905
    const v0, 0x7f0400ee

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setContentView(I)V

    .line 906
    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 907
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_de

    .line 911
    const v0, 0x7f1002cc

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 912
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 913
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 926
    :cond_56
    :goto_56
    const v0, 0x7f100052

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ButtonContainer;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    .line 927
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(Landroid/app/Application;)V

    .line 930
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_107

    .line 931
    const v0, 0x7f1002ce

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 935
    :goto_7d
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BaseAndroidView;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    .line 936
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V

    .line 938
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->orientationProvider:LaV/h;

    .line 940
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    .line 941
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a(Lcom/google/googlenav/ui/android/D;)V

    .line 947
    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapsActivity;->initClickableView(Lcom/google/googlenav/ui/s;)V

    .line 948
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->initTransitNavigation()V

    .line 951
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 952
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->createTabletDialog()V

    .line 956
    :cond_b2
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 957
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->c()Lcom/google/googlenav/ui/android/FloorPickerView;

    move-result-object v2

    .line 958
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setIndoorState(Ln/q;)V

    .line 959
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    invoke-interface {v0, v2}, LaH/m;->a(LaH/A;)V

    .line 960
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-static {v2, v1, v0}, Lcom/google/googlenav/ui/android/N;->a(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/googlenav/ui/android/N;

    .line 963
    :cond_d3
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    .line 964
    const-string v0, "MapsActivity.initMapUi"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 965
    return-void

    .line 914
    :cond_de
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_56

    .line 917
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    goto/16 :goto_56

    .line 921
    :cond_f5
    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 922
    const v0, 0x7f0400ef

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setContentView(I)V

    .line 923
    const-string v0, "Activity.setContentView"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    goto/16 :goto_56

    .line 933
    :cond_107
    const v0, 0x7f1002cd

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto/16 :goto_7d
.end method

.method public lockScreenOrientation()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 2499
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 2500
    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1b

    .line 2501
    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x6

    :goto_15
    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    .line 2511
    :cond_18
    :goto_18
    return-void

    .line 2501
    :cond_19
    const/4 v0, 0x0

    goto :goto_15

    .line 2505
    :cond_1b
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_18

    .line 2506
    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v1

    if-eqz v1, :cond_26

    const/4 v0, 0x7

    :cond_26
    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    goto :goto_18
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2165
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/android/S;->a(IILandroid/content/Intent;)V

    .line 2166
    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/android/BaseMapsActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2167
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 1574
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1579
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/am;->a(Landroid/content/res/Configuration;)V

    .line 1585
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->onConfigurationChangedInternal()V

    .line 1586
    return-void
.end method

.method public onConfigurationChangedInternal()V
    .registers 2

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_33

    .line 1542
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->d()V

    .line 1544
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1545
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->f()V

    .line 1551
    :cond_22
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1552
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->invalidateOptionsMenu()V

    .line 1558
    :cond_33
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 1559
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    .line 1562
    :cond_40
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->l()V

    .line 1564
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    if-eqz v0, :cond_50

    .line 1568
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->buttonContainer:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    .line 1570
    :cond_50
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x16

    const/4 v3, 0x1

    .line 508
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 516
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 520
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 527
    :cond_37
    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 548
    const-wide/32 v0, 0x600000

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->setMinimumHeapSize(J)V

    .line 552
    invoke-static {}, Lcom/google/googlenav/android/c;->b()Z

    move-result v0

    if-eqz v0, :cond_115

    sget-object v0, Lcom/google/android/maps/B;->d:Lcom/google/android/maps/B;

    :goto_4d
    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    .line 556
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->determineEntryPointType()Lcom/google/android/maps/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    .line 558
    iput-boolean v3, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    .line 563
    new-instance v0, LaU/d;

    invoke-direct {v0}, LaU/d;-><init>()V

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/o;->a(Lcom/google/android/maps/driveabout/vector/p;)V

    .line 568
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/c;->a(Landroid/app/Application;)Lcom/google/googlenav/android/c;

    .line 571
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_77

    .line 572
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 575
    :cond_77
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 577
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_93

    .line 578
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 579
    if-nez v0, :cond_119

    const/4 v0, 0x0

    .line 583
    :goto_8b
    const-string v1, "UA-25817243-1"

    invoke-static {v1, p0, v0}, Laj/a;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 588
    invoke-static {v3}, Laj/a;->a(Z)V

    .line 593
    :cond_93
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/android/maps/MapsActivity;Z)V

    .line 595
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->setupMapViewMenuController()V

    .line 602
    new-instance v0, Lcom/google/android/maps/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/u;-><init>(Lcom/google/android/maps/MapsActivity;Landroid/os/Bundle;)V

    .line 611
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/google/googlenav/android/i;->a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V

    .line 617
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 618
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->l()V

    .line 627
    :cond_c4
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/S;->a(Landroid/content/Context;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->a(Lcom/google/android/maps/MapsActivity;)V

    .line 633
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbl/j;->a(Landroid/content/Context;)V

    .line 638
    new-instance v0, Lbm/i;

    const-string v1, "menu open"

    const-string v2, "mo"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    .line 640
    new-instance v0, Lbm/i;

    const-string v1, "maps onPause"

    const-string v2, "ap"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    .line 643
    new-instance v0, Lbm/i;

    const-string v1, "maps onPause vm"

    const-string v2, "ap_vm"

    invoke-direct {v0, v1, v2, v4}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    .line 650
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-nez v0, :cond_10f

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->n()Z

    move-result v0

    if-eqz v0, :cond_10f

    .line 655
    invoke-direct {p0, p1}, Lcom/google/android/maps/MapsActivity;->onCreateInternal(Landroid/os/Bundle;)V

    .line 658
    :cond_10f
    const-string v0, "MapsActivity.onCreate"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 659
    return-void

    .line 552
    :cond_115
    sget-object v0, Lcom/google/android/maps/B;->c:Lcom/google/android/maps/B;

    goto/16 :goto_4d

    .line 579
    :cond_119
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8b
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 1595
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_3a

    .line 1596
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    .line 1598
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    .line 1599
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 1600
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gk;->w()Lcom/google/googlenav/actionbar/b;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/googlenav/actionbar/b;)V

    .line 1605
    :cond_39
    :goto_39
    return v0

    :cond_3a
    const/4 v0, 0x0

    goto :goto_39
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 1486
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onDestroy()V

    .line 1488
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1492
    invoke-static {}, Laj/a;->a()V

    .line 1496
    :cond_c
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 1497
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->M()V

    .line 1500
    :cond_1d
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    .line 1501
    if-eqz v0, :cond_26

    .line 1502
    invoke-virtual {v0}, LaS/a;->y()V

    .line 1507
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    if-eqz v0, :cond_32

    .line 1511
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bC;->d()V

    .line 1512
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->tabletDialog:Lcom/google/googlenav/ui/view/android/bC;

    .line 1517
    :cond_32
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_40

    .line 1518
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->a()V

    .line 1519
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->f()V

    .line 1522
    :cond_40
    invoke-static {p0}, Lcom/google/googlenav/android/J;->b(Landroid/app/Activity;)V

    .line 1525
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->latitudeBroadcastReceiver:Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->b(Lcom/google/android/maps/MapsActivity;)V

    .line 1526
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2175
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLowMemory()V
    .registers 2

    .prologue
    .line 2370
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v0, :cond_9

    .line 2371
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->g()V

    .line 2373
    :cond_9
    invoke-static {}, Lcom/google/googlenav/common/k;->a()V

    .line 2374
    return-void
.end method

.method public onMainMenuItemsChanged(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 1638
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lbf/i;->aE()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1639
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->invalidateOptionsMenu()V

    .line 1641
    :cond_b
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1645
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    .line 1646
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->b()V

    .line 1647
    invoke-direct {p0, p2}, Lcom/google/android/maps/MapsActivity;->setLastMenuForTest(Landroid/view/Menu;)V

    .line 1648
    return v0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1741
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    if-nez v0, :cond_7

    .line 1785
    :cond_6
    :goto_6
    return-void

    .line 1749
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/android/maps/MapsActivity;Z)V

    .line 1752
    invoke-virtual {p0, p1}, Lcom/google/android/maps/MapsActivity;->setIntent(Landroid/content/Intent;)V

    .line 1756
    iput-boolean v4, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    .line 1758
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1759
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    .line 1760
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/high16 v0, 0x10

    and-int/2addr v0, v1

    if-nez v0, :cond_6

    .line 1770
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1778
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/android/maps/h;

    invoke-direct {v1, p0, v4, v3}, Lcom/google/android/maps/h;-><init>(Lcom/google/android/maps/MapsActivity;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    goto :goto_6
.end method

.method public onOfflineDataUpdate(LaT/l;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 2523
    invoke-virtual {p1}, LaT/l;->j()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p1}, LaT/l;->b()I

    move-result v0

    if-ne v0, v2, :cond_27

    invoke-virtual {p1}, LaT/l;->k()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p1}, LaT/l;->c()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_27

    .line 2526
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/r;

    invoke-direct {v1, p0}, Lcom/google/android/maps/r;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 2534
    :cond_27
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 1620
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 3
    .parameter

    .prologue
    .line 1653
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 1654
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapsActivity;->setLastMenuForTest(Landroid/view/Menu;)V

    .line 1655
    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1665
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/android/BaseMapsActivity;->onPanelClosed(ILandroid/view/Menu;)V

    .line 1666
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->o(Z)V

    .line 1667
    return-void
.end method

.method public onPause()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1371
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1373
    invoke-static {}, LaT/a;->k()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->b(LaT/m;)V

    .line 1376
    :cond_e
    invoke-static {}, Lcom/google/googlenav/u;->c()V

    .line 1377
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/aa;->b()V

    .line 1379
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    .line 1380
    if-eqz v0, :cond_64

    .line 1381
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    .line 1387
    :goto_27
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 1391
    sget-object v1, Lcom/google/android/maps/B;->g:Lcom/google/android/maps/B;

    iput-object v1, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    .line 1395
    iput-boolean v3, p0, Lcom/google/android/maps/MapsActivity;->shouldSetupEntryPointOnResume:Z

    .line 1397
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/ak;->b(Z)V

    .line 1399
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onPause()V

    .line 1402
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_48

    .line 1403
    invoke-static {p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;)V

    .line 1406
    :cond_48
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->m()Z

    move-result v1

    if-nez v1, :cond_5c

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->n()Z

    move-result v1

    if-nez v1, :cond_70

    .line 1407
    :cond_5c
    if-eqz v0, :cond_6a

    .line 1408
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    .line 1472
    :goto_63
    return-void

    .line 1383
    :cond_64
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    goto :goto_27

    .line 1410
    :cond_6a
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    goto :goto_63

    .line 1415
    :cond_70
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    if-eqz v1, :cond_a0

    .line 1416
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->k(Z)V

    .line 1420
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/googlenav/android/background/MapWorkerService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1421
    const-string v2, "com.google.googlenav.android.background.ON_PAUSE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1422
    const-string v2, "is_finishing"

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1423
    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1424
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->T()V

    .line 1429
    :cond_a0
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    if-eqz v1, :cond_a9

    .line 1430
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->gmmView:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->b()V

    .line 1433
    :cond_a9
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->pauseTransitNavigationView()V

    .line 1437
    new-instance v1, Lcom/google/android/maps/f;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/f;-><init>(Lcom/google/android/maps/MapsActivity;Las/c;)V

    iput-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    .line 1454
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v2, v3}, Las/d;->a(J)V

    .line 1455
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v1}, Las/d;->g()V

    .line 1457
    if-eqz v0, :cond_cc

    .line 1458
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPauseVm:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_63

    .line 1460
    :cond_cc
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsLifecycleOnPause:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_63
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter

    .prologue
    .line 1611
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1612
    sget-object v0, Lcom/google/android/maps/MapsActivity;->stopwatchStatsMenuOpen:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 1613
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->mapViewMenuController:Lcom/google/googlenav/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/as;->a(Landroid/view/Menu;)Z

    move-result v0

    .line 1615
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected onRestart()V
    .registers 3

    .prologue
    .line 1059
    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 1061
    sget-object v0, Lcom/google/android/maps/B;->e:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    .line 1063
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onRestart()V

    .line 1064
    return-void
.end method

.method public onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 1068
    const-string v0, "MapsActivity.onResume"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 1070
    iget-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_16

    .line 1071
    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 1074
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    if-nez v0, :cond_1e

    .line 1075
    sget-object v0, Lcom/google/android/maps/A;->a:Lcom/google/android/maps/A;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->entryPointType:Lcom/google/android/maps/A;

    .line 1078
    :cond_1e
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onResume()V

    .line 1080
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1085
    invoke-static {}, LaT/a;->k()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p0}, LaT/a;->a(LaT/m;)V

    .line 1088
    :cond_2e
    invoke-static {}, Lcom/google/googlenav/u;->b()V

    .line 1089
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/aa;->a()V

    .line 1090
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    .line 1093
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 1094
    invoke-static {p0, p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;Lcom/google/googlenav/android/Y;)V

    .line 1099
    :cond_48
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->o()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 1102
    new-instance v0, Lcom/google/android/maps/z;

    invoke-direct {v0, p0}, Lcom/google/android/maps/z;-><init>(Lcom/google/android/maps/MapsActivity;)V

    .line 1109
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2, v0, v4, v4}, Lcom/google/googlenav/android/i;->a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V

    .line 1115
    :cond_68
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->d()V

    .line 1118
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-nez v0, :cond_86

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->n()Z

    move-result v0

    if-eqz v0, :cond_86

    .line 1119
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->onResumeInternal()V

    .line 1122
    :cond_86
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/ak;->b(Z)V

    .line 1124
    const-string v0, "MapsActivity.onResume"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 1125
    return-void
.end method

.method public onSearchRequested()Z
    .registers 2

    .prologue
    .line 1625
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1626
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    .line 1627
    const/4 v0, 0x1

    .line 1629
    :goto_12
    return v0

    :cond_13
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_12
.end method

.method protected onStart()V
    .registers 4

    .prologue
    .line 688
    const-string v0, "MapsActivity.onStart"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 689
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onStart()V

    .line 692
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 693
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 694
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 697
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/maps/MapsActivity;->ENTER_CAR_MODE_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 699
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 701
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 705
    :cond_32
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->b()Lcom/google/googlenav/prefetch/android/g;

    move-result-object v0

    .line 707
    if-eqz v0, :cond_47

    .line 708
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    invoke-interface {v1, v0}, LaH/m;->a(LaH/A;)V

    .line 711
    :cond_47
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 712
    new-instance v1, Lcom/google/android/maps/v;

    invoke-direct {v1, p0}, Lcom/google/android/maps/v;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    .line 725
    const-string v0, "MapsActivity.onStart"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 726
    return-void
.end method

.method protected onStop()V
    .registers 3

    .prologue
    .line 730
    invoke-super {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->onStop()V

    .line 731
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 735
    invoke-static {}, Laj/a;->b()V

    .line 742
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->screenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 745
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->dockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 747
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 749
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->connReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 753
    :cond_21
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->b()Lcom/google/googlenav/prefetch/android/g;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_36

    .line 756
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    invoke-interface {v1, v0}, LaH/m;->b(LaH/A;)V

    .line 759
    :cond_36
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->stopPowerConsumers()V

    .line 760
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapsActivity;->arePowerConsumersRunning:Z

    .line 761
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    if-eqz v0, :cond_48

    .line 762
    iget-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    .line 763
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->stopPowerConsumersTask:Las/d;

    .line 765
    :cond_48
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 2281
    invoke-super {p0, p1}, Lcom/google/googlenav/android/BaseMapsActivity;->onWindowFocusChanged(Z)V

    .line 2282
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    if-nez p1, :cond_14

    move v0, v1

    :goto_b
    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->o(Z)V

    .line 2283
    if-eqz p1, :cond_13

    .line 2284
    invoke-static {v1}, Lcom/google/android/maps/MapsActivity;->setInputFocusForTesting(I)V

    .line 2286
    :cond_13
    return-void

    .line 2282
    :cond_14
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public processIntentAndStartSession()I
    .registers 4

    .prologue
    .line 1029
    const/4 v0, 0x0

    .line 1031
    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->intentProcessor:Lcom/google/googlenav/android/M;

    invoke-virtual {v1}, Lcom/google/googlenav/android/M;->a()I

    move-result v1

    .line 1042
    const/4 v2, -0x1

    if-eq v1, v2, :cond_e

    const/4 v2, -0x2

    if-eq v1, v2, :cond_e

    move v0, v1

    .line 1051
    :cond_e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/K;->a(I)V

    .line 1053
    return v1
.end method

.method public refreshFriends()V
    .registers 5

    .prologue
    .line 1962
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    .line 1963
    if-eqz v0, :cond_15

    .line 1964
    const/16 v1, 0x158

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/X;->a(IILjava/lang/Object;)Z

    .line 1968
    :cond_15
    return-void
.end method

.method public refreshFriendsSettings()V
    .registers 4

    .prologue
    .line 1973
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/k;

    invoke-direct {v2, p0}, Lcom/google/android/maps/k;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    .line 2011
    return-void
.end method

.method public resetForInvocation()V
    .registers 2

    .prologue
    .line 1789
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->at()V

    .line 1790
    return-void
.end method

.method public resolveType(Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1811
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1803
    invoke-static {p0, p1, p2, p3}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 1804
    return-void
.end method

.method public screenDrawn()V
    .registers 7

    .prologue
    const-wide/16 v4, -0x1

    .line 2309
    iget-wide v0, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_29

    .line 2310
    sget-object v0, Lcom/google/android/maps/MapsActivity;->startupClock:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 2311
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-eqz v1, :cond_54

    .line 2312
    sget-object v1, Lcom/google/android/maps/s;->b:[I

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-virtual {v2}, Lcom/google/android/maps/B;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_86

    .line 2364
    :cond_29
    :goto_29
    sget-object v0, Lcom/google/android/maps/B;->a:Lcom/google/android/maps/B;

    iput-object v0, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    .line 2365
    iput-wide v4, p0, Lcom/google/android/maps/MapsActivity;->startupStartTime:J

    .line 2366
    return-void

    .line 2314
    :pswitch_30
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStringsVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2317
    :pswitch_36
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupColdVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2320
    :pswitch_3c
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBackVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2323
    :pswitch_42
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHotVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2326
    :pswitch_48
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOnVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2329
    :pswitch_4e
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPauseVm:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2333
    :cond_54
    sget-object v1, Lcom/google/android/maps/s;->b:[I

    iget-object v2, p0, Lcom/google/android/maps/MapsActivity;->startupType:Lcom/google/android/maps/B;

    invoke-virtual {v2}, Lcom/google/android/maps/B;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_96

    goto :goto_29

    .line 2335
    :pswitch_62
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupRemoteStrings:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2338
    :pswitch_68
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupCold:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2341
    :pswitch_6e
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBack:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2344
    :pswitch_74
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupHot:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2347
    :pswitch_7a
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupScreenOn:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2350
    :pswitch_80
    sget-object v1, Lcom/google/android/maps/MapsActivity;->stopwatchStatsStartupAfterBriefPause:Lbm/i;

    invoke-virtual {v1, v0}, Lbm/i;->a(I)V

    goto :goto_29

    .line 2312
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_30
        :pswitch_36
        :pswitch_3c
        :pswitch_42
        :pswitch_48
        :pswitch_4e
    .end packed-switch

    .line 2333
    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_62
        :pswitch_68
        :pswitch_6e
        :pswitch_74
        :pswitch_7a
        :pswitch_80
    .end packed-switch
.end method

.method public searchFor(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 2229
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 2233
    return-void
.end method

.method public showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .registers 3
    .parameter

    .prologue
    .line 2116
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->d(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    return-object v0
.end method

.method public showFriend(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 2237
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    .line 2238
    if-eqz v0, :cond_13

    .line 2241
    invoke-virtual {v0, p1}, Lbf/X;->b(Ljava/lang/String;)V

    .line 2246
    const/4 v0, 0x1

    .line 2248
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public showStarDetails(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2111
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->k(Ljava/lang/String;)V

    .line 2112
    return-void
.end method

.method public showStarOnMap(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2106
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->j(Ljava/lang/String;)V

    .line 2107
    return-void
.end method

.method public startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 2079
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;I)V

    .line 2081
    return-void
.end method

.method public startBusinessDetailsLayer(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2073
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    .line 2075
    return-void
.end method

.method public startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2087
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aC;->a(Lcom/google/googlenav/ai;)V

    .line 2089
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;ILjava/lang/String;)V

    .line 2091
    return-void
.end method

.method public startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2095
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 2097
    return-void
.end method

.method public startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 2060
    .line 2062
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    .line 2069
    return-void
.end method

.method public startFriendsLayer(Lbf/am;)V
    .registers 4
    .parameter

    .prologue
    .line 1860
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    .line 1861
    return-void
.end method

.method public startFriendsLayerHistorySummary()V
    .registers 3

    .prologue
    .line 1869
    const/4 v0, 0x1

    .line 1870
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/ui/s;Z)V

    .line 1871
    return-void
.end method

.method public startFriendsListView(Lbf/am;)V
    .registers 4
    .parameter

    .prologue
    .line 2016
    const/16 v0, 0x13c

    invoke-virtual {p1, v0}, Lbf/am;->d(I)V

    .line 2017
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    .line 2018
    return-void
.end method

.method public startFriendsLocation(Lbf/am;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2022
    const/16 v0, 0x14b

    invoke-virtual {p1, v0, p2}, Lbf/am;->a(ILjava/lang/Object;)V

    .line 2023
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    .line 2024
    return-void
.end method

.method public startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/16 v7, 0xc7

    const/4 v6, 0x1

    .line 1878
    invoke-static {}, Lcom/google/googlenav/friend/ad;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1879
    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    .line 1881
    and-int/lit8 v1, v1, -0x3

    .line 1883
    new-instance v2, Lcom/google/android/maps/i;

    invoke-direct {v2, p0}, Lcom/google/android/maps/i;-><init>(Lcom/google/android/maps/MapsActivity;)V

    .line 1895
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    .line 1896
    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v4

    if-eqz v4, :cond_27

    .line 1897
    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 1900
    :cond_27
    new-instance v4, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v4}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0x50d

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0x61b

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/j;

    invoke-direct {v1, p0, v2, p2}, Lcom/google/android/maps/j;-><init>(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/friend/aQ;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 1951
    return-void
.end method

.method public startFriendsProfile(Lbf/am;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2028
    const/16 v0, 0x14a

    invoke-virtual {p1, v0, p2}, Lbf/am;->a(ILjava/lang/Object;)V

    .line 2029
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Z)V

    .line 2030
    return-void
.end method

.method public startLatitudeSettingsActivity()V
    .registers 4

    .prologue
    .line 2143
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/googlenav/settings/LatitudeSettingsActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/o;

    invoke-direct {v2, p0}, Lcom/google/android/maps/o;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    .line 2161
    return-void
.end method

.method public startLocationHistoryOptIn(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 2034
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "start_activity_on_complete"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 2036
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/maps/m;

    invoke-direct {v3, p0, v0}, Lcom/google/android/maps/m;-><init>(Lcom/google/android/maps/MapsActivity;Ljava/lang/Class;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(ZLcom/google/googlenav/ui/wizard/db;)V

    .line 2055
    return-void
.end method

.method public startManageAutoCheckinPlaces()V
    .registers 3

    .prologue
    .line 1956
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/el;)V

    .line 1957
    return-void
.end method

.method public startMyPlacesList(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1831
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    .line 1832
    return-void
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2390
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 2391
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2393
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2394
    const/high16 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2397
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_18
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 2399
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_50

    .line 2400
    if-nez v1, :cond_36

    :goto_34
    move-object v1, v0

    .line 2405
    goto :goto_18

    .line 2402
    :cond_36
    iget v5, v1, Landroid/content/pm/ResolveInfo;->priority:I

    iget v0, v0, Landroid/content/pm/ResolveInfo;->priority:I

    if-ne v5, v0, :cond_50

    .line 2409
    :goto_3c
    if-eqz v2, :cond_4e

    .line 2410
    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411
    invoke-virtual {p0, v3}, Lcom/google/android/maps/MapsActivity;->startActivity(Landroid/content/Intent;)V

    .line 2412
    const/4 v0, 0x1

    .line 2414
    :goto_4d
    return v0

    :cond_4e
    const/4 v0, 0x0

    goto :goto_4d

    :cond_50
    move-object v0, v1

    goto :goto_34

    :cond_52
    move-object v2, v1

    goto :goto_3c
.end method

.method public startOffersList()V
    .registers 4

    .prologue
    .line 1837
    sget-object v0, LaA/b;->k:LaA/c;

    sget v1, LaA/c;->a:I

    invoke-virtual {v0, v1}, LaA/c;->a(I)V

    .line 1841
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "i"

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    .line 1843
    return-void
.end method

.method public startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1853
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    .line 1855
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2183
    invoke-static {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(Landroid/content/Context;)V

    .line 2187
    invoke-virtual {p0}, Lcom/google/android/maps/MapsActivity;->getCurrentViewportDetails()Landroid/os/Bundle;

    move-result-object v0

    .line 2188
    if-eqz p3, :cond_c

    .line 2189
    invoke-virtual {v0, p3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2193
    :cond_c
    const-string v1, "searchIncludeInHistory"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 2198
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    .line 2205
    :goto_27
    return-void

    .line 2200
    :cond_28
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->o()Lbf/i;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/n;->a(Lcom/google/googlenav/ui/wizard/C;Lbf/i;)I

    move-result v1

    .line 2202
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbb/o;->d(I)V

    .line 2203
    invoke-super {p0, p1, p2, v0, p4}, Lcom/google/googlenav/android/BaseMapsActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    goto :goto_27
.end method

.method public startSettingsActivity()V
    .registers 4

    .prologue
    .line 2121
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/googlenav/settings/SettingsPreferenceActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/n;

    invoke-direct {v2, p0}, Lcom/google/android/maps/n;-><init>(Lcom/google/android/maps/MapsActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    .line 2139
    return-void
.end method

.method public startTransitEntry()V
    .registers 2

    .prologue
    .line 1847
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ay()V

    .line 1848
    return-void
.end method

.method public startTransitNavigationLayer()V
    .registers 2

    .prologue
    .line 1826
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    .line 1827
    return-void
.end method

.method public startTransitStationPage(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2101
    invoke-direct {p0}, Lcom/google/android/maps/MapsActivity;->getController()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->f(Ljava/lang/String;)V

    .line 2102
    return-void
.end method

.method public startVoiceRecognition(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 2253
    const-string v0, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapsActivity;->isIntentAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2254
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/MapsActivity;->voiceRecognizer:Lcom/google/googlenav/android/ad;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    .line 2256
    :cond_11
    return-void
.end method

.method public unlockScreenOrientation()V
    .registers 2

    .prologue
    .line 2517
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapsActivity;->setRequestedOrientation(I)V

    .line 2518
    return-void
.end method
