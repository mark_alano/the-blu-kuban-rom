.class public Lcom/google/android/maps/driveabout/app/NavigationMapView;
.super Lcom/google/android/maps/driveabout/vector/VectorMapView;
.source "SourceFile"


# static fields
.field private static A:I


# instance fields
.field private volatile b:LaH/h;

.field private volatile c:Landroid/location/Location;

.field private d:[LO/z;

.field private e:LO/z;

.field private f:Lcom/google/android/maps/driveabout/app/de;

.field private g:[Lcom/google/android/maps/driveabout/app/de;

.field private final h:Lcom/google/android/maps/driveabout/vector/aA;

.field private final i:Lcom/google/android/maps/driveabout/app/bT;

.field private final j:Lcom/google/android/maps/driveabout/vector/A;

.field private k:Lcom/google/android/maps/driveabout/vector/j;

.field private final l:Lcom/google/android/maps/driveabout/vector/A;

.field private final m:Lcom/google/android/maps/driveabout/vector/M;

.field private n:Lcom/google/android/maps/driveabout/app/dL;

.field private o:Lcom/google/android/maps/driveabout/vector/aZ;

.field private p:Lcom/google/android/maps/driveabout/vector/aZ;

.field private final q:Lcom/google/android/maps/driveabout/vector/bd;

.field private r:Lcom/google/android/maps/driveabout/app/bC;

.field private s:Lcom/google/android/maps/driveabout/app/bK;

.field private final t:Landroid/graphics/Bitmap;

.field private u:Landroid/graphics/Bitmap;

.field private v:Landroid/graphics/Bitmap;

.field private w:LF/H;

.field private x:Lcom/google/android/maps/driveabout/app/cv;

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 121
    const/4 v0, 0x0

    sput v0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 124
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    .line 120
    iput v6, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    .line 125
    const v0, 0x7f02012e

    invoke-static {p2, v0}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->t:Landroid/graphics/Bitmap;

    .line 126
    new-instance v0, Lcom/google/android/maps/driveabout/vector/j;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    invoke-direct {v0, p2, v1}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    .line 127
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    .line 129
    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    .line 133
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    new-array v1, v6, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v3, 0x7f02016e

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v3, 0x7f02016b

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    const v1, 0x7f0b000a

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x7f0c

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v3, 0x7f0c0001

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(FII)V

    .line 149
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 150
    new-instance v0, Lcom/google/android/maps/driveabout/app/bT;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bT;-><init>(Lcom/google/android/maps/driveabout/vector/aA;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    .line 152
    new-instance v0, Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    .line 153
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    const v1, -0x7f000001

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;->b(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    const/high16 v2, 0x6000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/q;I)V

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LA/c;->g:LA/c;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;LA/c;)Lcom/google/android/maps/driveabout/vector/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    .line 157
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 161
    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTrafficMode(I)V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    .line 172
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    .line 173
    invoke-virtual {p0, v6}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBaseDistancePenaltyFactorForLabelOverlay(I)V

    .line 175
    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setFocusable(Z)V

    .line 176
    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setFocusableInTouchMode(Z)V

    .line 177
    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setEnabled(Z)V

    .line 178
    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowTiltGesture(Z)V

    .line 179
    invoke-virtual {p0, v4}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowRotateGesture(Z)V

    .line 180
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    .line 181
    sget-object v0, LG/a;->s:LG/a;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDefaultLabelTheme(LG/a;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLabelTheme(LG/a;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m_()V

    .line 184
    return-void
.end method

.method private B()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 418
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    if-ne v2, v0, :cond_19

    .line 419
    :goto_6
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    if-eqz v2, :cond_1b

    .line 421
    :goto_a
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 422
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/de;->b(Z)V

    .line 421
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_19
    move v0, v1

    .line 418
    goto :goto_6

    .line 425
    :cond_1b
    return-void
.end method

.method private a(LO/N;LO/N;Lo/X;)Lo/af;
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 607
    invoke-virtual {p1}, LO/N;->x()LO/P;

    move-result-object v0

    .line 608
    if-eqz v0, :cond_12

    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_14

    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_14

    .line 610
    :cond_12
    const/4 v0, 0x0

    .line 643
    :goto_13
    return-object v0

    .line 614
    :cond_14
    invoke-virtual {p1}, LO/N;->z()I

    move-result v1

    .line 615
    invoke-virtual {p2}, LO/N;->z()I

    move-result v2

    .line 616
    new-instance v3, Lo/am;

    invoke-direct {v3, p3, v1, v2}, Lo/am;-><init>(Lo/X;II)V

    invoke-virtual {v3}, Lo/am;->e()Lo/X;

    move-result-object v11

    .line 620
    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    .line 621
    const/4 v2, 0x0

    .line 622
    const/4 v1, 0x2

    .line 623
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3a

    .line 624
    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v4

    .line 625
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    .line 626
    const/4 v1, 0x3

    .line 630
    :cond_3a
    new-instance v9, Lo/H;

    new-instance v0, Lo/I;

    const/4 v3, 0x3

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lo/b;->b:Lo/b;

    invoke-direct {v9, v0, v1}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    .line 642
    const/4 v0, 0x0

    new-array v10, v0, [I

    .line 643
    new-instance v0, Lo/af;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v3, v2, [Lo/H;

    const/4 v2, 0x0

    aput-object v9, v3, v2

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v4

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    invoke-direct/range {v0 .. v10}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    goto :goto_13
.end method

.method private b(Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/f;
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x20

    const/16 v3, 0xa

    .line 662
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 664
    const v1, 0x7f040039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 667
    const v1, 0x7f1000f9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 668
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 669
    const v1, 0x7f1000fa

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 670
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_47

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_52

    .line 671
    :cond_47
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    :goto_4c
    new-instance v1, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    return-object v1

    .line 673
    :cond_52
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4c
.end method

.method public static setDefaultTrafficMode(I)V
    .registers 1
    .parameter

    .prologue
    .line 404
    sput p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    .line 405
    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    invoke-virtual {v0}, LO/z;->d()I

    move-result v0

    goto :goto_5
.end method

.method public a(LO/z;)V
    .registers 8
    .parameter

    .prologue
    .line 458
    invoke-virtual {p1}, LO/z;->p()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3dcccccd

    mul-float/2addr v1, v0

    .line 460
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 461
    const/4 v0, 0x0

    :goto_f
    invoke-virtual {p1}, LO/z;->k()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3a

    .line 462
    invoke-virtual {p1, v0}, LO/z;->a(I)LO/N;

    move-result-object v3

    .line 463
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v4}, LO/z;->a(I)LO/N;

    move-result-object v4

    .line 464
    invoke-virtual {v4}, LO/N;->e()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v5, v1

    if-lez v5, :cond_37

    .line 465
    invoke-virtual {p1}, LO/z;->n()Lo/X;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/N;LO/N;Lo/X;)Lo/af;

    move-result-object v3

    .line 467
    if-eqz v3, :cond_37

    .line 468
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    :cond_37
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 472
    :cond_3a
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setImportantLabelFeatures(Ljava/util/List;)V

    .line 473
    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    if-ne v0, p1, :cond_d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 365
    :cond_c
    :goto_c
    return-void

    .line 332
    :cond_d
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Z)V

    .line 333
    if-eqz p2, :cond_c

    .line 334
    array-length v0, p2

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/de;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    .line 335
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    .line 336
    const/4 v0, 0x0

    :goto_1b
    array-length v1, p2

    if-ge v0, v1, :cond_5a

    .line 337
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    new-instance v2, Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v4, p2, v0

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/de;-><init>(Landroid/content/res/Resources;LO/z;)V

    aput-object v2, v1, v0

    .line 338
    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_37

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_44

    :cond_37
    aget-object v1, p2, v0

    if-ne v1, p1, :cond_44

    .line 341
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 343
    :cond_44
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 344
    aget-object v1, p2, v0

    if-ne p1, v1, :cond_57

    .line 345
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    .line 346
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    .line 336
    :cond_57
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 349
    :cond_5a
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->B()V

    .line 351
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 352
    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 354
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 356
    new-instance v0, Lcom/google/android/maps/driveabout/app/cu;

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->t:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d0006

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cu;-><init>(Lo/u;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    .line 363
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto/16 :goto_c
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bd;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 277
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 3
    .parameter

    .prologue
    .line 567
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/f;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 568
    return-void
.end method

.method public a(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 438
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    if-eqz v0, :cond_1d

    .line 439
    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    array-length v1, v1

    if-ge v0, v1, :cond_15

    .line 440
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 439
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 442
    :cond_15
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->g:[Lcom/google/android/maps/driveabout/app/de;

    .line 443
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    .line 444
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->d:[LO/z;

    .line 445
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->e:LO/z;

    .line 447
    :cond_1d
    if-eqz p1, :cond_2c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    if-eqz v0, :cond_2c

    .line 448
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    .line 449
    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->w:LF/H;

    .line 451
    :cond_2c
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bd;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 281
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMyLocation(LaH/h;Z)V

    .line 521
    return-void
.end method

.method public c()LA/c;
    .registers 2

    .prologue
    .line 680
    sget-object v0, LA/c;->b:LA/c;

    return-object v0
.end method

.method public m_()V
    .registers 4

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 578
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 579
    new-instance v1, Lcom/google/android/maps/driveabout/vector/j;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    .line 580
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_20

    .line 581
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 583
    :cond_20
    return-void
.end method

.method public onSizeChanged(IIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 188
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->onSizeChanged(IIII)V

    .line 189
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    if-eqz v0, :cond_2a

    .line 190
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    .line 193
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    .line 194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    move v1, p1

    move v2, p2

    move v5, v4

    move v6, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/cv;->a(IIFIIII)V

    .line 197
    :cond_2a
    return-void
.end method

.method public setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 500
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 502
    if-eqz p2, :cond_21

    .line 503
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    if-nez v0, :cond_20

    .line 504
    const/4 v0, 0x1

    .line 505
    const/4 v1, 0x0

    .line 506
    sget-object v2, LA/c;->j:LA/c;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 508
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 517
    :cond_20
    :goto_20
    return-void

    .line 511
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_20

    .line 512
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 513
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_20
.end method

.method public setCompassMargins(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    .line 455
    return-void
.end method

.method public setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V
    .registers 3
    .parameter

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/j;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 587
    return-void
.end method

.method public setController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 595
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setController(Lcom/google/android/maps/driveabout/vector/bk;)V

    .line 596
    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/cq;

    if-eqz v0, :cond_e

    .line 597
    check-cast p1, Lcom/google/android/maps/driveabout/app/cq;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/bT;)V

    .line 600
    :cond_e
    return-void
.end method

.method public setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 4
    .parameter

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v0, v1, :cond_c

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v0, :cond_1c

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    if-eq v0, p1, :cond_1c

    .line 480
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 481
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 484
    :cond_1c
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v0, :cond_3d

    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    if-nez v0, :cond_39

    .line 486
    sget-object v0, LA/c;->d:LA/c;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 488
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 496
    :cond_39
    :goto_39
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 497
    return-void

    .line 491
    :cond_3d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_39

    .line 492
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 493
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o:Lcom/google/android/maps/driveabout/vector/aZ;

    goto :goto_39
.end method

.method public setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V
    .registers 5
    .parameter

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    if-eq p1, v0, :cond_29

    .line 285
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    if-eqz v0, :cond_d

    .line 286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 288
    :cond_d
    new-instance v0, Lcom/google/android/maps/driveabout/app/bK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    sget-object v2, LA/c;->h:LA/c;

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/bK;-><init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/NavigationMapView;Lcom/google/android/maps/driveabout/vector/A;LA/c;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    .line 289
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/android/maps/driveabout/app/bF;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->s:Lcom/google/android/maps/driveabout/app/bK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 292
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    .line 294
    :cond_29
    return-void
.end method

.method public setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 3
    .parameter

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 591
    return-void
.end method

.method public setMyLocation(LaH/h;Z)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 524
    if-eqz p1, :cond_14

    .line 525
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/bT;->a(Z)V

    .line 526
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    if-eq p1, v0, :cond_14

    .line 527
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bT;->a(LaH/h;)V

    .line 528
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b:LaH/h;

    .line 531
    :cond_14
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    if-eqz v0, :cond_a6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_a6

    .line 536
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v9

    .line 537
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    const/high16 v1, 0x44fa

    invoke-static {v0, v1}, Lo/T;->a(FF)Lo/T;

    move-result-object v0

    .line 538
    invoke-virtual {v9, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    .line 539
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 540
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->e()V

    .line 541
    const/high16 v0, 0x7f0b

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 542
    const v0, 0x7f0b0001

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 543
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    if-nez v0, :cond_63

    .line 544
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200fe

    invoke-static {v0, v2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    .line 546
    :cond_63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    if-nez v0, :cond_74

    .line 547
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200f6

    invoke-static {v0, v2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    .line 550
    :cond_74
    iget-object v11, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, LF/H;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->u:Landroid/graphics/Bitmap;

    const-string v6, ""

    const-string v7, ""

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    .line 552
    const v0, 0x7f0b0002

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 553
    const v0, 0x7f0b0003

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 554
    iget-object v10, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, LF/H;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->v:Landroid/graphics/Bitmap;

    const-string v6, ""

    const-string v7, ""

    move-object v1, v9

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    .line 556
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setModelChanged()V

    .line 559
    :cond_a6
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q_()V

    .line 560
    return-void
.end method

.method public setOnSizeChangedListener(Lcom/google/android/maps/driveabout/app/cv;)V
    .registers 2
    .parameter

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->x:Lcom/google/android/maps/driveabout/app/cv;

    .line 201
    return-void
.end method

.method public setRawLocation(Landroid/location/Location;)V
    .registers 2
    .parameter

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->c:Landroid/location/Location;

    .line 564
    return-void
.end method

.method public setTrafficMode(I)V
    .registers 4
    .parameter

    .prologue
    .line 375
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    .line 376
    sget p1, Lcom/google/android/maps/driveabout/app/NavigationMapView;->A:I

    .line 378
    :cond_5
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    if-eq p1, v0, :cond_29

    .line 379
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    if-eqz v0, :cond_12

    .line 380
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(I)V

    .line 382
    :cond_12
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2a

    .line 383
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 387
    :goto_1a
    const/4 v0, 0x1

    if-ne p1, v0, :cond_30

    .line 388
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    const/high16 v1, 0x41f0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->a(F)V

    .line 392
    :goto_24
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->z:I

    .line 393
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->B()V

    .line 395
    :cond_29
    return-void

    .line 385
    :cond_2a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1a

    .line 390
    :cond_30
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q:Lcom/google/android/maps/driveabout/vector/bd;

    const/high16 v1, 0x4170

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->a(F)V

    goto :goto_24
.end method

.method public setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 309
    if-eqz p1, :cond_2b

    if-eqz p2, :cond_2b

    .line 310
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    if-nez v0, :cond_1f

    .line 311
    new-instance v0, Lcom/google/android/maps/driveabout/app/dL;

    invoke-direct {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dL;-><init>(LO/z;LO/N;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    .line 315
    :goto_f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/dL;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 316
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_25

    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 324
    :goto_1e
    return-void

    .line 313
    :cond_1f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dL;->a(LO/z;LO/N;)V

    goto :goto_f

    .line 319
    :cond_25
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1e

    .line 322
    :cond_2b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1e
.end method

.method public setViewMode(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, 0x1

    .line 208
    .line 209
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-ne v0, v5, :cond_a5

    if-eq p1, v5, :cond_a5

    .line 212
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 214
    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowScroll(Z)V

    .line 215
    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowZoomGestures(Z)V

    .line 216
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v0, :cond_a3

    .line 217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    .line 221
    :goto_26
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    if-eqz v3, :cond_2f

    .line 222
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 225
    :cond_2f
    :goto_2f
    iget v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-ne v3, v6, :cond_4d

    if-eq p1, v6, :cond_4d

    .line 228
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 229
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v3, :cond_46

    .line 230
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    .line 235
    :cond_46
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 238
    :cond_4d
    iget v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-eq v3, v5, :cond_74

    if-ne p1, v5, :cond_74

    .line 241
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 242
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->k:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 243
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v3, :cond_69

    .line 244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v0, v1

    .line 248
    :cond_69
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowScroll(Z)V

    .line 249
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setAllowZoomGestures(Z)V

    .line 250
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n:Lcom/google/android/maps/driveabout/app/dL;

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 252
    :cond_74
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    if-eq v2, v6, :cond_8b

    if-ne p1, v6, :cond_8b

    .line 255
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    if-eqz v2, :cond_9a

    .line 256
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/de;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 262
    :goto_85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    move v0, v1

    .line 265
    :cond_8b
    if-eqz v0, :cond_97

    .line 268
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->f:Lcom/google/android/maps/driveabout/app/de;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 272
    :cond_97
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->y:I

    .line 273
    return-void

    .line 260
    :cond_9a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m:Lcom/google/android/maps/driveabout/vector/M;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->e:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    move v1, v0

    goto :goto_85

    :cond_a3
    move v0, v2

    goto :goto_26

    :cond_a5
    move v0, v2

    goto :goto_2f
.end method
