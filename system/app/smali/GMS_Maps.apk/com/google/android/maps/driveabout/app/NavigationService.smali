.class public Lcom/google/android/maps/driveabout/app/NavigationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:J


# instance fields
.field private A:Lcom/google/android/maps/driveabout/app/a;

.field private b:Lcom/google/android/maps/driveabout/app/dc;

.field private c:Law/p;

.field private d:LO/t;

.field private e:Lcom/google/android/maps/driveabout/app/dM;

.field private f:Lcom/google/android/maps/driveabout/app/aQ;

.field private g:Lcom/google/android/maps/driveabout/app/cS;

.field private h:Lcom/google/android/maps/driveabout/app/cT;

.field private i:LM/n;

.field private j:LL/j;

.field private k:LL/G;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/maps/driveabout/app/af;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;

.field private final t:Landroid/content/BroadcastReceiver;

.field private final u:Landroid/content/BroadcastReceiver;

.field private final v:Landroid/content/BroadcastReceiver;

.field private w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

.field private x:LO/q;

.field private final y:Landroid/content/ServiceConnection;

.field private final z:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 106
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    .line 158
    new-instance v0, Lcom/google/android/maps/driveabout/app/cD;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/cD;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/cw;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    .line 161
    new-instance v0, Lcom/google/android/maps/driveabout/app/cw;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cw;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    .line 178
    new-instance v0, Lcom/google/android/maps/driveabout/app/cx;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cx;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    .line 190
    new-instance v0, Lcom/google/android/maps/driveabout/app/cy;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cy;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->y:Landroid/content/ServiceConnection;

    .line 243
    new-instance v0, Lcom/google/android/maps/driveabout/app/cE;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cE;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->z:Landroid/os/IBinder;

    return-void
.end method

.method private static a(Landroid/content/res/Configuration;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 602
    if-nez p0, :cond_c

    move v1, v0

    .line 605
    :goto_4
    and-int/lit8 v2, v1, 0xf

    const/4 v3, 0x3

    if-ne v2, v3, :cond_b

    .line 606
    and-int/lit8 v0, v1, 0x30

    .line 608
    :cond_b
    return v0

    .line 602
    :cond_c
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->x:LO/q;

    return-object p1
.end method

.method private a(Ljava/lang/String;ZI)Landroid/content/Intent;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 805
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 807
    invoke-static {v7}, Lcom/google/android/maps/driveabout/app/dp;->a(Z)V

    .line 809
    new-instance v6, LL/O;

    invoke-direct {v6, p0, v1}, LL/O;-><init>(Landroid/content/Context;Ljava/io/File;)V

    .line 810
    invoke-virtual {v6}, LL/O;->i()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 813
    :cond_14
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    if-eqz v0, :cond_22

    .line 814
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    .line 818
    :goto_1d
    :try_start_1d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->join()V
    :try_end_22
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_22} :catch_9d

    .line 825
    :cond_22
    new-instance v0, LL/G;

    invoke-direct {v0}, LL/G;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    .line 826
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0, v2}, LM/n;->a(LL/G;)V

    .line 830
    if-eqz p2, :cond_6f

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 832
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v3

    .line 834
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    new-instance v2, LL/I;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    invoke-direct {v2, v4, v3}, LL/I;-><init>(Lcom/google/android/maps/driveabout/app/dM;Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v2}, LL/G;->a(LL/P;)V

    .line 837
    new-instance v0, LL/b;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct/range {v0 .. v5}, LL/b;-><init>(Ljava/lang/String;LL/k;Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 840
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v1, v0}, LL/G;->a(LL/b;)V

    .line 843
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v1, v7}, LL/G;->a(Z)V

    .line 844
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    new-instance v2, Lcom/google/android/maps/driveabout/app/cC;

    invoke-direct {v2, p0, v0}, Lcom/google/android/maps/driveabout/app/cC;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;LL/b;)V

    invoke-virtual {v1, v2}, LL/G;->a(LL/H;)V

    .line 852
    :cond_6f
    if-ltz p3, :cond_87

    .line 853
    if-nez p3, :cond_7b

    .line 854
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int p3, v0

    .line 856
    :cond_7b
    new-instance v0, LL/M;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    int-to-long v2, p3

    const/16 v4, 0x1e

    const/16 v5, 0xa

    invoke-direct/range {v0 .. v5}, LL/M;-><init>(Lcom/google/android/maps/driveabout/app/dM;JII)V

    .line 858
    :cond_87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0, v6}, LL/G;->a(Ll/f;)V

    .line 860
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v6}, LL/O;->g()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 861
    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 862
    return-object v0

    .line 820
    :catch_9d
    move-exception v0

    goto/16 :goto_1d
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/aQ;
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 757
    const-string v0, "DriveAbout"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 759
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 760
    const-string v1, "InterruptedDestination"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 761
    const-string v1, "InterruptedChimeBeforeSpeech"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 762
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 763
    return-void
.end method

.method private a(Landroid/content/Intent;II)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 430
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->v()V

    .line 431
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a()V

    .line 437
    if-nez p1, :cond_2b

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "f:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 441
    const-string v1, "NavigationService:onStartInternal"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    :cond_2a
    :goto_2a
    return-void

    .line 447
    :cond_2b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_56

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_56

    const/4 v0, 0x1

    .line 449
    :goto_3e
    const-string v1, "event_log"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 450
    if-eqz v2, :cond_58

    const/4 v1, 0x1

    .line 451
    :goto_47
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    .line 453
    if-eqz v0, :cond_5a

    if-nez v1, :cond_5a

    .line 456
    const-string v0, "NavigationService"

    const-string v1, "onStartInternal. Received a \"com.google.android.maps.driveabout.REPLAY_LOG\" intent without an associated \"event_log\" extra."

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2a

    .line 447
    :cond_56
    const/4 v0, 0x0

    goto :goto_3e

    .line 450
    :cond_58
    const/4 v1, 0x0

    goto :goto_47

    .line 459
    :cond_5a
    if-nez v0, :cond_f3

    if-eqz v1, :cond_f3

    .line 462
    const-string v0, "NavigationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartInternal. Ignoring \"event_log\" extra in a \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" intent. Event logs should only be specified in a \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" intent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string v0, "event_log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 478
    :cond_91
    :goto_91
    const/4 v0, 0x1

    .line 479
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_dd

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_dd

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_dd

    .line 482
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v5

    .line 483
    new-instance v7, Lcom/google/android/maps/driveabout/app/bn;

    invoke-direct {v7, v5}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    .line 484
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->a()Z

    move-result v1

    if-eqz v1, :cond_dd

    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->e()V

    .line 486
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->i()I

    move-result v6

    .line 487
    const/4 v0, 0x2

    if-ne v6, v0, :cond_116

    .line 488
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->j()Z

    move-result v0

    if-eqz v0, :cond_1f4

    .line 496
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->g()V

    move v0, v6

    .line 578
    :cond_dd
    :goto_dd
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    .line 580
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-nez v1, :cond_205

    const/4 v1, 0x3

    if-eq v0, v1, :cond_205

    .line 587
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    goto/16 :goto_2a

    .line 466
    :cond_f3
    if-eqz v0, :cond_91

    if-eqz v1, :cond_91

    .line 469
    const-string v0, "is_feature_test"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    .line 470
    const-string v0, "random_ui_seed"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 472
    :try_start_107
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;ZI)Landroid/content/Intent;
    :try_end_10c
    .catch Ljava/lang/RuntimeException; {:try_start_107 .. :try_end_10c} :catch_10e

    move-result-object p1

    goto :goto_91

    .line 473
    :catch_10e
    move-exception v0

    .line 474
    const-string v1, "Unable to replay the event log: "

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_91

    .line 498
    :cond_116
    const/4 v0, 0x3

    if-ne v6, v0, :cond_144

    .line 500
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    .line 501
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->b(I)V

    .line 502
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(I)V

    .line 503
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v0, :cond_1f4

    .line 504
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v1, LL/A;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/j;->a(Ll/j;)V

    move v0, v6

    goto :goto_dd

    .line 507
    :cond_144
    const/4 v0, 0x6

    if-ne v6, v0, :cond_14c

    .line 509
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    goto/16 :goto_2a

    .line 513
    :cond_14c
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->d()[LO/U;

    move-result-object v1

    .line 514
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v2

    .line 515
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->g()[LO/b;

    move-result-object v3

    .line 516
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->h()Ljava/lang/String;

    move-result-object v4

    .line 517
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->s:Ljava/lang/String;

    .line 519
    const/4 v0, 0x5

    if-ne v6, v0, :cond_191

    .line 523
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->c()LO/U;

    move-result-object v0

    .line 524
    const/4 v5, 0x2

    new-array v5, v5, [LO/U;

    const/4 v7, 0x0

    aput-object v0, v5, v7

    const/4 v0, 0x1

    array-length v7, v1

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v1, v7

    aput-object v7, v5, v0

    .line 525
    invoke-static {p0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    .line 526
    invoke-virtual {v0, v5}, LO/M;->a([LO/U;)LO/a;

    move-result-object v5

    .line 528
    invoke-virtual {v0}, LO/M;->d()V

    .line 529
    if-eqz v5, :cond_18e

    .line 530
    const-string v0, "~"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 531
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V

    :cond_18e
    move v0, v6

    .line 536
    goto/16 :goto_dd

    .line 541
    :cond_191
    const-string v0, "UserRequestedReroute"

    const/4 v8, 0x0

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 543
    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/app/cS;->l()Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/app/cT;->b()Landroid/content/Intent;

    move-result-object v8

    .line 545
    if-nez v0, :cond_1b6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f4

    invoke-virtual {v8}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f4

    .line 547
    :cond_1b6
    invoke-direct {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;)V

    .line 551
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->k()Z

    move-result v0

    if-eqz v0, :cond_1d8

    .line 552
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->b()LO/U;

    move-result-object v0

    .line 553
    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v5

    .line 554
    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v7

    .line 555
    if-eqz v5, :cond_1d8

    .line 556
    invoke-virtual {v5}, LO/V;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    if-nez v7, :cond_1f7

    const/4 v0, 0x0

    :goto_1d5
    invoke-static {p0, v5, v8, v0}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 563
    :cond_1d8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/a;->b(I)V

    .line 564
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v0, :cond_1f4

    .line 567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v1, LL/A;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/j;->a(Ll/j;)V

    :cond_1f4
    move v0, v6

    goto/16 :goto_dd

    .line 556
    :cond_1f7
    new-instance v0, LaN/B;

    invoke-virtual {v7}, Lo/u;->a()I

    move-result v9

    invoke-virtual {v7}, Lo/u;->a()I

    move-result v7

    invoke-direct {v0, v9, v7}, LaN/B;-><init>(II)V

    goto :goto_1d5

    .line 591
    :cond_205
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    .line 595
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-nez v0, :cond_2a

    .line 596
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->d()V

    goto/16 :goto_2a
.end method

.method private a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 371
    if-nez p1, :cond_31

    move-object v1, v0

    .line 373
    :goto_4
    if-nez p1, :cond_36

    .line 390
    :goto_6
    new-instance v2, Lcom/google/android/maps/driveabout/app/cB;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cB;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    .line 397
    invoke-static {}, Lcom/google/android/maps/driveabout/power/a;->a()Lcom/google/android/maps/driveabout/power/f;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/power/f;->a(Lcom/google/android/maps/driveabout/power/i;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/power/f;->a(Landroid/view/Window;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/power/f;->a(Lcom/google/android/maps/driveabout/power/e;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/power/f;->a()V

    .line 401
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PowerManagerEnabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 406
    if-nez v0, :cond_30

    .line 407
    const-string v0, "UserPreference"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    .line 409
    :cond_30
    return-void

    .line 371
    :cond_31
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    goto :goto_4

    .line 373
    :cond_36
    new-instance v0, Lcom/google/android/maps/driveabout/app/cA;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/cA;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    goto :goto_6
.end method

.method private a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 747
    const-string v0, "InterruptedDestination"

    invoke-static {p0, v0, p1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    return p1
.end method

.method private b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    .line 923
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->c()V

    .line 924
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 925
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationService;)Z
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationService;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->m:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .registers 1
    .parameter

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->m()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->x:LO/q;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/a;
    .registers 2
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    return-object v0
.end method

.method private m()V
    .registers 2

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->m:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    if-eqz v0, :cond_1b

    .line 217
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->V()V

    .line 218
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 219
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->U()V

    .line 231
    :cond_1a
    :goto_1a
    return-void

    .line 222
    :cond_1b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->W()V

    .line 223
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->Q()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 224
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 225
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    goto :goto_1a

    .line 227
    :cond_3a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->S()V

    goto :goto_1a
.end method

.method private n()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 771
    const-string v0, "InterruptedDestination"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LR/s;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 773
    const-string v1, "InterruptedChimeBeforeSpeech"

    invoke-static {p0, v1, v4}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 778
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    .line 779
    if-eqz v0, :cond_32

    .line 780
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 781
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 782
    const-string v0, "UserRequestedReroute"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 783
    invoke-virtual {p0, v2, v4}, Lcom/google/android/maps/driveabout/app/NavigationService;->onStart(Landroid/content/Intent;I)V

    .line 784
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Z)V

    .line 790
    :goto_31
    return-void

    .line 788
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->stopSelf()V

    goto :goto_31
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 617
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/res/Configuration;)I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_17

    .line 619
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(I)V

    .line 626
    :goto_16
    return-void

    .line 620
    :cond_17
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/res/Configuration;)I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_26

    .line 622
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(I)V

    goto :goto_16

    .line 624
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->L()V

    goto :goto_16
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(Z)V

    .line 638
    const-string v0, "InterruptedChimeBeforeSpeech"

    invoke-static {p0, v0, p1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 640
    return-void
.end method

.method public a([LO/U;I[LO/b;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 929
    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/U;I[LO/b;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 930
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 931
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v1, :cond_1e

    .line 932
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v2, LL/A;

    invoke-direct {v2, v0}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LL/j;->a(Ll/j;)V

    .line 934
    :cond_1e
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    .line 935
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;)V

    .line 937
    :cond_23
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 648
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    return v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 659
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-nez v0, :cond_10

    .line 661
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->b()V

    .line 662
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 664
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    .line 666
    :cond_10
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 677
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_19

    .line 678
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    .line 682
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 684
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 690
    if-eqz v0, :cond_19

    .line 691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    .line 694
    :cond_19
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 710
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    if-nez v0, :cond_9

    .line 711
    const-string v0, "Activity paused, no service. "

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b(Ljava/lang/String;)V

    .line 713
    :cond_9
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->c()V

    .line 722
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 729
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_e

    .line 732
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    .line 736
    :goto_d
    return-void

    .line 734
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->stopSelf()V

    goto :goto_d
.end method

.method public h()Lcom/google/android/maps/driveabout/app/dM;
    .registers 2

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 945
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    return v0
.end method

.method public k()LO/U;
    .registers 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    if-eqz v0, :cond_b

    .line 953
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    .line 955
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public l()I
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    if-eqz v0, :cond_b

    .line 964
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    .line 966
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->z:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .registers 12

    .prologue
    const/4 v10, 0x1

    .line 248
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    .line 249
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 253
    new-instance v0, Lcom/google/android/maps/driveabout/app/af;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->o:Lcom/google/android/maps/driveabout/app/af;

    .line 255
    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dc;->a(Landroid/content/Context;)Lcom/google/android/maps/driveabout/app/dc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->b:Lcom/google/android/maps/driveabout/app/dc;

    .line 256
    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    .line 259
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    invoke-interface {v0, v10}, Law/p;->a(Z)V

    .line 263
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Law/p;->c(I)V

    .line 270
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/cz;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cz;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    invoke-static {v1, v0, v2, v10}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    .line 280
    invoke-static {v10}, Lcom/google/android/maps/driveabout/app/dp;->a(Z)V

    .line 281
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->b(Z)V

    .line 284
    invoke-static {v1}, LO/t;->a(Landroid/content/Context;)LO/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    .line 287
    new-instance v0, LM/n;

    invoke-direct {v0, v1}, LM/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    .line 288
    new-instance v0, Lcom/google/android/maps/driveabout/app/dM;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dM;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    .line 290
    new-instance v0, Lcom/google/android/maps/driveabout/app/a;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/a;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    .line 292
    new-instance v3, Lcom/google/android/maps/driveabout/app/bC;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    invoke-direct {v3, v1, v0}, Lcom/google/android/maps/driveabout/app/bC;-><init>(Landroid/content/Context;Law/p;)V

    .line 294
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 295
    new-instance v0, LL/j;

    invoke-direct {v0, v1}, LL/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    .line 296
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "gps"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    .line 299
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "network"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "android_orientation"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-static {v0}, Ll/f;->a(Ll/f;)V

    .line 308
    :cond_9b
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/cT;->a(Landroid/app/Service;)Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    .line 309
    new-instance v0, Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/aQ;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    .line 310
    new-instance v0, Lcom/google/android/maps/driveabout/app/cS;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    new-instance v5, Lcom/google/android/maps/driveabout/vector/bB;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/maps/driveabout/vector/bB;-><init>(Law/p;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cS;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/cT;Lcom/google/android/maps/driveabout/vector/bB;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    .line 316
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    new-instance v2, Lcom/google/android/maps/driveabout/app/dv;

    invoke-direct {v2}, Lcom/google/android/maps/driveabout/app/dv;-><init>()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v1, v0, v2, v3, v4}, LQ/p;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/aQ;LO/t;)LQ/p;

    move-result-object v9

    .line 318
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    iget-object v7, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->o:Lcom/google/android/maps/driveabout/app/af;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    move-object v3, p0

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/maps/driveabout/app/aQ;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/dM;LO/t;LM/n;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/a;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dM;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    move-object v5, v9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/dM;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;Ll/f;LQ/p;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 325
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 331
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DOCK_EVENT"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 332
    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 333
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 340
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    .line 342
    if-nez v0, :cond_11d

    .line 343
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->n()V

    .line 352
    :cond_11d
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->D()Z

    move-result v2

    if-nez v2, :cond_132

    .line 353
    invoke-static {v1}, Lcom/google/android/maps/driveabout/power/BatteryMonitor;->a(Landroid/content/Context;)V

    .line 354
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 355
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(LO/t;)V

    .line 366
    :cond_132
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    .line 367
    iput-boolean v10, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    .line 368
    return-void
.end method

.method public onDestroy()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 867
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 870
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    if-eqz v0, :cond_f

    .line 871
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    .line 872
    iput-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    .line 874
    :cond_f
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Context;)V

    .line 875
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 876
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 877
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 881
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    invoke-virtual {v0}, LM/n;->k()V

    .line 882
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->l()V

    .line 883
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->b:Lcom/google/android/maps/driveabout/app/dc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dc;->d()V

    .line 884
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-virtual {v0}, LO/t;->a()V

    .line 886
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(LO/t;)V

    .line 888
    const-string v0, "g"

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/maps/driveabout/app/NavigationService;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 891
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->ag()F

    move-result v2

    .line 893
    long-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->s:Ljava/lang/String;

    float-to-int v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dq;->a(ILjava/lang/String;I)V

    .line 895
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_63

    .line 896
    const-string v0, "Service destroyed and activity paused. "

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b(Ljava/lang/String;)V

    .line 900
    :cond_63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    .line 901
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    .line 903
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->a()V

    .line 904
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    .line 905
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 416
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Intent;II)V

    .line 417
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 422
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Intent;II)V

    .line 425
    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .registers 2
    .parameter

    .prologue
    .line 913
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    .line 914
    return-void
.end method
