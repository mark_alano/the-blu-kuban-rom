.class public abstract Lcom/google/android/maps/driveabout/app/T;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/google/android/maps/driveabout/app/bR;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/os/Handler;

.field protected c:I

.field protected d:LaH/h;

.field private e:Ljava/util/ArrayList;

.field private f:Ljava/util/ArrayList;

.field private g:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 242
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    .line 238
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/T;->c:I

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->d:LaH/h;

    .line 243
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    .line 244
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->b:Landroid/os/Handler;

    .line 245
    return-void
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f1000e2

    const/high16 v8, -0x4080

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 432
    instance-of v0, p1, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_12

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_20

    .line 433
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 436
    :cond_20
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 437
    if-eqz p2, :cond_88

    .line 438
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 444
    :goto_2e
    const v0, 0x7f1000e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 445
    if-eqz p3, :cond_8c

    .line 446
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 452
    :goto_3f
    const v0, 0x7f100124

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 453
    const v1, 0x7f100125

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/app/DirectionView;

    .line 454
    cmpl-float v2, p5, v8

    if-eqz v2, :cond_94

    .line 455
    new-instance v2, Lcom/google/android/maps/driveabout/app/aK;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    .line 456
    invoke-static {p5}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v6, v4, v5}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZI)Ljava/lang/String;

    move-result-object v2

    .line 458
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 461
    cmpl-float v0, p6, v8

    if-eqz v0, :cond_90

    .line 462
    invoke-virtual {v1, p6}, Lcom/google/android/maps/driveabout/app/DirectionView;->setOrientation(F)V

    .line 463
    invoke-virtual {v1, v6}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    .line 472
    :goto_76
    const v0, 0x7f1000e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 473
    if-eqz p4, :cond_9b

    .line 474
    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 475
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 479
    :goto_87
    return-object p1

    .line 441
    :cond_88
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2e

    .line 449
    :cond_8c
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3f

    .line 465
    :cond_90
    invoke-virtual {v1, v7}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    goto :goto_76

    .line 468
    :cond_94
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 469
    invoke-virtual {v1, v7}, Lcom/google/android/maps/driveabout/app/DirectionView;->setVisibility(I)V

    goto :goto_76

    .line 477
    :cond_9b
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_87
.end method

.method public static a(Landroid/content/Context;LaH/h;Z)Lcom/google/android/maps/driveabout/app/T;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 283
    new-instance v0, Lcom/google/android/maps/driveabout/app/ad;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/ad;-><init>(Landroid/content/Context;LaH/h;Z)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;)Lcom/google/android/maps/driveabout/app/T;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 270
    .line 273
    new-instance v0, Lcom/google/android/maps/driveabout/app/aa;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aa;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZ)Lcom/google/android/maps/driveabout/app/T;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 261
    if-nez p3, :cond_d

    const/4 v3, 0x1

    .line 262
    :goto_3
    new-instance v0, Lcom/google/android/maps/driveabout/app/aa;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aa;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V

    return-object v0

    .line 261
    :cond_d
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Z)Lcom/google/android/maps/driveabout/app/T;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 289
    new-instance v0, Lcom/google/android/maps/driveabout/app/W;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/W;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method private a(Lcom/google/android/maps/driveabout/app/Y;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 399
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_20

    .line 408
    const-string v0, ""

    :goto_9
    return-object v0

    .line 402
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 404
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 399
    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_a
        :pswitch_a
        :pswitch_15
    .end packed-switch
.end method

.method static synthetic a()V
    .registers 0

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/maps/driveabout/app/T;->b()V

    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 550
    move v0, v1

    .line 552
    :goto_2
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 553
    if-gez v0, :cond_9

    .line 556
    :goto_8
    return v1

    .line 555
    :cond_9
    if-eqz v0, :cond_17

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 556
    :cond_17
    const/4 v1, 0x1

    goto :goto_8

    .line 558
    :cond_19
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_2
.end method

.method private static b()V
    .registers 1

    .prologue
    .line 1016
    const/16 v0, 0xa

    :try_start_2
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_5} :catch_6

    .line 1020
    :goto_5
    return-void

    .line 1017
    :catch_6
    move-exception v0

    goto :goto_5
.end method


# virtual methods
.method a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .registers 8
    .parameter

    .prologue
    .line 515
    if-nez p1, :cond_14

    const/4 v0, 0x0

    move-object v1, v0

    .line 517
    :goto_4
    monitor-enter p0

    .line 518
    :try_start_5
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 519
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 534
    :goto_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_55

    .line 535
    return-object v0

    .line 515
    :cond_14
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_4

    .line 521
    :cond_1e
    :try_start_1e
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 522
    const/4 v0, 0x0

    move v3, v0

    :goto_25
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_58

    .line 523
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    .line 524
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_51

    .line 525
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/T;->a(Lcom/google/android/maps/driveabout/app/Y;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 526
    invoke-static {v4, v1}, Lcom/google/android/maps/driveabout/app/T;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 527
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 522
    :cond_4d
    :goto_4d
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_25

    .line 530
    :cond_51
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4d

    .line 534
    :catchall_55
    move-exception v0

    monitor-exit p0
    :try_end_57
    .catchall {:try_start_1e .. :try_end_57} :catchall_55

    throw v0

    :cond_58
    move-object v0, v2

    goto :goto_12
.end method

.method public a(LaH/h;)V
    .registers 2
    .parameter

    .prologue
    .line 417
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->d:LaH/h;

    .line 418
    return-void
.end method

.method protected declared-synchronized a(Ljava/util/ArrayList;)V
    .registers 3
    .parameter

    .prologue
    .line 297
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->e:Ljava/util/ArrayList;

    .line 298
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/T;->notifyDataSetChanged()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 300
    monitor-exit p0

    return-void

    .line 297
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(I)V
    .registers 2
    .parameter

    .prologue
    .line 425
    iput p1, p0, Lcom/google/android/maps/driveabout/app/T;->c:I

    .line 426
    return-void
.end method

.method protected b(Ljava/util/ArrayList;)V
    .registers 4
    .parameter

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/T;->g:Z

    if-eqz v0, :cond_8

    .line 305
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->a(Ljava/util/ArrayList;)V

    .line 314
    :goto_7
    return-void

    .line 307
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/maps/driveabout/app/U;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/U;-><init>(Lcom/google/android/maps/driveabout/app/T;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_7
.end method

.method c(Ljava/util/ArrayList;)V
    .registers 2
    .parameter

    .prologue
    .line 540
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    .line 541
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/T;->notifyDataSetChanged()V

    .line 542
    return-void
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    .prologue
    .line 493
    new-instance v0, Lcom/google/android/maps/driveabout/app/V;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/V;-><init>(Lcom/google/android/maps/driveabout/app/T;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 325
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_c

    .line 326
    :cond_a
    const/4 v0, 0x0

    .line 328
    :goto_b
    return-object v0

    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_b
.end method

.method public getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 334
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 340
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    .line 341
    if-nez v0, :cond_a

    .line 379
    :goto_9
    return-object v3

    .line 345
    :cond_a
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_de

    goto :goto_9

    .line 347
    :pswitch_12
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040042

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 349
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 350
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->d()Z

    move-result v0

    if-eqz v0, :cond_db

    .line 351
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 353
    :goto_35
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v1

    .line 354
    goto :goto_9

    .line 356
    :pswitch_3a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->e()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->i()F

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->j()F

    move-result v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;

    move-result-object v3

    goto :goto_9

    .line 360
    :pswitch_57
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v2

    .line 363
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->h()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_88

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    const v4, 0x7f0d00d4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 366
    :cond_88
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->g()LO/U;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/maps/driveabout/app/dx;->b(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->i()F

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->j()F

    move-result v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IFF)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_9

    .line 370
    :pswitch_a3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_9

    .line 372
    :pswitch_b2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040044

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 374
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    move-object v3, v1

    .line 375
    goto/16 :goto_9

    .line 377
    :pswitch_cb
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->f()I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_9

    :cond_db
    move-object v0, v2

    goto/16 :goto_35

    .line 345
    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_12
        :pswitch_3a
        :pswitch_57
        :pswitch_a3
        :pswitch_b2
        :pswitch_cb
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 386
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/Y;

    .line 387
    if-eqz v0, :cond_f

    .line 388
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Y;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_14

    .line 394
    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0

    .line 391
    :pswitch_11
    const/4 v0, 0x1

    goto :goto_10

    .line 388
    nop

    :pswitch_data_14
    .packed-switch 0x2
        :pswitch_11
        :pswitch_11
    .end packed-switch
.end method
