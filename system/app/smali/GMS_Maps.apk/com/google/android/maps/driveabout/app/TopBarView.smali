.class public Lcom/google/android/maps/driveabout/app/TopBarView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

.field private b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/ViewGroup;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;)V

    .line 94
    return-void
.end method

.method private a(LO/N;)V
    .registers 7
    .parameter

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d000b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "<image>"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 414
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 415
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 418
    const-string v2, "<image>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 420
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, LO/m;->a(Landroid/content/Context;LO/N;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 421
    if-eqz v2, :cond_4b

    .line 423
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0038

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 425
    new-instance v4, Lcom/google/android/maps/driveabout/app/dj;

    invoke-direct {v4, v2, v3, v3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;FF)V

    .line 426
    const-string v2, "<image>"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    const/16 v3, 0x21

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 429
    :cond_4b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    .line 430
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 97
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 99
    const v1, 0x7f040054

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 101
    const v0, 0x7f100153

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    .line 103
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    .line 105
    const v0, 0x7f100154

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    .line 107
    const v0, 0x7f100155

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f100156

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    .line 110
    const v0, 0x7f10012f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    .line 111
    const v0, 0x7f100130

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->f:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f100157

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    .line 114
    const v0, 0x7f100158

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    .line 116
    const v0, 0x7f100159

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    .line 118
    const v0, 0x7f10015a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    .line 119
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->m()V

    .line 121
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 122
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f050011

    const v3, 0x7f050010

    const/4 v2, 0x0

    .line 174
    const-string v0, "__step_description"

    if-ne p1, v0, :cond_20

    .line 175
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    if-eq v0, v1, :cond_1f

    .line 176
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 178
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    .line 214
    :cond_1f
    :goto_1f
    return-void

    .line 180
    :cond_20
    const-string v0, "__route_overview"

    if-ne p1, v0, :cond_3f

    .line 181
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    if-eq v0, v1, :cond_1f

    .line 182
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 183
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    .line 184
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 186
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    goto :goto_1f

    .line 188
    :cond_3f
    const-string v0, "__reroute_prompt_overview"

    if-ne p1, v0, :cond_5e

    .line 189
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_1f

    .line 190
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    .line 192
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    goto :goto_1f

    .line 196
    :cond_5e
    const-string v0, "__destination_buttons"

    if-ne p1, v0, :cond_7d

    .line 197
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_1f

    .line 198
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 199
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    .line 200
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 202
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    goto :goto_1f

    .line 205
    :cond_7d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    if-eq v0, v1, :cond_97

    .line 206
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 207
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    .line 208
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 209
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    .line 210
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    .line 212
    :cond_97
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_1f
.end method

.method private a(I)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 440
    if-eq p1, v0, :cond_8

    const/4 v1, 0x2

    if-eq p1, v1, :cond_8

    .line 441
    const/4 v0, 0x0

    .line 450
    :goto_7
    return v0

    .line 443
    :cond_8
    if-ne p1, v0, :cond_1b

    .line 444
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0017

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_7

    .line 447
    :cond_1b
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0018

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_7
.end method

.method static a(Landroid/content/Context;LO/N;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 485
    if-nez p1, :cond_4

    .line 498
    :cond_3
    :goto_3
    return v0

    .line 488
    :cond_4
    invoke-virtual {p1}, LO/N;->f()I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_14

    invoke-virtual {p1}, LO/N;->e()I

    move-result v1

    const/16 v2, 0x1f4

    if-gt v1, v2, :cond_3

    .line 492
    :cond_14
    invoke-virtual {p1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x11

    if-eq v1, v2, :cond_3

    .line 495
    invoke-static {p1}, LO/m;->b(LO/N;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 498
    const/4 v0, 0x1

    goto :goto_3
.end method

.method private b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 6
    .parameter

    .prologue
    .line 458
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    .line 459
    if-eqz v1, :cond_25

    invoke-virtual {v1}, LO/N;->j()LO/N;

    move-result-object v0

    .line 460
    :goto_a
    if-eqz v1, :cond_2b

    invoke-virtual {v1}, LO/N;->b()I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2b

    .line 461
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 462
    invoke-virtual {v1}, LO/N;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 463
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->k()V

    .line 477
    :goto_24
    return-void

    .line 459
    :cond_25
    const/4 v0, 0x0

    goto :goto_a

    .line 465
    :cond_27
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_24

    .line 468
    :cond_2b
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    .line 469
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;LO/N;)Z

    move-result v1

    if-eqz v1, :cond_47

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v1

    const/16 v2, 0x1324

    if-gt v1, v2, :cond_47

    .line 471
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(LO/N;)V

    .line 472
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->j()V

    goto :goto_24

    .line 474
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    goto :goto_24
.end method

.method private j()V
    .registers 3

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    if-nez v0, :cond_f

    .line 138
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    const v1, 0x7f050009

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    .line 141
    :cond_f
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    if-nez v0, :cond_f

    .line 152
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    const v1, 0x7f050009

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    .line 156
    :cond_f
    return-void
.end method

.method private l()V
    .registers 3

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    if-eqz v0, :cond_f

    .line 160
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    const v1, 0x7f05000c

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    .line 164
    :cond_f
    return-void
.end method

.method private m()V
    .registers 6

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d009e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 506
    const-string v1, "%2$s"

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d009d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 507
    const-string v1, "%1$s"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 508
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 509
    new-instance v0, Lcom/google/android/maps/driveabout/app/dj;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1080038

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x3fa66666

    invoke-direct {v0, v3, v4}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v3, v1, 0x4

    const/16 v4, 0x21

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 514
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 515
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    if-eqz v0, :cond_f

    .line 145
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    const v1, 0x7f05000c

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    .line 148
    :cond_f
    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 332
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setRoutes(LO/z;[LO/z;)V

    .line 334
    const-string v0, "__route_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 335
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 311
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 312
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [LO/z;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setRoutes(LO/z;[LO/z;)V

    .line 315
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-nez v1, :cond_3b

    .line 317
    :goto_1c
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->i()I

    move-result v1

    if-ltz v1, :cond_44

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v1

    if-ltz v1, :cond_44

    .line 319
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->i()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(III)V

    .line 327
    :goto_35
    const-string v0, "__route_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 328
    return-void

    .line 315
    :cond_3b
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->i()I

    move-result v0

    goto :goto_1c

    .line 323
    :cond_44
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v2}, LO/z;->p()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v3

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(III)V

    goto :goto_35
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 275
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    .line 276
    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setStep(LO/N;)V

    .line 277
    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 278
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-ne v1, p2, :cond_4e

    .line 279
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 280
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    .line 281
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    .line 282
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setDistanceMeters(F)V

    .line 283
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setDistanceUnits(I)V

    .line 289
    :goto_30
    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setUseLongDistanceStepFormat(Z)V

    .line 290
    if-eqz p4, :cond_47

    if-nez p3, :cond_47

    .line 291
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 303
    :goto_3a
    const-string v0, "__step_description"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 304
    return-void

    .line 286
    :cond_40
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    .line 287
    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    goto :goto_30

    .line 293
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 294
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_3a

    .line 297
    :cond_4e
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    .line 298
    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    .line 299
    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setUseLongDistanceStepFormat(Z)V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 301
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_3a
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 363
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 364
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    const-string v0, "__reroute_prompt_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 366
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 374
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 375
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 387
    :goto_18
    return-void

    .line 378
    :cond_19
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 379
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_18

    .line 383
    :cond_2f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_18
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 342
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 343
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 344
    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    .line 352
    const-string v0, "__destination_buttons"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 354
    invoke-virtual {p0, p2, p3}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public b()Landroid/view/View;
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/view/View;
    .registers 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/android/maps/driveabout/app/RouteSelectorView;
    .registers 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    return-object v0
.end method

.method public e()Landroid/widget/Button;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    return-object v0
.end method

.method public f()Landroid/widget/Button;
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    return-object v0
.end method

.method public g()Landroid/widget/Button;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    return-object v0
.end method

.method public h()Landroid/widget/Button;
    .registers 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    return-object v0
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    if-ne v0, v1, :cond_10

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public setButtonVisibility(ZZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setButtonVisibility(ZZZ)V

    .line 403
    return-void
.end method

.method public setConnectedViews(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 129
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 132
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    .line 133
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 134
    return-void
.end method
