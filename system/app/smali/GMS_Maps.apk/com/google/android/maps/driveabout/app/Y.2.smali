.class public Lcom/google/android/maps/driveabout/app/Y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final j:Ljava/util/Comparator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:I

.field private final e:I

.field private final f:LO/U;

.field private final g:I

.field private h:F

.field private i:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/maps/driveabout/app/Z;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/Z;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/Y;->j:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(IIZIILO/U;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, -0x4080

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput v0, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    .line 103
    iput v0, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    .line 119
    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->a:I

    .line 120
    iput p2, p0, Lcom/google/android/maps/driveabout/app/Y;->b:I

    .line 121
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/Y;->c:Z

    .line 122
    iput p4, p0, Lcom/google/android/maps/driveabout/app/Y;->d:I

    .line 123
    iput p5, p0, Lcom/google/android/maps/driveabout/app/Y;->e:I

    .line 124
    iput-object p6, p0, Lcom/google/android/maps/driveabout/app/Y;->f:LO/U;

    .line 125
    iput p7, p0, Lcom/google/android/maps/driveabout/app/Y;->g:I

    .line 126
    return-void
.end method

.method static a()Lcom/google/android/maps/driveabout/app/Y;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 150
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x4

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(I)Lcom/google/android/maps/driveabout/app/Y;
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 130
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x1

    const/4 v6, 0x0

    move v2, p0

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(II)Lcom/google/android/maps/driveabout/app/Y;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 140
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x2

    const/4 v6, 0x0

    move v2, p0

    move v4, p1

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 145
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x3

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v6, p0

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static b(I)Lcom/google/android/maps/driveabout/app/Y;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 135
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v6, 0x0

    move v2, p0

    move v3, v1

    move v5, v4

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static c(I)Lcom/google/android/maps/driveabout/app/Y;
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 155
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x5

    const/4 v6, 0x0

    move v2, p0

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static d(I)Lcom/google/android/maps/driveabout/app/Y;
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 160
    new-instance v0, Lcom/google/android/maps/driveabout/app/Y;

    const/4 v1, 0x6

    const/4 v6, 0x0

    move v3, v2

    move v4, v2

    move v5, p0

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/Y;-><init>(IIZIILO/U;I)V

    return-object v0
.end method

.method static synthetic k()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/maps/driveabout/app/Y;->j:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 193
    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    .line 194
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->a:I

    return v0
.end method

.method public b(F)V
    .registers 2
    .parameter

    .prologue
    .line 205
    iput p1, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    .line 206
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 167
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->b:I

    return v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/Y;->c:Z

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->d:I

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 176
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->e:I

    return v0
.end method

.method public g()LO/U;
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Y;->f:LO/U;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 182
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->g:I

    return v0
.end method

.method public i()F
    .registers 2

    .prologue
    .line 187
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->h:F

    return v0
.end method

.method public j()F
    .registers 2

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/maps/driveabout/app/Y;->i:F

    return v0
.end method
