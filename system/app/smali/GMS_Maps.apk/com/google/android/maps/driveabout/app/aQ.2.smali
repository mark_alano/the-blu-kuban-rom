.class public Lcom/google/android/maps/driveabout/app/aQ;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:LO/t;

.field private B:Lcom/google/android/maps/driveabout/app/dM;

.field private C:Lcom/google/android/maps/driveabout/app/af;

.field private D:Lcom/google/android/maps/driveabout/app/a;

.field private E:Ljava/lang/Runnable;

.field private F:Ljava/lang/Runnable;

.field private G:LaH/h;

.field private H:Z

.field private I:LaH/h;

.field private J:F

.field private final K:Lcom/google/android/maps/driveabout/app/dr;

.field private final L:Lcom/google/android/maps/driveabout/app/dw;

.field private M:I

.field private N:LL/G;

.field private O:I

.field protected a:LM/n;

.field protected b:Lcom/google/android/maps/driveabout/app/NavigationService;

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:LaH/h;

.field protected g:F

.field protected h:F

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field protected p:Z

.field protected q:Z

.field protected r:I

.field protected s:I

.field protected t:J

.field protected u:LO/s;

.field protected v:[LO/U;

.field protected w:[LO/b;

.field protected x:Ljava/lang/String;

.field protected y:LO/a;

.field protected z:Z


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    const v3, 0x927c0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    .line 112
    iput v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->c:I

    .line 113
    const/16 v0, 0x7530

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->d:I

    .line 114
    iput v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->e:I

    .line 118
    const/high16 v0, -0x4080

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    .line 121
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    .line 122
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    .line 123
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    .line 125
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    .line 126
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    .line 127
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    .line 128
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    .line 129
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    .line 145
    new-instance v0, Lcom/google/android/maps/driveabout/app/dr;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dr;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->K:Lcom/google/android/maps/driveabout/app/dr;

    .line 148
    new-instance v0, Lcom/google/android/maps/driveabout/app/dw;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dw;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->L:Lcom/google/android/maps/driveabout/app/dw;

    .line 149
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    .line 156
    iput v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    .line 1214
    return-void
.end method

.method private static a(ZI)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 430
    invoke-static {p0}, LF/V;->a(Z)V

    .line 449
    return-void
.end method

.method private ah()V
    .registers 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    .line 581
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    .line 582
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    .line 584
    :cond_c
    return-void
.end method

.method private ai()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    .line 616
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    .line 617
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    .line 619
    :cond_c
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    .line 620
    return-void
.end method

.method private d(I)V
    .registers 5
    .parameter

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    if-nez v0, :cond_11

    .line 568
    new-instance v0, Lcom/google/android/maps/driveabout/app/aR;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aR;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    .line 575
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->F:Ljava/lang/Runnable;

    int-to-long v1, p1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    .line 577
    :cond_11
    return-void
.end method


# virtual methods
.method public A()I
    .registers 2

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    return v0
.end method

.method public B()V
    .registers 2

    .prologue
    .line 348
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    .line 349
    return-void
.end method

.method public C()LO/U;
    .registers 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    array-length v0, v0

    if-lez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public D()Z
    .registers 2

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public E()[LO/U;
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    return-object v0
.end method

.method public F()I
    .registers 2

    .prologue
    .line 370
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    return v0
.end method

.method public G()Ljava/lang/String;
    .registers 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->x:Ljava/lang/String;

    return-object v0
.end method

.method public H()[LO/b;
    .registers 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    return-object v0
.end method

.method protected I()LQ/s;
    .registers 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    return-object v0
.end method

.method protected J()V
    .registers 3

    .prologue
    .line 458
    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 459
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    invoke-virtual {v0, v1}, LM/n;->a(I)V

    .line 460
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->h()V

    .line 461
    return-void
.end method

.method public K()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 481
    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_e

    .line 482
    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    if-nez v2, :cond_c

    .line 484
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v0, v1

    .line 482
    goto :goto_b

    .line 484
    :cond_e
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->L:Lcom/google/android/maps/driveabout/app/dw;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/dw;->a(Landroid/location/Location;)I

    move-result v2

    if-eqz v2, :cond_b

    :cond_1c
    move v0, v1

    goto :goto_b
.end method

.method public L()V
    .registers 2

    .prologue
    .line 500
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    .line 501
    return-void
.end method

.method public M()V
    .registers 3

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    if-eqz v0, :cond_c

    .line 509
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MockLocationManager is already enabled!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_c
    new-instance v0, LL/G;

    invoke-direct {v0}, LL/G;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    .line 513
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    invoke-virtual {v0, v1}, LM/n;->a(LL/G;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, LL/O;->a(Landroid/content/Context;)LL/k;

    move-result-object v1

    invoke-virtual {v0, v1}, LL/G;->a(Ll/f;)V

    .line 516
    return-void
.end method

.method public N()V
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    if-eqz v0, :cond_c

    .line 520
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    .line 521
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->N:LL/G;

    .line 523
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->l()V

    .line 524
    return-void
.end method

.method public O()V
    .registers 2

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 541
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->j()V

    .line 542
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 543
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    .line 547
    :goto_11
    return-void

    .line 545
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->S()V

    goto :goto_11
.end method

.method public P()V
    .registers 2

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 555
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->i()V

    .line 556
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    .line 557
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ah()V

    .line 558
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    .line 559
    return-void
.end method

.method public Q()Z
    .registers 2

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 563
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    return v0
.end method

.method public R()V
    .registers 4

    .prologue
    .line 591
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-nez v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    if-nez v0, :cond_1b

    .line 592
    new-instance v0, Lcom/google/android/maps/driveabout/app/aS;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/aS;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    .line 599
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    .line 600
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->E:Ljava/lang/Runnable;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->c:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    .line 602
    :cond_1b
    return-void
.end method

.method public S()V
    .registers 2

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    .line 610
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->X()V

    .line 611
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    .line 612
    return-void
.end method

.method public T()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 626
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 627
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    if-eqz v0, :cond_1d

    .line 629
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->c()V

    .line 630
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->d()V

    .line 631
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    .line 632
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    .line 633
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 635
    :cond_1d
    return-void
.end method

.method public U()V
    .registers 2

    .prologue
    .line 642
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    if-eqz v0, :cond_e

    .line 643
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    .line 644
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->aa()V

    .line 646
    :cond_e
    return-void
.end method

.method public V()V
    .registers 2

    .prologue
    .line 653
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-nez v0, :cond_a

    .line 654
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    .line 656
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    .line 658
    :cond_a
    return-void
.end method

.method public W()V
    .registers 2

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    if-eqz v0, :cond_7

    .line 666
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->H:Z

    .line 669
    :cond_7
    return-void
.end method

.method protected X()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 676
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 677
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    if-nez v0, :cond_1d

    .line 679
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->g()V

    .line 680
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->d()V

    .line 681
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->i:Z

    .line 682
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->j:Z

    .line 683
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 685
    :cond_1d
    return-void
.end method

.method protected Y()V
    .registers 2

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->j()V

    .line 689
    return-void
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    .line 707
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 708
    return-void
.end method

.method public a(F)Lo/am;
    .registers 3
    .parameter

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0, p1}, LO/s;->a(F)Lo/am;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    .line 192
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 469
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->g()V

    .line 470
    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    .line 471
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    .line 472
    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/aQ;->a(ZI)V

    .line 473
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    .line 474
    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 475
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0, p1}, LM/n;->a(I)V

    .line 476
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->a()LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->h()V

    .line 477
    return-void
.end method

.method protected a(ILO/g;LO/s;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 868
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 869
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 870
    if-nez p1, :cond_1f

    .line 871
    const/4 v1, 0x0

    .line 872
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    if-nez v2, :cond_2f

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-virtual {p3}, LO/s;->l()Z

    move-result v2

    if-nez v2, :cond_2f

    .line 874
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    .line 876
    :goto_1b
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a(Z)V

    .line 882
    :cond_1e
    :goto_1e
    return-void

    .line 877
    :cond_1f
    if-ne p1, v0, :cond_25

    .line 878
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->aa()V

    goto :goto_1e

    .line 879
    :cond_25
    if-eqz p2, :cond_1e

    .line 880
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p2}, LQ/s;->b(LO/g;)V

    goto :goto_1e

    :cond_2f
    move v0, v1

    goto :goto_1b
.end method

.method public a(LM/C;)V
    .registers 3
    .parameter

    .prologue
    .line 527
    invoke-virtual {p1}, LM/C;->c()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    .line 528
    invoke-virtual {p1}, LM/C;->d()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->h:F

    .line 529
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->K()V

    .line 530
    return-void
.end method

.method protected a(LM/n;)V
    .registers 5
    .parameter

    .prologue
    .line 830
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    .line 831
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aV;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aV;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {v0, v1}, LM/n;->a(LM/b;)V

    .line 839
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0, v1}, LM/n;->a(LM/b;)V

    .line 840
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aT;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/aT;-><init>(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/aR;)V

    invoke-virtual {v0, v1}, LM/n;->a(LM/s;)V

    .line 841
    return-void
.end method

.method protected a(LO/s;)V
    .registers 3
    .parameter

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 849
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 850
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    .line 851
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    .line 852
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ac()V

    .line 853
    return-void
.end method

.method protected a(LO/t;)V
    .registers 3
    .parameter

    .prologue
    .line 1036
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    .line 1037
    new-instance v0, Lcom/google/android/maps/driveabout/app/ba;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ba;-><init>(Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {p1, v0}, LO/t;->a(LO/q;)V

    .line 1038
    return-void
.end method

.method protected a(LaH/h;)V
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x42c8

    .line 771
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 773
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 774
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    .line 776
    :cond_1b
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->I:LaH/h;

    .line 778
    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 779
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v2, v0}, LM/n;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_95

    .line 780
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    .line 781
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/ci;->a(LaH/h;)V

    .line 782
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ac()V

    .line 785
    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v0

    if-nez v0, :cond_a0

    .line 786
    const/4 v2, 0x1

    .line 787
    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    if-eqz v0, :cond_9e

    .line 788
    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v4

    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Lo/T;->a(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    .line 791
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    if-eqz v0, :cond_90

    const/high16 v0, 0x42a0

    .line 793
    :goto_5b
    float-to-double v6, v0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_9e

    move v0, v1

    .line 798
    :goto_61
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    if-eq v0, v1, :cond_6e

    .line 799
    if-eqz v0, :cond_92

    const-string v1, "o"

    :goto_69
    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 801
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    .line 804
    :cond_6e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    if-eqz v0, :cond_88

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->G:LaH/h;

    invoke-virtual {v1}, LaH/h;->q()Lo/u;

    move-result-object v1

    invoke-virtual {v0, v1}, LaH/h;->b(Lo/u;)F

    move-result v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_88

    .line 807
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ai()V

    .line 808
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    .line 811
    :cond_88
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->J()V

    .line 817
    :cond_8f
    :goto_8f
    return-void

    :cond_90
    move v0, v3

    .line 791
    goto :goto_5b

    .line 799
    :cond_92
    const-string v1, "O"

    goto :goto_69

    .line 812
    :cond_95
    const-string v1, "driveabout_base_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8f

    goto :goto_8f

    :cond_9e
    move v0, v2

    goto :goto_61

    :cond_a0
    move v0, v1

    goto :goto_61
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/dM;LO/t;LM/n;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/a;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    .line 182
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    .line 183
    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/aQ;->C:Lcom/google/android/maps/driveabout/app/af;

    .line 184
    iput-object p6, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    .line 185
    invoke-static {}, LO/s;->a()LO/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 186
    invoke-virtual {p0, p3}, Lcom/google/android/maps/driveabout/app/aQ;->a(LO/t;)V

    .line 187
    invoke-virtual {p0, p4}, Lcom/google/android/maps/driveabout/app/aQ;->a(LM/n;)V

    .line 188
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dM;->a(Ljava/lang/Runnable;)V

    .line 693
    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/dM;->a(Ljava/lang/Runnable;J)V

    .line 698
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 711
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 712
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    .line 713
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->g(Z)V

    .line 714
    return-void
.end method

.method public a([LO/U;I[LO/b;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 401
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V

    .line 402
    return-void
.end method

.method public a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 420
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    .line 421
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/aQ;->a(ZI)V

    .line 422
    iput p2, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    .line 423
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    .line 424
    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    .line 425
    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/aQ;->x:Ljava/lang/String;

    .line 426
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->J()V

    .line 427
    return-void
.end method

.method protected a([LO/W;)V
    .registers 6
    .parameter

    .prologue
    .line 968
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 969
    const/4 v0, 0x1

    :goto_6
    array-length v2, p1

    if-ge v0, v2, :cond_19

    .line 970
    aget-object v2, p1, v0

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-nez v2, :cond_16

    .line 971
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 969
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 974
    :cond_19
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/U;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/U;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    .line 975
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    invoke-static {v0}, LF/V;->a(Z)V

    .line 976
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    .line 978
    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-virtual {v0, v1, v2}, LO/c;->a(Landroid/content/Context;[LO/b;)V

    .line 982
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->g()Z

    move-result v0

    if-eqz v0, :cond_66

    .line 983
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->C:Lcom/google/android/maps/driveabout/app/af;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->l()LO/U;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v2}, LO/z;->m()LO/U;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/af;->a(LO/U;LO/U;)V

    .line 987
    :cond_66
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->v:[LO/U;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->s:I

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->w:[LO/b;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationService;->a([LO/U;I[LO/b;)V

    .line 988
    return-void
.end method

.method protected aa()V
    .registers 2

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 718
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    .line 719
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ad()V

    .line 720
    return-void
.end method

.method protected ab()Z
    .registers 2

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {v0}, LM/n;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected ac()V
    .registers 2

    .prologue
    .line 727
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 728
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ab()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-nez v0, :cond_11

    .line 729
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ad()V

    .line 733
    :cond_10
    :goto_10
    return-void

    .line 730
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ab()Z

    move-result v0

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-eqz v0, :cond_10

    .line 731
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ae()V

    goto :goto_10
.end method

.method protected ad()V
    .registers 2

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    .line 742
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Y()V

    .line 743
    return-void
.end method

.method protected ae()V
    .registers 2

    .prologue
    .line 750
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 751
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    .line 752
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Z()V

    .line 753
    return-void
.end method

.method public af()I
    .registers 2

    .prologue
    .line 1229
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    return v0
.end method

.method public ag()F
    .registers 2

    .prologue
    .line 1233
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->J:F

    return v0
.end method

.method public b()LaH/h;
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    return-object v0
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 495
    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->M:I

    .line 496
    return-void
.end method

.method protected b(LO/s;)V
    .registers 7
    .parameter

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 894
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    .line 895
    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v1

    .line 896
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 900
    iget v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    if-lez v2, :cond_1d

    invoke-virtual {v1}, LO/N;->i()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1d

    .line 904
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->r:I

    .line 907
    :cond_1d
    if-eqz v1, :cond_26

    .line 909
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LQ/s;->b(LO/N;LO/N;)V

    .line 911
    :cond_26
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->K:Lcom/google/android/maps/driveabout/app/dr;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v3}, LO/s;->g()LO/z;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/aQ;->f:LaH/h;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/maps/driveabout/app/dr;->a(LO/z;LO/N;LO/N;LaH/h;)V

    .line 913
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->B:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dM;->b(Ljava/lang/Runnable;)V

    .line 702
    return-void
.end method

.method public c()F
    .registers 2

    .prologue
    .line 203
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    return v0
.end method

.method protected c(I)V
    .registers 2
    .parameter

    .prologue
    .line 1027
    iput p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->O:I

    .line 1028
    return-void
.end method

.method protected c(LO/s;)V
    .registers 4
    .parameter

    .prologue
    .line 920
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 921
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 922
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-virtual {p1}, LO/s;->d()I

    move-result v0

    if-ltz v0, :cond_24

    invoke-virtual {p1}, LO/s;->d()I

    move-result v0

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_24

    .line 924
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->e:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->d(I)V

    .line 926
    :cond_24
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 927
    return-void
.end method

.method protected d(LO/s;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 936
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 937
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 938
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    .line 939
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    .line 940
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->a()Z

    move-result v0

    if-nez v0, :cond_28

    .line 941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    .line 942
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    if-eqz v0, :cond_28

    .line 943
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->D:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Ljava/lang/String;)V

    .line 946
    :cond_28
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->z:Z

    .line 949
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    .line 950
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->a:LM/n;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v0, v1}, LM/n;->a(LO/z;)V

    .line 951
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->A:LO/t;

    invoke-virtual {v0}, LO/t;->c()V

    .line 952
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LQ/s;->b(LO/z;[LO/z;)V

    .line 953
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LQ/s;->b(LO/N;LO/N;)V

    .line 959
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->b()Z

    move-result v0

    if-nez v0, :cond_6a

    .line 960
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->v()[LO/W;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/W;)V

    .line 962
    :cond_6a
    return-void
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 212
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->g:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public e()V
    .registers 2

    .prologue
    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    .line 223
    return-void
.end method

.method protected e(LO/s;)V
    .registers 3
    .parameter

    .prologue
    .line 997
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 998
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 999
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    .line 1000
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    .line 1001
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->ae()V

    .line 1002
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1003
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aQ;->ah()V

    .line 1004
    iget v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/aQ;->d(I)V

    .line 1006
    :cond_22
    return-void
.end method

.method public f()LO/a;
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->y:LO/a;

    return-object v0
.end method

.method protected f(LO/s;)V
    .registers 4
    .parameter

    .prologue
    .line 1012
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 1013
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 1014
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v1}, LO/s;->k()I

    move-result v1

    invoke-virtual {v0, v1}, LQ/s;->a(I)V

    .line 1015
    return-void
.end method

.method public g()I
    .registers 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->c()I

    move-result v0

    return v0
.end method

.method protected g(LO/s;)V
    .registers 3
    .parameter

    .prologue
    .line 1021
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->Y()V

    .line 1022
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    .line 1023
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->I()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->R()V

    .line 1024
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->b()I

    move-result v0

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->d()I

    move-result v0

    return v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    return v0
.end method

.method public k()LO/N;
    .registers 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    return-object v0
.end method

.method public l()LO/z;
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    return-object v0
.end method

.method public m()[LO/z;
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v0

    return-object v0
.end method

.method public n()LO/z;
    .registers 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->i()LO/z;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->j()Z

    move-result v0

    return v0
.end method

.method public p()I
    .registers 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->k()I

    move-result v0

    return v0
.end method

.method public q()Z
    .registers 2

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->p:Z

    return v0
.end method

.method public r()Z
    .registers 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    return v0
.end method

.method public s()I
    .registers 5

    .prologue
    .line 304
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/aQ;->t:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public t()V
    .registers 3

    .prologue
    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    .line 309
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->t:J

    .line 310
    return-void
.end method

.method public u()V
    .registers 2

    .prologue
    .line 313
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->q:Z

    .line 314
    return-void
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->l()Z

    move-result v0

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public w()Z
    .registers 2

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->o:Z

    return v0
.end method

.method public x()Z
    .registers 2

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->n:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->u:LO/s;

    invoke-virtual {v0}, LO/s;->l()Z

    move-result v0

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->k:Z

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->l:Z

    if-nez v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public y()Z
    .registers 2

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aQ;->m:Z

    if-nez v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->j()Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public z()Z
    .registers 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-nez v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method
