.class Lcom/google/android/maps/driveabout/app/aV;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/b;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/aQ;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 2
    .parameter

    .prologue
    .line 1043
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .registers 4
    .parameter

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aZ;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/aZ;-><init>(Lcom/google/android/maps/driveabout/app/aV;LM/C;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    .line 1103
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter

    .prologue
    .line 1051
    check-cast p1, LaH/h;

    .line 1052
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aW;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/aW;-><init>(Lcom/google/android/maps/driveabout/app/aV;LaH/h;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    .line 1056
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 1064
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1065
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aX;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aX;-><init>(Lcom/google/android/maps/driveabout/app/aV;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    .line 1070
    :cond_12
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1078
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1089
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1090
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aV;->a:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aY;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aY;-><init>(Lcom/google/android/maps/driveabout/app/aV;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;)V

    .line 1095
    :cond_12
    return-void
.end method
