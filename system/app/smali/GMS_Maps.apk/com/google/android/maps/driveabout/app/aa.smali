.class Lcom/google/android/maps/driveabout/app/aa;
.super Lcom/google/android/maps/driveabout/app/T;
.source "SourceFile"


# instance fields
.field e:Ljava/lang/Runnable;

.field private final f:Lcom/google/android/maps/driveabout/app/af;

.field private final g:Ljava/util/ArrayList;

.field private final h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 602
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/T;-><init>(Landroid/content/Context;)V

    .line 590
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    .line 593
    new-instance v0, Lcom/google/android/maps/driveabout/app/ab;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ab;-><init>(Lcom/google/android/maps/driveabout/app/aa;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    .line 603
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/aa;->f:Lcom/google/android/maps/driveabout/app/af;

    .line 605
    if-eqz p3, :cond_70

    .line 606
    if-eqz p5, :cond_26

    .line 607
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00ca

    const v2, 0x7f020144

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00c9

    const v2, 0x7f020146

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00cb

    const v2, 0x7f020142

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f0d00cc

    const v2, 0x7f020145

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/Y;->a(II)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    :cond_53
    :goto_53
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 622
    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->a()Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/aa;->a(Ljava/util/ArrayList;)V

    .line 626
    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    .line 630
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 631
    return-void

    .line 617
    :cond_70
    if-eqz p4, :cond_53

    .line 618
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    const v1, 0x7f04002f

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/Y;->d(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_53
.end method

.method private b()V
    .registers 2

    .prologue
    .line 686
    new-instance v0, Lcom/google/android/maps/driveabout/app/ac;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ac;-><init>(Lcom/google/android/maps/driveabout/app/aa;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ac;->start()V

    .line 714
    return-void
.end method


# virtual methods
.method public a(LaH/h;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 640
    if-eqz p1, :cond_b

    .line 641
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aa;->e:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 645
    :cond_b
    monitor-enter p0

    .line 646
    :try_start_c
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v2, :cond_35

    if-eqz p1, :cond_35

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x407f400000000000L

    cmpl-double v2, v2, v4

    if-lez v2, :cond_35

    move v2, v1

    .line 650
    :goto_23
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v3, :cond_29

    if-eqz v2, :cond_2a

    :cond_29
    move v0, v1

    .line 651
    :cond_2a
    if-eqz v0, :cond_2e

    .line 652
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    .line 654
    :cond_2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_c .. :try_end_2f} :catchall_37

    .line 656
    if-eqz v0, :cond_34

    .line 657
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aa;->b()V

    .line 659
    :cond_34
    return-void

    :cond_35
    move v2, v0

    .line 646
    goto :goto_23

    .line 654
    :catchall_37
    move-exception v0

    :try_start_38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    throw v0
.end method

.method a(Lo/u;LO/U;Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const v4, 0x7f0d00d1

    const v2, 0x7f0d00d0

    .line 773
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->g:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 774
    if-nez p2, :cond_17

    if-eqz p3, :cond_22

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    .line 775
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    if-eqz v0, :cond_47

    .line 776
    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/Y;->b(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    :cond_22
    :goto_22
    if-eqz p2, :cond_2c

    .line 782
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    :cond_2c
    if-eqz p3, :cond_4f

    .line 785
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/U;

    .line 786
    const/4 v3, 0x5

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 778
    :cond_47
    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_22

    .line 790
    :cond_4f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->f:Lcom/google/android/maps/driveabout/app/af;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2}, Lcom/google/android/maps/driveabout/app/af;->a(Lo/u;I)Ljava/util/List;

    move-result-object v0

    .line 791
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_68

    .line 792
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/aa;->h:Z

    if-eqz v2, :cond_81

    .line 793
    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/Y;->b(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 798
    :cond_68
    :goto_68
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/U;

    .line 799
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6c

    .line 795
    :cond_81
    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_68

    .line 801
    :cond_89
    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/aa;->b(Ljava/util/ArrayList;)V

    .line 802
    return-void
.end method

.method public a_(I)V
    .registers 3
    .parameter

    .prologue
    .line 666
    .line 668
    monitor-enter p0

    .line 669
    :try_start_1
    iput p1, p0, Lcom/google/android/maps/driveabout/app/aa;->c:I

    .line 673
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aa;->d:LaH/h;

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    .line 674
    :goto_8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_11

    .line 676
    if-eqz v0, :cond_e

    .line 677
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aa;->b()V

    .line 679
    :cond_e
    return-void

    .line 673
    :cond_f
    const/4 v0, 0x0

    goto :goto_8

    .line 674
    :catchall_11
    move-exception v0

    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    throw v0
.end method
