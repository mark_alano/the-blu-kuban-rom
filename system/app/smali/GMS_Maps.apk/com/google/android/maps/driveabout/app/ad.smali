.class Lcom/google/android/maps/driveabout/app/ad;
.super Lcom/google/android/maps/driveabout/app/T;
.source "SourceFile"


# instance fields
.field private e:Z


# direct methods
.method constructor <init>(Landroid/content/Context;LaH/h;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 822
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/T;-><init>(Landroid/content/Context;)V

    .line 823
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 824
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    .line 825
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v1, :cond_18

    .line 826
    const v1, 0x7f0d00cc

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    :cond_18
    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->a()Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/ad;->a(Ljava/util/ArrayList;)V

    .line 830
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/ad;->a(LaH/h;)V

    .line 831
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 919
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/StarredItemProvider;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/googlenav/provider/StarredItemProvider;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 921
    if-eqz v0, :cond_14

    .line 922
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 924
    :cond_14
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/ad;)V
    .registers 1
    .parameter

    .prologue
    .line 806
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/ad;->b()V

    return-void
.end method

.method private b()V
    .registers 12

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 868
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ad;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/StarredItemProvider;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/googlenav/provider/StarredItemProvider;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 870
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 871
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v0, :cond_25

    .line 872
    const v0, 0x7f0d00cc

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    :cond_25
    if-eqz v5, :cond_97

    .line 877
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v8, v0, [Lcom/google/android/maps/driveabout/app/Y;

    .line 878
    monitor-enter p0

    move v0, v6

    .line 879
    :cond_2f
    :goto_2f
    :try_start_2f
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_85

    .line 880
    const/4 v1, 0x4

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 881
    const/4 v1, 0x5

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 882
    if-nez v2, :cond_43

    if-eqz v4, :cond_83

    :cond_43
    new-instance v1, Lo/u;

    invoke-direct {v1, v2, v4}, Lo/u;-><init>(II)V

    move-object v4, v1

    .line 883
    :goto_49
    const/4 v1, 0x2

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 884
    const/4 v1, 0x6

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 885
    if-nez v2, :cond_57

    if-eqz v1, :cond_2f

    .line 886
    :cond_57
    if-nez v1, :cond_5a

    move-object v1, v2

    .line 889
    :cond_5a
    new-instance v9, LO/U;

    const/4 v10, 0x0

    invoke-direct {v9, v1, v4, v2, v10}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    const/4 v1, 0x3

    invoke-static {v9, v1}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v2

    .line 895
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v1, :cond_7d

    if-eqz v4, :cond_7d

    .line 896
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v1, v4}, LaH/h;->b(Lo/u;)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/Y;->a(F)V

    .line 897
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v1, v4}, LaH/h;->a(Lo/u;)F

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/Y;->b(F)V

    .line 899
    :cond_7d
    add-int/lit8 v1, v0, 0x1

    aput-object v2, v8, v0

    move v0, v1

    goto :goto_2f

    :cond_83
    move-object v4, v3

    .line 882
    goto :goto_49

    .line 902
    :cond_85
    monitor-exit p0
    :try_end_86
    .catchall {:try_start_2f .. :try_end_86} :catchall_b0

    .line 903
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 904
    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->k()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v8, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 905
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 910
    :cond_97
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/ad;->e:Z

    if-eqz v0, :cond_b3

    const/4 v0, 0x1

    .line 911
    :goto_9c
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_ac

    .line 912
    const v0, 0x7f0d00d8

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->c(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 914
    :cond_ac
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/app/ad;->b(Ljava/util/ArrayList;)V

    .line 915
    return-void

    .line 902
    :catchall_b0
    move-exception v0

    :try_start_b1
    monitor-exit p0
    :try_end_b2
    .catchall {:try_start_b1 .. :try_end_b2} :catchall_b0

    throw v0

    :cond_b3
    move v0, v6

    .line 910
    goto :goto_9c
.end method


# virtual methods
.method public a(LaH/h;)V
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 838
    .line 840
    monitor-enter p0

    .line 841
    :try_start_3
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v2, :cond_4d

    if-eqz p1, :cond_4d

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v2, p1}, LaH/h;->distanceTo(Landroid/location/Location;)F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v4, 0x4069

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4d

    move v3, v1

    .line 843
    :goto_17
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v2, :cond_4f

    if-eqz p1, :cond_4f

    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    invoke-virtual {v2}, LaH/h;->e()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x40c3880000000000L

    cmpl-double v2, v4, v6

    if-lez v2, :cond_4f

    move v2, v1

    .line 850
    :goto_33
    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/ad;->d:LaH/h;

    if-eqz v4, :cond_3b

    if-nez v2, :cond_3b

    if-eqz v3, :cond_3c

    :cond_3b
    move v0, v1

    .line 851
    :cond_3c
    if-eqz v0, :cond_41

    .line 852
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/app/T;->a(LaH/h;)V

    .line 854
    :cond_41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_51

    .line 856
    if-eqz v0, :cond_4c

    .line 857
    new-instance v0, Lcom/google/android/maps/driveabout/app/ae;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ae;-><init>(Lcom/google/android/maps/driveabout/app/ad;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ae;->start()V

    .line 865
    :cond_4c
    return-void

    :cond_4d
    move v3, v0

    .line 841
    goto :goto_17

    :cond_4f
    move v2, v0

    .line 843
    goto :goto_33

    .line 854
    :catchall_51
    move-exception v0

    :try_start_52
    monitor-exit p0
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_51

    throw v0
.end method
