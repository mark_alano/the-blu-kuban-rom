.class public Lcom/google/android/maps/driveabout/app/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/app/ProgressDialog;

.field private c:Landroid/content/DialogInterface$OnCancelListener;

.field private d:Landroid/app/Dialog;

.field private e:Lcom/google/android/maps/driveabout/app/bt;

.field private f:Landroid/app/Dialog;

.field private g:Landroid/app/Dialog;

.field private h:Landroid/app/Dialog;

.field private i:Landroid/app/Dialog;

.field private j:Landroid/app/Dialog;

.field private k:Landroid/app/Dialog;

.field private l:Landroid/widget/CheckBox;

.field private m:Landroid/app/AlertDialog;

.field private n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

.field private o:Landroid/widget/CheckBox;

.field private p:Landroid/app/Dialog;

.field private q:Landroid/app/Dialog;

.field private r:Lcom/google/android/maps/driveabout/app/dF;

.field private s:Landroid/app/Dialog;

.field private t:Landroid/app/Dialog;

.field private u:Landroid/app/Dialog;

.field private v:Landroid/app/Dialog;

.field private w:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    .line 155
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/RecordingLevelsView;)Lcom/google/android/maps/driveabout/app/RecordingLevelsView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/an;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/an;)Landroid/app/Dialog;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    return-object v0
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private v()Z
    .registers 2

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_bc

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_1e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_2a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_36
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_42
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_4e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_5a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    if-eqz v0, :cond_66

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_66
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    if-eqz v0, :cond_72

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dF;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_72
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_7e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_7e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    if-eqz v0, :cond_8a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_8a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    if-eqz v0, :cond_96

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_96
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    if-eqz v0, :cond_a2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_a2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    if-eqz v0, :cond_ae

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_ae
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    if-eqz v0, :cond_ba

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_bc

    :cond_ba
    const/4 v0, 0x1

    :goto_bb
    return v0

    :cond_bc
    const/4 v0, 0x0

    goto :goto_bb
.end method

.method private w()Landroid/app/AlertDialog$Builder;
    .registers 4

    .prologue
    .line 193
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method private x()V
    .registers 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 929
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    .line 932
    :cond_c
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 1074
    const-string v0, "loading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1075
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    .line 1087
    :goto_a
    return-object v0

    .line 1076
    :cond_b
    const-string v0, "destinations"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1077
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    goto :goto_a

    .line 1078
    :cond_16
    const-string v0, "layers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1079
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    goto :goto_a

    .line 1080
    :cond_21
    const-string v0, "fatal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1081
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    goto :goto_a

    .line 1082
    :cond_2c
    const-string v0, "exitconfirmation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1083
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    goto :goto_a

    .line 1084
    :cond_37
    const-string v0, "routeoptions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 1085
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    goto :goto_a

    .line 1087
    :cond_42
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a()V
    .registers 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->b()V

    .line 199
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->g()V

    .line 201
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->i()V

    .line 202
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->l()V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->j()V

    .line 204
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->o()V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->c()V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->e()V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->f()V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->q()V

    .line 210
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->x()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->s()V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->t()V

    .line 213
    return-void
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->v()Z

    move-result v0

    if-nez v0, :cond_7

    .line 241
    :goto_6
    return-void

    .line 222
    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;I)Landroid/text/Spannable;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_17

    .line 224
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 226
    :cond_17
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    .line 227
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_36

    .line 230
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 232
    :cond_36
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ao;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ao;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_6
.end method

.method public a(IF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    if-eqz v0, :cond_9

    .line 729
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;->setSample(IF)V

    .line 731
    :cond_9
    return-void
.end method

.method public a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    .line 407
    :goto_4
    return-void

    .line 385
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    .line 387
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3, p5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 391
    if-eqz p4, :cond_27

    if-eqz p6, :cond_27

    .line 392
    const v1, 0x7f0d007c

    new-instance v2, Lcom/google/android/maps/driveabout/app/aA;

    invoke-direct {v2, p0, p6}, Lcom/google/android/maps/driveabout/app/aA;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 400
    :cond_27
    if-eqz p6, :cond_38

    .line 401
    invoke-virtual {v0, p6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 405
    :goto_2c
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    .line 406
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_4

    .line 403
    :cond_38
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    goto :goto_2c
.end method

.method public a(IILandroid/content/DialogInterface$OnClickListener;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 420
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 434
    :goto_6
    return-void

    .line 424
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->e()V

    .line 426
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007a

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 432
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    .line 433
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_6
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 655
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 700
    :goto_6
    return-void

    .line 658
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    .line 660
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 661
    const v1, 0x7f040047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 663
    const v0, 0x7f10012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    .line 665
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/RecordingLevelsView;->setNumSamples(I)V

    .line 667
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_57

    .line 669
    const v0, 0x7f10012b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    .line 670
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const-string v3, "RmiMail"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-static {v2, v3, v4}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 672
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 675
    :cond_57
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d0097

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007d

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007c

    new-instance v2, Lcom/google/android/maps/driveabout/app/aE;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/aE;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    .line 688
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ap;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ap;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_6
.end method

.method public a(LO/U;Lcom/google/android/maps/driveabout/app/cQ;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 769
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_8

    .line 808
    :goto_7
    return-void

    .line 772
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->p()V

    .line 774
    invoke-virtual {p1}, LO/U;->h()Lbl/h;

    move-result-object v5

    .line 775
    invoke-virtual {v5}, Lbl/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/c;

    .line 776
    invoke-virtual {v0}, Lbl/c;->d()Ljava/util/List;

    move-result-object v4

    .line 777
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/CharSequence;

    move v2, v1

    .line 778
    :goto_24
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3a

    .line 779
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbl/c;

    invoke-virtual {v1}, Lbl/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    .line 778
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_24

    .line 783
    :cond_3a
    invoke-virtual {v0}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LO/U;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lbl/a;->a(Lbl/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lbl/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v0, Lcom/google/android/maps/driveabout/app/aq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aq;-><init>(Lcom/google/android/maps/driveabout/app/an;LO/U;Lcom/google/android/maps/driveabout/app/cQ;Ljava/util/List;Lbl/h;)V

    invoke-virtual {v7, v6, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    goto :goto_7
.end method

.method a(Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 2
    .parameter

    .prologue
    .line 818
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/an;->c:Landroid/content/DialogInterface$OnCancelListener;

    .line 819
    return-void
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;)V
    .registers 5
    .parameter

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 476
    :goto_6
    return-void

    .line 456
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->f()V

    .line 458
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007f

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007b

    new-instance v2, Lcom/google/android/maps/driveabout/app/aC;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/aC;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/aB;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/aB;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    .line 475
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_6
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 562
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 574
    :goto_6
    return-void

    .line 566
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->k()V

    .line 567
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0072

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007a

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 572
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    .line 573
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_6
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 534
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 549
    :goto_6
    return-void

    .line 538
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->j()V

    .line 539
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0071

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0070

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007a

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 544
    if-eqz p3, :cond_2f

    .line 545
    const v1, 0x7f0d007b

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 547
    :cond_2f
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    .line 548
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_6
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aG;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 997
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1060
    :goto_8
    return-void

    .line 1000
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->t()V

    .line 1003
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1004
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 1005
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setGravity(I)V

    .line 1006
    const v1, 0x7f0d004e

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 1007
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v2, 0x1010041

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    .line 1008
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const-string v2, "HomeAddress"

    invoke-static {v1, v2, v4}, LR/s;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1010
    if-eqz v1, :cond_3b

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3b

    .line 1011
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1013
    :cond_3b
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 1014
    new-instance v1, Lcom/google/android/maps/driveabout/app/av;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/av;-><init>(Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1028
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d004d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f020110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d0049

    new-instance v3, Lcom/google/android/maps/driveabout/app/aw;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/maps/driveabout/app/aw;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/widget/EditText;Lcom/google/android/maps/driveabout/app/aG;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    .line 1059
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_8
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 491
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 500
    :goto_6
    return-void

    .line 495
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->g()V

    .line 496
    new-instance v0, Lcom/google/android/maps/driveabout/app/bt;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bt;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    .line 499
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->show()V

    goto :goto_6
.end method

.method public a(Ljava/lang/String;Lcom/google/android/maps/driveabout/app/dJ;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 259
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_8

    .line 270
    :goto_7
    return-void

    .line 263
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->c()V

    .line 264
    new-instance v0, Lcom/google/android/maps/driveabout/app/dF;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/dF;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    .line 265
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    new-instance v1, Lcom/google/android/maps/driveabout/app/aF;

    invoke-direct {v1, v3}, Lcom/google/android/maps/driveabout/app/aF;-><init>(Lcom/google/android/maps/driveabout/app/ao;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dF;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v2, 0x7f0d00d3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f020465

    const v5, 0x7f0d0081

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/dF;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 268
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dF;->show()V

    .line 269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1, p2}, Lcom/google/android/maps/driveabout/app/dF;->a(ILcom/google/android/maps/driveabout/app/dJ;)V

    goto :goto_7
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 708
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_12

    .line 709
    if-eqz p1, :cond_13

    .line 710
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 714
    :goto_c
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    .line 715
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->n:Lcom/google/android/maps/driveabout/app/RecordingLevelsView;

    .line 716
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->o:Landroid/widget/CheckBox;

    .line 718
    :cond_12
    return-void

    .line 712
    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_c
.end method

.method public a([LO/U;ILcom/google/android/maps/driveabout/app/cR;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x21

    const/4 v6, 0x0

    .line 296
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_a

    .line 347
    :goto_9
    return-void

    .line 300
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    .line 301
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 302
    const/4 v1, 0x1

    if-ne p2, v1, :cond_97

    .line 303
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 304
    new-instance v2, Landroid/text/SpannableStringBuilder;

    const v3, 0x7f0d009f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 306
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v5, 0x1030042

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v6, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 308
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 309
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 310
    const v4, 0x7f0d00a1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 311
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    const v5, 0x7f0f0003

    invoke-direct {v1, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 314
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 318
    :goto_5e
    new-instance v1, Lcom/google/android/maps/driveabout/app/am;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    array-length v3, p1

    invoke-direct {v1, v2, p1, v3, v6}, Lcom/google/android/maps/driveabout/app/am;-><init>(Landroid/content/Context;[LO/U;IZ)V

    .line 320
    const v2, 0x7f020156

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 321
    new-instance v2, Lcom/google/android/maps/driveabout/app/ax;

    invoke-direct {v2, p0, p3}, Lcom/google/android/maps/driveabout/app/ax;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    new-instance v2, Lcom/google/android/maps/driveabout/app/ay;

    invoke-direct {v2, p0, p3, p1}, Lcom/google/android/maps/driveabout/app/ay;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;[LO/U;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 335
    const/4 v1, 0x2

    if-ne p2, v1, :cond_8a

    .line 336
    const v1, 0x7f0d007c

    new-instance v2, Lcom/google/android/maps/driveabout/app/az;

    invoke-direct {v2, p0, p3}, Lcom/google/android/maps/driveabout/app/az;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/cR;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 345
    :cond_8a
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    .line 346
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_9

    .line 316
    :cond_97
    const v1, 0x7f0d00a0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_5e
.end method

.method public a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 830
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_9

    .line 889
    :goto_8
    return-void

    .line 833
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->q()V

    .line 835
    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v4

    .line 837
    array-length v0, p1

    new-array v5, v0, [LO/b;

    .line 838
    array-length v0, p1

    invoke-static {p1, v1, v5, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 839
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 840
    :goto_1d
    array-length v2, p1

    if-ge v0, v2, :cond_3c

    .line 841
    aget-object v2, p1, v0

    invoke-virtual {v2}, LO/b;->b()I

    move-result v2

    invoke-virtual {v4, v2}, LO/c;->a(I)LO/e;

    move-result-object v2

    .line 842
    if-eqz v2, :cond_39

    invoke-virtual {v2}, LO/e;->b()I

    move-result v2

    if-nez v2, :cond_39

    .line 844
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 840
    :cond_39
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 848
    :cond_3c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v7, v0, [Z

    .line 849
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v8, v0, [Ljava/lang/String;

    move v2, v1

    .line 850
    :goto_49
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_78

    .line 851
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v9, p1, v0

    .line 852
    invoke-virtual {v9}, LO/b;->c()I

    move-result v0

    if-ne v0, v3, :cond_76

    move v0, v3

    :goto_62
    aput-boolean v0, v7, v2

    .line 853
    invoke-virtual {v9}, LO/b;->b()I

    move-result v0

    invoke-virtual {v4, v0}, LO/c;->a(I)LO/e;

    move-result-object v0

    .line 854
    invoke-virtual {v0}, LO/e;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    .line 850
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_49

    :cond_76
    move v0, v1

    .line 852
    goto :goto_62

    .line 857
    :cond_78
    new-instance v0, Lcom/google/android/maps/driveabout/app/ar;

    invoke-direct {v0, p0, v6, v5, v8}, Lcom/google/android/maps/driveabout/app/ar;-><init>(Lcom/google/android/maps/driveabout/app/an;Ljava/util/ArrayList;[LO/b;[Ljava/lang/String;)V

    .line 869
    new-instance v1, Lcom/google/android/maps/driveabout/app/as;

    invoke-direct {v1, p0, p2, v5}, Lcom/google/android/maps/driveabout/app/as;-><init>(Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/aH;[LO/b;)V

    .line 882
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0d00d6

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v8, v7, v0}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0d007a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 887
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    .line 888
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_8
.end method

.method public b()V
    .registers 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_c

    .line 246
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->b:Landroid/app/ProgressDialog;

    .line 249
    :cond_c
    return-void
.end method

.method public b(Landroid/content/DialogInterface$OnClickListener;)V
    .registers 5
    .parameter

    .prologue
    .line 740
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 751
    :goto_6
    return-void

    .line 743
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->o()V

    .line 745
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d005f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007a

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    goto :goto_6
.end method

.method public b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 587
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 616
    :goto_6
    return-void

    .line 590
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->l()V

    .line 592
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 593
    const v1, 0x7f040035

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 595
    const v0, 0x7f1000ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    .line 598
    const v0, 0x7f1000e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 599
    const v2, 0x7f0d0033

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 602
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007e

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0080

    new-instance v2, Lcom/google/android/maps/driveabout/app/aD;

    invoke-direct {v2, p0, p2}, Lcom/google/android/maps/driveabout/app/aD;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_6
.end method

.method public c()V
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    if-eqz v0, :cond_11

    .line 274
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dF;->cancel()V

    .line 275
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dF;->dismiss()V

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->r:Lcom/google/android/maps/driveabout/app/dF;

    .line 278
    :cond_11
    return-void
.end method

.method public c(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 942
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-nez v0, :cond_7

    .line 970
    :goto_6
    return-void

    .line 945
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->s()V

    .line 947
    new-instance v0, Lcom/google/android/maps/driveabout/app/at;

    invoke-direct {v0, p0, p2}, Lcom/google/android/maps/driveabout/app/at;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 957
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d00e2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d00e3

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d007a

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0d007c

    new-instance v3, Lcom/google/android/maps/driveabout/app/au;

    invoke-direct {v3, p0, v0}, Lcom/google/android/maps/driveabout/app/au;-><init>(Lcom/google/android/maps/driveabout/app/an;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 968
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    .line 969
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_6
.end method

.method public d()V
    .registers 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 351
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->d:Landroid/app/Dialog;

    .line 354
    :cond_c
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 439
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->j:Landroid/app/Dialog;

    .line 442
    :cond_c
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 481
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->s:Landroid/app/Dialog;

    .line 484
    :cond_c
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    if-eqz v0, :cond_c

    .line 504
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bt;->dismiss()V

    .line 505
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->e:Lcom/google/android/maps/driveabout/app/bt;

    .line 507
    :cond_c
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_d

    .line 520
    :cond_c
    :goto_c
    return-void

    .line 514
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/an;->i()V

    .line 515
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0096

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {}, LJ/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    .line 519
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_c
.end method

.method public i()V
    .registers 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 524
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->f:Landroid/app/Dialog;

    .line 527
    :cond_c
    return-void
.end method

.method public j()V
    .registers 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 553
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 554
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->g:Landroid/app/Dialog;

    .line 556
    :cond_c
    return-void
.end method

.method public k()V
    .registers 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 578
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 579
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->h:Landroid/app/Dialog;

    .line 581
    :cond_c
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 619
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_e

    .line 620
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 621
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    .line 622
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    .line 624
    :cond_e
    return-void
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public o()V
    .registers 2

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 755
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 756
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->p:Landroid/app/Dialog;

    .line 758
    :cond_c
    return-void
.end method

.method public p()V
    .registers 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 812
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 813
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->q:Landroid/app/Dialog;

    .line 815
    :cond_c
    return-void
.end method

.method public q()V
    .registers 2

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 893
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 894
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->t:Landroid/app/Dialog;

    .line 896
    :cond_c
    return-void
.end method

.method public r()V
    .registers 4

    .prologue
    .line 914
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->u()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_d

    .line 925
    :cond_c
    :goto_c
    return-void

    .line 918
    :cond_d
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->x()V

    .line 919
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/an;->w()Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d00e0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d00e1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d007a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    .line 924
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_c
.end method

.method public s()V
    .registers 2

    .prologue
    .line 973
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 974
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 975
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->v:Landroid/app/Dialog;

    .line 977
    :cond_c
    return-void
.end method

.method public t()V
    .registers 2

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 1064
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1065
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/an;->w:Landroid/app/Dialog;

    .line 1067
    :cond_c
    return-void
.end method
