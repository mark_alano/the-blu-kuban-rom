.class Lcom/google/android/maps/driveabout/app/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/google/android/maps/driveabout/app/aG;

.field final synthetic c:Lcom/google/android/maps/driveabout/app/an;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/an;Landroid/widget/EditText;Lcom/google/android/maps/driveabout/app/aG;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1032
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/aw;->c:Lcom/google/android/maps/driveabout/app/an;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/aw;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/aw;->b:Lcom/google/android/maps/driveabout/app/aG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aw;->c:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->t()V

    .line 1041
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aw;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1042
    if-eqz v0, :cond_17

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 1044
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aw;->c:Lcom/google/android/maps/driveabout/app/an;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/an;->b(Lcom/google/android/maps/driveabout/app/an;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "HomeAddress"

    invoke-virtual {v0, v1}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1052
    :goto_27
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aw;->b:Lcom/google/android/maps/driveabout/app/aG;

    if-eqz v0, :cond_30

    .line 1053
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/aw;->b:Lcom/google/android/maps/driveabout/app/aG;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/aG;->a()V

    .line 1055
    :cond_30
    return-void

    .line 1047
    :cond_31
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/aw;->c:Lcom/google/android/maps/driveabout/app/an;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/an;->b(Lcom/google/android/maps/driveabout/app/an;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "HomeAddress"

    invoke-static {v1, v2, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_27
.end method
