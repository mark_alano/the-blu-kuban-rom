.class public Lcom/google/android/maps/driveabout/app/bK;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/bF;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/bC;

.field private final b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private final c:Lcom/google/android/maps/driveabout/vector/A;

.field private d:Lk/b;

.field private final e:Lu/d;

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/Bitmap;

.field private i:Ljava/util/HashSet;

.field private final j:Ljava/util/List;

.field private volatile k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/NavigationMapView;Lcom/google/android/maps/driveabout/vector/A;LA/c;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    .line 117
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    .line 118
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    .line 119
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    .line 123
    new-instance v0, Lu/d;

    invoke-direct {v0, p4, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    .line 124
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    invoke-virtual {v0, v1}, Lu/d;->a(LA/b;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    new-instance v1, Lcom/google/android/maps/driveabout/app/bN;

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/bN;-><init>(Lcom/google/android/maps/driveabout/app/bK;Lcom/google/android/maps/driveabout/app/bL;)V

    invoke-virtual {v0, v1}, Lu/d;->a(Lu/i;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bK;->f:I

    .line 129
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bK;->g:I

    .line 131
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020133

    invoke-static {v0, v1}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->h:Landroid/graphics/Bitmap;

    .line 133
    return-void
.end method


# virtual methods
.method a(LC/a;)V
    .registers 11
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 214
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 216
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->d()[Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v4

    move v1, v2

    .line 220
    :goto_d
    array-length v0, v4

    if-ge v1, v0, :cond_51

    .line 221
    aget-object v5, v4, v1

    .line 222
    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/bE;->e()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(F)Z

    move-result v0

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/maps/driveabout/app/bE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 224
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_34
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 225
    new-instance v7, Lr/s;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/bE;->i()Lo/J;

    move-result-object v8

    invoke-direct {v7, v0, v8}, Lr/s;-><init>(Lo/aq;Lo/J;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_34

    .line 220
    :cond_4d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 236
    :cond_51
    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    .line 238
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    .line 240
    :goto_59
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7c

    .line 241
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/s;

    .line 242
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_72

    .line 243
    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_6e
    :goto_6e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_59

    .line 244
    :cond_72
    invoke-virtual {p0, v0, v4}, Lcom/google/android/maps/driveabout/app/bK;->a(Lr/s;[Lcom/google/android/maps/driveabout/app/bE;)Z

    move-result v7

    if-eqz v7, :cond_6e

    .line 245
    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_6e

    .line 251
    :cond_7c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->q()LF/H;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_9e

    instance-of v1, v0, Lcom/google/android/maps/driveabout/app/bM;

    if-eqz v1, :cond_9e

    .line 253
    check-cast v0, Lcom/google/android/maps/driveabout/app/bM;

    move v1, v2

    .line 255
    :goto_8b
    array-length v3, v4

    if-ge v1, v3, :cond_b6

    .line 256
    aget-object v3, v4, v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bM;->t()Lcom/google/android/maps/driveabout/app/bE;

    move-result-object v7

    if-ne v3, v7, :cond_b3

    .line 257
    const/4 v0, 0x1

    .line 261
    :goto_97
    if-nez v0, :cond_9e

    .line 262
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p()V

    .line 268
    :cond_9e
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_ae

    .line 269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v1, Lcom/google/android/maps/driveabout/app/bO;

    invoke-direct {v1, v5}, Lcom/google/android/maps/driveabout/app/bO;-><init>(Ljava/util/HashSet;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/J;Z)V

    .line 272
    :cond_ae
    iput-object v6, p0, Lcom/google/android/maps/driveabout/app/bK;->i:Ljava/util/HashSet;

    .line 273
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    .line 274
    return-void

    .line 255
    :cond_b3
    add-int/lit8 v1, v1, 0x1

    goto :goto_8b

    :cond_b6
    move v0, v2

    goto :goto_97
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    if-eqz v0, :cond_7

    .line 152
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/bK;->a(LC/a;)V

    .line 154
    :cond_7
    return-void
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LD/a;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    if-nez v0, :cond_15

    .line 168
    new-instance v0, Lk/b;

    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    invoke-direct {v0, v1}, Lk/b;-><init>(LA/b;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    .line 170
    :cond_15
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bC;)V
    .registers 3
    .parameter

    .prologue
    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    .line 341
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->b:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->postInvalidate()V

    .line 342
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Z)V

    .line 181
    return-void
.end method

.method a(Lr/s;[Lcom/google/android/maps/driveabout/app/bE;)Z
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Lo/aq;)LF/T;

    move-result-object v0

    .line 287
    if-eqz v0, :cond_28

    move v1, v9

    .line 289
    :goto_c
    array-length v2, p2

    if-ge v1, v2, :cond_8c

    .line 290
    aget-object v2, p2, v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bE;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lr/s;->l()Lo/J;

    move-result-object v3

    invoke-virtual {v3}, Lo/J;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 291
    aget-object v6, p2, v1

    .line 295
    :goto_25
    if-nez v6, :cond_2c

    move v9, v10

    .line 335
    :cond_28
    :goto_28
    return v9

    .line 289
    :cond_29
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 299
    :cond_2c
    check-cast v0, LF/Y;

    .line 300
    new-instance v11, Lcom/google/android/maps/driveabout/vector/aF;

    invoke-direct {v11}, Lcom/google/android/maps/driveabout/vector/aF;-><init>()V

    .line 301
    invoke-virtual {v0, v11}, LF/Y;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    .line 302
    :cond_36
    :goto_36
    invoke-virtual {v11}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 303
    invoke-virtual {v11}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v0

    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v5

    .line 304
    instance-of v0, v5, Lo/U;

    if-eqz v0, :cond_36

    .line 305
    check-cast v5, Lo/U;

    .line 311
    :try_start_4a
    invoke-virtual {v5}, Lo/U;->p()Lo/H;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5a
    .catch Ljava/lang/NumberFormatException; {:try_start_4a .. :try_end_5a} :catch_85

    move-result v0

    .line 316
    :goto_5b
    invoke-virtual {v6, v0}, Lcom/google/android/maps/driveabout/app/bE;->c(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 318
    if-eqz v1, :cond_36

    .line 321
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    .line 322
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    .line 323
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/bE;->f()Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 324
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bK;->h:Landroid/graphics/Bitmap;

    .line 325
    iget v3, p0, Lcom/google/android/maps/driveabout/app/bK;->f:I

    .line 326
    iget v4, p0, Lcom/google/android/maps/driveabout/app/bK;->g:I

    .line 328
    :goto_79
    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/bK;->c:Lcom/google/android/maps/driveabout/vector/A;

    new-instance v0, Lcom/google/android/maps/driveabout/app/bM;

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/bM;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILo/U;Lcom/google/android/maps/driveabout/app/bE;Lr/s;Lcom/google/android/maps/driveabout/app/bL;)V

    invoke-virtual {v12, v0}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto :goto_36

    .line 313
    :catch_85
    move-exception v0

    move v0, v9

    goto :goto_5b

    :cond_88
    move v9, v10

    .line 333
    goto :goto_28

    :cond_8a
    move-object v2, v8

    goto :goto_79

    :cond_8c
    move-object v6, v8

    goto :goto_25
.end method

.method public b(LC/a;LD/a;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 137
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->a:Lcom/google/android/maps/driveabout/app/bC;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->e()I

    move-result v0

    if-nez v0, :cond_a

    .line 146
    :cond_9
    :goto_9
    return v2

    .line 140
    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->d:Lk/b;

    invoke-virtual {v0, p1}, Lk/b;->a(LC/a;)Ljava/util/List;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-static {v1, v0}, Lk/b;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 142
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 143
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 144
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bK;->k:Z

    goto :goto_9
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 174
    const/4 v0, 0x0

    .line 175
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v1, v0}, Lu/d;->a(Z)V

    .line 176
    return-void
.end method

.method public i_()V
    .registers 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bK;->e:Lu/d;

    invoke-virtual {v0}, Lu/d;->g()V

    .line 186
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
