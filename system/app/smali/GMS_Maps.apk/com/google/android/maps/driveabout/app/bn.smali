.class public Lcom/google/android/maps/driveabout/app/bn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:LO/U;

.field private c:[LO/U;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:[LO/b;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    const-string v0, "+"

    const-string v1, "%20"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    .line 122
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    .line 123
    return-void
.end method

.method private static a(Lbm/b;)I
    .registers 2
    .parameter

    .prologue
    .line 244
    invoke-virtual {p0}, Lbm/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 245
    const/4 v0, 0x2

    .line 249
    :goto_7
    return v0

    .line 246
    :cond_8
    invoke-virtual {p0}, Lbm/b;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 247
    const/4 v0, 0x3

    goto :goto_7

    .line 249
    :cond_10
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LO/U;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 265
    invoke-static {p1}, Lcom/google/android/maps/driveabout/app/bn;->b(Ljava/lang/String;)Lo/u;

    move-result-object v1

    .line 268
    if-nez p0, :cond_a

    if-nez v1, :cond_a

    .line 269
    const/4 v0, 0x0

    .line 272
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, LO/U;

    invoke-direct {v0, p0, v1, p2, p3}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public static a(I)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 773
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/bn;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 786
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 791
    const-string v0, "fd"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 793
    if-eqz p0, :cond_30

    .line 795
    packed-switch p0, :pswitch_data_42

    .line 803
    const-string v0, "d"

    .line 806
    :goto_2b
    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 809
    :cond_30
    if-eqz p1, :cond_37

    .line 810
    const-string v0, "entry"

    invoke-virtual {v1, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 813
    :cond_37
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 797
    :pswitch_3c
    const-string v0, "w"

    goto :goto_2b

    .line 800
    :pswitch_3f
    const-string v0, "b"

    goto :goto_2b

    .line 795
    :pswitch_data_42
    .packed-switch 0x2
        :pswitch_3c
        :pswitch_3f
    .end packed-switch
.end method

.method public static a(LO/U;I[LO/b;)Landroid/net/Uri;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 605
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(LO/U;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 621
    new-instance v0, Lcom/google/android/maps/driveabout/app/bo;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/bo;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bo;->a(I)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/bo;->a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/bo;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bo;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lax/y;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 685
    if-nez p0, :cond_7

    .line 686
    invoke-static {p1, p3}, Lcom/google/android/maps/driveabout/app/bn;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 698
    :goto_6
    return-object v0

    .line 690
    :cond_7
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v1

    .line 691
    invoke-virtual {p0}, Lax/y;->h()Ljava/lang/String;

    move-result-object v2

    .line 692
    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    .line 693
    invoke-virtual {p0}, Lax/y;->g()Ljava/lang/String;

    move-result-object v4

    .line 694
    new-instance v5, LO/U;

    if-nez v1, :cond_24

    const/4 v0, 0x0

    :goto_1c
    invoke-direct {v5, v3, v0, v2, v4}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    invoke-static {v5, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_6

    .line 694
    :cond_24
    new-instance v0, Lo/u;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v6

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-direct {v0, v6, v1}, Lo/u;-><init>(II)V

    goto :goto_1c
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 720
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 726
    if-eqz p0, :cond_24

    .line 727
    const-string v1, "q"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 729
    :cond_24
    if-eqz p1, :cond_2b

    .line 730
    const-string v1, "sll"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 732
    :cond_2b
    if-eqz p2, :cond_32

    .line 733
    const-string v1, "sspn"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 735
    :cond_32
    if-eqz p3, :cond_39

    .line 736
    const-string v1, "mode"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 738
    :cond_39
    if-eqz p4, :cond_46

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_46

    .line 739
    const-string v1, "entry"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 741
    :cond_46
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Z)Landroid/net/Uri;
    .registers 4
    .parameter

    .prologue
    .line 753
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 758
    const-string v1, "resume"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 759
    if-eqz p0, :cond_2d

    .line 760
    const-string v1, "sync_layers"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 762
    :cond_2d
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a([LO/U;I[LO/b;)Landroid/net/Uri;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 641
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/U;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a([LO/U;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 658
    new-instance v0, Lcom/google/android/maps/driveabout/app/bo;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/bo;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/bo;->a([LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bo;->a(I)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/bo;->a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/bo;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bo;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(LO/U;ILjava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x3d

    .line 849
    if-nez p0, :cond_6

    .line 850
    const/4 v0, 0x0

    .line 891
    :goto_5
    return-object v0

    .line 853
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 856
    const-string v1, "nav"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 857
    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    const-string v1, "daddr"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 864
    invoke-virtual {p0}, LO/U;->d()LO/V;

    move-result-object v1

    .line 865
    if-eqz v1, :cond_68

    .line 866
    invoke-virtual {v1}, LO/V;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    :goto_3a
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 873
    const-string v1, "dirflg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 874
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 875
    const/4 v1, 0x2

    if-ne p1, v1, :cond_70

    .line 876
    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 884
    :goto_4f
    if-eqz p2, :cond_63

    .line 885
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 886
    const-string v1, "entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 887
    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 888
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 891
    :cond_63
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 868
    :cond_68
    invoke-virtual {p0}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3a

    .line 877
    :cond_70
    const/4 v1, 0x3

    if-ne p1, v1, :cond_79

    .line 878
    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4f

    .line 880
    :cond_79
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4f
.end method

.method static synthetic a(Lo/u;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/bn;->b(Lo/u;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a([LO/b;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/bn;->b([LO/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 389
    const-string v0, "google.navigation"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "http"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private static b(Lo/u;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lo/u;->a()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lo/u;->b()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b([LO/b;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 345
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 346
    const/4 v0, 0x0

    :goto_6
    array-length v2, p0

    if-ge v0, v2, :cond_3d

    .line 347
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_14

    .line 348
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    :cond_14
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, p0, v0

    invoke-virtual {v3}, LO/b;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p0, v0

    invoke-virtual {v3}, LO/b;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 352
    :cond_3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lo/u;
    .registers 9
    .parameter

    .prologue
    const-wide v6, 0x412e848000000000L

    const/4 v0, 0x0

    .line 282
    if-nez p0, :cond_9

    .line 296
    :cond_8
    :goto_8
    return-object v0

    .line 286
    :cond_9
    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 287
    if-eqz v1, :cond_8

    array-length v2, v1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_8

    .line 289
    const/4 v2, 0x0

    :try_start_16
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 290
    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 291
    new-instance v1, Lo/u;

    mul-double/2addr v2, v6

    double-to-int v2, v2

    mul-double v3, v4, v6

    double-to-int v3, v3

    invoke-direct {v1, v2, v3}, Lo/u;-><init>(II)V
    :try_end_2d
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_2d} :catch_2f

    move-object v0, v1

    goto :goto_8

    .line 292
    :catch_2f
    move-exception v1

    goto :goto_8
.end method

.method private static c(Ljava/lang/String;)[LO/b;
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x2

    const/4 v2, 0x0

    .line 317
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 318
    new-array v0, v2, [LO/b;

    .line 336
    :cond_b
    :goto_b
    return-object v0

    .line 320
    :cond_c
    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 321
    array-length v1, v5

    new-array v1, v1, [LO/b;

    .line 323
    array-length v6, v5

    move v3, v2

    :goto_17
    if-ge v2, v6, :cond_3f

    aget-object v4, v5, v2

    .line 324
    const-string v7, ":"

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 325
    array-length v7, v4

    if-ne v7, v10, :cond_b

    .line 329
    const/4 v7, 0x0

    :try_start_25
    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 330
    const/4 v8, 0x1

    aget-object v4, v4, v8

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 331
    add-int/lit8 v4, v3, 0x1

    new-instance v9, LO/b;

    invoke-direct {v9, v7, v8}, LO/b;-><init>(II)V

    aput-object v9, v1, v3
    :try_end_3b
    .catch Ljava/lang/NumberFormatException; {:try_start_25 .. :try_end_3b} :catch_41

    .line 323
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_17

    :cond_3f
    move-object v0, v1

    .line 336
    goto :goto_b

    .line 332
    :catch_41
    move-exception v1

    goto :goto_b
.end method

.method public static l()Landroid/net/Uri;
    .registers 3

    .prologue
    .line 823
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "quitquitquit"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 829
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private m()Z
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 362
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    const-string v3, "http:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 382
    :cond_17
    :goto_17
    return v0

    .line 366
    :cond_18
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 367
    const-string v3, "1"

    const-string v4, "nav"

    invoke-virtual {v2, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 372
    const-string v3, "daddr"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 373
    if-eqz v3, :cond_17

    .line 377
    new-instance v4, Lbm/b;

    const-string v5, "dirflg"

    invoke-virtual {v2, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Lbm/b;-><init>(Ljava/lang/String;)V

    .line 379
    invoke-static {v4}, Lcom/google/android/maps/driveabout/app/bn;->a(Lbm/b;)I

    move-result v2

    iput v2, p0, Lcom/google/android/maps/driveabout/app/bn;->d:I

    .line 381
    new-array v2, v1, [LO/U;

    new-instance v4, LO/U;

    invoke-direct {v4, v3, v6, v6, v6}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v0

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    move v0, v1

    .line 382
    goto :goto_17
.end method


# virtual methods
.method public a()Z
    .registers 9

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 132
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/bn;->m()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 133
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bn;->i:Z

    move v0, v2

    .line 240
    :goto_c
    return v0

    .line 137
    :cond_d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    const-string v1, "google.navigation:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    move v0, v3

    .line 138
    goto :goto_c

    .line 145
    :cond_19
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    const-string v1, "google.navigation:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 147
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&x=y"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 155
    :goto_47
    const-string v0, "quitquitquit"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string v4, "true"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 157
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    move v0, v2

    .line 158
    goto :goto_c

    .line 151
    :cond_5a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/?"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&x=y"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_47

    .line 162
    :cond_79
    const-string v0, "sync_layers"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v4, "true"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 164
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/bn;->j:Z

    .line 169
    :cond_89
    const-string v0, "resume"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    const-string v4, "true"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 171
    iput v7, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    move v0, v2

    .line 172
    goto/16 :goto_c

    .line 176
    :cond_9c
    new-instance v0, Lbm/b;

    const-string v4, "mode"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lbm/b;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bn;->a(Lbm/b;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->d:I

    .line 181
    const-string v0, "entry"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->e:Ljava/lang/String;

    .line 184
    const-string v0, "fd"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v4, "true"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 186
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    move v0, v2

    .line 187
    goto/16 :goto_c

    .line 190
    :cond_cd
    const-string v0, "opt"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bn;->c(Ljava/lang/String;)[LO/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->f:[LO/b;

    .line 191
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->f:[LO/b;

    if-nez v0, :cond_e0

    move v0, v3

    .line 192
    goto/16 :goto_c

    .line 195
    :cond_e0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 197
    const-string v0, "altvia"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 198
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_ef
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_110

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 199
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bn;->b(Ljava/lang/String;)Lo/u;

    move-result-object v0

    .line 200
    if-nez v0, :cond_104

    move v0, v3

    .line 201
    goto/16 :goto_c

    .line 203
    :cond_104
    new-instance v6, LO/U;

    invoke-direct {v6, v0}, LO/U;-><init>(Lo/u;)V

    .line 204
    invoke-virtual {v6, v7}, LO/U;->a(I)V

    .line 205
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_ef

    .line 208
    :cond_110
    const-string v0, "r"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->g:Ljava/lang/String;

    .line 210
    const-string v0, "s"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "sll"

    invoke-virtual {v1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "stitle"

    invoke-virtual {v1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "stoken"

    invoke-virtual {v1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v5, v6, v7}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LO/U;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->b:LO/U;

    .line 215
    const-string v0, "sr"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    const-string v5, "true"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14e

    .line 217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->b:LO/U;

    if-nez v0, :cond_14b

    move v0, v3

    .line 218
    goto/16 :goto_c

    .line 220
    :cond_14b
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    .line 223
    :cond_14e
    const-string v0, "q"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "ll"

    invoke-virtual {v1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "token"

    invoke-virtual {v1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v5, v6, v7}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LO/U;

    move-result-object v0

    .line 229
    if-nez v0, :cond_16f

    move v0, v3

    .line 230
    goto/16 :goto_c

    .line 232
    :cond_16f
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/U;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/U;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    .line 234
    const-string v0, "true"

    const-string v4, "goff"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19d

    .line 235
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    array-length v0, v0

    if-lt v0, v2, :cond_197

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->b:LO/U;

    if-nez v0, :cond_19a

    :cond_197
    move v0, v3

    .line 236
    goto/16 :goto_c

    .line 238
    :cond_19a
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    :cond_19d
    move v0, v2

    .line 240
    goto/16 :goto_c
.end method

.method public b()LO/U;
    .registers 3

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    array-length v0, v0

    if-lez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public c()LO/U;
    .registers 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->b:LO/U;

    return-object v0
.end method

.method public d()[LO/U;
    .registers 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->c:[LO/U;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 419
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bn;->d:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()[LO/b;
    .registers 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->f:[LO/b;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bn;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 449
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bn;->h:I

    return v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 456
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bn;->j:Z

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bn;->i:Z

    return v0
.end method
