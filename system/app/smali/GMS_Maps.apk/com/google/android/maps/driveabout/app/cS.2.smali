.class public Lcom/google/android/maps/driveabout/app/cS;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:LO/N;

.field protected b:Z

.field protected c:Z

.field protected d:Z

.field protected e:Lcom/google/android/maps/driveabout/vector/q;

.field protected f:I

.field protected g:Z

.field protected final h:Lcom/google/android/maps/driveabout/app/cq;

.field protected i:Landroid/content/Context;

.field private j:Z

.field private final k:Lcom/google/android/maps/driveabout/app/bC;

.field private final l:Lcom/google/android/maps/driveabout/app/cT;

.field private final m:Lcom/google/android/maps/driveabout/app/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/cT;Lcom/google/android/maps/driveabout/vector/bB;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    .line 37
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/cS;->m:Lcom/google/android/maps/driveabout/app/a;

    .line 39
    new-instance v0, Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    .line 40
    if-eqz p5, :cond_1d

    .line 41
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {v0, p5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/vector/bB;)V

    .line 43
    :cond_1d
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/cS;->k:Lcom/google/android/maps/driveabout/app/bC;

    .line 44
    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/cS;->l:Lcom/google/android/maps/driveabout/app/cT;

    .line 45
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    const-string v1, "HeadingUpPreferred"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    .line 47
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/app/cq;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->h:Lcom/google/android/maps/driveabout/app/cq;

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    .line 63
    return-void
.end method

.method public a(LO/N;)V
    .registers 2
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->a:LO/N;

    .line 59
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 2
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    .line 103
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->b:Z

    .line 95
    return-void
.end method

.method public b()LO/N;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->a:LO/N;

    return-object v0
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->j:Z

    .line 111
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    .line 67
    return-void
.end method

.method public c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->g:Z

    .line 128
    return-void
.end method

.method public d()I
    .registers 2

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cS;->f:I

    return v0
.end method

.method public d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/cS;->c:Z

    .line 145
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    return v0
.end method

.method public f()V
    .registers 4

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    .line 85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->i:Landroid/content/Context;

    const-string v1, "HeadingUpPreferred"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cS;->d:Z

    invoke-static {v0, v1, v2}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 87
    return-void

    .line 84
    :cond_11
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->b:Z

    return v0
.end method

.method public h()Lcom/google/android/maps/driveabout/vector/q;
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->e:Lcom/google/android/maps/driveabout/vector/q;

    return-object v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->j:Z

    return v0
.end method

.method public j()Lcom/google/android/maps/driveabout/app/a;
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->m:Lcom/google/android/maps/driveabout/app/a;

    return-object v0
.end method

.method public k()Lcom/google/android/maps/driveabout/app/bC;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->k:Lcom/google/android/maps/driveabout/app/bC;

    return-object v0
.end method

.method public l()Lcom/google/android/maps/driveabout/app/cT;
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cS;->l:Lcom/google/android/maps/driveabout/app/cT;

    return-object v0
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->g:Z

    return v0
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cS;->c:Z

    return v0
.end method
