.class public Lcom/google/android/maps/driveabout/app/cW;
.super Lcom/google/android/maps/driveabout/app/k;
.source "SourceFile"


# static fields
.field private static final e:F


# instance fields
.field private final d:[F

.field private final f:Z

.field private g:F


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 37
    const-wide/high16 v0, 0x3ff0

    const-wide/high16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/app/cW;->e:F

    return-void
.end method

.method public constructor <init>(FFFZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/k;-><init>()V

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    const/4 v1, 0x2

    aput p3, v0, v1

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    .line 50
    iput p1, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    .line 51
    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    .line 52
    return-void
.end method

.method private static a(LaH/h;)F
    .registers 5
    .parameter

    .prologue
    const/high16 v1, 0x4168

    const/high16 v0, 0x4138

    .line 190
    invoke-virtual {p0}, LaH/h;->f()Z

    move-result v2

    if-nez v2, :cond_b

    .line 202
    :cond_a
    :goto_a
    return v0

    .line 193
    :cond_b
    invoke-virtual {p0}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 194
    goto :goto_a

    .line 196
    :cond_19
    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v2

    invoke-virtual {v2}, Lo/af;->f()I

    move-result v2

    .line 197
    const/16 v3, 0x30

    if-gt v2, v3, :cond_27

    move v0, v1

    .line 198
    goto :goto_a

    .line 199
    :cond_27
    const/16 v1, 0x50

    if-gt v2, v1, :cond_a

    .line 200
    const/high16 v0, 0x4148

    goto :goto_a
.end method

.method private a(Lo/T;Lo/T;IF)F
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-virtual {p1, p2}, Lo/T;->c(Lo/T;)F

    move-result v0

    .line 98
    const/high16 v1, 0x4380

    mul-float/2addr v0, v1

    mul-float/2addr v0, p4

    int-to-float v1, p3

    const/high16 v2, 0x3f00

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    .line 100
    const/high16 v1, 0x41f0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v0, v2

    sget v2, Lcom/google/android/maps/driveabout/app/cW;->e:F

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    const v1, 0x3e4ccccd

    sub-float v1, v0, v1

    .line 102
    const/4 v0, 0x0

    :goto_20
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_37

    .line 103
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    aget v2, v2, v0

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_34

    .line 104
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    aget v0, v1, v0

    .line 107
    :goto_33
    return v0

    .line 102
    :cond_34
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 107
    :cond_37
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cW;->d:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_33
.end method


# virtual methods
.method public a(LC/b;F)LC/b;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 128
    .line 129
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_29

    move v4, p2

    .line 132
    :goto_6
    invoke-virtual {p1}, LC/b;->e()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_16

    invoke-virtual {p1}, LC/b;->d()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_28

    .line 134
    :cond_16
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, LC/b;->a()F

    move-result v2

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    .line 138
    :cond_28
    return-object p1

    :cond_29
    move v4, v3

    goto :goto_6
.end method

.method public a(LC/b;LO/N;Z)LC/b;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v1

    .line 116
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_16

    .line 117
    invoke-virtual {p2}, LO/N;->g()F

    move-result v4

    .line 119
    :goto_d
    new-instance v0, LC/b;

    const/high16 v2, 0x4182

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0

    :cond_16
    move v4, v3

    goto :goto_d
.end method

.method public a(LC/b;LaH/h;IIF)LC/b;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 181
    invoke-virtual {p2}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, LaH/h;->getLongitude()D

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v1

    .line 182
    new-instance v0, LC/b;

    invoke-static {p2}, Lcom/google/android/maps/driveabout/app/cW;->a(LaH/h;)F

    move-result v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method public a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 65
    cmpl-float v0, p5, v3

    if-ltz v0, :cond_19

    move v2, p5

    .line 79
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cW;->f:Z

    if-eqz v0, :cond_37

    .line 80
    invoke-virtual {p3}, Lo/S;->b()F

    move-result v4

    .line 82
    :goto_e
    new-instance v0, LC/b;

    invoke-virtual {p3}, Lo/S;->a()Lo/T;

    move-result-object v1

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0

    .line 67
    :cond_19
    if-eqz p4, :cond_34

    .line 68
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cW;->b:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p7, v0

    invoke-static {p6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 70
    invoke-virtual {p3}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {p4}, LO/N;->a()Lo/T;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0, p8}, Lcom/google/android/maps/driveabout/app/cW;->a(Lo/T;Lo/T;IF)F

    move-result v2

    .line 72
    iput v2, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    goto :goto_6

    .line 76
    :cond_34
    iget v2, p0, Lcom/google/android/maps/driveabout/app/cW;->g:F

    goto :goto_6

    :cond_37
    move v4, v3

    goto :goto_e
.end method

.method public a(LC/b;LaH/h;Lo/am;IIF)LC/b;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v8, 0x4380

    const/4 v3, 0x0

    .line 148
    if-nez p3, :cond_6

    .line 175
    :goto_5
    return-object p1

    .line 152
    :cond_6
    invoke-virtual {p3}, Lo/am;->f()Lo/ad;

    move-result-object v0

    .line 153
    const/4 v1, 0x3

    new-array v1, v1, [Lo/T;

    .line 154
    const/4 v2, 0x0

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    aput-object v4, v1, v2

    .line 155
    const/4 v2, 0x1

    invoke-virtual {v0}, Lo/ad;->e()Lo/T;

    move-result-object v0

    aput-object v0, v1, v2

    .line 156
    const/4 v0, 0x2

    invoke-virtual {p2}, LaH/h;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, LaH/h;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lo/T;->a(DD)Lo/T;

    move-result-object v2

    aput-object v2, v1, v0

    .line 158
    invoke-static {v1}, Lo/ad;->a([Lo/T;)Lo/ad;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v8

    mul-float/2addr v0, p6

    int-to-float v2, p4

    div-float/2addr v0, v2

    .line 162
    invoke-virtual {v1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v8

    mul-float/2addr v2, p6

    int-to-float v4, p5

    div-float/2addr v2, v4

    .line 165
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const v2, 0x3f4ccccd

    div-float/2addr v0, v2

    .line 167
    const/high16 v2, 0x41f0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v0, v4

    sget v4, Lcom/google/android/maps/driveabout/app/cW;->e:F

    mul-float/2addr v0, v4

    sub-float v0, v2, v0

    const v2, 0x3e4ccccd

    sub-float/2addr v0, v2

    .line 170
    const/high16 v2, 0x4100

    const/high16 v4, 0x4170

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 175
    new-instance v0, LC/b;

    invoke-virtual {v1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    goto :goto_5
.end method
