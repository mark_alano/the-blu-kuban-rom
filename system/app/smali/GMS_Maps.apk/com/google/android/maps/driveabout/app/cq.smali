.class public Lcom/google/android/maps/driveabout/app/cq;
.super Lcom/google/android/maps/driveabout/vector/bk;
.source "SourceFile"


# instance fields
.field private b:I

.field private c:I

.field private d:F

.field private e:I

.field private f:Z

.field private g:I

.field private h:Lcom/google/android/maps/driveabout/app/k;

.field private i:Ljava/util/HashMap;

.field private j:Lcom/google/android/maps/driveabout/app/bT;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;-><init>(Landroid/content/res/Resources;)V

    .line 77
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    .line 144
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/cq;->k()V

    .line 145
    iput v3, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    .line 146
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v3, v2, v2}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/k;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    .line 148
    const/high16 v0, 0x3fc0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/cq;->d(F)V

    .line 149
    return-void
.end method

.method private a(Lo/ad;)Lo/ad;
    .registers 12
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 841
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b002c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 843
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    sub-int v4, v0, v2

    .line 849
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 851
    mul-int/lit8 v0, v2, 0x3

    .line 855
    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    mul-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    sub-int/2addr v3, v2

    sub-int/2addr v3, v0

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 856
    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    mul-int/lit8 v7, v5, 0x2

    sub-int/2addr v6, v7

    sub-int/2addr v6, v4

    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 857
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v7

    div-int v3, v7, v3

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v7

    div-int v6, v7, v6

    if-le v3, v6, :cond_63

    .line 862
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0030

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    move v2, v1

    .line 865
    :goto_57
    add-int/2addr v2, v5

    add-int v3, v1, v5

    add-int/2addr v4, v5

    add-int/2addr v5, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;IIII)Lo/ad;

    move-result-object v0

    return-object v0

    :cond_63
    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_57
.end method

.method private a(Lo/ad;FFFF)Lo/ad;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 904
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v1, v1

    sub-float/2addr v1, p2

    sub-float/2addr v1, p3

    div-float/2addr v0, v1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    .line 906
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v2, v2

    sub-float/2addr v2, p2

    sub-float/2addr v2, p3

    div-float/2addr v1, v2

    mul-float/2addr v1, p3

    float-to-int v1, v1

    .line 908
    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v3, v3

    sub-float/2addr v3, p4

    sub-float/2addr v3, p5

    div-float/2addr v2, v3

    mul-float/2addr v2, p4

    float-to-int v2, v2

    .line 910
    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v4, v4

    sub-float/2addr v4, p4

    sub-float/2addr v4, p5

    div-float/2addr v3, v4

    mul-float/2addr v3, p5

    float-to-int v3, v3

    .line 913
    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    new-instance v5, Lo/T;

    invoke-direct {v5, v0, v3}, Lo/T;-><init>(II)V

    invoke-virtual {v4, v5}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    .line 914
    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v3

    new-instance v4, Lo/T;

    invoke-direct {v4, v1, v2}, Lo/T;-><init>(II)V

    invoke-virtual {v3, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    .line 915
    new-instance v2, Lo/ad;

    invoke-direct {v2, v0, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v2
.end method

.method private a(Lo/ad;IIII)Lo/ad;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 885
    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    int-to-float v5, p5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;FFFF)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/ad;Landroid/location/Location;)Lo/ad;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 781
    if-eqz p2, :cond_25

    .line 782
    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Lo/T;->a(D)D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v0, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    .line 789
    invoke-virtual {p1, v0}, Lo/ad;->a(Lo/ad;)Lo/ad;

    move-result-object p1

    .line 791
    :cond_25
    return-object p1
.end method

.method private a(Lo/ad;Lo/u;)Lo/ad;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 753
    if-eqz p2, :cond_38

    .line 754
    invoke-virtual {p2}, Lo/u;->a()I

    move-result v0

    invoke-virtual {p2}, Lo/u;->b()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    .line 756
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 758
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 760
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 762
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    .line 764
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Lo/T;IIII)Lo/ad;

    move-result-object p1

    .line 767
    :cond_38
    return-object p1
.end method

.method private a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 730
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 731
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Lo/u;)Lo/ad;

    move-result-object p2

    .line 734
    :cond_12
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 735
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;Landroid/location/Location;)Lo/ad;

    move-result-object p2

    .line 738
    :cond_20
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;)Lo/ad;

    move-result-object v1

    .line 739
    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    const/16 v5, 0x320

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;IIFI)V

    .line 741
    return-void
.end method

.method private k()V
    .registers 16

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/high16 v12, 0x4282

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    .line 157
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c

    const/high16 v4, 0x4184

    const/high16 v5, 0x4174

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x4186

    const/high16 v5, 0x4178

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c

    const/high16 v4, 0x4184

    const/high16 v5, 0x4174

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x418c

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4188

    const/high16 v4, 0x4192

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v14, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x418c

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c

    const/high16 v4, 0x4184

    const/high16 v5, 0x4174

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x4186

    const/high16 v5, 0x4178

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x417c

    const/high16 v4, 0x4184

    const/high16 v5, 0x4174

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x418c

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4188

    const/high16 v4, 0x4192

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v1, v13, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cW;

    const/high16 v3, 0x4186

    const/high16 v4, 0x418c

    const/high16 v5, 0x4182

    invoke-direct {v2, v3, v4, v5, v10}, Lcom/google/android/maps/driveabout/app/cW;-><init>(FFFZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const/4 v5, 0x0

    .line 196
    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_11d

    .line 197
    new-instance v5, Lq/e;

    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-direct {v5, v0}, Lq/e;-><init>(Lr/z;)V

    .line 209
    :cond_11d
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4186

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x418e

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x4198

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v10}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4188

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x4192

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x4198

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cU;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4192

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x4192

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41a0

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v13}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cU;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4194

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x4194

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x41a0

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v10, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4186

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x418e

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x4198

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v7, Lcom/google/android/maps/driveabout/app/cr;

    invoke-direct {v7, v11, v11, v14}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/bl;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v2, 0x4188

    invoke-direct {v1, v2, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v3, 0x4192

    const/high16 v4, 0x425c

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v4, 0x4178

    invoke-direct {v3, v4, v12}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    new-instance v4, Lcom/google/android/maps/driveabout/app/cV;

    const/high16 v8, 0x4198

    const/high16 v9, 0x4296

    invoke-direct {v4, v8, v9}, Lcom/google/android/maps/driveabout/app/cV;-><init>(FF)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/bl;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    return-void
.end method


# virtual methods
.method public a(F)F
    .registers 3
    .parameter

    .prologue
    .line 380
    const/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(FI)F

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/cq;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 295
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eq v1, v2, :cond_2f

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nMapWidth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_2f
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eq v1, v2, :cond_59

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nMapHeight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_59
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->d:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_85

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenDensity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_85
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->e:I

    if-eq v1, v2, :cond_af

    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nBottomMargin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_af
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->g:I

    if-eq v1, v2, :cond_d9

    .line 311
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nCameraType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/maps/driveabout/app/cq;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :cond_d9
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10f

    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nCameraPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    :cond_10f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lo/aQ;
    .registers 6

    .prologue
    .line 326
    new-instance v0, LC/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method a(Lo/ad;Lo/T;IIII)Lo/ad;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 810
    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {p2, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    .line 811
    invoke-virtual {v1}, Lo/T;->f()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    .line 812
    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 814
    int-to-float v2, p5

    cmpg-float v2, v3, v2

    if-gez v2, :cond_6b

    .line 815
    int-to-float v2, p5

    sub-float/2addr v2, v3

    .line 817
    :goto_2e
    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    sub-int v5, p3, p5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_69

    .line 818
    int-to-float v4, p3

    int-to-float v5, p5

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    int-to-float v5, v5

    sub-float v3, v5, v3

    sub-float v3, v4, v3

    .line 820
    :goto_43
    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v4, v4

    sub-float/2addr v4, v1

    int-to-float v5, p6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_67

    .line 821
    int-to-float v4, p6

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v5, v5

    sub-float/2addr v5, v1

    sub-float/2addr v4, v5

    .line 823
    :goto_52
    sub-int v5, p4, p6

    int-to-float v5, v5

    cmpg-float v5, v1, v5

    if-gez v5, :cond_65

    .line 824
    int-to-float v0, p4

    int-to-float v5, p6

    sub-float/2addr v0, v5

    sub-float v5, v0, v1

    :goto_5e
    move-object v0, p0

    move-object v1, p1

    .line 826
    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/ad;FFFF)Lo/ad;

    move-result-object v0

    return-object v0

    :cond_65
    move v5, v0

    goto :goto_5e

    :cond_67
    move v4, v0

    goto :goto_52

    :cond_69
    move v3, v0

    goto :goto_43

    :cond_6b
    move v2, v0

    goto :goto_2e
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 358
    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    .line 359
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/k;->a(I)V

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    .line 361
    return-void
.end method

.method public a(IIF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    .line 347
    iput p2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    .line 348
    iput p3, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    .line 349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    .line 350
    return-void
.end method

.method public a(ILcom/google/android/maps/driveabout/vector/q;I)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 674
    iput p1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    .line 676
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->i:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/maps/driveabout/app/cr;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_4f

    move v0, v1

    :goto_d
    invoke-direct {v4, p1, v0, p3}, Lcom/google/android/maps/driveabout/app/cr;-><init>(IZI)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/k;

    .line 678
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    if-eq v0, v3, :cond_1e

    .line 679
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    .line 680
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    .line 687
    :cond_1e
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    if-nez v0, :cond_4e

    .line 691
    :try_start_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v3, 0x7f0c0003

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_2a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_22 .. :try_end_2a} :catch_51

    move-result v0

    .line 700
    :goto_2b
    :try_start_2b
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->a:Landroid/content/res/Resources;

    const v4, 0x7f0c0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_33
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2b .. :try_end_33} :catch_60

    move-result v2

    .line 708
    :goto_34
    const/4 v3, 0x2

    if-ne p3, v3, :cond_6e

    :goto_37
    int-to-float v0, v0

    const/high16 v2, 0x42c8

    div-float/2addr v0, v2

    .line 710
    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 711
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/k;->b(I)V

    .line 712
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->e:I

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(I)V

    .line 714
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/cq;->f:Z

    .line 716
    :cond_4e
    return-void

    :cond_4f
    move v0, v2

    .line 676
    goto :goto_d

    .line 693
    :catch_51
    move-exception v0

    .line 694
    const-string v3, "Failed to load chevron walking margin resource."

    invoke-static {v3, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 695
    const-string v0, "NavigationMapController"

    const-string v3, "Failed to load da_mylocation_chevron_margin_walking_percent resource"

    invoke-static {v0, v3}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_2b

    .line 702
    :catch_60
    move-exception v3

    .line 703
    const-string v4, "Failed to load chevron driving margin resource."

    invoke-static {v4, v3}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 704
    const-string v3, "NavigationMapController"

    const-string v4, "Failed to load da_mylocation_chevron_margin_percent resource"

    invoke-static {v3, v4}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_34

    :cond_6e
    move v0, v2

    .line 708
    goto :goto_37
.end method

.method public a(LC/c;)V
    .registers 3
    .parameter

    .prologue
    .line 389
    const/16 v0, 0x320

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    .line 390
    return-void
.end method

.method public a(LO/N;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 656
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-nez v0, :cond_9

    .line 662
    :cond_8
    :goto_8
    return-void

    .line 659
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    .line 660
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {v1, v0, p1, p3}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LO/N;Z)LC/b;

    move-result-object v1

    .line 661
    if-eqz p2, :cond_1b

    const/16 v0, 0x320

    :goto_17
    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_8

    :cond_1b
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public a(LaH/h;)V
    .registers 8
    .parameter

    .prologue
    .line 591
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_a

    if-nez p1, :cond_b

    .line 598
    :cond_a
    :goto_a
    return-void

    .line 594
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    .line 595
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;IIF)LC/b;

    move-result-object v0

    .line 597
    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_a
.end method

.method public a(LaH/h;Lo/am;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 574
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_a

    if-nez p1, :cond_b

    .line 582
    :cond_a
    :goto_a
    return-void

    .line 577
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    .line 578
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;Lo/am;IIF)LC/b;

    move-result-object v0

    .line 581
    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto :goto_a
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 3
    .parameter

    .prologue
    .line 417
    const/16 v0, 0x320

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;I)V

    .line 418
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;FZ)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x1f4

    const/high16 v9, 0x4170

    .line 443
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_12

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_12

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-nez v0, :cond_13

    .line 496
    :cond_12
    :goto_12
    return-void

    .line 447
    :cond_13
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    .line 448
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    .line 449
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bT;->c()Lo/S;

    move-result-object v3

    .line 450
    if-eqz v3, :cond_12

    invoke-virtual {v3}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 453
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v4

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v7, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v8, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    move v5, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;

    move-result-object v0

    .line 457
    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_ad

    const/high16 v1, -0x4080

    cmpl-float v1, p2, v1

    if-nez v1, :cond_ad

    .line 458
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v1

    if-nez v1, :cond_d0

    .line 461
    invoke-virtual {v2}, LaH/h;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_d0

    .line 462
    invoke-virtual {v0}, LC/b;->a()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_ce

    .line 463
    new-instance v3, LC/b;

    invoke-virtual {v0}, LC/b;->c()Lo/T;

    move-result-object v4

    invoke-virtual {v0}, LC/b;->d()F

    move-result v6

    invoke-virtual {v0}, LC/b;->e()F

    move-result v7

    invoke-virtual {v0}, LC/b;->f()F

    move-result v8

    move v5, v9

    invoke-direct/range {v3 .. v8}, LC/b;-><init>(Lo/T;FFFF)V

    move-object v4, v3

    .line 467
    :goto_70
    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v0

    const v1, 0x3f8ccccd

    mul-float/2addr v0, v1

    invoke-virtual {v2}, LaH/h;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Lo/T;->a(D)D

    move-result-wide v5

    double-to-float v1, v5

    mul-float v5, v0, v1

    .line 471
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v6, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v7, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v8, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;FIIF)LC/b;

    move-result-object v0

    move-object v1, v0

    .line 479
    :goto_90
    const/4 v3, 0x0

    .line 480
    invoke-virtual {v2}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    .line 482
    if-eqz v0, :cond_cc

    .line 483
    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v2

    double-to-float v0, v2

    .line 485
    :goto_9e
    const/high16 v2, 0x3fc0

    mul-float/2addr v2, v0

    .line 486
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v5, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;FIIF)LC/b;

    move-result-object v0

    .line 489
    :cond_ad
    if-eqz p3, :cond_c0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_c0

    .line 490
    new-instance v1, Lcom/google/android/maps/driveabout/app/cs;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-direct {v1, v0, v2, p1}, Lcom/google/android/maps/driveabout/app/cs;-><init>(LC/b;Lcom/google/android/maps/driveabout/app/bT;Lcom/google/android/maps/driveabout/app/aQ;)V

    invoke-virtual {p0, v1, v10}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto/16 :goto_12

    .line 493
    :cond_c0
    new-instance v1, Lcom/google/android/maps/driveabout/app/ct;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/ct;-><init>(LC/b;Lcom/google/android/maps/driveabout/app/bT;)V

    invoke-virtual {p0, v1, v10}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    goto/16 :goto_12

    :cond_cc
    move v0, v3

    goto :goto_9e

    :cond_ce
    move-object v4, v0

    goto :goto_70

    :cond_d0
    move-object v1, v0

    goto :goto_90
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    .line 401
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_1c

    .line 402
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {v2}, LaH/h;->getBearing()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;F)LC/b;

    move-result-object v0

    .line 407
    :goto_18
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;I)V

    .line 408
    return-void

    .line 405
    :cond_1c
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->h:Lcom/google/android/maps/driveabout/app/k;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;F)LC/b;

    move-result-object v0

    goto :goto_18
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bT;)V
    .registers 2
    .parameter

    .prologue
    .line 919
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    .line 920
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 4
    .parameter

    .prologue
    .line 426
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    const/16 v1, 0x320

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lo/T;I)V

    .line 427
    return-void
.end method

.method public a_(LC/a;)I
    .registers 4
    .parameter

    .prologue
    .line 924
    const/4 v0, 0x0

    .line 925
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    if-eqz v1, :cond_b

    .line 926
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cq;->j:Lcom/google/android/maps/driveabout/app/bT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bT;->a()I

    move-result v0

    .line 928
    :cond_b
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a_(LC/a;)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public b()Lo/T;
    .registers 6

    .prologue
    .line 331
    new-instance v0, LC/a;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    iget v3, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/cq;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->h()Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 6
    .parameter

    .prologue
    .line 608
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_15

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_16

    .line 619
    :cond_15
    :goto_15
    return-void

    .line 613
    :cond_16
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    .line 614
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v1

    .line 615
    const/4 v0, 0x1

    :goto_26
    array-length v3, v2

    if-ge v0, v3, :cond_3a

    .line 616
    aget-object v3, v2, v0

    invoke-virtual {v3}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->a()Lo/ad;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/ad;->a(Lo/ad;)Lo/ad;

    move-result-object v1

    .line 615
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 618
    :cond_3a
    invoke-direct {p0, p1, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V

    goto :goto_15
.end method

.method public c()LC/b;
    .registers 2

    .prologue
    .line 933
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 3
    .parameter

    .prologue
    .line 630
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_f

    .line 643
    :cond_e
    :goto_e
    return-void

    .line 634
    :cond_f
    const/high16 v0, -0x4080

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a(F)Lo/am;

    move-result-object v0

    .line 637
    if-eqz v0, :cond_1f

    .line 638
    invoke-virtual {v0}, Lo/am;->f()Lo/ad;

    move-result-object v0

    .line 642
    :goto_1b
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;Lo/ad;)V

    goto :goto_e

    .line 640
    :cond_1f
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v0

    goto :goto_1b
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 275
    if-ne p0, p1, :cond_5

    .line 281
    :cond_4
    :goto_4
    return v0

    .line 278
    :cond_5
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/cq;

    if-eqz v2, :cond_17

    .line 279
    check-cast p1, Lcom/google/android/maps/driveabout/app/cq;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/cq;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_4

    :cond_17
    move v0, v1

    .line 281
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 262
    .line 264
    iget v0, p0, Lcom/google/android/maps/driveabout/app/cq;->g:I

    add-int/lit8 v0, v0, 0x1f

    .line 265
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->c:I

    add-int/2addr v0, v1

    .line 266
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cq;->b:I

    add-int/2addr v0, v1

    .line 267
    return v0
.end method
