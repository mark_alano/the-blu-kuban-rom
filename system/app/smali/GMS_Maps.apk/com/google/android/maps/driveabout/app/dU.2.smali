.class Lcom/google/android/maps/driveabout/app/dU;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/maps/driveabout/app/dM;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/dM;)V
    .registers 3
    .parameter

    .prologue
    .line 860
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dU;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 861
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dU;->a:Z

    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 902
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dU;->a:Z

    if-nez v0, :cond_11

    .line 903
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_12

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 904
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/dU;->a:Z

    .line 906
    :cond_11
    return-void

    .line 903
    :cond_12
    const/4 v0, 0x0

    goto :goto_c
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 865
    .line 867
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_4e

    move v1, v0

    .line 898
    :cond_a
    :goto_a
    return v1

    .line 869
    :pswitch_b
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/dU;->a:Z

    if-nez v2, :cond_1d

    .line 870
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dU;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Lcom/google/android/maps/driveabout/app/dM;)LQ/p;

    move-result-object v0

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    goto :goto_a

    .line 872
    :cond_1d
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dU;->a:Z

    goto :goto_a

    .line 877
    :pswitch_20
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v2

    if-nez v2, :cond_27

    move v0, v1

    :cond_27
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_a

    .line 881
    :pswitch_2b
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dU;->a(Landroid/view/View;)V

    goto :goto_a

    .line 885
    :pswitch_2f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 886
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 888
    if-ltz v0, :cond_49

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    if-ge v0, v3, :cond_49

    if-ltz v2, :cond_49

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lt v2, v0, :cond_a

    .line 890
    :cond_49
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dU;->a(Landroid/view/View;)V

    goto :goto_a

    .line 867
    nop

    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_20
        :pswitch_b
        :pswitch_2f
        :pswitch_2b
    .end packed-switch
.end method
