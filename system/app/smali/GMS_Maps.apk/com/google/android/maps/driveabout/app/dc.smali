.class public Lcom/google/android/maps/driveabout/app/dc;
.super LR/v;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/maps/driveabout/app/dc;


# instance fields
.field private c:Law/h;

.field private d:LO/t;

.field private e:Lcom/google/android/maps/driveabout/app/dB;

.field private f:Lcom/google/android/maps/driveabout/app/dB;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/maps/driveabout/app/dc;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dc;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 51
    invoke-direct {p0}, LR/v;-><init>()V

    return-void
.end method

.method private a(Lr/t;Lcom/google/android/maps/driveabout/app/dz;LO/t;)Lcom/google/android/maps/driveabout/app/dB;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/maps/driveabout/app/dB;

    invoke-direct {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/dB;-><init>(Lr/t;Lcom/google/android/maps/driveabout/app/dz;)V

    .line 130
    invoke-virtual {p3, v0}, LO/t;->a(LO/q;)V

    .line 133
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/maps/driveabout/app/dc;
    .registers 2
    .parameter

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/dc;->c(Landroid/content/Context;)V

    .line 47
    sget-object v0, Lcom/google/android/maps/driveabout/app/dc;->a:Lcom/google/android/maps/driveabout/app/dc;

    return-object v0
.end method

.method private static a(Lcom/google/android/maps/driveabout/app/dB;Lcom/google/android/maps/driveabout/app/dz;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/maps/driveabout/app/dd;

    const-string v1, "OfflineReroutingHelper"

    invoke-direct {v0, v1, p2, p1, p0}, Lcom/google/android/maps/driveabout/app/dd;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/maps/driveabout/app/dz;Lcom/google/android/maps/driveabout/app/dB;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dd;->start()V

    .line 114
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dB;->o_()V

    .line 81
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dB;

    if-eqz v0, :cond_e

    .line 82
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dB;->o_()V

    .line 84
    :cond_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-virtual {v0}, LO/t;->a()V

    .line 85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->c:Law/h;

    invoke-virtual {v0}, Law/h;->u()V

    .line 86
    return-void
.end method

.method protected b(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 58
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->c:Law/h;

    .line 61
    invoke-static {p1}, LO/t;->a(Landroid/content/Context;)LO/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    .line 63
    sget-object v0, LA/c;->b:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    .line 65
    new-instance v1, Lcom/google/android/maps/driveabout/app/dz;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/app/dz;-><init>()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dc;->a(Lr/t;Lcom/google/android/maps/driveabout/app/dz;LO/t;)Lcom/google/android/maps/driveabout/app/dB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dB;

    .line 67
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->e:Lcom/google/android/maps/driveabout/app/dB;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dB;->n_()V

    .line 69
    sget-object v0, LA/c;->i:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/w;

    .line 71
    new-instance v1, Lcom/google/android/maps/driveabout/app/dz;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/app/dz;-><init>()V

    .line 72
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dc;->d:LO/t;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dc;->a(Lr/t;Lcom/google/android/maps/driveabout/app/dz;LO/t;)Lcom/google/android/maps/driveabout/app/dB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dB;

    .line 74
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dc;->f:Lcom/google/android/maps/driveabout/app/dB;

    invoke-static {v0, v1, p1}, Lcom/google/android/maps/driveabout/app/dc;->a(Lcom/google/android/maps/driveabout/app/dB;Lcom/google/android/maps/driveabout/app/dz;Landroid/content/Context;)V

    .line 76
    return-void
.end method
