.class public Lcom/google/android/maps/driveabout/app/dp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 120
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/app/dp;->a:Z

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 143
    const-string v0, ""

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public static a(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 165
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 187
    sget-boolean v0, Lcom/google/android/maps/driveabout/app/dp;->a:Z

    if-eqz v0, :cond_9

    .line 188
    const/16 v0, 0x45

    invoke-static {v0, p0, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_9
    return-void
.end method

.method public static a(Ljava/lang/String;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 154
    if-eqz p1, :cond_8

    const-string v0, "t"

    :goto_4
    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void

    .line 154
    :cond_8
    const-string v0, "f"

    goto :goto_4
.end method

.method public static a(Z)V
    .registers 1
    .parameter

    .prologue
    .line 128
    sput-boolean p0, Lcom/google/android/maps/driveabout/app/dp;->a:Z

    .line 129
    return-void
.end method

.method public static b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 206
    sget-boolean v0, Lcom/google/android/maps/driveabout/app/dp;->a:Z

    if-eqz v0, :cond_b

    .line 207
    invoke-static {}, Lbm/m;->a()Lbm/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbm/m;->a(Z)V

    .line 209
    :cond_b
    return-void
.end method
