.class public Lcom/google/android/maps/driveabout/app/dx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static b:Lcom/google/android/maps/driveabout/app/dx;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/maps/driveabout/app/aK;

.field private final e:C

.field private final f:C

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/HashSet;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:LO/P;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 66
    const-string v0, "%1$s"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/app/dx;->a:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    .line 166
    new-instance v0, Lcom/google/android/maps/driveabout/app/aK;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->d:Lcom/google/android/maps/driveabout/app/aK;

    .line 168
    const v0, 0x7f0d00be

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dx;->b(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->h:Ljava/util/HashSet;

    .line 169
    const v0, 0x7f0d00bf

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dx;->b(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->g:Ljava/util/HashSet;

    .line 171
    const v0, 0x7f0d0026

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Lcom/google/android/maps/driveabout/app/dx;->e:C

    .line 174
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Lcom/google/android/maps/driveabout/app/dx;->f:C

    .line 175
    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1230
    const/16 v0, 0x20

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1231
    if-lez v0, :cond_19

    if-le v0, p2, :cond_19

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->h:Ljava/util/HashSet;

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1233
    add-int/lit8 v0, v0, 0x1

    .line 1235
    :goto_18
    return v0

    :cond_19
    const/4 v0, -0x1

    goto :goto_18
.end method

.method private a(IIIF)Landroid/text/Spannable;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v8, 0x7f09007a

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x21

    .line 208
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_be

    const/4 v0, 0x2

    .line 211
    :goto_c
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dx;->d:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v3, p1, p2, v1, v0}, Lcom/google/android/maps/driveabout/app/aK;->a(IIZI)Ljava/lang/String;

    move-result-object v3

    .line 214
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move v0, v2

    .line 220
    :goto_18
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_5b

    .line 221
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 222
    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_2c

    iget-char v6, p0, Lcom/google/android/maps/driveabout/app/dx;->e:C

    if-ne v5, v6, :cond_c1

    .line 223
    :cond_2c
    if-lez v0, :cond_5b

    .line 224
    and-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_3a

    .line 225
    new-instance v5, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 228
    :cond_3a
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const v6, 0x3f19999a

    invoke-direct {v5, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 230
    and-int/lit8 v5, p3, 0x2

    if-eqz v5, :cond_5b

    .line 231
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4, v5, v2, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 241
    :cond_5b
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    :goto_5f
    if-le v2, v0, :cond_b3

    .line 243
    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 244
    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_71

    iget-char v6, p0, Lcom/google/android/maps/driveabout/app/dx;->f:C

    if-ne v5, v6, :cond_c5

    .line 245
    :cond_71
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_b3

    .line 246
    and-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_87

    .line 247
    new-instance v5, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 251
    :cond_87
    const/high16 v5, 0x3f80

    cmpl-float v5, p4, v5

    if-eqz v5, :cond_99

    .line 252
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v5, p4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 256
    :cond_99
    and-int/lit8 v5, p3, 0x2

    if-eqz v5, :cond_b3

    .line 257
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4, v5, v2, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 269
    :cond_b3
    if-ge v0, v2, :cond_bd

    .line 270
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v3, v0, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 273
    :cond_bd
    return-object v4

    :cond_be
    move v0, v1

    .line 208
    goto/16 :goto_c

    .line 220
    :cond_c1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_18

    .line 242
    :cond_c5
    add-int/lit8 v2, v2, -0x1

    goto :goto_5f
.end method

.method private static a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x21

    .line 572
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 574
    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const v2, 0x3f99999a

    invoke-direct {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 576
    invoke-virtual {p0}, LO/P;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_50

    .line 577
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 581
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 582
    invoke-virtual {p0}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 583
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 584
    new-instance v3, Landroid/text/style/RelativeSizeSpan;

    const/high16 v4, 0x3f40

    invoke-direct {v3, v4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0, v3, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 586
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 589
    :cond_50
    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Landroid/text/Spannable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1101
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1103
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1106
    return-object v0
.end method

.method public static a(Landroid/content/Context;IIZ)Landroid/text/Spannable;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1119
    invoke-virtual {p0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/high16 v1, 0x3fc0

    invoke-static {p0, v0, p2, v1, p3}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1186
    const v0, 0x7f020152

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x21

    const/4 v4, 0x0

    .line 1144
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1145
    if-eqz p4, :cond_21

    .line 1146
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090081

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1150
    :cond_21
    if-eqz p2, :cond_39

    .line 1151
    const-string v1, "  "

    invoke-virtual {v0, v4, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1152
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1153
    new-instance v2, Lcom/google/android/maps/driveabout/app/dj;

    invoke-direct {v2, v1, p3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1156
    :cond_39
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/text/Spannable;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1130
    const/4 v0, 0x0

    const/high16 v1, 0x3fc0

    const/4 v2, 0x1

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v8, 0x21

    const/4 v7, 0x1

    const v6, 0x3f19999a

    .line 942
    const-string v0, "%1$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 943
    sget v0, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int v1, v3, v0

    .line 944
    const-string v0, "%2$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 945
    sget v0, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v0, v2

    .line 947
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 953
    if-ge v3, v2, :cond_5e

    move v9, v2

    move v2, v1

    move v1, v9

    .line 969
    :goto_23
    if-eqz p4, :cond_3d

    .line 971
    if-lez v3, :cond_2b

    .line 972
    const/4 v5, 0x0

    invoke-direct {p0, v4, v5, v3, v6}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 974
    :cond_2b
    if-ge v2, v1, :cond_30

    .line 975
    invoke-direct {p0, v4, v2, v1, v6}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 978
    :cond_30
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_3d

    .line 979
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {p0, v4, v0, v5, v6}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 985
    :cond_3d
    invoke-virtual {v4, v1, v0, p3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 986
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v4, v0, v1, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 989
    invoke-virtual {v4, v3, v2, p2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 990
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v4, v0, v3, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 993
    return-object v4

    :cond_5e
    move-object v9, p2

    move-object p2, p3

    move-object p3, v9

    move v10, v1

    move v1, v3

    move v3, v2

    move v2, v0

    move v0, v10

    .line 966
    goto :goto_23
.end method

.method public static a()Lcom/google/android/maps/driveabout/app/dx;
    .registers 1

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/maps/driveabout/app/dx;->b:Lcom/google/android/maps/driveabout/app/dx;

    return-object v0
.end method

.method public static a(Landroid/content/Context;LO/U;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1019
    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    .line 1020
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1027
    :goto_a
    return-object v0

    .line 1023
    :cond_b
    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v0

    .line 1024
    if-eqz v0, :cond_1d

    invoke-virtual {v0}, LO/V;->a()I

    move-result v1

    if-lez v1, :cond_1d

    .line 1025
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    .line 1027
    :cond_1d
    const v0, 0x7f0d009c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Landroid/content/Context;LO/U;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1055
    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v2

    .line 1056
    if-eqz v2, :cond_d

    invoke-virtual {v2}, LO/V;->a()I

    move-result v0

    if-nez v0, :cond_f

    :cond_d
    move-object v0, v1

    .line 1085
    :cond_e
    :goto_e
    return-object v0

    .line 1061
    :cond_f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1062
    const/4 v0, 0x0

    :goto_15
    invoke-virtual {v2}, LO/V;->a()I

    move-result v4

    if-ge v0, v4, :cond_2a

    .line 1063
    if-lez v0, :cond_20

    .line 1064
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1066
    :cond_20
    invoke-virtual {v2, v0}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1062
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 1068
    :cond_2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1074
    invoke-static {p0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v2

    .line 1075
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1076
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v3, v4, :cond_44

    move-object v0, v1

    .line 1077
    goto :goto_e

    .line 1079
    :cond_44
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1080
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    .line 1081
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 9
    .parameter

    .prologue
    .line 693
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_8

    .line 731
    :goto_7
    return-object p0

    .line 698
    :cond_8
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 699
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_11
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    .line 700
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 701
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 705
    :cond_31
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 706
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 707
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 708
    :cond_3f
    :goto_3f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_86

    .line 709
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    .line 710
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    .line 711
    if-eqz v1, :cond_76

    .line 712
    invoke-virtual {v0}, LO/P;->e()Ljava/lang/String;

    move-result-object v6

    .line 713
    if-nez v6, :cond_5d

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3f

    .line 718
    :cond_5d
    if-eqz v6, :cond_70

    .line 719
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 721
    :cond_70
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 726
    :cond_76
    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    .line 727
    if-eqz v1, :cond_3f

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 728
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3f

    :cond_86
    move-object p0, v2

    .line 731
    goto :goto_7
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/maps/driveabout/app/dx;->b:Lcom/google/android/maps/driveabout/app/dx;

    if-nez v0, :cond_b

    .line 150
    new-instance v0, Lcom/google/android/maps/driveabout/app/dx;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dx;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dx;->b:Lcom/google/android/maps/driveabout/app/dx;

    .line 152
    :cond_b
    return-void
.end method

.method private a(Landroid/text/Spannable;IIF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x21

    .line 471
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v0, p4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-interface {p1, v0, p2, p3, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 473
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p1, v0, p2, p3, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 476
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 510
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, LO/P;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    move v1, v0

    .line 511
    :goto_c
    array-length v0, v2

    if-ge v1, v0, :cond_2d

    .line 512
    aget-object v0, v2, v1

    check-cast v0, LO/P;

    .line 514
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 516
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 511
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 518
    :cond_2d
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 424
    if-eqz p3, :cond_8

    invoke-virtual {p3}, LO/P;->f()Z

    move-result v0

    if-nez v0, :cond_23

    .line 425
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-static {v0, p2}, LO/m;->a(Landroid/content/Context;LO/N;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_23

    .line 427
    const-string v1, "  "

    invoke-virtual {p1, p4, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 428
    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v2, 0x3f80

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v0, p4, 0x1

    const/16 v2, 0x21

    invoke-virtual {p1, v1, p4, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 433
    :cond_23
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;Lv/c;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 480
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, LO/P;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    move v1, v0

    .line 481
    :goto_c
    array-length v0, v3

    if-ge v1, v0, :cond_67

    .line 482
    aget-object v0, v3, v1

    check-cast v0, LO/P;

    .line 484
    const/4 v2, 0x0

    .line 485
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3b

    .line 487
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v4

    .line 488
    if-eqz v4, :cond_3b

    .line 489
    invoke-virtual {v0}, LO/P;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p2}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v4

    .line 491
    if-eqz v4, :cond_3b

    invoke-virtual {v4}, Lv/a;->b()Z

    move-result v5

    if-eqz v5, :cond_3b

    invoke-virtual {v4}, Lv/a;->c()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3b

    .line 493
    invoke-virtual {v4}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 497
    :cond_3b
    if-eqz v2, :cond_53

    .line 498
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {p1, v4, v5, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 505
    :goto_4c
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 481
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 501
    :cond_53
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {p1, v2, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4c

    .line 507
    :cond_67
    return-void
.end method

.method private b(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 1212
    const/4 v0, 0x0

    .line 1214
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/String;I)I

    move-result v1

    .line 1215
    if-gez v1, :cond_8

    .line 1220
    return v0

    :cond_8
    move v0, v1

    .line 1219
    goto :goto_1
.end method

.method private b(Ljava/lang/String;I)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1261
    const/16 v0, 0x20

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    .line 1262
    if-lez v0, :cond_1d

    add-int/lit8 v1, p2, -0x1

    if-ge v0, v1, :cond_1d

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->g:Ljava/util/HashSet;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 1266
    :goto_1c
    return v0

    :cond_1d
    const/4 v0, -0x1

    goto :goto_1c
.end method

.method public static b(Landroid/content/Context;)Landroid/text/Spannable;
    .registers 7
    .parameter

    .prologue
    const/16 v5, 0x21

    .line 1163
    const v0, 0x7f0d00e8

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1164
    new-instance v1, Landroid/text/SpannableString;

    const v2, 0x7f0d00d6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1166
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1169
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1170
    const-string v3, "%2$s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1171
    sget v3, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v3, v0

    invoke-virtual {v2, v0, v3, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1173
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1175
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "%1$s"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1176
    sget v3, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v3, v1

    const-string v4, " "

    invoke-virtual {v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1177
    new-instance v3, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v4, 0x3f80

    invoke-direct {v3, v0, v4}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1179
    return-object v2
.end method

.method public static b(Landroid/content/Context;LO/U;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1040
    const v0, 0x7f0d000a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Ljava/util/HashSet;
    .registers 7
    .parameter

    .prologue
    .line 1270
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1271
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1272
    array-length v3, v1

    const/4 v0, 0x0

    :goto_13
    if-ge v0, v3, :cond_21

    aget-object v4, v1, v0

    .line 1273
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1272
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1275
    :cond_21
    return-object v2
.end method

.method private b(LO/P;)Z
    .registers 3
    .parameter

    .prologue
    .line 628
    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, LO/P;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private c(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 1243
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1245
    :goto_4
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/dx;->b(Ljava/lang/String;I)I

    move-result v1

    .line 1246
    if-gez v1, :cond_b

    .line 1251
    return v0

    :cond_b
    move v0, v1

    .line 1250
    goto :goto_4
.end method


# virtual methods
.method a(LO/P;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 640
    monitor-enter p0

    .line 645
    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->j:LO/P;

    if-ne v0, p1, :cond_e

    .line 646
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->i:Landroid/graphics/drawable/Drawable;

    monitor-exit p0

    .line 688
    :goto_d
    return-object v0

    .line 648
    :cond_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_7c

    .line 651
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 653
    const v1, 0x7f040038

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 657
    invoke-virtual {p1}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    invoke-virtual {p1}, LO/P;->g()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7f

    .line 661
    const v1, 0x7f020104

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 669
    :goto_3b
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 672
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 675
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 678
    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 679
    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 680
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 682
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 683
    monitor-enter p0

    .line 685
    :try_start_73
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->i:Landroid/graphics/drawable/Drawable;

    .line 686
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dx;->j:LO/P;

    .line 687
    monitor-exit p0

    goto :goto_d

    :catchall_79
    move-exception v0

    monitor-exit p0
    :try_end_7b
    .catchall {:try_start_73 .. :try_end_7b} :catchall_79

    throw v0

    .line 648
    :catchall_7c
    move-exception v0

    :try_start_7d
    monitor-exit p0
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_7c

    throw v0

    .line 662
    :cond_7f
    invoke-virtual {p1}, LO/P;->g()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_91

    .line 663
    const v1, 0x7f020105

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_3b

    .line 665
    :cond_91
    const v1, 0x7f020106

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_3b
.end method

.method public a(II)Landroid/text/Spannable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v1, 0x7f0d000c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 287
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 288
    const/4 v2, 0x0

    const/high16 v3, 0x3f80

    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/maps/driveabout/app/dx;->a(IIIF)Landroid/text/Spannable;

    move-result-object v2

    .line 290
    const-string v3, "%1$s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 293
    sget v3, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v3, v0

    invoke-virtual {v1, v0, v3, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 295
    return-object v1
.end method

.method public a(III)Landroid/text/Spannable;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 1001
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1002
    new-instance v1, Landroid/text/SpannableString;

    const-string v2, " "

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1003
    new-instance v2, Landroid/text/style/ScaleXSpan;

    const/high16 v3, 0x4080

    invoke-direct {v2, v3}, Landroid/text/style/ScaleXSpan;-><init>(F)V

    const/4 v3, 0x1

    const/16 v4, 0x21

    invoke-interface {v1, v2, v5, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1005
    invoke-virtual {p0, p1, p3, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(IIZ)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1006
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1007
    invoke-virtual {p0, p2, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(IZ)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1009
    return-object v0
.end method

.method public a(IIZ)Landroid/text/Spannable;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    if-eqz p3, :cond_b

    const/4 v0, 0x1

    :goto_3
    const v1, 0x3f19999a

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(IIIF)Landroid/text/Spannable;

    move-result-object v0

    return-object v0

    :cond_b
    const/4 v0, 0x2

    goto :goto_3
.end method

.method public a(ILO/N;I)Landroid/text/Spannable;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 548
    if-gtz p1, :cond_8

    .line 549
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/maps/driveabout/app/dx;->b(LO/N;Lv/c;)Landroid/text/Spannable;

    move-result-object v0

    .line 559
    :goto_7
    return-object v0

    .line 551
    :cond_8
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v2, 0x7f0d002e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 553
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 554
    const-string v2, "%1$s"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/4 v3, 0x1

    invoke-virtual {p0, p1, p3, v3}, Lcom/google/android/maps/driveabout/app/dx;->a(IIZ)Landroid/text/Spannable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 556
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "%2$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 557
    const-string v2, "%2$s"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p2}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 558
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/SpannableStringBuilder;)V

    goto :goto_7
.end method

.method public a(IZ)Landroid/text/Spannable;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const v7, 0x3f19999a

    const/4 v1, 0x1

    .line 899
    new-instance v0, Lcom/google/android/maps/driveabout/app/dy;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/dy;-><init>(Lcom/google/android/maps/driveabout/app/dx;I)V

    .line 900
    iget v3, v0, Lcom/google/android/maps/driveabout/app/dy;->a:I

    .line 901
    iget v4, v0, Lcom/google/android/maps/driveabout/app/dy;->b:I

    .line 902
    iget v5, v0, Lcom/google/android/maps/driveabout/app/dy;->c:I

    .line 904
    if-lez v3, :cond_28

    .line 905
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v2, 0x7f0d0028

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 906
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;

    move-result-object v0

    .line 936
    :goto_27
    return-object v0

    .line 907
    :cond_28
    if-gtz v4, :cond_2c

    if-eqz p2, :cond_65

    .line 908
    :cond_2c
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    if-eqz p2, :cond_5f

    const v0, 0x7f0d002d

    :goto_33
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 912
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 913
    if-eqz p2, :cond_54

    const/16 v6, 0xa

    if-ge v5, v6, :cond_54

    .line 914
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x30

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 916
    :cond_54
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    if-nez p2, :cond_63

    :goto_5a
    invoke-direct {p0, v3, v4, v0, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_27

    .line 908
    :cond_5f
    const v0, 0x7f0d002a

    goto :goto_33

    :cond_63
    move v1, v2

    .line 916
    goto :goto_5a

    .line 919
    :cond_65
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v3, 0x7f0d002c

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 920
    const-string v0, "%1$s"

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 921
    sget v0, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int v6, v4, v0

    .line 923
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 924
    if-lez v4, :cond_82

    .line 925
    invoke-direct {p0, v0, v2, v4, v7}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 927
    :cond_82
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v6, v2, :cond_8f

    .line 928
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v0, v6, v2, v7}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 932
    :cond_8f
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 933
    invoke-virtual {v0, v4, v6, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 934
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v4

    const/16 v2, 0x21

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_27
.end method

.method public a(LO/N;IILv/c;)Landroid/text/Spannable;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 348
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 350
    div-float/2addr v3, v4

    const v4, 0x3f19999a

    mul-float v5, v3, v4

    .line 353
    const/4 v3, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(IIIF)Landroid/text/Spannable;

    move-result-object v6

    .line 357
    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v3

    if-eqz v3, :cond_112

    const/4 v3, 0x1

    .line 358
    :goto_36
    const/4 v4, 0x0

    .line 359
    invoke-virtual/range {p1 .. p1}, LO/N;->y()LO/P;

    move-result-object v7

    if-eqz v7, :cond_115

    if-eqz v3, :cond_49

    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v7

    invoke-virtual {v7}, LO/P;->d()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_115

    .line 361
    :cond_49
    invoke-virtual/range {p1 .. p1}, LO/N;->y()LO/P;

    move-result-object v3

    .line 367
    :goto_4d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v7, 0x7f0d001b

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 369
    const-string v7, "%1$s"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 370
    const-string v8, "%2$s"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 371
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 374
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 375
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 376
    sget v12, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v12, v10

    .line 377
    sget v13, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v13, v11

    .line 378
    if-lez v10, :cond_a2

    .line 379
    new-instance v14, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v14, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/4 v15, 0x0

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v9, v14, v15, v10, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 381
    new-instance v14, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09007a

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    invoke-direct {v14, v15}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v15, 0x0

    const/16 v16, 0x21

    move/from16 v0, v16

    invoke-virtual {v9, v14, v15, v10, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 385
    :cond_a2
    if-le v11, v12, :cond_c7

    .line 386
    new-instance v10, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v10, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/16 v14, 0x21

    invoke-virtual {v9, v10, v12, v11, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 388
    new-instance v10, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09007a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-direct {v10, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v14, 0x21

    invoke-virtual {v9, v10, v12, v11, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 393
    :cond_c7
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v13, v10, :cond_f8

    .line 394
    new-instance v10, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v10, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v11, 0x21

    invoke-virtual {v9, v10, v13, v5, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 397
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09007a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v5, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v10, 0x21

    invoke-virtual {v9, v5, v13, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 403
    :cond_f8
    if-nez v3, :cond_11d

    const-string v4, ""

    .line 406
    :goto_fc
    if-le v8, v7, :cond_126

    .line 407
    sget v5, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v5, v8

    invoke-virtual {v9, v8, v5, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 409
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v3, v8}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V

    .line 410
    sget v3, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v3, v7

    invoke-virtual {v9, v7, v3, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 419
    :goto_111
    return-object v9

    .line 357
    :cond_112
    const/4 v3, 0x0

    goto/16 :goto_36

    .line 362
    :cond_115
    if-eqz v3, :cond_13a

    .line 363
    invoke-virtual/range {p1 .. p1}, LO/N;->x()LO/P;

    move-result-object v3

    goto/16 :goto_4d

    .line 403
    :cond_11d
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v4

    goto :goto_fc

    .line 413
    :cond_126
    sget v5, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v5, v7

    invoke-virtual {v9, v7, v5, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 415
    sget v5, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v5, v8

    invoke-virtual {v9, v8, v5, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 417
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v3, v8}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/SpannableStringBuilder;LO/N;LO/P;I)V

    goto :goto_111

    :cond_13a
    move-object v3, v4

    goto/16 :goto_4d
.end method

.method public a(LO/N;Lv/c;)Landroid/text/Spannable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 312
    invoke-virtual {p1}, LO/N;->x()LO/P;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v2, 0x7f0d0019

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 319
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 320
    sget v1, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v2, v1, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 322
    return-object v3
.end method

.method a(LO/P;Lv/c;)Landroid/text/Spannable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 603
    const/4 v0, 0x0

    .line 604
    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2f

    .line 606
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v1

    .line 607
    if-eqz v1, :cond_28

    .line 608
    invoke-virtual {p1}, LO/P;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v1

    .line 610
    if-eqz v1, :cond_28

    invoke-virtual {v1}, Lv/a;->b()Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-virtual {v1}, Lv/a;->c()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_28

    .line 612
    invoke-virtual {v1}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 619
    :cond_28
    :goto_28
    if-eqz v0, :cond_3a

    .line 620
    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    .line 622
    :goto_2e
    return-object v0

    .line 615
    :cond_2f
    invoke-virtual {p1}, LO/P;->f()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 617
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_28

    .line 622
    :cond_3a
    invoke-virtual {p1}, LO/P;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_2e
.end method

.method public a(LO/Q;)Landroid/text/Spannable;
    .registers 7
    .parameter

    .prologue
    .line 1197
    invoke-virtual {p1}, LO/Q;->b()Ljava/lang/String;

    move-result-object v1

    .line 1198
    invoke-virtual {p1}, LO/Q;->a()I

    move-result v0

    .line 1199
    const/4 v2, 0x2

    if-eq v0, v2, :cond_15

    const/4 v2, 0x3

    if-eq v0, v2, :cond_15

    const/16 v2, 0x9

    if-eq v0, v2, :cond_15

    const/4 v2, 0x4

    if-ne v0, v2, :cond_20

    .line 1203
    :cond_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x3f80

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;Ljava/lang/CharSequence;IFZ)Landroid/text/Spannable;

    move-result-object v0

    .line 1205
    :goto_1f
    return-object v0

    :cond_20
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1f
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .registers 3
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Landroid/text/Spannable;
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    const v4, 0x3f19999a

    .line 443
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 444
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dx;->b(Ljava/lang/String;)I

    move-result v1

    .line 445
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dx;->c(Ljava/lang/String;)I

    move-result v0

    .line 446
    if-gt v0, v1, :cond_18

    .line 450
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    move v1, v2

    .line 454
    :cond_18
    if-lez v1, :cond_1d

    .line 455
    invoke-direct {p0, v3, v2, v1, v4}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 459
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2a

    .line 460
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v3, v0, v2, v4}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 464
    :cond_2a
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-interface {v3, v2, v1, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 466
    return-object v3
.end method

.method public a(J)Ljava/lang/CharSequence;
    .registers 14
    .parameter

    .prologue
    const v10, 0xef01

    const v9, 0xef00

    const/16 v8, 0x21

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 1288
    instance-of v1, v0, Ljava/text/SimpleDateFormat;

    if-eqz v1, :cond_51

    .line 1289
    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 1298
    :goto_1a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8a

    .line 1304
    const/4 v5, -0x1

    move v1, v2

    move v3, v2

    .line 1306
    :goto_25
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_e4

    .line 1307
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1309
    const/16 v7, 0x27

    if-ne v6, v7, :cond_36

    .line 1310
    if-nez v3, :cond_54

    move v3, v4

    .line 1312
    :cond_36
    :goto_36
    if-nez v3, :cond_56

    const/16 v7, 0x61

    if-ne v6, v7, :cond_56

    move v3, v1

    .line 1319
    :goto_3d
    if-ltz v3, :cond_8a

    move v1, v3

    .line 1323
    :goto_40
    if-lez v1, :cond_59

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-eqz v5, :cond_59

    .line 1324
    add-int/lit8 v1, v1, -0x1

    goto :goto_40

    .line 1294
    :cond_51
    const-string v0, "HH:mm"

    goto :goto_1a

    :cond_54
    move v3, v2

    .line 1310
    goto :goto_36

    .line 1306
    :cond_56
    add-int/lit8 v1, v1, 0x1

    goto :goto_25

    .line 1326
    :cond_59
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "a"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1331
    :cond_8a
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1332
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1333
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1335
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 1336
    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1337
    if-ltz v3, :cond_d7

    if-le v0, v3, :cond_d7

    .line 1338
    const v5, 0x3f19999a

    invoke-direct {p0, v1, v3, v0, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 1339
    if-lez v3, :cond_b8

    .line 1340
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v5, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1344
    :cond_b8
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_cc

    .line 1345
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v2, v0, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1349
    :cond_cc
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 1350
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v1, v3, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 1357
    :goto_d6
    return-object v1

    .line 1352
    :cond_d7
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_d6

    :cond_e4
    move v3, v5

    goto/16 :goto_3d
.end method

.method public a(Ljava/util/Collection;IILandroid/text/TextPaint;ILv/c;)Ljava/lang/CharSequence;
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 751
    const v1, 0x7f0d0007

    move/from16 v0, p5

    if-eq v0, v1, :cond_e

    const v1, 0x7f0d0008

    move/from16 v0, p5

    if-ne v0, v1, :cond_bd

    .line 752
    :cond_e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 757
    :goto_16
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 761
    const-string v2, "%1$s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 762
    sget v3, Lcom/google/android/maps/driveabout/app/dx;->a:I

    add-int/2addr v3, v2

    .line 764
    if-lez v2, :cond_2d

    .line 765
    const/4 v4, 0x0

    const v5, 0x3f19999a

    invoke-direct {p0, v9, v4, v2, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 767
    :cond_2d
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3d

    .line 768
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const v4, 0x3f19999a

    invoke-direct {p0, v9, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/Spannable;IIF)V

    .line 771
    :cond_3d
    const-string v1, ""

    invoke-virtual {v9, v2, v3, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 775
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v3, 0x7f0d0009

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 777
    const-string v6, " "

    .line 779
    const v1, 0x7f0d0009

    move/from16 v0, p5

    if-ne v0, v1, :cond_14f

    .line 780
    const/4 v1, 0x0

    invoke-virtual {v9, v1, v7}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 781
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v9, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 785
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v2

    .line 788
    :goto_79
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 789
    const/4 v4, 0x0

    .line 790
    const/4 v3, 0x0

    .line 793
    const/4 v2, 0x0

    move v5, v4

    move v4, v3

    move v3, v1

    .line 794
    :goto_83
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_147

    move/from16 v0, p2

    if-ge v5, v0, :cond_147

    .line 795
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LO/P;

    .line 796
    move-object/from16 v0, p6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/P;Lv/c;)Landroid/text/Spannable;

    move-result-object v8

    .line 798
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v11

    move/from16 v0, p2

    if-gt v11, v0, :cond_c1

    .line 800
    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 801
    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v2

    add-int/2addr v2, v3

    .line 802
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14b

    .line 803
    const-string v3, "\n"

    invoke-virtual {v9, v2, v3}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 804
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    move v4, v5

    :goto_b8
    move v5, v4

    move v4, v3

    move v3, v2

    move-object v2, v1

    .line 850
    goto :goto_83

    .line 754
    :cond_bd
    const-string v1, "%1$s"

    goto/16 :goto_16

    .line 806
    :cond_c1
    if-nez v2, :cond_ce

    .line 807
    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 808
    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v2

    add-int/2addr v2, v3

    move v3, v4

    move v4, v5

    goto :goto_b8

    .line 815
    :cond_ce
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/dx;->b(LO/P;)Z

    move-result v11

    if-eqz v11, :cond_da

    invoke-virtual {v2}, LO/P;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_149

    :cond_da
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/app/dx;->b(LO/P;)Z

    move-result v2

    if-nez v2, :cond_149

    move-object v2, v7

    .line 822
    :goto_e1
    invoke-virtual {v9, v3, v2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 823
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v11

    add-int/2addr v11, v3

    .line 824
    invoke-virtual {v9, v11, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 825
    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v8

    add-int/2addr v8, v11

    .line 827
    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    move-object/from16 v0, p4

    invoke-static {v9, v4, v11, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    move-result v11

    move/from16 v0, p3

    int-to-float v12, v0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_124

    .line 831
    add-int/lit8 v4, p2, -0x1

    if-ge v5, v4, :cond_11f

    .line 832
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v4, v3

    const-string v11, "\n"

    invoke-virtual {v9, v3, v4, v11}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 834
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v8, v2

    .line 835
    add-int/lit8 v5, v5, 0x1

    .line 836
    add-int/lit8 v4, v3, 0x1

    move v3, v4

    move v4, v5

    goto :goto_b8

    .line 838
    :cond_11f
    invoke-virtual {v9, v3, v8}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-object v1, v9

    .line 851
    :goto_123
    return-object v1

    .line 841
    :cond_124
    if-eq v2, v6, :cond_142

    .line 842
    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f09007a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-direct {v11, v12}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v3

    const/16 v12, 0x21

    invoke-virtual {v9, v11, v3, v2, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_142
    move v2, v8

    move v3, v4

    move v4, v5

    goto/16 :goto_b8

    :cond_147
    move-object v1, v9

    .line 851
    goto :goto_123

    :cond_149
    move-object v2, v6

    goto :goto_e1

    :cond_14b
    move v3, v4

    move v4, v5

    goto/16 :goto_b8

    :cond_14f
    move v1, v2

    goto/16 :goto_79
.end method

.method public a(I)Ljava/lang/String;
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 862
    new-instance v3, Lcom/google/android/maps/driveabout/app/dy;

    invoke-direct {v3, p0, p1}, Lcom/google/android/maps/driveabout/app/dy;-><init>(Lcom/google/android/maps/driveabout/app/dx;I)V

    .line 865
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e

    iget v2, v3, Lcom/google/android/maps/driveabout/app/dy;->a:I

    new-array v4, v9, [Ljava/lang/Object;

    iget v5, v3, Lcom/google/android/maps/driveabout/app/dy;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 867
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0001

    iget v4, v3, Lcom/google/android/maps/driveabout/app/dy;->b:I

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, v3, Lcom/google/android/maps/driveabout/app/dy;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 869
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0e0002

    iget v5, v3, Lcom/google/android/maps/driveabout/app/dy;->c:I

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, v3, Lcom/google/android/maps/driveabout/app/dy;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 872
    iget v4, v3, Lcom/google/android/maps/driveabout/app/dy;->a:I

    if-lez v4, :cond_6b

    .line 873
    iget v2, v3, Lcom/google/android/maps/driveabout/app/dy;->b:I

    if-nez v2, :cond_5b

    .line 885
    :goto_5a
    return-object v0

    .line 876
    :cond_5b
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v3, 0x7f0d0029

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5a

    .line 878
    :cond_6b
    iget v0, v3, Lcom/google/android/maps/driveabout/app/dy;->b:I

    if-lez v0, :cond_85

    .line 879
    iget v0, v3, Lcom/google/android/maps/driveabout/app/dy;->c:I

    if-nez v0, :cond_75

    move-object v0, v1

    .line 880
    goto :goto_5a

    .line 882
    :cond_75
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    const v3, 0x7f0d002b

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v8

    aput-object v2, v4, v9

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5a

    :cond_85
    move-object v0, v2

    .line 885
    goto :goto_5a
.end method

.method public a(LO/U;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dx;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;LO/U;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(LO/N;Lv/c;)Landroid/text/Spannable;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 530
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 532
    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/text/SpannableStringBuilder;Lv/c;)V

    .line 533
    return-object v0
.end method
