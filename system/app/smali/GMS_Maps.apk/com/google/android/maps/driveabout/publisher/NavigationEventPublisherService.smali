.class public final Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:LO/q;

.field private b:Ljava/util/List;

.field private volatile c:Landroid/os/Bundle;

.field private volatile d:Landroid/os/Bundle;

.field private final e:Landroid/os/Messenger;

.field private final f:Landroid/os/IBinder;

.field private final g:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 50
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/maps/driveabout/publisher/f;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/publisher/f;-><init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->e:Landroid/os/Messenger;

    .line 51
    new-instance v0, Lcom/google/android/maps/driveabout/publisher/d;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/publisher/d;-><init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->f:Landroid/os/IBinder;

    .line 52
    new-instance v0, Lcom/google/android/maps/driveabout/publisher/c;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/publisher/c;-><init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->g:Landroid/os/IBinder;

    .line 280
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->c:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Landroid/os/Messenger;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->e:Landroid/os/Messenger;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b:Ljava/util/List;

    monitor-enter v1

    .line 176
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_1e

    move-result v0

    if-eqz v0, :cond_21

    .line 178
    :try_start_f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    invoke-direct {p0, v0, p1}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Landroid/os/Messenger;I)V
    :try_end_18
    .catchall {:try_start_f .. :try_end_18} :catchall_1e
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_18} :catch_19

    goto :goto_9

    .line 179
    :catch_19
    move-exception v0

    .line 183
    :try_start_1a
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_9

    .line 186
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1a .. :try_end_20} :catchall_1e

    throw v0

    :cond_21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_1e

    .line 187
    return-void
.end method

.method private a(Landroid/os/Messenger;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 190
    if-nez p1, :cond_3

    .line 207
    :goto_2
    return-void

    .line 194
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 195
    packed-switch p2, :pswitch_data_1c

    .line 206
    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto :goto_2

    .line 197
    :pswitch_f
    iget-object v1, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    goto :goto_b

    .line 200
    :pswitch_15
    iget-object v1, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    goto :goto_b

    .line 195
    nop

    :pswitch_data_1c
    .packed-switch 0x3
        :pswitch_f
        :pswitch_15
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;Landroid/os/Messenger;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Landroid/os/Messenger;I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->d:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()LO/q;
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a:LO/q;

    return-object v0
.end method

.method a(LO/s;)Landroid/os/Bundle;
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 211
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 212
    invoke-virtual {p1}, LO/s;->b()I

    move-result v1

    .line 213
    const-string v2, "METERS_TO_NEXT_STEP"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v2, "METERS_TO_DEST"

    invoke-virtual {p1}, LO/s;->d()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    const-string v2, "ESTIMATED_SECONDS_TO_DEST"

    invoke-virtual {p1}, LO/s;->c()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    const-string v2, "TRAFFIC_STATUS"

    invoke-virtual {p1}, LO/s;->k()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 217
    const-string v2, "IS_ON_ROUTE"

    invoke-virtual {p1}, LO/s;->l()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 218
    const-string v2, "METERS_FROM_PREV_STEP"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 219
    const-string v2, "NEXT_STEP_INDEX"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 220
    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    if-eqz v2, :cond_61

    .line 221
    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-virtual {v2}, LO/N;->e()I

    move-result v2

    .line 222
    sub-int v1, v2, v1

    .line 223
    if-ltz v1, :cond_54

    .line 224
    const-string v2, "METERS_FROM_PREV_STEP"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 233
    :cond_54
    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->i()I

    move-result v1

    .line 234
    const-string v2, "NEXT_STEP_INDEX"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 237
    :cond_61
    return-object v0
.end method

.method b(LO/s;)Landroid/os/Bundle;
    .registers 17
    .parameter

    .prologue
    .line 242
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 243
    invoke-virtual/range {p1 .. p1}, LO/s;->g()LO/z;

    move-result-object v2

    .line 248
    invoke-virtual {v2}, LO/z;->k()I

    move-result v3

    .line 249
    new-array v4, v3, [I

    .line 250
    new-array v5, v3, [I

    .line 251
    new-array v6, v3, [I

    .line 252
    new-array v7, v3, [I

    .line 253
    new-array v8, v3, [D

    .line 254
    new-array v9, v3, [D

    .line 255
    new-array v10, v3, [Ljava/lang/String;

    .line 257
    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v3, :cond_57

    .line 258
    invoke-virtual {v2, v0}, LO/z;->a(I)LO/N;

    move-result-object v11

    .line 259
    invoke-virtual {v11}, LO/N;->i()I

    move-result v12

    aput v12, v4, v0

    .line 260
    invoke-virtual {v11}, LO/N;->b()I

    move-result v12

    aput v12, v5, v0

    .line 261
    invoke-virtual {v11}, LO/N;->c()I

    move-result v12

    aput v12, v6, v0

    .line 262
    invoke-virtual {v11}, LO/N;->e()I

    move-result v12

    aput v12, v7, v0

    .line 263
    invoke-virtual {v11}, LO/N;->a()Lo/T;

    move-result-object v12

    .line 264
    invoke-virtual {v12}, Lo/T;->b()D

    move-result-wide v13

    aput-wide v13, v8, v0

    .line 265
    invoke-virtual {v12}, Lo/T;->d()D

    move-result-wide v12

    aput-wide v12, v9, v0

    .line 266
    invoke-virtual {v11}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v0

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 269
    :cond_57
    const-string v0, "STEP_NUMBERS"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 270
    const-string v0, "STEP_TYPES"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 271
    const-string v0, "STEP_TURN_SIDES"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 272
    const-string v0, "STEP_METERS_FROM_PREVIOUS_STEP"

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 273
    const-string v0, "STEP_LATITUDES"

    invoke-virtual {v1, v0, v8}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 274
    const-string v0, "STEP_LONGITUDES"

    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 275
    const-string v0, "STEP_TEXTS"

    invoke-virtual {v1, v0, v10}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 277
    return-object v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 5
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->f:Landroid/os/IBinder;

    .line 152
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.apps.maps.NAVIGATION_EVENT_PUBLISHER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->g:Landroid/os/IBinder;

    .line 162
    :cond_16
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    .prologue
    .line 141
    new-instance v0, Lcom/google/android/maps/driveabout/publisher/e;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/publisher/e;-><init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a:LO/q;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b:Ljava/util/List;

    .line 144
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->c:Landroid/os/Bundle;

    .line 145
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->d:Landroid/os/Bundle;

    .line 146
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 136
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
