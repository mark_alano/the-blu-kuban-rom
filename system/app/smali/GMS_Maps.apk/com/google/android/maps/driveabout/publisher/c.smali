.class Lcom/google/android/maps/driveabout/publisher/c;
.super Lcom/google/android/maps/driveabout/publisher/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/publisher/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x2

    .line 55
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 56
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 58
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_4a

    .line 79
    iput v6, v0, Landroid/os/Message;->what:I

    .line 87
    :goto_e
    :try_start_e
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_11} :catch_48

    .line 91
    :goto_11
    return-void

    .line 61
    :pswitch_12
    iget-object v2, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v2

    .line 63
    iget-object v3, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    const-string v4, "com.google.android.apps.maps.NAVIGATION_DATA"

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v3

    .line 65
    iget-object v4, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v4

    .line 67
    if-nez v2, :cond_45

    if-nez v3, :cond_45

    .line 70
    const-string v2, "P"

    invoke-static {v2, v4}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v2, 0x1

    iput v2, v0, Landroid/os/Message;->what:I

    .line 73
    iget-object v2, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Landroid/os/Messenger;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    goto :goto_e

    .line 75
    :cond_45
    iput v6, v0, Landroid/os/Message;->what:I

    goto :goto_e

    .line 88
    :catch_48
    move-exception v0

    goto :goto_11

    .line 58
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_12
    .end packed-switch
.end method
