.class public Lcom/google/android/maps/driveabout/vector/A;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/LinkedList;

.field private e:Z

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private h:Lo/aQ;

.field private i:Ljava/util/List;

.field private final j:Lcom/google/android/maps/driveabout/vector/E;

.field private k:I

.field private l:Z

.field private m:LF/H;

.field private n:Lcom/google/android/maps/driveabout/vector/C;

.field private o:Lcom/google/android/maps/driveabout/vector/B;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Lcom/google/android/maps/driveabout/vector/x;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    .line 85
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    .line 111
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    .line 117
    iput v1, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    .line 120
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    .line 138
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/A;->j:Lcom/google/android/maps/driveabout/vector/E;

    .line 139
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;)V

    return-void
.end method

.method private a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 737
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 738
    const-string v1, "#:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/maps/driveabout/vector/A;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " T:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " E:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " C:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " numM:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 743
    const-string v1, "GLMarkerOverlay"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    return-void
.end method

.method private b(LC/a;)V
    .registers 7
    .parameter

    .prologue
    .line 614
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v1

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 616
    invoke-virtual {v0}, LF/H;->f()Lo/D;

    move-result-object v3

    .line 617
    if-eqz v3, :cond_a

    .line 618
    invoke-virtual {v3}, Lo/D;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v3

    .line 620
    if-eqz v3, :cond_a

    .line 625
    monitor-enter v0

    .line 626
    :try_start_27
    invoke-virtual {v0}, LF/H;->e()Lo/T;

    move-result-object v4

    .line 627
    invoke-virtual {v3, p1, v4}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4, v3}, Lo/T;->b(I)V

    .line 628
    invoke-virtual {v0, v4}, LF/H;->a(Lo/T;)V

    .line 629
    monitor-exit v0

    goto :goto_a

    :catchall_38
    move-exception v1

    monitor-exit v0
    :try_end_3a
    .catchall {:try_start_27 .. :try_end_3a} :catchall_38

    throw v1

    .line 633
    :cond_3b
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->c(LC/a;)V

    return-void
.end method

.method private c(FFLC/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 332
    const/high16 v0, 0x428c

    sub-float v0, p2, v0

    .line 335
    invoke-virtual {p3, p1, v0}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1, v0}, LF/H;->a(Lo/T;)V

    .line 337
    return-void
.end method

.method private c(LC/a;)V
    .registers 4
    .parameter

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 714
    invoke-virtual {v0, p1}, LF/H;->b(LC/a;)Z

    goto :goto_6

    .line 716
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_24

    .line 717
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 719
    :cond_24
    return-void
.end method

.method private d(LF/H;)V
    .registers 4
    .parameter

    .prologue
    .line 259
    invoke-virtual {p1}, LF/H;->r()I

    move-result v0

    if-nez v0, :cond_f

    .line 260
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->n()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    :cond_f
    invoke-virtual {p1}, LF/H;->s()I

    move-result v0

    if-nez v0, :cond_1e

    .line 263
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->o()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_1e
    return-void
.end method

.method private e(LF/H;)V
    .registers 3
    .parameter

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->o:Lcom/google/android/maps/driveabout/vector/B;

    if-eqz v0, :cond_9

    .line 293
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->o:Lcom/google/android/maps/driveabout/vector/B;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/B;->a(LF/H;)V

    .line 295
    :cond_9
    return-void
.end method

.method private i()V
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    .line 168
    return-void
.end method

.method private declared-synchronized j()V
    .registers 3

    .prologue
    .line 726
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 727
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 728
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 729
    invoke-virtual {v0}, LF/H;->r()I

    .line 730
    invoke-virtual {v0}, LF/H;->s()I
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1a

    goto :goto_7

    .line 726
    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 732
    :cond_1d
    :try_start_1d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_1a

    .line 733
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)LF/H;
    .registers 3
    .parameter

    .prologue
    .line 211
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    monitor-exit p0

    return-object v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(LC/a;)V
    .registers 10
    .parameter

    .prologue
    const v7, -0x41b33333

    .line 642
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    .line 643
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->h:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 703
    :goto_13
    return-void

    .line 646
    :cond_14
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    if-nez v0, :cond_a2

    .line 647
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    .line 651
    :goto_1f
    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v2

    .line 658
    invoke-virtual {v1}, Lo/aQ;->d()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v0, v3, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    invoke-virtual {v1}, Lo/aQ;->e()Lo/T;

    move-result-object v3

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v3

    invoke-virtual {v1}, Lo/aQ;->g()Lo/T;

    move-result-object v4

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v4

    invoke-virtual {v1}, Lo/aQ;->f()Lo/T;

    move-result-object v5

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6, v7}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lo/aQ;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;

    move-result-object v3

    .line 664
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 666
    :cond_5d
    :goto_5d
    :try_start_5d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 667
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 668
    invoke-virtual {v0}, LF/H;->n()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_5d

    .line 675
    invoke-virtual {v0}, LF/H;->e()Lo/T;

    move-result-object v5

    .line 676
    invoke-virtual {v2, v5}, Lo/aR;->a(Lo/T;)Z

    move-result v6

    if-eqz v6, :cond_7f

    invoke-virtual {v1, v5}, Lo/aQ;->a(Lo/T;)Z

    move-result v6

    if-nez v6, :cond_8b

    .line 680
    :cond_7f
    invoke-virtual {v3, v5}, Lo/aQ;->a(Lo/T;)Z

    move-result v5

    if-eqz v5, :cond_5d

    .line 686
    invoke-virtual {v0, p1}, LF/H;->a(LC/a;)Z

    move-result v5

    if-eqz v5, :cond_5d

    .line 691
    :cond_8b
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_90
    .catch Ljava/lang/RuntimeException; {:try_start_5d .. :try_end_90} :catch_91

    goto :goto_5d

    .line 693
    :catch_91
    move-exception v0

    .line 699
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0, v4, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V

    .line 701
    :cond_9b
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->h:Lo/aQ;

    .line 702
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->e:Z

    goto/16 :goto_13

    .line 649
    :cond_a2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_1f
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 535
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_22

    .line 536
    monitor-enter p0

    .line 537
    :try_start_10
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    .line 538
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;)V

    .line 539
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/A;->c(LC/a;)V

    .line 540
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_23

    .line 541
    monitor-exit p0

    .line 583
    :cond_22
    :goto_22
    return-void

    .line 546
    :cond_23
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 547
    invoke-virtual {p1}, LD/a;->p()V

    .line 548
    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 549
    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 551
    iget-object v0, p1, LD/a;->g:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 553
    new-instance v3, Lcom/google/android/maps/driveabout/vector/aT;

    invoke-direct {v3, p3}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/r;)V

    .line 556
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    .line 557
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4d
    :goto_4d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_66

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 558
    invoke-virtual {v0}, LF/H;->o()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_4d

    .line 559
    invoke-virtual {v0, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_4d

    .line 581
    :catchall_63
    move-exception v0

    monitor-exit p0
    :try_end_65
    .catchall {:try_start_10 .. :try_end_65} :catchall_63

    throw v0

    .line 563
    :cond_66
    const/4 v0, 0x1

    :try_start_67
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    .line 564
    const/4 v0, 0x0

    .line 565
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_72
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 566
    invoke-virtual {v0}, LF/H;->q()Z

    move-result v5

    if-eqz v5, :cond_85

    move-object v2, v0

    .line 569
    :cond_85
    invoke-virtual {v0, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_72

    .line 576
    :cond_89
    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_9d

    move v0, v1

    :goto_93
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    .line 578
    if-eqz v2, :cond_9b

    .line 579
    invoke-virtual {v2, p1, p2, v3}, LF/H;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 581
    :cond_9b
    monitor-exit p0
    :try_end_9c
    .catchall {:try_start_67 .. :try_end_9c} :catchall_63

    goto :goto_22

    .line 576
    :cond_9d
    const/4 v0, 0x2

    goto :goto_93
.end method

.method public declared-synchronized a(LF/H;)V
    .registers 4
    .parameter

    .prologue
    .line 149
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 150
    invoke-virtual {p1, p0}, LF/H;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 155
    :cond_21
    monitor-exit p0

    return-void

    .line 149
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LF/J;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 225
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    .line 226
    :try_start_3
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_3 .. :try_end_4} :catchall_2c

    .line 227
    :try_start_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 228
    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 229
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 230
    invoke-interface {p1, v0}, LF/J;->a(LF/H;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 231
    invoke-virtual {v0}, LF/H;->q()Z

    move-result v3

    if-eqz v3, :cond_2f

    if-nez p2, :cond_2f

    .line 234
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LF/H;->d(Z)V

    goto :goto_a

    .line 250
    :catchall_29
    move-exception v0

    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_4 .. :try_end_2b} :catchall_29

    :try_start_2b
    throw v0

    .line 251
    :catchall_2c
    move-exception v0

    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_2c

    throw v0

    .line 236
    :cond_2f
    :try_start_2f
    invoke-virtual {v0}, LF/H;->q()Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 239
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LF/H;->d(Z)V

    .line 240
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    .line 242
    :cond_3e
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, LF/H;->p()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    .line 244
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 245
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    .line 246
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    goto :goto_a

    .line 250
    :cond_54
    monitor-exit p0
    :try_end_55
    .catchall {:try_start_2f .. :try_end_55} :catchall_29

    .line 251
    :try_start_55
    monitor-exit v1
    :try_end_56
    .catchall {:try_start_55 .. :try_end_56} :catchall_2c

    .line 252
    return-void
.end method

.method public declared-synchronized a(Ljava/util/List;FFLo/T;LC/a;I)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 588
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0, p5}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    .line 592
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 593
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->k()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 594
    invoke-interface {v0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v2

    .line 595
    if-ge v2, p6, :cond_a

    .line 596
    new-instance v3, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v3, v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2a
    .catchall {:try_start_1 .. :try_end_2a} :catchall_2b

    goto :goto_a

    .line 588
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 600
    :cond_2e
    monitor-exit p0

    return-void
.end method

.method public b(LF/H;)V
    .registers 5
    .parameter

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    .line 176
    :try_start_3
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_3 .. :try_end_4} :catchall_3c

    .line 177
    :try_start_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 178
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 180
    invoke-virtual {p1}, LF/H;->q()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 182
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LF/H;->d(Z)V

    .line 183
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    .line 185
    :cond_2d
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    .line 189
    :cond_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_4 .. :try_end_37} :catchall_39

    .line 190
    :try_start_37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_3c

    .line 191
    return-void

    .line 189
    :catchall_39
    move-exception v0

    :try_start_3a
    monitor-exit p0
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_39

    :try_start_3b
    throw v0

    .line 190
    :catchall_3c
    move-exception v0

    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_3c

    throw v0
.end method

.method public b(FFLC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    if-eqz v0, :cond_14

    .line 357
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    .line 358
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v0, :cond_12

    .line 359
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/C;->b(LF/H;)V

    .line 361
    :cond_12
    const/4 v0, 0x1

    .line 363
    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 526
    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    .line 318
    :try_start_3
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_3 .. :try_end_4} :catchall_d

    .line 321
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->j()V

    .line 322
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_4 .. :try_end_8} :catchall_a

    .line 323
    :try_start_8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_8 .. :try_end_9} :catchall_d

    .line 324
    return-void

    .line 322
    :catchall_a
    move-exception v0

    :try_start_b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_b .. :try_end_c} :catchall_a

    :try_start_c
    throw v0

    .line 323
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_d

    throw v0
.end method

.method public declared-synchronized c(LF/H;)V
    .registers 4
    .parameter

    .prologue
    .line 198
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 199
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, LF/H;->p()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->d(LF/H;)V

    .line 202
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    .line 203
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    .line 205
    :cond_24
    monitor-exit p0

    return-void

    .line 198
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 368
    monitor-enter p0

    .line 369
    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 370
    invoke-virtual {v0}, LF/H;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 371
    invoke-virtual {v0, p1, p2, p3, p4}, LF/H;->b(FFLo/T;LC/a;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 372
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    .line 373
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    .line 374
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v0, :cond_33

    .line 376
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/vector/C;->a(LF/H;)V

    .line 378
    :cond_33
    monitor-exit p0

    move v0, v1

    .line 383
    :goto_35
    return v0

    .line 382
    :cond_36
    monitor-exit p0

    .line 383
    const/4 v0, 0x0

    goto :goto_35

    .line 382
    :catchall_39
    move-exception v0

    monitor-exit p0
    :try_end_3b
    .catchall {:try_start_2 .. :try_end_3b} :catchall_39

    throw v0
.end method

.method public d(FFLo/T;LC/a;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 388
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    if-eqz v1, :cond_30

    .line 389
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLC/a;)V

    .line 392
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1}, LF/H;->l()LF/I;

    move-result-object v1

    if-eqz v1, :cond_1f

    .line 393
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v1}, LF/H;->l()LF/I;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-virtual {v2}, LF/H;->e()Lo/T;

    move-result-object v2

    invoke-interface {v1, v2}, LF/I;->a(Lo/T;)V

    .line 395
    :cond_1f
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    if-eqz v1, :cond_2a

    .line 396
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->n:Lcom/google/android/maps/driveabout/vector/C;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    invoke-interface {v1, v2}, Lcom/google/android/maps/driveabout/vector/C;->c(LF/H;)V

    .line 398
    :cond_2a
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->m:LF/H;

    .line 399
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    .line 400
    const/4 v0, 0x1

    .line 402
    :cond_30
    return v0
.end method

.method public e()V
    .registers 4

    .prologue
    .line 273
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    monitor-enter v1

    .line 274
    :try_start_3
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_3 .. :try_end_4} :catchall_39

    .line 275
    :try_start_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->e()Lcom/google/android/maps/driveabout/vector/c;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_1d

    instance-of v2, v0, LF/H;

    if-eqz v2, :cond_1d

    check-cast v0, LF/H;

    invoke-virtual {v0}, LF/H;->m()Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    if-ne v0, p0, :cond_1d

    .line 278
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->a:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    .line 280
    :cond_1d
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->j()V

    .line 281
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 282
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/A;->e(LF/H;)V

    goto :goto_26

    .line 287
    :catchall_36
    move-exception v0

    monitor-exit p0
    :try_end_38
    .catchall {:try_start_4 .. :try_end_38} :catchall_36

    :try_start_38
    throw v0

    .line 288
    :catchall_39
    move-exception v0

    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_39

    throw v0

    .line 284
    :cond_3c
    :try_start_3c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 285
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/A;->i()V

    .line 287
    monitor-exit p0
    :try_end_4a
    .catchall {:try_start_3c .. :try_end_4a} :catchall_36

    .line 288
    :try_start_4a
    monitor-exit v1
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_39

    .line 289
    return-void
.end method

.method public h()Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method public j_()Z
    .registers 2

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/A;->l:Z

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/A;->j:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
