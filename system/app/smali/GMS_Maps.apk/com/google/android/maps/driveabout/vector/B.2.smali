.class Lcom/google/android/maps/driveabout/vector/B;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# instance fields
.field a:LD/a;

.field b:Lz/c;

.field c:LC/a;

.field d:Ljava/util/List;

.field final synthetic e:Lcom/google/android/maps/driveabout/vector/A;


# direct methods
.method private a(Ljava/util/List;)V
    .registers 5
    .parameter

    .prologue
    .line 483
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 484
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LF/H;->b(I)V

    goto :goto_4

    .line 486
    :cond_15
    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 455
    return-void
.end method

.method public declared-synchronized a(LF/H;)V
    .registers 3
    .parameter

    .prologue
    .line 449
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {p1, v0}, LF/H;->c(LD/a;)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 450
    monitor-exit p0

    return-void

    .line 449
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lz/c;)V
    .registers 2
    .parameter

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/B;->b:Lz/c;

    .line 443
    return-void
.end method

.method public declared-synchronized b(Lz/c;)V
    .registers 6
    .parameter

    .prologue
    .line 459
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_55

    .line 461
    :try_start_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/B;->a(Ljava/util/List;)V

    .line 462
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 463
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/B;->a(Ljava/util/List;)V

    .line 466
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LC/a;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/B;->c:LC/a;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(Lcom/google/android/maps/driveabout/vector/A;LC/a;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/B;->e:Lcom/google/android/maps/driveabout/vector/A;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/A;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_36
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/H;

    .line 472
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {v0, v3}, LF/H;->a(LD/a;)V

    .line 473
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/B;->a:LD/a;

    invoke-virtual {v0, v3}, LF/H;->b(LD/a;)V

    .line 474
    const/16 v3, 0xff

    invoke-virtual {v0, v3}, LF/H;->b(I)V

    goto :goto_36

    .line 476
    :catchall_52
    move-exception v0

    monitor-exit v1
    :try_end_54
    .catchall {:try_start_4 .. :try_end_54} :catchall_52

    :try_start_54
    throw v0
    :try_end_55
    .catchall {:try_start_54 .. :try_end_55} :catchall_55

    .line 459
    :catchall_55
    move-exception v0

    monitor-exit p0

    throw v0

    .line 476
    :cond_58
    :try_start_58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_52

    .line 477
    monitor-exit p0

    return-void
.end method
