.class public abstract Lcom/google/android/maps/driveabout/vector/d;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/maps/driveabout/vector/x;

.field protected b:Z

.field protected c:I

.field private d:Lcom/google/android/maps/driveabout/vector/e;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/x;)V
    .registers 2
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/d;->a:Lcom/google/android/maps/driveabout/vector/x;

    .line 49
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/d;->g()V

    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    if-eqz v0, :cond_c

    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    invoke-interface {v0, p0, p1}, Lcom/google/android/maps/driveabout/vector/e;->a(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/c;)V

    .line 72
    :cond_c
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/d;->d:Lcom/google/android/maps/driveabout/vector/e;

    .line 53
    return-void
.end method

.method public abstract a(Ljava/util/List;FFLo/T;LC/a;I)V
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 96
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/d;->c:I

    .line 97
    return-void
.end method

.method public l()I
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/d;->m()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    .line 106
    :goto_7
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/d;->c:I

    add-int/2addr v0, v1

    return v0

    .line 105
    :cond_b
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public l_()Z
    .registers 2

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/d;->b:Z

    return v0
.end method
