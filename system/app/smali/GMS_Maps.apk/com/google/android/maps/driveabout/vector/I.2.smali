.class Lcom/google/android/maps/driveabout/vector/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/vector/G;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/G;)V
    .registers 2
    .parameter

    .prologue
    .line 664
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/G;Lcom/google/android/maps/driveabout/vector/H;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 664
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/I;-><init>(Lcom/google/android/maps/driveabout/vector/G;)V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 671
    :try_start_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->b(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 672
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_2c

    .line 675
    :try_start_12
    sget-object v1, Lm/q;->c:Lm/q;

    invoke-static {v0, v1}, Lm/o;->a(Ljava/util/List;Lm/q;)Lm/v;
    :try_end_17
    .catch Lm/k; {:try_start_12 .. :try_end_17} :catch_2f

    move-result-object v0

    .line 693
    :goto_18
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 694
    :try_start_1f
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;Lm/v;)Lm/v;

    .line 695
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;Z)Z

    .line 696
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_1f .. :try_end_2b} :catchall_69

    .line 700
    return-void

    .line 672
    :catchall_2c
    move-exception v0

    :try_start_2d
    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    throw v0

    .line 676
    :catch_2f
    move-exception v0

    .line 679
    :try_start_30
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->c(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 680
    const/4 v0, 0x0

    .line 681
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_40
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 682
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_40

    .line 684
    :cond_53
    const/16 v0, 0x7d0

    if-ge v1, v0, :cond_5e

    .line 685
    sget-object v0, Lm/q;->d:Lm/q;

    invoke-static {v2, v0}, Lm/o;->a(Ljava/util/List;Lm/q;)Lm/v;

    move-result-object v0

    goto :goto_18

    .line 687
    :cond_5e
    invoke-static {}, Lm/v;->a()Lm/v;
    :try_end_61
    .catch Lm/k; {:try_start_30 .. :try_end_61} :catch_63

    move-result-object v0

    goto :goto_18

    .line 689
    :catch_63
    move-exception v0

    .line 690
    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    goto :goto_18

    .line 696
    :catchall_69
    move-exception v0

    :try_start_6a
    monitor-exit v1
    :try_end_6b
    .catchall {:try_start_6a .. :try_end_6b} :catchall_69

    throw v0
.end method
