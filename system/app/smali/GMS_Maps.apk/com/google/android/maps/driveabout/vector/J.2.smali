.class public Lcom/google/android/maps/driveabout/vector/j;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:LD/b;

.field private c:Lo/T;

.field private d:F

.field private e:I

.field private f:I

.field private final g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lh/e;

.field private volatile m:I

.field private final n:I

.field private final o:Lcom/google/android/maps/driveabout/vector/m;

.field private p:Lcom/google/android/maps/driveabout/vector/l;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->j:Z

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->k:Z

    .line 80
    const/high16 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->p:Lcom/google/android/maps/driveabout/vector/l;

    .line 105
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/j;->a:Landroid/content/res/Resources;

    .line 106
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/j;->o:Lcom/google/android/maps/driveabout/vector/m;

    .line 107
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_37

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_37

    const v0, 0x7f0b004b

    :goto_27
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    .line 110
    const v0, 0x7f090093

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->n:I

    .line 111
    return-void

    .line 107
    :cond_37
    const v0, 0x7f0b004a

    goto :goto_27
.end method

.method private a(FF)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 499
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    if-nez v2, :cond_9

    move v0, v1

    .line 511
    :cond_8
    :goto_8
    return v0

    .line 503
    :cond_9
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/j;->f:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    .line 504
    sget-object v3, Lcom/google/android/maps/driveabout/vector/k;->a:[I

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/j;->o:Lcom/google/android/maps/driveabout/vector/m;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/m;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_5a

    .line 510
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->e:I

    int-to-float v3, v3

    add-float/2addr v3, p1

    .line 511
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/j;->h:I

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_3b

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/j;->h:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3b

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_3b

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_8

    :cond_3b
    move v0, v1

    goto :goto_8

    .line 506
    :pswitch_3d
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->e:I

    int-to-float v3, v3

    sub-float v3, p1, v3

    .line 507
    cmpl-float v4, v3, v6

    if-ltz v4, :cond_58

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_58

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_58

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_8

    :cond_58
    move v0, v1

    goto :goto_8

    .line 504
    :pswitch_data_5a
    .packed-switch 0x1
        :pswitch_3d
    .end packed-switch
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->l:Lh/e;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public a(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 120
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/j;->e:I

    .line 121
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/j;->f:I

    .line 122
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/high16 v7, 0x420c

    const/high16 v1, 0x3f80

    const/4 v6, 0x0

    .line 405
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    if-eqz v0, :cond_14

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    if-nez v0, :cond_15

    .line 467
    :cond_14
    :goto_14
    return-void

    .line 410
    :cond_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    if-nez v0, :cond_3f

    .line 411
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    .line 412
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    invoke-virtual {v0, v5}, LD/b;->c(Z)V

    .line 415
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_b9

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_b9

    const v0, 0x7f020171

    .line 418
    :goto_38
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/j;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v3, v0}, LD/b;->a(Landroid/content/res/Resources;I)V

    .line 422
    :cond_3f
    invoke-virtual {p1}, LD/a;->p()V

    .line 423
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    .line 424
    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 426
    const/16 v0, 0x303

    invoke-interface {v2, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 427
    iget-object v0, p1, LD/a;->d:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 429
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/j;->e()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 430
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->l:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    .line 431
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    if-nez v0, :cond_6e

    .line 432
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->l:Lh/e;

    .line 436
    :cond_6e
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->i:Z

    if-eqz v0, :cond_be

    .line 437
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/j;->n:I

    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/vector/j;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 443
    :goto_77
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 444
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->d:F

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->i:Z

    if-eqz v0, :cond_ca

    const v0, 0x3faa3d71

    :goto_83
    mul-float/2addr v0, v3

    .line 445
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    invoke-static {p1, p2, v3, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 448
    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    .line 449
    cmpl-float v3, v0, v7

    if-lez v3, :cond_a4

    .line 451
    invoke-virtual {p2}, LC/a;->p()F

    move-result v3

    neg-float v3, v3

    invoke-interface {v2, v3, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 454
    sub-float/2addr v0, v7

    .line 455
    invoke-interface {v2, v0, v1, v6, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 458
    invoke-virtual {p2}, LC/a;->p()F

    move-result v0

    invoke-interface {v2, v0, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 462
    :cond_a4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    invoke-virtual {v0, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 463
    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 464
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v3, 0x4

    invoke-interface {v2, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 466
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_14

    .line 415
    :cond_b9
    const v0, 0x7f020170

    goto/16 :goto_38

    .line 439
    :cond_be
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    invoke-interface {v2, v0, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_77

    :cond_ca
    move v0, v1

    .line 444
    goto :goto_83
.end method

.method public a(FFLC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 480
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 481
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->i:Z

    .line 487
    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a_(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 471
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 472
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/j;->g()V

    .line 473
    const/4 v0, 0x1

    .line 475
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 516
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/j;->j:Z

    .line 517
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->j:Z

    if-eqz v0, :cond_14

    .line 518
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->k:Z

    if-eqz v0, :cond_13

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/j;->e()Z

    move-result v0

    if-nez v0, :cond_13

    .line 519
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    .line 524
    :cond_13
    :goto_13
    return-void

    .line 522
    :cond_14
    const/high16 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    goto :goto_13
.end method

.method public b(LC/a;LD/a;)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 126
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/j;->g:I

    div-int/lit8 v0, v0, 0x2

    .line 128
    sget-object v1, Lcom/google/android/maps/driveabout/vector/k;->a:[I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/j;->o:Lcom/google/android/maps/driveabout/vector/m;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/m;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_80

    .line 134
    invoke-virtual {p1}, LC/a;->k()I

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/j;->e:I

    add-int/2addr v2, v0

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/j;->f:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    .line 139
    :goto_27
    invoke-virtual {p1}, LC/a;->k()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/j;->h:I

    .line 141
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    if-eqz v1, :cond_3e

    .line 143
    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    invoke-virtual {p1, v1, v4}, LC/a;->a(Lo/T;Z)F

    move-result v1

    invoke-virtual {p1, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->d:F

    .line 146
    :cond_3e
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->j:Z

    if-eqz v0, :cond_65

    .line 147
    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_75

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_75

    .line 148
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->k:Z

    if-nez v0, :cond_65

    .line 149
    iput-boolean v6, p0, Lcom/google/android/maps/driveabout/vector/j;->k:Z

    .line 154
    new-instance v0, Lh/e;

    const-wide/16 v1, 0x7d0

    const-wide/16 v3, 0x1f4

    sget-object v5, Lh/g;->b:Lh/g;

    invoke-direct/range {v0 .. v5}, Lh/e;-><init>(JJLh/g;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->l:Lh/e;

    .line 174
    :cond_65
    :goto_65
    return v6

    .line 130
    :pswitch_66
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/j;->e:I

    add-int/2addr v1, v0

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/j;->f:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/j;->c:Lo/T;

    goto :goto_27

    .line 162
    :cond_75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->l:Lh/e;

    .line 163
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/j;->k:Z

    .line 165
    const/high16 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/j;->m:I

    goto :goto_65

    .line 128
    nop

    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_66
    .end packed-switch
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    if-eqz v0, :cond_c

    .line 190
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/j;->b:LD/b;

    .line 193
    :cond_c
    return-void
.end method

.method public k_()V
    .registers 2

    .prologue
    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/j;->i:Z

    .line 496
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->y:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
