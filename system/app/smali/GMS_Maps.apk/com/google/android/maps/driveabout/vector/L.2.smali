.class public Lcom/google/android/maps/driveabout/vector/L;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final a:F

.field private static final b:F


# instance fields
.field private c:Lo/T;

.field private d:I

.field private e:Lcom/google/android/maps/driveabout/vector/q;

.field private f:Z

.field private final g:LE/o;

.field private final h:LE/a;

.field private final i:LE/a;

.field private final j:Lo/T;

.field private final k:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 21
    const-wide v0, 0x3fb657184ae74487L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/vector/L;->a:F

    .line 26
    const-wide v0, 0x3faacee9f37bebd6L

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/vector/L;->b:F

    return-void
.end method

.method public constructor <init>()V
    .registers 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x6

    .line 46
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    .line 31
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->e:Lcom/google/android/maps/driveabout/vector/q;

    .line 42
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    .line 43
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    .line 49
    new-instance v0, LE/o;

    invoke-direct {v0, v2}, LE/o;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    .line 53
    new-instance v0, LE/a;

    invoke-direct {v0, v2}, LE/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->h:LE/a;

    .line 54
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/L;->a([I)I

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->h:LE/a;

    invoke-virtual {v1, v0, v3}, LE/a;->a(II)V

    .line 57
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->h:LE/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, LE/a;->a(II)V

    .line 59
    new-instance v0, LE/a;

    invoke-direct {v0, v2}, LE/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->i:LE/a;

    .line 60
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/L;->a([I)I

    move-result v0

    .line 62
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->i:LE/a;

    invoke-virtual {v1, v0, v3}, LE/a;->a(II)V

    .line 63
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->i:LE/a;

    or-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0, v4}, LE/a;->a(II)V

    .line 64
    return-void
.end method

.method private static a([I)I
    .registers 4
    .parameter

    .prologue
    const v2, 0xff00

    .line 165
    const/4 v0, 0x0

    aget v0, p0, v0

    and-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x10

    const/4 v1, 0x1

    aget v1, p0, v1

    and-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x2

    aget v1, p0, v1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method private a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    invoke-virtual {v0, p1}, LE/o;->a(LD/a;)V

    .line 117
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p3, v0, :cond_17

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p3, v0, :cond_17

    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_18

    .line 161
    :cond_17
    :goto_17
    return-void

    .line 121
    :cond_18
    invoke-virtual {p2}, LC/a;->t()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->h()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, LC/a;->c(F)F

    move-result v1

    .line 123
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p3, v0, :cond_c2

    const/high16 v0, 0x4040

    :goto_2b
    sub-float v0, v1, v0

    .line 125
    invoke-virtual {p2}, LC/a;->q()F

    move-result v1

    invoke-virtual {p2}, LC/a;->n()F

    move-result v2

    const/high16 v3, 0x3f00

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpg-float v1, v1, v0

    if-lez v1, :cond_17

    .line 136
    invoke-virtual {p2}, LC/a;->o()F

    move-result v1

    float-to-int v1, v1

    .line 137
    iput v1, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    .line 139
    invoke-virtual {p2, v0}, LC/a;->d(F)Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->c()Lo/ae;

    move-result-object v0

    check-cast v0, Lo/t;

    .line 141
    invoke-virtual {v0}, Lo/t;->g()Lo/T;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->c:Lo/T;

    .line 142
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    invoke-virtual {v2, v4, v4}, Lo/T;->d(II)V

    .line 143
    invoke-virtual {v0}, Lo/t;->f()Lo/T;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->c:Lo/T;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    invoke-static {v0, v2, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v2, v3}, LE/o;->a(Lo/T;I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v2, v3}, LE/o;->a(Lo/T;I)V

    .line 147
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->c:Lo/T;

    invoke-virtual {p2}, LC/a;->t()Lo/T;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/T;->c(Lo/T;)F

    move-result v2

    .line 149
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p3, v0, :cond_c6

    sget v0, Lcom/google/android/maps/driveabout/vector/L;->b:F

    :goto_86
    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 152
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->b(I)V

    .line 153
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->b(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v2, v3}, LE/o;->a(Lo/T;I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v2, v3}, LE/o;->a(Lo/T;I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->b(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->b(I)V

    .line 159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->j:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v1, v2}, LE/o;->a(Lo/T;I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->k:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    invoke-virtual {v0, v1, v2}, LE/o;->a(Lo/T;I)V

    goto/16 :goto_17

    .line 123
    :cond_c2
    const/high16 v0, 0x40a0

    goto/16 :goto_2b

    .line 149
    :cond_c6
    sget v0, Lcom/google/android/maps/driveabout/vector/L;->a:F

    goto :goto_86
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/q;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 107
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/L;->f:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p1, v1, :cond_e

    .line 108
    :cond_9
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/L;->e:Lcom/google/android/maps/driveabout/vector/q;

    .line 109
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/L;->f:Z

    .line 110
    const/4 v0, 0x1

    .line 112
    :cond_e
    return v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/L;->a(Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 74
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/vector/L;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;)V

    .line 76
    :cond_11
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-nez v0, :cond_1a

    .line 94
    :goto_19
    return-void

    .line 79
    :cond_1a
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 80
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 82
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->c:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/L;->d:I

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 83
    invoke-virtual {p1}, LD/a;->n()V

    .line 84
    invoke-virtual {p1}, LD/a;->t()V

    .line 85
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 86
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 87
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v1, v2, :cond_57

    .line 88
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->i:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    .line 92
    :goto_48
    const/4 v1, 0x5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/L;->g:LE/o;

    invoke-virtual {v3}, LE/o;->a()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 93
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_19

    .line 90
    :cond_57
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/L;->h:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    goto :goto_48
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 98
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/L;->f:Z

    .line 99
    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->s:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
