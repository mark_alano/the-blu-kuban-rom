.class public Lcom/google/android/maps/driveabout/vector/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/c;

.field private final b:Lcom/google/android/maps/driveabout/vector/d;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    .line 40
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    .line 41
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    return v0
.end method

.method public a(FFLo/T;LC/a;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    .line 46
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_2c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/d;->l()I

    move-result v0

    if-lez v0, :cond_2c

    .line 47
    invoke-virtual {p4}, LC/a;->m()F

    move-result v0

    const/high16 v1, 0x40a0

    mul-float/2addr v0, v1

    .line 48
    mul-float/2addr v0, v0

    .line 52
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/d;->l()I

    move-result v2

    float-to-int v0, v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/t;->c:I

    .line 54
    :cond_2c
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/t;->d:Z

    .line 62
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/t;->d:Z

    return v0
.end method

.method public c()V
    .registers 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/t;->b:Lcom/google/android/maps/driveabout/vector/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/t;->a:Lcom/google/android/maps/driveabout/vector/c;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/d;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 75
    return-void
.end method
