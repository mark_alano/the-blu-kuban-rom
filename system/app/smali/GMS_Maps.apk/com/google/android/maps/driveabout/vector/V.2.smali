.class Lcom/google/android/maps/driveabout/vector/v;
.super LR/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/vector/u;

.field private volatile b:Z

.field private volatile c:I

.field private volatile d:Z

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/u;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 415
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/v;->a:Lcom/google/android/maps/driveabout/vector/u;

    .line 416
    const-string v0, "RenderDrive"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 393
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->b:Z

    .line 398
    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/v;->c:I

    .line 401
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/v;->d:Z

    .line 407
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/v;->e:Z

    .line 413
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/v;->f:J

    .line 417
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/v;)Z
    .registers 2
    .parameter

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->e:Z

    return v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 449
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/v;->a:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/w;->q_()V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    .line 450
    monitor-exit p0

    return-void

    .line 449
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 458
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/v;->c:I

    if-eq v0, p1, :cond_10

    const/16 v0, 0xf

    if-le p1, v0, :cond_10

    .line 459
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/v;->c:I

    .line 460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->d:Z

    .line 461
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/v;->interrupt()V

    .line 463
    :cond_10
    return-void
.end method

.method public declared-synchronized a(J)V
    .registers 4
    .parameter

    .prologue
    .line 482
    monitor-enter p0

    :try_start_1
    iput-wide p1, p0, Lcom/google/android/maps/driveabout/vector/v;->f:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 483
    monitor-exit p0

    return-void

    .line 482
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->b:Z

    .line 454
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/v;->interrupt()V

    .line 455
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 469
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/v;->c:I

    return v0
.end method

.method public d()V
    .registers 1

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/v;->e()V

    .line 474
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/v;->interrupt()V

    .line 475
    return-void
.end method

.method public declared-synchronized e()V
    .registers 2

    .prologue
    .line 478
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->e:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 479
    monitor-exit p0

    return-void

    .line 478
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()J
    .registers 3

    .prologue
    .line 486
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/v;->f:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-wide v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()V
    .registers 5

    .prologue
    .line 421
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->b:Z

    if-eqz v0, :cond_36

    .line 424
    :cond_4
    const/4 v0, 0x0

    :try_start_5
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->d:Z

    .line 425
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/v;->c:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/v;->sleep(J)V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_d} :catch_37

    .line 429
    :goto_d
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->d:Z

    if-nez v0, :cond_4

    .line 431
    monitor-enter p0

    .line 432
    :try_start_12
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->e:Z

    if-nez v0, :cond_20

    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/v;->f:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_31

    .line 435
    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/v;->e:Z

    .line 436
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/v;->f:J

    .line 437
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/v;->a:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/w;->q_()V

    .line 439
    :cond_31
    monitor-exit p0

    goto :goto_0

    :catchall_33
    move-exception v0

    monitor-exit p0
    :try_end_35
    .catchall {:try_start_12 .. :try_end_35} :catchall_33

    throw v0

    .line 441
    :cond_36
    return-void

    .line 426
    :catch_37
    move-exception v0

    goto :goto_d
.end method
