.class public Lcom/google/android/maps/driveabout/vector/VectorMapView;
.super Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/ay;
.implements Lcom/google/android/maps/driveabout/vector/bA;
.implements Lcom/google/android/maps/driveabout/vector/bv;
.implements Lcom/google/android/maps/driveabout/vector/w;
.implements Lo/C;


# instance fields
.field protected a:Lcom/google/android/maps/driveabout/vector/bk;

.field private final b:Lcom/google/android/maps/driveabout/vector/s;

.field private final c:Landroid/content/res/Resources;

.field private d:Lcom/google/android/maps/driveabout/vector/aK;

.field private e:LaD/g;

.field private f:Lcom/google/android/maps/driveabout/vector/bz;

.field private g:Lcom/google/android/maps/driveabout/vector/by;

.field private h:Lcom/google/android/maps/driveabout/vector/bt;

.field private i:Lcom/google/android/maps/driveabout/vector/D;

.field private j:Z

.field private k:LC/b;

.field private l:J

.field private m:Z

.field private n:LG/a;

.field private o:Lcom/google/android/maps/driveabout/vector/u;

.field private p:Lz/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 87
    new-instance v0, Lcom/google/android/maps/driveabout/vector/s;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    .line 100
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    .line 205
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    .line 206
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->B()V

    .line 207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 197
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    new-instance v0, Lcom/google/android/maps/driveabout/vector/s;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    .line 100
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    .line 198
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    .line 201
    return-void
.end method

.method private B()V
    .registers 12

    .prologue
    .line 217
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setPreserveEGLContextOnPause(Z)V

    .line 219
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bt;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/bt;-><init>(Lcom/google/android/maps/driveabout/vector/bv;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    .line 220
    new-instance v0, LaD/g;

    invoke-direct {v0}, LaD/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    .line 221
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, v1, v2}, LaD/g;->a(Landroid/content/Context;LaD/m;)V

    .line 222
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setFocusable(Z)V

    .line 223
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setClickable(Z)V

    .line 224
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v8, v0, Landroid/util/DisplayMetrics;->density:F

    .line 226
    const/16 v9, 0x100

    .line 227
    const/16 v10, 0x100

    .line 230
    new-instance v0, Lcom/google/android/maps/driveabout/vector/u;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/u;-><init>(Lcom/google/android/maps/driveabout/vector/w;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    .line 235
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 241
    const/4 v4, 0x0

    .line 242
    const/16 v5, 0x10

    .line 243
    const/16 v6, 0x8

    .line 244
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 245
    const/16 v1, 0x8

    .line 246
    const/16 v2, 0x8

    .line 247
    const/16 v3, 0x8

    .line 248
    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    :cond_55
    const/4 v1, 0x5

    .line 253
    const/4 v2, 0x6

    .line 254
    const/4 v3, 0x5

    .line 255
    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    const/4 v6, 0x0

    .line 260
    new-instance v0, Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/O;-><init>(IIIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/O;

    .line 264
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/O;->a(Z)V

    goto :goto_6d

    .line 267
    :cond_7e
    new-instance v1, Lcom/google/android/maps/driveabout/vector/N;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/O;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/O;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/vector/N;-><init>([Lcom/google/android/maps/driveabout/vector/O;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setEGLConfigChooser(Lcom/google/android/maps/driveabout/vector/U;)V

    .line 270
    const/4 v7, 0x0

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p:Lz/k;

    .line 295
    new-instance v0, LC/a;

    sget-object v1, LC/a;->d:LC/b;

    const/4 v5, 0x0

    move v2, v9

    move v3, v10

    move v4, v8

    invoke-direct/range {v0 .. v5}, LC/a;-><init>(LC/b;IIFLjava/lang/Thread;)V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c()LA/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v5

    .line 300
    new-instance v1, Lcom/google/android/maps/driveabout/vector/aK;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p:Lz/k;

    move-object v4, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/maps/driveabout/vector/aK;-><init>(Lcom/google/android/maps/driveabout/vector/u;Landroid/content/res/Resources;LC/a;Lcom/google/android/maps/driveabout/vector/aZ;Lz/k;Lz/t;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    .line 355
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setRenderer(Lcom/google/android/maps/driveabout/vector/ac;)V

    .line 357
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 361
    :cond_c4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setRenderMode(I)V

    .line 363
    return-void
.end method


# virtual methods
.method public A()Lcom/google/android/maps/driveabout/vector/aq;
    .registers 2

    .prologue
    .line 1132
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;
    .registers 3
    .parameter

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    return-object v0
.end method

.method public a(LaN/B;)Lo/D;
    .registers 4
    .parameter

    .prologue
    .line 1137
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/aq;

    move-result-object v0

    .line 1138
    if-eqz v0, :cond_f

    .line 1139
    invoke-static {p1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    .line 1140
    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/T;)Lo/D;

    move-result-object v0

    .line 1142
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(FF)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_5

    .line 907
    :cond_4
    :goto_4
    return-void

    .line 892
    :cond_5
    new-instance v2, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v2, v0, v1, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    .line 897
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v3

    .line 898
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 899
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_29
    if-ltz v1, :cond_4

    .line 900
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 901
    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/maps/driveabout/vector/D;->a(FFLC/a;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 902
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    .line 903
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto :goto_4

    .line 899
    :cond_3d
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_29
.end method

.method public a(FFF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_9

    .line 823
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;FFF)V

    .line 825
    :cond_9
    return-void
.end method

.method protected a(I)V
    .registers 3
    .parameter

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(I)V

    .line 593
    return-void
.end method

.method public a(LS/a;)V
    .registers 3
    .parameter

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1}, LS/b;->a(LS/a;)V

    .line 412
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 3
    .parameter

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 408
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aG;)V
    .registers 3
    .parameter

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v0, :cond_9

    .line 1011
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aG;)V

    .line 1013
    :cond_9
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    .line 1164
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 974
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    .line 975
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 976
    return-void
.end method

.method public a(ZZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 367
    return-void
.end method

.method public a(Landroid/view/MotionEvent;FF)Z
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 653
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_9

    move v0, v2

    .line 688
    :goto_8
    return v0

    .line 661
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    .line 662
    invoke-virtual {v0}, LS/b;->j_()Z

    move-result v4

    if-eqz v4, :cond_28

    .line 663
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v0, v4, v5, v1}, LS/b;->b(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 664
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v3

    .line 665
    goto :goto_8

    .line 669
    :cond_28
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v5

    .line 670
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 671
    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_35
    if-ltz v4, :cond_73

    .line 672
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 673
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->j_()Z

    move-result v6

    if-eqz v6, :cond_6f

    .line 675
    if-nez v1, :cond_5c

    .line 676
    new-instance v1, LC/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v9

    invoke-direct {v1, v6, v7, v8, v9}, LC/a;-><init>(LC/b;IIF)V

    .line 679
    :cond_5c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v0, v6, v7, v1}, Lcom/google/android/maps/driveabout/vector/D;->b(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 682
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v3

    .line 683
    goto :goto_8

    .line 671
    :cond_6f
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_35

    :cond_73
    move v0, v2

    .line 688
    goto :goto_8
.end method

.method public b()V
    .registers 3

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d()V

    .line 494
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->g()V

    .line 495
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->b()V

    .line 499
    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_1e

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v1, :cond_1e

    .line 501
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->p()LD/a;

    move-result-object v1

    invoke-virtual {v0, v1}, LB/a;->a(LD/a;)V

    .line 503
    :cond_1e
    return-void
.end method

.method public b(LS/a;)V
    .registers 3
    .parameter

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1}, LS/b;->b(LS/a;)V

    .line 416
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 3
    .parameter

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, p1, :cond_c

    .line 422
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d()V

    .line 424
    :cond_c
    return-void
.end method

.method public b(FF)Z
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 915
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_9

    move v0, v3

    .line 948
    :goto_8
    return v0

    .line 919
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v6

    .line 920
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 925
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v2

    .line 926
    invoke-virtual {v2}, LS/b;->j_()Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 927
    invoke-virtual {v2, p1, p2, v1, v1}, LS/b;->d(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 928
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m()V

    move v0, v4

    .line 929
    goto :goto_8

    .line 933
    :cond_2a
    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move-object v2, v1

    :goto_2e
    if-ltz v5, :cond_65

    .line 934
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 935
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->j_()Z

    move-result v7

    if-eqz v7, :cond_61

    .line 937
    if-nez v2, :cond_59

    .line 938
    new-instance v2, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v9

    invoke-direct {v2, v1, v7, v8, v9}, LC/a;-><init>(LC/b;IIF)V

    .line 941
    invoke-virtual {v2, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    .line 943
    :cond_59
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/D;->d(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_61

    move v0, v4

    .line 944
    goto :goto_8

    .line 933
    :cond_61
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_2e

    :cond_65
    move v0, v3

    .line 948
    goto :goto_8
.end method

.method public c()LA/c;
    .registers 2

    .prologue
    .line 1158
    sget-object v0, LA/c;->a:LA/c;

    return-object v0
.end method

.method public c(Z)Lcom/google/android/maps/driveabout/vector/aA;
    .registers 3
    .parameter

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    return-object v0
.end method

.method public c(FF)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 730
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_6

    .line 814
    :goto_5
    return-void

    .line 737
    :cond_6
    new-instance v4, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v3

    invoke-direct {v4, v0, v1, v2, v3}, LC/a;-><init>(LC/b;IIF)V

    .line 741
    invoke-virtual {v4, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v3

    .line 745
    const/4 v0, 0x0

    .line 751
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    .line 754
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v1

    if-eqz v1, :cond_36

    .line 755
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, LS/b;->a_(FFLo/T;LC/a;)Z

    move-result v0

    .line 759
    :cond_36
    if-nez v0, :cond_4a

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v1

    if-eqz v1, :cond_4a

    .line 760
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v3, v4}, Lcom/google/android/maps/driveabout/vector/x;->a_(FFLo/T;LC/a;)Z

    move-result v0

    .line 765
    :cond_4a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    if-eqz v1, :cond_56

    if-nez v0, :cond_56

    .line 766
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    invoke-interface {v0, p0, v3}, Lcom/google/android/maps/driveabout/vector/by;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z

    move-result v0

    .line 769
    :cond_56
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v8

    .line 770
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 771
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 773
    add-int/lit8 v1, v1, -0x1

    move v6, v0

    :goto_68
    if-nez v6, :cond_98

    if-ltz v1, :cond_98

    .line 774
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 775
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->l_()Z

    move-result v9

    if-eqz v9, :cond_82

    .line 776
    check-cast v0, Lcom/google/android/maps/driveabout/vector/d;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v6

    .line 773
    :goto_7e
    add-int/lit8 v1, v1, -0x1

    move v6, v0

    goto :goto_68

    .line 777
    :cond_82
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_d6

    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v9}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v9

    if-eq v0, v9, :cond_d6

    invoke-virtual {v0, p1, p2, v3, v4}, Lcom/google/android/maps/driveabout/vector/D;->a_(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_d6

    move v0, v7

    .line 784
    goto :goto_7e

    .line 787
    :cond_98
    if-nez v6, :cond_be

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_be

    .line 788
    if-eqz v2, :cond_b0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->k:LC/b;

    invoke-virtual {v0, v1}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b3

    .line 800
    :cond_b0
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setShouldUpdateFeatureCluster(Z)V

    .line 802
    :cond_b3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/s;->a(FFLo/T;LC/a;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_be

    move v6, v7

    .line 808
    :cond_be
    if-nez v6, :cond_c9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_c9

    .line 809
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, v3}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)V

    .line 811
    :cond_c9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->k:LC/b;

    .line 813
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto/16 :goto_5

    :cond_d6
    move v0, v6

    goto :goto_7e
.end method

.method public d()V
    .registers 2

    .prologue
    .line 957
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_f

    .line 958
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->k_()V

    .line 959
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i:Lcom/google/android/maps/driveabout/vector/D;

    .line 960
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    .line 962
    :cond_f
    return-void
.end method

.method public d(FF)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 711
    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    .line 715
    invoke-virtual {v0, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    .line 718
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v1, v0}, Lcom/google/android/maps/driveabout/vector/x;->b(FFLo/T;LC/a;)Z

    move-result v0

    .line 720
    :goto_31
    return v0

    :cond_32
    const/4 v0, 0x0

    goto :goto_31
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 541
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 542
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    if-eqz v0, :cond_f

    .line 543
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    .line 544
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->d()V

    .line 546
    :cond_f
    return-void
.end method

.method public e(FF)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    if-nez v0, :cond_5

    .line 878
    :goto_4
    return-void

    .line 840
    :cond_5
    new-instance v3, LC/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, LC/a;-><init>(LC/b;IIF)V

    .line 844
    invoke-virtual {v3, p1, p2}, LC/a;->d(FF)Lo/T;

    move-result-object v4

    .line 848
    const/4 v0, 0x0

    .line 851
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v1

    if-eqz v1, :cond_33

    .line 852
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v4, v3}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLo/T;LC/a;)Z

    move-result v0

    .line 857
    :cond_33
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    if-eqz v1, :cond_77

    if-nez v0, :cond_77

    .line 858
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    invoke-interface {v0, p0, v4}, Lcom/google/android/maps/driveabout/vector/by;->b(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z

    move-result v0

    move v1, v0

    .line 861
    :goto_40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->g()Ljava/util/ArrayList;

    move-result-object v5

    .line 862
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 863
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_4d
    if-ltz v2, :cond_64

    .line 864
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 866
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/aK;->m()Lcom/google/android/maps/driveabout/vector/x;

    move-result-object v6

    if-eq v0, v6, :cond_73

    invoke-virtual {v0, p1, p2, v4, v3}, Lcom/google/android/maps/driveabout/vector/D;->c(FFLo/T;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 868
    const/4 v1, 0x1

    .line 873
    :cond_64
    if-nez v1, :cond_6f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_6f

    .line 874
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, v4}, Lcom/google/android/maps/driveabout/vector/bz;->b(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)V

    .line 877
    :cond_6f
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    goto :goto_4

    .line 863
    :cond_73
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4d

    :cond_77
    move v1, v0

    goto :goto_40
.end method

.method public f()V
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    .line 374
    return-void
.end method

.method public f(FF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    if-eqz v0, :cond_9

    .line 698
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/bz;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;FF)V

    .line 700
    :cond_9
    return-void
.end method

.method public g()LS/b;
    .registers 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->n()LS/b;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 2

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    return-object v0
.end method

.method public h()V
    .registers 3

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Ljava/util/List;)V

    .line 470
    return-void
.end method

.method public i()V
    .registers 7

    .prologue
    .line 518
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 519
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-gez v2, :cond_17

    .line 520
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aK;->c(Z)V

    .line 524
    :goto_14
    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l:J

    .line 525
    return-void

    .line 522
    :cond_17
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aK;->c(Z)V

    goto :goto_14
.end method

.method public isOpaque()Z
    .registers 2

    .prologue
    .line 480
    const/4 v0, 0x1

    return v0
.end method

.method public j()V
    .registers 2

    .prologue
    .line 533
    invoke-static {}, Lcom/google/googlenav/android/E;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 534
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->c()V

    .line 535
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->m:Z

    .line 537
    :cond_e
    return-void
.end method

.method public k()Lcom/google/android/maps/driveabout/vector/q;
    .registers 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->j()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    return-object v0
.end method

.method public l()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 599
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    monitor-enter v1

    .line 600
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    .line 601
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 602
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public m()V
    .registers 3

    .prologue
    .line 616
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 617
    return-void
.end method

.method public n()LC/a;
    .registers 6

    .prologue
    .line 625
    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    return-object v0
.end method

.method public n_()V
    .registers 2

    .prologue
    .line 507
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->n_()V

    .line 508
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->f()V

    .line 509
    return-void
.end method

.method public o()Lo/aQ;
    .registers 6

    .prologue
    .line 634
    new-instance v0, LC/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    invoke-virtual {v0}, LC/a;->B()Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method public o_()V
    .registers 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->b()V

    .line 514
    return-void
.end method

.method public onFinishInflate()V
    .registers 1

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->onFinishInflate()V

    .line 212
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->B()V

    .line 213
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 640
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    invoke-virtual {v0, p1}, LaD/g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public onWindowFocusChanged(Z)V
    .registers 4
    .parameter

    .prologue
    .line 474
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/GmmGLSurfaceView;->onWindowFocusChanged(Z)V

    .line 475
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-nez p1, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Z)V

    .line 476
    return-void

    .line 475
    :cond_c
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public p()V
    .registers 2

    .prologue
    .line 980
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j:Z

    .line 981
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->k()V

    .line 982
    return-void
.end method

.method public p_()Lcom/google/android/maps/driveabout/vector/bk;
    .registers 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    return-object v0
.end method

.method public q()LF/H;
    .registers 2

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->l()LF/H;

    move-result-object v0

    return-object v0
.end method

.method public r()V
    .registers 2

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    if-eqz v0, :cond_9

    .line 1032
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->q()V

    .line 1034
    :cond_9
    return-void
.end method

.method public s()F
    .registers 2

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public setAllowLongPressGesture(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->f(Z)V

    .line 1079
    return-void
.end method

.method public setAllowRotateGesture(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->e(Z)V

    .line 1089
    return-void
.end method

.method public setAllowScroll(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->a(Z)V

    .line 1052
    return-void
.end method

.method public setAllowSingleTapGesture(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->g(Z)V

    .line 1084
    return-void
.end method

.method public setAllowTiltGesture(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->d(Z)V

    .line 1074
    return-void
.end method

.method public setAllowZoomGestures(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->b(Z)V

    .line 1057
    return-void
.end method

.method public setBaseDistancePenaltyFactorForLabelOverlay(I)V
    .registers 3
    .parameter

    .prologue
    .line 999
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(I)V

    .line 1000
    return-void
.end method

.method public setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V
    .registers 3
    .parameter

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 454
    return-void
.end method

.method public setController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .registers 4
    .parameter

    .prologue
    .line 391
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    .line 392
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/aU;)V

    .line 393
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lw/c;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/b;)V

    .line 395
    return-void
.end method

.method public setCopyrightPadding(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aK;->a(II)V

    .line 584
    return-void
.end method

.method public setDefaultLabelTheme(LG/a;)V
    .registers 2
    .parameter

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n:LG/a;

    .line 1110
    return-void
.end method

.method public setDoubleTapZoomsAboutCenter(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bt;->c(Z)V

    .line 1069
    return-void
.end method

.method public setDrawMode(Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 3
    .parameter

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 560
    return-void
.end method

.method public setEventBus(LZ/a;)V
    .registers 3
    .parameter

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LZ/a;)V

    .line 1181
    return-void
.end method

.method public setImportantLabelFeatures(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Ljava/util/List;)V

    .line 463
    return-void
.end method

.method public setInterceptingOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/by;)V
    .registers 2
    .parameter

    .prologue
    .line 398
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g:Lcom/google/android/maps/driveabout/vector/by;

    .line 399
    return-void
.end method

.method public setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 3
    .parameter

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 992
    return-void
.end method

.method public setLabelTheme(LG/a;)V
    .registers 3
    .parameter

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LG/a;)V

    .line 576
    return-void
.end method

.method public setModelChanged()V
    .registers 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->e()V

    .line 485
    return-void
.end method

.method public setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 402
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f:Lcom/google/android/maps/driveabout/vector/bz;

    .line 403
    return-void
.end method

.method public setShouldUpdateFeatureCluster(Z)V
    .registers 3
    .parameter

    .prologue
    .line 969
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b:Lcom/google/android/maps/driveabout/vector/s;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/s;->a(Z)V

    .line 970
    return-void
.end method

.method public t()V
    .registers 2

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bt;->a()V

    .line 1043
    return-void
.end method

.method public u()V
    .registers 2

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->h:Lcom/google/android/maps/driveabout/vector/bt;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bt;->b()V

    .line 1047
    return-void
.end method

.method public v()Lcom/google/android/maps/driveabout/vector/aK;
    .registers 2

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    return-object v0
.end method

.method public w()V
    .registers 2

    .prologue
    .line 1101
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTheme(LG/a;)V

    .line 1102
    return-void
.end method

.method public x()LG/a;
    .registers 2

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n:LG/a;

    return-object v0
.end method

.method public y()LaD/g;
    .registers 2

    .prologue
    .line 1114
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 1115
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->e:LaD/g;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 2

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/VectorMapView;->d:Lcom/google/android/maps/driveabout/vector/aK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    return-object v0
.end method
