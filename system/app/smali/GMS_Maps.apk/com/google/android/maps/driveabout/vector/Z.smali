.class public Lcom/google/android/maps/driveabout/vector/z;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final a:Lo/T;

.field private static final b:Lo/T;


# instance fields
.field private final c:Lo/X;

.field private d:Lo/X;

.field private e:Lo/X;

.field private final f:Ljava/util/List;

.field private final g:Lo/r;

.field private h:Lo/ad;

.field private i:F

.field private j:F

.field private final k:LE/o;

.field private final l:LE/d;

.field private final m:LE/i;

.field private n:F

.field private o:I

.field private p:Z

.field private final q:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 62
    new-instance v0, Lo/T;

    const/high16 v1, 0x4000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    .line 63
    new-instance v0, Lo/T;

    const/high16 v1, -0x4000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    return-void
.end method

.method public constructor <init>(Lo/X;FILo/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    .line 157
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/z;->n:F

    .line 158
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    .line 159
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    .line 160
    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    .line 166
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 167
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-static {v0}, Lx/h;->a(Ljava/util/List;)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    .line 175
    new-instance v1, LE/o;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    invoke-direct {v1, v2}, LE/o;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    .line 176
    new-instance v1, LE/i;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->q:I

    invoke-direct {v1, v2}, LE/i;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    .line 177
    new-instance v1, LE/d;

    invoke-static {v0}, Lx/h;->b(Ljava/util/List;)I

    move-result v0

    invoke-direct {v1, v0}, LE/d;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    .line 179
    return-void
.end method

.method static a(Lo/X;)Ljava/util/List;
    .registers 14
    .parameter

    .prologue
    .line 442
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_12

    .line 443
    const/4 v0, 0x1

    new-array v0, v0, [Lo/X;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 488
    :goto_11
    return-object v0

    .line 445
    :cond_12
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 446
    new-instance v2, Lo/Z;

    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    invoke-direct {v2, v0}, Lo/Z;-><init>(I)V

    .line 447
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    .line 448
    invoke-virtual {v2, v3}, Lo/Z;->a(Lo/T;)Z

    .line 449
    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    .line 450
    const/4 v0, 0x1

    :goto_2d
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v5

    if-ge v0, v5, :cond_100

    .line 451
    invoke-virtual {p0, v0, v4}, Lo/X;->a(ILo/T;)V

    .line 453
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    if-ge v5, v6, :cond_9f

    .line 454
    invoke-virtual {v3}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    sub-int/2addr v5, v6

    .line 455
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x4000

    add-int/2addr v6, v7

    .line 456
    if-ge v6, v5, :cond_96

    .line 457
    const/high16 v5, 0x2000

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v5, v7

    .line 458
    invoke-virtual {v4}, Lo/T;->g()I

    move-result v7

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    sub-int/2addr v7, v8

    .line 459
    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    .line 461
    new-instance v6, Lo/T;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    .line 462
    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 463
    invoke-virtual {v2}, Lo/Z;->c()V

    .line 464
    new-instance v6, Lo/T;

    const/high16 v7, -0x2000

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    .line 481
    :cond_96
    :goto_96
    invoke-virtual {v2, v4}, Lo/Z;->a(Lo/T;)Z

    .line 482
    invoke-virtual {v3, v4}, Lo/T;->b(Lo/T;)V

    .line 450
    add-int/lit8 v0, v0, 0x1

    goto :goto_2d

    .line 467
    :cond_9f
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    if-le v5, v6, :cond_96

    .line 468
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    sub-int/2addr v5, v6

    .line 469
    invoke-virtual {v3}, Lo/T;->f()I

    move-result v6

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v7

    sub-int/2addr v6, v7

    const/high16 v7, 0x4000

    add-int/2addr v6, v7

    .line 470
    if-ge v6, v5, :cond_96

    .line 471
    invoke-virtual {v3}, Lo/T;->f()I

    move-result v5

    const/high16 v7, 0x2000

    add-int/2addr v5, v7

    .line 472
    invoke-virtual {v4}, Lo/T;->g()I

    move-result v7

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    sub-int/2addr v7, v8

    .line 473
    invoke-virtual {v3}, Lo/T;->g()I

    move-result v8

    int-to-double v9, v7

    int-to-double v11, v5

    mul-double/2addr v9, v11

    int-to-double v5, v6

    div-double v5, v9, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    add-int/2addr v5, v8

    .line 475
    new-instance v6, Lo/T;

    const/high16 v7, -0x2000

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    .line 476
    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 477
    invoke-virtual {v2}, Lo/Z;->c()V

    .line 478
    new-instance v6, Lo/T;

    const v7, 0x1fffffff

    invoke-direct {v6, v7, v5}, Lo/T;-><init>(II)V

    invoke-virtual {v2, v6}, Lo/Z;->a(Lo/T;)Z

    goto :goto_96

    .line 484
    :cond_100
    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v0

    .line 485
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_10e

    .line 486
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10e
    move-object v0, v1

    .line 488
    goto/16 :goto_11
.end method

.method private a(F)Lo/X;
    .registers 3
    .parameter

    .prologue
    .line 527
    const/high16 v0, 0x4120

    cmpl-float v0, p1, v0

    if-lez v0, :cond_9

    .line 528
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    .line 534
    :goto_8
    return-object v0

    .line 530
    :cond_9
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/z;->h()V

    .line 531
    const/high16 v0, 0x40c0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_15

    .line 532
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    goto :goto_8

    .line 534
    :cond_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->e:Lo/X;

    goto :goto_8
.end method

.method private a(LD/a;LC/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->a(LD/a;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    invoke-virtual {v0, p1}, LE/d;->a(LD/a;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 497
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 498
    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(Lo/X;LC/a;)V

    goto :goto_15

    .line 500
    :cond_25
    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    .line 501
    return-void
.end method

.method private a(LD/a;Lcom/google/android/maps/driveabout/vector/r;LC/a;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/high16 v5, 0x1

    .line 274
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 278
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/z;->e()Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v2

    .line 279
    if-eqz v2, :cond_1c

    .line 280
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    invoke-interface {v2, p1, p3, p2, v0}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    .line 284
    :cond_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v0

    .line 285
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    .line 290
    invoke-static {p1, p3, v0, v3}, Lcom/google/android/maps/driveabout/vector/be;->b(LD/a;LC/a;Lo/T;F)V

    .line 293
    invoke-virtual {p1}, LD/a;->p()V

    .line 294
    const/4 v0, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 298
    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 299
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/z;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 300
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    .line 307
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5, v5, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 310
    if-eqz v2, :cond_6a

    .line 311
    invoke-interface {v2, p1, p2}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 313
    :cond_6a
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0xff00

    .line 319
    monitor-enter p0

    .line 320
    :try_start_4
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shr-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v4

    .line 321
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shr-int/lit8 v1, v1, 0x8

    and-int/2addr v1, v4

    .line 322
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    and-int/2addr v2, v4

    .line 323
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->o:I

    shl-int/lit8 v3, v3, 0x8

    and-int/2addr v3, v4

    .line 324
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    .line 325
    invoke-interface {p1, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 326
    return-void

    .line 324
    :catchall_1b
    move-exception v0

    :try_start_1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method private a(Lo/X;LC/a;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    .line 507
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v5

    .line 515
    monitor-enter p0

    .line 516
    :try_start_d
    invoke-virtual {p2}, LC/a;->y()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/z;->n:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    const/high16 v1, 0x40e0

    div-float/2addr v0, v1

    const/high16 v1, 0x4100

    mul-float v2, v0, v1

    .line 517
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_d .. :try_end_1f} :catchall_32

    .line 520
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v0

    const/4 v6, 0x0

    const/high16 v7, 0x1

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    iget-object v10, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v10}, Lx/h;->a(Lo/X;FFLo/T;IIILE/q;LE/e;LE/k;)V

    .line 523
    return-void

    .line 517
    :catchall_32
    move-exception v0

    :try_start_33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    throw v0
.end method

.method private a(LC/a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa0

    .line 333
    monitor-enter p0

    .line 334
    :try_start_5
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    if-eqz v2, :cond_e

    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    .line 336
    monitor-exit p0

    .line 340
    :goto_d
    return v1

    .line 338
    :cond_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_24

    .line 339
    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    .line 340
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_21

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/z;->j:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_22

    :cond_21
    move v0, v1

    :cond_22
    move v1, v0

    goto :goto_d

    .line 338
    :catchall_24
    move-exception v0

    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    throw v0
.end method

.method private static b(I)I
    .registers 3
    .parameter

    .prologue
    .line 549
    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method private b(LC/a;)Z
    .registers 6
    .parameter

    .prologue
    const/high16 v3, 0x4000

    const/4 v0, 0x1

    .line 348
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    if-nez v1, :cond_8

    .line 362
    :cond_7
    :goto_7
    return v0

    .line 353
    :cond_8
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    .line 354
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_7

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    div-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_7

    .line 359
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->c()Lo/ae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/ad;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 362
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private c(LC/a;)V
    .registers 11
    .parameter

    .prologue
    const/high16 v5, 0x2000

    .line 370
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 371
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/z;->a(F)Lo/X;

    move-result-object v4

    .line 375
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->b()Lo/ad;

    move-result-object v2

    .line 377
    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v0

    .line 378
    invoke-virtual {v2}, Lo/ad;->h()I

    move-result v1

    .line 379
    const v3, 0x71c71c7

    .line 381
    if-gt v0, v3, :cond_26

    if-le v1, v3, :cond_c1

    .line 383
    :cond_26
    new-instance v1, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    sub-int/2addr v0, v5

    const/high16 v3, -0x4000

    invoke-direct {v1, v0, v3}, Lo/T;-><init>(II)V

    .line 384
    new-instance v0, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, v5

    add-int/lit8 v2, v2, -0x1

    const v3, 0x3fffffff

    invoke-direct {v0, v2, v3}, Lo/T;-><init>(II)V

    move-object v2, v0

    move-object v3, v1

    .line 390
    :goto_4b
    new-instance v0, Lo/ad;

    invoke-direct {v0, v3, v2}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    .line 394
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 395
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v0

    .line 396
    int-to-float v0, v0

    invoke-virtual {v4, v0}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/z;->a(Lo/X;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 397
    new-instance v1, Lo/h;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    invoke-direct {v1, v6}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v1, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 398
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 399
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 411
    new-instance v1, Lo/ad;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v3, v6}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v6

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v2, v7}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    .line 413
    new-instance v6, Lo/h;

    invoke-direct {v6, v1}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v6, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 414
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_a9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_de

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/X;

    .line 416
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    sget-object v8, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v1, v8}, Lo/X;->b(Lo/T;)Lo/X;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a9

    .line 386
    :cond_c1
    new-instance v3, Lo/T;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lo/T;-><init>(II)V

    .line 387
    invoke-virtual {v2}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    .line 388
    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    goto/16 :goto_4b

    .line 418
    :cond_de
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 420
    new-instance v1, Lo/ad;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v3, v6}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v6

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->b:Lo/T;

    invoke-virtual {v2, v7}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    .line 422
    new-instance v6, Lo/h;

    invoke-direct {v6, v1}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v6, v0, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 423
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_fe
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_116

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 425
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    sget-object v7, Lcom/google/android/maps/driveabout/vector/z;->a:Lo/T;

    invoke-virtual {v0, v7}, Lo/X;->b(Lo/T;)Lo/X;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_fe

    .line 427
    :cond_116
    invoke-interface {v5}, Ljava/util/List;->clear()V

    goto/16 :goto_6e

    .line 430
    :cond_11b
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/z;->i:F

    .line 431
    return-void
.end method

.method private e()Lcom/google/android/maps/driveabout/vector/aJ;
    .registers 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-nez v0, :cond_1d

    .line 267
    :cond_1b
    const/4 v0, 0x0

    .line 269
    :goto_1c
    return-object v0

    :cond_1d
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/z;->g:Lo/r;

    invoke-virtual {v0, v1}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v0

    goto :goto_1c
.end method

.method private h()V
    .registers 3

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    if-nez v0, :cond_21

    .line 542
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->c:Lo/X;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    .line 543
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->d:Lo/X;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/z;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->e:Lo/X;

    .line 545
    :cond_21
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->l:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->m:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 262
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_2b

    .line 225
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->h:Lo/ad;

    if-nez v0, :cond_d

    .line 227
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/z;->c(LC/a;)V

    .line 229
    :cond_d
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 230
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;)V

    .line 232
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/z;->k:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_2b

    .line 233
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 234
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 235
    invoke-direct {p0, p1, p3, p2}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;LC/a;)V

    .line 236
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 239
    :cond_2b
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 189
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->c(LC/a;)V

    .line 191
    monitor-enter p0

    .line 192
    const/4 v0, 0x1

    :try_start_c
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/z;->p:Z

    .line 193
    monitor-exit p0

    .line 198
    :cond_f
    return v1

    .line 193
    :catchall_10
    move-exception v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_10

    throw v0
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 243
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;)V

    .line 244
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
