.class public Lcom/google/android/maps/driveabout/vector/a;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static i:LE/o;

.field private static j:LE/d;

.field private static k:LE/o;

.field private static l:LE/d;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lo/T;

.field private c:Lo/r;

.field private d:Lo/ad;

.field private e:I

.field private f:F

.field private g:I

.field private h:I

.field private m:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x64

    .line 243
    new-instance v0, LE/o;

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    .line 244
    new-instance v0, LE/d;

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    .line 245
    new-instance v0, LE/o;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    .line 246
    new-instance v0, LE/d;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    .line 247
    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    invoke-static {v0, v1}, Lx/r;->a(LE/q;LE/e;)V

    .line 248
    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    invoke-static {v0, v1}, Lx/r;->b(LE/q;LE/e;)V

    .line 249
    return-void
.end method

.method public constructor <init>(Lo/T;IIILo/r;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 264
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    .line 265
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    .line 266
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/a;->e()V

    .line 267
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    .line 268
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    .line 269
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    .line 270
    iput-object p5, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    .line 271
    iput-object p6, p0, Lcom/google/android/maps/driveabout/vector/a;->a:Ljava/lang/String;

    .line 272
    return-void
.end method

.method private static a(ILo/T;)F
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 279
    if-eqz p1, :cond_4

    if-nez p0, :cond_6

    .line 280
    :cond_4
    const/4 v0, 0x0

    .line 282
    :goto_5
    return v0

    :cond_6
    int-to-float v0, p0

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    goto :goto_5
.end method

.method private a(LD/a;F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 378
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 381
    invoke-interface {v0, p2, p2, p2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 383
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 386
    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->k:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 387
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 388
    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->l:LE/d;

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v2}, LE/d;->a(LD/a;I)V

    .line 392
    sget-object v1, Lcom/google/android/maps/driveabout/vector/a;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 393
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 394
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    .line 395
    sget-object v0, Lcom/google/android/maps/driveabout/vector/a;->j:LE/d;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    .line 396
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 451
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(ILo/T;)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    .line 452
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-eqz v0, :cond_1d

    .line 453
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->d:Lo/ad;

    .line 456
    :cond_1d
    return-void
.end method


# virtual methods
.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 442
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->m:F

    .line 443
    return-void
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 345
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 349
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-eqz v0, :cond_21

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_21

    .line 353
    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->d:Lo/ad;

    invoke-virtual {v0, v1}, Lo/aR;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 375
    :cond_21
    :goto_21
    return-void

    .line 357
    :cond_22
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 358
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 360
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    if-eqz v1, :cond_45

    .line 361
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    invoke-virtual {v1, v2}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v1

    .line 363
    if-eqz v1, :cond_45

    .line 364
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-virtual {v1, p2, v3}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lo/T;->b(I)V

    .line 369
    :cond_45
    invoke-virtual {p2}, LC/a;->y()F

    move-result v1

    .line 370
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    invoke-static {p1, p2, v2, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 372
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/a;->f:F

    div-float v1, v2, v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;F)V

    .line 374
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_21
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 459
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->a:Ljava/lang/String;

    .line 460
    return-void
.end method

.method public a(Lo/T;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    if-ne p1, v0, :cond_8

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    if-eq v0, p2, :cond_f

    .line 403
    :cond_8
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->b:Lo/T;

    .line 404
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/a;->e:I

    .line 405
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/a;->e()V

    .line 412
    :cond_f
    return-void
.end method

.method public a(Lo/r;)V
    .registers 2
    .parameter

    .prologue
    .line 415
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/a;->c:Lo/r;

    .line 416
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 424
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->g:I

    .line 425
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 299
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .registers 2
    .parameter

    .prologue
    .line 433
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/a;->h:I

    .line 434
    return-void
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 304
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;)V

    .line 305
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 289
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
