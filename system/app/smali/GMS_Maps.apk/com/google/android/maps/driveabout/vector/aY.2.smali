.class Lcom/google/android/maps/driveabout/vector/aY;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/maps/driveabout/vector/aX;

.field private final c:Lo/ao;

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aY;->a:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aY;->b:Lcom/google/android/maps/driveabout/vector/aX;

    .line 77
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    .line 78
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/aY;->d:F

    .line 79
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/aY;->e:I

    .line 80
    iput p6, p0, Lcom/google/android/maps/driveabout/vector/aY;->f:I

    .line 81
    iput p7, p0, Lcom/google/android/maps/driveabout/vector/aY;->g:I

    .line 82
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    if-ne p1, p0, :cond_5

    .line 113
    :cond_4
    :goto_4
    return v0

    .line 102
    :cond_5
    instance-of v2, p1, Lcom/google/android/maps/driveabout/vector/aY;

    if-eqz v2, :cond_4b

    .line 103
    check-cast p1, Lcom/google/android/maps/driveabout/vector/aY;

    .line 104
    iget v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->d:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_49

    iget v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->e:I

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->e:I

    if-ne v2, v3, :cond_49

    iget v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->f:I

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->f:I

    if-ne v2, v3, :cond_49

    iget v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->g:I

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->g:I

    if-ne v2, v3, :cond_49

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->b:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->b:Lcom/google/android/maps/driveabout/vector/aX;

    if-ne v2, v3, :cond_49

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    if-eq v2, v3, :cond_3f

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    if-eqz v2, :cond_49

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    invoke-virtual {v2, v3}, Lo/ao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_49

    :cond_3f
    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aY;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aY;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_49
    move v0, v1

    goto :goto_4

    :cond_4b
    move v0, v1

    .line 113
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 86
    .line 87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aY;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->b:Lcom/google/android/maps/driveabout/vector/aX;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    if-eqz v1, :cond_1e

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->c:Lo/ao;

    invoke-virtual {v1}, Lo/ao;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_1e
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->e:I

    add-int/2addr v0, v1

    .line 94
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->f:I

    add-int/2addr v0, v1

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aY;->g:I

    add-int/2addr v0, v1

    .line 96
    return v0
.end method
