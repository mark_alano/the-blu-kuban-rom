.class public Lcom/google/android/maps/driveabout/vector/az;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:LD/b;

.field private final c:Lh/k;

.field private d:Lo/T;

.field private e:F

.field private final f:I

.field private final g:I

.field private final h:I

.field private volatile i:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 4
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    .line 39
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    .line 40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    .line 41
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->g:I

    .line 42
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->h:I

    .line 43
    new-instance v0, Lh/k;

    invoke-direct {v0}, Lh/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->c:Lh/k;

    .line 44
    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    if-nez v0, :cond_7

    .line 110
    :cond_6
    :goto_6
    return-void

    .line 79
    :cond_7
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    if-eqz v0, :cond_6

    .line 84
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    if-nez v0, :cond_2b

    .line 85
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    .line 86
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v0, v5}, LD/b;->c(Z)V

    .line 87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->a:Landroid/content/res/Resources;

    const v2, 0x7f020174

    invoke-virtual {v0, v1, v2}, LD/b;->a(Landroid/content/res/Resources;I)V

    .line 91
    :cond_2b
    invoke-virtual {p1}, LD/a;->p()V

    .line 92
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 93
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 95
    const/16 v1, 0x303

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 96
    iget-object v1, p1, LD/a;->d:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 99
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 100
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->e:F

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 101
    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    .line 102
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->c:Lh/k;

    invoke-virtual {v1, p1}, Lh/k;->a(LD/a;)F

    move-result v1

    const/high16 v2, 0x3f80

    invoke-interface {v0, v1, v4, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 105
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 106
    iget-object v1, p1, LD/a;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 107
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 109
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_6
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    .line 114
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->g:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/az;->h:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    .line 57
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    if-eqz v0, :cond_2b

    .line 58
    const/4 v0, 0x0

    .line 59
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/az;->f:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/az;->d:Lo/T;

    invoke-virtual {p1, v2, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    invoke-virtual {p1, v1, v0}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/az;->e:F

    .line 62
    :cond_2b
    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    if-eqz v0, :cond_c

    .line 68
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/az;->b:LD/b;

    .line 71
    :cond_c
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/az;->i:Z

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->z:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
