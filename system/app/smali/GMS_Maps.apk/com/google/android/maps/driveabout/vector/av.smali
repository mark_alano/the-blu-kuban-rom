.class public Lcom/google/android/maps/driveabout/vector/av;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Set;

.field private b:Ljava/util/Set;

.field private c:Ljava/util/Set;

.field private d:Lcom/google/googlenav/intersectionexplorer/c;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    .line 61
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Lcom/google/android/maps/driveabout/vector/av;)V

    .line 62
    return-void
.end method

.method private static a(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/android/maps/driveabout/vector/z;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    .line 141
    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    .line 144
    new-instance v2, Lo/Z;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lo/Z;-><init>(I)V

    .line 145
    invoke-virtual {v2, v0}, Lo/Z;->a(Lo/T;)Z

    .line 146
    invoke-virtual {v2, v1}, Lo/Z;->a(Lo/T;)Z

    .line 148
    new-instance v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v1

    const/high16 v2, 0x3f80

    const v3, -0xffff01

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    return-object v0
.end method

.method private declared-synchronized b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 168
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    if-eqz v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_5b

    if-nez v1, :cond_c

    .line 187
    :goto_a
    monitor-exit p0

    return v0

    .line 172
    :cond_c
    :try_start_c
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    .line 173
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->c:Ljava/util/Set;

    .line 174
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 175
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 176
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_35
    :goto_35
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    .line 178
    new-instance v4, Lcom/google/android/maps/driveabout/vector/aw;

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aw;-><init>(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)V

    .line 179
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/av;->c:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 180
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/av;->a(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/android/maps/driveabout/vector/z;

    move-result-object v1

    .line 181
    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 182
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5a
    .catchall {:try_start_c .. :try_end_5a} :catchall_5b

    goto :goto_35

    .line 168
    :catchall_5b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 186
    :cond_5e
    const/4 v0, 0x0

    :try_start_5f
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z
    :try_end_61
    .catchall {:try_start_5f .. :try_end_61} :catchall_5b

    .line 187
    const/4 v0, 0x1

    goto :goto_a
.end method


# virtual methods
.method public declared-synchronized a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    monitor-enter p0

    :try_start_1
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_3a

    move-result v0

    if-lez v0, :cond_9

    .line 97
    :cond_7
    :goto_7
    monitor-exit p0

    return-void

    .line 73
    :cond_9
    :try_start_9
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    if-eqz v0, :cond_1a

    .line 75
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->g()Z

    move-result v0

    if-nez v0, :cond_3d

    const/4 v0, 0x1

    :goto_18
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    .line 79
    :cond_1a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    if-eqz v0, :cond_7

    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/av;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)Z

    move-result v0

    if-nez v0, :cond_55

    .line 85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 86
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_39
    .catchall {:try_start_9 .. :try_end_39} :catchall_3a

    goto :goto_2a

    .line 66
    :catchall_3a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75
    :cond_3d
    const/4 v0, 0x0

    goto :goto_18

    .line 88
    :cond_3f
    :try_start_3f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_45
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    .line 89
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_45

    .line 94
    :cond_55
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_7

    .line 95
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_5e
    .catchall {:try_start_3f .. :try_end_5e} :catchall_3a

    goto :goto_7
.end method

.method public declared-synchronized a(Lcom/google/googlenav/intersectionexplorer/c;)V
    .registers 4
    .parameter

    .prologue
    .line 123
    monitor-enter p0

    if-nez p1, :cond_5

    .line 133
    :goto_3
    monitor-exit p0

    return-void

    .line 128
    :cond_5
    :try_start_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_f

    .line 129
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Z)V

    .line 131
    :cond_f
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    .line 132
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Z)V
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_18

    goto :goto_3

    .line 123
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 108
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z

    .line 109
    if-nez p1, :cond_11

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    if-eqz v0, :cond_11

    .line 110
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_18

    .line 117
    :goto_f
    monitor-exit p0

    return-void

    .line 116
    :cond_11
    :try_start_11
    invoke-static {p1}, Lcom/google/common/collect/dA;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_18

    goto :goto_f

    .line 108
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .registers 2

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    .line 157
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->r:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
