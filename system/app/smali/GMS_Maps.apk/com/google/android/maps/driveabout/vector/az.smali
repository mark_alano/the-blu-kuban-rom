.class public Lcom/google/android/maps/driveabout/vector/aZ;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final M:Lg/c;

.field private static final d:LR/a;

.field private static final e:LR/a;


# instance fields
.field private volatile A:Z

.field private final B:Ljava/util/Set;

.field private final C:LR/h;

.field private final D:Lo/T;

.field private E:LC/b;

.field private F:J

.field private G:J

.field private H:J

.field private I:Z

.field private final J:Z

.field private final K:Z

.field private final L:Lg/c;

.field private N:I

.field private O:J

.field private final P:Ljava/util/Set;

.field private final Q:LA/a;

.field private R:Ljava/lang/ref/WeakReference;

.field protected volatile a:Lcom/google/android/maps/driveabout/vector/aU;

.field protected volatile b:Z

.field protected final c:Ly/b;

.field private final f:I

.field private final g:I

.field private final h:Lcom/google/android/maps/driveabout/vector/E;

.field private i:Z

.field private final j:I

.field private final k:I

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:LA/c;

.field private final q:Lu/d;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:[I

.field private final u:Ljava/util/ArrayList;

.field private final v:[I

.field private final w:Lcom/google/android/maps/driveabout/vector/bc;

.field private x:Lg/a;

.field private y:Lk/f;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 80
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    invoke-static {v0}, LR/a;->a([I)LR/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    .line 84
    sget-object v0, LF/Y;->a:LR/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    invoke-static {v0, v1}, LR/a;->a(LR/a;LR/a;)LR/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->e:LR/a;

    .line 238
    new-instance v0, Lg/b;

    invoke-direct {v0}, Lg/b;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    return-void
.end method

.method constructor <init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 735
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 172
    new-instance v1, Lcom/google/android/maps/driveabout/vector/bc;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/vector/bc;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    .line 193
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    .line 201
    new-instance v1, LR/h;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, LR/h;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->C:LR/h;

    .line 205
    new-instance v1, Lo/T;

    invoke-direct {v1}, Lo/T;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    .line 211
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    .line 214
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    .line 217
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    .line 230
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    .line 257
    invoke-static {}, Lcom/google/common/collect/dA;->d()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    .line 736
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    .line 737
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    .line 738
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    .line 739
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    .line 740
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/aZ;->g:I

    .line 741
    iput-object p7, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    .line 742
    iput p8, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    .line 743
    iput p9, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    .line 744
    iput-boolean p10, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    .line 745
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->l:Z

    .line 746
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->m:Z

    .line 747
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->n:Z

    .line 748
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    .line 749
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    .line 750
    new-instance v1, LA/a;

    invoke-direct {v1}, LA/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    .line 751
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v1, v2}, Lu/d;->a(LA/b;)V

    .line 755
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    if-eqz v1, :cond_ac

    invoke-virtual {p1}, LA/c;->h()Z

    move-result v1

    if-nez v1, :cond_ac

    .line 756
    new-instance v1, Ly/b;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aZ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ly/b;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    .line 761
    :goto_83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    .line 762
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    .line 763
    new-array v1, p4, [I

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    .line 764
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    .line 765
    const/4 v1, 0x0

    :goto_9d
    if-ge v1, p6, :cond_b0

    .line 766
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 765
    add-int/lit8 v1, v1, 0x1

    goto :goto_9d

    .line 758
    :cond_ac
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    goto :goto_83

    .line 768
    :cond_b0
    add-int/lit8 v1, p6, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    .line 770
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    .line 771
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0xfa0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    .line 773
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    new-instance v2, Lcom/google/android/maps/driveabout/vector/ba;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/vector/ba;-><init>(Lcom/google/android/maps/driveabout/vector/aZ;)V

    invoke-virtual {v1, v2}, Lu/d;->a(Lu/i;)V

    .line 798
    return-void
.end method

.method private a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1292
    const/4 v0, 0x0

    .line 1293
    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v1, :cond_27

    move v1, v0

    .line 1294
    :goto_6
    if-ge p3, p4, :cond_21

    .line 1295
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    invoke-interface {v0, p1, p2}, LF/T;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    aput v0, v2, p3

    .line 1296
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    aget v0, v0, p3

    or-int/2addr v0, v1

    .line 1294
    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_6

    .line 1299
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v0, v1, p2}, LA/c;->a(ILcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    .line 1301
    :cond_27
    return v0
.end method

.method public static a(Landroid/content/res/Resources;I)I
    .registers 5
    .parameter
    .parameter

    .prologue
    const v1, 0x64140

    .line 334
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v2

    .line 336
    if-ge v0, v1, :cond_13

    move v0, v1

    .line 339
    :cond_13
    div-int/lit16 v2, p1, 0x100

    int-to-float v2, v2

    .line 340
    mul-float/2addr v2, v2

    .line 341
    mul-int/lit8 v0, v0, 0x18

    div-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v0, v0

    return v0
.end method

.method public static a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 373
    sget-object v0, LA/c;->a:LA/c;

    if-eq p0, v0, :cond_d

    sget-object v0, LA/c;->b:LA/c;

    if-eq p0, v0, :cond_d

    sget-object v0, LA/c;->c:LA/c;

    if-ne p0, v0, :cond_13

    :cond_d
    move v0, v1

    .line 376
    :goto_e
    invoke-static {p0, v0, v1, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    return-object v0

    .line 373
    :cond_13
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static a(LA/c;ZZLandroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 390
    const/16 v1, 0x100

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    .line 391
    mul-int/lit8 v6, v5, 0x2

    .line 394
    if-nez p1, :cond_12

    sget-object v1, LA/c;->j:LA/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_5d

    :cond_12
    const/4 v13, 0x1

    .line 397
    :goto_13
    const/4 v11, 0x0

    .line 398
    const/4 v12, 0x0

    .line 401
    if-nez v13, :cond_23

    sget-object v1, LA/c;->o:LA/c;

    move-object/from16 v0, p0

    if-eq v0, v1, :cond_23

    sget-object v1, LA/c;->p:LA/c;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_5f

    :cond_23
    const/16 v16, 0x1

    .line 404
    :goto_25
    const/4 v4, 0x0

    .line 407
    if-eqz p1, :cond_62

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aZ;->e:LR/a;

    move-object v2, v1

    .line 409
    :goto_2b
    if-eqz v13, :cond_66

    if-eqz v16, :cond_66

    const/4 v1, 0x1

    .line 410
    :goto_30
    new-instance v7, Lu/a;

    const/16 v3, 0x8

    invoke-direct {v7, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    .line 413
    new-instance v3, Lu/d;

    new-instance v1, LB/e;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v4, v2}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v1, v7, v2}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    .line 416
    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    new-instance v4, Lk/e;

    invoke-direct {v4}, Lk/e;-><init>()V

    const/16 v7, 0x8

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->c:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p0

    move v14, v13

    move/from16 v15, p2

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1

    .line 394
    :cond_5d
    const/4 v13, 0x0

    goto :goto_13

    .line 401
    :cond_5f
    const/16 v16, 0x0

    goto :goto_25

    .line 407
    :cond_62
    sget-object v1, LR/a;->a:LR/a;

    move-object v2, v1

    goto :goto_2b

    .line 409
    :cond_66
    const/4 v1, 0x0

    goto :goto_30
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ln/q;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x100

    const/4 v5, 0x4

    .line 564
    invoke-static {p1, v7}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 566
    mul-int/lit8 v3, v0, 0x2

    .line 567
    mul-int/lit8 v4, v3, 0x2

    .line 569
    const/4 v0, 0x1

    .line 570
    const/4 v1, 0x0

    .line 571
    new-instance v2, Lu/a;

    invoke-direct {v2, v5, v4, v1, v0}, Lu/a;-><init>(IIZZ)V

    .line 573
    new-instance v1, Lu/d;

    sget-object v0, LA/c;->n:LA/c;

    invoke-direct {v1, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    .line 575
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aq;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/E;->g:Lcom/google/android/maps/driveabout/vector/E;

    move-object v8, p0

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/maps/driveabout/vector/aq;-><init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;ILandroid/content/Context;Ln/q;)V

    .line 584
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aq;->e()V

    .line 585
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 18
    .parameter

    .prologue
    .line 539
    const/16 v1, 0x100

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    .line 540
    mul-int/lit8 v6, v5, 0x2

    .line 541
    const/4 v11, 0x0

    .line 542
    const/4 v12, 0x0

    .line 543
    const/4 v13, 0x0

    .line 544
    const/4 v14, 0x0

    .line 545
    const/4 v15, 0x0

    .line 546
    const/16 v16, 0x0

    .line 548
    const/4 v1, 0x0

    .line 549
    new-instance v2, Lu/a;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    .line 551
    new-instance v3, Lu/d;

    sget-object v1, LA/c;->m:LA/c;

    invoke-direct {v3, v1, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    .line 553
    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v2, LA/c;->m:LA/c;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->i:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method public static a(Lg/c;LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 20
    .parameter
    .parameter
    .parameter

    .prologue
    .line 433
    const/16 v1, 0x100

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v5

    .line 434
    mul-int/lit8 v6, v5, 0x2

    .line 435
    const/4 v11, 0x0

    .line 436
    const/4 v12, 0x0

    .line 437
    const/4 v13, 0x0

    .line 438
    const/4 v14, 0x0

    .line 439
    const/4 v15, 0x0

    .line 440
    const/16 v16, 0x0

    .line 441
    const/4 v2, 0x0

    .line 443
    const/4 v1, 0x0

    .line 444
    new-instance v4, Lu/a;

    const/4 v3, 0x0

    invoke-direct {v4, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    .line 449
    new-instance v3, Lu/d;

    new-instance v1, LB/e;

    sget-object v7, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2, v7}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->d:LR/a;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v1, v4, v2}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    .line 453
    new-instance v1, Lcom/google/android/maps/driveabout/vector/i;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->j:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    const/16 v10, 0x100

    move-object/from16 v2, p1

    move-object/from16 v4, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/i;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method public static a(Landroid/content/res/Resources;LA/c;)Lcom/google/android/maps/driveabout/vector/bd;
    .registers 15
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x100

    const/4 v5, 0x0

    .line 517
    invoke-static {p0, v7}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v3

    .line 518
    mul-int/lit8 v4, v3, 0x2

    .line 519
    const/4 v8, 0x1

    .line 526
    new-instance v0, Lu/a;

    invoke-direct {v0, v5, v4, v5, v8}, Lu/a;-><init>(IIZZ)V

    .line 529
    new-instance v1, Lu/d;

    invoke-direct {v1, p1, v0}, Lu/d;-><init>(LA/c;Lu/a;)V

    .line 531
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bd;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/E;->f:Lcom/google/android/maps/driveabout/vector/E;

    move v9, v5

    move v10, v5

    move v11, v5

    move v12, v5

    invoke-direct/range {v0 .. v12}, Lcom/google/android/maps/driveabout/vector/bd;-><init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IZZZZZ)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aZ;)Lu/d;
    .registers 2
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    return-object v0
.end method

.method private a(LC/a;Ljava/util/Collection;ILjava/util/Set;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 875
    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v4

    monitor-enter v4

    .line 878
    :try_start_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->c()V

    .line 880
    const/4 v0, 0x0

    move v3, v0

    :goto_c
    if-gt v3, p3, :cond_6a

    .line 882
    if-ne v3, p3, :cond_71

    .line 883
    const/4 v0, 0x0

    move-object v2, v0

    .line 889
    :goto_12
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_16
    :goto_16
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 892
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v1, v0}, Lu/d;->a(Lo/aq;)LF/T;

    move-result-object v6

    .line 893
    if-eqz v6, :cond_7e

    .line 894
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 895
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LD/a;

    invoke-interface {v6, p1, v1}, LF/T;->a(LC/a;LD/a;)V

    .line 896
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v1, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_46

    .line 897
    const/4 v1, 0x1

    invoke-interface {v6, v1}, LF/T;->a(Z)V

    .line 899
    :cond_46
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v7, v1, v3

    add-int/lit8 v7, v7, 0x1

    aput v7, v1, v3

    .line 900
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v7, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    if-ne v1, v7, :cond_7e

    .line 920
    :cond_58
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    if-eq v0, v1, :cond_6a

    if-eqz v2, :cond_6a

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_9f

    .line 929
    :cond_6a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->d()V

    .line 930
    monitor-exit v4

    .line 931
    return-void

    .line 885
    :cond_71
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 886
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move-object v2, v0

    goto :goto_12

    .line 905
    :cond_7e
    if-eqz v6, :cond_86

    invoke-interface {v6}, LF/T;->f()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 907
    :cond_86
    if-eqz v2, :cond_95

    .line 908
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    invoke-interface {v1, v0, v6}, Lg/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v1

    .line 909
    if-eqz v1, :cond_95

    .line 910
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 913
    :cond_95
    if-nez v3, :cond_16

    .line 914
    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_16

    .line 930
    :catchall_9c
    move-exception v0

    monitor-exit v4
    :try_end_9e
    .catchall {:try_start_5 .. :try_end_9e} :catchall_9c

    throw v0

    .line 880
    :cond_9f
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object p2, v2

    goto/16 :goto_c
.end method

.method private a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;Lo/aq;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 1344
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 1345
    const/high16 v1, 0x4000

    invoke-virtual {p4}, Lo/aq;->b()I

    move-result v2

    shr-int/2addr v1, v2

    .line 1346
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 1347
    invoke-virtual {p4}, Lo/aq;->g()Lo/T;

    move-result-object v2

    int-to-float v1, v1

    invoke-static {p1, p2, v2, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 1351
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 1352
    invoke-static {p3}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v1

    .line 1353
    aget v2, v1, v6

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    invoke-interface {v0, v2, v3, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1354
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 1355
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 1356
    return-void
.end method

.method public static b(Landroid/content/res/Resources;I)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 353
    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    .line 354
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, 0x2

    .line 356
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    add-int/lit8 v0, v0, 0x2

    .line 359
    mul-int/2addr v0, v1

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/aZ;)LR/h;
    .registers 2
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->C:LR/h;

    return-object v0
.end method

.method public static b(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 19
    .parameter
    .parameter

    .prologue
    .line 463
    const/16 v1, 0x100

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Landroid/content/res/Resources;I)I

    move-result v5

    .line 464
    mul-int/lit8 v6, v5, 0x2

    .line 465
    const/16 v10, 0x180

    .line 466
    const/4 v11, 0x1

    .line 467
    const/4 v12, 0x1

    .line 468
    const/4 v13, 0x0

    .line 469
    const/4 v14, 0x0

    .line 470
    const/4 v15, 0x0

    .line 471
    const/16 v16, 0x0

    .line 473
    const/4 v1, 0x0

    .line 474
    new-instance v2, Lu/a;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    .line 477
    new-instance v3, Lu/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    .line 479
    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x4

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method private b(I)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 1449
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 1450
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget v3, v3, LA/c;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1451
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "n="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1452
    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 19
    .parameter
    .parameter

    .prologue
    .line 490
    const/16 v10, 0x14c

    .line 491
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Landroid/content/res/Resources;I)I

    move-result v5

    .line 492
    mul-int/lit8 v6, v5, 0x2

    .line 493
    const/4 v11, 0x0

    .line 494
    const/4 v12, 0x1

    .line 495
    const/4 v13, 0x0

    .line 496
    const/4 v14, 0x0

    .line 497
    const/4 v15, 0x0

    .line 498
    const/16 v16, 0x0

    .line 500
    const/4 v1, 0x0

    .line 501
    new-instance v2, Lu/a;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v6, v1, v12}, Lu/a;-><init>(IIZZ)V

    .line 504
    new-instance v3, Lu/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lu/d;-><init>(LA/c;Lu/a;)V

    .line 506
    new-instance v1, Lcom/google/android/maps/driveabout/vector/aZ;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aZ;->M:Lg/c;

    const/4 v7, 0x2

    sget-object v8, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v9, 0x100

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    return-object v1
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 1
    .parameter

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aZ;->e()V

    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    .line 1435
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    .line 1436
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_29

    .line 1437
    const/16 v0, 0x6e

    const-string v1, "l"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1439
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->N:I

    .line 1440
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->O:J

    .line 1442
    :cond_29
    return-void
.end method

.method private h()Z
    .registers 4

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 1478
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v2}, Lu/d;->k()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v0, v2}, LF/T;->a(Lcom/google/googlenav/common/a;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1479
    const/4 v0, 0x1

    .line 1482
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method


# virtual methods
.method public a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1386
    const/4 v0, 0x0

    .line 1390
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 1391
    if-eqz v0, :cond_3d

    if-eqz p1, :cond_26

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->i()Lo/ad;

    move-result-object v3

    invoke-virtual {p1, v3}, Lo/aQ;->b(Lo/ae;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 1396
    :cond_26
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    invoke-interface {v0, v3}, LF/T;->a(Ly/b;)V

    .line 1397
    invoke-interface {v0, p2}, LF/T;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 1405
    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->b()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1408
    :cond_3d
    instance-of v3, v0, LF/Y;

    if-eqz v3, :cond_8

    .line 1409
    check-cast v0, LF/Y;

    invoke-virtual {v0, p3}, LF/Y;->a(Ljava/util/Set;)Z

    goto :goto_8

    .line 1413
    :cond_47
    return v1
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 853
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    .line 854
    return-void

    .line 853
    :cond_8
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(LA/c;)V
    .registers 3
    .parameter

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LA/c;)V

    .line 677
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 678
    return-void
.end method

.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 1319
    .line 1320
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, p2, v0, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I

    .line 1321
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 1322
    invoke-interface {v0, p1, v2, p3}, LF/T;->a(LC/a;ILjava/util/Collection;)V

    .line 1323
    invoke-interface {v0, p1, p4}, LF/T;->a(LC/a;Ljava/util/Collection;)V

    .line 1324
    invoke-interface {v0}, LF/T;->h()I

    move-result v0

    .line 1325
    if-le v0, v1, :cond_34

    :goto_2a
    move v1, v0

    .line 1328
    goto :goto_12

    .line 1329
    :cond_2c
    if-eqz p5, :cond_33

    array-length v0, p5

    if-lez v0, :cond_33

    .line 1330
    aput v1, p5, v4

    .line 1332
    :cond_33
    return-void

    :cond_34
    move v0, v1

    goto :goto_2a
.end method

.method public a(LC/b;)V
    .registers 2
    .parameter

    .prologue
    .line 693
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    .line 694
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 26
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1093
    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v3

    if-lez v3, :cond_7

    .line 1285
    :cond_6
    :goto_6
    return-void

    .line 1096
    :cond_7
    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v14

    .line 1097
    invoke-interface/range {p3 .. p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v15

    .line 1098
    new-instance v16, Lcom/google/android/maps/driveabout/vector/aT;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1100
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->f()Z

    move-result v3

    if-eqz v3, :cond_9d

    .line 1104
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v3, :cond_2f

    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/driveabout/vector/aZ;->h()Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 1105
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 1109
    :cond_2f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-eqz v3, :cond_44

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    if-nez v3, :cond_44

    .line 1110
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    .line 1115
    :cond_44
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_8c

    .line 1116
    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    .line 1117
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    .line 1118
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1119
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_73
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_89

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    .line 1120
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, LF/T;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_73

    .line 1122
    :cond_89
    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    .line 1127
    :cond_8c
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    if-eqz v3, :cond_9d

    .line 1128
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lu/d;->a(Ljava/util/List;)V

    .line 1135
    :cond_9d
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->m:Z

    if-eqz v3, :cond_14b

    sget-object v3, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v14, v3, :cond_ab

    sget-object v3, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v14, v3, :cond_14b

    :cond_ab
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_14b

    :cond_c6
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->c()Z

    move-result v3

    if-eqz v3, :cond_14b

    const/4 v3, 0x1

    move v5, v3

    .line 1140
    :goto_ce
    invoke-virtual/range {p1 .. p1}, LD/a;->I()I

    move-result v3

    if-lez v3, :cond_14e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v3}, LA/c;->h()Z

    move-result v3

    if-eqz v3, :cond_14e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    if-eqz v3, :cond_14e

    invoke-virtual/range {p1 .. p1}, LD/a;->J()Z

    move-result v3

    if-eqz v3, :cond_14e

    const/4 v3, 0x1

    move v13, v3

    .line 1144
    :goto_ec
    if-eqz v13, :cond_f1

    .line 1145
    invoke-virtual/range {p1 .. p1}, LD/a;->w()V

    .line 1151
    :cond_f1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1154
    const/4 v6, 0x0

    .line 1163
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->d()Z

    move-result v17

    move v12, v3

    .line 1165
    :goto_107
    if-ltz v12, :cond_293

    .line 1166
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v3, v3, v12

    if-lez v3, :cond_2c0

    .line 1167
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    aget v3, v3, v12

    sub-int v7, v8, v3

    .line 1170
    if-eqz v5, :cond_151

    move v4, v7

    .line 1171
    :goto_11c
    if-ge v4, v8, :cond_151

    .line 1172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    .line 1173
    invoke-interface {v3}, LF/T;->f()Z

    move-result v9

    if-nez v9, :cond_147

    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v9

    invoke-virtual {v9}, Lo/aq;->b()I

    move-result v9

    const/16 v10, 0xe

    if-lt v9, v10, :cond_147

    .line 1175
    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/q;Lo/aq;)V

    .line 1171
    :cond_147
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_11c

    .line 1135
    :cond_14b
    const/4 v3, 0x0

    move v5, v3

    goto :goto_ce

    .line 1140
    :cond_14e
    const/4 v3, 0x0

    move v13, v3

    goto :goto_ec

    .line 1186
    :cond_151
    if-eqz v13, :cond_1bf

    .line 1190
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e01

    const/16 v9, 0x1e01

    const/16 v10, 0x1e01

    invoke-interface {v3, v4, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    .line 1191
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x7f

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    move v4, v7

    .line 1192
    :goto_16a
    if-ge v4, v8, :cond_1b2

    .line 1193
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    .line 1195
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    const/16 v10, 0x200

    add-int/lit8 v11, v4, 0x1

    const/16 v18, 0x7f

    move/from16 v0, v18

    invoke-interface {v9, v10, v11, v0}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    .line 1196
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 1197
    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->i()Lo/ad;

    move-result-object v3

    .line 1198
    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v9

    invoke-virtual {v3}, Lo/ad;->g()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v9, v3}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 1200
    invoke-static/range {p1 .. p2}, LF/U;->a(LD/a;LC/a;)V

    .line 1201
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 1192
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_16a

    .line 1203
    :cond_1b2
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e00

    const/16 v9, 0x1e00

    const/16 v10, 0x1e00

    invoke-interface {v3, v4, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    .line 1207
    :cond_1bf
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v14, v7, v8}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;II)I

    move-result v4

    .line 1210
    const/4 v3, 0x0

    move v10, v3

    move v11, v4

    :goto_1ca
    if-eqz v11, :cond_28f

    .line 1211
    and-int/lit8 v3, v11, 0x1

    if-eqz v3, :cond_287

    .line 1212
    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    .line 1213
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/google/android/maps/driveabout/vector/aT;->a(I)V

    .line 1214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1216
    const/4 v3, 0x1

    shl-int v18, v3, v10

    move v9, v7

    .line 1217
    :goto_1ef
    if-ge v9, v8, :cond_284

    .line 1218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LF/T;

    .line 1219
    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v4

    invoke-virtual {v4}, Lo/aq;->k()Lo/aB;

    move-result-object v19

    .line 1220
    const/4 v4, 0x0

    .line 1226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    move-object/from16 v20, v0

    sget-object v21, LA/c;->n:LA/c;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_232

    if-eqz v19, :cond_232

    .line 1228
    sget-object v4, Lo/av;->c:Lo/av;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v4

    check-cast v4, Lo/E;

    .line 1230
    if-nez v4, :cond_22d

    const/4 v4, 0x0

    .line 1231
    :goto_221
    invoke-virtual {v15, v4}, Lcom/google/android/maps/driveabout/vector/aH;->a(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v4

    .line 1232
    if-nez v4, :cond_232

    move v3, v6

    .line 1217
    :goto_228
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    move v6, v3

    goto :goto_1ef

    .line 1230
    :cond_22d
    invoke-virtual {v4}, Lo/E;->b()Lo/r;

    move-result-object v4

    goto :goto_221

    .line 1236
    :cond_232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aZ;->t:[I

    move-object/from16 v19, v0

    aget v19, v19, v9

    and-int v19, v19, v18

    if-eqz v19, :cond_2bd

    .line 1237
    if-eqz v13, :cond_253

    .line 1238
    invoke-virtual/range {p1 .. p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    const/16 v19, 0x202

    add-int/lit8 v20, v9, 0x1

    const/16 v21, 0x7f

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v6, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    .line 1242
    :cond_253
    invoke-virtual/range {p1 .. p1}, LD/a;->A()V

    .line 1243
    invoke-interface {v3}, LF/T;->b()Lo/aq;

    move-result-object v6

    invoke-virtual {v6}, Lo/aq;->i()Lo/ad;

    move-result-object v6

    invoke-virtual {v6}, Lo/ad;->d()Lo/T;

    move-result-object v6

    .line 1244
    if-eqz v4, :cond_26d

    .line 1245
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v4, v0, v1, v2, v6}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    .line 1251
    :cond_26d
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v3, v0, v1, v2}, LF/T;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1252
    if-eqz v4, :cond_27f

    .line 1253
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1255
    :cond_27f
    const/4 v3, 0x1

    .line 1256
    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    goto :goto_228

    .line 1259
    :cond_284
    invoke-virtual/range {p1 .. p1}, LD/a;->B()V

    .line 1210
    :cond_287
    add-int/lit8 v3, v10, 0x1

    ushr-int/lit8 v4, v11, 0x1

    move v10, v3

    move v11, v4

    goto/16 :goto_1ca

    .line 1268
    :cond_28f
    if-eqz v17, :cond_2b6

    if-eqz v6, :cond_2b6

    .line 1274
    :cond_293
    if-eqz v13, :cond_298

    .line 1275
    invoke-virtual/range {p1 .. p1}, LD/a;->x()V

    .line 1278
    :cond_298
    invoke-virtual {v15}, Lcom/google/android/maps/driveabout/vector/aH;->g()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1280
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    if-eqz v3, :cond_6

    .line 1281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lu/d;->b(Ljava/util/List;)V

    .line 1282
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    goto/16 :goto_6

    :cond_2b6
    move v3, v7

    .line 1165
    :goto_2b7
    add-int/lit8 v4, v12, -0x1

    move v12, v4

    move v8, v3

    goto/16 :goto_107

    :cond_2bd
    move v3, v6

    goto/16 :goto_228

    :cond_2c0
    move v3, v8

    goto :goto_2b7
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 599
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    .line 600
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->a(LD/a;)V

    .line 601
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->a:Lcom/google/android/maps/driveabout/vector/aU;

    .line 602
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    if-nez v0, :cond_41

    .line 606
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-interface {v0, v1, v2, v3, v4}, Lg/c;->a(LA/c;IZLA/b;)Lg/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/a;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-interface {v0, v1, v2, v3}, Lg/c;->a(LA/c;ZLA/b;)Lk/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    .line 610
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    if-nez v0, :cond_41

    .line 613
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    instance-of v0, v0, Lk/f;

    if-eqz v0, :cond_42

    .line 614
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    check-cast v0, Lk/f;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    .line 621
    :cond_41
    return-void

    .line 617
    :cond_42
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad out-of-bounds coord generator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 805
    return-void
.end method

.method protected a(Lg/a;)V
    .registers 4
    .parameter

    .prologue
    .line 589
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    .line 590
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    .line 591
    return-void
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1427
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->c:Ly/b;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1428
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0, p1}, Lu/d;->b(Z)V

    .line 646
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 647
    return-void
.end method

.method public a(Lo/at;)Z
    .registers 3
    .parameter

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v0, p1}, LA/a;->a(Lo/at;)Z

    move-result v0

    return v0
.end method

.method b(Lo/T;)F
    .registers 3
    .parameter

    .prologue
    .line 816
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    if-nez v0, :cond_7

    .line 817
    const/high16 v0, 0x41a8

    .line 819
    :goto_6
    return v0

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0, p1}, Lg/a;->a(Lo/T;)F

    move-result v0

    goto :goto_6
.end method

.method protected b(LC/a;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 863
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 808
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 809
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 665
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->i:Z

    .line 666
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 943
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    .line 944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->I:Z

    .line 945
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->D:Lo/T;

    invoke-virtual {p1, v0}, LC/a;->a(Lo/T;)V

    .line 947
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0, p1}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v7

    .line 950
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_26

    .line 951
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-virtual {p1}, LC/a;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bc;->a(Lo/T;)V

    .line 952
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->w:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 955
    :cond_26
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    int-to-float v0, v0

    invoke-virtual {p1}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v8, v0

    .line 958
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 960
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 961
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 962
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 964
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 965
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v10

    .line 968
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->h()Z

    move-result v0

    .line 970
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    if-eqz v1, :cond_dc

    .line 971
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    .line 972
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->E:LC/b;

    .line 974
    if-eqz v0, :cond_d6

    .line 975
    new-instance v3, LC/a;

    invoke-virtual {p1}, LC/a;->k()I

    move-result v1

    invoke-virtual {p1}, LC/a;->k()I

    move-result v2

    invoke-virtual {p1}, LC/a;->m()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, LC/a;-><init>(LC/b;IIF)V

    .line 977
    const/4 v4, 0x0

    .line 978
    const/4 v5, 0x0

    .line 980
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    new-instance v2, Lo/T;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v6

    invoke-direct {v2, v6}, Lo/T;-><init>(Lo/T;)V

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v6, v3}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v3

    iget-boolean v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    invoke-virtual/range {v0 .. v6}, Lu/d;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    .line 1010
    :cond_8c
    :goto_8c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->b()I

    move-result v1

    .line 1031
    invoke-direct {p0, p1, v7, v9, v10}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Ljava/util/Collection;ILjava/util/Set;)V

    .line 1035
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->v:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_134

    const/4 v0, 0x1

    :goto_a1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    .line 1037
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->i:Z

    if-eqz v0, :cond_16c

    .line 1039
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_ab
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_137

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 1040
    const/4 v3, 0x0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1041
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    new-instance v5, LF/j;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, LF/j;-><init>(Lo/aq;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_ab

    .line 988
    :cond_d6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->e()V

    goto :goto_8c

    .line 990
    :cond_dc
    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    invoke-virtual {p1}, LC/a;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_104

    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v3}, Lg/a;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_104

    iget-wide v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v3}, LA/a;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_104

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v1, :cond_104

    if-eqz v0, :cond_8c

    .line 996
    :cond_104
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;)Ljava/util/Set;

    move-result-object v4

    .line 997
    const/4 v5, 0x0

    .line 998
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    new-instance v2, Lo/T;

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-direct {v2, v3}, Lo/T;-><init>(Lo/T;)V

    iget-boolean v6, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    move-object v3, v7

    invoke-virtual/range {v0 .. v6}, Lu/d;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    .line 1005
    invoke-virtual {p1}, LC/a;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->F:J

    .line 1006
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->x:Lg/a;

    invoke-interface {v0}, Lg/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->G:J

    .line 1007
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->Q:LA/a;

    invoke-virtual {v0}, LA/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->H:J

    goto/16 :goto_8c

    .line 1035
    :cond_134
    const/4 v0, 0x0

    goto/16 :goto_a1

    .line 1045
    :cond_137
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->y:Lk/f;

    invoke-virtual {v0, p1}, Lk/f;->b(LC/a;)Ljava/util/List;

    move-result-object v0

    .line 1047
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_141
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 1048
    const/4 v3, 0x0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1049
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aZ;->r:Ljava/util/ArrayList;

    new-instance v5, LF/j;

    shl-int v3, v8, v3

    invoke-direct {v5, v0, v3}, LF/j;-><init>(Lo/aq;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_141

    .line 1056
    :cond_16c
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->z:Z

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 1058
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->b()I

    move-result v0

    sub-int/2addr v0, v1

    .line 1059
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    if-nez v1, :cond_1b2

    if-nez v0, :cond_1b2

    .line 1068
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    monitor-enter v1

    .line 1069
    :try_start_180
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1070
    monitor-exit v1
    :try_end_188
    .catchall {:try_start_180 .. :try_end_188} :catchall_1ad

    .line 1071
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_18c
    :goto_18c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bb;

    .line 1072
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b0

    const/4 v1, 0x1

    :goto_1a1
    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/bb;->a(Z)Z

    move-result v1

    if-nez v1, :cond_18c

    .line 1073
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aZ;->B:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_18c

    .line 1070
    :catchall_1ad
    move-exception v0

    :try_start_1ae
    monitor-exit v1
    :try_end_1af
    .catchall {:try_start_1ae .. :try_end_1af} :catchall_1ad

    throw v0

    .line 1072
    :cond_1b0
    const/4 v1, 0x0

    goto :goto_1a1

    .line 1079
    :cond_1b2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1b8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 1080
    const/4 v2, 0x0

    invoke-interface {v0, v2}, LF/T;->a(Z)V

    goto :goto_1b8

    .line 1082
    :cond_1c9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1083
    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 625
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->R:Ljava/lang/ref/WeakReference;

    .line 626
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->a:Lcom/google/android/maps/driveabout/vector/aU;

    .line 630
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->i()V

    .line 635
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 636
    return-void
.end method

.method public i()V
    .registers 4

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lu/d;->a(J)V

    .line 595
    return-void
.end method

.method public i_()V
    .registers 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->g()V

    .line 657
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->A:Z

    .line 658
    return-void
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 681
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 686
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    return v0
.end method

.method public l()Lu/d;
    .registers 2

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    return-object v0
.end method

.method public m()LA/c;
    .registers 2

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    return-object v0
.end method

.method public n()Ljava/util/List;
    .registers 2

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 1308
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->b:Z

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public q()Lg/c;
    .registers 2

    .prologue
    .line 1338
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->L:Lg/c;

    return-object v0
.end method

.method public r()LB/e;
    .registers 2

    .prologue
    .line 1367
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aZ;->q:Lu/d;

    invoke-virtual {v0}, Lu/d;->f()LB/e;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 1457
    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "tileType"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->p:LA/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isBase"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->K:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTilesPerView"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTilesToFetch"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "drawOrder"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->h:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "fetchMissingAncestorTiles"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->l:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "prefetchAncestors"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "tileSize"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->o:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isContributingLabels"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->J:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "maxTileSize"

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aZ;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .registers 2

    .prologue
    .line 848
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->a:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method
