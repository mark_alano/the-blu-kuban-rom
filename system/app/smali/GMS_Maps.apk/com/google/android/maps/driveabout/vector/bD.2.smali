.class public Lcom/google/android/maps/driveabout/vector/bd;
.super Lcom/google/android/maps/driveabout/vector/aZ;
.source "SourceFile"


# instance fields
.field private d:F

.field private e:Z


# direct methods
.method constructor <init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IZZZZZ)V
    .registers 30
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-virtual/range {p1 .. p1}, Lu/d;->a()LA/c;

    move-result-object v2

    const/4 v11, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    move/from16 v15, p11

    move/from16 v16, p12

    invoke-direct/range {v1 .. v16}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    .line 18
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/maps/driveabout/vector/bd;->e:Z

    .line 34
    const/high16 v1, 0x41f0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/maps/driveabout/vector/bd;->d:F

    .line 35
    return-void
.end method


# virtual methods
.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 72
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/bd;->d:F

    .line 73
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-virtual {p2}, LC/a;->r()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bd;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_15

    .line 60
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bd;->e:Z

    if-eqz v0, :cond_11

    .line 61
    invoke-super {p0, p2, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    .line 63
    :cond_11
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 68
    :goto_14
    return-void

    .line 66
    :cond_15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bd;->b:Z

    goto :goto_14
.end method

.method public b(LC/a;LD/a;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 48
    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bd;->d:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_13

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bd;->e:Z

    .line 50
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    move-result v0

    .line 53
    :goto_12
    return v0

    .line 52
    :cond_13
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bd;->e:Z

    goto :goto_12
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .registers 2

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method
