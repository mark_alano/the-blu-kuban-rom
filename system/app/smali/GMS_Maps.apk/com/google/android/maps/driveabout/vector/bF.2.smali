.class public Lcom/google/android/maps/driveabout/vector/bF;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Lo/aq;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/bG;

.field private final c:Ljava/util/Map;

.field private d:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 122
    new-instance v0, Lo/aq;

    invoke-direct {v0, v1, v1, v1}, Lo/aq;-><init>(III)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bF;->b:Lo/aq;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/bG;)V
    .registers 3
    .parameter

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->c:Ljava/util/Map;

    .line 206
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bF;->a:Lcom/google/android/maps/driveabout/vector/bG;

    .line 207
    return-void
.end method

.method private a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bG;)Lcom/google/android/maps/driveabout/vector/bE;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->a:Lcom/google/android/maps/driveabout/vector/bG;

    if-eq p5, v0, :cond_2b

    invoke-virtual {p5, p4}, Lcom/google/android/maps/driveabout/vector/bG;->a(LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 245
    invoke-virtual {p5, p4}, Lcom/google/android/maps/driveabout/vector/bG;->a(LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    .line 246
    if-nez v0, :cond_2a

    .line 247
    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No zoom table for tile type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bE;->a:Lcom/google/android/maps/driveabout/vector/bE;

    .line 263
    :cond_2a
    :goto_2a
    return-object v0

    .line 252
    :cond_2b
    add-int/lit8 v3, p3, -0x1

    .line 253
    invoke-virtual {p5, p1, p2, v3}, Lcom/google/android/maps/driveabout/vector/bG;->a(III)I

    move-result v0

    .line 254
    invoke-virtual {p5, v0}, Lcom/google/android/maps/driveabout/vector/bG;->a(I)Lcom/google/android/maps/driveabout/vector/bG;

    move-result-object v5

    .line 255
    if-nez v5, :cond_5a

    .line 256
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->a:Lcom/google/android/maps/driveabout/vector/bG;

    invoke-virtual {v0, p4}, Lcom/google/android/maps/driveabout/vector/bG;->a(LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    .line 257
    if-nez v0, :cond_2a

    .line 258
    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No root zoom table for tile type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bE;->a:Lcom/google/android/maps/driveabout/vector/bE;

    goto :goto_2a

    :cond_5a
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p4

    .line 263
    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bF;->a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bG;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    goto :goto_2a
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/maps/driveabout/vector/bF;
    .registers 16
    .parameter

    .prologue
    .line 138
    if-nez p0, :cond_4

    .line 139
    const/4 v0, 0x0

    .line 202
    :goto_3
    return-object v0

    .line 142
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    .line 143
    if-nez v9, :cond_d

    .line 148
    const/4 v0, 0x0

    goto :goto_3

    .line 150
    :cond_d
    const-string v0, "ZoomTableQuadTree.fromProto"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bG;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/bG;-><init>()V

    .line 152
    const/4 v1, 0x0

    move v8, v1

    :goto_19
    if-ge v8, v9, :cond_ad

    .line 153
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    .line 155
    const/4 v1, 0x3

    invoke-virtual {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 157
    const/4 v1, 0x2

    invoke-virtual {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    .line 158
    const/4 v1, 0x5

    invoke-virtual {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 161
    if-lez v4, :cond_40

    .line 162
    new-array v1, v4, [I

    .line 163
    const/4 v2, 0x0

    :goto_34
    if-ge v2, v4, :cond_51

    .line 164
    const/4 v5, 0x2

    invoke-virtual {v10, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v5

    aput v5, v1, v2

    .line 163
    add-int/lit8 v2, v2, 0x1

    goto :goto_34

    .line 169
    :cond_40
    add-int/lit8 v1, v3, 0x1

    sub-int/2addr v1, v6

    new-array v1, v1, [I

    .line 170
    const/4 v2, 0x0

    :goto_46
    sub-int v4, v3, v6

    if-gt v2, v4, :cond_51

    .line 171
    add-int v4, v2, v6

    aput v4, v1, v2

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_46

    .line 174
    :cond_51
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 176
    new-instance v5, Lcom/google/android/maps/driveabout/vector/bE;

    invoke-direct {v5, v1, v6, v2, v3}, Lcom/google/android/maps/driveabout/vector/bE;-><init>([IIII)V

    .line 179
    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v11

    .line 181
    const/4 v1, 0x0

    move v7, v1

    :goto_62
    if-ge v7, v11, :cond_a8

    .line 182
    const/4 v1, 0x4

    invoke-virtual {v10, v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v12

    .line 184
    const/4 v1, 0x2

    invoke-virtual {v12, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 185
    const/4 v2, 0x3

    invoke-virtual {v12, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 186
    const/4 v3, 0x4

    invoke-virtual {v12, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 187
    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v13

    .line 188
    new-instance v14, Lo/aq;

    invoke-direct {v14, v1, v2, v3}, Lo/aq;-><init>(III)V

    .line 189
    const/4 v1, 0x0

    move v6, v1

    :goto_84
    if-ge v6, v13, :cond_a4

    .line 190
    const/4 v1, 0x1

    invoke-virtual {v12, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v1

    invoke-static {v1}, LA/c;->a(I)LA/c;

    move-result-object v4

    .line 194
    if-eqz v4, :cond_a0

    .line 195
    invoke-virtual {v14}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {v14}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {v14}, Lo/aq;->b()I

    move-result v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bG;->a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bE;)V

    .line 189
    :cond_a0
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_84

    .line 181
    :cond_a4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_62

    .line 152
    :cond_a8
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_19

    .line 201
    :cond_ad
    const-string v1, "ZoomTableQuadTree.fromProto"

    invoke-static {v1}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 202
    new-instance v1, Lcom/google/android/maps/driveabout/vector/bF;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/vector/bF;-><init>(Lcom/google/android/maps/driveabout/vector/bG;)V

    move-object v0, v1

    goto/16 :goto_3
.end method


# virtual methods
.method public a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->d:Lo/T;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->d:Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 274
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bE;

    .line 275
    if-eqz v0, :cond_1c

    .line 289
    :goto_16
    return-object v0

    .line 279
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bF;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 281
    :cond_1c
    const/16 v0, 0x1e

    invoke-static {v0, p1}, Lo/aq;->a(ILo/T;)Lo/aq;

    move-result-object v0

    .line 282
    if-nez v0, :cond_26

    .line 283
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bF;->b:Lo/aq;

    .line 285
    :cond_26
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bF;->d:Lo/T;

    .line 286
    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bF;->a:Lcom/google/android/maps/driveabout/vector/bG;

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bF;->a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bG;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    .line 288
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bF;->c:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_16
.end method
