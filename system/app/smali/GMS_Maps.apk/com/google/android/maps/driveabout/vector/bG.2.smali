.class Lcom/google/android/maps/driveabout/vector/bG;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/HashMap;

.field private b:[Lcom/google/android/maps/driveabout/vector/bG;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method


# virtual methods
.method public a(III)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    shr-int v0, p1, p3

    and-int/lit8 v0, v0, 0x1

    .line 102
    shr-int v1, p2, p3

    and-int/lit8 v1, v1, 0x1

    .line 103
    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LA/c;)Lcom/google/android/maps/driveabout/vector/bE;
    .registers 3
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->a:Ljava/util/HashMap;

    if-nez v0, :cond_6

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bE;

    goto :goto_5
.end method

.method public a(I)Lcom/google/android/maps/driveabout/vector/bG;
    .registers 3
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    if-eqz v0, :cond_9

    if-ltz p1, :cond_9

    const/4 v0, 0x3

    if-le p1, v0, :cond_b

    .line 112
    :cond_9
    const/4 v0, 0x0

    .line 114
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    aget-object v0, v0, p1

    goto :goto_a
.end method

.method public a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bE;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    if-gtz p3, :cond_12

    .line 77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->a:Ljava/util/HashMap;

    if-nez v0, :cond_c

    .line 78
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->a:Ljava/util/HashMap;

    .line 80
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :goto_11
    return-void

    .line 83
    :cond_12
    add-int/lit8 v3, p3, -0x1

    .line 84
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/maps/driveabout/vector/bG;->a(III)I

    move-result v1

    .line 85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    if-nez v0, :cond_21

    .line 86
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/bG;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    .line 88
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    aget-object v0, v0, v1

    .line 89
    if-nez v0, :cond_30

    .line 90
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bG;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/bG;-><init>()V

    .line 91
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bG;->b:[Lcom/google/android/maps/driveabout/vector/bG;

    aput-object v0, v2, v1

    :cond_30
    move v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 93
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bG;->a(IIILA/c;Lcom/google/android/maps/driveabout/vector/bE;)V

    goto :goto_11
.end method
