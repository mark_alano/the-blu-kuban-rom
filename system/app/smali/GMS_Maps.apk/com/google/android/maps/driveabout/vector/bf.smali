.class public Lcom/google/android/maps/driveabout/vector/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[LA/c;

.field public static final b:[LA/c;

.field public static volatile c:I

.field private static d:Z

.field private static e:Z

.field private static volatile f:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 53
    const/4 v0, 0x5

    new-array v0, v0, [LA/c;

    sget-object v1, LA/c;->b:LA/c;

    aput-object v1, v0, v3

    sget-object v1, LA/c;->d:LA/c;

    aput-object v1, v0, v2

    sget-object v1, LA/c;->g:LA/c;

    aput-object v1, v0, v4

    sget-object v1, LA/c;->h:LA/c;

    aput-object v1, v0, v5

    sget-object v1, LA/c;->i:LA/c;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    .line 61
    const/16 v0, 0xc

    new-array v0, v0, [LA/c;

    sget-object v1, LA/c;->a:LA/c;

    aput-object v1, v0, v3

    sget-object v1, LA/c;->c:LA/c;

    aput-object v1, v0, v2

    sget-object v1, LA/c;->d:LA/c;

    aput-object v1, v0, v4

    sget-object v1, LA/c;->f:LA/c;

    aput-object v1, v0, v5

    sget-object v1, LA/c;->e:LA/c;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, LA/c;->j:LA/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LA/c;->l:LA/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LA/c;->k:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LA/c;->m:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LA/c;->n:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LA/c;->o:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LA/c;->p:LA/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    .line 85
    sput-boolean v3, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    .line 89
    const/16 v0, 0xa

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    .line 96
    const/4 v0, -0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Law/h;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 480
    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    .line 482
    new-instance v3, Law/j;

    invoke-direct {v3}, Law/j;-><init>()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->Q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->a(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->F()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->b(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-virtual {v3, p3}, Law/j;->c(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->d(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->a(Z)Law/j;

    move-result-object v3

    invoke-virtual {v3, v1}, Law/j;->b(Z)Law/j;

    move-result-object v3

    invoke-static {p0}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->e(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/res/Resources;)Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->c(Z)Law/j;

    move-result-object v3

    invoke-virtual {v0, p0}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->e(Z)Law/j;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/capabilities/a;->b()Z

    move-result v0

    if-nez v0, :cond_b1

    move v0, v1

    :goto_56
    invoke-virtual {v3, v0}, Law/j;->d(Z)Law/j;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v3}, Law/j;->a(I)Law/j;

    move-result-object v3

    .line 501
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 502
    if-eqz v4, :cond_b3

    const-string v0, "GMM"

    :goto_72
    invoke-virtual {v3, v0}, Law/j;->f(Ljava/lang/String;)Law/j;

    .line 514
    invoke-virtual {v3, v1}, Law/j;->b(I)Law/j;

    .line 516
    invoke-virtual {v3, v1}, Law/j;->f(Z)Law/j;

    .line 518
    invoke-virtual {v3}, Law/j;->a()Law/h;

    move-result-object v0

    .line 522
    const-string v1, "GMM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 523
    if-nez v1, :cond_89

    if-eqz v4, :cond_9f

    .line 524
    :cond_89
    const/4 v1, -0x1

    invoke-virtual {v3, v1}, Law/j;->b(I)Law/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Law/j;->f(Z)Law/j;

    move-result-object v1

    const-string v2, "DriveAbout"

    invoke-virtual {v1, v2}, Law/j;->f(Ljava/lang/String;)Law/j;

    move-result-object v1

    invoke-virtual {v1}, Law/j;->b()Law/p;

    move-result-object v1

    .line 530
    invoke-virtual {v0, v1}, Law/h;->a(Law/p;)V

    .line 533
    :cond_9f
    new-instance v1, Lcom/google/android/maps/driveabout/vector/bh;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/bh;-><init>(Lcom/google/android/maps/driveabout/vector/bg;)V

    invoke-virtual {v0, v1}, Law/h;->a(Law/q;)V

    .line 538
    const-string v1, "1"

    .line 539
    const-string v1, "2"

    .line 540
    const-string v1, "3"

    .line 541
    const-string v1, "3"

    .line 546
    return-object v0

    :cond_b1
    move v0, v2

    .line 482
    goto :goto_56

    :cond_b3
    move-object v0, p2

    .line 502
    goto :goto_72
.end method

.method public static declared-synchronized a(LA/c;Landroid/content/Context;Landroid/content/res/Resources;)Lr/z;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 298
    const-class v6, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v6

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    if-nez v0, :cond_12

    .line 299
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorGlobalState.initialize() must be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_f

    .line 298
    :catchall_f
    move-exception v0

    monitor-exit v6

    throw v0

    .line 301
    :cond_12
    const/4 v0, 0x1

    :try_start_13
    new-array v0, v0, [LA/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p1}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 307
    invoke-static {p0}, Lr/C;->c(LA/c;)Lr/z;
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_f

    move-result-object v0

    monitor-exit v6

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    const-class v10, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v10

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_c7

    if-eqz v0, :cond_9

    .line 231
    :goto_7
    monitor-exit v10

    return-void

    .line 127
    :cond_9
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 128
    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 130
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    .line 132
    invoke-static {}, LJ/a;->a()V

    .line 133
    invoke-static/range {p5 .. p5}, Lbm/m;->a(Lbm/q;)V

    .line 135
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    .line 139
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bf;->b(Landroid/content/Context;)V

    .line 142
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    .line 143
    if-nez v1, :cond_53

    .line 149
    invoke-static {}, LJ/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p3, v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Law/h;

    move-result-object v1

    .line 152
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 153
    if-eqz v0, :cond_46

    .line 154
    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v2, v3}, Law/h;->a(J)V

    .line 158
    :cond_46
    if-eqz v0, :cond_48

    .line 165
    :cond_48
    invoke-static {v1}, Lcom/google/googlenav/clientparam/f;->a(Law/h;)V

    .line 167
    new-instance v0, LR/l;

    invoke-direct {v0, v1}, LR/l;-><init>(Law/h;)V

    invoke-virtual {v1, v0}, Law/h;->a(Law/q;)V

    .line 169
    :cond_53
    invoke-virtual {v1}, Law/h;->v()V

    .line 173
    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    .line 174
    invoke-static {v1, v3}, Lv/d;->a(Law/h;Ljava/io/File;)Lv/d;

    .line 177
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_60
    .catchall {:try_start_9 .. :try_end_60} :catchall_c7

    move-result-object v2

    .line 179
    const/4 v0, -0x1

    if-eq p4, v0, :cond_6b

    .line 181
    :try_start_64
    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lr/y;->a(Ljava/io/InputStream;)V
    :try_end_6b
    .catchall {:try_start_64 .. :try_end_6b} :catchall_c7
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_6b} :catch_ca

    .line 187
    :cond_6b
    :goto_6b
    :try_start_6b
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 188
    const-string v4, "GMM"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 189
    if-nez v0, :cond_7b

    if-eqz v4, :cond_d1

    .line 192
    :cond_7b
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    move-object v4, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 193
    sget-object v4, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v5

    move-object v6, v2

    move-object v7, v3

    move-object v8, p0

    move-object v9, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 211
    :cond_8f
    :goto_8f
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 212
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    invoke-static {v1, v3, v2, v0}, Lr/n;->a(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)Lr/n;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_a6

    .line 216
    invoke-virtual {v0}, Lr/n;->c()V

    .line 217
    invoke-static {v0}, Ln/q;->a(Lr/n;)Ln/q;

    .line 221
    :cond_a6
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 223
    new-instance v0, Ll/a;

    invoke-direct {v0, p0}, Ll/a;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ll/f;->a(Ll/f;)V

    .line 226
    :cond_b4
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->e:Z

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 229
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    .line 230
    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_c5
    .catchall {:try_start_6b .. :try_end_c5} :catchall_c7

    goto/16 :goto_7

    .line 123
    :catchall_c7
    move-exception v0

    monitor-exit v10

    throw v0

    .line 182
    :catch_ca
    move-exception v0

    .line 183
    :try_start_cb
    const-string v4, "Could not load encryption key"

    invoke-static {v4, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6b

    .line 201
    :cond_d1
    if-eqz p2, :cond_8f

    move-object v0, p2

    move-object v4, p0

    move-object v5, p1

    .line 202
    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    :try_end_d9
    .catchall {:try_start_cb .. :try_end_d9} :catchall_c7

    goto :goto_8f
.end method

.method public static declared-synchronized a(Ljava/util/Locale;)V
    .registers 5
    .parameter

    .prologue
    .line 458
    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_3
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    .line 459
    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 460
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0, p0}, Lr/z;->a(Ljava/util/Locale;)V
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_25

    goto :goto_b

    .line 458
    :catchall_25
    move-exception v0

    monitor-exit v1

    throw v0

    .line 464
    :cond_28
    monitor-exit v1

    return-void
.end method

.method public static a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 285
    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 287
    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    invoke-virtual {v0, p0}, Lr/I;->a(Z)V

    .line 290
    :cond_13
    return-void
.end method

.method private static declared-synchronized a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 317
    const-class v9, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v9

    :try_start_3
    const-string v0, "GMM"

    invoke-interface {p1}, Law/p;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 318
    if-eqz v6, :cond_12

    .line 323
    const/4 v0, 0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    .line 325
    :cond_12
    array-length v10, p0

    const/4 v0, 0x0

    move v8, v0

    :goto_15
    if-ge v8, v10, :cond_3a

    aget-object v0, p0, v8

    .line 327
    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 325
    :cond_1f
    :goto_1f
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_15

    :cond_23
    move-object v1, p1

    move-object v2, p4

    move-object/from16 v3, p5

    move-object v4, p2

    move-object v5, p3

    move v7, v6

    .line 332
    invoke-virtual/range {v0 .. v7}, LA/c;->a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;

    move-result-object v1

    .line 339
    if-eqz v1, :cond_1f

    .line 340
    invoke-interface {v1}, Lr/z;->g()V

    .line 341
    invoke-static {v0, v1}, Lr/C;->a(LA/c;Lr/z;)V
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_37

    goto :goto_1f

    .line 317
    :catchall_37
    move-exception v0

    monitor-exit v9

    throw v0

    .line 344
    :cond_3a
    monitor-exit v9

    return-void
.end method

.method public static a()Z
    .registers 1

    .prologue
    .line 262
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->e:Z

    return v0
.end method

.method private static a(Landroid/content/Context;)Z
    .registers 8
    .parameter

    .prologue
    const-wide/high16 v5, 0x3fd0

    .line 234
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 241
    int-to-float v0, v0

    .line 243
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 244
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-gtz v1, :cond_2f

    iget v1, v2, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-lez v1, :cond_45

    :cond_2f
    move v1, v0

    .line 254
    :goto_30
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    div-float v1, v3, v1

    .line 255
    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    .line 257
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x41c8

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4a

    const/4 v0, 0x1

    :goto_44
    return v0

    .line 249
    :cond_45
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    .line 250
    iget v0, v2, Landroid/util/DisplayMetrics;->ydpi:F

    goto :goto_30

    .line 257
    :cond_4a
    const/4 v0, 0x0

    goto :goto_44
.end method

.method private static b(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 568
    invoke-static {p0}, LJ/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 569
    invoke-static {p0}, LJ/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 570
    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 571
    return-void
.end method

.method public static b()Z
    .registers 1

    .prologue
    .line 276
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .registers 6

    .prologue
    .line 350
    const-class v2, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v2

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_4c

    if-nez v0, :cond_9

    .line 380
    :goto_7
    monitor-exit v2

    return-void

    .line 355
    :cond_9
    :try_start_9
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_11
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    .line 356
    invoke-static {v0}, Lr/C;->b(LA/c;)Z
    :try_end_20
    .catchall {:try_start_9 .. :try_end_20} :catchall_4c

    move-result v1

    if-eqz v1, :cond_11

    .line 358
    :try_start_23
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v1

    invoke-interface {v1}, Lr/z;->h()V

    .line 359
    invoke-static {v0}, Lr/C;->a(LA/c;)V
    :try_end_2d
    .catchall {:try_start_23 .. :try_end_2d} :catchall_4c
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_2d} :catch_2e

    goto :goto_11

    .line 360
    :catch_2e
    move-exception v1

    .line 361
    :try_start_2f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not stop "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " tile store"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4b
    .catchall {:try_start_2f .. :try_end_4b} :catchall_4c

    goto :goto_11

    .line 350
    :catchall_4c
    move-exception v0

    monitor-exit v2

    throw v0

    .line 366
    :cond_4f
    :try_start_4f
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_58

    .line 367
    invoke-static {}, Lr/n;->b()V

    .line 369
    :cond_58
    invoke-static {}, Lv/d;->d()V

    .line 371
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_67

    .line 373
    invoke-virtual {v0}, Law/h;->u()V

    .line 374
    invoke-static {}, Law/h;->c()V

    .line 377
    :cond_67
    invoke-static {}, Lbm/m;->d()V

    .line 379
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_6d
    .catchall {:try_start_4f .. :try_end_6d} :catchall_4c

    goto :goto_7
.end method

.method public static declared-synchronized d()V
    .registers 4

    .prologue
    .line 386
    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2b

    if-nez v0, :cond_9

    .line 401
    :cond_7
    :goto_7
    monitor-exit v1

    return-void

    .line 390
    :cond_9
    :try_start_9
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    .line 391
    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 392
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->i()V
    :try_end_2a
    .catchall {:try_start_9 .. :try_end_2a} :catchall_2b

    goto :goto_11

    .line 386
    :catchall_2b
    move-exception v0

    monitor-exit v1

    throw v0

    .line 395
    :cond_2e
    :try_start_2e
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 396
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lv/d;->a(Z)V

    .line 398
    :cond_3c
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 399
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->e()V
    :try_end_49
    .catchall {:try_start_2e .. :try_end_49} :catchall_2b

    goto :goto_7
.end method

.method public static e()I
    .registers 1

    .prologue
    .line 408
    sget v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    return v0
.end method

.method public static declared-synchronized f()V
    .registers 4

    .prologue
    .line 415
    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2b

    if-nez v0, :cond_9

    .line 429
    :cond_7
    :goto_7
    monitor-exit v1

    return-void

    .line 419
    :cond_9
    :try_start_9
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    .line 420
    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 421
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->b()V
    :try_end_2a
    .catchall {:try_start_9 .. :try_end_2a} :catchall_2b

    goto :goto_11

    .line 415
    :catchall_2b
    move-exception v0

    monitor-exit v1

    throw v0

    .line 425
    :cond_2e
    :try_start_2e
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lv/d;->a(Z)V

    .line 426
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 427
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->f()V
    :try_end_43
    .catchall {:try_start_2e .. :try_end_43} :catchall_2b

    goto :goto_7
.end method

.method public static declared-synchronized g()J
    .registers 7

    .prologue
    .line 435
    const-class v3, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v3

    const-wide/16 v0, 0x0

    .line 436
    :try_start_5
    sget-boolean v2, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_4c

    if-nez v2, :cond_b

    .line 450
    :goto_9
    monitor-exit v3

    return-wide v0

    .line 440
    :cond_b
    :try_start_b
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v1, v0

    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    .line 441
    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v5

    if-eqz v5, :cond_4f

    .line 442
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->e()J

    move-result-wide v5

    add-long v0, v1, v5

    :goto_30
    move-wide v1, v0

    goto :goto_14

    .line 446
    :cond_32
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 447
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->g()J

    move-result-wide v4

    add-long/2addr v1, v4

    .line 449
    :cond_41
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    invoke-virtual {v0}, Lv/d;->b()J
    :try_end_48
    .catchall {:try_start_b .. :try_end_48} :catchall_4c

    move-result-wide v4

    add-long v0, v1, v4

    .line 450
    goto :goto_9

    .line 435
    :catchall_4c
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4f
    move-wide v0, v1

    goto :goto_30
.end method

.method public static h()I
    .registers 1

    .prologue
    .line 555
    sget v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    return v0
.end method
