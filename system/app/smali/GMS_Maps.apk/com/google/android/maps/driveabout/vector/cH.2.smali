.class public final enum Lcom/google/android/maps/driveabout/vector/cH;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/maps/driveabout/vector/cH;

.field public static final enum b:Lcom/google/android/maps/driveabout/vector/cH;

.field public static final enum c:Lcom/google/android/maps/driveabout/vector/cH;

.field private static final synthetic d:[Lcom/google/android/maps/driveabout/vector/cH;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 456
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cH;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cH;->a:Lcom/google/android/maps/driveabout/vector/cH;

    .line 459
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cH;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/cH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cH;->b:Lcom/google/android/maps/driveabout/vector/cH;

    .line 462
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cH;

    const-string v1, "REPLACE_BASE_TILE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/cH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cH;->c:Lcom/google/android/maps/driveabout/vector/cH;

    .line 454
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/cH;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cH;->a:Lcom/google/android/maps/driveabout/vector/cH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cH;->b:Lcom/google/android/maps/driveabout/vector/cH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cH;->c:Lcom/google/android/maps/driveabout/vector/cH;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cH;->d:[Lcom/google/android/maps/driveabout/vector/cH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/cH;
    .registers 2
    .parameter

    .prologue
    .line 454
    const-class v0, Lcom/google/android/maps/driveabout/vector/cH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cH;

    return-object v0
.end method

.method public static values()[Lcom/google/android/maps/driveabout/vector/cH;
    .registers 1

    .prologue
    .line 454
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cH;->d:[Lcom/google/android/maps/driveabout/vector/cH;

    invoke-virtual {v0}, [Lcom/google/android/maps/driveabout/vector/cH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/cH;

    return-object v0
.end method
