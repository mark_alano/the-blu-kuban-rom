.class public Lcom/google/android/maps/driveabout/vector/cL;
.super Lcom/google/android/maps/driveabout/vector/en;
.source "SourceFile"


# instance fields
.field private d:Z

.field private e:J

.field private f:Lo/aK;

.field private g:Lo/aK;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/di;Lcom/google/android/maps/driveabout/vector/dh;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/en;-><init>(Lcom/google/android/maps/driveabout/vector/di;Lcom/google/android/maps/driveabout/vector/dh;)V

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->e:J

    .line 48
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->w()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/cL;->a(Lcom/google/android/maps/driveabout/vector/di;D)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->d:Z

    .line 50
    return-void
.end method

.method private static a(Lcom/google/android/maps/driveabout/vector/di;D)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->k:Lcom/google/android/maps/driveabout/vector/di;

    if-eq p0, v0, :cond_c

    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->l:Lcom/google/android/maps/driveabout/vector/di;

    if-eq p0, v0, :cond_c

    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->m:Lcom/google/android/maps/driveabout/vector/di;

    if-ne p0, v0, :cond_14

    :cond_c
    const-wide/high16 v0, 0x3ff0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 88
    const/4 v0, 0x1

    .line 90
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/am;

    invoke-virtual {v0}, Lo/am;->k()Lo/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cL;->b:Lcom/google/android/maps/driveabout/vector/dh;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/dh;->a()Lo/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/av;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_9
.end method


# virtual methods
.method public a(Lo/Q;)F
    .registers 4
    .parameter

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->d:Z

    if-eqz v0, :cond_c

    .line 145
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/en;->a(Lo/Q;)F

    move-result v0

    const/high16 v1, 0x3f80

    sub-float/2addr v0, v1

    .line 147
    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/en;->a(Lo/Q;)F

    move-result v0

    goto :goto_b
.end method

.method public a()J
    .registers 3

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->e:J

    return-wide v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;)Ljava/util/List;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 70
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->v()Lo/aK;

    move-result-object v1

    .line 71
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->f:Lo/aK;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->f:Lo/aK;

    invoke-virtual {v1, v0}, Lo/aK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cL;->b()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 72
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->h:Ljava/util/List;

    .line 83
    :goto_19
    return-object v0

    .line 74
    :cond_1a
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cL;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cL;->e:J

    .line 75
    invoke-virtual {v1}, Lo/aK;->a()Lo/aL;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/cL;->c(Lcom/google/android/maps/driveabout/vector/k;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cL;->b:Lcom/google/android/maps/driveabout/vector/dh;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dh;->a()Lo/av;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lo/am;->a(Lo/aL;ILo/av;)Ljava/util/ArrayList;

    move-result-object v2

    .line 77
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_50

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->j()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_50

    const/4 v0, 0x1

    .line 78
    :goto_44
    if-nez v0, :cond_49

    .line 79
    invoke-virtual {p0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cL;->a(Lo/aK;Ljava/util/ArrayList;)V

    .line 81
    :cond_49
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/cL;->h:Ljava/util/List;

    .line 82
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/cL;->f:Lo/aK;

    .line 83
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->h:Ljava/util/List;

    goto :goto_19

    .line 77
    :cond_50
    const/4 v0, 0x0

    goto :goto_44
.end method

.method a(Lo/aK;Ljava/util/ArrayList;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 127
    .line 128
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 129
    :goto_7
    if-ge v3, v4, :cond_24

    .line 130
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/am;

    .line 131
    invoke-virtual {v0}, Lo/am;->i()Lo/aa;

    move-result-object v1

    .line 132
    invoke-virtual {p1, v1}, Lo/aK;->b(Lo/ab;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 133
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 129
    :goto_1f
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_7

    .line 137
    :cond_24
    add-int/lit8 v0, v4, -0x1

    :goto_26
    if-lt v0, v2, :cond_2e

    .line 138
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 137
    add-int/lit8 v0, v0, -0x1

    goto :goto_26

    .line 140
    :cond_2e
    return-void

    :cond_2f
    move v0, v2

    goto :goto_1f
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/k;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 106
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->v()Lo/aK;

    move-result-object v1

    .line 107
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->g:Lo/aK;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->g:Lo/aK;

    invoke-virtual {v1, v0}, Lo/aK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 108
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->i:Ljava/util/List;

    .line 119
    :goto_13
    return-object v0

    .line 110
    :cond_14
    invoke-virtual {v1}, Lo/aK;->a()Lo/aL;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/cL;->c(Lcom/google/android/maps/driveabout/vector/k;)I

    move-result v2

    invoke-static {v0, v2}, Lo/am;->b(Lo/aL;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 113
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3d

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->j()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3d

    const/4 v0, 0x1

    .line 114
    :goto_31
    if-nez v0, :cond_36

    .line 115
    invoke-virtual {p0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cL;->a(Lo/aK;Ljava/util/ArrayList;)V

    .line 117
    :cond_36
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/cL;->g:Lo/aK;

    .line 118
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/cL;->i:Ljava/util/List;

    .line 119
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cL;->i:Ljava/util/List;

    goto :goto_13

    .line 113
    :cond_3d
    const/4 v0, 0x0

    goto :goto_31
.end method

.method protected c(Lcom/google/android/maps/driveabout/vector/k;)I
    .registers 6
    .parameter

    .prologue
    .line 154
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v0

    .line 155
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cL;->c:Lcom/google/android/maps/driveabout/vector/el;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cL;->a:Lcom/google/android/maps/driveabout/vector/di;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/el;->a(Lo/Q;Lcom/google/android/maps/driveabout/vector/di;)Lcom/google/android/maps/driveabout/vector/ek;

    move-result-object v1

    .line 156
    if-eqz v1, :cond_17

    .line 157
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/ek;->a(F)I

    move-result v0

    .line 160
    :goto_16
    return v0

    :cond_17
    float-to-int v0, v0

    goto :goto_16
.end method
