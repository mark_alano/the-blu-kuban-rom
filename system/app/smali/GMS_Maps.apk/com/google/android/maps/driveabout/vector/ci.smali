.class public Lcom/google/android/maps/driveabout/vector/ci;
.super Lcom/google/android/maps/driveabout/vector/aD;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:Lcom/google/android/maps/driveabout/vector/cX;

.field private final c:Lv/k;

.field private d:Lo/Q;

.field private e:F

.field private final f:I

.field private final g:I

.field private final h:I

.field private volatile i:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 4
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aD;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->i:Z

    .line 36
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/ci;->a:Landroid/content/res/Resources;

    .line 37
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->f:I

    .line 38
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->g:I

    .line 39
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->h:I

    .line 40
    new-instance v0, Lv/k;

    invoke-direct {v0}, Lv/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->c:Lv/k;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->i:Z

    if-nez v0, :cond_7

    .line 107
    :cond_6
    :goto_6
    return-void

    .line 76
    :cond_7
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/E;->b()I

    move-result v0

    if-gtz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->d:Lo/Q;

    if-eqz v0, :cond_6

    .line 81
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    if-nez v0, :cond_2b

    .line 82
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cX;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/vector/cX;-><init>(Lcom/google/android/maps/driveabout/vector/aV;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    .line 83
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/cX;->c(Z)V

    .line 84
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->a:Landroid/content/res/Resources;

    const v2, 0x7f020172

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cX;->a(Landroid/content/res/Resources;I)V

    .line 88
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->o()V

    .line 89
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->x()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 90
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 92
    const/16 v1, 0x303

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 93
    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/cO;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/cO;->d(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 96
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 97
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->d:Lo/Q;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ci;->e:F

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/dB;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lo/Q;F)V

    .line 98
    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/vector/dB;->a(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/maps/driveabout/vector/k;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->c:Lv/k;

    invoke-virtual {v1, p1}, Lv/k;->a(Lcom/google/android/maps/driveabout/vector/aV;)F

    move-result v1

    const/high16 v2, 0x3f80

    invoke-interface {v0, v1, v4, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 102
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/cX;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 103
    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/aV;->i:Lcom/google/android/maps/driveabout/vector/eb;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/eb;->d(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 104
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 106
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_6
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/aV;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->e()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ci;->g:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->f:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ci;->h:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->d(FF)Lo/Q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->d:Lo/Q;

    .line 54
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->d:Lo/Q;

    if-eqz v0, :cond_2b

    .line 55
    const/4 v0, 0x0

    .line 56
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/ci;->f:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/ci;->d:Lo/Q;

    invoke-virtual {p1, v2, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(Lo/Q;Z)F

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->e:F

    .line 59
    :cond_2b
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lcom/google/android/maps/driveabout/vector/aV;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    if-eqz v0, :cond_c

    .line 65
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cX;->g()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->b:Lcom/google/android/maps/driveabout/vector/cX;

    .line 68
    :cond_c
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/ci;->i:Z

    .line 111
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/ci;->i:Z

    return v0
.end method

.method public u_()I
    .registers 2

    .prologue
    .line 45
    const v0, 0x900b0

    return v0
.end method
