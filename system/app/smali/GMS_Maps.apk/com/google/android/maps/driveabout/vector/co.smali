.class public Lcom/google/android/maps/driveabout/vector/co;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field private final g:Ljava/lang/Boolean;

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 950
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/co;->g:Ljava/lang/Boolean;

    .line 951
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/co;->h:Ljava/lang/Boolean;

    .line 952
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/co;->i:Ljava/lang/Boolean;

    .line 953
    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    .line 954
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/co;->b:I

    .line 955
    iput p6, p0, Lcom/google/android/maps/driveabout/vector/co;->c:I

    .line 956
    iput p7, p0, Lcom/google/android/maps/driveabout/vector/co;->d:I

    .line 957
    iput p8, p0, Lcom/google/android/maps/driveabout/vector/co;->e:I

    .line 958
    iput p9, p0, Lcom/google/android/maps/driveabout/vector/co;->f:I

    .line 959
    return-void
.end method

.method public static a()Lcom/google/android/maps/driveabout/vector/cp;
    .registers 1

    .prologue
    .line 975
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cp;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/cp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(ZZZ)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 962
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_e

    .line 971
    :cond_d
    :goto_d
    return v0

    .line 965
    :cond_e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p2, :cond_d

    .line 968
    :cond_1a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/co;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p3, :cond_d

    .line 971
    :cond_26
    const/4 v0, 0x1

    goto :goto_d
.end method
