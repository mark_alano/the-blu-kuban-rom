.class public Lcom/google/android/maps/driveabout/vector/cz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/bu;
.implements Lcom/google/android/maps/driveabout/vector/o;


# static fields
.field public static volatile a:Z

.field static final b:Ljava/lang/ThreadLocal;

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final l:Ljava/util/Comparator;


# instance fields
.field private final A:Lcom/google/android/maps/driveabout/vector/bY;

.field private final B:Lcom/google/android/maps/driveabout/vector/A;

.field private final C:Lcom/google/android/maps/driveabout/vector/S;

.field private final D:Lz/b;

.field private E:Lz/c;

.field private final F:Ljava/util/HashSet;

.field private final G:Ljava/util/HashSet;

.field private final H:[I

.field private final I:Ljava/util/List;

.field private J:J

.field private K:Z

.field private final L:Lcom/google/android/maps/driveabout/vector/df;

.field private M:Ljava/util/List;

.field private N:Z

.field private O:Landroid/graphics/Bitmap;

.field private P:Z

.field private Q:F

.field private R:J

.field private volatile S:Lcom/google/android/maps/driveabout/vector/D;

.field private final T:Ljava/util/List;

.field private U:Ljava/util/List;

.field private V:Lcom/google/android/maps/driveabout/vector/cv;

.field private volatile W:Lcom/google/android/maps/driveabout/vector/cv;

.field private volatile X:Lcom/google/android/maps/driveabout/vector/cv;

.field private volatile Y:Z

.field private Z:J

.field private aa:I

.field private volatile ab:Lcom/google/android/maps/driveabout/vector/dd;

.field private ac:Z

.field private volatile ad:F

.field private volatile ae:Z

.field private af:Z

.field private ag:Ljava/lang/Object;

.field private ah:Z

.field private volatile ai:I

.field private aj:Z

.field private ak:I

.field private al:J

.field private am:Z

.field private an:LA/q;

.field private final ao:LA/j;

.field private volatile ap:J

.field private aq:Ljava/lang/Object;

.field protected c:Ljava/util/Map;

.field protected d:Ljava/util/List;

.field protected e:Z

.field private volatile j:Lcom/google/android/maps/driveabout/vector/ca;

.field private volatile k:Lcom/google/android/maps/driveabout/vector/l;

.field private m:Lcom/google/android/maps/driveabout/vector/aV;

.field private volatile n:I

.field private volatile o:I

.field private final p:Ljava/util/LinkedList;

.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:Lcom/google/android/maps/driveabout/vector/k;

.field private final u:Lcom/google/android/maps/driveabout/vector/I;

.field private final v:Lcom/google/android/maps/driveabout/vector/cq;

.field private final w:Landroid/content/res/Resources;

.field private final x:F

.field private y:Lcom/google/android/maps/driveabout/vector/cc;

.field private final z:Lcom/google/android/maps/driveabout/vector/bY;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x4

    .line 77
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/cz;->a:Z

    .line 83
    new-array v0, v1, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->f:[I

    .line 88
    new-array v0, v1, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->g:[I

    .line 93
    new-array v0, v1, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->h:[I

    .line 98
    new-array v0, v1, [I

    fill-array-data v0, :array_54

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->i:[I

    .line 120
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cA;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/cA;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->b:Ljava/lang/ThreadLocal;

    .line 136
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cB;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/cB;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cz;->l:Ljava/util/Comparator;

    return-void

    .line 83
    nop

    :array_30
    .array-data 0x4
        0x0t 0xedt 0x0t 0x0t
        0x0t 0xeat 0x0t 0x0t
        0x0t 0xe2t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 88
    :array_3c
    .array-data 0x4
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 93
    :array_48
    .array-data 0x4
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 98
    :array_54
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/I;Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/dd;LA/j;LA/q;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->o:I

    .line 247
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->F:Ljava/util/HashSet;

    .line 253
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->G:Ljava/util/HashSet;

    .line 259
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->I:Ljava/util/List;

    .line 268
    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->J:J

    .line 276
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cC;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/cC;-><init>(Lcom/google/android/maps/driveabout/vector/cz;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->L:Lcom/google/android/maps/driveabout/vector/df;

    .line 332
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->T:Ljava/util/List;

    .line 338
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->U:Ljava/util/List;

    .line 401
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ag:Ljava/lang/Object;

    .line 406
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ah:Z

    .line 419
    iput v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ai:I

    .line 427
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->aj:Z

    .line 432
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    .line 437
    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->al:J

    .line 481
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->c:Ljava/util/Map;

    .line 487
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    .line 489
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    .line 754
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    .line 759
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->aq:Ljava/lang/Object;

    .line 530
    sget-object v0, Lcom/google/android/maps/driveabout/vector/ca;->s:Lcom/google/android/maps/driveabout/vector/ca;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->j:Lcom/google/android/maps/driveabout/vector/ca;

    .line 531
    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    .line 532
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    .line 533
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cq;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/cq;-><init>(Lcom/google/android/maps/driveabout/vector/cz;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->v:Lcom/google/android/maps/driveabout/vector/cq;

    .line 534
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/cz;->w:Landroid/content/res/Resources;

    .line 535
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->x:F

    .line 536
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->x:F

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/ax;->a(F)V

    .line 537
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->x:F

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/an;->a(F)V

    .line 539
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    .line 540
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    .line 543
    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    .line 544
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    .line 545
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->s:Ljava/util/ArrayList;

    .line 552
    new-instance v0, Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, p2}, Lcom/google/android/maps/driveabout/vector/S;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    .line 553
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bY;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bY;-><init>(ILcom/google/android/maps/driveabout/vector/S;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->z:Lcom/google/android/maps/driveabout/vector/bY;

    .line 554
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bY;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, v3, v1}, Lcom/google/android/maps/driveabout/vector/bY;-><init>(ILcom/google/android/maps/driveabout/vector/S;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->A:Lcom/google/android/maps/driveabout/vector/bY;

    .line 555
    new-instance v0, Lcom/google/android/maps/driveabout/vector/A;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/A;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->B:Lcom/google/android/maps/driveabout/vector/A;

    .line 556
    new-instance v0, Lz/b;

    invoke-direct {v0}, Lz/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->D:Lz/b;

    .line 558
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->z:Lcom/google/android/maps/driveabout/vector/bY;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 560
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 561
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->D:Lz/b;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 562
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->B:Lcom/google/android/maps/driveabout/vector/A;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 564
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aU;

    const v1, 0x1d4c0

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aU;-><init>(I)V

    .line 566
    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    const/high16 v2, -0x8000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aU;->a(Lcom/google/android/maps/driveabout/vector/D;I)V

    .line 567
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 568
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aT;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aT;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 570
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dF;->a()Z

    move-result v0

    if-eqz v0, :cond_138

    .line 571
    new-instance v0, Lz/c;

    invoke-direct {v0, p2}, Lz/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    .line 572
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->D:Lz/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    invoke-virtual {v0, v1}, Lz/b;->a(Lz/a;)V

    .line 584
    :goto_121
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->L:Lcom/google/android/maps/driveabout/vector/df;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/df;)V

    .line 587
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dd;->b(Z)V

    .line 592
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->R:J

    .line 594
    iput-object p5, p0, Lcom/google/android/maps/driveabout/vector/cz;->ao:LA/j;

    .line 595
    iput-object p6, p0, Lcom/google/android/maps/driveabout/vector/cz;->an:LA/q;

    .line 605
    return-void

    .line 574
    :cond_138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    goto :goto_121
.end method

.method static a(IIF)F
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 930
    add-int v0, p0, p1

    .line 931
    int-to-float v0, v0

    const/high16 v1, 0x4380

    mul-float/2addr v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    .line 935
    const/high16 v1, 0x3f80

    add-float/2addr v0, v1

    .line 940
    const/high16 v1, 0x4000

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/k;->a(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/cz;)Lz/c;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    return-object v0
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/aV;)V
    .registers 5
    .parameter

    .prologue
    .line 641
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 642
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aD;

    .line 643
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aD;->a_(Lcom/google/android/maps/driveabout/vector/aV;)V

    goto :goto_9

    .line 646
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    .line 645
    :cond_1c
    const/4 v0, 0x0

    :try_start_1d
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->aj:Z

    .line 646
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_19

    .line 647
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/k;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1859
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->e()I

    move-result v0

    .line 1860
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->f()I

    move-result v1

    .line 1862
    if-lez v0, :cond_2b

    if-lez v1, :cond_2b

    .line 1868
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, v2, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 1869
    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 1870
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 1871
    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    .line 1873
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->u()[F

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 1877
    const/16 v3, 0xc11

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 1878
    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    .line 1881
    :cond_2b
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/k;IZZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1387
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->u()Lcom/google/android/maps/driveabout/vector/cE;

    move-result-object v8

    .line 1389
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1391
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->b()[Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_10
    if-ge v0, v2, :cond_1a

    aget-object v3, v1, v0

    .line 1392
    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/dd;->h()V

    .line 1391
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 1397
    :cond_1a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->A()V

    .line 1399
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    .line 1400
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->h()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->Q:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_34

    .line 1401
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/k;)V

    .line 1402
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->h()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->Q:F

    .line 1404
    :cond_34
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/cz;->b(Lcom/google/android/maps/driveabout/vector/k;)V

    .line 1408
    const/4 v0, 0x0

    :goto_38
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_4b

    .line 1409
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)V

    .line 1408
    add-int/lit8 v0, v0, 0x1

    goto :goto_38

    .line 1416
    :cond_4b
    if-eqz p3, :cond_b3

    .line 1417
    const/4 v0, 0x0

    :goto_4e
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_63

    .line 1418
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v1, p1, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/aV;)Z

    .line 1417
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 1420
    :cond_63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1421
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/cz;->a:Z

    if-eqz v0, :cond_ac

    .line 1422
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->b()[Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_73
    if-ge v3, v5, :cond_ac

    aget-object v0, v4, v3

    .line 1423
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7f
    :goto_7f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aW;

    .line 1424
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/aW;->e()Lcom/google/android/maps/driveabout/vector/ad;

    move-result-object v1

    .line 1425
    if-nez v1, :cond_9a

    .line 1426
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/maps/driveabout/vector/cc;->a(Lcom/google/android/maps/driveabout/vector/aW;Lcom/google/android/maps/driveabout/vector/k;)Lcom/google/android/maps/driveabout/vector/ad;

    move-result-object v1

    .line 1427
    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/aW;->a(Lcom/google/android/maps/driveabout/vector/ad;)V

    :cond_9a
    move-object v0, v1

    .line 1429
    if-eqz v0, :cond_7f

    .line 1430
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/ad;->a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/aV;)Z

    .line 1431
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7f

    .line 1422
    :cond_a8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_73

    .line 1436
    :cond_ac
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->B:Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->I:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/util/List;)V

    .line 1447
    :cond_b3
    if-eqz p4, :cond_c4

    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->f:Lcom/google/android/maps/driveabout/vector/D;

    if-eq v2, v0, :cond_c4

    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->e:Lcom/google/android/maps/driveabout/vector/D;

    if-eq v2, v0, :cond_c4

    .line 1448
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->b()[Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/k;IZ[Lcom/google/android/maps/driveabout/vector/dd;)V

    .line 1454
    :cond_c4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    if-eqz v0, :cond_11a

    .line 1456
    monitor-enter p0

    .line 1457
    :try_start_c9
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->K:Z

    .line 1458
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->K:Z

    .line 1459
    monitor-exit p0
    :try_end_cf
    .catchall {:try_start_c9 .. :try_end_cf} :catchall_10c

    .line 1460
    if-eqz v0, :cond_11a

    .line 1461
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->F:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1462
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->G:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1463
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    const/4 v1, 0x0

    const/4 v3, -0x1

    aput v3, v0, v1

    .line 1464
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 1465
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->b()[Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_ed
    if-ge v7, v10, :cond_10f

    aget-object v0, v9, v7

    .line 1466
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->F:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->G:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/D;Ljava/util/HashSet;Ljava/util/HashSet;[I)V

    .line 1468
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-le v0, v6, :cond_239

    .line 1469
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1465
    :goto_107
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_ed

    .line 1459
    :catchall_10c
    move-exception v0

    :try_start_10d
    monitor-exit p0
    :try_end_10e
    .catchall {:try_start_10d .. :try_end_10e} :catchall_10c

    throw v0

    .line 1472
    :cond_10f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->F:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->G:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1, v3, v6, v4}, Lz/c;->a(Ljava/util/HashSet;Ljava/util/HashSet;ILcom/google/android/maps/driveabout/vector/D;)V

    .line 1477
    :cond_11a
    if-nez p3, :cond_11e

    if-eqz p2, :cond_197

    :cond_11e
    const/4 v0, 0x1

    :goto_11f
    invoke-virtual {p0, v8, v0}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/cE;Z)V

    .line 1479
    new-instance v3, Lcom/google/android/maps/driveabout/vector/cI;

    const/4 v0, 0x0

    invoke-direct {v3, v2, v0}, Lcom/google/android/maps/driveabout/vector/cI;-><init>(Lcom/google/android/maps/driveabout/vector/D;I)V

    .line 1481
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->am:Z

    .line 1497
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->l()V

    .line 1498
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->q()V

    .line 1502
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13b
    :goto_13b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cw;

    .line 1503
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/cI;->a(Lcom/google/android/maps/driveabout/vector/cw;)V

    .line 1504
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    .line 1506
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v4, :cond_157

    .line 1507
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v4, v1}, Lcom/google/android/maps/driveabout/vector/cv;->a(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 1510
    :cond_157
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->b()Lcom/google/android/maps/driveabout/vector/cx;

    move-result-object v0

    sget-object v4, Lcom/google/android/maps/driveabout/vector/cx;->a:Lcom/google/android/maps/driveabout/vector/cx;

    if-ne v0, v4, :cond_16a

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->m()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sget-object v4, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    if-ne v0, v4, :cond_16a

    .line 1515
    :cond_16a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->y()V

    .line 1516
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v1, v0, p1, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V

    .line 1517
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->z()V

    .line 1522
    instance-of v0, v1, Lcom/google/android/maps/driveabout/vector/dd;

    if-eqz v0, :cond_18d

    .line 1523
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->am:Z

    if-eqz v0, :cond_199

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->o()Z

    move-result v0

    if-eqz v0, :cond_199

    const/4 v0, 0x1

    :goto_18b
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->am:Z

    .line 1527
    :cond_18d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v0, :cond_13b

    .line 1528
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cv;->b(Lcom/google/android/maps/driveabout/vector/aD;)V

    goto :goto_13b

    .line 1477
    :cond_197
    const/4 v0, 0x0

    goto :goto_11f

    .line 1523
    :cond_199
    const/4 v0, 0x0

    goto :goto_18b

    .line 1533
    :cond_19b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->A()V

    .line 1536
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    .line 1537
    if-eqz v0, :cond_21d

    .line 1538
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1539
    const/16 v2, 0x505

    if-ne v0, v2, :cond_1d7

    .line 1540
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->Z:J

    sub-long/2addr v2, v4

    .line 1541
    const-string v4, "\nTime in current GL context: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1542
    invoke-static {}, Lk/a;->a()Lk/a;

    move-result-object v2

    invoke-virtual {v2}, Lk/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1543
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->Y:Z

    .line 1545
    :cond_1d7
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GL Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Li/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawFrameInternal GL ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    :cond_21d
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->Y:Z

    if-eqz v1, :cond_221

    .line 1555
    :cond_221
    const/16 v1, 0x505

    if-ne v0, v1, :cond_235

    .line 1557
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_22b
    if-ge v0, v2, :cond_235

    aget-object v3, v1, v0

    .line 1558
    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aD;->b()V

    .line 1557
    add-int/lit8 v0, v0, 0x1

    goto :goto_22b

    .line 1561
    :cond_235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->Y:Z

    .line 1562
    return-void

    :cond_239
    move v0, v6

    goto/16 :goto_107
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/k;IZ[Lcom/google/android/maps/driveabout/vector/dd;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v0, :cond_e

    .line 1772
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    if-nez p3, :cond_32

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/cv;->a(Lcom/google/android/maps/driveabout/vector/cc;Z)V

    .line 1775
    :cond_e
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_34

    const/4 v0, 0x1

    .line 1777
    :goto_13
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v1, p2}, Lcom/google/android/maps/driveabout/vector/cc;->a(I)V

    .line 1778
    if-eqz v0, :cond_36

    .line 1781
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->w()Lo/aK;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cc;->a(Lo/aM;)V

    .line 1782
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->P:Z

    .line 1830
    :goto_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v0, :cond_31

    .line 1831
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cv;->a(Lcom/google/android/maps/driveabout/vector/cc;)V

    .line 1833
    :cond_31
    return-void

    .line 1772
    :cond_32
    const/4 v0, 0x0

    goto :goto_b

    .line 1775
    :cond_34
    const/4 v0, 0x0

    goto :goto_13

    .line 1784
    :cond_36
    if-nez p3, :cond_44

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->P:Z

    if-nez v0, :cond_44

    .line 1788
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cc;->b(I)V

    goto :goto_26

    .line 1792
    :cond_44
    new-instance v5, Lcom/google/android/maps/driveabout/vector/cu;

    invoke-direct {v5}, Lcom/google/android/maps/driveabout/vector/cu;-><init>()V

    .line 1793
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1794
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->w()Lo/aK;

    move-result-object v2

    .line 1795
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_80

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->j()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_80

    const/4 v0, 0x1

    .line 1798
    :goto_65
    if-eqz v0, :cond_82

    const/4 v0, 0x0

    .line 1799
    :goto_68
    const/4 v3, 0x0

    .line 1800
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v7

    .line 1801
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v8

    .line 1803
    array-length v4, p4

    const/4 v1, 0x0

    :goto_73
    if-ge v1, v4, :cond_90

    aget-object v9, p4, v1

    .line 1804
    invoke-virtual {v9}, Lcom/google/android/maps/driveabout/vector/dd;->i()Z

    move-result v10

    if-nez v10, :cond_84

    .line 1803
    :goto_7d
    add-int/lit8 v1, v1, 0x1

    goto :goto_73

    .line 1795
    :cond_80
    const/4 v0, 0x0

    goto :goto_65

    :cond_82
    move-object v0, v2

    .line 1798
    goto :goto_68

    .line 1807
    :cond_84
    invoke-virtual {v9, v0, v5, v6}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lo/aK;Lcom/google/android/maps/driveabout/vector/cu;Ljava/util/Set;)I

    move-result v10

    .line 1809
    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1810
    invoke-virtual {v9, v7, v8}, Lcom/google/android/maps/driveabout/vector/dd;->a(Ljava/util/Set;Ljava/util/Map;)V

    goto :goto_7d

    .line 1813
    :cond_90
    monitor-enter p0

    .line 1814
    :try_start_91
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->M:Ljava/util/List;

    if-eqz v0, :cond_af

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1816
    :goto_9b
    monitor-exit p0
    :try_end_9c
    .catchall {:try_start_91 .. :try_end_9c} :catchall_b1

    .line 1817
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    const/16 v9, 0x14

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dd;->m()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v10

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/maps/driveabout/vector/cc;->a(Lcom/google/android/maps/driveabout/vector/k;Lo/aM;ILjava/util/Iterator;Lcom/google/android/maps/driveabout/vector/cu;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILcom/google/android/maps/driveabout/vector/di;)V

    .line 1827
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->P:Z

    goto/16 :goto_26

    .line 1814
    :cond_af
    const/4 v4, 0x0

    goto :goto_9b

    .line 1816
    :catchall_b1
    move-exception v0

    :try_start_b2
    monitor-exit p0
    :try_end_b3
    .catchall {:try_start_b2 .. :try_end_b3} :catchall_b1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/cz;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->K:Z

    return p1
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/D;)[I
    .registers 3
    .parameter

    .prologue
    .line 508
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cD;->a:[I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/D;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1e

    .line 516
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->i:[I

    :goto_d
    return-object v0

    .line 509
    :pswitch_e
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->f:[I

    goto :goto_d

    .line 510
    :pswitch_11
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->f:[I

    goto :goto_d

    .line 513
    :pswitch_14
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->f:[I

    goto :goto_d

    .line 514
    :pswitch_17
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->g:[I

    goto :goto_d

    .line 515
    :pswitch_1a
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cz;->h:[I

    goto :goto_d

    .line 508
    nop

    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
    .end packed-switch
.end method

.method private b(Lcom/google/android/maps/driveabout/vector/k;)V
    .registers 5
    .parameter

    .prologue
    .line 1890
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 1891
    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 1892
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 1893
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->t()[F

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 1894
    return-void
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 1837
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 1838
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/D;)[I

    move-result-object v0

    .line 1839
    aget v2, v0, v6

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClearColorx(IIII)V

    .line 1840
    const/16 v0, 0x4000

    .line 1842
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aV;->g()Z

    move-result v2

    if-eqz v2, :cond_28

    .line 1843
    const/16 v0, 0x4100

    .line 1844
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aV;->h()V

    .line 1847
    :cond_28
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aV;->i()Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 1848
    invoke-interface {v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glClearStencil(I)V

    .line 1849
    or-int/lit16 v0, v0, 0x400

    .line 1850
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aV;->j()V

    .line 1853
    :cond_3a
    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 1856
    return-void
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/aD;)V
    .registers 7
    .parameter

    .prologue
    .line 1623
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 1624
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/dd;

    if-eqz v0, :cond_5a

    move-object v0, p1

    .line 1625
    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1626
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/aD;

    .line 1627
    instance-of v3, v1, Lcom/google/android/maps/driveabout/vector/dd;

    if-eqz v3, :cond_13

    .line 1628
    check-cast v1, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1629
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dd;->q()Lk/e;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->q()Lk/e;

    move-result-object v4

    if-ne v3, v4, :cond_13

    .line 1631
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The added tile Overlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->m()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " shares the same GLTileCache with "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dd;->m()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1643
    :cond_5a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    .line 1644
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1645
    return-void
.end method

.method private d(I)V
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1293
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->c()Z

    move-result v0

    if-eqz v0, :cond_cc

    :cond_e
    move v0, v2

    .line 1296
    :goto_f
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v3, v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/I;->a(Z)V

    .line 1298
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/I;->h()Z

    move-result v0

    .line 1299
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/k;->a()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/maps/driveabout/vector/cz;->J:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_cf

    move v3, v2

    .line 1300
    :goto_29
    if-eqz v3, :cond_34

    .line 1301
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/k;->a()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->J:J

    move v0, v2

    .line 1305
    :cond_34
    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/vector/cz;->d(Z)V

    .line 1307
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aV;->e()V

    .line 1310
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->v()V

    .line 1313
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->W:Lcom/google/android/maps/driveabout/vector/cv;

    iput-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    .line 1314
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v3, :cond_51

    .line 1315
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v3, p0}, Lcom/google/android/maps/driveabout/vector/cv;->a(Lcom/google/android/maps/driveabout/vector/cz;)V

    .line 1316
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/cv;->a()V

    .line 1319
    :cond_51
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->X:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v3, :cond_5a

    .line 1320
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->X:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/cv;->a()V

    .line 1326
    :cond_5a
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/k;->i()F

    move-result v3

    const/high16 v4, 0x3f80

    cmpl-float v3, v3, v4

    if-lez v3, :cond_70

    .line 1328
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-direct {p0, v3, p1, v0, v2}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/k;IZZ)V

    .line 1329
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aV;->f()V

    .line 1332
    :cond_70
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->am:Z

    if-eqz v3, :cond_d2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/cc;->c()Z

    move-result v3

    if-nez v3, :cond_d2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aV;->c()Z

    move-result v3

    if-nez v3, :cond_d2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/I;->i()Z

    move-result v3

    if-nez v3, :cond_d2

    .line 1337
    :goto_8c
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    if-eqz v3, :cond_a2

    .line 1338
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/vector/cv;->b(Z)V

    .line 1339
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->V:Lcom/google/android/maps/driveabout/vector/cv;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/cv;->b()Z

    move-result v2

    if-eqz v2, :cond_a2

    .line 1340
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v2, v1, v1}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1345
    :cond_a2
    monitor-enter p0

    .line 1346
    :try_start_a3
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->N:Z

    .line 1347
    monitor-exit p0
    :try_end_a6
    .catchall {:try_start_a3 .. :try_end_a6} :catchall_d4

    .line 1349
    if-eqz v2, :cond_b6

    .line 1350
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->t()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1351
    monitor-enter p0

    .line 1352
    :try_start_ad
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->O:Landroid/graphics/Bitmap;

    .line 1353
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->N:Z

    .line 1354
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1355
    monitor-exit p0
    :try_end_b6
    .catchall {:try_start_ad .. :try_end_b6} :catchall_d7

    .line 1358
    :cond_b6
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/cc;->c()Z

    move-result v2

    if-nez v2, :cond_c6

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aV;->c()Z

    move-result v2

    if-eqz v2, :cond_da

    .line 1360
    :cond_c6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1378
    :cond_cb
    :goto_cb
    return-void

    :cond_cc
    move v0, v1

    .line 1293
    goto/16 :goto_f

    :cond_cf
    move v3, v1

    .line 1299
    goto/16 :goto_29

    :cond_d2
    move v2, v1

    .line 1332
    goto :goto_8c

    .line 1347
    :catchall_d4
    move-exception v0

    :try_start_d5
    monitor-exit p0
    :try_end_d6
    .catchall {:try_start_d5 .. :try_end_d6} :catchall_d4

    throw v0

    .line 1355
    :catchall_d7
    move-exception v0

    :try_start_d8
    monitor-exit p0
    :try_end_d9
    .catchall {:try_start_d8 .. :try_end_d9} :catchall_d7

    throw v0

    .line 1364
    :cond_da
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_f7

    if-nez v0, :cond_f7

    if-nez p1, :cond_f7

    .line 1369
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1370
    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->R:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v0, v4, v6

    if-lez v0, :cond_f7

    .line 1371
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1372
    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->R:J

    .line 1375
    :cond_f7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_cb

    .line 1376
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    goto :goto_cb
.end method

.method private declared-synchronized d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1572
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cF;

    .line 1573
    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/vector/cF;->a(Z)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 1572
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1575
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method private r()V
    .registers 2

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    if-eqz v0, :cond_9

    .line 651
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cc;->a()V

    .line 653
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    if-eqz v0, :cond_25

    .line 654
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->E()Lcom/google/android/maps/driveabout/vector/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/r;->a()V

    .line 655
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/cZ;->a(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 656
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->A()V

    .line 657
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->B()V

    .line 659
    :cond_25
    return-void
.end method

.method private s()V
    .registers 5

    .prologue
    .line 979
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->n:I

    if-gez v0, :cond_5

    .line 989
    :goto_4
    return-void

    .line 982
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ac:Z

    if-eqz v0, :cond_2b

    const/16 v0, 0xa

    .line 984
    :goto_b
    :try_start_b
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->n:I

    invoke-static {v1, v0}, Landroid/os/Process;->setThreadPriority(II)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_10} :catch_11

    goto :goto_4

    .line 986
    :catch_11
    move-exception v0

    .line 987
    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 982
    :cond_2b
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->o:I

    goto :goto_b
.end method

.method private t()Landroid/graphics/Bitmap;
    .registers 15

    .prologue
    const/4 v1, 0x0

    .line 1603
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 1604
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/k;->e()I

    move-result v3

    .line 1605
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/k;->f()I

    move-result v4

    .line 1606
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->v:Lcom/google/android/maps/driveabout/vector/cq;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/maps/driveabout/vector/cq;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 1608
    invoke-static {v13}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 1609
    invoke-static {v13}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 1610
    mul-int v2, v3, v4

    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v7

    move v2, v1

    .line 1611
    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1612
    invoke-virtual {v7}, Ljava/nio/IntBuffer;->array()[I

    move-result-object v6

    move-object v5, v13

    move v7, v1

    move v8, v3

    move v9, v1

    move v10, v1

    move v11, v3

    move v12, v4

    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1613
    return-object v13
.end method

.method private u()Lcom/google/android/maps/driveabout/vector/cE;
    .registers 10

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 1665
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v7

    .line 1666
    :try_start_6
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->aj:Z

    if-nez v0, :cond_2a

    .line 1669
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aD;

    .line 1670
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/cJ;)V

    goto :goto_10

    .line 1762
    :catchall_24
    move-exception v0

    monitor-exit v7
    :try_end_26
    .catchall {:try_start_6 .. :try_end_26} :catchall_24

    throw v0

    .line 1672
    :cond_27
    const/4 v0, 0x1

    :try_start_28
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->aj:Z

    :cond_2a
    move v6, v1

    move v2, v1

    move v4, v1

    .line 1678
    :goto_2d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_108

    .line 1679
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    .line 1680
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cG;

    .line 1681
    sget-object v1, Lcom/google/android/maps/driveabout/vector/cD;->b:[I

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/cG;->b:Lcom/google/android/maps/driveabout/vector/cH;

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/cH;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_168

    :cond_4d
    :goto_4d
    move v0, v2

    move v1, v4

    .line 1678
    :goto_4f
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    move v2, v0

    goto :goto_2d

    .line 1683
    :pswitch_55
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4d

    .line 1686
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a_(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 1687
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/dd;

    if-eqz v1, :cond_164

    .line 1688
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    check-cast v1, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1689
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->L:Lcom/google/android/maps/driveabout/vector/df;

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/df;)V

    .line 1690
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1692
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dd;->u_()I

    move-result v4

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    if-ge v4, v8, :cond_9e

    move v4, v3

    .line 1701
    :goto_82
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dd;->j()Z

    move-result v2

    if-eqz v2, :cond_a4

    .line 1702
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_8e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/driveabout/vector/di;

    .line 1703
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/di;)V

    goto :goto_8e

    .line 1697
    :cond_9e
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/dd;->b(Z)V

    move v4, v2

    goto :goto_82

    :cond_a4
    move v1, v4

    .line 1707
    :goto_a5
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 1708
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v2

    .line 1709
    if-eqz v2, :cond_b5

    .line 1710
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/b;)V

    .line 1712
    :cond_b5
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/cJ;)V

    move v0, v1

    move v1, v3

    .line 1714
    goto :goto_4f

    .line 1717
    :pswitch_c1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 1718
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/dd;

    if-eqz v1, :cond_162

    .line 1719
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->u_()I

    move-result v1

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    if-ne v1, v8, :cond_15f

    move v1, v3

    .line 1724
    :goto_dc
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1726
    :goto_e3
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->c:Ljava/util/Map;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-interface {v2, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1727
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v2, v8}, Lcom/google/android/maps/driveabout/vector/aD;->a_(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 1728
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aD;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v0

    .line 1729
    if-eqz v0, :cond_fc

    .line 1730
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->b(Lcom/google/android/maps/driveabout/vector/b;)V

    :cond_fc
    move v0, v1

    move v1, v4

    .line 1732
    goto/16 :goto_4f

    .line 1735
    :pswitch_100
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/cG;->a:Lcom/google/android/maps/driveabout/vector/aD;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    goto/16 :goto_4d

    .line 1739
    :cond_108
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1741
    if-eqz v2, :cond_141

    .line 1744
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    .line 1746
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v5

    :goto_11b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1747
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/dd;->b(Z)V

    .line 1748
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->u_()I

    move-result v5

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    if-ge v5, v6, :cond_15d

    .line 1749
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->u_()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ak:I

    :goto_139
    move-object v1, v0

    .line 1750
    goto :goto_11b

    .line 1753
    :cond_13b
    if-eqz v1, :cond_141

    .line 1754
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/dd;->b(Z)V

    .line 1758
    :cond_141
    if-eqz v4, :cond_152

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_152

    .line 1759
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cz;->l:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1761
    :cond_152
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cE;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cE;-><init>(Ljava/util/List;Ljava/util/List;)V

    monitor-exit v7
    :try_end_15c
    .catchall {:try_start_28 .. :try_end_15c} :catchall_24

    return-object v0

    :cond_15d
    move-object v0, v1

    goto :goto_139

    :cond_15f
    move v1, v2

    goto/16 :goto_dc

    :cond_162
    move v1, v2

    goto :goto_e3

    :cond_164
    move v1, v2

    goto/16 :goto_a5

    .line 1681
    nop

    :pswitch_data_168
    .packed-switch 0x1
        :pswitch_55
        :pswitch_c1
        :pswitch_100
    .end packed-switch
.end method

.method private v()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1960
    monitor-enter p0

    .line 1961
    :try_start_2
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->aa:I

    .line 1962
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->aa:I

    .line 1963
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_30

    .line 1965
    if-eqz v1, :cond_36

    .line 1966
    const/4 v2, 0x2

    if-ne v1, v2, :cond_33

    const/4 v0, 0x1

    move v1, v0

    .line 1970
    :goto_f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cc;->a(Z)V

    .line 1973
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1974
    :try_start_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aD;

    .line 1975
    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)V

    goto :goto_1d

    .line 1977
    :catchall_2d
    move-exception v0

    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_2d

    throw v0

    .line 1963
    :catchall_30
    move-exception v0

    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    throw v0

    :cond_33
    move v1, v0

    .line 1966
    goto :goto_f

    .line 1977
    :cond_35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_2d

    .line 1979
    :cond_36
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 1185
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ad:F

    return v0
.end method

.method public a(Lo/Q;)F
    .registers 6
    .parameter

    .prologue
    .line 1171
    const/high16 v0, 0x41a8

    .line 1172
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1173
    :try_start_5
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1174
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dd;->b(Lo/Q;)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v0

    goto :goto_c

    .line 1176
    :cond_22
    monitor-exit v2

    .line 1177
    return v1

    .line 1176
    :catchall_24
    move-exception v0

    monitor-exit v2
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_24

    throw v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 945
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->o:I

    .line 946
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->s()V

    .line 947
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    if-eqz v0, :cond_9

    .line 1936
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->E:Lz/c;

    invoke-virtual {v0, p1, p2}, Lz/c;->a(II)V

    .line 1938
    :cond_9
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/T;)V
    .registers 3
    .parameter

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/S;->a(Lcom/google/android/maps/driveabout/vector/T;)V

    .line 1052
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aD;)V
    .registers 5
    .parameter

    .prologue
    .line 1097
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1099
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cG;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cH;->a:Lcom/google/android/maps/driveabout/vector/cH;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/cG;-><init>(Lcom/google/android/maps/driveabout/vector/cH;Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 1100
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1101
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_18

    .line 1102
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1103
    return-void

    .line 1101
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160
    return-void
.end method

.method a(Lcom/google/android/maps/driveabout/vector/cE;Z)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1237
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v5

    .line 1238
    :try_start_5
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    if-nez v0, :cond_d

    if-nez p2, :cond_d

    .line 1239
    monitor-exit v5

    .line 1287
    :goto_c
    return-void

    .line 1241
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    .line 1242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->e:Z

    .line 1243
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_18
    if-ge v4, v7, :cond_39

    aget-object v8, v6, v4

    .line 1244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->c:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1245
    if-nez v0, :cond_30

    .line 1246
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1247
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->c:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .line 1250
    :cond_30
    invoke-virtual {v8, v0}, Lcom/google/android/maps/driveabout/vector/aD;->b(Ljava/util/List;)Z

    move-result v0

    or-int/2addr v2, v0

    .line 1243
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_18

    .line 1252
    :cond_39
    if-eqz v2, :cond_c9

    .line 1253
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1257
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/cE;->a()[Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    array-length v3, v2

    :goto_45
    if-ge v1, v3, :cond_5c

    aget-object v0, v2, v1

    .line 1258
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->c:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1259
    if-eqz v0, :cond_58

    .line 1260
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1257
    :cond_58
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_45

    .line 1263
    :cond_5c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1267
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1268
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cw;

    .line 1269
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_90

    .line 1270
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/cw;->a(Z)V

    .line 1271
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6b

    .line 1286
    :catchall_8d
    move-exception v0

    monitor-exit v5
    :try_end_8f
    .catchall {:try_start_5 .. :try_end_8f} :catchall_8d

    throw v0

    .line 1273
    :cond_90
    const/4 v3, 0x0

    :try_start_91
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/cw;->a(Z)V

    goto :goto_6b

    .line 1276
    :cond_95
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1277
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/cx;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cw;

    .line 1278
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c4

    .line 1279
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/cw;->b(Z)V

    .line 1280
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cw;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a2

    .line 1282
    :cond_c4
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/cw;->b(Z)V

    goto :goto_a2

    .line 1286
    :cond_c9
    monitor-exit v5
    :try_end_ca
    .catchall {:try_start_91 .. :try_end_ca} :catchall_8d

    goto/16 :goto_c
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/ca;)V
    .registers 5
    .parameter

    .prologue
    .line 1921
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->j:Lcom/google/android/maps/driveabout/vector/ca;

    if-eq p1, v0, :cond_16

    .line 1922
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->j:Lcom/google/android/maps/driveabout/vector/ca;

    .line 1923
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    if-eqz v0, :cond_16

    .line 1924
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/cc;->a(Lcom/google/android/maps/driveabout/vector/ca;)V

    .line 1925
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1928
    :cond_16
    return-void
.end method

.method a(Lcom/google/android/maps/driveabout/vector/cv;)V
    .registers 2
    .parameter

    .prologue
    .line 1015
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->W:Lcom/google/android/maps/driveabout/vector/cv;

    .line 1016
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/g;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1986
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/S;->a(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/g;)V

    .line 1987
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/dd;)V
    .registers 6
    .parameter

    .prologue
    .line 2139
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 2141
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cG;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cH;->a:Lcom/google/android/maps/driveabout/vector/cH;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/cG;-><init>(Lcom/google/android/maps/driveabout/vector/cH;Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 2142
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2144
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cG;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cH;->c:Lcom/google/android/maps/driveabout/vector/cH;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/cG;-><init>(Lcom/google/android/maps/driveabout/vector/cH;Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 2145
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2147
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cG;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cH;->b:Lcom/google/android/maps/driveabout/vector/cH;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/cG;-><init>(Lcom/google/android/maps/driveabout/vector/cH;Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 2148
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2149
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_3b

    .line 2150
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    if-eqz v0, :cond_33

    .line 2151
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cc;->b()V

    .line 2153
    :cond_33
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 2154
    return-void

    .line 2149
    :catchall_3b
    move-exception v0

    :try_start_3c
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_3b

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/di;)V
    .registers 6
    .parameter

    .prologue
    .line 1060
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1062
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1063
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->j()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1064
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/di;)V

    goto :goto_9

    .line 1070
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0

    .line 1069
    :cond_22
    :try_start_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1070
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_22 .. :try_end_28} :catchall_1f

    .line 1071
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/f;)V
    .registers 3
    .parameter

    .prologue
    .line 2067
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->z:Lcom/google/android/maps/driveabout/vector/bY;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bY;->a(Lcom/google/android/maps/driveabout/vector/f;)V

    .line 2068
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 1140
    if-eqz p1, :cond_1c

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1142
    :goto_7
    monitor-enter p0

    .line 1143
    :try_start_8
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->M:Ljava/util/List;

    .line 1144
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_1e

    .line 1145
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_21

    .line 1146
    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->A:Lcom/google/android/maps/driveabout/vector/bY;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->b(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 1150
    :goto_18
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cz;->f()V

    .line 1151
    return-void

    .line 1140
    :cond_1c
    const/4 v0, 0x0

    goto :goto_7

    .line 1144
    :catchall_1e
    move-exception v0

    :try_start_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    throw v0

    .line 1148
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->A:Lcom/google/android/maps/driveabout/vector/bY;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/aD;)V

    goto :goto_18
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 800
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eq v0, p1, :cond_23

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/L;

    if-nez v0, :cond_23

    .line 801
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 803
    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 804
    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 809
    :cond_23
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->J()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;->a(Z)V

    .line 812
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 816
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ai:I

    if-lez v0, :cond_45

    .line 817
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ai:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ai:I

    .line 823
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 889
    :goto_44
    return-void

    .line 831
    :cond_45
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ae:Z

    if-eqz v0, :cond_61

    .line 832
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ag:Ljava/lang/Object;

    monitor-enter v1

    .line 833
    const/4 v0, 0x0

    :try_start_4d
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ae:Z

    .line 834
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 835
    :goto_54
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->af:Z
    :try_end_56
    .catchall {:try_start_4d .. :try_end_56} :catchall_a0

    if-nez v0, :cond_60

    .line 838
    :try_start_58
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_5d
    .catchall {:try_start_58 .. :try_end_5d} :catchall_a0
    .catch Ljava/lang/InterruptedException; {:try_start_58 .. :try_end_5d} :catch_5e

    goto :goto_54

    .line 839
    :catch_5e
    move-exception v0

    goto :goto_54

    .line 843
    :cond_60
    :try_start_60
    monitor-exit v1
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_a0

    .line 848
    :cond_61
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_7e

    .line 849
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->aq:Ljava/lang/Object;

    monitor-enter v1

    .line 850
    :try_start_6a
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_78

    .line 851
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    .line 853
    :cond_78
    monitor-exit v1
    :try_end_79
    .catchall {:try_start_6a .. :try_end_79} :catchall_a3

    .line 854
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/I;->e()V

    .line 864
    :cond_7e
    invoke-static {}, Lu/m;->e()Z

    move-result v0

    if-nez v0, :cond_a6

    .line 867
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 868
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 869
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->al:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_9a

    .line 872
    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->al:J

    .line 875
    :cond_9a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    goto :goto_44

    .line 843
    :catchall_a0
    move-exception v0

    :try_start_a1
    monitor-exit v1
    :try_end_a2
    .catchall {:try_start_a1 .. :try_end_a2} :catchall_a0

    throw v0

    .line 853
    :catchall_a3
    move-exception v0

    :try_start_a4
    monitor-exit v1
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_a3

    throw v0

    .line 881
    :cond_a6
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cz;->h()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->d(I)V

    goto :goto_44
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eq v0, p1, :cond_1f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/L;

    if-nez v0, :cond_1f

    .line 725
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 727
    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 728
    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 730
    :cond_1f
    if-lez p2, :cond_23

    if-gtz p3, :cond_24

    .line 746
    :cond_23
    :goto_23
    return-void

    .line 734
    :cond_24
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->x:F

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(IIF)V

    .line 739
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/k;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/k;->h()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->Q:F

    .line 742
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->x:F

    invoke-static {p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/cz;->a(IIF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ad:F

    .line 745
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    goto :goto_23
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 663
    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 664
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->n:I

    .line 665
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->s()V

    .line 667
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/E;->a(Ljava/lang/String;)V

    .line 669
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/cz;->a(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 679
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eq v0, p1, :cond_2b

    .line 680
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->r()V

    .line 681
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    .line 684
    :cond_2b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    if-nez v0, :cond_74

    .line 685
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->Z:J

    .line 686
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->v:Lcom/google/android/maps/driveabout/vector/cq;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cz;->ao:LA/j;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/maps/driveabout/vector/cq;Lcom/google/android/maps/driveabout/vector/I;LA/j;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    .line 688
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/I;

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ac:Z

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/I;->b(Z)V

    .line 692
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->w:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/cZ;->a(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 694
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->D()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/ax;->a(I)V

    .line 696
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cc;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->j:Lcom/google/android/maps/driveabout/vector/ca;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cc;-><init>(Lcom/google/android/maps/driveabout/vector/ca;Lcom/google/android/maps/driveabout/vector/aV;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    .line 697
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->z:Lcom/google/android/maps/driveabout/vector/bY;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bY;->a(Lcom/google/android/maps/driveabout/vector/cc;)V

    .line 698
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->A:Lcom/google/android/maps/driveabout/vector/bY;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bY;->a(Lcom/google/android/maps/driveabout/vector/cc;)V

    .line 707
    :cond_74
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/I;->c(Z)V

    .line 709
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->P:Z

    .line 713
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ah:Z

    if-eqz v0, :cond_82

    .line 714
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ai:I

    .line 716
    :cond_82
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cz;->ah:Z

    .line 719
    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 720
    return-void
.end method

.method declared-synchronized a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 967
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->ac:Z

    .line 968
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    if-eqz v0, :cond_17

    .line 969
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/I;->b(Z)V

    .line 971
    if-nez p1, :cond_17

    .line 972
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/I;->c()V

    .line 975
    :cond_17
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cz;->s()V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1c

    .line 976
    monitor-exit p0

    return-void

    .line 967
    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)Lcom/google/android/maps/driveabout/vector/aB;
    .registers 4
    .parameter

    .prologue
    .line 1032
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aB;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/aB;-><init>(ILcom/google/android/maps/driveabout/vector/S;)V

    return-object v0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/cm;
    .registers 5
    .parameter

    .prologue
    .line 1045
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cm;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->w:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/maps/driveabout/vector/cm;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/S;Z)V

    return-object v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 630
    invoke-static {}, Lk/a;->a()Lk/a;

    move-result-object v0

    .line 631
    if-eqz v0, :cond_6

    .line 638
    :cond_6
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 3
    .parameter

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    if-eq p1, v0, :cond_e

    .line 1902
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    .line 1903
    monitor-enter p0

    .line 1904
    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->K:Z

    .line 1905
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_f

    .line 1906
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cz;->f()V

    .line 1908
    :cond_e
    return-void

    .line 1905
    :catchall_f
    move-exception v0

    :try_start_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/aD;)V
    .registers 5
    .parameter

    .prologue
    .line 1111
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1113
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cG;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cH;->b:Lcom/google/android/maps/driveabout/vector/cH;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/cG;-><init>(Lcom/google/android/maps/driveabout/vector/cH;Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 1114
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1115
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_18

    .line 1116
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1117
    return-void

    .line 1115
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1167
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/di;)V
    .registers 6
    .parameter

    .prologue
    .line 1079
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1081
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dd;

    .line 1082
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->j()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1083
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->k()Lx/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lx/e;->b(Lcom/google/android/maps/driveabout/vector/di;)V

    goto :goto_9

    .line 1088
    :catchall_23
    move-exception v0

    monitor-exit v1
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    throw v0

    .line 1087
    :cond_26
    :try_start_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1088
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_26 .. :try_end_2c} :catchall_23

    .line 1089
    return-void
.end method

.method public c()V
    .registers 7

    .prologue
    .line 768
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->aq:Ljava/lang/Object;

    monitor-enter v1

    .line 769
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    .line 771
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_13

    .line 773
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/I;->e()V

    .line 774
    return-void

    .line 771
    :catchall_13
    move-exception v0

    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->z:Lcom/google/android/maps/driveabout/vector/bY;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bY;->b_(I)V

    .line 2076
    return-void
.end method

.method public c(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1948
    monitor-enter p0

    .line 1949
    if-eqz p1, :cond_e

    const/4 v0, 0x2

    :goto_5
    :try_start_5
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->aa:I

    .line 1951
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_10

    .line 1955
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1956
    return-void

    .line 1949
    :cond_e
    const/4 v0, 0x1

    goto :goto_5

    .line 1951
    :catchall_10
    move-exception v0

    :try_start_11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public d()V
    .registers 7

    .prologue
    .line 784
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->aq:Ljava/lang/Object;

    monitor-enter v1

    .line 785
    :try_start_3
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_14

    .line 786
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->ap:J

    .line 790
    :cond_14
    monitor-exit v1

    .line 791
    return-void

    .line 790
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public declared-synchronized e()V
    .registers 3

    .prologue
    .line 996
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/I;->c(Z)V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 997
    monitor-exit p0

    return-void

    .line 996
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .registers 4

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    if-eqz v0, :cond_10

    .line 1004
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->y:Lcom/google/android/maps/driveabout/vector/cc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cc;->b()V

    .line 1005
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1007
    :cond_10
    return-void
.end method

.method public g()Ljava/util/ArrayList;
    .registers 4

    .prologue
    .line 1125
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1127
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1128
    :try_start_e
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1129
    monitor-exit v1

    .line 1130
    return-object v0

    .line 1129
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    throw v0
.end method

.method public h()I
    .registers 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1199
    .line 1201
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v4

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/b;

    .line 1202
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    invoke-interface {v0, v6}, Lcom/google/android/maps/driveabout/vector/b;->a(Lcom/google/android/maps/driveabout/vector/k;)I

    move-result v6

    .line 1203
    if-eqz v6, :cond_56

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v7

    if-eqz v7, :cond_56

    .line 1204
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    .line 1207
    :goto_28
    or-int v1, v3, v6

    move v3, v1

    move-object v1, v0

    .line 1208
    goto :goto_a

    .line 1210
    :cond_2d
    if-eqz v3, :cond_4e

    .line 1212
    if-eqz v1, :cond_40

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->k:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 1214
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1215
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->k:Lcom/google/android/maps/driveabout/vector/l;

    .line 1219
    :cond_40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->u:Lcom/google/android/maps/driveabout/vector/I;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/maps/driveabout/vector/I;->a(ZZ)V

    .line 1227
    :goto_45
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->t:Lcom/google/android/maps/driveabout/vector/k;

    if-eqz v3, :cond_4a

    const/4 v4, 0x1

    :cond_4a
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/k;->a(Z)V

    .line 1228
    return v3

    .line 1221
    :cond_4e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1222
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/cz;->k:Lcom/google/android/maps/driveabout/vector/l;

    goto :goto_45

    :cond_56
    move-object v0, v1

    goto :goto_28
.end method

.method public declared-synchronized i()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 1585
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->N:Z

    .line 1586
    :goto_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->O:Landroid/graphics/Bitmap;
    :try_end_6
    .catchall {:try_start_2 .. :try_end_6} :catchall_15

    if-nez v0, :cond_e

    .line 1588
    :try_start_8
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_15
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_b} :catch_c

    goto :goto_4

    .line 1589
    :catch_c
    move-exception v0

    goto :goto_4

    .line 1593
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->O:Landroid/graphics/Bitmap;

    .line 1594
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/cz;->O:Landroid/graphics/Bitmap;
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_15

    .line 1595
    monitor-exit p0

    return-object v0

    .line 1585
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/D;
    .registers 2

    .prologue
    .line 1914
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->S:Lcom/google/android/maps/driveabout/vector/D;

    return-object v0
.end method

.method public k()V
    .registers 2

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/S;->f()V

    .line 1995
    return-void
.end method

.method public l()Lcom/google/android/maps/driveabout/vector/ay;
    .registers 3

    .prologue
    .line 2002
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/S;->e()Lcom/google/android/maps/driveabout/vector/d;

    move-result-object v0

    .line 2003
    instance-of v1, v0, Lcom/google/android/maps/driveabout/vector/ay;

    if-eqz v1, :cond_d

    .line 2004
    check-cast v0, Lcom/google/android/maps/driveabout/vector/ay;

    .line 2006
    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public m()Lcom/google/android/maps/driveabout/vector/S;
    .registers 2

    .prologue
    .line 2011
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->C:Lcom/google/android/maps/driveabout/vector/S;

    return-object v0
.end method

.method public n()Lz/b;
    .registers 2

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->D:Lz/b;

    return-object v0
.end method

.method public o()Lcom/google/android/maps/driveabout/vector/dd;
    .registers 2

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->ab:Lcom/google/android/maps/driveabout/vector/dd;

    return-object v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/aV;
    .registers 2

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->m:Lcom/google/android/maps/driveabout/vector/aV;

    return-object v0
.end method

.method public q()V
    .registers 2

    .prologue
    .line 2092
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cz;->Y:Z

    .line 2093
    return-void
.end method
