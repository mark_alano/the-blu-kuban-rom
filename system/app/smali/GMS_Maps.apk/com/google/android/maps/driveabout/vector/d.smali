.class public abstract Lcom/google/android/maps/driveabout/vector/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# instance fields
.field private a:Lcom/google/android/maps/driveabout/vector/F;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    return-void
.end method

.method protected static a(Ljavax/microedition/khronos/opengles/GL10;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const v4, 0xff00

    .line 435
    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v4

    .line 436
    shr-int/lit8 v1, p1, 0x8

    and-int/2addr v1, v4

    .line 437
    and-int v2, p1, v4

    .line 438
    shl-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v4

    .line 440
    invoke-interface {p0, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 441
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;
    .registers 4
    .parameter

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)V

    return-object v0
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v0
.end method

.method protected varargs a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)V

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 416
    return-void
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 180
    return-void
.end method

.method public abstract a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 213
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/F;)V
    .registers 2
    .parameter

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    .line 407
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 238
    return-void
.end method

.method public a(FFLC/a;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 363
    const/4 v0, 0x0

    return v0
.end method

.method public a_(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 184
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;)V

    .line 185
    return-void
.end method

.method public b(FFLC/a;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method public b(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 282
    const/4 v0, 0x0

    return v0
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/List;)Z
    .registers 3
    .parameter

    .prologue
    .line 153
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 154
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/D;->v_()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/D;->a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    const/4 v0, 0x1

    .line 157
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 226
    return-void
.end method

.method public c(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public d(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    if-eqz v0, :cond_9

    .line 423
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    invoke-interface {v0, p0}, Lcom/google/android/maps/driveabout/vector/F;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 425
    :cond_9
    return-void
.end method

.method public i_()V
    .registers 1

    .prologue
    .line 250
    return-void
.end method

.method public j_()Z
    .registers 2

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method

.method public k_()V
    .registers 1

    .prologue
    .line 373
    return-void
.end method

.method public l_()Z
    .registers 2

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public abstract p()Lcom/google/android/maps/driveabout/vector/E;
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .registers 2

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .registers 2

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method
