.class public Lcom/google/android/maps/driveabout/vector/dF;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/android/maps/driveabout/vector/di;

.field public static final b:[Lcom/google/android/maps/driveabout/vector/di;

.field public static volatile c:I

.field private static d:Z

.field private static e:Z

.field private static volatile f:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/di;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->b:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->e:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->h:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->i:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->j:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dF;->a:[Lcom/google/android/maps/driveabout/vector/di;

    .line 60
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/di;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->d:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->e:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->g:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->f:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->k:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->m:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->l:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->n:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->o:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->p:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/maps/driveabout/vector/di;->q:Lcom/google/android/maps/driveabout/vector/di;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dF;->b:[Lcom/google/android/maps/driveabout/vector/di;

    .line 84
    sput-boolean v3, Lcom/google/android/maps/driveabout/vector/dF;->d:Z

    .line 88
    const/16 v0, 0xa

    sput v0, Lcom/google/android/maps/driveabout/vector/dF;->c:I

    .line 95
    const/4 v0, -0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/dF;->f:I

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lad/h;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 479
    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    .line 481
    new-instance v3, Lad/j;

    invoke-direct {v3}, Lad/j;-><init>()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->Q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lad/j;->a(Ljava/lang/String;)Lad/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->E()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lad/j;->b(Ljava/lang/String;)Lad/j;

    move-result-object v3

    invoke-virtual {v3, p3}, Lad/j;->c(Ljava/lang/String;)Lad/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lad/j;->d(Ljava/lang/String;)Lad/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Lad/j;->a(Z)Lad/j;

    move-result-object v3

    invoke-virtual {v3, v1}, Lad/j;->b(Z)Lad/j;

    move-result-object v3

    invoke-static {p0}, Li/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lad/j;->e(Ljava/lang/String;)Lad/j;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/res/Resources;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lad/j;->c(Z)Lad/j;

    move-result-object v3

    invoke-virtual {v0, p0}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lad/j;->e(Z)Lad/j;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/capabilities/a;->b()Z

    move-result v0

    if-nez v0, :cond_b1

    move v0, v1

    :goto_56
    invoke-virtual {v3, v0}, Lad/j;->d(Z)Lad/j;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v3}, Lad/j;->a(I)Lad/j;

    move-result-object v3

    .line 500
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 501
    if-eqz v4, :cond_b3

    const-string v0, "GMM"

    :goto_72
    invoke-virtual {v3, v0}, Lad/j;->f(Ljava/lang/String;)Lad/j;

    .line 513
    invoke-virtual {v3, v1}, Lad/j;->b(I)Lad/j;

    .line 515
    invoke-virtual {v3, v1}, Lad/j;->f(Z)Lad/j;

    .line 517
    invoke-virtual {v3}, Lad/j;->a()Lad/h;

    move-result-object v0

    .line 521
    const-string v1, "GMM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 522
    if-nez v1, :cond_89

    if-eqz v4, :cond_9f

    .line 523
    :cond_89
    const/4 v1, -0x1

    invoke-virtual {v3, v1}, Lad/j;->b(I)Lad/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Lad/j;->f(Z)Lad/j;

    move-result-object v1

    const-string v2, "DriveAbout"

    invoke-virtual {v1, v2}, Lad/j;->f(Ljava/lang/String;)Lad/j;

    move-result-object v1

    invoke-virtual {v1}, Lad/j;->b()Lad/p;

    move-result-object v1

    .line 529
    invoke-virtual {v0, v1}, Lad/h;->a(Lad/p;)V

    .line 532
    :cond_9f
    new-instance v1, Lcom/google/android/maps/driveabout/vector/dH;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/dH;-><init>(Lcom/google/android/maps/driveabout/vector/dG;)V

    invoke-virtual {v0, v1}, Lad/h;->a(Lad/q;)V

    .line 537
    const-string v1, "1"

    .line 538
    const-string v1, "2"

    .line 539
    const-string v1, "3"

    .line 540
    const-string v1, "3"

    .line 545
    return-object v0

    :cond_b1
    move v0, v2

    .line 481
    goto :goto_56

    :cond_b3
    move-object v0, p2

    .line 501
    goto :goto_72
.end method

.method public static declared-synchronized a(Lcom/google/android/maps/driveabout/vector/di;Landroid/content/Context;Landroid/content/res/Resources;)Ls/aH;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 297
    const-class v6, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v6

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z

    if-nez v0, :cond_12

    .line 298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorGlobalState.initialize() must be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_f

    .line 297
    :catchall_f
    move-exception v0

    monitor-exit v6

    throw v0

    .line 300
    :cond_12
    const/4 v0, 0x1

    :try_start_13
    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/di;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p1}, Li/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dF;->a([Lcom/google/android/maps/driveabout/vector/di;Lad/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 306
    invoke-static {p0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_f

    move-result-object v0

    monitor-exit v6

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;[Lcom/google/android/maps/driveabout/vector/di;Ljava/lang/String;ILaU/q;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    const-class v10, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v10

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_c7

    if-eqz v0, :cond_9

    .line 230
    :goto_7
    monitor-exit v10

    return-void

    .line 126
    :cond_9
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 127
    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 129
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    .line 131
    invoke-static {}, Li/a;->a()V

    .line 132
    invoke-static/range {p5 .. p5}, LaU/m;->a(LaU/q;)V

    .line 134
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/vector/dF;->f:I

    .line 138
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/dF;->b(Landroid/content/Context;)V

    .line 141
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v1

    .line 142
    if-nez v1, :cond_53

    .line 148
    invoke-static {}, Li/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p3, v0}, Lcom/google/android/maps/driveabout/vector/dF;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lad/h;

    move-result-object v1

    .line 151
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 152
    if-eqz v0, :cond_46

    .line 153
    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v2, v3}, Lad/h;->a(J)V

    .line 157
    :cond_46
    if-eqz v0, :cond_48

    .line 164
    :cond_48
    invoke-static {v1}, Lcom/google/googlenav/clientparam/f;->a(Lad/h;)V

    .line 166
    new-instance v0, Lu/j;

    invoke-direct {v0, v1}, Lu/j;-><init>(Lad/h;)V

    invoke-virtual {v1, v0}, Lad/h;->a(Lad/q;)V

    .line 168
    :cond_53
    invoke-virtual {v1}, Lad/h;->v()V

    .line 172
    invoke-static {p0}, Li/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    .line 173
    invoke-static {v1, v3}, Ls/af;->a(Lad/h;Ljava/io/File;)Ls/af;

    .line 176
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_60
    .catchall {:try_start_9 .. :try_end_60} :catchall_c7

    move-result-object v2

    .line 178
    const/4 v0, -0x1

    if-eq p4, v0, :cond_6b

    .line 180
    :try_start_64
    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Ls/aG;->a(Ljava/io/InputStream;)V
    :try_end_6b
    .catchall {:try_start_64 .. :try_end_6b} :catchall_c7
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_6b} :catch_ca

    .line 186
    :cond_6b
    :goto_6b
    :try_start_6b
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 187
    const-string v4, "GMM"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 188
    if-nez v0, :cond_7b

    if-eqz v4, :cond_d1

    .line 191
    :cond_7b
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dF;->b:[Lcom/google/android/maps/driveabout/vector/di;

    move-object v4, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dF;->a([Lcom/google/android/maps/driveabout/vector/di;Lad/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 192
    sget-object v4, Lcom/google/android/maps/driveabout/vector/dF;->a:[Lcom/google/android/maps/driveabout/vector/di;

    invoke-static {}, Lad/h;->b()Lad/p;

    move-result-object v5

    move-object v6, v2

    move-object v7, v3

    move-object v8, p0

    move-object v9, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/maps/driveabout/vector/dF;->a([Lcom/google/android/maps/driveabout/vector/di;Lad/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 210
    :cond_8f
    :goto_8f
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dF;->b()Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 211
    new-instance v0, LS/a;

    invoke-direct {v0}, LS/a;-><init>()V

    invoke-static {v1, v3, v2, v0}, Ls/Q;->a(Lad/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)Ls/Q;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_a6

    .line 215
    invoke-virtual {v0}, Ls/Q;->d()V

    .line 216
    invoke-static {v0}, Lm/q;->a(Ls/Q;)Lm/q;

    .line 220
    :cond_a6
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 222
    new-instance v0, LB/a;

    invoke-direct {v0, p0}, LB/a;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, LB/f;->a(LB/f;)V

    .line 225
    :cond_b4
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/dF;->a(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->e:Z

    .line 226
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 228
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z

    .line 229
    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_c5
    .catchall {:try_start_6b .. :try_end_c5} :catchall_c7

    goto/16 :goto_7

    .line 122
    :catchall_c7
    move-exception v0

    monitor-exit v10

    throw v0

    .line 181
    :catch_ca
    move-exception v0

    .line 182
    :try_start_cb
    const-string v4, "Could not load encryption key"

    invoke-static {v4, v0}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6b

    .line 200
    :cond_d1
    if-eqz p2, :cond_8f

    move-object v0, p2

    move-object v4, p0

    move-object v5, p1

    .line 201
    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dF;->a([Lcom/google/android/maps/driveabout/vector/di;Lad/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    :try_end_d9
    .catchall {:try_start_cb .. :try_end_d9} :catchall_c7

    goto :goto_8f
.end method

.method public static declared-synchronized a(Ljava/util/Locale;)V
    .registers 5
    .parameter

    .prologue
    .line 457
    const-class v1, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v1

    :try_start_3
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    .line 458
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 459
    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v0

    invoke-interface {v0, p0}, Ls/aH;->a(Ljava/util/Locale;)V
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_25

    goto :goto_b

    .line 457
    :catchall_25
    move-exception v0

    monitor-exit v1

    throw v0

    .line 463
    :cond_28
    monitor-exit v1

    return-void
.end method

.method public static a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 284
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 286
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v0

    check-cast v0, Ls/aP;

    invoke-virtual {v0, p0}, Ls/aP;->a(Z)V

    .line 289
    :cond_13
    return-void
.end method

.method private static declared-synchronized a([Lcom/google/android/maps/driveabout/vector/di;Lad/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 316
    const-class v9, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v9

    :try_start_3
    const-string v0, "GMM"

    invoke-interface {p1}, Lad/p;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 317
    if-eqz v6, :cond_12

    .line 322
    const/4 v0, 0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/dF;->c:I

    .line 324
    :cond_12
    array-length v10, p0

    const/4 v0, 0x0

    move v8, v0

    :goto_15
    if-ge v8, v10, :cond_3a

    aget-object v0, p0, v8

    .line 326
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 324
    :cond_1f
    :goto_1f
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_15

    :cond_23
    move-object v1, p1

    move-object v2, p4

    move-object/from16 v3, p5

    move-object v4, p2

    move-object v5, p3

    move v7, v6

    .line 331
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/di;->a(Lad/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Ls/aH;

    move-result-object v1

    .line 338
    if-eqz v1, :cond_1f

    .line 339
    invoke-interface {v1}, Ls/aH;->h()V

    .line 340
    invoke-static {v0, v1}, Ls/aJ;->a(Lcom/google/android/maps/driveabout/vector/di;Ls/aH;)V
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_37

    goto :goto_1f

    .line 316
    :catchall_37
    move-exception v0

    monitor-exit v9

    throw v0

    .line 343
    :cond_3a
    monitor-exit v9

    return-void
.end method

.method public static a()Z
    .registers 1

    .prologue
    .line 261
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->e:Z

    return v0
.end method

.method private static a(Landroid/content/Context;)Z
    .registers 8
    .parameter

    .prologue
    const-wide/high16 v5, 0x3fd0

    .line 233
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 240
    int-to-float v0, v0

    .line 242
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 243
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-gtz v1, :cond_2f

    iget v1, v2, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-lez v1, :cond_45

    :cond_2f
    move v1, v0

    .line 253
    :goto_30
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    div-float v1, v3, v1

    .line 254
    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    .line 256
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x41c8

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4a

    const/4 v0, 0x1

    :goto_44
    return v0

    .line 248
    :cond_45
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    .line 249
    iget v0, v2, Landroid/util/DisplayMetrics;->ydpi:F

    goto :goto_30

    .line 256
    :cond_4a
    const/4 v0, 0x0

    goto :goto_44
.end method

.method private static b(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 567
    invoke-static {p0}, Li/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 568
    invoke-static {p0}, Li/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 569
    invoke-static {p0}, Li/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 570
    return-void
.end method

.method public static b()Z
    .registers 1

    .prologue
    .line 275
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .registers 6

    .prologue
    .line 349
    const-class v2, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v2

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_4c

    if-nez v0, :cond_9

    .line 379
    :goto_7
    monitor-exit v2

    return-void

    .line 354
    :cond_9
    :try_start_9
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_11
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    .line 355
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z
    :try_end_20
    .catchall {:try_start_9 .. :try_end_20} :catchall_4c

    move-result v1

    if-eqz v1, :cond_11

    .line 357
    :try_start_23
    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v1

    invoke-interface {v1}, Ls/aH;->i()V

    .line 358
    invoke-static {v0}, Ls/aJ;->a(Lcom/google/android/maps/driveabout/vector/di;)V
    :try_end_2d
    .catchall {:try_start_23 .. :try_end_2d} :catchall_4c
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_2d} :catch_2e

    goto :goto_11

    .line 359
    :catch_2e
    move-exception v1

    .line 360
    :try_start_2f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not stop "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " tile store"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4b
    .catchall {:try_start_2f .. :try_end_4b} :catchall_4c

    goto :goto_11

    .line 349
    :catchall_4c
    move-exception v0

    monitor-exit v2

    throw v0

    .line 365
    :cond_4f
    :try_start_4f
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    if-eqz v0, :cond_58

    .line 366
    invoke-static {}, Ls/Q;->c()V

    .line 368
    :cond_58
    invoke-static {}, Ls/af;->e()V

    .line 370
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_67

    .line 372
    invoke-virtual {v0}, Lad/h;->u()V

    .line 373
    invoke-static {}, Lad/h;->c()V

    .line 376
    :cond_67
    invoke-static {}, LaU/m;->d()V

    .line 378
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_6d
    .catchall {:try_start_4f .. :try_end_6d} :catchall_4c

    goto :goto_7
.end method

.method public static declared-synchronized d()V
    .registers 4

    .prologue
    .line 385
    const-class v1, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v1

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2b

    if-nez v0, :cond_9

    .line 400
    :cond_7
    :goto_7
    monitor-exit v1

    return-void

    .line 389
    :cond_9
    :try_start_9
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    .line 390
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 391
    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v0

    invoke-interface {v0}, Ls/aH;->j()V
    :try_end_2a
    .catchall {:try_start_9 .. :try_end_2a} :catchall_2b

    goto :goto_11

    .line 385
    :catchall_2b
    move-exception v0

    monitor-exit v1

    throw v0

    .line 394
    :cond_2e
    :try_start_2e
    invoke-static {}, Ls/af;->d()Ls/af;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 395
    invoke-static {}, Ls/af;->d()Ls/af;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ls/af;->a(Z)V

    .line 397
    :cond_3c
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 398
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    invoke-virtual {v0}, Ls/Q;->f()V
    :try_end_49
    .catchall {:try_start_2e .. :try_end_49} :catchall_2b

    goto :goto_7
.end method

.method public static e()I
    .registers 1

    .prologue
    .line 407
    sget v0, Lcom/google/android/maps/driveabout/vector/dF;->c:I

    return v0
.end method

.method public static declared-synchronized f()V
    .registers 4

    .prologue
    .line 414
    const-class v1, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v1

    :try_start_3
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2b

    if-nez v0, :cond_9

    .line 428
    :cond_7
    :goto_7
    monitor-exit v1

    return-void

    .line 418
    :cond_9
    :try_start_9
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    .line 419
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 420
    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v0

    invoke-interface {v0}, Ls/aH;->c()V
    :try_end_2a
    .catchall {:try_start_9 .. :try_end_2a} :catchall_2b

    goto :goto_11

    .line 414
    :catchall_2b
    move-exception v0

    monitor-exit v1

    throw v0

    .line 424
    :cond_2e
    :try_start_2e
    invoke-static {}, Ls/af;->d()Ls/af;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ls/af;->a(Z)V

    .line 425
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 426
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    invoke-virtual {v0}, Ls/Q;->g()V
    :try_end_43
    .catchall {:try_start_2e .. :try_end_43} :catchall_2b

    goto :goto_7
.end method

.method public static declared-synchronized g()J
    .registers 7

    .prologue
    .line 434
    const-class v3, Lcom/google/android/maps/driveabout/vector/dF;

    monitor-enter v3

    const-wide/16 v0, 0x0

    .line 435
    :try_start_5
    sget-boolean v2, Lcom/google/android/maps/driveabout/vector/dF;->d:Z
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_4c

    if-nez v2, :cond_b

    .line 449
    :goto_9
    monitor-exit v3

    return-wide v0

    .line 439
    :cond_b
    :try_start_b
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v1, v0

    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    .line 440
    invoke-static {v0}, Ls/aJ;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v5

    if-eqz v5, :cond_4f

    .line 441
    invoke-static {v0}, Ls/aJ;->c(Lcom/google/android/maps/driveabout/vector/di;)Ls/aH;

    move-result-object v0

    invoke-interface {v0}, Ls/aH;->f()J

    move-result-wide v5

    add-long v0, v1, v5

    :goto_30
    move-wide v1, v0

    goto :goto_14

    .line 445
    :cond_32
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 446
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    invoke-virtual {v0}, Ls/Q;->h()J

    move-result-wide v4

    add-long/2addr v1, v4

    .line 448
    :cond_41
    invoke-static {}, Ls/af;->d()Ls/af;

    move-result-object v0

    invoke-virtual {v0}, Ls/af;->c()J
    :try_end_48
    .catchall {:try_start_b .. :try_end_48} :catchall_4c

    move-result-wide v4

    add-long v0, v1, v4

    .line 449
    goto :goto_9

    .line 434
    :catchall_4c
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4f
    move-wide v0, v1

    goto :goto_30
.end method

.method public static h()I
    .registers 1

    .prologue
    .line 554
    sget v0, Lcom/google/android/maps/driveabout/vector/dF;->f:I

    return v0
.end method
