.class Lcom/google/android/maps/driveabout/vector/dH;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lad/q;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dI;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/dI;-><init>(Lcom/google/android/maps/driveabout/vector/dH;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dH;->a:Landroid/os/Handler;

    .line 584
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dJ;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/dJ;-><init>(Lcom/google/android/maps/driveabout/vector/dH;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dH;->b:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/dG;)V
    .registers 2
    .parameter

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/dH;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 608
    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 603
    return-void
.end method

.method public a(Lad/g;)V
    .registers 4
    .parameter

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dH;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 596
    return-void
.end method

.method public b(Lad/g;)V
    .registers 4
    .parameter

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dH;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 614
    return-void
.end method
