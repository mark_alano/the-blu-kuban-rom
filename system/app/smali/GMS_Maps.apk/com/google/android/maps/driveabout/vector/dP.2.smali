.class Lcom/google/android/maps/driveabout/vector/dP;
.super Lcom/google/android/maps/driveabout/vector/c;
.source "SourceFile"


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private volatile h:Z

.field private final i:Lcom/google/android/maps/driveabout/vector/n;

.field private final j:[F


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1245
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/c;-><init>(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1242
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->j:[F

    .line 1246
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/dP;->i:Lcom/google/android/maps/driveabout/vector/n;

    .line 1247
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1364
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->h:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;)Lcom/google/android/maps/driveabout/vector/m;
    .registers 13
    .parameter

    .prologue
    .line 1285
    monitor-enter p0

    .line 1286
    :try_start_1
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dP;->f:F

    .line 1287
    iget v5, p0, Lcom/google/android/maps/driveabout/vector/dP;->g:F

    .line 1288
    iget v7, p0, Lcom/google/android/maps/driveabout/vector/dP;->d:F

    .line 1289
    iget v8, p0, Lcom/google/android/maps/driveabout/vector/dP;->e:F

    .line 1291
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_76

    .line 1292
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c8

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 1304
    :goto_21
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_88

    .line 1305
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    mul-float/2addr v1, v2

    const v2, -0x42333333

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v6, v0

    .line 1313
    :goto_38
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    .line 1314
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    .line 1315
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->d:F

    .line 1316
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->e:F

    .line 1319
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_99

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_99

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_99

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_99

    .line 1321
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->h:Z

    .line 1322
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    monitor-exit p0

    move-object p0, v0

    .line 1359
    :cond_75
    :goto_75
    return-object p0

    .line 1295
    :cond_76
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c8

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_21

    .line 1308
    :cond_88
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    mul-float/2addr v1, v2

    const v2, 0x3dcccccd

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v6, v0

    goto :goto_38

    .line 1324
    :cond_99
    monitor-exit p0
    :try_end_9a
    .catchall {:try_start_1 .. :try_end_9a} :catchall_e9

    .line 1327
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_a4

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_ec

    :cond_a4
    const/4 v0, 0x1

    move v2, v0

    .line 1328
    :goto_a6
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_ef

    const/4 v0, 0x1

    move v1, v0

    .line 1329
    :goto_ad
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_f2

    const/4 v0, 0x1

    .line 1332
    :goto_b3
    if-eqz v2, :cond_c6

    .line 1333
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-static {v2, p1, v7, v8}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;FF)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    .line 1336
    if-nez v1, :cond_c1

    if-eqz v0, :cond_c6

    .line 1337
    :cond_c1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1342
    :cond_c6
    if-eqz v1, :cond_db

    .line 1344
    invoke-virtual {p1, v4, v5}, Lcom/google/android/maps/driveabout/vector/k;->d(FF)Lo/Q;

    move-result-object v1

    .line 1347
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->i:Lcom/google/android/maps/driveabout/vector/n;

    invoke-static {p1, v2, v1, v6}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/n;Lo/Q;F)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    .line 1350
    if-eqz v0, :cond_db

    .line 1351
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1356
    :cond_db
    if-eqz v0, :cond_75

    .line 1357
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->i:Lcom/google/android/maps/driveabout/vector/n;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/n;FFF)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    goto :goto_75

    .line 1324
    :catchall_e9
    move-exception v0

    :try_start_ea
    monitor-exit p0
    :try_end_eb
    .catchall {:try_start_ea .. :try_end_eb} :catchall_e9

    throw v0

    .line 1327
    :cond_ec
    const/4 v0, 0x0

    move v2, v0

    goto :goto_a6

    .line 1328
    :cond_ef
    const/4 v0, 0x0

    move v1, v0

    goto :goto_ad

    .line 1329
    :cond_f2
    const/4 v0, 0x0

    goto :goto_b3
.end method

.method declared-synchronized a(FFFFFF)[F
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1257
    monitor-enter p0

    :try_start_2
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    .line 1258
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    .line 1259
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->d:F

    add-float/2addr v0, p5

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->d:F

    .line 1260
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->e:F

    add-float/2addr v0, p6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->e:F

    .line 1267
    cmpl-float v0, p1, v1

    if-nez v0, :cond_1e

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_22

    .line 1268
    :cond_1e
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/dP;->f:F

    .line 1269
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/dP;->g:F

    .line 1272
    :cond_22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->h:Z

    .line 1275
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->j:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/dP;->b:F

    add-float/2addr v2, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dK;->j()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    .line 1277
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->j:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dP;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/dP;->c:F

    add-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/vector/dK;->e(F)F

    move-result v2

    aput v2, v0, v1

    .line 1278
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dP;->j:[F
    :try_end_55
    .catchall {:try_start_2 .. :try_end_55} :catchall_57

    monitor-exit p0

    return-object v0

    .line 1257
    :catchall_57
    move-exception v0

    monitor-exit p0

    throw v0
.end method
