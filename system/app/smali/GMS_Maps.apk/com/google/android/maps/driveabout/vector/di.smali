.class public abstract Lcom/google/android/maps/driveabout/vector/di;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final A:Ljava/util/Map;

.field private static final G:Lo/af;

.field private static final H:Lo/aj;

.field private static final I:Lo/aj;

.field private static final J:Lo/aj;

.field private static final K:Lo/ak;

.field private static final L:Lo/ag;

.field private static final M:Lo/ag;

.field private static final N:Lo/ag;

.field public static final a:Lcom/google/android/maps/driveabout/vector/di;

.field public static final b:Lcom/google/android/maps/driveabout/vector/di;

.field public static final c:Lcom/google/android/maps/driveabout/vector/di;

.field public static final d:Lcom/google/android/maps/driveabout/vector/di;

.field public static final e:Lcom/google/android/maps/driveabout/vector/di;

.field public static final f:Lcom/google/android/maps/driveabout/vector/di;

.field public static final g:Lcom/google/android/maps/driveabout/vector/di;

.field public static final h:Lcom/google/android/maps/driveabout/vector/di;

.field public static final i:Lcom/google/android/maps/driveabout/vector/di;

.field public static final j:Lcom/google/android/maps/driveabout/vector/di;

.field public static final k:Lcom/google/android/maps/driveabout/vector/di;

.field public static final l:Lcom/google/android/maps/driveabout/vector/di;

.field public static final m:Lcom/google/android/maps/driveabout/vector/di;

.field public static final n:Lcom/google/android/maps/driveabout/vector/di;

.field public static final o:Lcom/google/android/maps/driveabout/vector/di;

.field public static final p:Lcom/google/android/maps/driveabout/vector/di;

.field public static final q:Lcom/google/android/maps/driveabout/vector/di;

.field public static final r:Lcom/google/android/maps/driveabout/vector/di;

.field public static final s:Lcom/google/android/maps/driveabout/vector/di;

.field public static final t:Lcom/google/android/maps/driveabout/vector/di;

.field public static final u:Lcom/google/android/maps/driveabout/vector/di;


# instance fields
.field private final B:I

.field private final C:Z

.field private final D:Z

.field private final E:Z

.field private final F:Ls/aB;

.field public final v:I

.field public final w:I

.field public final x:Z

.field public final y:I

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/16 v9, 0xc

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    .line 69
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dl;

    const/16 v3, 0xa

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dl;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dl;->f(Z)Lcom/google/android/maps/driveabout/vector/dy;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dy;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    .line 74
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dl;

    const/16 v3, 0x16

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dl;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dl;->f(Z)Lcom/google/android/maps/driveabout/vector/dy;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dy;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->b:Lcom/google/android/maps/driveabout/vector/di;

    .line 79
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dl;

    const/16 v3, 0x1a

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dl;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dl;->f(Z)Lcom/google/android/maps/driveabout/vector/dy;

    move-result-object v0

    const-string v3, "_alt_base"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->c:Lcom/google/android/maps/driveabout/vector/di;

    .line 85
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dl;

    const/16 v3, 0x14

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dl;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dl;->f(Z)Lcom/google/android/maps/driveabout/vector/dy;

    move-result-object v0

    const-string v3, "_tran_base"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->d:Lcom/google/android/maps/driveabout/vector/di;

    .line 91
    new-instance v0, Lcom/google/android/maps/driveabout/vector/do;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/do;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/do;->c(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->e:Lcom/google/android/maps/driveabout/vector/di;

    .line 95
    new-instance v0, Lcom/google/android/maps/driveabout/vector/do;

    invoke-direct {v0, v9, v8}, Lcom/google/android/maps/driveabout/vector/do;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_ter"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/do;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/dm;->c(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->f:Lcom/google/android/maps/driveabout/vector/di;

    .line 100
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/4 v3, 0x4

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->g:Lcom/google/android/maps/driveabout/vector/di;

    .line 103
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0x17

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->h:Lcom/google/android/maps/driveabout/vector/di;

    .line 106
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dt;

    const/16 v3, 0x8

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dt;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dt;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->i:Lcom/google/android/maps/driveabout/vector/di;

    .line 108
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dv;

    const/16 v3, 0xb

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dv;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dv;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->j:Lcom/google/android/maps/driveabout/vector/di;

    .line 114
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dl;

    const/16 v3, 0x12

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dl;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dl;->a(Z)Lcom/google/android/maps/driveabout/vector/dl;

    move-result-object v0

    const-string v3, "_vec_bic"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dl;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->k:Lcom/google/android/maps/driveabout/vector/di;

    .line 125
    new-instance v0, Lcom/google/android/maps/driveabout/vector/do;

    const/4 v3, 0x7

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/do;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/do;->a(I)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    const-string v3, "_ter_bic"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dm;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->l:Lcom/google/android/maps/driveabout/vector/di;

    .line 129
    new-instance v0, Lcom/google/android/maps/driveabout/vector/do;

    const/4 v3, 0x6

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/do;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/do;->a(I)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    const-string v3, "_hy_bic"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dm;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->m:Lcom/google/android/maps/driveabout/vector/di;

    .line 133
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0xd

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_tran"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->n:Lcom/google/android/maps/driveabout/vector/di;

    .line 137
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0xe

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dy;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    const-string v3, "_inaka"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dm;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->o:Lcom/google/android/maps/driveabout/vector/di;

    .line 141
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dr;

    const/16 v3, 0xf

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dr;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_labl"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dr;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->p:Lcom/google/android/maps/driveabout/vector/di;

    .line 146
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dr;

    const/16 v3, 0x15

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dr;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_tran_labl"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dr;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->d(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->q:Lcom/google/android/maps/driveabout/vector/di;

    .line 151
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0x10

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_psm"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->b(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->r:Lcom/google/android/maps/driveabout/vector/di;

    .line 156
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0x11

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_related"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->b(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->s:Lcom/google/android/maps/driveabout/vector/di;

    .line 160
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0x18

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_high"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->b(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/dm;->e(Z)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->t:Lcom/google/android/maps/driveabout/vector/di;

    .line 165
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dy;

    const/16 v3, 0x19

    invoke-direct {v0, v3, v8}, Lcom/google/android/maps/driveabout/vector/dy;-><init>(ILcom/google/android/maps/driveabout/vector/dj;)V

    const-string v3, "_api"

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/dy;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dm;->a()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->u:Lcom/google/android/maps/driveabout/vector/di;

    .line 214
    new-instance v0, Lo/af;

    const/4 v3, 0x0

    new-array v4, v2, [I

    invoke-direct {v0, v2, v3, v4, v2}, Lo/af;-><init>(IF[II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->G:Lo/af;

    .line 215
    new-instance v0, Lo/aj;

    const/high16 v3, -0x100

    sget-object v4, Lcom/google/android/maps/driveabout/vector/di;->G:Lo/af;

    invoke-direct {v0, v3, v4}, Lo/aj;-><init>(ILo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->H:Lo/aj;

    .line 216
    new-instance v0, Lo/aj;

    const v3, -0xffff01

    sget-object v4, Lcom/google/android/maps/driveabout/vector/di;->G:Lo/af;

    invoke-direct {v0, v3, v4}, Lo/aj;-><init>(ILo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->I:Lo/aj;

    .line 217
    new-instance v0, Lo/aj;

    const/high16 v3, -0x1

    sget-object v4, Lcom/google/android/maps/driveabout/vector/di;->G:Lo/af;

    invoke-direct {v0, v3, v4}, Lo/aj;-><init>(ILo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->J:Lo/aj;

    .line 218
    new-instance v0, Lo/ak;

    const/16 v3, 0xa

    const v4, 0x3f99999a

    const/high16 v5, 0x3f80

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lo/ak;-><init>(IIIFFI)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->K:Lo/ak;

    .line 220
    new-instance v0, Lo/ag;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/di;->K:Lo/ak;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/di;->H:Lo/aj;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/ag;-><init>(II[I[Lo/af;Lo/ak;Lo/aj;Lo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->L:Lo/ag;

    .line 223
    new-instance v0, Lo/ag;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/di;->K:Lo/ak;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/di;->J:Lo/aj;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/ag;-><init>(II[I[Lo/af;Lo/ak;Lo/aj;Lo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->M:Lo/ag;

    .line 226
    new-instance v0, Lo/ag;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/di;->K:Lo/ak;

    sget-object v6, Lcom/google/android/maps/driveabout/vector/di;->I:Lo/aj;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/ag;-><init>(II[I[Lo/af;Lo/ak;Lo/aj;Lo/af;)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/di;->N:Lo/ag;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/dm;)V
    .registers 6
    .parameter

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->a(Lcom/google/android/maps/driveabout/vector/dm;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/di;->v:I

    .line 237
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->b(Lcom/google/android/maps/driveabout/vector/dm;)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/di;->w:I

    .line 238
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->c(Lcom/google/android/maps/driveabout/vector/dm;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/di;->z:Ljava/lang/String;

    .line 239
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->d(Lcom/google/android/maps/driveabout/vector/dm;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->x:Z

    .line 240
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->e(Lcom/google/android/maps/driveabout/vector/dm;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->C:Z

    .line 241
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->f(Lcom/google/android/maps/driveabout/vector/dm;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->D:Z

    .line 242
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dm;->g(Lcom/google/android/maps/driveabout/vector/dm;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->E:Z

    .line 244
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->E:Z

    if-eqz v0, :cond_7e

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/di;->l()Ls/aB;

    move-result-object v0

    :goto_35
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/di;->F:Ls/aB;

    .line 248
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/di;->v:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/di;->w:I

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/di;->y:I

    .line 250
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/di;->B:I

    .line 251
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/di;->v:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/di;->w:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 252
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_80

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_80

    .line 253
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tile type with key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already defined"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 244
    :cond_7e
    const/4 v0, 0x0

    goto :goto_35

    .line 255
    :cond_80
    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_9d

    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x20

    if-le v0, v1, :cond_9d

    .line 260
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Currently maximum 32 tile types allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_9d
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/dm;Lcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/di;-><init>(Lcom/google/android/maps/driveabout/vector/dm;)V

    return-void
.end method

.method public static a(I)Lcom/google/android/maps/driveabout/vector/di;
    .registers 3
    .parameter

    .prologue
    .line 398
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/di;

    return-object v0
.end method

.method static b()I
    .registers 2

    .prologue
    .line 363
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dF;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    .line 364
    const/16 v1, 0x80

    mul-int/lit8 v0, v0, 0x12

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(I)I
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/di;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/di;)Z
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->C:Z

    return v0
.end method

.method static c()I
    .registers 2

    .prologue
    .line 375
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dF;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    .line 376
    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private static c(I)I
    .registers 2
    .parameter

    .prologue
    .line 350
    const/16 v0, 0xa0

    if-le p0, v0, :cond_6

    const/4 v0, 0x3

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x1

    goto :goto_5
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/di;)Z
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->D:Z

    return v0
.end method

.method public static e()Ljava/lang/Iterable;
    .registers 1

    .prologue
    .line 405
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m()Lo/ag;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->L:Lo/ag;

    return-object v0
.end method

.method static synthetic n()Lo/ag;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->M:Lo/ag;

    return-object v0
.end method

.method static synthetic o()Lo/ag;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->N:Lo/ag;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public a(ILcom/google/android/maps/driveabout/vector/D;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 447
    return p1
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/di;)I
    .registers 4
    .parameter

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/di;->B:I

    iget v1, p1, Lcom/google/android/maps/driveabout/vector/di;->B:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lo/aa;)Lo/Q;
    .registers 3
    .parameter

    .prologue
    .line 479
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Lad/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Ls/aH;
.end method

.method public a(Ljava/lang/String;ZLs/t;)Ls/s;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->E:Z

    if-nez v0, :cond_6

    .line 331
    const/4 v0, 0x0

    .line 333
    :goto_5
    return-object v0

    :cond_6
    new-instance v0, Ls/aq;

    if-eqz p2, :cond_14

    const/4 v2, -0x1

    :goto_b
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/di;->F:Ls/aB;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ls/aq;-><init>(Ljava/lang/String;ILs/aB;Lcom/google/android/maps/driveabout/vector/di;Ls/t;)V

    goto :goto_5

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/di;->a()I

    move-result v2

    goto :goto_b
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 2
    .parameter

    .prologue
    .line 472
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 57
    check-cast p1, Lcom/google/android/maps/driveabout/vector/di;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/di;->a(Lcom/google/android/maps/driveabout/vector/di;)I

    move-result v0

    return v0
.end method

.method public d()Ls/aE;
    .registers 3

    .prologue
    .line 385
    new-instance v0, Ls/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->c()I

    move-result v1

    invoke-direct {v0, v1}, Ls/aC;-><init>(I)V

    return-object v0
.end method

.method public f()I
    .registers 3

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/di;->v:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/di;->w:I

    add-int/2addr v0, v1

    return v0
.end method

.method public g()I
    .registers 2

    .prologue
    .line 419
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/di;->B:I

    return v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 454
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 462
    const/4 v0, 0x0

    return v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/aJ;
    .registers 2

    .prologue
    .line 486
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Lo/ag;
    .registers 2

    .prologue
    .line 493
    const/4 v0, 0x0

    return-object v0
.end method

.method abstract l()Ls/aB;
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 427
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_1d

    aget-object v3, v1, v0

    .line 429
    :try_start_e
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_1a

    .line 430
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_17} :catch_19

    move-result-object v0

    .line 436
    :goto_18
    return-object v0

    .line 432
    :catch_19
    move-exception v3

    .line 427
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 436
    :cond_1d
    const-string v0, "unknown"

    goto :goto_18
.end method
