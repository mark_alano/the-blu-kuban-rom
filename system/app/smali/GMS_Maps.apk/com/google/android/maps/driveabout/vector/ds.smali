.class Lcom/google/android/maps/driveabout/vector/ds;
.super Lcom/google/android/maps/driveabout/vector/di;
.source "SourceFile"


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/dt;)V
    .registers 3
    .parameter

    .prologue
    .line 915
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/di;-><init>(Lcom/google/android/maps/driveabout/vector/dm;Lcom/google/android/maps/driveabout/vector/dj;)V

    .line 916
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/dt;Lcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 887
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/ds;-><init>(Lcom/google/android/maps/driveabout/vector/dt;)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 3

    .prologue
    .line 908
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 909
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LayerTileType does not support disk caches."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 911
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lad/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Ls/aH;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 896
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/dd;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 898
    new-instance v1, Ls/T;

    invoke-direct {v1, p1, v0, p4, p5}, Ls/T;-><init>(Lad/p;ILjava/util/Locale;Ljava/io/File;)V

    return-object v1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 887
    check-cast p1, Lcom/google/android/maps/driveabout/vector/di;

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/di;->a(Lcom/google/android/maps/driveabout/vector/di;)I

    move-result v0

    return v0
.end method

.method public l()Ls/aB;
    .registers 2

    .prologue
    .line 903
    const/4 v0, 0x0

    return-object v0
.end method
