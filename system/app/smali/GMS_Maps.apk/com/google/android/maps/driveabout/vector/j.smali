.class public Lcom/google/android/maps/driveabout/vector/J;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/maps/driveabout/vector/E;


# instance fields
.field private final b:Lcom/google/android/maps/driveabout/vector/K;

.field private c:Lcom/google/android/maps/driveabout/vector/K;

.field private d:Lcom/google/android/maps/driveabout/vector/K;

.field private final e:Ljava/util/List;

.field private f:Lo/ad;

.field private g:F

.field private h:F

.field private i:I

.field private final j:LE/o;

.field private final k:LE/d;

.field private final l:LE/i;

.field private m:Lcom/google/android/maps/driveabout/vector/E;

.field private final n:F

.field private final o:I

.field private p:Z

.field private q:Z

.field private final r:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->k:Lcom/google/android/maps/driveabout/vector/E;

    sput-object v0, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method public constructor <init>(Lo/X;II[IF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 154
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 155
    new-instance v0, Lcom/google/android/maps/driveabout/vector/K;

    invoke-direct {v0, p1, p4}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    .line 156
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    .line 157
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/J;->n:F

    .line 158
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    .line 159
    new-array v0, v2, [Lo/X;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 160
    invoke-static {v0, v2}, Lx/h;->a(Ljava/util/List;Z)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    .line 162
    invoke-static {v0, v2}, Lx/h;->b(Ljava/util/List;Z)I

    move-result v0

    .line 165
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/J;->o:I

    .line 171
    new-instance v1, LE/o;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    invoke-direct {v1, v2}, LE/o;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    .line 172
    new-instance v1, LE/i;

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->r:I

    invoke-direct {v1, v2}, LE/i;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    .line 173
    new-instance v1, LE/d;

    invoke-direct {v1, v0}, LE/d;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    .line 175
    sget-object v0, Lcom/google/android/maps/driveabout/vector/J;->a:Lcom/google/android/maps/driveabout/vector/E;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    .line 176
    return-void
.end method

.method static a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 414
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v0, p1, p2}, Lo/X;->a(FI)[Z

    move-result-object v3

    .line 417
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-eqz v0, :cond_23

    move v0, v1

    .line 418
    :goto_d
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_23

    .line 419
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    aget v4, v4, v0

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    if-eq v4, v5, :cond_20

    .line 420
    aput-boolean v1, v3, v0

    .line 418
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_23
    move v0, v2

    move v1, v2

    .line 427
    :goto_25
    array-length v4, v3

    if-ge v0, v4, :cond_31

    .line 428
    aget-boolean v4, v3, v0

    if-eqz v4, :cond_2e

    .line 429
    add-int/lit8 v1, v1, 0x1

    .line 427
    :cond_2e
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 433
    :cond_31
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-ne v1, v0, :cond_3a

    .line 454
    :goto_39
    return-object p0

    .line 437
    :cond_3a
    const/4 v0, 0x0

    .line 438
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-eqz v4, :cond_41

    .line 439
    new-array v0, v1, [I

    .line 441
    :cond_41
    mul-int/lit8 v1, v1, 0x3

    new-array v4, v1, [I

    .line 443
    new-instance v5, Lo/T;

    invoke-direct {v5}, Lo/T;-><init>()V

    move v1, v2

    .line 444
    :goto_4b
    array-length v6, v3

    if-ge v2, v6, :cond_67

    .line 445
    aget-boolean v6, v3, v2

    if-eqz v6, :cond_64

    .line 446
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v6, v2, v5}, Lo/X;->a(ILo/T;)V

    .line 447
    invoke-virtual {v5, v4, v1}, Lo/T;->a([II)V

    .line 448
    if-eqz v0, :cond_62

    .line 449
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    aget v6, v6, v2

    aput v6, v0, v1

    .line 451
    :cond_62
    add-int/lit8 v1, v1, 0x1

    .line 444
    :cond_64
    add-int/lit8 v2, v2, 0x1

    goto :goto_4b

    .line 454
    :cond_67
    new-instance p0, Lcom/google/android/maps/driveabout/vector/K;

    invoke-static {v4}, Lo/X;->a([I)Lo/X;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    goto :goto_39
.end method

.method private a(LD/a;LC/a;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    .line 295
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    .line 296
    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 299
    invoke-virtual {p1}, LD/a;->p()V

    .line 300
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 301
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 304
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 305
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 306
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    invoke-static {p1, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    .line 308
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/K;LC/a;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x1

    .line 478
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v4

    .line 479
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v5

    .line 480
    invoke-virtual {p2}, LC/a;->y()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/J;->n:F

    mul-float v2, v0, v1

    .line 481
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v0

    .line 482
    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    const/high16 v6, 0x3f80

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    .line 489
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    if-eqz v1, :cond_3a

    iget-object v5, p1, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    .line 490
    :goto_2b
    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v1}, Lo/X;->b()I

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/J;->o:I

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    move-object v1, v0

    invoke-virtual/range {v1 .. v6}, Lx/h;->a(IZI[ILE/k;)V

    .line 493
    return-void

    :cond_3a
    move-object v5, v9

    .line 489
    goto :goto_2b
.end method

.method private a(LC/a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa0

    .line 315
    monitor-enter p0

    .line 316
    :try_start_5
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    if-eqz v2, :cond_e

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    .line 318
    monitor-exit p0

    .line 322
    :goto_d
    return v1

    .line 320
    :cond_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_24

    .line 321
    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    .line 322
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_21

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_22

    :cond_21
    move v0, v1

    :cond_22
    move v1, v0

    goto :goto_d

    .line 320
    :catchall_24
    move-exception v0

    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    throw v0
.end method

.method private b(LD/a;LC/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0, p1}, LE/o;->a(LD/a;)V

    .line 464
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual {v0, p1}, LE/d;->a(LD/a;)V

    .line 465
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/K;

    .line 468
    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;LC/a;)V

    goto :goto_15

    .line 470
    :cond_25
    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/J;->h:F

    .line 471
    return-void
.end method

.method private b(LC/a;)Z
    .registers 6
    .parameter

    .prologue
    const/high16 v3, 0x4000

    const/4 v0, 0x1

    .line 330
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    if-nez v1, :cond_8

    .line 344
    :cond_7
    :goto_7
    return v0

    .line 335
    :cond_8
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    .line 336
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_7

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    div-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_7

    .line 341
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->c()Lo/ae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/ad;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 344
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static c(I)I
    .registers 3
    .parameter

    .prologue
    .line 522
    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method private c(LC/a;)V
    .registers 10
    .parameter

    .prologue
    const v7, 0x1fffffff

    const/high16 v6, -0x2000

    const/4 v2, 0x0

    .line 353
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 354
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/J;->a(F)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    .line 359
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lo/aQ;->b()Lo/ad;

    move-result-object v1

    .line 361
    invoke-virtual {v1}, Lo/ad;->g()I

    move-result v3

    .line 362
    invoke-virtual {v1}, Lo/ad;->h()I

    move-result v4

    .line 363
    const v5, 0x71c71c7

    .line 364
    if-gt v3, v5, :cond_2a

    if-le v4, v5, :cond_69

    .line 365
    :cond_2a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    new-instance v0, Lo/ad;

    new-instance v1, Lo/T;

    invoke-direct {v1, v6, v6}, Lo/T;-><init>(II)V

    new-instance v3, Lo/T;

    invoke-direct {v3, v7, v7}, Lo/T;-><init>(II)V

    invoke-direct {v0, v1, v3}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    .line 398
    :cond_40
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    .line 399
    :goto_4b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_ef

    .line 400
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/K;

    int-to-float v4, v1

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 399
    add-int/lit8 v2, v2, 0x1

    goto :goto_4b

    .line 370
    :cond_69
    new-instance v5, Lo/T;

    mul-int/lit8 v3, v3, 0x4

    mul-int/lit8 v4, v4, 0x4

    invoke-direct {v5, v3, v4}, Lo/T;-><init>(II)V

    .line 371
    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v5}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    .line 372
    invoke-virtual {v1}, Lo/ad;->e()Lo/T;

    move-result-object v1

    invoke-virtual {v1, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    .line 373
    invoke-virtual {v3, v3}, Lo/T;->j(Lo/T;)V

    .line 374
    invoke-virtual {v1, v1}, Lo/T;->j(Lo/T;)V

    .line 375
    new-instance v4, Lo/ad;

    invoke-direct {v4, v3, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v4, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    .line 377
    new-instance v1, Lo/h;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    invoke-direct {v1, v3}, Lo/h;-><init>(Lo/ae;)V

    .line 379
    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    if-nez v3, :cond_bf

    .line 380
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 381
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    invoke-virtual {v1, v0, v3}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    move v1, v2

    .line 382
    :goto_a4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_40

    .line 383
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    new-instance v5, Lcom/google/android/maps/driveabout/vector/K;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a4

    .line 386
    :cond_bf
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 387
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 388
    iget-object v3, v0, Lcom/google/android/maps/driveabout/vector/K;->a:Lo/X;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/K;->b:[I

    invoke-virtual {v1, v3, v0, v4, v5}, Lo/h;->a(Lo/X;[ILjava/util/List;Ljava/util/List;)V

    move v3, v2

    .line 390
    :goto_cf
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_40

    .line 391
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/J;->e:Ljava/util/List;

    new-instance v7, Lcom/google/android/maps/driveabout/vector/K;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    invoke-direct {v7, v0, v1}, Lcom/google/android/maps/driveabout/vector/K;-><init>(Lo/X;[I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_cf

    .line 403
    :cond_ef
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/J;->g:F

    .line 404
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    if-nez v0, :cond_24

    .line 513
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    int-to-float v1, v1

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    .line 515
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/J;->c(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/J;->a(Lcom/google/android/maps/driveabout/vector/K;FI)Lcom/google/android/maps/driveabout/vector/K;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->d:Lcom/google/android/maps/driveabout/vector/K;

    .line 518
    :cond_24
    return-void
.end method


# virtual methods
.method a(F)Lcom/google/android/maps/driveabout/vector/K;
    .registers 3
    .parameter

    .prologue
    .line 498
    const/high16 v0, 0x4120

    cmpl-float v0, p1, v0

    if-lez v0, :cond_9

    .line 499
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->b:Lcom/google/android/maps/driveabout/vector/K;

    .line 505
    :goto_8
    return-object v0

    .line 501
    :cond_9
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/J;->e()V

    .line 502
    const/high16 v0, 0x40c0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_15

    .line 503
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->c:Lcom/google/android/maps/driveabout/vector/K;

    goto :goto_8

    .line 505
    :cond_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->d:Lcom/google/android/maps/driveabout/vector/K;

    goto :goto_8
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->k:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 288
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_2b

    .line 251
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->f:Lo/ad;

    if-nez v0, :cond_d

    .line 253
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/J;->c(LC/a;)V

    .line 255
    :cond_d
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 256
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/J;->b(LD/a;LC/a;)V

    .line 258
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->j:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_2b

    .line 259
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 260
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 261
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/J;->a(LD/a;LC/a;)V

    .line 262
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 265
    :cond_2b
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 2
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    .line 209
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 201
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/J;->i:I

    .line 205
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 218
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 219
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->c(LC/a;)V

    .line 220
    monitor-enter p0

    .line 221
    const/4 v0, 0x1

    :try_start_c
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z

    .line 222
    monitor-exit p0

    .line 227
    :cond_f
    return v1

    .line 222
    :catchall_10
    move-exception v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_10

    throw v0
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 269
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/J;->a(LD/a;)V

    .line 270
    return-void
.end method

.method public declared-synchronized c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 235
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    if-eq v0, p1, :cond_a

    .line 236
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/J;->p:Z

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/J;->q:Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 242
    :cond_a
    monitor-exit p0

    return-void

    .line 235
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/J;->m:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
