.class public Lcom/google/android/maps/driveabout/vector/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:I

.field private c:[F

.field private d:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/s;->d:Z

    return-void
.end method

.method private static a(FFFF)I
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    sub-float v0, p0, p2

    sub-float v1, p0, p2

    mul-float/2addr v0, v1

    sub-float v1, p1, p3

    sub-float v2, p1, p3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a()V
    .registers 4

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/t;

    .line 160
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/t;->a(Z)V

    goto :goto_6

    .line 162
    :cond_17
    return-void
.end method

.method private a(FFLo/T;LC/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/t;

    .line 149
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/t;->a(FFLo/T;LC/a;)V

    goto :goto_6

    .line 151
    :cond_16
    return-void
.end method

.method private b()I
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 173
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_c

    .line 209
    :goto_b
    return v4

    .line 178
    :cond_c
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v6, :cond_16

    move v4, v0

    .line 179
    goto :goto_b

    .line 184
    :cond_16
    const v1, 0x7fffffff

    move v2, v1

    move v3, v4

    move v1, v0

    .line 185
    :goto_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_42

    .line 186
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/t;

    .line 187
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/t;->b()Z

    move-result v5

    if-eqz v5, :cond_36

    .line 185
    :cond_32
    :goto_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1c

    .line 190
    :cond_36
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/t;->a()I

    move-result v5

    if-ge v5, v2, :cond_32

    .line 192
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/t;->a()I

    move-result v2

    move v3, v1

    goto :goto_32

    .line 198
    :cond_42
    if-ne v3, v4, :cond_5d

    .line 199
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/s;->a()V

    .line 203
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    if-eq v0, v4, :cond_58

    .line 204
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/t;

    invoke-virtual {v0, v6}, Lcom/google/android/maps/driveabout/vector/t;->a(Z)V

    .line 207
    :cond_58
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/s;->b()I

    move-result v4

    goto :goto_b

    :cond_5d
    move v4, v3

    .line 209
    goto :goto_b
.end method

.method private b(FFLo/T;LC/a;Ljava/util/List;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 120
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    if-nez v0, :cond_e

    .line 121
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    .line 123
    :cond_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 124
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 126
    const/high16 v0, 0x41f0

    invoke-virtual {p4}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 128
    mul-int v6, v0, v0

    .line 130
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_26
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/d;

    .line 131
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/d;->a(Ljava/util/List;FFLo/T;LC/a;I)V

    goto :goto_26

    .line 134
    :cond_3c
    return-void
.end method


# virtual methods
.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/s;->d:Z

    .line 54
    return-void
.end method

.method public a(FFLo/T;LC/a;Ljava/util/List;)Z
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    const/high16 v0, 0x41f0

    invoke-virtual {p4}, LC/a;->m()F

    move-result v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 71
    mul-int/2addr v0, v0

    .line 74
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/s;->d:Z

    if-nez v3, :cond_21

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    if-eqz v3, :cond_21

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/s;->c:[F

    aget v4, v4, v2

    invoke-static {v3, v4, p1, p2}, Lcom/google/android/maps/driveabout/vector/s;->a(FFFF)I

    move-result v3

    if-le v3, v0, :cond_4b

    .line 77
    :cond_21
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/s;->d:Z

    .line 78
    invoke-direct/range {p0 .. p5}, Lcom/google/android/maps/driveabout/vector/s;->b(FFLo/T;LC/a;Ljava/util/List;)V

    move v0, v2

    .line 84
    :goto_27
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    .line 85
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/s;->b()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    .line 86
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_50

    .line 87
    if-nez v0, :cond_3a

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    if-eq v3, v0, :cond_4a

    .line 88
    :cond_3a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/s;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/s;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/t;

    .line 89
    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/t;->a(Z)V

    .line 90
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/t;->c()V

    .line 94
    :cond_4a
    :goto_4a
    return v2

    .line 81
    :cond_4b
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/s;->a(FFLo/T;LC/a;)V

    move v0, v1

    goto :goto_27

    :cond_50
    move v2, v1

    .line 94
    goto :goto_4a
.end method
