.class public Lcom/google/android/maps/driveabout/vector/x;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/E;

.field private final b:[F

.field private c:Lcom/google/android/maps/driveabout/vector/c;

.field private d:Lo/T;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/maps/driveabout/vector/f;

.field private g:Landroid/graphics/Bitmap;

.field private h:LD/b;

.field private i:Lcom/google/android/maps/driveabout/vector/y;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:F

.field private final o:I

.field private final p:Lh/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 3
    .parameter

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->A:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {p0, v0, p1}, Lcom/google/android/maps/driveabout/vector/x;-><init>(Lcom/google/android/maps/driveabout/vector/E;Landroid/content/res/Resources;)V

    .line 140
    return-void
.end method

.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Landroid/content/res/Resources;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 69
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    .line 75
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    .line 117
    new-instance v0, Lh/e;

    const-wide/16 v1, 0xbb8

    const-wide/16 v3, 0x2710

    sget-object v5, Lh/g;->c:Lh/g;

    const/high16 v6, 0x1

    const v7, 0x8000

    invoke-direct/range {v0 .. v7}, Lh/e;-><init>(JJLh/g;II)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    .line 148
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->a:Lcom/google/android/maps/driveabout/vector/E;

    .line 149
    if-nez p2, :cond_2c

    const v0, 0xffff00

    :goto_29
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    .line 152
    return-void

    .line 149
    :cond_2c
    const v0, 0x7f090093

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_29
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 313
    invoke-virtual {p3, p2}, LE/o;->d(LD/a;)V

    .line 314
    invoke-virtual {p4, p2}, LE/i;->d(LD/a;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    invoke-virtual {v0, p1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 316
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 317
    return-void
.end method

.method private c(FFLC/a;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 331
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 351
    :cond_f
    :goto_f
    return v0

    .line 334
    :cond_10
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v2

    invoke-virtual {p3, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    .line 335
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    aget v4, v2, v1

    int-to-float v4, v4

    aget v5, v2, v0

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/maps/driveabout/vector/f;->a(FF)V

    .line 337
    aget v3, v2, v1

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    .line 338
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    add-int/2addr v4, v3

    .line 341
    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_39

    int-to-float v3, v4

    cmpl-float v3, p1, v3

    if-lez v3, :cond_3b

    :cond_39
    move v0, v1

    .line 342
    goto :goto_f

    .line 345
    :cond_3b
    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v3

    sub-int/2addr v2, v3

    .line 346
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    sub-int v3, v2, v3

    .line 347
    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_52

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-lez v2, :cond_f

    :cond_52
    move v0, v1

    .line 348
    goto :goto_f
.end method

.method private h()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 476
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    if-eqz v0, :cond_c

    .line 477
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 478
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    .line 480
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_17

    .line 481
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 482
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    .line 484
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_20

    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->b()V

    .line 487
    :cond_20
    return-void
.end method

.method private i()Landroid/graphics/Bitmap;
    .registers 7

    .prologue
    const/high16 v2, -0x8000

    const/4 v3, 0x0

    .line 509
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 511
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 513
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 514
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    .line 515
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    .line 516
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 518
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, LD/b;->c(II)I

    move-result v0

    .line 519
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    const/16 v2, 0x20

    invoke-static {v1, v2}, LD/b;->c(II)I

    move-result v1

    .line 522
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 523
    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 525
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 526
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/x;->l:I

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    .line 527
    iget v5, p0, Lcom/google/android/maps/driveabout/vector/x;->m:I

    sub-int v5, v1, v5

    .line 528
    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 529
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 530
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/maps/driveabout/vector/f;->d(FF)V

    .line 532
    return-object v2
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v7, 0x1

    .line 194
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_12

    .line 195
    monitor-enter p0

    .line 196
    :try_start_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_13

    .line 197
    :cond_11
    monitor-exit p0

    .line 309
    :cond_12
    :goto_12
    return-void

    .line 201
    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/c;->f()Lo/D;

    move-result-object v1

    .line 205
    if-eqz v1, :cond_42

    if-eqz v0, :cond_42

    .line 206
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v2

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v2, v1}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_42

    .line 210
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->b(Lo/T;)V

    .line 211
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    invoke-virtual {v1, p2, v0}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Lo/T;->b(I)V

    .line 212
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->d:Lo/T;

    .line 216
    :cond_42
    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v1

    .line 217
    invoke-virtual {v1}, Lo/aS;->a()Lo/aR;

    move-result-object v2

    .line 218
    if-nez v0, :cond_51

    .line 219
    monitor-exit p0

    goto :goto_12

    .line 306
    :catchall_4e
    move-exception v0

    monitor-exit p0
    :try_end_50
    .catchall {:try_start_9 .. :try_end_50} :catchall_4e

    throw v0

    .line 221
    :cond_51
    :try_start_51
    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_5d

    invoke-virtual {v1, v0}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-nez v1, :cond_67

    :cond_5d
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v1, p2}, Lcom/google/android/maps/driveabout/vector/c;->a(LC/a;)Z

    move-result v1

    if-nez v1, :cond_67

    .line 223
    monitor-exit p0

    goto :goto_12

    .line 229
    :cond_67
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v1

    if-nez v1, :cond_8a

    .line 231
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    invoke-virtual {p2, v0, v1}, LC/a;->a(Lo/T;[F)V

    .line 232
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    .line 235
    :cond_8a
    if-nez v0, :cond_d0

    .line 239
    const-string v0, "UI"

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Null point for ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->b:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "); "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "camera="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 242
    monitor-exit p0

    goto/16 :goto_12

    .line 245
    :cond_d0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 246
    invoke-virtual {p1}, LD/a;->p()V

    .line 247
    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 248
    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 250
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    if-nez v2, :cond_f8

    .line 251
    new-instance v2, LD/b;

    invoke-direct {v2, p1}, LD/b;-><init>(LD/a;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    .line 252
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->h:LD/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 255
    :cond_f8
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 256
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->n:F

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 257
    invoke-static {v1, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    .line 263
    invoke-virtual {p2}, LC/a;->t()Lo/T;

    move-result-object v2

    invoke-virtual {p2}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v0

    .line 265
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 266
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    .line 268
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v0

    .line 269
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v5

    if-eqz v5, :cond_173

    const/4 v0, 0x0

    .line 272
    :goto_12e
    neg-float v4, v2

    const/high16 v5, 0x3f00

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v5}, Lcom/google/android/maps/driveabout/vector/c;->h()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v6}, Lcom/google/android/maps/driveabout/vector/c;->h_()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v0, v6

    invoke-interface {v1, v4, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 274
    const/high16 v0, 0x3f80

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 276
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->c()Z

    move-result v0

    if-eqz v0, :cond_17c

    .line 277
    const/16 v0, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 279
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 281
    iget-object v0, p1, LD/a;->g:LE/o;

    iget-object v2, p1, LD/a;->d:LE/i;

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V

    .line 305
    :cond_168
    :goto_168
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 306
    monitor-exit p0
    :try_end_16c
    .catchall {:try_start_51 .. :try_end_16c} :catchall_4e

    .line 307
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->f()V

    goto/16 :goto_12

    .line 269
    :cond_173
    :try_start_173
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    sub-float v4, v5, v4

    mul-float/2addr v0, v4

    goto :goto_12e

    .line 284
    :cond_17c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->a()Lcom/google/android/maps/driveabout/vector/g;

    move-result-object v0

    .line 285
    if-nez v0, :cond_1af

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aF()Z

    move-result v2

    if-eqz v2, :cond_1af

    .line 286
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v2, p1}, Lh/e;->a(LD/a;)I

    move-result v2

    .line 287
    if-ge v2, v7, :cond_1af

    .line 288
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 289
    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 290
    const/high16 v3, 0x1

    const/high16 v4, 0x1

    const/high16 v5, 0x1

    invoke-interface {v1, v3, v4, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 294
    :cond_1af
    iget-object v2, p1, LD/a;->g:LE/o;

    iget-object v3, p1, LD/a;->d:LE/i;

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V

    .line 296
    if-eqz v0, :cond_168

    .line 297
    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 299
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/x;->o:I

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 300
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/g;->a:LE/o;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/g;->b:LE/i;

    invoke-direct {p0, v1, p1, v2, v0}, Lcom/google/android/maps/driveabout/vector/x;->a(Ljavax/microedition/khronos/opengles/GL10;LD/a;LE/o;LE/i;)V
    :try_end_1cd
    .catchall {:try_start_173 .. :try_end_1cd} :catchall_4e

    goto :goto_168
.end method

.method public declared-synchronized a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 170
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_b

    .line 171
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 173
    :cond_b
    monitor-exit p0

    return-void

    .line 170
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 424
    monitor-enter p0

    :try_start_1
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    .line 426
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v0}, Lh/e;->a()V

    .line 430
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-ne v0, p1, :cond_32

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_32

    .line 431
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_1d

    .line 432
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->g_()V

    .line 434
    :cond_1d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    if-eqz v0, :cond_30

    .line 435
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    .line 436
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    .line 441
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_30
    .catchall {:try_start_1 .. :try_end_30} :catchall_5e

    .line 463
    :cond_30
    :goto_30
    monitor-exit p0

    return-void

    .line 447
    :cond_32
    :try_start_32
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_3b

    .line 448
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->d()V

    .line 450
    :cond_3b
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    .line 451
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->g_()V

    .line 452
    if-eqz p2, :cond_4a

    .line 453
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/f;->e()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    .line 456
    :cond_4a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    if-eqz v0, :cond_30

    .line 457
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    .line 458
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    .line 461
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_5d
    .catchall {:try_start_32 .. :try_end_5d} :catchall_5e

    goto :goto_30

    .line 424
    :catchall_5e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/y;)V
    .registers 3
    .parameter

    .prologue
    .line 155
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 156
    monitor-exit p0

    return-void

    .line 155
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(FFLC/a;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 401
    monitor-enter p0

    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->p:Lh/e;

    invoke-virtual {v0}, Lh/e;->a()V

    .line 403
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/f;->c(FF)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_16

    .line 404
    const/4 v0, 0x1

    .line 406
    :goto_12
    monitor-exit p0

    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_12

    .line 401
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a_(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 356
    monitor-enter p0

    .line 357
    :try_start_1
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 358
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/f;->b(FF)V

    .line 359
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->g()V

    .line 360
    const/4 v0, 0x1

    monitor-exit p0

    .line 369
    :goto_11
    return v0

    .line 361
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_1c

    .line 363
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->g()V

    .line 368
    :cond_1c
    monitor-exit p0

    .line 369
    const/4 v0, 0x0

    goto :goto_11

    .line 368
    :catchall_1f
    move-exception v0

    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public b(FFLo/T;LC/a;)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 375
    monitor-enter p0

    .line 376
    :try_start_1
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    if-eqz v0, :cond_12

    .line 378
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/y;->b(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 380
    :cond_12
    const/4 v0, 0x1

    monitor-exit p0

    .line 383
    :goto_14
    return v0

    .line 382
    :cond_15
    monitor-exit p0

    .line 383
    const/4 v0, 0x0

    goto :goto_14

    .line 382
    :catchall_18
    move-exception v0

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public declared-synchronized b(LC/a;LD/a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 178
    monitor-enter p0

    const/high16 v0, 0x3f80

    :try_start_3
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    invoke-virtual {p1, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->n:F

    .line 179
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    if-ne v0, v1, :cond_1d

    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    if-eq v0, v1, :cond_36

    .line 181
    :cond_1d
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->j:I

    .line 182
    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/x;->k:I

    .line 183
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_36

    .line 184
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V

    .line 185
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->g:Landroid/graphics/Bitmap;
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_39

    .line 188
    :cond_36
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 178
    :catchall_39
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 165
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/x;->h()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    .line 166
    monitor-exit p0

    return-void

    .line 165
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 388
    monitor-enter p0

    .line 389
    :try_start_1
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/vector/x;->c(FFLC/a;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 392
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/x;->k_()V

    .line 393
    const/4 v0, 0x1

    monitor-exit p0

    .line 396
    :goto_c
    return v0

    .line 395
    :cond_d
    monitor-exit p0

    .line 396
    const/4 v0, 0x0

    goto :goto_c

    .line 395
    :catchall_10
    move-exception v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public declared-synchronized e()Lcom/google/android/maps/driveabout/vector/c;
    .registers 2

    .prologue
    .line 321
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .registers 2

    .prologue
    .line 469
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_a

    .line 470
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->d()V

    .line 472
    :cond_a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 473
    monitor-exit p0

    return-void

    .line 469
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected g()V
    .registers 3

    .prologue
    .line 496
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/D;->g()V

    .line 499
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    if-eqz v0, :cond_e

    .line 500
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->i:Lcom/google/android/maps/driveabout/vector/y;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/x;->c:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/y;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 502
    :cond_e
    return-void
.end method

.method public declared-synchronized k_()V
    .registers 2

    .prologue
    .line 411
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_a

    .line 412
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->f:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->b()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 414
    :cond_a
    monitor-exit p0

    return-void

    .line 411
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/x;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
