.class public final enum Lcom/google/commerce/wireless/topiary/V;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/commerce/wireless/topiary/V;

.field public static final enum b:Lcom/google/commerce/wireless/topiary/V;

.field public static final enum c:Lcom/google/commerce/wireless/topiary/V;

.field private static final synthetic d:[Lcom/google/commerce/wireless/topiary/V;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/commerce/wireless/topiary/V;

    const-string v1, "Never"

    invoke-direct {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/V;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/V;->a:Lcom/google/commerce/wireless/topiary/V;

    .line 24
    new-instance v0, Lcom/google/commerce/wireless/topiary/V;

    const-string v1, "Check"

    invoke-direct {v0, v1, v3}, Lcom/google/commerce/wireless/topiary/V;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    .line 25
    new-instance v0, Lcom/google/commerce/wireless/topiary/V;

    const-string v1, "Always"

    invoke-direct {v0, v1, v4}, Lcom/google/commerce/wireless/topiary/V;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/V;->c:Lcom/google/commerce/wireless/topiary/V;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/commerce/wireless/topiary/V;

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->a:Lcom/google/commerce/wireless/topiary/V;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->c:Lcom/google/commerce/wireless/topiary/V;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/commerce/wireless/topiary/V;->d:[Lcom/google/commerce/wireless/topiary/V;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/V;
    .registers 2
    .parameter

    .prologue
    .line 22
    const-class v0, Lcom/google/commerce/wireless/topiary/V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/V;

    return-object v0
.end method

.method public static values()[Lcom/google/commerce/wireless/topiary/V;
    .registers 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/commerce/wireless/topiary/V;->d:[Lcom/google/commerce/wireless/topiary/V;

    invoke-virtual {v0}, [Lcom/google/commerce/wireless/topiary/V;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/commerce/wireless/topiary/V;

    return-object v0
.end method
