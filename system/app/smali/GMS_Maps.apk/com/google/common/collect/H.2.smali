.class Lcom/google/common/collect/H;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field final b:Ljava/util/Collection;

.field final synthetic c:Lcom/google/common/collect/G;


# direct methods
.method constructor <init>(Lcom/google/common/collect/G;)V
    .registers 4
    .parameter

    .prologue
    .line 503
    iput-object p1, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    iget-object v0, v0, Lcom/google/common/collect/G;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/common/collect/H;->b:Ljava/util/Collection;

    .line 504
    iget-object v0, p1, Lcom/google/common/collect/G;->f:Lcom/google/common/collect/x;

    iget-object v1, p1, Lcom/google/common/collect/G;->c:Ljava/util/Collection;

    invoke-static {v0, v1}, Lcom/google/common/collect/x;->a(Lcom/google/common/collect/x;Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    .line 505
    return-void
.end method

.method constructor <init>(Lcom/google/common/collect/G;Ljava/util/Iterator;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    iget-object v0, v0, Lcom/google/common/collect/G;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/common/collect/H;->b:Ljava/util/Collection;

    .line 508
    iput-object p2, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    .line 509
    return-void
.end method


# virtual methods
.method a()V
    .registers 3

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    invoke-virtual {v0}, Lcom/google/common/collect/G;->a()V

    .line 517
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    iget-object v0, v0, Lcom/google/common/collect/G;->c:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/common/collect/H;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_13

    .line 518
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 520
    :cond_13
    return-void
.end method

.method b()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/google/common/collect/H;->a()V

    .line 543
    iget-object v0, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    return-object v0
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/google/common/collect/H;->a()V

    .line 525
    iget-object v0, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/google/common/collect/H;->a()V

    .line 531
    iget-object v0, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/common/collect/H;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 537
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    iget-object v0, v0, Lcom/google/common/collect/G;->f:Lcom/google/common/collect/x;

    invoke-static {v0}, Lcom/google/common/collect/x;->b(Lcom/google/common/collect/x;)I

    .line 538
    iget-object v0, p0, Lcom/google/common/collect/H;->c:Lcom/google/common/collect/G;

    invoke-virtual {v0}, Lcom/google/common/collect/G;->b()V

    .line 539
    return-void
.end method
