.class final Lcom/google/common/collect/aJ;
.super Lcom/google/common/collect/ImmutableList;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/dH;


# instance fields
.field private final transient a:Lcom/google/common/collect/aR;

.field private final transient b:Lcom/google/common/collect/ImmutableList;


# direct methods
.method constructor <init>(Lcom/google/common/collect/aR;Lcom/google/common/collect/ImmutableList;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/common/collect/ImmutableList;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    .line 37
    iput-object p2, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    .line 38
    return-void
.end method


# virtual methods
.method public a(II)Lcom/google/common/collect/ImmutableList;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/common/collect/aJ;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/google/common/base/J;->a(III)V

    .line 63
    if-ne p1, p2, :cond_e

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/google/common/collect/dw;

    iget-object v1, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, p1, p2}, Lcom/google/common/collect/ImmutableList;->a(II)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    invoke-virtual {v2}, Lcom/google/common/collect/aR;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/dw;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Lcom/google/common/collect/dw;->d()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_d
.end method

.method public a(I)Lcom/google/common/collect/dZ;
    .registers 3
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->a(I)Lcom/google/common/collect/dZ;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->a()Z

    move-result v0

    return v0
.end method

.method public b()Lcom/google/common/collect/dY;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/common/collect/dZ;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->c()Lcom/google/common/collect/dZ;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    invoke-virtual {v0}, Lcom/google/common/collect/aR;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/common/collect/aJ;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/common/collect/aJ;->a:Lcom/google/common/collect/aR;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/common/collect/aJ;->c()Lcom/google/common/collect/dZ;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aJ;->a(I)Lcom/google/common/collect/dZ;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/common/collect/aJ;->b:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aJ;->a(II)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
