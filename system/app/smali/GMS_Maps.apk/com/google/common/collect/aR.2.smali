.class public abstract Lcom/google/common/collect/aR;
.super Lcom/google/common/collect/aS;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/dH;
.implements Ljava/util/SortedSet;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final d:Lcom/google/common/collect/aR;


# instance fields
.field final transient a:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 92
    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    .line 95
    new-instance v0, Lcom/google/common/collect/ae;

    sget-object v1, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/common/collect/ae;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/common/collect/aR;->d:Lcom/google/common/collect/aR;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .registers 2
    .parameter

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/google/common/collect/aS;-><init>()V

    .line 567
    iput-object p1, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    .line 568
    return-void
.end method

.method static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 560
    .line 561
    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;
    .registers 2
    .parameter

    .prologue
    .line 105
    sget-object v0, Lcom/google/common/collect/aR;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 106
    invoke-static {}, Lcom/google/common/collect/aR;->h()Lcom/google/common/collect/aR;

    move-result-object v0

    .line 108
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/google/common/collect/ae;

    invoke-direct {v0, p0}, Lcom/google/common/collect/ae;-><init>(Ljava/util/Comparator;)V

    goto :goto_c
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/aR;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 392
    invoke-static {p0, p1}, Lcom/google/common/collect/dI;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    .line 395
    if-eqz v0, :cond_14

    instance-of v0, p1, Lcom/google/common/collect/aR;

    if-eqz v0, :cond_14

    move-object v0, p1

    .line 397
    check-cast v0, Lcom/google/common/collect/aR;

    .line 398
    invoke-virtual {v0}, Lcom/google/common/collect/aR;->a()Z

    move-result v1

    if-nez v1, :cond_14

    .line 404
    :goto_13
    return-object v0

    .line 402
    :cond_14
    invoke-static {p0, p1}, Lcom/google/common/collect/dI;->b(Ljava/util/Comparator;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 404
    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-static {p0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_13

    :cond_27
    new-instance v0, Lcom/google/common/collect/dw;

    invoke-direct {v0, v1, p0}, Lcom/google/common/collect/dw;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_13
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/google/common/collect/aR;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-static {p0, p1}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/common/collect/aR;
    .registers 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/common/collect/aR;->d:Lcom/google/common/collect/aR;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
.end method

.method abstract a(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method b(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, p2}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method abstract b(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
.end method

.method b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 625
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_17

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    .line 628
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0

    .line 627
    :cond_17
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public abstract b()Lcom/google/common/collect/dY;
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .registers 3
    .parameter

    .prologue
    .line 598
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 620
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/common/collect/aR;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method c(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 602
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/common/collect/aR;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/common/collect/aR;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Lcom/google/common/collect/aR;
    .registers 3
    .parameter

    .prologue
    .line 644
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aR;->d(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method d(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 648
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/common/collect/aR;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/common/collect/aR;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aR;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aR;->d(Ljava/lang/Object;)Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method
