.class Lcom/google/common/collect/bA;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:Z

.field b:Z

.field final synthetic c:Ljava/util/ListIterator;

.field final synthetic d:Lcom/google/common/collect/bz;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bz;Ljava/util/ListIterator;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 824
    iput-object p1, p0, Lcom/google/common/collect/bA;->d:Lcom/google/common/collect/bz;

    iput-object p2, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 831
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 832
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->a:Z

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->b:Z

    .line 833
    return-void
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .registers 2

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 844
    invoke-virtual {p0}, Lcom/google/common/collect/bA;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 845
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 847
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->a:Z

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->b:Z

    .line 848
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .registers 3

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/common/collect/bA;->d:Lcom/google/common/collect/bz;

    iget-object v1, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/common/collect/bz;->a(Lcom/google/common/collect/bz;I)I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/google/common/collect/bA;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_c

    .line 857
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 859
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->a:Z

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->b:Z

    .line 860
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .registers 2

    .prologue
    .line 864
    invoke-virtual {p0}, Lcom/google/common/collect/bA;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 868
    iget-boolean v0, p0, Lcom/google/common/collect/bA;->a:Z

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 869
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 870
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->b:Z

    iput-boolean v0, p0, Lcom/google/common/collect/bA;->a:Z

    .line 871
    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 874
    iget-boolean v0, p0, Lcom/google/common/collect/bA;->b:Z

    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 875
    iget-object v0, p0, Lcom/google/common/collect/bA;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 876
    return-void
.end method
