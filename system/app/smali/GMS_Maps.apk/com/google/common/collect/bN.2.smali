.class Lcom/google/common/collect/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final synthetic a:Lcom/google/common/collect/bu;

.field final synthetic b:Lcom/google/common/collect/bm;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bm;Lcom/google/common/collect/bu;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 853
    iput-object p1, p0, Lcom/google/common/collect/bn;->b:Lcom/google/common/collect/bm;

    iput-object p2, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->hasNext()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .registers 2

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->a()Lcom/google/common/collect/bt;

    move-result-object v0

    iget-object v0, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public nextIndex()I
    .registers 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->nextIndex()I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->b()Lcom/google/common/collect/bt;

    move-result-object v0

    iget-object v0, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public previousIndex()I
    .registers 2

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->previousIndex()I

    move-result v0

    return v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0}, Lcom/google/common/collect/bu;->remove()V

    .line 881
    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/common/collect/bn;->a:Lcom/google/common/collect/bu;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/bu;->a(Ljava/lang/Object;)V

    .line 885
    return-void
.end method
