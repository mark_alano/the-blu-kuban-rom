.class Lcom/google/common/collect/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Set;

.field b:Lcom/google/common/collect/bt;

.field c:Lcom/google/common/collect/bt;

.field final synthetic d:Lcom/google/common/collect/bj;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/bj;)V
    .registers 3
    .parameter

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-virtual {v0}, Lcom/google/common/collect/bj;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/dA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    .line 366
    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/google/common/collect/bs;-><init>(Lcom/google/common/collect/bj;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    .line 377
    iget-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 379
    :cond_12
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    .line 380
    iget-object v0, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/common/collect/bs;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/common/collect/bs;->b:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 381
    :cond_28
    iget-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 386
    iget-object v0, p0, Lcom/google/common/collect/bs;->d:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    iget-object v1, v1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bs;->c:Lcom/google/common/collect/bt;

    .line 388
    return-void

    .line 385
    :cond_15
    const/4 v0, 0x0

    goto :goto_5
.end method
