.class Lcom/google/common/collect/bo;
.super Ljava/util/AbstractMap;
.source "SourceFile"


# instance fields
.field a:Ljava/util/Set;

.field final synthetic b:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;)V
    .registers 2
    .parameter

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/google/common/collect/bo;->b:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4
    .parameter

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/google/common/collect/bo;->b:Lcom/google/common/collect/bj;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/bj;->d(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1059
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v0, 0x0

    :cond_d
    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4
    .parameter

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/common/collect/bo;->b:Lcom/google/common/collect/bj;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/bj;->c(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1064
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v0, 0x0

    :cond_d
    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/google/common/collect/bo;->b:Lcom/google/common/collect/bj;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 4

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/google/common/collect/bo;->a:Ljava/util/Set;

    .line 1044
    if-nez v0, :cond_e

    .line 1045
    new-instance v0, Lcom/google/common/collect/bp;

    iget-object v1, p0, Lcom/google/common/collect/bo;->b:Lcom/google/common/collect/bj;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bp;-><init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V

    iput-object v0, p0, Lcom/google/common/collect/bo;->a:Ljava/util/Set;

    .line 1047
    :cond_e
    return-object v0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1039
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bo;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1039
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bo;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
