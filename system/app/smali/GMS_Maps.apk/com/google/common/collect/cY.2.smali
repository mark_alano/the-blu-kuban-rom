.class public final Lcom/google/common/collect/cY;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/common/collect/dh;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 953
    new-instance v0, Lcom/google/common/collect/cZ;

    invoke-direct {v0}, Lcom/google/common/collect/cZ;-><init>()V

    sput-object v0, Lcom/google/common/collect/cY;->a:Lcom/google/common/collect/dh;

    return-void
.end method

.method static a(Ljava/lang/Iterable;)Lcom/google/common/collect/cW;
    .registers 1
    .parameter

    .prologue
    .line 950
    check-cast p0, Lcom/google/common/collect/cW;

    return-object p0
.end method

.method static a(Lcom/google/common/collect/cW;)Ljava/util/Iterator;
    .registers 3
    .parameter

    .prologue
    .line 878
    new-instance v0, Lcom/google/common/collect/de;

    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/de;-><init>(Lcom/google/common/collect/cW;Ljava/util/Iterator;)V

    return-object v0
.end method

.method static a(Lcom/google/common/collect/cW;Ljava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 703
    if-ne p1, p0, :cond_6

    move v0, v1

    .line 725
    :goto_5
    return v0

    .line 706
    :cond_6
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_50

    .line 707
    check-cast p1, Lcom/google/common/collect/cW;

    .line 714
    invoke-interface {p0}, Lcom/google/common/collect/cW;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/common/collect/cW;->size()I

    move-result v3

    if-ne v0, v3, :cond_28

    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2a

    :cond_28
    move v0, v2

    .line 716
    goto :goto_5

    .line 718
    :cond_2a
    invoke-interface {p1}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_32
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    .line 719
    invoke-interface {v0}, Lcom/google/common/collect/cX;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    if-eq v4, v0, :cond_32

    move v0, v2

    .line 720
    goto :goto_5

    :cond_4e
    move v0, v1

    .line 723
    goto :goto_5

    :cond_50
    move v0, v2

    .line 725
    goto :goto_5
.end method

.method static a(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 733
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 734
    const/4 v0, 0x0

    .line 744
    :goto_7
    return v0

    .line 736
    :cond_8
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_30

    .line 737
    invoke-static {p1}, Lcom/google/common/collect/cY;->a(Ljava/lang/Iterable;)Lcom/google/common/collect/cW;

    move-result-object v0

    .line 738
    invoke-interface {v0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    .line 739
    invoke-interface {v0}, Lcom/google/common/collect/cX;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;I)I

    goto :goto_18

    .line 742
    :cond_30
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/common/collect/aZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 744
    :cond_37
    const/4 v0, 0x1

    goto :goto_7
.end method

.method static b(Lcom/google/common/collect/cW;)I
    .registers 7
    .parameter

    .prologue
    .line 935
    const-wide/16 v0, 0x0

    .line 936
    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    .line 937
    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    int-to-long v4, v0

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_b

    .line 939
    :cond_20
    invoke-static {v1, v2}, Lac/a;->a(J)I

    move-result v0

    return v0
.end method

.method static b(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 752
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p1}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object p1

    .line 755
    :cond_a
    invoke-interface {p0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 763
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p1}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object p1

    .line 766
    :cond_a
    invoke-interface {p0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
