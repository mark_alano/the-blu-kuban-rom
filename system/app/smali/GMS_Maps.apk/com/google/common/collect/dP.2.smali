.class public abstract enum Lcom/google/common/collect/dP;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/dP;

.field public static final enum b:Lcom/google/common/collect/dP;

.field public static final enum c:Lcom/google/common/collect/dP;

.field public static final enum d:Lcom/google/common/collect/dP;

.field public static final enum e:Lcom/google/common/collect/dP;

.field private static final synthetic f:[Lcom/google/common/collect/dP;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lcom/google/common/collect/dQ;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/dQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    .line 63
    new-instance v0, Lcom/google/common/collect/dR;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/dR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dP;->b:Lcom/google/common/collect/dP;

    .line 87
    new-instance v0, Lcom/google/common/collect/dS;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/dS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dP;->c:Lcom/google/common/collect/dP;

    .line 113
    new-instance v0, Lcom/google/common/collect/dT;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Lcom/google/common/collect/dT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dP;->d:Lcom/google/common/collect/dP;

    .line 124
    new-instance v0, Lcom/google/common/collect/dU;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lcom/google/common/collect/dU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dP;->e:Lcom/google/common/collect/dP;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/common/collect/dP;->b:Lcom/google/common/collect/dP;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/dP;->c:Lcom/google/common/collect/dP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/common/collect/dP;->d:Lcom/google/common/collect/dP;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/common/collect/dP;->e:Lcom/google/common/collect/dP;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/common/collect/dP;->f:[Lcom/google/common/collect/dP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/dK;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/dP;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/dP;
    .registers 2
    .parameter

    .prologue
    .line 48
    const-class v0, Lcom/google/common/collect/dP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/dP;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/dP;
    .registers 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/common/collect/dP;->f:[Lcom/google/common/collect/dP;

    invoke-virtual {v0}, [Lcom/google/common/collect/dP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/dP;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
.end method
