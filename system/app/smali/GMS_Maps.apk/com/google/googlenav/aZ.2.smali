.class public Lcom/google/googlenav/aZ;
.super Law/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/F;


# static fields
.field private static final F:Lbm/i;

.field public static a:Z

.field private static final aa:Lcom/google/common/collect/ImmutableList;

.field private static c:Z

.field private static final d:Ljava/util/regex/Pattern;

.field private static e:I


# instance fields
.field private volatile A:Z

.field private B:I

.field private C:Ljava/lang/String;

.field private D:I

.field private E:B

.field private G:I

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Lcom/google/googlenav/layer/m;

.field private P:Lcom/google/googlenav/layer/a;

.field private Q:Z

.field private R:[I

.field private final S:Ljava/util/HashSet;

.field private T:Z

.field private U:[Lcom/google/googlenav/ai;

.field private V:I

.field private W:Lcom/google/googlenav/bb;

.field private X:J

.field private Y:[Ljava/lang/String;

.field private Z:I

.field private ab:[Lcom/google/googlenav/bc;

.field private ac:[Lcom/google/googlenav/be;

.field private ad:[LaW/R;

.field private ae:Z

.field private af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private ag:I

.field private ah:I

.field private b:J

.field private f:[Lcom/google/googlenav/ai;

.field private g:[I

.field private h:Lcom/google/googlenav/bf;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:LaN/H;

.field private v:LaN/B;

.field private w:I

.field private x:I

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 108
    sput-boolean v0, Lcom/google/googlenav/aZ;->a:Z

    .line 121
    sput-boolean v0, Lcom/google/googlenav/aZ;->c:Z

    .line 124
    const-string v0, "(.*) cid:\\d+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/aZ;->d:Ljava/util/regex/Pattern;

    .line 228
    const/4 v0, -0x1

    sput v0, Lcom/google/googlenav/aZ;->e:I

    .line 302
    new-instance v0, Lbm/i;

    const-string v1, "search"

    const-string v2, "s"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    .line 426
    new-instance v0, Lcom/google/common/collect/aw;

    invoke-direct {v0}, Lcom/google/common/collect/aw;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    move-result-object v0

    .line 431
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    .line 433
    invoke-virtual {v0}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/aZ;->aa:Lcom/google/common/collect/ImmutableList;

    .line 434
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 577
    invoke-direct {p0}, Law/a;-><init>()V

    .line 118
    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    .line 230
    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    .line 237
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 260
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    .line 265
    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    .line 266
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    .line 271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    .line 290
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    .line 296
    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    .line 299
    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    .line 337
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    .line 343
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    .line 349
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 359
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    .line 373
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    .line 380
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    .line 403
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    .line 415
    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 462
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    .line 467
    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    .line 471
    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    .line 578
    iput v2, p0, Lcom/google/googlenav/aZ;->G:I

    .line 579
    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 635
    invoke-direct {p0}, Law/a;-><init>()V

    .line 118
    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    .line 230
    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    .line 237
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 260
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    .line 265
    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    .line 266
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 269
    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->s:Z

    .line 271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    .line 290
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    .line 296
    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    .line 299
    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    .line 337
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    .line 343
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    .line 349
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 359
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    .line 373
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    .line 380
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    .line 403
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    .line 415
    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 462
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    .line 467
    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    .line 471
    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    .line 636
    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 638
    iput-object p2, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 639
    iput p3, p0, Lcom/google/googlenav/aZ;->G:I

    .line 641
    iget-object v0, p1, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    .line 642
    iget v0, p1, Lcom/google/googlenav/aZ;->j:I

    iput v0, p0, Lcom/google/googlenav/aZ;->j:I

    .line 643
    iget-object v0, p1, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    .line 645
    iget-object v0, p1, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    .line 646
    iget-object v0, p1, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    .line 648
    iget-object v0, p1, Lcom/google/googlenav/aZ;->u:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    .line 649
    iget v0, p1, Lcom/google/googlenav/aZ;->x:I

    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    .line 650
    iget v0, p1, Lcom/google/googlenav/aZ;->w:I

    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    .line 652
    iget v0, p1, Lcom/google/googlenav/aZ;->y:I

    iput v0, p0, Lcom/google/googlenav/aZ;->y:I

    .line 653
    iget-object v0, p1, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    .line 655
    iput-object p4, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    .line 656
    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->T:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    .line 658
    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->r:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 659
    iget-object v0, p1, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    .line 660
    iget-object v0, p1, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 661
    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->o:Z

    .line 663
    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->Q:Z

    .line 664
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/bf;LaN/u;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 590
    invoke-direct {p0}, Law/a;-><init>()V

    .line 118
    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    .line 230
    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    .line 237
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 260
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->o:Z

    .line 265
    iput v3, p0, Lcom/google/googlenav/aZ;->q:I

    .line 266
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    .line 271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    .line 290
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->A:Z

    .line 296
    iput v3, p0, Lcom/google/googlenav/aZ;->D:I

    .line 299
    iput-byte v2, p0, Lcom/google/googlenav/aZ;->E:B

    .line 337
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->L:Z

    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    .line 343
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    .line 349
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 359
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->Q:Z

    .line 373
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    .line 380
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->T:Z

    .line 403
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    .line 415
    iput v3, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 462
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->ae:Z

    .line 467
    iput v3, p0, Lcom/google/googlenav/aZ;->ag:I

    .line 471
    iput v3, p0, Lcom/google/googlenav/aZ;->ah:I

    .line 591
    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 592
    iput-object p1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 594
    iget v0, p1, Lcom/google/googlenav/bf;->r:I

    iput v0, p0, Lcom/google/googlenav/aZ;->G:I

    .line 595
    iget-object v0, p1, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    .line 596
    iget v0, p1, Lcom/google/googlenav/bf;->b:I

    iput v0, p0, Lcom/google/googlenav/aZ;->j:I

    .line 597
    iget-object v0, p1, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    .line 602
    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    if-nez v0, :cond_b7

    invoke-virtual {p2}, LaN/u;->f()LaN/H;

    move-result-object v0

    :goto_74
    iput-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    .line 603
    if-eqz p2, :cond_c0

    .line 607
    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    if-ne v0, v3, :cond_ba

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {p2, v0, v1}, LaN/u;->a(ILaN/H;)I

    move-result v0

    :goto_8a
    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    .line 610
    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    if-ne v0, v3, :cond_bd

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {p2, v0}, LaN/u;->b(LaN/H;)I

    move-result v0

    :goto_96
    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    .line 616
    :goto_98
    iget v0, p1, Lcom/google/googlenav/bf;->c:I

    iput v0, p0, Lcom/google/googlenav/aZ;->y:I

    .line 617
    iget-object v0, p1, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    .line 619
    iget-object v0, p1, Lcom/google/googlenav/bf;->i:Lcom/google/googlenav/bb;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    .line 620
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    if-nez v0, :cond_b2

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_b6

    .line 621
    :cond_b2
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    .line 623
    :cond_b6
    return-void

    .line 602
    :cond_b7
    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    goto :goto_74

    .line 607
    :cond_ba
    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    goto :goto_8a

    .line 610
    :cond_bd
    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    goto :goto_96

    .line 613
    :cond_c0
    iput v2, p0, Lcom/google/googlenav/aZ;->w:I

    .line 614
    iput v2, p0, Lcom/google/googlenav/aZ;->x:I

    goto :goto_98
.end method

.method public static a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 552
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 519
    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0}, Lcom/google/googlenav/aZ;-><init>()V

    .line 521
    iput-object p1, v0, Lcom/google/googlenav/aZ;->u:LaN/H;

    .line 522
    iput p2, v0, Lcom/google/googlenav/aZ;->w:I

    .line 523
    iput p3, v0, Lcom/google/googlenav/aZ;->x:I

    .line 524
    array-length v1, p0

    iput v1, v0, Lcom/google/googlenav/aZ;->q:I

    .line 525
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/googlenav/aZ;->s:Z

    .line 527
    iput-object p0, v0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 528
    invoke-direct {v0}, Lcom/google/googlenav/aZ;->ba()V

    .line 530
    if-eqz p4, :cond_2f

    .line 531
    iput-object p4, v0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    .line 532
    iget-object v1, p4, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    .line 533
    iget v1, p4, Lcom/google/googlenav/bf;->b:I

    iput v1, v0, Lcom/google/googlenav/aZ;->j:I

    .line 534
    iget-object v1, p4, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    .line 535
    iget v1, p4, Lcom/google/googlenav/bf;->c:I

    iput v1, v0, Lcom/google/googlenav/aZ;->y:I

    .line 536
    iget-object v1, p4, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    .line 541
    :goto_2e
    return-object v0

    .line 538
    :cond_2f
    const-string v1, ""

    iput-object v1, v0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    .line 539
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/googlenav/aZ;->j:I

    goto :goto_2e
.end method

.method public static a([Lcom/google/googlenav/ai;Lcom/google/googlenav/bf;LaN/u;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 569
    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    .line 570
    iput-object p0, v0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 571
    iput-object p3, v0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 572
    invoke-direct {v0}, Lcom/google/googlenav/aZ;->ba()V

    .line 573
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 727
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 786
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 787
    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 788
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 790
    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, " "

    :goto_1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 791
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    :cond_22
    :goto_22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 790
    :cond_27
    const-string v0, " loc:"

    goto :goto_1c

    .line 793
    :cond_2a
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 794
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_22
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2924
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2925
    :cond_8
    const/4 v0, 0x0

    .line 2944
    :goto_9
    return-object v0

    .line 2927
    :cond_a
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2928
    const/4 v0, 0x1

    .line 2930
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2931
    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2933
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    .line 2934
    if-nez v1, :cond_30

    .line 2935
    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2938
    :cond_30
    const-string v1, "ft"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2939
    const/16 v1, 0x3a

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2940
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 2941
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1d

    .line 2944
    :cond_44
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9
.end method

.method public static a(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 740
    invoke-static {p0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741
    const-string v0, ""

    .line 763
    :cond_8
    :goto_8
    return-object v0

    .line 744
    :cond_9
    if-nez p0, :cond_3f

    const-string p0, ""

    :cond_d
    :goto_d
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 747
    sget-object v1, Lcom/google/googlenav/aZ;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 748
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 749
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 753
    :cond_26
    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 754
    const/4 v2, -0x1

    if-eq v1, v2, :cond_38

    .line 755
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 758
    :cond_38
    if-eqz p1, :cond_8

    .line 760
    invoke-static {v0}, Lcom/google/googlenav/aZ;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 744
    :cond_3f
    invoke-static {p0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    goto :goto_d
.end method

.method private aT()Z
    .registers 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private aU()Z
    .registers 2

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private aV()V
    .registers 8

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/16 v3, 0x10

    .line 1556
    invoke-static {}, Lcom/google/googlenav/android/F;->g()Landroid/util/Pair;

    move-result-object v2

    .line 1560
    if-nez v2, :cond_b

    .line 1581
    :goto_a
    return-void

    .line 1564
    :cond_b
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1565
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_99

    .line 1566
    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1568
    :goto_1a
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1569
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_28

    .line 1570
    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1572
    :cond_28
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "s=l"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nt="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "st="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "e="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/google/googlenav/aZ;->B:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v3}, Lbm/i;->g()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1579
    const-string v1, "stat"

    invoke-static {v6, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_99
    move-object v1, v0

    goto :goto_1a
.end method

.method private aW()[I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1740
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    new-array v3, v1, [I

    move v1, v0

    .line 1743
    :goto_7
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v2, v2

    if-ge v0, v2, :cond_2c

    .line 1744
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-nez v2, :cond_20

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 1745
    :cond_20
    const/4 v2, -0x1

    aput v2, v3, v0

    .line 1743
    :goto_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1747
    :cond_26
    add-int/lit8 v2, v1, 0x1

    aput v1, v3, v0

    move v1, v2

    goto :goto_23

    .line 1750
    :cond_2c
    return-object v3
.end method

.method private aX()V
    .registers 6

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1755
    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_25

    .line 1756
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    .line 1757
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    if-eqz v2, :cond_22

    .line 1758
    iget-object v2, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1755
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1761
    :cond_25
    return-void
.end method

.method private aY()V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 1932
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v5, v0

    .line 1933
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    move v4, v3

    .line 1934
    :goto_9
    if-ge v4, v5, :cond_43

    move v2, v3

    move v0, v4

    .line 1937
    :goto_d
    if-gt v2, v4, :cond_40

    .line 1938
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iget-object v6, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v6, v6, v2

    aget-object v1, v1, v6

    .line 1939
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    .line 1940
    iget-object v6, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v6

    .line 1944
    if-eq v2, v4, :cond_33

    if-eqz v1, :cond_3c

    if-eqz v6, :cond_3c

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v6}, LaN/B;->c()I

    move-result v6

    if-ge v1, v6, :cond_3c

    .line 1947
    :cond_33
    iget-object v1, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v1, v1, v2

    .line 1948
    iget-object v6, p0, Lcom/google/googlenav/aZ;->g:[I

    aput v0, v6, v2

    move v0, v1

    .line 1937
    :cond_3c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_d

    .line 1934
    :cond_40
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 1953
    :cond_43
    return-void
.end method

.method private aZ()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7

    .prologue
    .line 1991
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ah()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1992
    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1994
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ag;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1996
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1999
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    iget-object v5, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v5}, LaN/H;->b()LaN/Y;

    move-result-object v5

    invoke-virtual {v5}, LaN/Y;->a()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2002
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/googlenav/aZ;->j:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2003
    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 2004
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2006
    :cond_43
    iget-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    if-eqz v0, :cond_4d

    .line 2007
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2010
    :cond_4d
    return-object v1
.end method

.method public static b(Ljava/io/DataInput;)Lcom/google/googlenav/aZ;
    .registers 7
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 2158
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2160
    invoke-static {v0}, Lcom/google/googlenav/aZ;->m(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2163
    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-direct {v1}, Lcom/google/googlenav/aZ;-><init>()V

    .line 2164
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 2166
    const/4 v2, 0x3

    invoke-static {v0, v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    iput v2, v1, Lcom/google/googlenav/aZ;->j:I

    .line 2168
    iput-boolean v3, v1, Lcom/google/googlenav/aZ;->o:Z

    .line 2169
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 2170
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    .line 2172
    :cond_2d
    const/4 v2, 0x5

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 2174
    if-eqz v2, :cond_36

    .line 2175
    iput-object v2, v1, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    .line 2178
    :cond_36
    iput-boolean v3, v1, Lcom/google/googlenav/aZ;->A:Z

    .line 2181
    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2183
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 2184
    new-instance v3, LaN/H;

    invoke-static {v2}, LaN/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    const/4 v4, 0x0

    invoke-direct {v3, v2, v0, v4}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    iput-object v3, v1, Lcom/google/googlenav/aZ;->u:LaN/H;

    .line 2187
    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2960
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2961
    :cond_8
    const/4 v0, 0x0

    .line 2980
    :goto_9
    return-object v0

    .line 2963
    :cond_a
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2964
    const/4 v0, 0x1

    .line 2966
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2967
    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2969
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/be;

    .line 2970
    if-nez v1, :cond_30

    .line 2971
    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2974
    :cond_30
    const-string v1, "ot"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2975
    const/16 v1, 0x3a

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2976
    invoke-virtual {v0}, Lcom/google/googlenav/be;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 2977
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1d

    .line 2980
    :cond_44
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9
.end method

.method private ba()V
    .registers 2

    .prologue
    .line 2699
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 2700
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2701
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aY()V

    .line 2702
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aW()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    .line 2703
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aX()V

    .line 2705
    :cond_15
    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1417
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/bK;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1420
    iget-object v3, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v3, v3, Lcom/google/googlenav/bf;->t:Z

    if-eqz v3, :cond_13

    .line 1421
    invoke-virtual {v2, v0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 1425
    :cond_13
    iget-object v3, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v3, v3, Lcom/google/googlenav/bf;->C:I

    if-eqz v3, :cond_34

    .line 1426
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->n:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1428
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v4, v4, Lcom/google/googlenav/bf;->C:I

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1431
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1436
    :goto_2c
    if-nez v1, :cond_33

    .line 1437
    const/16 v0, 0x25

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1439
    :cond_33
    return-void

    :cond_34
    move v1, v0

    goto :goto_2c
.end method

.method public static c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 953
    if-eqz p0, :cond_10

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 954
    const/4 v0, 0x1

    .line 956
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x3

    const/4 v4, 0x1

    .line 1591
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1592
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    .line 1596
    :cond_11
    const/16 v0, 0xe

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    .line 1598
    const/16 v0, 0xf

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    .line 1601
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1602
    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1603
    invoke-static {v0}, LaN/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    .line 1604
    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->w:I

    .line 1605
    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->x:I

    .line 1607
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1609
    iput-boolean v2, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 1610
    if-eqz v0, :cond_71

    .line 1611
    iput-boolean v4, p0, Lcom/google/googlenav/aZ;->r:Z

    .line 1612
    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    .line 1614
    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    .line 1616
    iget-object v1, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_69

    .line 1618
    iget-object v1, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    .line 1620
    :cond_69
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    .line 1624
    :cond_71
    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    .line 1625
    const/16 v1, 0xb

    invoke-static {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/aZ;->G:I

    .line 1627
    iget v1, p0, Lcom/google/googlenav/aZ;->G:I

    if-ge v1, v0, :cond_a3

    .line 1630
    const-string v1, "offset"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    :cond_a3
    const/16 v0, 0xd

    invoke-static {p1, v0, v5}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->q:I

    .line 1635
    const/16 v0, 0x18

    invoke-static {p1, v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    .line 1638
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    .line 1640
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1650
    sget v0, Lcom/google/googlenav/aZ;->e:I

    if-ne v0, v5, :cond_da

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_da

    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    if-nez v0, :cond_da

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/aZ;->q:I

    if-ge v0, v1, :cond_da

    .line 1652
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    sput v0, Lcom/google/googlenav/aZ;->e:I

    .line 1658
    :cond_da
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1661
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1664
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1667
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1670
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1671
    return-void
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 696
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_b

    .line 715
    :cond_a
    :goto_a
    return-object p0

    .line 711
    :cond_b
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 712
    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    const-string v1, "("

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_24

    const-string v1, ")"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 713
    :cond_24
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_a
.end method

.method private e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 9
    .parameter

    .prologue
    const/16 v0, 0x8

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1678
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 1679
    new-array v0, v2, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    move v0, v1

    .line 1682
    :goto_d
    if-ge v0, v2, :cond_78

    .line 1684
    const/16 v3, 0x8

    :try_start_11
    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1688
    const/16 v4, 0x9f

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-nez v4, :cond_2c

    iget-object v4, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2c

    .line 1690
    const/16 v4, 0x9f

    iget-object v5, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1692
    :cond_2c
    invoke-static {v3}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v3

    .line 1693
    sget-object v4, Lcom/google/googlenav/av;->a:Lcom/google/googlenav/av;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/av;)V

    .line 1695
    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v4, v4, Lcom/google/googlenav/bf;->A:Z

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->e(Z)V

    .line 1696
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ai()Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 1697
    iget-object v4, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->h(Ljava/lang/String;)V

    .line 1698
    iget-object v4, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->i(Ljava/lang/String;)V

    .line 1704
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aT()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5b

    .line 1705
    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->j(Ljava/lang/String;)V

    .line 1707
    :cond_5b
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 1708
    iget-object v4, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->k(Ljava/lang/String;)V

    .line 1712
    :cond_6a
    iget-object v4, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aput-object v3, v4, v0
    :try_end_6e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_6e} :catch_71

    .line 1682
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 1715
    :catch_71
    move-exception v0

    .line 1716
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    .line 1719
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 1728
    :cond_78
    sget-object v0, Laz/a;->a:Laz/a;

    invoke-virtual {v0}, Laz/a;->d()Z

    move-result v0

    if-eqz v0, :cond_9c

    if-le v2, v6, :cond_9c

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 1731
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v6

    .line 1732
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v3, v3, v1

    aput-object v3, v2, v6

    .line 1733
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aput-object v0, v2, v1

    .line 1736
    :cond_9c
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    .line 1737
    return-void
.end method

.method private f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x10

    const/4 v1, 0x5

    .line 1772
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1773
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->n:Z

    .line 1777
    :cond_f
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    .line 1781
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->l:Ljava/lang/String;

    .line 1784
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1787
    new-array v0, v1, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    .line 1788
    const/4 v0, 0x0

    :goto_26
    if-ge v0, v1, :cond_37

    .line 1789
    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1791
    invoke-static {v2}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v2

    .line 1792
    iget-object v3, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    aput-object v2, v3, v0

    .line 1788
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 1795
    :cond_37
    return-void
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x11

    .line 1799
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1802
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    .line 1803
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_18

    .line 1804
    iget-object v2, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1803
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1806
    :cond_18
    return-void
.end method

.method private h(I)I
    .registers 3
    .parameter

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/googlenav/aZ;->g:[I

    aget v0, v0, p1

    return v0
.end method

.method private h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 14
    .parameter

    .prologue
    .line 1810
    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 1813
    new-array v0, v0, [Lcom/google/googlenav/bc;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    .line 1814
    const/4 v0, 0x0

    :goto_b
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v1, v1

    if-ge v0, v1, :cond_59

    .line 1815
    const/16 v1, 0x12

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1816
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    long-to-int v3, v3

    .line 1817
    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    .line 1818
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    .line 1819
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6, v5}, Ljava/util/Vector;-><init>(I)V

    .line 1820
    const/4 v1, 0x0

    :goto_2c
    if-ge v1, v5, :cond_4d

    .line 1821
    const/4 v7, 0x3

    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 1822
    new-instance v8, Lcom/google/googlenav/bd;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v9

    const/4 v10, 0x2

    invoke-static {v7, v10}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-static {v7, v11}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v9, v10, v7}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1820
    add-int/lit8 v1, v1, 0x1

    goto :goto_2c

    .line 1827
    :cond_4d
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    new-instance v2, Lcom/google/googlenav/bc;

    invoke-direct {v2, v3, v4, v6}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    aput-object v2, v1, v0

    .line 1814
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1829
    :cond_59
    return-void
.end method

.method private i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    const/16 v7, 0x15

    .line 1832
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1835
    new-array v0, v1, [Lcom/google/googlenav/be;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    .line 1837
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_2d

    .line 1838
    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1839
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    long-to-int v3, v3

    .line 1840
    const/4 v4, 0x2

    invoke-static {v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    .line 1841
    const/4 v5, 0x4

    invoke-static {v2, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 1843
    iget-object v5, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    new-instance v6, Lcom/google/googlenav/be;

    invoke-direct {v6, v3, v4, v2}, Lcom/google/googlenav/be;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v0

    .line 1837
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1845
    :cond_2d
    return-void
.end method

.method private j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0x14

    .line 1848
    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1852
    new-array v0, v1, [LaW/R;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->ad:[LaW/R;

    .line 1855
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_1d

    .line 1856
    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1857
    iget-object v3, p0, Lcom/google/googlenav/aZ;->ad:[LaW/R;

    const/4 v4, 0x0

    invoke-static {v2, v4}, LaW/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaW/S;)LaW/R;

    move-result-object v2

    aput-object v2, v3, v0

    .line 1855
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1859
    :cond_1d
    return-void
.end method

.method private k(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x2

    .line 1866
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1867
    new-instance v0, Lcom/google/googlenav/layer/m;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    .line 1873
    :goto_13
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1874
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1875
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 1876
    new-instance v2, Lcom/google/googlenav/layer/a;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/layer/m;

    invoke-direct {v4, v1}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {v2, v3, v4, v0}, Lcom/google/googlenav/layer/a;-><init>(Ljava/lang/String;Lcom/google/googlenav/layer/m;I)V

    iput-object v2, p0, Lcom/google/googlenav/aZ;->P:Lcom/google/googlenav/layer/a;

    .line 1879
    :cond_38
    return-void

    .line 1869
    :cond_39
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    goto :goto_13
.end method

.method private l(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    .line 2143
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_16

    .line 2145
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 2146
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2143
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2148
    :cond_16
    return-void
.end method

.method private static m(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 2196
    const/4 v0, 0x0

    .line 2197
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v1

    if-nez v1, :cond_31

    .line 2199
    const-string v0, "Missing SEARCH_RESPONSE_WITH_PAYLOAD"

    .line 2211
    :cond_a
    :goto_a
    if-eqz v0, :cond_4c

    .line 2212
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 2213
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 2214
    invoke-virtual {v1, v0}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 2215
    const-string v2, " proto:\n"

    invoke-virtual {v1, v2}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 2216
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toTextFormat(Ljava/io/Writer;)V

    .line 2218
    const-string v2, "Search.assertCorrectProtoBuf"

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220
    :cond_2b
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2201
    :cond_31
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 2203
    if-nez v1, :cond_3a

    .line 2204
    const-string v0, "Null SEARCH_RESPONSE_WITH_PAYLOAD"

    goto :goto_a

    .line 2205
    :cond_3a
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v2

    if-nez v2, :cond_43

    .line 2206
    const-string v0, "Missing RESPONSE"

    goto :goto_a

    .line 2207
    :cond_43
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_a

    .line 2208
    const-string v0, "null RESPONSE"

    goto :goto_a

    .line 2222
    :cond_4c
    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .registers 2

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    goto :goto_a
.end method

.method public B()Ljava/lang/String;
    .registers 2

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .registers 5

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 969
    const/16 v0, 0x4f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 972
    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    const/16 v0, 0x4f2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    :cond_30
    iget-object v0, p0, Lcom/google/googlenav/aZ;->k:Ljava/lang/String;

    goto :goto_e
.end method

.method public D()Z
    .registers 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 980
    const/4 v0, 0x0

    .line 982
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->y:Z

    goto :goto_9
.end method

.method public E()Ljava/lang/String;
    .registers 2

    .prologue
    .line 987
    iget-object v0, p0, Lcom/google/googlenav/aZ;->l:Ljava/lang/String;

    return-object v0
.end method

.method public F()Z
    .registers 3

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public G()Ljava/lang/String;
    .registers 2

    .prologue
    .line 995
    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    return-object v0
.end method

.method public H()Z
    .registers 2

    .prologue
    .line 999
    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public I()[Lcom/google/googlenav/ai;
    .registers 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/googlenav/aZ;->U:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public J()I
    .registers 2

    .prologue
    .line 1027
    iget v0, p0, Lcom/google/googlenav/aZ;->V:I

    return v0
.end method

.method public K()Z
    .registers 2

    .prologue
    .line 1032
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->n:Z

    return v0
.end method

.method public L()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    return-object v0
.end method

.method public M()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    return-object v0
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 1048
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->o:Z

    return v0
.end method

.method public O()LaN/B;
    .registers 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/aZ;->v:LaN/B;

    goto :goto_a
.end method

.method public P()LaN/M;
    .registers 2

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->l:LaN/M;

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    return-object v0
.end method

.method public S()I
    .registers 2

    .prologue
    .line 1092
    iget v0, p0, Lcom/google/googlenav/aZ;->w:I

    return v0
.end method

.method public T()I
    .registers 2

    .prologue
    .line 1096
    iget v0, p0, Lcom/google/googlenav/aZ;->x:I

    return v0
.end method

.method public U()LaN/H;
    .registers 2

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    return-object v0
.end method

.method public V()LaN/H;
    .registers 2

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->E:LaN/H;

    return-object v0
.end method

.method public W()I
    .registers 2

    .prologue
    .line 1109
    iget v0, p0, Lcom/google/googlenav/aZ;->y:I

    return v0
.end method

.method public X()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/googlenav/aZ;->z:Ljava/lang/String;

    return-object v0
.end method

.method public Y()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 1123
    iget v1, p0, Lcom/google/googlenav/aZ;->Z:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9

    .line 1124
    iget v0, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 1133
    :goto_8
    return v0

    .line 1127
    :cond_9
    iput v0, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 1128
    :goto_b
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_23

    .line 1129
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->M()Lcom/google/googlenav/aq;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 1130
    iget v1, p0, Lcom/google/googlenav/aZ;->Z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/googlenav/aZ;->Z:I

    .line 1128
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1133
    :cond_23
    iget v0, p0, Lcom/google/googlenav/aZ;->Z:I

    goto :goto_8
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 1150
    invoke-super {p0}, Law/a;->Z()V

    .line 1151
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_c

    .line 1152
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-interface {v0, p0}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    .line 1154
    :cond_c
    return-void
.end method

.method public a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 688
    new-instance v0, Lcom/google/googlenav/aZ;

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 676
    new-instance v0, Lcom/google/googlenav/aZ;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)V

    return-object v0
.end method

.method public a(B)V
    .registers 2
    .parameter

    .prologue
    .line 2520
    iput-byte p1, p0, Lcom/google/googlenav/aZ;->E:B

    .line 2521
    return-void
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 2469
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->ag:I

    .line 2472
    iget v0, p0, Lcom/google/googlenav/aZ;->D:I

    if-eq p1, v0, :cond_1a

    .line 2473
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_22

    .line 2474
    const/4 v0, 0x5

    const-string v1, "1"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2484
    :cond_1a
    :goto_1a
    iput p1, p0, Lcom/google/googlenav/aZ;->D:I

    .line 2486
    if-gez p1, :cond_32

    .line 2487
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    .line 2493
    :cond_21
    :goto_21
    return-void

    .line 2477
    :cond_22
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    if-ne v0, v3, :cond_1a

    .line 2478
    const/16 v0, 0x12

    const-string v1, "1"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1a

    .line 2489
    :cond_32
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    if-nez v0, :cond_21

    .line 2490
    iput-byte v3, p0, Lcom/google/googlenav/aZ;->E:B

    goto :goto_21
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 2463
    iput-wide p1, p0, Lcom/google/googlenav/aZ;->X:J

    .line 2464
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2631
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    add-int/2addr v0, v2

    .line 2633
    new-array v2, v0, [Lcom/google/googlenav/ai;

    move v0, v1

    .line 2634
    :goto_d
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_1c

    .line 2635
    iget-object v3, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    .line 2634
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_1c
    move v0, v1

    .line 2638
    :goto_1d
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_43

    .line 2639
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v3

    aget-object v3, v3, v0

    .line 2644
    invoke-virtual {v3, v1}, Lcom/google/googlenav/ai;->a(B)V

    .line 2645
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ae()V

    .line 2646
    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v4, v4, Lcom/google/googlenav/bf;->A:Z

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->e(Z)V

    .line 2647
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    add-int/2addr v4, v0

    aput-object v3, v2, v4

    .line 2648
    invoke-virtual {p0, v3}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/ai;)V

    .line 2638
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 2651
    :cond_43
    iput-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 2653
    iget v0, p1, Lcom/google/googlenav/aZ;->q:I

    iput v0, p0, Lcom/google/googlenav/aZ;->q:I

    .line 2654
    iget-boolean v0, p1, Lcom/google/googlenav/aZ;->s:Z

    iput-boolean v0, p0, Lcom/google/googlenav/aZ;->s:Z

    .line 2655
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    .line 2656
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 5
    .parameter

    .prologue
    .line 1764
    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1765
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 13
    .parameter

    .prologue
    const/16 v10, 0x10

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1165
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1167
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1168
    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    invoke-virtual {v5, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1169
    const/16 v0, 0x3e

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->J:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1170
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1171
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1173
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->n:Z

    if-eqz v0, :cond_4f

    .line 1174
    iget-object v0, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    iget-object v6, p0, Lcom/google/googlenav/aZ;->u:LaN/H;

    invoke-virtual {v6}, LaN/H;->b()LaN/Y;

    move-result-object v6

    invoke-virtual {v6}, LaN/Y;->a()I

    move-result v6

    invoke-static {v0, v1, v4, v6}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1177
    invoke-virtual {v5, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1180
    :cond_4f
    sget-boolean v0, Lcom/google/googlenav/aZ;->a:Z

    if-eqz v0, :cond_6b

    .line 1182
    const/4 v0, 0x7

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1183
    const/16 v0, 0x12

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1184
    const/16 v0, 0x2e

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1185
    const/16 v0, 0x33

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1186
    const/16 v0, 0x34

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1188
    :cond_6b
    const/16 v0, 0x8

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1189
    const/16 v0, 0x11

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1190
    const/16 v0, 0x24

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1195
    const/16 v1, 0x3d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1dc

    move v0, v2

    :goto_87
    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1200
    const/4 v0, 0x6

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1203
    const/16 v0, 0x13

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1206
    const/16 v0, 0x14

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1209
    const/16 v0, 0x18

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1213
    const/16 v0, 0xe

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1217
    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_f0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->c()Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 1218
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/D;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1221
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    .line 1222
    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->I()I

    move-result v4

    add-int/lit8 v4, v4, -0x14

    invoke-virtual {v0, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1224
    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/bi;->a(Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1226
    const/16 v1, 0x12

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1227
    invoke-virtual {v0, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 1232
    invoke-virtual {v0, v10, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 1238
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    long-to-int v1, v6

    div-int/lit16 v1, v1, 0x400

    div-int/lit8 v1, v1, 0x3

    .line 1239
    const/16 v4, 0xc8

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1242
    const/16 v1, 0xc

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1245
    :cond_f0
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    invoke-virtual {v5, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1247
    const/16 v0, 0x1a

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1249
    const/16 v0, 0x12

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1251
    const/16 v0, 0x16

    const/16 v1, 0x100

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1252
    const/16 v0, 0x19

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1259
    const/16 v0, 0x20

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1262
    const/16 v0, 0x21

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1264
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aN()Z

    move-result v0

    if-eqz v0, :cond_122

    .line 1265
    const/16 v0, 0x32

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1269
    :cond_122
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    if-eqz v0, :cond_152

    .line 1270
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1272
    invoke-virtual {v0, v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1273
    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 1274
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v6

    invoke-virtual {v6}, LaN/M;->j()[LaN/B;

    move-result-object v6

    invoke-static {v6}, LaN/M;->a([LaN/B;)[B

    move-result-object v6

    invoke-virtual {v1, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1277
    const/4 v4, 0x4

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1278
    invoke-virtual {v0, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1279
    const/16 v1, 0x1b

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1283
    :cond_152
    const/16 v0, 0x17

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1287
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_174

    .line 1288
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1290
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1292
    const/16 v1, 0x1c

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1296
    :cond_174
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/googlenav/aZ;->T:Z

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1299
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->av()Z

    move-result v0

    if-eqz v0, :cond_199

    .line 1300
    const/16 v0, 0x1f

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1301
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eW;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1302
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aw()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1303
    const/16 v1, 0x23

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1308
    :cond_199
    const/16 v0, 0x2f

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1310
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->z:Z

    if-eqz v0, :cond_1a9

    .line 1311
    const/16 v0, 0x31

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1314
    :cond_1a9
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1b8

    .line 1315
    const/16 v0, 0x37

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1319
    :cond_1b8
    invoke-direct {p0, v5}, Lcom/google/googlenav/aZ;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1322
    const/16 v0, 0x26

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1325
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1df

    move v0, v3

    .line 1326
    :goto_1c7
    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v1, v1

    if-ge v0, v1, :cond_1df

    .line 1327
    const/16 v1, 0x27

    iget-object v4, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v4, v4, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v4, v4, v0

    invoke-virtual {v5, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1326
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c7

    :cond_1dc
    move v0, v3

    .line 1195
    goto/16 :goto_87

    .line 1332
    :cond_1df
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->b()I

    move-result v0

    invoke-static {v0, v5, v2}, Lcom/google/googlenav/ah;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 1335
    const/16 v0, 0x2b

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1340
    const/16 v0, 0x35

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1344
    const/16 v0, 0x36

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1347
    sget-boolean v0, Lcom/google/googlenav/aZ;->c:Z

    if-eqz v0, :cond_297

    .line 1348
    sget-object v0, Lcom/google/googlenav/aZ;->aa:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1ff
    :goto_1ff
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_222

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1349
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1ff

    .line 1350
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1352
    invoke-virtual {v4, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1353
    const/16 v0, 0x2c

    invoke-virtual {v5, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1ff

    .line 1359
    :cond_222
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    .line 1360
    if-eqz v0, :cond_297

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_297

    .line 1361
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_236
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_297

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    .line 1362
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1364
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    invoke-virtual {v7, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1365
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_291

    move v4, v3

    .line 1366
    :goto_257
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_291

    .line 1367
    new-instance v8, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fC;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1369
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bd;

    invoke-virtual {v1}, Lcom/google/googlenav/bd;->a()I

    move-result v1

    invoke-virtual {v8, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1370
    invoke-virtual {v0}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/bd;

    invoke-virtual {v1}, Lcom/google/googlenav/bd;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1372
    invoke-virtual {v7, v9, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1366
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_257

    .line 1375
    :cond_291
    const/16 v0, 0x2d

    invoke-virtual {v5, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_236

    .line 1381
    :cond_297
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v0

    .line 1383
    if-eqz v0, :cond_2c1

    .line 1384
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2a1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/be;

    .line 1385
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/fC;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1389
    invoke-virtual {v0}, Lcom/google/googlenav/be;->a()I

    move-result v0

    invoke-virtual {v4, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1390
    const/16 v0, 0x39

    invoke-virtual {v5, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_2a1

    .line 1396
    :cond_2c1
    const/16 v0, 0x2a

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->D()Z

    move-result v1

    if-eqz v1, :cond_2d0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->af()Z

    move-result v1

    if-nez v1, :cond_2d0

    move v3, v2

    :cond_2d0
    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1399
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->D:LaW/R;

    if-eqz v0, :cond_2f0

    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->D:LaW/R;

    invoke-virtual {v0}, LaW/R;->f()Z

    move-result v0

    if-eqz v0, :cond_2f0

    .line 1400
    const/16 v0, 0x3a

    iget-object v1, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v1, v1, Lcom/google/googlenav/bf;->D:LaW/R;

    invoke-virtual {v1}, LaW/R;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1404
    :cond_2f0
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->G:Z

    if-eqz v0, :cond_2fb

    .line 1405
    const/16 v0, 0x3c

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1408
    :cond_2fb
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v5, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 1409
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1044
    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->o:Z

    .line 1045
    return-void
.end method

.method public a([Lcom/google/googlenav/ai;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2659
    if-nez p1, :cond_4

    .line 2679
    :goto_3
    return-void

    .line 2662
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 2663
    :goto_a
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_1a

    .line 2664
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2663
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_1a
    move v0, v1

    .line 2668
    :goto_1b
    array-length v2, p1

    if-ge v1, v2, :cond_33

    .line 2669
    aget-object v4, p1, v1

    .line 2670
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aN()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 2671
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v2

    .line 2668
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1b

    .line 2673
    :cond_2f
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2c

    .line 2677
    :cond_33
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ai;

    iput-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 2678
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    goto :goto_3
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x1b0

    const/16 v5, 0x1a

    const/16 v4, 0x17

    const/4 v1, 0x1

    .line 1464
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/aZ;->a(J)V

    .line 1466
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1469
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->B:I

    .line 1470
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    .line 1471
    const/16 v0, 0x19

    const/4 v3, -0x1

    invoke-static {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/aZ;->ah:I

    .line 1473
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    packed-switch v0, :pswitch_data_d8

    .line 1514
    :cond_35
    :goto_35
    :pswitch_35
    iput-boolean v1, p0, Lcom/google/googlenav/aZ;->A:Z

    .line 1517
    sget-object v0, Lcom/google/googlenav/aZ;->F:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 1519
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aV()V

    .line 1523
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_4c

    .line 1524
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->w()Z

    move-result v3

    invoke-interface {v0, p0, v3}, Lcom/google/googlenav/bb;->a(Lcom/google/googlenav/aZ;Z)V

    .line 1532
    :cond_4c
    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 1533
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-nez v0, :cond_c2

    move v0, v1

    :goto_5d
    invoke-virtual {v3, v0}, Lcom/google/googlenav/K;->d(Z)V

    .line 1543
    :goto_60
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 1544
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 1546
    if-gez v0, :cond_cc

    int-to-long v2, v0

    :goto_6d
    iput-wide v2, p0, Lcom/google/googlenav/aZ;->b:J

    .line 1551
    :goto_6f
    return v1

    .line 1476
    :pswitch_70
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    .line 1480
    :pswitch_74
    invoke-direct {p0, v2}, Lcom/google/googlenav/aZ;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1481
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->k(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1482
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v0

    if-nez v0, :cond_8b

    .line 1483
    iput v1, p0, Lcom/google/googlenav/aZ;->B:I

    .line 1484
    const/16 v0, 0x5ef

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto :goto_35

    .line 1485
    :cond_8b
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_35

    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-nez v0, :cond_35

    .line 1487
    iput v1, p0, Lcom/google/googlenav/aZ;->B:I

    .line 1488
    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto :goto_35

    .line 1495
    :pswitch_a4
    invoke-direct {p0, v2}, Lcom/google/googlenav/aZ;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1496
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 1498
    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    goto/16 :goto_35

    .line 1508
    :pswitch_b9
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aZ;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_35

    .line 1533
    :cond_c2
    const/4 v0, 0x0

    goto :goto_5d

    .line 1537
    :cond_c4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/K;->d(Z)V

    goto :goto_60

    .line 1546
    :cond_cc
    int-to-long v2, v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    goto :goto_6d

    .line 1549
    :cond_d1
    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/googlenav/aZ;->b:J

    goto :goto_6f

    .line 1473
    :pswitch_data_d8
    .packed-switch 0x0
        :pswitch_74
        :pswitch_a4
        :pswitch_35
        :pswitch_70
        :pswitch_35
        :pswitch_35
        :pswitch_35
        :pswitch_b9
    .end packed-switch
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 3
    .parameter

    .prologue
    .line 1444
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1446
    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public aA()Z
    .registers 2

    .prologue
    .line 2543
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->T:Z

    return v0
.end method

.method public aB()LaN/B;
    .registers 2

    .prologue
    .line 2547
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->p:LaN/B;

    return-object v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 2551
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->F:Z

    return v0
.end method

.method public aD()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 2573
    move v1, v0

    .line 2574
    :goto_2
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_17

    .line 2575
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bb()I

    move-result v2

    if-lez v2, :cond_17

    .line 2576
    add-int/lit8 v1, v1, 0x1

    .line 2574
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2581
    :cond_17
    return v1
.end method

.method public aE()[Lcom/google/googlenav/ai;
    .registers 2

    .prologue
    .line 2622
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public aF()V
    .registers 5

    .prologue
    const/16 v3, 0xa

    .line 2683
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-le v0, v3, :cond_1a

    .line 2684
    new-array v1, v3, [Lcom/google/googlenav/ai;

    .line 2685
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v3, :cond_15

    .line 2686
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 2685
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2689
    :cond_15
    iput-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    .line 2690
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->ba()V

    .line 2692
    :cond_1a
    return-void
.end method

.method public aG()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 2708
    iget-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    return-object v0
.end method

.method public aH()[Lcom/google/googlenav/bc;
    .registers 2

    .prologue
    .line 2712
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    return-object v0
.end method

.method public aI()[Lcom/google/googlenav/be;
    .registers 2

    .prologue
    .line 2721
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ac:[Lcom/google/googlenav/be;

    return-object v0
.end method

.method public aJ()Ljava/util/Map;
    .registers 2

    .prologue
    .line 2753
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    return-object v0
.end method

.method public aK()Ljava/util/Set;
    .registers 2

    .prologue
    .line 2765
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    return-object v0
.end method

.method public aL()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2769
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->v:Ljava/lang/String;

    return-object v0
.end method

.method public aM()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/google/googlenav/aZ;->af:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public aN()Z
    .registers 2

    .prologue
    .line 2888
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->ae:Z

    return v0
.end method

.method public aO()Z
    .registers 2

    .prologue
    .line 2909
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->I:Z

    return v0
.end method

.method public aP()J
    .registers 3

    .prologue
    .line 2988
    iget-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    return-wide v0
.end method

.method public aQ()Z
    .registers 5

    .prologue
    .line 2998
    iget-wide v0, p0, Lcom/google/googlenav/aZ;->b:J

    invoke-static {}, Lbf/bk;->bV()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 1119
    const/4 v0, 0x1

    return v0
.end method

.method public aa()Lcom/google/googlenav/layer/a;
    .registers 2

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/google/googlenav/aZ;->P:Lcom/google/googlenav/layer/a;

    return-object v0
.end method

.method public ab()Z
    .registers 2

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public ac()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 1890
    iget-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method public ad()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1900
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ne v2, v0, :cond_15

    invoke-virtual {p0, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_15

    :goto_14
    return v0

    :cond_15
    move v0, v1

    goto :goto_14
.end method

.method public ae()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1910
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v1, :cond_16

    invoke-virtual {p0, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bC()Z

    move-result v0

    if-eqz v0, :cond_16

    move v0, v1

    :goto_15
    return v0

    :cond_16
    move v0, v2

    goto :goto_15
.end method

.method public af()Z
    .registers 2

    .prologue
    .line 1921
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->Q:Z

    if-eqz v0, :cond_4

    .line 1924
    :cond_4
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->Q:Z

    return v0
.end method

.method public ag()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1963
    const/4 v0, 0x0

    .line 1971
    :goto_9
    return-object v0

    .line 1966
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 1969
    iget-object v0, p0, Lcom/google/googlenav/aZ;->K:Ljava/lang/String;

    goto :goto_9

    .line 1971
    :cond_1d
    iget-object v0, p0, Lcom/google/googlenav/aZ;->J:Ljava/lang/String;

    goto :goto_9
.end method

.method public ah()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 13

    .prologue
    const/4 v11, 0x3

    const/4 v6, -0x1

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2022
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2024
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    invoke-virtual {v5, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2025
    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 2026
    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-virtual {v5, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2028
    :cond_1e
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    if-eq v0, v6, :cond_29

    .line 2029
    const/16 v0, 0x19

    iget v3, p0, Lcom/google/googlenav/aZ;->ah:I

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2031
    :cond_29
    iget-object v0, p0, Lcom/google/googlenav/aZ;->i:Ljava/lang/String;

    invoke-virtual {v5, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2032
    iget-object v0, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 2033
    const/16 v0, 0xe

    iget-object v3, p0, Lcom/google/googlenav/aZ;->M:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2035
    :cond_3d
    iget-object v0, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 2036
    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/googlenav/aZ;->N:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2038
    :cond_4c
    iget-object v0, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5a

    .line 2039
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/googlenav/aZ;->m:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2042
    :cond_5a
    const/4 v0, 0x5

    invoke-virtual {v5, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2044
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    iget v3, p0, Lcom/google/googlenav/aZ;->w:I

    iget v4, p0, Lcom/google/googlenav/aZ;->x:I

    invoke-static {v0, v3, v4, v6}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2046
    const/4 v3, 0x7

    invoke-virtual {v5, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2048
    const/16 v0, 0xb

    iget v3, p0, Lcom/google/googlenav/aZ;->G:I

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2049
    invoke-direct {p0, v5}, Lcom/google/googlenav/aZ;->l(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2051
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2054
    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_136

    .line 2055
    iget-object v0, p0, Lcom/google/googlenav/aZ;->H:Ljava/lang/String;

    invoke-virtual {v3, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 2059
    :goto_8d
    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9c

    .line 2060
    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/googlenav/aZ;->I:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 2065
    :cond_9c
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ag()Ljava/lang/String;

    move-result-object v4

    .line 2066
    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_ac

    .line 2070
    const/16 v0, 0x8

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 2074
    :cond_ac
    if-eqz v0, :cond_b3

    .line 2075
    const/16 v0, 0x9

    invoke-virtual {v5, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2078
    :cond_b3
    iget-object v0, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    if-eqz v0, :cond_bc

    .line 2079
    const/16 v0, 0xc

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2082
    :cond_bc
    iget-object v0, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    if-eqz v0, :cond_d2

    move v0, v2

    .line 2083
    :goto_c1
    iget-object v3, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_d2

    .line 2084
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/googlenav/aZ;->Y:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v5, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 2083
    add-int/lit8 v0, v0, 0x1

    goto :goto_c1

    .line 2088
    :cond_d2
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    if-eqz v0, :cond_135

    move v3, v2

    .line 2089
    :goto_d7
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v0, v0

    if-ge v3, v0, :cond_135

    .line 2090
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v6, v0, v3

    .line 2091
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2093
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->a()I

    move-result v0

    invoke-virtual {v7, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2094
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2095
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_12c

    move v4, v2

    .line 2096
    :goto_fc
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_12c

    .line 2097
    new-instance v8, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/fC;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2099
    invoke-virtual {v6}, Lcom/google/googlenav/bc;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bd;

    .line 2100
    invoke-virtual {v0}, Lcom/google/googlenav/bd;->a()I

    move-result v9

    invoke-virtual {v8, v1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2101
    invoke-virtual {v0}, Lcom/google/googlenav/bd;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2102
    invoke-virtual {v7, v11, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2096
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_fc

    .line 2105
    :cond_12c
    const/16 v0, 0x12

    invoke-virtual {v5, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2089
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_d7

    .line 2108
    :cond_135
    return-object v5

    :cond_136
    move v0, v2

    goto/16 :goto_8d
.end method

.method public ai()[Lcom/google/googlenav/ui/aH;
    .registers 6

    .prologue
    .line 2231
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2234
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 2235
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2238
    :cond_12
    const/4 v0, 0x0

    move v2, v0

    :goto_14
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-ge v2, v0, :cond_23

    .line 2239
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v1

    .line 2240
    if-nez v1, :cond_2b

    .line 2253
    :cond_23
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_47

    .line 2254
    const/4 v0, 0x0

    .line 2256
    :goto_2a
    return-object v0

    .line 2243
    :cond_2b
    invoke-interface {v1}, LaN/g;->a()I

    move-result v0

    .line 2244
    const/4 v4, 0x2

    if-eq v0, v4, :cond_35

    const/4 v4, 0x3

    if-ne v0, v4, :cond_43

    :cond_35
    move-object v0, v1

    .line 2246
    check-cast v0, Lcom/google/googlenav/ui/aH;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aH;->h()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 2248
    check-cast v1, Lcom/google/googlenav/ui/aH;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2238
    :cond_43
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 2256
    :cond_47
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ui/aH;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ui/aH;

    goto :goto_2a
.end method

.method public aj()I
    .registers 2

    .prologue
    .line 2265
    iget v0, p0, Lcom/google/googlenav/aZ;->G:I

    return v0
.end method

.method public ak()I
    .registers 2

    .prologue
    .line 2269
    iget v0, p0, Lcom/google/googlenav/aZ;->q:I

    return v0
.end method

.method public al()I
    .registers 3

    .prologue
    .line 2273
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_e

    sget v0, Lcom/google/googlenav/aZ;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_e

    .line 2274
    sget v0, Lcom/google/googlenav/aZ;->e:I

    .line 2279
    :goto_d
    return v0

    :cond_e
    const/16 v0, 0xa

    goto :goto_d
.end method

.method public am()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 2285
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v2

    if-ge v1, v2, :cond_c

    .line 2299
    :cond_b
    :goto_b
    return v0

    .line 2291
    :cond_c
    iget-boolean v1, p0, Lcom/google/googlenav/aZ;->s:Z

    if-eqz v1, :cond_b

    .line 2295
    const v0, 0x7fffffff

    .line 2296
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ak()I

    move-result v1

    if-lez v1, :cond_27

    .line 2297
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ak()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aj()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 2299
    :cond_27
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->al()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_b
.end method

.method public an()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2303
    iget-object v0, p0, Lcom/google/googlenav/aZ;->t:Ljava/lang/String;

    return-object v0
.end method

.method public ao()Z
    .registers 2

    .prologue
    .line 2307
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->r:Z

    return v0
.end method

.method public ap()I
    .registers 3

    .prologue
    .line 2328
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v1, v1

    if-ge v0, v1, :cond_1e

    .line 2329
    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_1b

    iget-object v1, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ac()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 2333
    :goto_1a
    return v0

    .line 2328
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2333
    :cond_1e
    const/4 v0, -0x1

    goto :goto_1a
.end method

.method public aq()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 2338
    move v1, v0

    .line 2339
    :goto_2
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v2, v2

    if-ge v0, v2, :cond_20

    .line 2340
    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-nez v2, :cond_1d

    iget-object v2, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 2341
    add-int/lit8 v1, v1, 0x1

    .line 2339
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2344
    :cond_20
    return v1
.end method

.method public ar()Z
    .registers 3

    .prologue
    .line 2370
    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public as()Z
    .registers 3

    .prologue
    .line 2380
    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public at()Z
    .registers 3

    .prologue
    .line 2399
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    .line 2400
    if-eqz v0, :cond_10

    .line 2401
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 2403
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public au()Z
    .registers 3

    .prologue
    .line 2412
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public av()Z
    .registers 2

    .prologue
    .line 2426
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->q:Z

    return v0
.end method

.method public aw()I
    .registers 2

    .prologue
    .line 2430
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget v0, v0, Lcom/google/googlenav/bf;->s:I

    return v0
.end method

.method public ax()Z
    .registers 2

    .prologue
    .line 2446
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->m:Z

    if-eqz v0, :cond_20

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aT()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aU()Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public ay()J
    .registers 3

    .prologue
    .line 2455
    iget-wide v0, p0, Lcom/google/googlenav/aZ;->X:J

    return-wide v0
.end method

.method public az()Z
    .registers 2

    .prologue
    .line 2539
    iget-boolean v0, p0, Lcom/google/googlenav/aZ;->L:Z

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 1088
    const/16 v0, 0x2e

    return v0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .registers 3
    .parameter

    .prologue
    .line 2507
    if-ltz p1, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    if-lt p1, v0, :cond_9

    .line 2508
    :cond_7
    const/4 v0, 0x0

    .line 2510
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    aget-object v0, v0, p1

    goto :goto_8
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter

    .prologue
    .line 2122
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2124
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2126
    iget-object v1, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    if-eqz v1, :cond_19

    .line 2127
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/aZ;->O:Lcom/google/googlenav/layer/m;

    invoke-virtual {v2}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2130
    :cond_19
    return-object v0
.end method

.method public b(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 1981
    invoke-direct {p0}, Lcom/google/googlenav/aZ;->aZ()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1982
    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1983
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 835
    iput-object p1, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    .line 836
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2558
    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->L:Z

    .line 2559
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 2497
    iget v0, p0, Lcom/google/googlenav/aZ;->D:I

    return v0
.end method

.method public c(I)I
    .registers 3
    .parameter

    .prologue
    .line 2530
    invoke-direct {p0, p1}, Lcom/google/googlenav/aZ;->h(I)I

    move-result v0

    return v0
.end method

.method public c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2892
    iput-boolean p1, p0, Lcom/google/googlenav/aZ;->ae:Z

    .line 2893
    return-void
.end method

.method public c_()Z
    .registers 2

    .prologue
    .line 1159
    const/4 v0, 0x1

    return v0
.end method

.method public d()B
    .registers 2

    .prologue
    .line 2502
    iget-byte v0, p0, Lcom/google/googlenav/aZ;->E:B

    return v0
.end method

.method public d(I)V
    .registers 2
    .parameter

    .prologue
    .line 839
    iput p1, p0, Lcom/google/googlenav/aZ;->B:I

    .line 840
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 2422
    iget-object v0, p0, Lcom/google/googlenav/aZ;->S:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d_()V
    .registers 2

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    if-eqz v0, :cond_9

    .line 1144
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    invoke-interface {v0, p0}, Lcom/google/googlenav/bb;->a(Lcom/google/googlenav/aZ;)V

    .line 1146
    :cond_9
    return-void
.end method

.method public e()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 2515
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .registers 2
    .parameter

    .prologue
    .line 1023
    iput p1, p0, Lcom/google/googlenav/aZ;->V:I

    .line 1024
    return-void
.end method

.method public f()I
    .registers 2

    .prologue
    .line 2525
    iget-object v0, p0, Lcom/google/googlenav/aZ;->f:[Lcom/google/googlenav/ai;

    array-length v0, v0

    return v0
.end method

.method public f(I)I
    .registers 3
    .parameter

    .prologue
    .line 2317
    if-ltz p1, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    array-length v0, v0

    if-lt p1, v0, :cond_9

    .line 2318
    :cond_7
    const/4 v0, -0x1

    .line 2320
    :goto_8
    return v0

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/aZ;->R:[I

    aget v0, v0, p1

    goto :goto_8
.end method

.method public g(I)Lcom/google/googlenav/bc;
    .registers 4
    .parameter

    .prologue
    .line 2742
    iget-object v0, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    if-eqz v0, :cond_1c

    .line 2743
    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    array-length v1, v1

    if-ge v0, v1, :cond_1c

    .line 2744
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->a()I

    move-result v1

    if-ne v1, p1, :cond_19

    .line 2745
    iget-object v1, p0, Lcom/google/googlenav/aZ;->ab:[Lcom/google/googlenav/bc;

    aget-object v0, v1, v0

    .line 2749
    :goto_18
    return-object v0

    .line 2743
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2749
    :cond_1c
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public i()I
    .registers 2

    .prologue
    .line 692
    iget v0, p0, Lcom/google/googlenav/aZ;->ag:I

    return v0
.end method

.method public k()Lcom/google/googlenav/ba;
    .registers 2

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    iget-object v0, v0, Lcom/google/googlenav/bf;->h:Lcom/google/googlenav/ba;

    return-object v0
.end method

.method public l()Lcom/google/googlenav/bb;
    .registers 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/googlenav/aZ;->W:Lcom/google/googlenav/bb;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/bf;
    .registers 2

    .prologue
    .line 808
    iget-object v0, p0, Lcom/google/googlenav/aZ;->h:Lcom/google/googlenav/bf;

    return-object v0
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/googlenav/aZ;->C:Ljava/lang/String;

    return-object v0
.end method

.method public p()I
    .registers 2

    .prologue
    .line 843
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    return v0
.end method

.method public q()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 851
    iget v1, p0, Lcom/google/googlenav/aZ;->B:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public r()Z
    .registers 3

    .prologue
    .line 855
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public t()Z
    .registers 3

    .prologue
    .line 860
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public u()I
    .registers 3

    .prologue
    .line 873
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    .line 874
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 876
    :cond_b
    iget v0, p0, Lcom/google/googlenav/aZ;->ah:I

    return v0
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 880
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public w()Z
    .registers 3

    .prologue
    .line 892
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public x()Z
    .registers 3

    .prologue
    .line 902
    iget v0, p0, Lcom/google/googlenav/aZ;->B:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public y()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/googlenav/aZ;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public z()I
    .registers 2

    .prologue
    .line 917
    iget v0, p0, Lcom/google/googlenav/aZ;->j:I

    return v0
.end method
