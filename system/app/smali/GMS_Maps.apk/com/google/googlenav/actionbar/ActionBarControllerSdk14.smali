.class public Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;
.super Lcom/google/googlenav/actionbar/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;


# static fields
.field private static final a:[I

.field public static final b:Landroid/graphics/drawable/ColorDrawable;

.field public static final c:Landroid/graphics/drawable/ColorDrawable;

.field public static final d:Landroid/graphics/drawable/ColorDrawable;

.field public static final e:Landroid/graphics/drawable/ColorDrawable;

.field static final synthetic g:Z


# instance fields
.field protected f:Landroid/app/ActionBar;

.field private h:Landroid/widget/SearchView;

.field private i:Lcom/google/googlenav/ui/s;

.field private j:Landroid/app/SearchManager;

.field private k:Lcom/google/android/maps/MapsActivity;

.field private l:Lcom/google/googlenav/actionbar/b;

.field private m:Landroid/view/MenuItem;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:I

.field private q:LaA/g;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, -0x6700

    .line 56
    const-class v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_3a

    move v0, v1

    :goto_d
    sput-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    .line 65
    new-array v0, v1, [I

    const v1, 0x10102eb

    aput v1, v0, v2

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a:[I

    .line 67
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    .line 69
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->c:Landroid/graphics/drawable/ColorDrawable;

    .line 70
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x3400

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    .line 72
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0xd6d6d7

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->e:Landroid/graphics/drawable/ColorDrawable;

    return-void

    :cond_3a
    move v0, v2

    .line 56
    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/googlenav/actionbar/a;-><init>()V

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)Lcom/google/googlenav/actionbar/b;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method private a(Ljava/lang/String;LaN/B;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    if-nez v0, :cond_f

    .line 403
    :cond_e
    :goto_e
    return-void

    .line 369
    :cond_f
    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    .line 371
    const/4 v0, 0x0

    .line 372
    if-eqz p2, :cond_27

    .line 377
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v1

    invoke-virtual {v1, p2}, LaN/u;->c(LaN/B;)V

    .line 382
    :cond_27
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1}, Lcom/google/googlenav/actionbar/b;->a()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 383
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p1, p2}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 387
    :cond_38
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1, p1}, Lcom/google/googlenav/actionbar/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5a

    .line 389
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v1, p1}, Lcom/google/googlenav/actionbar/b;->a(Ljava/lang/String;)Lcom/google/googlenav/bf;

    move-result-object v1

    .line 391
    if-eqz v0, :cond_64

    .line 392
    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2, v1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->b(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 396
    :goto_55
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 400
    :cond_5a
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_e

    .line 401
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_e

    :cond_64
    move-object v0, v1

    goto :goto_55
.end method

.method private a(ILbb/w;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 446
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 447
    invoke-virtual {p2}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v1

    .line 450
    invoke-virtual {v0}, Lbb/o;->i()V

    .line 451
    invoke-static {v1}, Lau/b;->i(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Lbb/o;->a(III)V

    .line 453
    const-string v2, "s"

    sget-object v3, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-virtual {v0, v1, v2, v3}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 456
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_2a

    .line 458
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    const-string v1, "stars"

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    .line 484
    :cond_29
    :goto_29
    return v4

    .line 462
    :cond_2a
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_39

    .line 464
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    invoke-interface {v0, p2}, Lcom/google/googlenav/actionbar/b;->a(Lbb/w;)Z

    move-result v0

    if-nez v0, :cond_29

    .line 469
    :cond_39
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4b

    .line 472
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbf/am;->a(Lbb/w;)V

    goto :goto_29

    .line 476
    :cond_4b
    invoke-virtual {p2}, Lbb/w;->e()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_5e

    .line 477
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_29

    .line 482
    :cond_5e
    invoke-virtual {p2}, Lbb/w;->h()LaN/B;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Ljava/lang/String;LaN/B;)V

    goto :goto_29
.end method

.method static synthetic b(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private p()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 348
    :try_start_1
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb/o;->c(I)Lbb/w;
    :try_end_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_9} :catch_10

    move-result-object v0

    .line 352
    :goto_a
    if-eqz v0, :cond_f

    .line 353
    invoke-direct {p0, v2, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(ILbb/w;)Z

    .line 355
    :cond_f
    return-void

    .line 349
    :catch_10
    move-exception v0

    .line 350
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private q()LaA/g;
    .registers 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    if-nez v0, :cond_b

    .line 537
    new-instance v0, Lcom/google/googlenav/actionbar/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/actionbar/e;-><init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)V

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    .line 553
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q:LaA/g;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_f

    .line 634
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 636
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_47

    .line 160
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 161
    if-eqz p2, :cond_36

    .line 162
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_36

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 165
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    if-eqz v0, :cond_36

    .line 168
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    :cond_36
    if-eqz p3, :cond_3d

    .line 173
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 178
    :cond_3d
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 181
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 183
    :cond_47
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_9

    .line 573
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 575
    :cond_9
    return-void
.end method

.method public a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_f

    .line 518
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, p1, p2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 519
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 521
    :cond_f
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 267
    :cond_7
    :goto_7
    return-void

    .line 202
    :cond_8
    sget-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    if-nez v0, :cond_14

    if-nez p1, :cond_14

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 203
    :cond_14
    sget-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->g:Z

    if-nez v0, :cond_20

    if-nez p3, :cond_20

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 205
    :cond_20
    iput-object p3, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    .line 206
    check-cast p1, Landroid/widget/SearchView;

    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    .line 207
    iput-object p2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    .line 210
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 212
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 213
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    .line 215
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const/16 v1, 0x4f6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    .line 221
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    .line 224
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setMaxWidth(I)V

    .line 230
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    invoke-virtual {v2, v0}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 237
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p2, :cond_7

    .line 238
    new-instance v0, Lcom/google/googlenav/actionbar/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/actionbar/d;-><init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)V

    invoke-interface {p2, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    goto :goto_7
.end method

.method public a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 117
    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    .line 118
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-nez v0, :cond_c

    .line 149
    :goto_b
    return-void

    .line 121
    :cond_c
    invoke-virtual {p0, p1, p3}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/s;)V

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 127
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/16 v1, 0x2a7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 129
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 130
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    .line 136
    :goto_36
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSplitBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 145
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 148
    iput-boolean v3, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    goto :goto_b

    .line 133
    :cond_4c
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_36
.end method

.method protected a(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/s;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->k:Lcom/google/android/maps/MapsActivity;

    .line 187
    iput-object p2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    .line 188
    const-string v0, "search"

    invoke-virtual {p1, v0}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    .line 190
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    .line 191
    return-void
.end method

.method public a(Lcom/google/googlenav/actionbar/b;)V
    .registers 3
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_6

    .line 283
    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    .line 285
    :cond_6
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_11

    if-eqz p1, :cond_11

    .line 272
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 275
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 277
    :cond_11
    return-void
.end method

.method public b(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_1d

    .line 526
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n()Landroid/content/Context;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LaA/h;->a(Landroid/app/ActionBar;Landroid/view/View;Landroid/app/ActionBar$LayoutParams;LaA/g;Landroid/content/Context;)V

    .line 528
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 530
    :cond_1d
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->j:Landroid/app/SearchManager;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public c()Lcom/google/googlenav/actionbar/b;
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->l:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_24

    .line 293
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_15

    .line 294
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    .line 298
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_24

    .line 299
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 300
    const/4 v0, 0x1

    .line 303
    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public e()V
    .registers 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_9

    .line 309
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->m:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 311
    :cond_9
    return-void
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public g()V
    .registers 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_c

    .line 497
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 498
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    .line 500
    :cond_c
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_c

    .line 505
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    .line 508
    :cond_c
    return-void
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o:Z

    return v0
.end method

.method public j()V
    .registers 4

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_1b

    .line 561
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 562
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->q()LaA/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    .line 568
    :cond_1b
    :goto_1b
    return-void

    .line 565
    :cond_1c
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    goto :goto_1b
.end method

.method public k()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 591
    iget v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    .line 592
    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i()Z

    move-result v2

    if-nez v2, :cond_a

    .line 608
    :goto_9
    return v0

    .line 595
    :cond_a
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2f

    .line 596
    invoke-virtual {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a:[I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 600
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_23

    if-eqz v1, :cond_2c

    .line 601
    :cond_23
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    .line 602
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 606
    :cond_2c
    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    goto :goto_9

    :cond_2f
    move v0, v1

    goto :goto_9
.end method

.method public l()V
    .registers 2

    .prologue
    .line 613
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p:I

    .line 614
    return-void
.end method

.method public m()V
    .registers 5

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_1c

    .line 619
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LaA/h;->a(Landroid/content/Context;Landroid/view/View;LaA/g;)V

    .line 622
    :cond_1c
    return-void
.end method

.method public n()Landroid/content/Context;
    .registers 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public o()Landroid/content/Context;
    .registers 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->k:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_11

    .line 325
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 326
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 328
    :cond_11
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 419
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 420
    if-eqz p2, :cond_2e

    .line 421
    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->i:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->o()Lbf/i;

    move-result-object v2

    invoke-static {v1, v2}, Lbb/n;->a(Lcom/google/googlenav/ui/wizard/C;Lbf/i;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbb/o;->d(I)V

    .line 425
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 426
    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->h:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 431
    :cond_2d
    :goto_2d
    return-void

    .line 429
    :cond_2e
    invoke-virtual {v0}, Lbb/o;->j()V

    goto :goto_2d
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->n:Ljava/lang/String;

    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 332
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 333
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->i()V

    .line 334
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Ljava/lang/String;LaN/B;)V

    .line 338
    :goto_11
    const/4 v0, 0x1

    return v0

    .line 336
    :cond_13
    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->p()V

    goto :goto_11
.end method

.method public onSuggestionClick(I)Z
    .registers 3
    .parameter

    .prologue
    .line 437
    :try_start_0
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb/o;->c(I)Lbb/w;
    :try_end_7
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_7} :catch_d

    move-result-object v0

    .line 442
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(ILbb/w;)Z

    move-result v0

    :goto_c
    return v0

    .line 438
    :catch_d
    move-exception v0

    .line 440
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public onSuggestionSelect(I)Z
    .registers 3
    .parameter

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method
