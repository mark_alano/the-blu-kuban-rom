.class public Lcom/google/googlenav/ae;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/googlenav/af;

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;ILjava/lang/String;Lcom/google/googlenav/af;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Law/a;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    .line 62
    iput p2, p0, Lcom/google/googlenav/ae;->b:I

    .line 63
    iput-object p3, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    .line 65
    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 74
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 75
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/googlenav/ae;->b:I

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 79
    iget v0, p0, Lcom/google/googlenav/ae;->b:I

    if-nez v0, :cond_6c

    .line 80
    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    invoke-virtual {v2, v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 81
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 82
    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 83
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 92
    :cond_3b
    :goto_3b
    iget-object v0, p0, Lcom/google/googlenav/ae;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_49
    :goto_49
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ay;

    .line 95
    invoke-virtual {v0}, Lcom/google/googlenav/ay;->d()Z

    move-result v1

    if-nez v1, :cond_49

    .line 96
    iget v1, p0, Lcom/google/googlenav/ae;->b:I

    if-ne v1, v5, :cond_77

    const/4 v1, -0x1

    .line 99
    :goto_60
    const/4 v4, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ay;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 100
    invoke-virtual {v0, v5}, Lcom/google/googlenav/ay;->a(Z)V

    goto :goto_49

    .line 86
    :cond_6c
    iget-object v0, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    if-eqz v0, :cond_3b

    .line 88
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/googlenav/ae;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_3b

    .line 96
    :cond_77
    const v1, 0x124f80

    goto :goto_60

    .line 103
    :cond_7b
    const/4 v0, 0x5

    invoke-virtual {v2, v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 104
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 105
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 109
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_14

    .line 112
    iput-boolean v2, p0, Lcom/google/googlenav/ae;->e:Z

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ae;->g:I

    .line 119
    :goto_13
    return v1

    .line 115
    :cond_14
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ae;->g:I

    .line 116
    iget v0, p0, Lcom/google/googlenav/ae;->g:I

    if-nez v0, :cond_2a

    move v0, v1

    :goto_1f
    iput-boolean v0, p0, Lcom/google/googlenav/ae;->e:Z

    .line 117
    iget v0, p0, Lcom/google/googlenav/ae;->g:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_27

    move v2, v1

    :cond_27
    iput-boolean v2, p0, Lcom/google/googlenav/ae;->f:Z

    goto :goto_13

    :cond_2a
    move v0, v2

    .line 116
    goto :goto_1f
.end method

.method public b()I
    .registers 2

    .prologue
    .line 69
    const/16 v0, 0x75

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    .line 125
    iget-boolean v0, p0, Lcom/google/googlenav/ae;->e:Z

    if-eqz v0, :cond_38

    .line 126
    const-string v0, "s"

    .line 130
    :goto_7
    const/16 v1, 0x65

    const-string v2, "p"

    new-array v3, v7, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "r="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    if-eqz v0, :cond_37

    .line 136
    iget-boolean v0, p0, Lcom/google/googlenav/ae;->f:Z

    if-eqz v0, :cond_3b

    .line 137
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    invoke-interface {v0, v7}, Lcom/google/googlenav/af;->b(Z)V

    .line 142
    :cond_37
    :goto_37
    return-void

    .line 128
    :cond_38
    const-string v0, "e"

    goto :goto_7

    .line 139
    :cond_3b
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    iget-boolean v1, p0, Lcom/google/googlenav/ae;->e:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/af;->a(Z)V

    goto :goto_37
.end method

.method public u_()V
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    if-eqz v0, :cond_a

    .line 152
    iget-object v0, p0, Lcom/google/googlenav/ae;->d:Lcom/google/googlenav/af;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/af;->b(Z)V

    .line 154
    :cond_a
    return-void
.end method
