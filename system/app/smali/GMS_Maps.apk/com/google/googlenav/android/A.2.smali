.class public Lcom/google/googlenav/android/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/v;
.implements LaV/i;
.implements Lcom/google/android/maps/driveabout/vector/bq;


# static fields
.field public static final a:Lcom/google/googlenav/android/A;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Lcom/google/googlenav/android/C;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/googlenav/android/A;

    invoke-direct {v0}, Lcom/google/googlenav/android/A;-><init>()V

    sput-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->d:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0}, Lcom/google/googlenav/android/C;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_d

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    .line 202
    :cond_d
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    .line 203
    return-void
.end method

.method public a(FF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 97
    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->c:Z

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/android/A;->a(FZ)V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->c:Z

    .line 102
    :cond_e
    return-void
.end method

.method a(FZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, p1, v1, p2}, Lcom/google/googlenav/android/C;->a(FZZ)V

    .line 130
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 92
    return-void
.end method

.method public a(Lcom/google/googlenav/android/B;)V
    .registers 3
    .parameter

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    if-eqz v0, :cond_9

    .line 239
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/C;->a(Lcom/google/googlenav/android/B;)V

    .line 241
    :cond_9
    return-void
.end method

.method public a(Lcom/google/googlenav/android/C;)V
    .registers 2
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    .line 79
    return-void
.end method

.method public a(ZZZII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 214
    iput-boolean p3, p0, Lcom/google/googlenav/android/A;->d:Z

    .line 215
    if-eqz p1, :cond_14

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->j()Z

    move-result v0

    if-nez v0, :cond_14

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    .line 221
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    .line 223
    :cond_14
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->f:Z

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public d()V
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->f:Z

    .line 83
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_c

    .line 85
    invoke-virtual {v0, p0}, LaV/h;->a(LaV/i;)V

    .line 87
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0, p0}, Lcom/google/googlenav/android/C;->a(LaN/v;)V

    .line 88
    return-void
.end method

.method e()Z
    .registers 2

    .prologue
    .line 110
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lcom/google/googlenav/android/A;->l()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public f()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 137
    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-nez v0, :cond_10

    .line 139
    iput-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    .line 140
    iput-boolean v1, p0, Lcom/google/googlenav/android/A;->c:Z

    .line 141
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->g()V

    .line 149
    :goto_c
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    .line 150
    return-void

    .line 145
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/android/A;->a(FZ)V

    goto :goto_c
.end method

.method g()V
    .registers 4

    .prologue
    .line 154
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_19

    .line 157
    const/16 v0, 0x62

    const-string v1, "a"

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v2

    invoke-virtual {v2}, LaV/h;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_19
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 180
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->l()Z

    move-result v0

    .line 181
    if-nez v0, :cond_17

    .line 182
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/C;->a(Z)V

    .line 185
    :cond_17
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/googlenav/android/A;->b:Z

    if-eqz v0, :cond_25

    sget-object v0, LaV/j;->c:LaV/j;

    :goto_21
    invoke-virtual {v1, v0}, LaV/h;->a(LaV/j;)V

    .line 189
    :cond_24
    return-void

    .line 185
    :cond_25
    sget-object v0, LaV/j;->b:LaV/j;

    goto :goto_21
.end method

.method public i()V
    .registers 2

    .prologue
    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/A;->d:Z

    .line 208
    invoke-virtual {p0}, Lcom/google/googlenav/android/A;->h()V

    .line 209
    return-void
.end method

.method j()Z
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    invoke-interface {v0}, Lcom/google/googlenav/android/C;->b()Z

    move-result v0

    return v0
.end method

.method public k()I
    .registers 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/googlenav/android/A;->e:Lcom/google/googlenav/android/C;

    iget-boolean v1, p0, Lcom/google/googlenav/android/A;->b:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/C;->b(Z)I

    move-result v0

    return v0
.end method
