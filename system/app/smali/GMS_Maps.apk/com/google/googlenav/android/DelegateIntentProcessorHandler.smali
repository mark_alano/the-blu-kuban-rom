.class public abstract Lcom/google/googlenav/android/DelegateIntentProcessorHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/R;


# instance fields
.field protected delegate:Lcom/google/googlenav/android/R;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/R;)V
    .registers 2
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    .line 29
    return-void
.end method


# virtual methods
.method public getActivity()Lcom/google/android/maps/MapsActivity;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method

.method public getDelegate()Lcom/google/googlenav/android/R;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUiThreadHandler()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public refreshFriends()V
    .registers 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriends()V

    .line 184
    return-void
.end method

.method public refreshFriendsSettings()V
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriendsSettings()V

    .line 189
    return-void
.end method

.method public resetForInvocation()V
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 43
    return-void
.end method

.method public resolveType(Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->resolveType(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 48
    return-void
.end method

.method public showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .registers 3
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    return-object v0
.end method

.method public showStarDetails(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showStarDetails(Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public showStarOnMap(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->showStarOnMap(Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startActivity(Landroid/content/Intent;)V

    .line 68
    return-void
.end method

.method public startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V

    .line 129
    return-void
.end method

.method public startBusinessDetailsLayer(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    .line 124
    return-void
.end method

.method public startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 139
    return-void
.end method

.method public startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/googlenav/android/R;->startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V

    .line 119
    return-void
.end method

.method public startFriendsLayer(Lbf/am;)V
    .registers 3
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    .line 88
    return-void
.end method

.method public startFriendsLayerHistorySummary()V
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startFriendsLayerHistorySummary()V

    .line 98
    return-void
.end method

.method public startFriendsListView(Lbf/am;)V
    .registers 3
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    .line 103
    return-void
.end method

.method public startFriendsLocation(Lbf/am;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsLocation(Lbf/am;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V

    .line 174
    return-void
.end method

.method public startFriendsProfile(Lbf/am;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/android/R;->startFriendsProfile(Lbf/am;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public startLatitudeSettingsActivity()V
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startLatitudeSettingsActivity()V

    .line 169
    return-void
.end method

.method public startLocationHistoryOptIn(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startLocationHistoryOptIn(Landroid/content/Intent;)V

    .line 93
    return-void
.end method

.method public startManageAutoCheckinPlaces()V
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startManageAutoCheckinPlaces()V

    .line 179
    return-void
.end method

.method public startMyPlacesList(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;)Z
    .registers 3
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startNextMatchingActivity(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public startOffersList()V
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startOffersList()V

    .line 199
    return-void
.end method

.method public startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/googlenav/android/R;->startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    .line 210
    return-void
.end method

.method public startSettingsActivity()V
    .registers 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startSettingsActivity()V

    .line 164
    return-void
.end method

.method public startTransitEntry()V
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitEntry()V

    .line 204
    return-void
.end method

.method public startTransitNavigationLayer()V
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitNavigationLayer()V

    .line 83
    return-void
.end method

.method public startTransitStationPage(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/googlenav/android/DelegateIntentProcessorHandler;->delegate:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startTransitStationPage(Ljava/lang/String;)V

    .line 144
    return-void
.end method
