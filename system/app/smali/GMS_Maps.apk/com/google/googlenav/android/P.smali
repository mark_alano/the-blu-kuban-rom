.class Lcom/google/googlenav/android/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaV/e;


# instance fields
.field final synthetic a:Lcom/google/googlenav/android/i;


# direct methods
.method constructor <init>(Lcom/google/googlenav/android/i;)V
    .registers 2
    .parameter

    .prologue
    .line 415
    iput-object p1, p0, Lcom/google/googlenav/android/p;->a:Lcom/google/googlenav/android/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 418
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    .line 419
    if-eqz v1, :cond_d

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v2

    if-nez v2, :cond_e

    .line 433
    :cond_d
    :goto_d
    return-object v0

    .line 423
    :cond_e
    invoke-interface {v1}, LaH/m;->s()LaH/h;

    move-result-object v1

    .line 424
    if-eqz v1, :cond_d

    .line 430
    new-instance v0, Landroid/location/Location;

    const-string v2, "GmmLocationManagerImpl"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 431
    invoke-virtual {v1}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 432
    invoke-virtual {v1}, LaH/h;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_d
.end method
