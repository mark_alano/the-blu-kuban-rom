.class public Lcom/google/googlenav/android/w;
.super Lcom/google/googlenav/ui/aC;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/android/i;

.field private final b:Las/c;

.field private c:Lcom/google/googlenav/ui/X;

.field private d:Lcom/google/googlenav/ui/aD;

.field private e:Lcom/google/googlenav/ui/android/p;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/i;Las/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1072
    iput-object p1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-direct {p0}, Lcom/google/googlenav/ui/aC;-><init>()V

    .line 1073
    iput-object p2, p0, Lcom/google/googlenav/android/w;->b:Las/c;

    .line 1074
    invoke-virtual {p2}, Las/c;->d()V

    .line 1075
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/aA;
    .registers 2

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->h(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/d;

    move-result-object v0

    return-object v0
.end method

.method public a(LaN/p;)Lcom/google/googlenav/ui/bF;
    .registers 4
    .parameter

    .prologue
    .line 1110
    new-instance v1, Lcom/google/googlenav/ui/android/q;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/android/w;->b(LaN/p;)Lcom/google/googlenav/ui/p;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/p;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/android/q;-><init>(LaN/p;Lcom/google/googlenav/ui/android/p;)V

    return-object v1
.end method

.method public b()Lcom/google/googlenav/ui/aD;
    .registers 3

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    if-nez v0, :cond_11

    .line 1085
    new-instance v0, Lcom/google/googlenav/ui/android/c;

    iget-object v1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v1}, Lcom/google/googlenav/android/i;->e(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/AndroidGmmApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    .line 1087
    :cond_11
    iget-object v0, p0, Lcom/google/googlenav/android/w;->d:Lcom/google/googlenav/ui/aD;

    return-object v0
.end method

.method public b(LaN/p;)Lcom/google/googlenav/ui/p;
    .registers 5
    .parameter

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    if-nez v0, :cond_17

    .line 1136
    new-instance v0, Lcom/google/googlenav/ui/android/p;

    iget-object v1, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v1}, Lcom/google/googlenav/android/i;->l(Lcom/google/googlenav/android/i;)LaN/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v2}, Lcom/google/googlenav/android/i;->i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/android/p;-><init>(LaN/p;Lcom/google/googlenav/ui/android/L;)V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    .line 1138
    :cond_17
    iget-object v0, p0, Lcom/google/googlenav/android/w;->e:Lcom/google/googlenav/ui/android/p;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/ui/X;
    .registers 2

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    if-nez v0, :cond_b

    .line 1093
    new-instance v0, Lcom/google/googlenav/ui/android/d;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/d;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    .line 1095
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/android/w;->c:Lcom/google/googlenav/ui/X;

    return-object v0
.end method

.method public d()Lcom/google/googlenav/ui/android/L;
    .registers 2

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->j(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/aa;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/googlenav/layer/r;
    .registers 2

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->k(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/layer/r;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;
    .registers 2

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .registers 2

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/googlenav/android/w;->a:Lcom/google/googlenav/android/i;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->b(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    return-object v0
.end method

.method public i()Las/c;
    .registers 2

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/google/googlenav/android/w;->b:Las/c;

    return-object v0
.end method
