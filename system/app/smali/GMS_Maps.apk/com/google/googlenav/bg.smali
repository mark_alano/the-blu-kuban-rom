.class public Lcom/google/googlenav/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private B:Ljava/util/Set;

.field private C:I

.field private D:LaW/R;

.field private E:LaN/H;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:LaN/H;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/googlenav/ba;

.field private i:Lcom/google/googlenav/bb;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:LaN/M;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:LaN/B;

.field private q:Z

.field private r:I

.field private s:I

.field private t:Z

.field private u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private v:Ljava/lang/String;

.field private w:Ljava/util/Map;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    .line 426
    iput v4, p0, Lcom/google/googlenav/bg;->b:I

    .line 427
    iput v1, p0, Lcom/google/googlenav/bg;->c:I

    .line 428
    iput v3, p0, Lcom/google/googlenav/bg;->d:I

    .line 429
    iput v3, p0, Lcom/google/googlenav/bg;->e:I

    .line 430
    iput-object v2, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    .line 431
    const-string v0, "100"

    iput-object v0, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    .line 432
    iput-object v2, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    .line 433
    iput-object v2, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    .line 434
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/googlenav/bg;->j:I

    .line 435
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    .line 436
    iput-object v2, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    .line 437
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->m:Z

    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->n:Z

    .line 439
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->o:Z

    .line 440
    iput-object v2, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    .line 441
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->q:Z

    .line 442
    iput v1, p0, Lcom/google/googlenav/bg;->r:I

    .line 443
    iput v4, p0, Lcom/google/googlenav/bg;->s:I

    .line 444
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->t:Z

    .line 445
    iput-object v2, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 446
    iput-object v2, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    .line 447
    iput-object v2, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    .line 448
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->x:Z

    .line 449
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->y:Z

    .line 450
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->z:Z

    .line 451
    iput-object v2, p0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 452
    iput-object v2, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    .line 453
    iput v1, p0, Lcom/google/googlenav/bg;->C:I

    .line 454
    iput-object v2, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    .line 455
    iput-object v2, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    .line 456
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->F:Z

    .line 457
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->G:Z

    .line 458
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->H:Z

    .line 459
    iput-boolean v1, p0, Lcom/google/googlenav/bg;->I:Z

    .line 460
    iput-object v2, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    .line 461
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/bf;)V
    .registers 3
    .parameter

    .prologue
    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469
    iget-object v0, p1, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    .line 470
    iget v0, p1, Lcom/google/googlenav/bf;->b:I

    iput v0, p0, Lcom/google/googlenav/bg;->b:I

    .line 471
    iget v0, p1, Lcom/google/googlenav/bf;->c:I

    iput v0, p0, Lcom/google/googlenav/bg;->c:I

    .line 472
    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    iput v0, p0, Lcom/google/googlenav/bg;->d:I

    .line 473
    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    iput v0, p0, Lcom/google/googlenav/bg;->e:I

    .line 474
    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    .line 475
    iget-object v0, p1, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    .line 476
    iget-object v0, p1, Lcom/google/googlenav/bf;->h:Lcom/google/googlenav/ba;

    iput-object v0, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    .line 477
    iget-object v0, p1, Lcom/google/googlenav/bf;->i:Lcom/google/googlenav/bb;

    iput-object v0, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    .line 478
    iget v0, p1, Lcom/google/googlenav/bf;->j:I

    iput v0, p0, Lcom/google/googlenav/bg;->j:I

    .line 479
    iget-object v0, p1, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    .line 480
    iget-object v0, p1, Lcom/google/googlenav/bf;->l:LaN/M;

    iput-object v0, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    .line 481
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->m:Z

    .line 482
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->n:Z

    .line 483
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->o:Z

    .line 484
    iget-object v0, p1, Lcom/google/googlenav/bf;->p:LaN/B;

    iput-object v0, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    .line 485
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->q:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->q:Z

    .line 486
    iget v0, p1, Lcom/google/googlenav/bf;->r:I

    iput v0, p0, Lcom/google/googlenav/bg;->r:I

    .line 487
    iget v0, p1, Lcom/google/googlenav/bf;->s:I

    iput v0, p0, Lcom/google/googlenav/bg;->s:I

    .line 488
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->t:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->t:Z

    .line 489
    iget-object v0, p1, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 490
    iget-object v0, p1, Lcom/google/googlenav/bf;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    .line 491
    iget-object v0, p1, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    .line 492
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->z:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->x:Z

    .line 493
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->y:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->y:Z

    .line 494
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->A:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->z:Z

    .line 495
    iget-object v0, p1, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 496
    iget-object v0, p1, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    iput-object v0, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    .line 497
    iget v0, p1, Lcom/google/googlenav/bf;->C:I

    iput v0, p0, Lcom/google/googlenav/bg;->C:I

    .line 498
    iget-object v0, p1, Lcom/google/googlenav/bf;->D:LaW/R;

    iput-object v0, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    .line 499
    iget-object v0, p1, Lcom/google/googlenav/bf;->E:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    .line 500
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->F:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->F:Z

    .line 501
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->G:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->G:Z

    .line 502
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->H:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->H:Z

    .line 503
    iget-boolean v0, p1, Lcom/google/googlenav/bf;->I:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->I:Z

    .line 504
    iget-object v0, p1, Lcom/google/googlenav/bf;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    .line 505
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/bf;
    .registers 39

    .prologue
    .line 731
    new-instance v1, Lcom/google/googlenav/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/bg;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/bg;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/googlenav/bg;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/googlenav/bg;->e:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/bg;->f:LaN/H;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/googlenav/bg;->j:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/bg;->l:LaN/M;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/googlenav/bg;->m:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/googlenav/bg;->n:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->p:LaN/B;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->q:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->r:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->s:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->t:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->x:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->y:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->z:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->C:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->D:LaW/R;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->E:LaN/H;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->F:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->G:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->H:Z

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->I:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-direct/range {v1 .. v37}, Lcom/google/googlenav/bf;-><init>(Ljava/lang/String;IIIILaN/H;Ljava/lang/String;Lcom/google/googlenav/ba;Lcom/google/googlenav/bb;ILjava/lang/String;LaN/M;ZZZLaN/B;ZIIZ[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/util/Map;ZZZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;ILaW/R;LaN/H;ZZZZLjava/lang/String;)V

    return-object v1
.end method

.method public a(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 515
    iput p1, p0, Lcom/google/googlenav/bg;->b:I

    .line 516
    return-object p0
.end method

.method public a(LaN/B;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 599
    iput-object p1, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    .line 600
    return-object p0
.end method

.method public a(LaN/H;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 539
    iput-object p1, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    .line 540
    return-object p0
.end method

.method public a(LaN/M;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 575
    iput-object p1, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    .line 576
    return-object p0
.end method

.method public a(LaW/R;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 691
    iput-object p1, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    .line 692
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 551
    iput-object p1, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    .line 552
    return-object p0
.end method

.method public a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 557
    iput-object p1, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    .line 558
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    .line 510
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 641
    iput-object p1, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    .line 642
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 646
    iput-object p1, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    .line 647
    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 581
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->m:Z

    .line 582
    return-object p0
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 629
    iput-object p1, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 630
    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 521
    iput p1, p0, Lcom/google/googlenav/bg;->c:I

    .line 522
    return-object p0
.end method

.method public b(LaN/H;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 697
    iput-object p1, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    .line 698
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 545
    iput-object p1, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    .line 546
    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 587
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->n:Z

    .line 588
    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 527
    iput p1, p0, Lcom/google/googlenav/bg;->d:I

    .line 528
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 569
    iput-object p1, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    .line 570
    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 593
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->o:Z

    .line 594
    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 533
    iput p1, p0, Lcom/google/googlenav/bg;->e:I

    .line 534
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 635
    iput-object p1, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    .line 636
    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 605
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->q:Z

    .line 606
    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 563
    iput p1, p0, Lcom/google/googlenav/bg;->j:I

    .line 564
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 722
    iput-object p1, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    .line 723
    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 623
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->t:Z

    .line 624
    return-object p0
.end method

.method public f(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 617
    iput p1, p0, Lcom/google/googlenav/bg;->s:I

    .line 618
    return-object p0
.end method

.method public f(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 652
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->y:Z

    .line 653
    return-object p0
.end method

.method public g(I)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 683
    iput p1, p0, Lcom/google/googlenav/bg;->C:I

    .line 684
    return-object p0
.end method

.method public g(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 658
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->x:Z

    .line 659
    return-object p0
.end method

.method public h(Z)Lcom/google/googlenav/bg;
    .registers 3
    .parameter

    .prologue
    .line 664
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->z:Z

    .line 665
    return-object p0
.end method

.method public i(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->F:Z

    .line 703
    return-object p0
.end method

.method public j(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 707
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->G:Z

    .line 708
    return-object p0
.end method

.method public k(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 712
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->H:Z

    .line 713
    return-object p0
.end method

.method public l(Z)Lcom/google/googlenav/bg;
    .registers 2
    .parameter

    .prologue
    .line 717
    iput-boolean p1, p0, Lcom/google/googlenav/bg;->I:Z

    .line 718
    return-object p0
.end method
