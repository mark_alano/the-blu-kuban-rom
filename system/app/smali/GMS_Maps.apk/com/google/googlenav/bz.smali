.class public Lcom/google/googlenav/bz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/C;
.implements LaR/ab;
.implements Lcom/google/googlenav/F;


# instance fields
.field private a:I

.field private b:B

.field private c:Lcom/google/googlenav/by;

.field private volatile d:Lcom/google/googlenav/bB;

.field private final e:LaR/u;

.field private final f:LaR/aa;

.field private final g:Lbf/k;

.field private final h:Lcom/google/googlenav/bC;


# direct methods
.method public constructor <init>(LaR/u;LaR/aa;Lbf/k;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    new-instance v0, Lcom/google/googlenav/bC;

    invoke-direct {v0, p1}, Lcom/google/googlenav/bC;-><init>(LaR/u;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/googlenav/bz;-><init>(LaR/u;LaR/aa;Lbf/k;Lcom/google/googlenav/bC;)V

    .line 89
    return-void
.end method

.method constructor <init>(LaR/u;LaR/aa;Lbf/k;Lcom/google/googlenav/bC;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/bz;->a:I

    .line 50
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    .line 65
    new-instance v0, Lcom/google/googlenav/bB;

    invoke-direct {v0}, Lcom/google/googlenav/bB;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    .line 94
    iput-object p1, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    .line 95
    iput-object p2, p0, Lcom/google/googlenav/bz;->f:LaR/aa;

    .line 96
    iput-object p3, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    .line 97
    iput-object p4, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    .line 107
    invoke-interface {p1, p0}, LaR/u;->a(LaR/C;)V

    .line 108
    invoke-interface {p2, p0}, LaR/aa;->a(LaR/ab;)V

    .line 110
    invoke-static {p4}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    .line 111
    return-void
.end method

.method static synthetic a(LaR/D;)Lcom/google/googlenav/by;
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-static {p0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaR/D;LaR/H;)Lcom/google/googlenav/by;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/googlenav/bz;->b(LaR/D;LaR/H;)Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bz;)Lcom/google/googlenav/by;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bz;Lcom/google/googlenav/by;)Lcom/google/googlenav/by;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/googlenav/bz;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/bz;)LaR/u;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    return-object v0
.end method

.method private static b(LaR/D;)Lcom/google/googlenav/by;
    .registers 2
    .parameter

    .prologue
    .line 206
    new-instance v0, Lcom/google/googlenav/by;

    invoke-direct {v0, p0}, Lcom/google/googlenav/by;-><init>(LaR/D;)V

    return-object v0
.end method

.method private static b(LaR/D;LaR/H;)Lcom/google/googlenav/by;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 214
    if-eqz p1, :cond_16

    .line 216
    :try_start_2
    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 217
    new-instance v0, Lcom/google/googlenav/by;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/by;-><init>(LaR/D;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_14} :catch_15

    .line 224
    :goto_14
    return-object v0

    .line 220
    :catch_15
    move-exception v0

    .line 224
    :cond_16
    invoke-static {p0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    goto :goto_14
.end method

.method private static b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 196
    if-eqz p0, :cond_4

    if-nez p1, :cond_6

    .line 197
    :cond_4
    const/4 v0, 0x0

    .line 199
    :goto_5
    return v0

    :cond_6
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method private declared-synchronized g()V
    .registers 2

    .prologue
    .line 316
    monitor-enter p0

    :try_start_1
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    if-eqz v0, :cond_1a

    .line 317
    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    invoke-virtual {v0}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 319
    if-ltz v0, :cond_1a

    .line 320
    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(I)V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1f

    .line 325
    :goto_18
    monitor-exit p0

    return-void

    .line 324
    :cond_1a
    const/4 v0, -0x2

    :try_start_1b
    invoke-virtual {p0, v0}, Lcom/google/googlenav/bz;->a(I)V
    :try_end_1e
    .catchall {:try_start_1b .. :try_end_1e} :catchall_1f

    goto :goto_18

    .line 316
    :catchall_1f
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public F_()V
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    .line 342
    return-void
.end method

.method public a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 273
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 277
    :goto_7
    return v1

    .line 276
    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 277
    if-nez v0, :cond_17

    move v0, v1

    :goto_15
    move v1, v0

    goto :goto_7

    :cond_17
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_15
.end method

.method public a()V
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    invoke-interface {v0, p0}, LaR/u;->b(LaR/C;)V

    .line 115
    iget-object v0, p0, Lcom/google/googlenav/bz;->f:LaR/aa;

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    .line 116
    return-void
.end method

.method public declared-synchronized a(B)V
    .registers 3
    .parameter

    .prologue
    .line 154
    monitor-enter p0

    :try_start_1
    iput-byte p1, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 155
    monitor-exit p0

    return-void

    .line 154
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 4
    .parameter

    .prologue
    .line 120
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/googlenav/bz;->a:I

    .line 122
    const/4 v0, -0x1

    if-ne p1, v0, :cond_e

    .line 123
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_39

    .line 140
    :cond_c
    :goto_c
    monitor-exit p0

    return-void

    .line 128
    :cond_e
    :try_start_e
    invoke-virtual {p0, p1}, Lcom/google/googlenav/bz;->d(I)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_2d

    iget-object v1, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    invoke-static {v0, v1}, Lcom/google/googlenav/bz;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ai;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 130
    iget-object v1, p0, Lcom/google/googlenav/bz;->e:LaR/u;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 131
    if-nez v0, :cond_3c

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    .line 137
    :cond_2d
    :goto_2d
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;

    if-eqz v0, :cond_c

    .line 138
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_38
    .catchall {:try_start_e .. :try_end_38} :catchall_39

    goto :goto_c

    .line 120
    :catchall_39
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :cond_3c
    :try_start_3c
    invoke-static {v0}, Lcom/google/googlenav/bz;->b(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_39

    goto :goto_2d
.end method

.method public a(LaR/P;)V
    .registers 2
    .parameter

    .prologue
    .line 336
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 347
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .registers 4
    .parameter

    .prologue
    .line 236
    new-instance v0, Lcom/google/googlenav/bA;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bA;-><init>(Lcom/google/googlenav/bz;Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/googlenav/bA;->g()V

    .line 267
    return-void
.end method

.method public a(Z)Z
    .registers 4
    .parameter

    .prologue
    .line 293
    if-eqz p1, :cond_d

    .line 294
    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    move-result v0

    .line 295
    iget-object v1, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v1, v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;I)V

    .line 297
    :cond_d
    monitor-enter p0

    .line 298
    :try_start_e
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v1, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v1}, Lcom/google/googlenav/bC;->b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;

    move-result-object v1

    if-ne v0, v1, :cond_1b

    .line 299
    const/4 v0, 0x0

    monitor-exit p0

    .line 308
    :goto_1a
    return v0

    .line 301
    :cond_1b
    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    .line 302
    invoke-direct {p0}, Lcom/google/googlenav/bz;->g()V

    .line 303
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_e .. :try_end_27} :catchall_33

    .line 304
    iget-object v0, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    if-eqz v0, :cond_31

    .line 305
    const/4 v0, 0x0

    .line 306
    iget-object v1, p0, Lcom/google/googlenav/bz;->g:Lbf/k;

    invoke-interface {v1, v0}, Lbf/k;->c(Lbf/i;)V

    .line 308
    :cond_31
    const/4 v0, 0x1

    goto :goto_1a

    .line 303
    :catchall_33
    move-exception v0

    :try_start_34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    throw v0
.end method

.method public synthetic b(I)Lcom/google/googlenav/E;
    .registers 3
    .parameter

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/google/googlenav/bz;->d(I)Lcom/google/googlenav/ai;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()Lcom/google/googlenav/by;
    .registers 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_1
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    if-nez v0, :cond_8

    .line 160
    const/4 v0, 0x0

    .line 162
    :goto_6
    monitor-exit p0

    return-object v0

    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/google/googlenav/bz;->c:Lcom/google/googlenav/by;
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_b

    goto :goto_6

    .line 159
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/googlenav/bz;->h:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;)I

    .line 331
    return-void
.end method

.method public declared-synchronized c()I
    .registers 2

    .prologue
    .line 144
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/googlenav/bz;->a:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)I
    .registers 2
    .parameter

    .prologue
    .line 184
    return p1
.end method

.method public declared-synchronized d()B
    .registers 2

    .prologue
    .line 149
    monitor-enter p0

    :try_start_1
    iget-byte v0, p0, Lcom/google/googlenav/bz;->b:B
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(I)Lcom/google/googlenav/ai;
    .registers 3
    .parameter

    .prologue
    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;
    :try_end_a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_a} :catch_b

    .line 173
    :goto_a
    return-object v0

    .line 172
    :catch_b
    move-exception v0

    .line 173
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public synthetic e()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/googlenav/bz;->d:Lcom/google/googlenav/bB;

    iget-object v0, v0, Lcom/google/googlenav/bB;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    return v0
.end method
