.class public Lcom/google/googlenav/common/Config;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/d;


# static fields
.field protected static final a:Ljava/lang/Object;

.field public static b:[Ljava/lang/String;

.field private static f:Lcom/google/googlenav/common/Config;

.field private static g:Ljava/lang/String;

.field private static volatile h:Ljava/lang/Boolean;

.field private static volatile i:Ljava/lang/Boolean;

.field private static p:Ljava/lang/Thread;


# instance fields
.field protected c:Lcom/google/googlenav/common/io/g;

.field protected d:Landroid/content/Context;

.field protected e:I

.field private final j:Lcom/google/googlenav/common/io/j;

.field private final k:Lam/h;

.field private final l:Lam/b;

.field private m:F

.field private final n:F

.field private final o:F

.field private volatile q:Z

.field private r:Lcom/google/googlenav/common/e;

.field private final s:Lcom/google/googlenav/common/a;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    const/high16 v2, 0x4320

    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/common/Config;->q:Z

    .line 308
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    .line 168
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 169
    iput-object v1, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    .line 170
    iput-object v1, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    .line 171
    iput-object v1, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    .line 172
    iput-object v1, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    .line 173
    iput-object v1, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    .line 174
    const/16 v0, 0xa0

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    .line 175
    iput v2, p0, Lcom/google/googlenav/common/Config;->n:F

    .line 176
    iput v2, p0, Lcom/google/googlenav/common/Config;->o:F

    .line 177
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    .line 178
    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->G()V

    .line 179
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->a(Lcom/google/googlenav/common/Config;)V

    .line 180
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 183
    if-nez p1, :cond_10

    new-instance v0, Lcom/google/googlenav/common/io/h;

    invoke-direct {v0}, Lcom/google/googlenav/common/io/h;-><init>()V

    :goto_7
    new-instance v1, Lan/a;

    invoke-direct {v1, p1}, Lan/a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/common/Config;-><init>(Landroid/content/Context;Lcom/google/googlenav/common/io/j;Lam/h;)V

    .line 186
    return-void

    .line 183
    :cond_10
    new-instance v0, Lap/d;

    invoke-direct {v0, p1}, Lap/d;-><init>(Landroid/content/Context;)V

    goto :goto_7
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/googlenav/common/io/j;Lam/h;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/high16 v4, 0x3fd0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/common/Config;->q:Z

    .line 308
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    .line 192
    iput-object p1, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    .line 193
    iput-object p2, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    .line 194
    iput-object p3, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    .line 195
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->p:Ljava/lang/Thread;

    .line 197
    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->G()V

    .line 199
    if-eqz p1, :cond_80

    .line 203
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    .line 205
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    .line 215
    :goto_3c
    iget v0, p0, Lcom/google/googlenav/common/Config;->e:I

    int-to-float v0, v0

    .line 216
    if-eqz p1, :cond_92

    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 218
    iget v2, v1, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_63

    iget v2, v1, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-lez v2, :cond_89

    .line 220
    :cond_63
    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    .line 221
    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    .line 231
    :goto_67
    new-instance v0, Lan/d;

    invoke-direct {v0}, Lan/d;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    .line 232
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    .line 238
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->a(Lcom/google/googlenav/common/Config;)V

    .line 239
    new-instance v0, Lap/a;

    invoke-direct {v0, p1}, Lap/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    .line 240
    return-void

    .line 207
    :cond_80
    const/16 v0, 0xa0

    iput v0, p0, Lcom/google/googlenav/common/Config;->e:I

    .line 208
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/googlenav/common/Config;->m:F

    goto :goto_3c

    .line 223
    :cond_89
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    .line 224
    iget v0, v1, Landroid/util/DisplayMetrics;->ydpi:F

    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    goto :goto_67

    .line 227
    :cond_92
    iput v0, p0, Lcom/google/googlenav/common/Config;->n:F

    .line 228
    iput v0, p0, Lcom/google/googlenav/common/Config;->o:F

    goto :goto_67
.end method

.method public static F()Ljava/lang/String;
    .registers 7

    .prologue
    const/16 v6, 0x5f

    const/16 v5, 0x2d

    .line 932
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 933
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 934
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 937
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private G()V
    .registers 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    if-nez v0, :cond_b

    .line 319
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;)Lcom/google/googlenav/common/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    .line 322
    :cond_b
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->c()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    .line 323
    sget-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 324
    const-string v0, "unknown"

    sput-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    .line 327
    :cond_19
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->b()V

    .line 328
    return-void
.end method

.method private H()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 876
    sget-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    if-nez v0, :cond_12

    .line 877
    const-string v0, "en ar bg ca cs da de el en_GB es es_MX fi fr hr hu it ja ko lt lv nl no pl pt_BR pt_PT ro ru sk sl sr sv tl tr uk vi zh zh_CN"

    const-string v1, " "

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 878
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->b([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    .line 880
    :cond_12
    sget-object v0, Lcom/google/googlenav/common/Config;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public static a()Lcom/google/googlenav/common/Config;
    .registers 2

    .prologue
    .line 293
    sget-object v1, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 294
    :try_start_3
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    monitor-exit v1

    return-object v0

    .line 295
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 898
    invoke-static {p0, p1}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 899
    invoke-static {p0}, Lcom/google/googlenav/common/e;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 900
    invoke-static {v0}, Lcom/google/googlenav/common/e;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 902
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 903
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 905
    :cond_2f
    return-object v0
.end method

.method protected static a(Lcom/google/googlenav/common/Config;)V
    .registers 1
    .parameter

    .prologue
    .line 250
    sput-object p0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    .line 251
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/DataInput;Z)[Ljava/lang/String;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 537
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;Ljava/io/DataInput;Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 528
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/common/e;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 379
    .line 390
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_33
    .catchall {:try_start_1 .. :try_end_33} :catchall_56
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_33} :catch_4d

    move-result-object v1

    .line 393
    if-eqz v1, :cond_65

    :try_start_36
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 394
    const-string v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_45
    .catchall {:try_start_36 .. :try_end_45} :catchall_5d
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_45} :catch_60

    move-result-object v6

    move-object v0, v6

    .line 404
    :goto_47
    if-eqz v1, :cond_4c

    .line 405
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 408
    :cond_4c
    :goto_4c
    return-object v0

    .line 396
    :catch_4d
    move-exception v0

    move-object v0, v6

    .line 404
    :goto_4f
    if-eqz v0, :cond_63

    .line 405
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_4c

    .line 404
    :catchall_56
    move-exception v0

    :goto_57
    if-eqz v6, :cond_5c

    .line 405
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5c
    throw v0

    .line 404
    :catchall_5d
    move-exception v0

    move-object v6, v1

    goto :goto_57

    .line 396
    :catch_60
    move-exception v0

    move-object v0, v1

    goto :goto_4f

    :cond_63
    move-object v0, v6

    goto :goto_4c

    :cond_65
    move-object v0, v6

    goto :goto_47
.end method

.method public static declared-synchronized e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 442
    const-class v1, Lcom/google/googlenav/common/Config;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->b()Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_d

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_d
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 455
    const-class v1, Lcom/google/googlenav/common/Config;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->a()Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_d

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_d
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;
    .registers 3
    .parameter

    .prologue
    .line 280
    sget-object v1, Lcom/google/googlenav/common/Config;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 281
    :try_start_3
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    if-nez v0, :cond_c

    .line 282
    new-instance v0, Lcom/google/googlenav/common/Config;

    invoke-direct {v0, p0}, Lcom/google/googlenav/common/Config;-><init>(Landroid/content/Context;)V

    .line 284
    :cond_c
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    monitor-exit v1

    return-object v0

    .line 285
    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public static h()Ljava/lang/String;
    .registers 1

    .prologue
    .line 543
    sget-object v0, Lcom/google/googlenav/common/Config;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static j()Z
    .registers 1

    .prologue
    .line 562
    sget-object v0, Lcom/google/googlenav/common/Config;->f:Lcom/google/googlenav/common/Config;

    iget-object v0, v0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->d()Z

    move-result v0

    return v0
.end method

.method public static w()Z
    .registers 2

    .prologue
    .line 707
    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v0

    .line 708
    if-eqz v0, :cond_10

    const-string v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public A()Z
    .registers 3

    .prologue
    .line 813
    sget-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    .line 814
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 815
    const-string v1, "android.hardware.touchscreen.multitouch"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    .line 818
    :cond_16
    sget-object v0, Lcom/google/googlenav/common/Config;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public B()Z
    .registers 3

    .prologue
    .line 827
    sget-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    .line 828
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 829
    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    .line 832
    :cond_16
    sget-object v0, Lcom/google/googlenav/common/Config;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected C()Z
    .registers 3

    .prologue
    .line 848
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public D()Landroid/location/LocationManager;
    .registers 3

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    return-object v0
.end method

.method public E()Landroid/content/Context;
    .registers 2

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    return-object v0
.end method

.method public a(D)I
    .registers 5
    .parameter

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/j;->a(D)I

    move-result v0

    return v0
.end method

.method public a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .registers 5
    .parameter

    .prologue
    .line 695
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    new-instance v1, Ljava/util/zip/Inflater;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v0, p1, v1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)Ljava/lang/String;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 910
    const-wide/32 v4, 0xea60

    move-wide v0, p1

    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->H()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/e;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 927
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    .line 928
    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .registers 5
    .parameter

    .prologue
    .line 838
    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 839
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->g()Lcom/google/googlenav/common/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/e;->c(Ljava/lang/String;)V

    .line 840
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->g()Lcom/google/googlenav/common/e;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/common/Config;->H()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/common/Config;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/e;->d(Ljava/lang/String;)V

    .line 841
    return-void
.end method

.method public a([Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/e;->a([Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 344
    new-instance v0, Lcom/google/googlenav/common/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/common/b;-><init>(Lcom/google/googlenav/common/Config;)V

    invoke-static {v0}, Lcom/google/googlenav/common/io/e;->a(Lcom/google/googlenav/common/io/f;)V

    .line 350
    return-void
.end method

.method public b([Ljava/lang/String;)[Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    .line 859
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->C()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 860
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 861
    array-length v2, p1

    const/4 v0, 0x0

    :goto_e
    if-ge v0, v2, :cond_1e

    aget-object v3, p1, v0

    .line 862
    invoke-static {v3}, Lcom/google/googlenav/common/e;->g(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 863
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 861
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 866
    :cond_1e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 868
    :goto_2a
    return-object v0

    :cond_2b
    move-object v0, p1

    goto :goto_2a
.end method

.method public c(I)I
    .registers 6
    .parameter

    .prologue
    .line 799
    int-to-double v0, p1

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/j;->a(D)I

    move-result v0

    return v0
.end method

.method protected c()Ljava/lang/String;
    .registers 4

    .prologue
    .line 360
    const-string v0, "Web"

    .line 361
    const-string v0, "maps_client_id"

    invoke-direct {p0, v0}, Lcom/google/googlenav/common/Config;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 365
    const-string v0, "Web"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    :goto_18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 367
    :cond_1d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_18
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    invoke-virtual {v0}, Lcom/google/googlenav/common/e;->c()Z

    move-result v0

    return v0
.end method

.method protected g()Lcom/google/googlenav/common/e;
    .registers 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->r:Lcom/google/googlenav/common/e;

    return-object v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 554
    const/16 v0, 0x22

    return v0
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 570
    const-string v0, "6.14.1"

    return-object v0
.end method

.method public l()Lcom/google/googlenav/common/io/g;
    .registers 2

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->c:Lcom/google/googlenav/common/io/g;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/common/io/j;
    .registers 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->j:Lcom/google/googlenav/common/io/j;

    return-object v0
.end method

.method public n()Lcom/google/googlenav/common/j;
    .registers 3

    .prologue
    .line 592
    new-instance v0, Lcom/google/googlenav/common/j;

    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/j;-><init>(Lcom/google/googlenav/common/io/j;)V

    return-object v0
.end method

.method public o()Lam/h;
    .registers 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->k:Lam/h;

    return-object v0
.end method

.method public p()Lam/b;
    .registers 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->l:Lam/b;

    return-object v0
.end method

.method public q()Lcom/google/googlenav/common/d;
    .registers 1

    .prologue
    .line 615
    return-object p0
.end method

.method public r()Ljava/lang/String;
    .registers 3

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 625
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_17

    .line 627
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_38

    .line 645
    :cond_17
    :goto_17
    const-string v0, "Unknown"

    :goto_19
    return-object v0

    .line 629
    :pswitch_1a
    const-string v0, "WiFi"

    goto :goto_19

    .line 631
    :pswitch_1d
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->d:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 633
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_40

    goto :goto_17

    .line 639
    :pswitch_2f
    const-string v0, "GPRS"

    goto :goto_19

    .line 635
    :pswitch_32
    const-string v0, "UMTS"

    goto :goto_19

    .line 637
    :pswitch_35
    const-string v0, "EDGE"

    goto :goto_19

    .line 627
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1a
    .end packed-switch

    .line 633
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_35
        :pswitch_32
    .end packed-switch
.end method

.method public s()I
    .registers 2

    .prologue
    .line 654
    iget v0, p0, Lcom/google/googlenav/common/Config;->e:I

    return v0
.end method

.method public t()F
    .registers 2

    .prologue
    .line 658
    iget v0, p0, Lcom/google/googlenav/common/Config;->n:F

    return v0
.end method

.method public u()F
    .registers 2

    .prologue
    .line 662
    iget v0, p0, Lcom/google/googlenav/common/Config;->o:F

    return v0
.end method

.method public v()Lcom/google/googlenav/common/a;
    .registers 2

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/googlenav/common/Config;->s:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method public x()D
    .registers 3

    .prologue
    .line 721
    iget v0, p0, Lcom/google/googlenav/common/Config;->m:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public y()Z
    .registers 3

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/google/googlenav/common/Config;->s()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public z()Z
    .registers 3

    .prologue
    .line 772
    sget-object v0, Lcom/google/googlenav/common/Config;->p:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method
