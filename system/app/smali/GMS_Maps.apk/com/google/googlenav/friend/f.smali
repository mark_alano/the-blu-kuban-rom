.class public Lcom/google/googlenav/friend/f;
.super Law/b;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private c:Z

.field private d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private e:Ljava/util/List;

.field private f:I


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Law/b;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/googlenav/friend/f;->a:Ljava/util/List;

    .line 88
    iput-object p2, p0, Lcom/google/googlenav/friend/f;->b:Ljava/util/List;

    .line 89
    iput-object p3, p0, Lcom/google/googlenav/friend/f;->e:Ljava/util/List;

    .line 90
    iput p4, p0, Lcom/google/googlenav/friend/f;->f:I

    .line 91
    return-void
.end method

.method private a(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 144
    sget-object v1, Lcom/google/googlenav/friend/g;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_16

    .line 156
    :goto_c
    :pswitch_c
    return v0

    .line 146
    :pswitch_d
    const/4 v0, 0x0

    goto :goto_c

    .line 148
    :pswitch_f
    const/4 v0, 0x1

    goto :goto_c

    .line 150
    :pswitch_11
    const/4 v0, 0x2

    goto :goto_c

    .line 152
    :pswitch_13
    const/4 v0, 0x3

    goto :goto_c

    .line 144
    nop

    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_d
        :pswitch_f
        :pswitch_11
        :pswitch_13
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 111
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 113
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbO/bi;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 114
    iget-object v0, p0, Lcom/google/googlenav/friend/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/reporting/k;

    .line 115
    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/k;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_17

    .line 118
    :cond_2b
    iget-object v0, p0, Lcom/google/googlenav/friend/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_31
    :goto_31
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    .line 119
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, LbO/G;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 120
    invoke-virtual {v0}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/googlenav/friend/f;->a(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;)I

    move-result v5

    .line 121
    invoke-virtual {v4, v8, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 122
    invoke-virtual {v0}, Lcom/google/android/location/clientlib/NlpActivity;->a()J

    move-result-wide v6

    invoke-virtual {v4, v9, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 123
    const/4 v0, -0x1

    if-eq v5, v0, :cond_31

    .line 124
    const/4 v0, 0x6

    invoke-virtual {v2, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_31

    .line 127
    :cond_5e
    const/4 v0, 0x3

    invoke-virtual {v2, v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 128
    iget-object v0, p0, Lcom/google/googlenav/friend/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_68
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 129
    const/4 v4, 0x4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    goto :goto_68

    .line 132
    :cond_7d
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/googlenav/friend/f;->f:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 133
    invoke-virtual {v2, v10, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 135
    invoke-virtual {v1, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 136
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 137
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 164
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->ao:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/f;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 165
    iget-object v0, p0, Lcom/google/googlenav/friend/f;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/google/googlenav/friend/f;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 170
    invoke-static {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    .line 173
    packed-switch v0, :pswitch_data_2c

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/f;->c:Z

    .line 183
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    .line 185
    :goto_28
    return v3

    .line 175
    :pswitch_29
    iput-boolean v3, p0, Lcom/google/googlenav/friend/f;->c:Z

    goto :goto_28

    .line 173
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_29
    .end packed-switch
.end method

.method public b()I
    .registers 2

    .prologue
    .line 98
    const/16 v0, 0x79

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic k()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/googlenav/friend/f;->n()Lcom/google/googlenav/friend/h;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/googlenav/friend/h;
    .registers 4

    .prologue
    .line 190
    new-instance v0, Lcom/google/googlenav/friend/h;

    invoke-direct {v0}, Lcom/google/googlenav/friend/h;-><init>()V

    .line 192
    iget-boolean v1, p0, Lcom/google/googlenav/friend/f;->c:Z

    iput-boolean v1, v0, Lcom/google/googlenav/friend/h;->a:Z

    .line 193
    iget-object v1, p0, Lcom/google/googlenav/friend/f;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/friend/h;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 195
    return-object v0
.end method

.method public s_()Z
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method
