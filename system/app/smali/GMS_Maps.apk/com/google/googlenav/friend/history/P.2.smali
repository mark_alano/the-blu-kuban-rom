.class public Lcom/google/googlenav/friend/history/P;
.super Lcom/google/googlenav/friend/history/O;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/googlenav/ui/view/dialog/aD;

.field private c:Ljava/util/List;

.field private d:Lcom/google/googlenav/ui/view/android/J;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/O;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/googlenav/friend/history/P;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    .line 63
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/google/googlenav/ui/view/android/J;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, p1, v2, v0, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/P;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    return-object p1
.end method

.method private a(Landroid/view/View;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const v5, 0x7f1002b7

    const v4, 0x7f1001ed

    const v3, 0x7f100026

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 182
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 183
    const v0, 0x7f1002ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 184
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 185
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 186
    packed-switch p2, :pswitch_data_6a

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "viewToShow is invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :pswitch_47
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    :goto_4e
    return-void

    .line 191
    :pswitch_4f
    const v0, 0x7f1002ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4e

    .line 194
    :pswitch_5a
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4e

    .line 197
    :pswitch_62
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4e

    .line 186
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_47
        :pswitch_4f
        :pswitch_5a
        :pswitch_62
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/P;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/P;)Lcom/google/googlenav/ui/view/android/J;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method private b()Ljava/util/List;
    .registers 8

    .prologue
    .line 217
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 222
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/Z;

    .line 224
    new-instance v3, Lcom/google/googlenav/friend/history/S;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/friend/history/S;-><init>(Lcom/google/googlenav/friend/history/P;Lcom/google/googlenav/friend/history/Z;)V

    .line 231
    new-instance v4, Lcom/google/googlenav/friend/history/T;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/friend/history/T;-><init>(Lcom/google/googlenav/friend/history/P;Lcom/google/googlenav/friend/history/Z;)V

    .line 240
    new-instance v5, Lcom/google/googlenav/friend/history/ac;

    iget-object v6, p0, Lcom/google/googlenav/friend/history/P;->a:Landroid/content/Context;

    invoke-direct {v5, v0, v6, v3, v4}, Lcom/google/googlenav/friend/history/ac;-><init>(Lcom/google/googlenav/friend/history/Z;Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 247
    :cond_2b
    return-object v1
.end method

.method private b(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 100
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    .line 101
    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 102
    iget-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 104
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->c(Landroid/view/View;)V

    return-void
.end method

.method private c(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 110
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    .line 111
    const v0, 0x7f1002b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 112
    const/16 v1, 0x26d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    const v0, 0x7f1002b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 114
    const/16 v1, 0x26c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 115
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->f(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/P;)Z
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/google/googlenav/friend/history/P;)Lcom/google/googlenav/ui/view/dialog/aD;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 121
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    .line 122
    const v0, 0x7f1002bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 123
    const/16 v1, 0x273

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->e(Landroid/view/View;)V

    return-void
.end method

.method private e(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 130
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/friend/history/P;->a(Landroid/view/View;I)V

    .line 131
    const v0, 0x7f1001ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 132
    const/16 v1, 0x25b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void
.end method

.method private f(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    new-instance v1, Lcom/google/googlenav/friend/history/Q;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/friend/history/Q;-><init>(Lcom/google/googlenav/friend/history/P;Landroid/view/View;)V

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aD;->a(Lcom/google/googlenav/ui/view/dialog/aG;)V

    .line 171
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    .line 210
    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->c:Ljava/util/List;

    if-nez v0, :cond_12

    .line 77
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    if-nez v0, :cond_11

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/P;->e:Z

    .line 80
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->e(Landroid/view/View;)V

    .line 81
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->f(Landroid/view/View;)V

    .line 94
    :cond_11
    :goto_11
    return-void

    .line 85
    :cond_12
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/P;->b()Ljava/util/List;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 87
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->d(Landroid/view/View;)V

    goto :goto_11

    .line 89
    :cond_20
    iget-object v1, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    .line 90
    iget-object v0, p0, Lcom/google/googlenav/friend/history/P;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    .line 91
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/P;->b(Landroid/view/View;)V

    goto :goto_11
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 70
    const/16 v0, 0x28c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 256
    if-ne p0, p1, :cond_4

    .line 257
    const/4 v0, 0x1

    .line 263
    :goto_3
    return v0

    .line 259
    :cond_4
    if-nez p1, :cond_8

    .line 260
    const/4 v0, 0x0

    goto :goto_3

    .line 263
    :cond_8
    instance-of v0, p1, Lcom/google/googlenav/friend/history/P;

    goto :goto_3
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 272
    const/16 v0, 0x2a

    return v0
.end method
