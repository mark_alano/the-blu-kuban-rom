.class public Lcom/google/googlenav/friend/history/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/Z;

.field private final b:Landroid/content/Context;

.field private c:Landroid/view/View$OnClickListener;

.field private d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/Z;Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    .line 54
    iput-object p2, p0, Lcom/google/googlenav/friend/history/ac;->b:Landroid/content/Context;

    .line 55
    iput-object p3, p0, Lcom/google/googlenav/friend/history/ac;->c:Landroid/view/View$OnClickListener;

    .line 56
    iput-object p4, p0, Lcom/google/googlenav/friend/history/ac;->d:Landroid/view/View$OnClickListener;

    .line 57
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 103
    new-instance v1, Lcom/google/googlenav/friend/history/ae;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/history/ae;-><init>(Lcom/google/googlenav/friend/history/ad;)V

    .line 104
    const v0, 0x7f1000ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 105
    const v0, 0x7f100292

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->b(Lcom/google/googlenav/friend/history/ae;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 106
    const v0, 0x7f10026c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->c(Lcom/google/googlenav/friend/history/ae;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 107
    const v0, 0x7f100258

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 108
    const v0, 0x7f100257

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;

    .line 109
    const v0, 0x7f100051

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 110
    const v0, 0x7f10026d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;Landroid/view/View;)Landroid/view/View;

    .line 112
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 61
    check-cast p2, Lcom/google/googlenav/friend/history/ae;

    .line 62
    iget-object v0, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/Z;->p()Z

    move-result v2

    .line 64
    iget-object v0, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/Z;->n()Z

    move-result v0

    if-nez v0, :cond_94

    iget-object v0, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/Z;->o()Z

    move-result v0

    if-nez v0, :cond_94

    const/4 v0, 0x1

    :goto_1c
    const-string v3, "Work and home are not supposed to be in this list!"

    invoke-static {v0, v3}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    .line 68
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->a(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/Z;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->b(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/Z;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/Z;->l()Ljava/lang/String;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_96

    if-eqz v2, :cond_96

    .line 73
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->c(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->c(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :goto_53
    if-eqz v2, :cond_9e

    .line 81
    const v0, 0x7f0203ff

    .line 85
    :goto_58
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->d(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/friend/history/ac;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->e(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/friend/history/ac;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->f(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/friend/history/ac;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/google/googlenav/friend/history/ac;->a:Lcom/google/googlenav/friend/history/Z;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/Z;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_85

    if-eqz v2, :cond_a2

    .line 93
    :cond_85
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->g(Lcom/google/googlenav/friend/history/ae;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->e(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 99
    :goto_93
    return-void

    :cond_94
    move v0, v1

    .line 64
    goto :goto_1c

    .line 76
    :cond_96
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->c(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_53

    .line 83
    :cond_9e
    const v0, 0x7f020419

    goto :goto_58

    .line 96
    :cond_a2
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->g(Lcom/google/googlenav/friend/history/ae;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    invoke-static {p2}, Lcom/google/googlenav/friend/history/ae;->e(Lcom/google/googlenav/friend/history/ae;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_93
.end method

.method public b()I
    .registers 2

    .prologue
    .line 122
    const v0, 0x7f0400d4

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method
