.class public Lcom/google/googlenav/friend/history/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Ljava/util/Calendar;


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 39
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 40
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 41
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 43
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    .line 44
    iget-object v3, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->clear()V

    .line 45
    iget-object v3, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v1, v2, v0}, Ljava/util/Calendar;->set(III)V

    .line 46
    return-void
.end method

.method public constructor <init>(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    .line 57
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 58
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 59
    return-void
.end method

.method public constructor <init>(J)V
    .registers 4
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    .line 70
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 71
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 72
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    .line 80
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 83
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 85
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 87
    iget-object v3, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->set(III)V

    .line 88
    return-void
.end method

.method public static a()Lcom/google/googlenav/friend/history/b;
    .registers 2

    .prologue
    .line 30
    new-instance v0, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/history/b;-><init>()V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 239
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-lez v0, :cond_e

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startDay must be <= endDay"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_e
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 245
    :goto_12
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 246
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object p0

    goto :goto_12

    .line 249
    :cond_21
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/friend/history/b;)I
    .registers 4
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    iget-object v1, p1, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    return v0
.end method

.method public a(I)Lcom/google/googlenav/friend/history/b;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x5

    .line 161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 163
    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 165
    invoke-virtual {v0, v4, p1}, Ljava/util/Calendar;->add(II)V

    .line 166
    new-instance v1, Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/googlenav/friend/history/b;-><init>(III)V

    return-object v1
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 118
    new-instance v0, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/history/b;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 119
    const/16 v0, 0x289

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    :goto_11
    return-object v0

    .line 120
    :cond_12
    new-instance v0, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/history/b;-><init>()V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 121
    const/16 v0, 0x28d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 123
    :cond_29
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11
.end method

.method public b(Lcom/google/googlenav/friend/history/b;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v1

    if-nez v1, :cond_8

    .line 193
    :cond_7
    :goto_7
    return v0

    .line 184
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v1

    if-lez v1, :cond_13

    .line 185
    invoke-virtual {p1, p0}, Lcom/google/googlenav/friend/history/b;->b(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    goto :goto_7

    .line 190
    :cond_13
    :goto_13
    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_13
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 95
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/aN;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 96
    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 99
    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 102
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 105
    return-object v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 130
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .registers 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 22
    check-cast p1, Lcom/google/googlenav/friend/history/b;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    return v0
.end method

.method public d()J
    .registers 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()I
    .registers 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 135
    instance-of v0, p1, Lcom/google/googlenav/friend/history/b;

    if-nez v0, :cond_6

    .line 136
    const/4 v0, 0x0

    .line 140
    :goto_5
    return v0

    .line 138
    :cond_6
    check-cast p1, Lcom/google/googlenav/friend/history/b;

    .line 140
    iget-object v0, p1, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public f()I
    .registers 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public g()I
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .registers 5

    .prologue
    .line 257
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 258
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 110
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM/dd/yy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/google/googlenav/friend/history/b;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
