.class public Lcom/google/googlenav/friend/history/g;
.super Law/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:Z

.field private final c:I

.field private final d:I

.field private final e:Lcom/google/googlenav/friend/history/h;


# direct methods
.method public constructor <init>(IILcom/google/googlenav/friend/history/h;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0}, Law/b;-><init>()V

    .line 103
    iput p1, p0, Lcom/google/googlenav/friend/history/g;->c:I

    .line 104
    iput p2, p0, Lcom/google/googlenav/friend/history/g;->d:I

    .line 105
    iput-object p3, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    .line 106
    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 126
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->av:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 128
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/aN;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 129
    iget v2, p0, Lcom/google/googlenav/friend/history/g;->c:I

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 130
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/googlenav/friend/history/g;->d:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 131
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 132
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 133
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 140
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 141
    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 145
    iget-object v1, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 146
    invoke-static {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    .line 149
    packed-switch v0, :pswitch_data_2c

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    .line 157
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    .line 160
    :goto_28
    return v3

    .line 151
    :pswitch_29
    iput-boolean v3, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    goto :goto_28

    .line 149
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_29
    .end packed-switch
.end method

.method public b()I
    .registers 2

    .prologue
    .line 113
    const/16 v0, 0x8b

    return v0
.end method

.method public d_()V
    .registers 3

    .prologue
    .line 165
    invoke-super {p0}, Law/b;->d_()V

    .line 166
    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    if-nez v0, :cond_8

    .line 175
    :goto_7
    return-void

    .line 170
    :cond_8
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    if-eqz v0, :cond_14

    .line 171
    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/history/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_7

    .line 173
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    invoke-interface {v0}, Lcom/google/googlenav/friend/history/h;->a()V

    goto :goto_7
.end method

.method protected synthetic k()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/g;->n()Lcom/google/googlenav/friend/history/i;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lcom/google/googlenav/friend/history/i;
    .registers 4

    .prologue
    .line 179
    new-instance v0, Lcom/google/googlenav/friend/history/i;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    iget-object v2, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/history/i;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public s_()Z
    .registers 2

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method
