.class public Lcom/google/googlenav/friend/history/j;
.super Lcom/google/googlenav/friend/history/x;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/U;

.field private d:Lcom/google/googlenav/ui/view/android/J;

.field private final e:Lcom/google/googlenav/ui/view/dialog/aD;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;Lcom/google/googlenav/friend/history/U;Lcom/google/googlenav/ui/view/dialog/aD;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/friend/history/x;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    .line 63
    iput-object p3, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    .line 64
    iput-object p4, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    .line 65
    return-void
.end method

.method private static a(Lcom/google/googlenav/friend/history/U;Landroid/content/Context;)Lbe/o;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->b()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/b;->h()Ljava/lang/String;

    move-result-object v1

    .line 203
    const/16 v0, 0x269

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    new-instance v0, Lcom/google/googlenav/friend/history/W;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    .line 207
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 208
    new-instance v3, Lbe/o;

    invoke-direct {v3, v1, v2, v4, v0}, Lbe/o;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v3
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/j;)Lcom/google/googlenav/ui/view/android/J;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/friend/history/c;)Z
    .registers 4
    .parameter

    .prologue
    .line 193
    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/history/p;->a:Lcom/google/googlenav/friend/history/p;

    if-eq v0, v1, :cond_10

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    if-ne v0, v1, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/j;)Lcom/google/googlenav/ui/view/dialog/aD;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/J;->notifyDataSetChanged()V

    .line 113
    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/16 v1, 0x8

    const/4 v6, 0x0

    .line 69
    const v0, 0x7f1001ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    const v0, 0x7f1002ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 72
    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 73
    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 76
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    iget-object v3, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/U;Landroid/content/Context;)Lbe/o;

    move-result-object v2

    .line 78
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/j;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 81
    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v7, v1, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v3, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    .line 85
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 86
    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 87
    iget-object v1, p0, Lcom/google/googlenav/friend/history/j;->d:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 91
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->e:Lcom/google/googlenav/ui/view/dialog/aD;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/friend/history/k;

    invoke-direct {v3, p0, v2}, Lcom/google/googlenav/friend/history/k;-><init>(Lcom/google/googlenav/friend/history/j;Lbe/o;)V

    invoke-interface {v0, v1, v3}, Lcom/google/googlenav/ui/view/dialog/aD;->a(Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/aF;)V

    .line 105
    return-void
.end method

.method b()Ljava/util/List;
    .registers 14

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v2, -0x1

    .line 121
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v10

    .line 122
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v3

    move v1, v7

    .line 126
    :goto_e
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_ad

    .line 127
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/c;)Z

    move-result v0

    if-eqz v0, :cond_28

    move v12, v1

    .line 135
    :goto_21
    if-ne v12, v2, :cond_2c

    .line 136
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 186
    :goto_27
    return-object v0

    .line 126
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 141
    :cond_2c
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_32
    if-ltz v1, :cond_ab

    .line 142
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/history/j;->a(Lcom/google/googlenav/friend/history/c;)Z

    move-result v0

    if-eqz v0, :cond_69

    move v8, v1

    :goto_41
    move v11, v12

    .line 148
    :goto_42
    if-gt v11, v8, :cond_a8

    .line 149
    iget-object v0, p0, Lcom/google/googlenav/friend/history/j;->a:Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/c;

    .line 150
    if-ne v11, v12, :cond_6c

    move v3, v9

    .line 151
    :goto_53
    if-ne v11, v8, :cond_6e

    move v4, v9

    .line 152
    :goto_56
    sget-object v0, Lcom/google/googlenav/friend/history/n;->a:[I

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/p;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_b0

    .line 148
    :goto_65
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_42

    .line 141
    :cond_69
    add-int/lit8 v1, v1, -0x1

    goto :goto_32

    :cond_6c
    move v3, v7

    .line 150
    goto :goto_53

    :cond_6e
    move v4, v7

    .line 151
    goto :goto_56

    .line 154
    :pswitch_70
    new-instance v0, Lbe/k;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3, v4}, Lbe/k;-><init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_65

    .line 157
    :pswitch_7b
    new-instance v5, Lcom/google/googlenav/friend/history/l;

    invoke-direct {v5, p0, v1}, Lcom/google/googlenav/friend/history/l;-><init>(Lcom/google/googlenav/friend/history/j;Lcom/google/googlenav/friend/history/c;)V

    .line 164
    new-instance v6, Lcom/google/googlenav/friend/history/m;

    invoke-direct {v6, p0, v1}, Lcom/google/googlenav/friend/history/m;-><init>(Lcom/google/googlenav/friend/history/j;Lcom/google/googlenav/friend/history/c;)V

    .line 171
    new-instance v0, Lbe/x;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/j;->c:Landroid/content/Context;

    invoke-direct/range {v0 .. v6}, Lbe/x;-><init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZLandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_65

    .line 179
    :pswitch_90
    new-instance v0, Lbe/D;

    invoke-direct {v0, v1}, Lbe/D;-><init>(Lcom/google/googlenav/friend/history/c;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_65

    .line 182
    :pswitch_99
    new-instance v0, Lbe/r;

    const/16 v1, 0x30d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbe/r;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_65

    :cond_a8
    move-object v0, v10

    .line 186
    goto/16 :goto_27

    :cond_ab
    move v8, v2

    goto :goto_41

    :cond_ad
    move v12, v2

    goto/16 :goto_21

    .line 152
    :pswitch_data_b0
    .packed-switch 0x1
        :pswitch_70
        :pswitch_7b
        :pswitch_90
        :pswitch_99
    .end packed-switch
.end method
