.class public abstract Lcom/google/googlenav/friend/history/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected final b:Lcom/google/googlenav/friend/history/b;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 85
    new-instance v0, Lcom/google/googlenav/friend/history/b;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->f()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/history/b;-><init>(J)V

    iput-object v0, p0, Lcom/google/googlenav/friend/history/o;->b:Lcom/google/googlenav/friend/history/b;

    .line 86
    iput-object p2, p0, Lcom/google/googlenav/friend/history/o;->d:Landroid/content/Context;

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 75
    iput-object p2, p0, Lcom/google/googlenav/friend/history/o;->b:Lcom/google/googlenav/friend/history/b;

    .line 76
    iput-object p3, p0, Lcom/google/googlenav/friend/history/o;->d:Landroid/content/Context;

    .line 77
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 294
    return-void
.end method

.method public c()Lcom/google/googlenav/friend/history/p;
    .registers 3

    .prologue
    const/4 v1, 0x6

    .line 93
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 95
    sget-object v0, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    .line 101
    :goto_18
    return-object v0

    .line 96
    :cond_19
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 97
    sget-object v0, Lcom/google/googlenav/friend/history/p;->a:Lcom/google/googlenav/friend/history/p;

    goto :goto_18

    .line 98
    :cond_25
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 99
    sget-object v0, Lcom/google/googlenav/friend/history/p;->c:Lcom/google/googlenav/friend/history/p;

    goto :goto_18

    .line 101
    :cond_31
    sget-object v0, Lcom/google/googlenav/friend/history/p;->d:Lcom/google/googlenav/friend/history/p;

    goto :goto_18
.end method

.method public d()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 109
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->m()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 112
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->j()Z

    move-result v0

    if-eqz v0, :cond_29

    const/16 v0, 0x26f

    :goto_18
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_28
    return-object v0

    .line 112
    :cond_29
    const/16 v0, 0x26e

    goto :goto_18

    .line 119
    :cond_2c
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_37

    .line 120
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 122
    :cond_37
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->n()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 123
    const/16 v0, 0x1e0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 126
    :cond_44
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->o()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 127
    const/16 v0, 0x619

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 131
    :cond_51
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_63

    .line 132
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 135
    :cond_63
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_28
.end method

.method public e()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->g()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->f()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/bd;->a(IZ)Ljava/lang/String;

    move-result-object v1

    .line 146
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 147
    const/16 v0, 0x28a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-ne v2, v6, :cond_32

    .line 150
    const/16 v0, 0x270

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    :cond_32
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()J
    .registers 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .registers 5

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->f()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .registers 5

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 182
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 192
    iget-object v1, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_13

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public k()Ljava/lang/String;
    .registers 3

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 6

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/googlenav/friend/history/V;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    .line 215
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 216
    const/4 v0, 0x0

    .line 225
    :goto_d
    return-object v0

    .line 222
    :cond_e
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->g()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v3, v3

    rem-long/2addr v1, v3

    long-to-int v1, v1

    .line 225
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/V;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/V;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method public m()Z
    .registers 3

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public n()Z
    .registers 4

    .prologue
    const/4 v2, 0x5

    .line 236
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 237
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public o()Z
    .registers 4

    .prologue
    const/4 v2, 0x6

    .line 241
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 242
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public p()Z
    .registers 5

    .prologue
    const/4 v3, 0x7

    const/4 v0, 0x1

    .line 246
    iget-object v1, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_7

    .line 250
    :cond_6
    :goto_6
    return v0

    .line 249
    :cond_7
    iget-object v1, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 250
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_1a
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 259
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 260
    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 261
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 262
    array-length v0, v0

    if-lez v0, :cond_2e

    .line 263
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 266
    :cond_2e
    return-object v1
.end method

.method public r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_7

    .line 276
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 283
    :goto_6
    return-object v0

    .line 279
    :cond_7
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_16

    .line 280
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_6

    .line 283
    :cond_16
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public t()Lcom/google/googlenav/friend/history/b;
    .registers 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->b:Lcom/google/googlenav/friend/history/b;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .registers 3

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()LaH/h;
    .registers 10

    .prologue
    const-wide v7, 0x416312d000000000L

    .line 319
    iget-object v0, p0, Lcom/google/googlenav/friend/history/o;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 320
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 322
    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v7

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-double v5, v1

    div-double/2addr v5, v7

    invoke-virtual {v2, v3, v4, v5, v6}, LaH/j;->a(DD)LaH/j;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    invoke-virtual {v1, v0}, LaH/j;->a(F)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    return-object v0
.end method
