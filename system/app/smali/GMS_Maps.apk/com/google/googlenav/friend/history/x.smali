.class public abstract Lcom/google/googlenav/friend/history/x;
.super Lcom/google/googlenav/friend/history/O;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field protected final b:Lcom/google/googlenav/friend/history/b;

.field protected c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/O;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    .line 25
    iput-object p2, p0, Lcom/google/googlenav/friend/history/x;->c:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/friend/history/x;)I
    .registers 4
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p1, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    return v0
.end method

.method public c()Lcom/google/googlenav/friend/history/b;
    .registers 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 14
    check-cast p1, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/x;->a(Lcom/google/googlenav/friend/history/x;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .registers 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/x;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 53
    if-ne p0, p1, :cond_5

    .line 54
    const/4 v0, 0x1

    .line 64
    :cond_4
    :goto_4
    return v0

    .line 56
    :cond_5
    if-eqz p1, :cond_4

    .line 59
    instance-of v1, p1, Lcom/google/googlenav/friend/history/x;

    if-eqz v1, :cond_4

    .line 63
    check-cast p1, Lcom/google/googlenav/friend/history/x;

    .line 64
    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v1, p1, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/googlenav/friend/history/x;->b:Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/b;->hashCode()I

    move-result v0

    return v0
.end method
