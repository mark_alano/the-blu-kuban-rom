.class public Lcom/google/googlenav/friend/history/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/v;

.field private final b:Lcom/google/googlenav/friend/history/q;

.field private final c:Lcom/google/googlenav/friend/history/W;

.field private final d:Lcom/google/googlenav/friend/reporting/s;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/v;Lcom/google/googlenav/friend/history/q;Lcom/google/googlenav/friend/history/W;Lcom/google/googlenav/friend/reporting/s;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    .line 90
    iput-object p3, p0, Lcom/google/googlenav/friend/history/z;->b:Lcom/google/googlenav/friend/history/q;

    .line 91
    iput-object p4, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    .line 92
    iput-object p5, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/z;)Lcom/google/googlenav/friend/history/v;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/z;
    .registers 7
    .parameter

    .prologue
    .line 69
    invoke-static {p0}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/google/googlenav/friend/reporting/f;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    .line 71
    invoke-static {p0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v5

    .line 74
    new-instance v0, Lcom/google/googlenav/friend/history/z;

    new-instance v2, Lcom/google/googlenav/friend/history/v;

    invoke-static {p0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/friend/history/v;-><init>(Landroid/content/Context;Lbm/c;)V

    new-instance v3, Lcom/google/googlenav/friend/history/q;

    invoke-direct {v3}, Lcom/google/googlenav/friend/history/q;-><init>()V

    new-instance v4, Lcom/google/googlenav/friend/history/W;

    invoke-direct {v4, p0}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/z;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/v;Lcom/google/googlenav/friend/history/q;Lcom/google/googlenav/friend/history/W;Lcom/google/googlenav/friend/reporting/s;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 462
    :try_start_1
    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->b:Lcom/google/googlenav/friend/history/q;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/friend/history/q;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_6} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_f

    move-result-object v0

    .line 468
    :goto_7
    return-object v0

    .line 463
    :catch_8
    move-exception v1

    .line 464
    const-string v2, "LocationHistory.LocationHistoryManager"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 466
    :catch_f
    move-exception v1

    .line 467
    const-string v2, "LocationHistory.LocationHistoryManager"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7
.end method

.method static synthetic b(Lcom/google/googlenav/friend/history/z;)Lcom/google/googlenav/friend/reporting/s;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    return-object v0
.end method

.method private c()Z
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 517
    new-instance v0, Lcom/google/googlenav/friend/S;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    .line 520
    invoke-virtual {v0}, Lcom/google/googlenav/friend/S;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/T;

    .line 521
    if-eqz v0, :cond_1c

    iget-boolean v2, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-nez v2, :cond_1e

    .line 522
    :cond_1c
    const/4 v0, 0x0

    .line 531
    :goto_1d
    return v0

    .line 525
    :cond_1e
    new-instance v2, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 527
    new-instance v0, Lcom/google/googlenav/friend/ae;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 529
    iget-object v2, p0, Lcom/google/googlenav/friend/history/z;->d:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/ae;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/reporting/s;->a(Z)V

    .line 530
    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(Lcom/google/googlenav/friend/ae;)V

    move v0, v1

    .line 531
    goto :goto_1d
.end method

.method static synthetic c(Lcom/google/googlenav/friend/history/z;)Z
    .registers 2
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/googlenav/friend/history/z;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .registers 4
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/history/v;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_9

    .line 238
    :cond_8
    :goto_8
    return-object v0

    .line 233
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/z;->b(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_8

    .line 235
    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v1, p1, v0}, Lcom/google/googlenav/friend/history/v;->a(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;)Z

    goto :goto_8
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/history/U;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x5

    .line 495
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 498
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 499
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 500
    new-instance v3, Lcom/google/googlenav/friend/history/c;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    invoke-direct {v3, v0, p1, v4}, Lcom/google/googlenav/friend/history/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 503
    :cond_29
    const/4 v0, 0x0

    .line 504
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 505
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    .line 508
    :cond_34
    new-instance v2, Lcom/google/googlenav/friend/history/U;

    const/4 v3, 0x3

    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3, v1, v0}, Lcom/google/googlenav/friend/history/U;-><init>(Lcom/google/googlenav/friend/history/b;Ljava/lang/String;Ljava/util/List;F)V

    return-object v2
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Landroid/util/Pair;Z)Lcom/google/googlenav/friend/history/f;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 437
    new-instance v0, Lcom/google/googlenav/friend/history/d;

    iget-object v1, p3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, p3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/friend/history/d;-><init>(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;IIZLcom/google/googlenav/friend/history/e;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/d;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/f;

    .line 442
    return-object v0
.end method

.method a(Landroid/util/Pair;)Lcom/google/googlenav/friend/history/i;
    .registers 6
    .parameter

    .prologue
    .line 448
    new-instance v1, Lcom/google/googlenav/friend/history/g;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/googlenav/friend/history/g;-><init>(IILcom/google/googlenav/friend/history/h;)V

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/g;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/i;

    .line 451
    return-object v0
.end method

.method a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 478
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 479
    invoke-static {p1, p2}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/b;

    .line 480
    iget-object v3, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/friend/history/v;->b(Lcom/google/googlenav/friend/history/b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 481
    if-eqz v3, :cond_c

    .line 482
    invoke-virtual {p0, v0, v3}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/history/U;

    move-result-object v0

    .line 484
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 487
    :cond_28
    return-object v1
.end method

.method public a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Z)Ljava/util/List;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 357
    invoke-static {p1, p2}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v2

    .line 359
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    .line 365
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_15

    .line 397
    :goto_14
    return-object v0

    .line 370
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    .line 371
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Landroid/util/Pair;Z)Lcom/google/googlenav/friend/history/f;

    move-result-object v0

    .line 375
    if-eqz v0, :cond_27

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/f;->a()Z

    move-result v2

    if-nez v2, :cond_29

    :cond_27
    move-object v0, v1

    .line 377
    goto :goto_14

    .line 380
    :cond_29
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/f;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 382
    if-nez v0, :cond_36

    move-object v0, v1

    .line 384
    goto :goto_14

    .line 389
    :cond_36
    invoke-static {v0, v7}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 392
    array-length v2, v1

    const/4 v0, 0x0

    :goto_3c
    if-ge v0, v2, :cond_51

    aget-object v3, v1, v0

    .line 393
    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->a:Lcom/google/googlenav/friend/history/v;

    new-instance v5, Lcom/google/googlenav/friend/history/b;

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/googlenav/friend/history/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v4, v5, v3}, Lcom/google/googlenav/friend/history/v;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 397
    :cond_51
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    goto :goto_14
.end method

.method public a()V
    .registers 3

    .prologue
    .line 153
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    .line 154
    new-instance v1, Lcom/google/googlenav/friend/history/B;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/friend/history/B;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;)V

    .line 160
    invoke-virtual {v1}, Las/b;->g()V

    .line 161
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/N;)V
    .registers 4
    .parameter

    .prologue
    .line 314
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    .line 315
    new-instance v1, Lcom/google/googlenav/friend/history/J;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/googlenav/friend/history/J;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/N;)V

    .line 340
    invoke-virtual {v1}, Las/b;->g()V

    .line 341
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;)V
    .registers 4
    .parameter

    .prologue
    .line 142
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    .line 143
    new-instance v1, Lcom/google/googlenav/friend/history/A;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/googlenav/friend/history/A;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/b;)V

    .line 149
    invoke-virtual {v1}, Las/b;->g()V

    .line 150
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/L;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    .line 248
    new-instance v0, Lcom/google/googlenav/friend/history/F;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/F;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Lcom/google/googlenav/friend/history/L;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    .line 305
    invoke-virtual {v0}, Las/b;->g()V

    .line 306
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/friend/history/M;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/google/googlenav/friend/history/C;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/googlenav/friend/history/C;-><init>(Lcom/google/googlenav/friend/history/z;Las/c;Ljava/lang/String;Lcom/google/googlenav/friend/history/M;)V

    .line 217
    invoke-virtual {v1}, Las/b;->g()V

    .line 218
    return-void
.end method

.method public b()Ljava/util/List;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 404
    iget-object v1, p0, Lcom/google/googlenav/friend/history/z;->c:Lcom/google/googlenav/friend/history/W;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v1

    .line 405
    invoke-virtual {p0, v1}, Lcom/google/googlenav/friend/history/z;->a(Landroid/util/Pair;)Lcom/google/googlenav/friend/history/i;

    move-result-object v1

    .line 408
    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/i;->a()Z

    move-result v2

    if-nez v2, :cond_14

    .line 430
    :cond_13
    :goto_13
    return-object v0

    .line 413
    :cond_14
    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/i;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 415
    if-eqz v1, :cond_13

    .line 422
    const/4 v0, 0x1

    invoke-static {v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 424
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 426
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_30
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 427
    new-instance v3, Lcom/google/googlenav/friend/history/Z;

    iget-object v4, p0, Lcom/google/googlenav/friend/history/z;->e:Landroid/content/Context;

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/friend/history/Z;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_30

    :cond_47
    move-object v0, v1

    .line 430
    goto :goto_13
.end method
