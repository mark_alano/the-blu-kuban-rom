.class public Lcom/google/googlenav/friend/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z

.field private static final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# instance fields
.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 20
    const-class v0, Lcom/google/googlenav/friend/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/googlenav/friend/i;->a:Z

    .line 28
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/googlenav/friend/i;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void

    .line 20
    :cond_15
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 35
    iput-object p1, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 36
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/i;->d:J

    .line 49
    return-void
.end method

.method private f()V
    .registers 1

    .prologue
    .line 269
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/google/googlenav/friend/i;->d:J

    return-wide v0
.end method

.method public a(Lcom/google/googlenav/ct;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x5

    const/4 v5, 0x4

    .line 191
    iget-object v0, p1, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_e

    .line 192
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p1, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 196
    :cond_e
    iget-object v0, p1, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_35

    .line 197
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    move v0, v2

    .line 198
    :goto_19
    if-ge v0, v1, :cond_35

    .line 199
    iget-object v3, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 201
    iget-object v4, p1, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v4, v3}, Lcom/google/googlenav/az;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 202
    iget-object v3, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 203
    add-int/lit8 v0, v0, -0x1

    .line 204
    add-int/lit8 v1, v1, -0x1

    .line 198
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 210
    :cond_35
    iget-object v0, p1, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_40

    .line 211
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p1, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 215
    :cond_40
    iget-object v0, p1, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_67

    .line 216
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    move v0, v2

    .line 217
    :goto_4b
    if-ge v0, v1, :cond_67

    .line 218
    iget-object v2, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 220
    iget-object v3, p1, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v3, v2}, Lcom/google/googlenav/az;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-eqz v2, :cond_64

    .line 221
    iget-object v2, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 222
    add-int/lit8 v0, v0, -0x1

    .line 223
    add-int/lit8 v1, v1, -0x1

    .line 217
    :cond_64
    add-int/lit8 v0, v0, 0x1

    goto :goto_4b

    .line 228
    :cond_67
    iget-object v0, p1, Lcom/google/googlenav/ct;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_74

    .line 229
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/googlenav/ct;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 231
    :cond_74
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 66
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v1, v0

    .line 111
    :goto_9
    if-ge v1, v2, :cond_18

    .line 112
    iget-object v3, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 114
    invoke-static {v3, p1}, Lcom/google/googlenav/az;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 115
    const/4 v0, 0x1

    .line 118
    :cond_18
    return v0

    .line 111
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_9
.end method

.method public b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 99
    invoke-direct {p0}, Lcom/google/googlenav/friend/i;->f()V

    .line 100
    return-void
.end method

.method public b()Z
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 59
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    :goto_f
    return v0

    :cond_10
    sget-object v0, Lcom/google/googlenav/friend/i;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    goto :goto_f
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 144
    iget-object v1, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v1, v0

    .line 145
    :goto_9
    if-ge v1, v2, :cond_18

    .line 146
    iget-object v3, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 148
    invoke-static {v3, p1}, Lcom/google/googlenav/az;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 149
    const/4 v0, 0x1

    .line 152
    :cond_18
    return v0

    .line 145
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_9
.end method

.method public c()Z
    .registers 3

    .prologue
    const/4 v1, 0x3

    .line 85
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 86
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    .line 91
    :goto_f
    return v0

    :cond_10
    sget-object v0, Lcom/google/googlenav/friend/i;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    goto :goto_f
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/googlenav/friend/i;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method
