.class public Lcom/google/googlenav/friend/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/h;
.implements Lcom/google/googlenav/bb;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Lcom/google/googlenav/friend/o;

.field private static final b:Ljava/lang/Runnable;

.field private static d:Lcom/google/googlenav/ui/s;

.field private static volatile g:Z


# instance fields
.field private final c:Las/d;

.field private e:Z

.field private volatile f:Z

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/googlenav/friend/o;

    invoke-direct {v0}, Lcom/google/googlenav/friend/o;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    .line 49
    new-instance v0, Lcom/google/googlenav/friend/q;

    invoke-direct {v0}, Lcom/google/googlenav/friend/q;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/p;->b:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Las/c;Lcom/google/googlenav/ui/s;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    .line 91
    new-instance v0, Las/d;

    invoke-direct {v0, p1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    .line 92
    sput-object p2, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    .line 93
    return-void
.end method

.method public static a(Lcom/google/googlenav/bb;)V
    .registers 5
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 252
    new-instance v0, Lcom/google/googlenav/friend/r;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/r;-><init>(Lcom/google/googlenav/bb;)V

    .line 276
    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-static {}, LaN/H;->i()LaN/H;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 281
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 282
    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(Z)Lcom/google/googlenav/bg;

    .line 286
    :cond_2e
    sget-object v1, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/o;->a()I

    move-result v1

    .line 287
    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->g(I)Lcom/google/googlenav/bg;

    .line 289
    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    .line 290
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 293
    invoke-static {v3}, Lcom/google/googlenav/friend/p;->b(Z)V

    .line 294
    return-void
.end method

.method static synthetic a(Z)V
    .registers 1
    .parameter

    .prologue
    .line 27
    invoke-static {p0}, Lcom/google/googlenav/friend/p;->b(Z)V

    return-void
.end method

.method private static b(Z)V
    .registers 1
    .parameter

    .prologue
    .line 297
    sput-boolean p0, Lcom/google/googlenav/friend/p;->g:Z

    .line 298
    invoke-static {}, Lcom/google/googlenav/friend/p;->l()V

    .line 299
    return-void
.end method

.method public static e()Z
    .registers 1

    .prologue
    .line 317
    sget-boolean v0, Lcom/google/googlenav/friend/p;->g:Z

    return v0
.end method

.method public static g()Lcom/google/googlenav/friend/o;
    .registers 1

    .prologue
    .line 322
    sget-object v0, Lcom/google/googlenav/friend/p;->a:Lcom/google/googlenav/friend/o;

    return-object v0
.end method

.method static synthetic h()Lcom/google/googlenav/ui/s;
    .registers 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    .line 101
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    .line 102
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    const-wide/32 v1, 0x1d4c0

    .line 108
    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    .line 109
    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1, v2}, Las/d;->c(J)V

    .line 110
    iget-object v0, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    .line 111
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 117
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/friend/p;->c:Las/d;

    invoke-virtual {v0, v1}, Las/c;->b(Las/a;)I

    .line 118
    return-void
.end method

.method private static l()V
    .registers 3

    .prologue
    .line 309
    sget-object v0, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_10

    .line 310
    const/4 v0, 0x1

    .line 311
    sget-object v1, Lcom/google/googlenav/friend/p;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/friend/p;->b:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 313
    :cond_10
    return-void
.end method


# virtual methods
.method public C_()V
    .registers 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->i()V

    .line 196
    return-void
.end method

.method public D_()V
    .registers 1

    .prologue
    .line 214
    return-void
.end method

.method public E_()V
    .registers 2

    .prologue
    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    .line 223
    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 224
    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    if-nez v0, :cond_11

    .line 226
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->i()V

    .line 237
    :cond_10
    :goto_10
    return-void

    .line 232
    :cond_11
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    .line 234
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->j()V

    goto :goto_10
.end method

.method public M_()V
    .registers 2

    .prologue
    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    .line 203
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    .line 204
    return-void
.end method

.method public N_()V
    .registers 2

    .prologue
    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    .line 218
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    .line 219
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .registers 2
    .parameter

    .prologue
    .line 134
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    .line 127
    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->f:Z

    if-eqz v0, :cond_10

    if-nez p2, :cond_10

    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    if-nez v0, :cond_10

    .line 128
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->j()V

    .line 130
    :cond_10
    return-void
.end method

.method public a(Lcom/google/googlenav/bb;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-boolean v0, p0, Lcom/google/googlenav/friend/p;->e:Z

    if-eqz v0, :cond_b

    .line 164
    if-eqz p1, :cond_a

    .line 165
    invoke-interface {p1, v1}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    .line 184
    :cond_a
    :goto_a
    return-void

    .line 171
    :cond_b
    monitor-enter p0

    .line 172
    :try_start_c
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 173
    iget-wide v2, p0, Lcom/google/googlenav/friend/p;->h:J

    sub-long v2, v0, v2

    cmp-long v2, v2, p2

    if-ltz v2, :cond_27

    .line 174
    iput-wide v0, p0, Lcom/google/googlenav/friend/p;->h:J

    .line 181
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_c .. :try_end_23} :catchall_2f

    .line 183
    invoke-static {p1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;)V

    goto :goto_a

    .line 176
    :cond_27
    if-eqz p1, :cond_2d

    .line 177
    const/4 v0, 0x0

    :try_start_2a
    invoke-interface {p1, v0}, Lcom/google/googlenav/bb;->b(Lcom/google/googlenav/aZ;)V

    .line 179
    :cond_2d
    monitor-exit p0

    goto :goto_a

    .line 181
    :catchall_2f
    move-exception v0

    monitor-exit p0
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_2f

    throw v0
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .registers 2
    .parameter

    .prologue
    .line 140
    return-void
.end method

.method public run()V
    .registers 3

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/google/googlenav/friend/p;->k()V

    .line 151
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    .line 152
    return-void
.end method
