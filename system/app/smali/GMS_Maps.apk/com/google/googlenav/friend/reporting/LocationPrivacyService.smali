.class public Lcom/google/googlenav/friend/reporting/LocationPrivacyService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private b:Lak/a;

.field private c:Lcom/google/googlenav/friend/history/z;

.field private d:Lcom/google/googlenav/friend/history/v;

.field private e:Lcom/google/googlenav/friend/reporting/s;

.field private f:Lcom/google/googlenav/friend/reporting/e;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 68
    const-string v0, "LocationPrivacyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 170
    new-instance v0, Lcom/google/googlenav/friend/S;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v2, v2, v1}, Lcom/google/googlenav/friend/S;-><init>(ILcom/google/googlenav/friend/aq;Lcom/google/googlenav/android/aa;Z)V

    .line 177
    invoke-virtual {v0}, Lcom/google/googlenav/friend/S;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/T;

    .line 180
    if-eqz v0, :cond_14

    iget-boolean v1, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-nez v1, :cond_15

    .line 215
    :cond_14
    :goto_14
    return-void

    .line 187
    :cond_15
    new-instance v1, Lcom/google/googlenav/friend/ae;

    new-instance v2, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 189
    invoke-virtual {v1}, Lcom/google/googlenav/friend/ae;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_68

    .line 214
    :goto_2c
    invoke-virtual {v1}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->b(Z)V

    goto :goto_14

    .line 191
    :pswitch_34
    invoke-static {}, Lcom/google/googlenav/friend/aH;->a()Z

    move-result v0

    if-nez v0, :cond_48

    .line 194
    invoke-static {}, Lcom/google/googlenav/friend/ad;->B()V

    .line 198
    :goto_3d
    invoke-static {}, Lcom/google/googlenav/friend/aH;->f()V

    .line 199
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    goto :goto_2c

    .line 196
    :cond_48
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    goto :goto_3d

    .line 202
    :pswitch_4c
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    .line 203
    invoke-static {}, Lcom/google/googlenav/friend/aH;->g()V

    .line 204
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    goto :goto_2c

    .line 207
    :pswitch_5a
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    .line 208
    invoke-static {}, Lcom/google/googlenav/friend/aH;->h()V

    .line 209
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    goto :goto_2c

    .line 189
    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_34
        :pswitch_4c
        :pswitch_5a
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 80
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 82
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 83
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/32 v3, 0x36ee80

    add-long v2, v1, v3

    .line 84
    const/4 v1, 0x2

    const-wide/32 v4, 0x2932e00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 86
    return-void
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    invoke-virtual {v0}, Lak/a;->a()V

    .line 136
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->f:Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/e;->a()Z

    move-result v0

    if-nez v0, :cond_44

    .line 137
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    .line 147
    :goto_14
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->d:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/v;->a()V

    .line 154
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_43

    .line 155
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->e:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/s;->d()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 157
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->c:Lcom/google/googlenav/friend/history/z;

    new-instance v1, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v1}, Lcom/google/googlenav/friend/history/b;-><init>()V

    new-instance v2, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v2}, Lcom/google/googlenav/friend/history/b;-><init>()V

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Z)Ljava/util/List;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_43

    .line 166
    :cond_43
    return-void

    .line 139
    :cond_44
    invoke-direct {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a()V

    goto :goto_14
.end method

.method public onCreate()V
    .registers 7

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 94
    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 95
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 98
    :cond_c
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 99
    const/4 v1, 0x1

    const-string v2, "LocationPrivacyService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    .line 100
    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    .line 101
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lak/a;->a(Landroid/content/Context;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/google/googlenav/friend/history/v;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/friend/history/v;-><init>(Landroid/content/Context;Lbm/c;)V

    iput-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->d:Lcom/google/googlenav/friend/history/v;

    .line 105
    new-instance v0, Lcom/google/googlenav/friend/reporting/f;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    .line 107
    invoke-static {p0, v0}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->e:Lcom/google/googlenav/friend/reporting/s;

    .line 110
    new-instance v0, Lcom/google/googlenav/friend/history/z;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->d:Lcom/google/googlenav/friend/history/v;

    new-instance v3, Lcom/google/googlenav/friend/history/q;

    invoke-direct {v3}, Lcom/google/googlenav/friend/history/q;-><init>()V

    new-instance v4, Lcom/google/googlenav/friend/history/W;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->e:Lcom/google/googlenav/friend/reporting/s;

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/z;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/v;Lcom/google/googlenav/friend/history/q;Lcom/google/googlenav/friend/history/W;Lcom/google/googlenav/friend/reporting/s;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->c:Lcom/google/googlenav/friend/history/z;

    .line 114
    new-instance v0, Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->f:Lcom/google/googlenav/friend/reporting/e;

    .line 115
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 121
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a(Landroid/content/Intent;)V
    :try_end_8
    .catchall {:try_start_0 .. :try_end_8} :catchall_e

    .line 123
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 125
    return-void

    .line 123
    :catchall_e
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
