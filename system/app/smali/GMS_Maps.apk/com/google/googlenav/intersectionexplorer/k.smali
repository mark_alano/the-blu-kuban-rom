.class public Lcom/google/googlenav/intersectionexplorer/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lq/e;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field private final b:Ljava/util/Set;

.field private c:Ljava/util/TreeSet;

.field private d:Lcom/google/googlenav/intersectionexplorer/c;

.field private e:Lo/ad;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 409
    new-instance v0, Lcom/google/googlenav/intersectionexplorer/l;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/l;-><init>()V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/k;->g:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    .line 101
    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    .line 102
    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    .line 103
    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    return-object p1
.end method

.method static a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 505
    const/4 v5, 0x0

    .line 506
    const-wide v3, 0x7fefffffffffffffL

    .line 508
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 509
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    .line 510
    invoke-virtual {v1, p1}, Lo/T;->c(Lo/T;)F

    move-result v1

    float-to-double v1, v1

    .line 512
    cmpg-double v7, v1, v3

    if-gez v7, :cond_2a

    move-wide v8, v1

    move-object v2, v0

    move-wide v0, v8

    :goto_26
    move-wide v3, v0

    move-object v5, v2

    .line 516
    goto :goto_a

    .line 518
    :cond_29
    return-object v5

    :cond_2a
    move-wide v0, v3

    move-object v2, v5

    goto :goto_26
.end method

.method static a(Ljava/util/Set;Lo/T;Z)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 474
    if-nez p0, :cond_4

    .line 495
    :cond_3
    :goto_3
    return-object v0

    .line 478
    :cond_4
    const-wide/high16 v1, 0x4000

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v3

    mul-double v2, v1, v3

    .line 481
    invoke-static {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v1

    .line 484
    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v4

    invoke-virtual {v4, p1}, Lo/T;->c(Lo/T;)F

    move-result v4

    float-to-double v4, v4

    cmpl-double v2, v4, v2

    if-lez v2, :cond_2c

    .line 490
    :goto_1f
    if-nez v0, :cond_3

    if-eqz p2, :cond_3

    .line 491
    new-instance v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-direct {v0, p1}, Lcom/google/googlenav/intersectionexplorer/c;-><init>(Lo/T;)V

    .line 492
    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2c
    move-object v0, v1

    goto :goto_1f
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Ljava/util/TreeSet;)Ljava/util/TreeSet;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object p1
.end method

.method static a(Ljava/util/TreeSet;)Ljava/util/TreeSet;
    .registers 6
    .parameter

    .prologue
    .line 422
    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 423
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 427
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->d()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 433
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    goto :goto_4

    .line 435
    :cond_2e
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->f()Z

    goto :goto_4

    .line 440
    :cond_32
    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v2

    .line 441
    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3a
    :goto_3a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_68

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 442
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a()Z

    move-result v1

    if-nez v1, :cond_50

    .line 443
    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_3a

    .line 445
    :cond_50
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_58
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    .line 446
    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->e(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_58

    .line 451
    :cond_68
    return-object v2
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Lo/ad;)Lo/ad;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    return p1
.end method

.method static synthetic b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->c(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/TreeSet;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object v0
.end method

.method private static c(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 462
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;Z)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method private c(Lo/T;)V
    .registers 4
    .parameter

    .prologue
    .line 214
    new-instance v0, Lcom/google/googlenav/intersectionexplorer/m;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/intersectionexplorer/m;-><init>(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;Lo/T;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 215
    return-void
.end method

.method static synthetic e()Lq/e;
    .registers 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->g()Lq/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->g:Ljava/util/Comparator;

    return-object v0
.end method

.method private static g()Lq/e;
    .registers 2

    .prologue
    .line 90
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    if-nez v0, :cond_11

    .line 91
    new-instance v0, Lq/e;

    sget-object v1, LA/c;->a:LA/c;

    invoke-static {v1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v1

    invoke-direct {v0, v1}, Lq/e;-><init>(Lr/z;)V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    .line 93
    :cond_11
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    return-object v0
.end method


# virtual methods
.method public a(D)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 5
    .parameter

    .prologue
    .line 572
    const-wide v0, 0x3fd921fb54442d18L

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->b(DD)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 163
    if-nez p1, :cond_4

    .line 165
    const/4 v0, 0x0

    .line 182
    :goto_3
    return-object v0

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/intersectionexplorer/c;->c(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;

    move-result-object v1

    .line 169
    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    .line 172
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    .line 173
    invoke-interface {v0, p1}, Lcom/google/googlenav/intersectionexplorer/n;->b(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_12

    .line 177
    :cond_22
    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    .line 178
    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 179
    invoke-direct {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->c(Lo/T;)V

    :cond_2f
    move-object v0, v1

    .line 182
    goto :goto_3
.end method

.method public a(DD)Lo/af;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 529
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/googlenav/intersectionexplorer/k;->b(DD)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    .line 530
    if-nez v0, :cond_8

    .line 531
    const/4 v0, 0x0

    .line 534
    :goto_7
    return-object v0

    :cond_8
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->f(Lcom/google/googlenav/intersectionexplorer/c;)Lo/af;

    move-result-object v0

    goto :goto_7
.end method

.method public a(Lcom/google/googlenav/intersectionexplorer/n;)V
    .registers 3
    .parameter

    .prologue
    .line 118
    if-nez p1, :cond_3

    .line 123
    :goto_2
    return-void

    .line 122
    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 148
    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 149
    invoke-direct {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->c(Lo/T;)V

    .line 153
    :goto_9
    return-void

    .line 151
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    invoke-static {v0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;

    goto :goto_9
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public b()Lcom/google/googlenav/intersectionexplorer/c;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    return-object v0
.end method

.method public b(DD)Lcom/google/googlenav/intersectionexplorer/c;
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 546
    const/4 v5, 0x0

    .line 549
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v3, p3

    :goto_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 550
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->d(Lcom/google/googlenav/intersectionexplorer/c;)D

    move-result-wide v1

    .line 551
    invoke-static {p1, p2, v1, v2}, Lcom/google/googlenav/intersectionexplorer/c;->a(DD)D

    move-result-wide v1

    .line 554
    cmpg-double v7, v1, v3

    if-gez v7, :cond_2d

    move-wide v8, v1

    move-object v2, v0

    move-wide v0, v8

    :goto_29
    move-wide v3, v0

    move-object v5, v2

    .line 558
    goto :goto_c

    .line 560
    :cond_2c
    return-object v5

    :cond_2d
    move-wide v0, v3

    move-object v2, v5

    goto :goto_29
.end method

.method b(Lo/T;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 190
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 201
    :cond_11
    :goto_11
    return v0

    .line 195
    :cond_12
    const-wide/high16 v1, 0x4079

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    .line 196
    invoke-static {p1, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v2

    .line 198
    invoke-virtual {v1}, Lo/ad;->e()Lo/T;

    move-result-object v1

    .line 201
    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v3

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v4

    if-gt v3, v4, :cond_11

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    if-gt v3, v2, :cond_11

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v3

    if-lt v2, v3, :cond_11

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    if-lt v2, v1, :cond_11

    const/4 v0, 0x0

    goto :goto_11
.end method

.method public c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 595
    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    return v0
.end method
