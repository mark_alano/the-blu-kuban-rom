.class Lcom/google/googlenav/intersectionexplorer/m;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/googlenav/intersectionexplorer/k;

.field final b:Lcom/google/googlenav/intersectionexplorer/c;

.field final c:Lo/T;

.field d:Ljava/util/TreeSet;

.field e:Lcom/google/googlenav/intersectionexplorer/c;

.field f:Lo/ad;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;Lo/T;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 233
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 234
    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    .line 235
    iput-object p2, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    .line 236
    iput-object p3, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    .line 237
    return-void
.end method

.method private a(Lq/a;)Ljava/util/TreeSet;
    .registers 14
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 347
    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v5

    move v2, v3

    .line 349
    :goto_6
    invoke-virtual {p1}, Lq/a;->a()I

    move-result v0

    if-ge v2, v0, :cond_aa

    .line 350
    invoke-virtual {p1, v2}, Lq/a;->a(I)Lq/c;

    move-result-object v1

    .line 351
    invoke-virtual {v1}, Lq/c;->b()Lo/af;

    move-result-object v6

    .line 354
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 355
    invoke-virtual {v1}, Lq/c;->c()Lo/T;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    .line 357
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    invoke-virtual {v1}, Lq/c;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 361
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->f()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 363
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_32
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_79

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq/b;

    .line 364
    invoke-virtual {v0}, Lq/b;->a()Lq/c;

    move-result-object v8

    .line 366
    invoke-virtual {v1}, Lq/c;->c()Lo/T;

    move-result-object v9

    invoke-virtual {v1}, Lq/c;->d()Lo/T;

    move-result-object v10

    invoke-virtual {v0}, Lq/b;->b()F

    move-result v11

    invoke-virtual {v9, v10, v11}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v9

    .line 368
    invoke-virtual {v8}, Lq/c;->c()Lo/T;

    move-result-object v10

    invoke-virtual {v8}, Lq/c;->d()Lo/T;

    move-result-object v8

    invoke-virtual {v0}, Lq/b;->c()F

    move-result v0

    invoke-virtual {v10, v8, v0}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    .line 371
    invoke-static {v5, v9}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v8

    .line 373
    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    .line 377
    if-eq v8, v0, :cond_75

    .line 378
    invoke-virtual {v8, v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    .line 379
    invoke-virtual {v5, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 380
    invoke-virtual {v5, v8}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_75
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 386
    :cond_79
    invoke-virtual {v1}, Lq/c;->d()Lo/T;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    .line 388
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v4, v3

    .line 392
    :goto_89
    add-int/lit8 v0, v8, -0x1

    if-ge v4, v0, :cond_a5

    .line 393
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    .line 394
    add-int/lit8 v1, v4, 0x1

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    .line 396
    invoke-virtual {v0, v1, v6}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V

    .line 397
    invoke-virtual {v1, v0, v6}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V

    .line 392
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_89

    .line 349
    :cond_a5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_6

    .line 402
    :cond_aa
    invoke-static {v5}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/TreeSet;)Ljava/util/TreeSet;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/T;D)Lo/ad;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 301
    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v0

    mul-double/2addr v0, p2

    const-wide/high16 v2, 0x3fe0

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 302
    invoke-static {p1, v0}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/ad;)Lq/d;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 313
    :try_start_1
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->e()Lq/e;

    move-result-object v1

    const/4 v2, 0x1

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, p1, v2, v3, v4}, Lq/e;->a(Lo/ad;ZJ)Lq/d;
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_b} :catch_37

    move-result-object v1

    .line 318
    if-nez v1, :cond_f

    .line 335
    :goto_e
    return-object v0

    .line 322
    :cond_f
    sget-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->a:Z

    if-eqz v0, :cond_15

    move-object v0, v1

    .line 323
    goto :goto_e

    .line 327
    :cond_15
    new-instance v2, Lq/d;

    invoke-direct {v2}, Lq/d;-><init>()V

    .line 328
    const/4 v0, 0x0

    :goto_1b
    invoke-virtual {v1}, Lq/d;->a()I

    move-result v3

    if-ge v0, v3, :cond_35

    .line 329
    invoke-virtual {v1, v0}, Lq/d;->a(I)Lo/af;

    move-result-object v3

    .line 330
    invoke-virtual {v3}, Lo/af;->c()I

    move-result v4

    if-lez v4, :cond_32

    .line 331
    invoke-virtual {v1, v0}, Lq/d;->b(I)Lo/X;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lq/d;->a(Lo/af;Lo/X;)V

    .line 328
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    :cond_35
    move-object v0, v2

    .line 335
    goto :goto_e

    .line 314
    :catch_37
    move-exception v1

    goto :goto_e
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 7
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    const-wide v1, 0x4082c00000000000L

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lo/T;D)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    .line 249
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    invoke-direct {p0, v0}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lo/ad;)Lq/d;

    move-result-object v0

    .line 250
    if-nez v0, :cond_1b

    .line 251
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 271
    :goto_1a
    return-object v0

    .line 254
    :cond_1b
    const-wide/high16 v1, 0x4024

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    .line 256
    new-instance v2, Lq/a;

    const/4 v3, 0x2

    invoke-direct {v2, v0, v1, v3}, Lq/a;-><init>(Lq/d;II)V

    .line 259
    invoke-direct {p0, v2}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lq/a;)Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    .line 262
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    if-nez v0, :cond_45

    .line 263
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    .line 269
    :goto_37
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    invoke-static {v1, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    .line 271
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1a

    .line 265
    :cond_45
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    goto :goto_37
.end method

.method protected a(Ljava/lang/Boolean;)V
    .registers 5
    .parameter

    .prologue
    .line 276
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_14

    .line 277
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z

    .line 278
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->h()V

    .line 298
    :cond_13
    return-void

    .line 283
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/googlenav/intersectionexplorer/c;

    .line 284
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Ljava/util/TreeSet;)Ljava/util/TreeSet;

    .line 285
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Lo/ad;)Lo/ad;

    .line 288
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_53

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_53

    .line 289
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_41
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    .line 290
    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-interface {v0, v2}, Lcom/google/googlenav/intersectionexplorer/n;->b(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_41

    .line 295
    :cond_53
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    .line 296
    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v2}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/TreeSet;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/googlenav/intersectionexplorer/n;->a(Ljava/util/Set;)V

    goto :goto_5d
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 223
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/m;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 223
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/m;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z

    .line 242
    return-void
.end method
