.class public Lcom/google/googlenav/layer/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:J

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/lang/Object;

.field private g:Lcom/google/googlenav/ui/m;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private k:Lcom/google/googlenav/layer/l;


# direct methods
.method private constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v2, p0, Lcom/google/googlenav/layer/f;->a:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    .line 94
    new-instance v0, Lcom/google/googlenav/ui/m;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/m;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    .line 95
    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->h:Z

    .line 109
    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->i:Z

    .line 129
    invoke-direct {p0}, Lcom/google/googlenav/layer/f;->i()V

    .line 130
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/layer/g;)V
    .registers 2
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/googlenav/layer/f;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/layer/f;
    .registers 1

    .prologue
    .line 164
    sget-object v0, Lcom/google/googlenav/layer/h;->a:Lcom/google/googlenav/layer/f;

    return-object v0
.end method

.method private i()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 136
    const/16 v0, 0x23e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 138
    new-instance v2, Lcom/google/googlenav/layer/m;

    const-string v3, "LayerTransit"

    new-array v4, v7, [Lan/f;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v5, Lcom/google/googlenav/ui/bi;->aF:C

    invoke-interface {v0, v5}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    aput-object v0, v4, v6

    invoke-direct {v2, v3, v1, v4, v6}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;Ljava/lang/String;[Lan/f;I)V

    .line 143
    const-string v0, "LayerTransit"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    .line 145
    if-nez v0, :cond_63

    .line 146
    new-instance v3, Lcom/google/googlenav/layer/i;

    const-string v0, "__LAYERS"

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "LayerTransit"

    aput-object v5, v4, v6

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/layer/i;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/google/googlenav/layer/i;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    new-instance v0, Lcom/google/googlenav/layer/e;

    const-string v4, "LayerTransit"

    const/4 v5, 0x2

    invoke-direct {v0, v4, v5, v1}, Lcom/google/googlenav/layer/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;Lcom/google/googlenav/layer/e;)V

    .line 153
    iget-object v1, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :goto_59
    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    if-nez v1, :cond_62

    .line 159
    invoke-virtual {v0, v2}, Lcom/google/googlenav/layer/e;->a(Lcom/google/googlenav/layer/m;)V

    .line 161
    :cond_62
    return-void

    .line 155
    :cond_63
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    const-string v1, "__LAYERS"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    .line 156
    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    goto :goto_59
.end method


# virtual methods
.method public a(I)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 309
    iget-boolean v1, p0, Lcom/google/googlenav/layer/f;->h:Z

    if-eqz v1, :cond_9

    if-eq p1, v0, :cond_9

    add-int/lit16 v0, p1, 0x3e8

    :cond_9
    return v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;
    .registers 4
    .parameter

    .prologue
    .line 319
    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_3
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    monitor-exit v1

    return-object v0

    .line 321
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 12
    .parameter

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x7

    const/4 v1, 0x0

    .line 213
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/layer/f;->a:I

    .line 216
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 219
    const-string v0, "__LAYERS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 223
    const/16 v0, 0xc

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/googlenav/layer/f;->c:J

    .line 234
    :goto_20
    iget-object v3, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 239
    :try_start_23
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    .line 240
    if-eqz v0, :cond_4d

    .line 241
    invoke-virtual {v0}, Lcom/google/googlenav/layer/i;->b()[Ljava/lang/String;

    move-result-object v4

    .line 242
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_34
    if-ltz v0, :cond_48

    .line 243
    iget-object v5, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    aget-object v6, v4, v0

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3d
    .catchall {:try_start_23 .. :try_end_3d} :catchall_d2

    .line 242
    add-int/lit8 v0, v0, -0x1

    goto :goto_34

    .line 228
    :cond_40
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    goto :goto_20

    .line 245
    :cond_48
    :try_start_48
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    :cond_4d
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    .line 250
    :goto_53
    if-ge v0, v2, :cond_6b

    .line 251
    new-instance v4, Lcom/google/googlenav/layer/i;

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/googlenav/layer/i;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 253
    iget-object v5, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-virtual {v4}, Lcom/google/googlenav/layer/i;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_53

    .line 259
    :cond_6b
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    move v2, v1

    .line 260
    :goto_71
    if-ge v2, v4, :cond_b1

    .line 261
    new-instance v5, Lcom/google/googlenav/layer/e;

    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/layer/e;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 263
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_87
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ad

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    .line 264
    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v7

    if-eqz v7, :cond_87

    .line 265
    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;Lcom/google/googlenav/layer/e;)V

    .line 266
    iget-object v6, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/google/googlenav/layer/e;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_ad
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_71

    .line 271
    :cond_b1
    monitor-exit v3
    :try_end_b2
    .catchall {:try_start_48 .. :try_end_b2} :catchall_d2

    .line 274
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    .line 275
    :goto_b7
    if-ge v0, v2, :cond_d5

    .line 276
    new-instance v3, Lcom/google/googlenav/layer/m;

    invoke-virtual {p1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 278
    invoke-virtual {v3}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v4

    .line 279
    if-eqz v4, :cond_cf

    .line 280
    invoke-virtual {v4, v3}, Lcom/google/googlenav/layer/e;->a(Lcom/google/googlenav/layer/m;)V

    .line 275
    :cond_cf
    add-int/lit8 v0, v0, 0x1

    goto :goto_b7

    .line 271
    :catchall_d2
    move-exception v0

    :try_start_d3
    monitor-exit v3
    :try_end_d4
    .catchall {:try_start_d3 .. :try_end_d4} :catchall_d2

    throw v0

    .line 287
    :cond_d5
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    .line 288
    :goto_da
    if-ge v0, v2, :cond_110

    .line 289
    invoke-virtual {p1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 290
    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 291
    iget-object v4, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    const/16 v5, 0xa

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v5

    int-to-long v6, v3

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    .line 293
    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_10d

    .line 294
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/googlenav/layer/f;->h:Z

    .line 295
    iget-object v4, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    const/16 v5, 0xb

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/google/googlenav/layer/f;->a(I)I

    move-result v3

    int-to-long v5, v3

    invoke-virtual {v4, v1, v5, v6}, Lcom/google/googlenav/ui/m;->a([BJ)Lcom/google/googlenav/ui/o;

    .line 288
    :cond_10d
    add-int/lit8 v0, v0, 0x1

    goto :goto_da

    .line 300
    :cond_110
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/layer/l;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-boolean v0, p0, Lcom/google/googlenav/layer/f;->i:Z

    if-eqz v0, :cond_a

    .line 182
    iput-object p1, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 183
    iput-object p2, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    .line 210
    :cond_9
    :goto_9
    :pswitch_9
    return-void

    .line 186
    :cond_a
    iput-object v1, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 187
    iput-object v1, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    .line 190
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 191
    packed-switch v0, :pswitch_data_2c

    goto :goto_9

    .line 193
    :pswitch_17
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 195
    if-eqz p2, :cond_9

    .line 196
    invoke-interface {p2}, Lcom/google/googlenav/layer/l;->a()V

    goto :goto_9

    .line 207
    :pswitch_24
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->h()V

    goto :goto_9

    .line 191
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_17
        :pswitch_9
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
    .end packed-switch
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 454
    iput-boolean p1, p0, Lcom/google/googlenav/layer/f;->i:Z

    .line 458
    if-nez p1, :cond_f

    iget-object v0, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_f

    .line 460
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/layer/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/layer/l;)V

    .line 462
    :cond_f
    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;
    .registers 4
    .parameter

    .prologue
    .line 340
    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 341
    :try_start_3
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/i;

    .line 342
    if-nez v0, :cond_10

    .line 343
    const/4 v0, 0x0

    monitor-exit v1

    .line 345
    :goto_f
    return-object v0

    :cond_10
    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/i;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    monitor-exit v1

    goto :goto_f

    .line 346
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 366
    const-string v0, "TrafficIncident"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;
    .registers 3
    .parameter

    .prologue
    .line 356
    invoke-virtual {p0, p1}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public c()Ljava/util/List;
    .registers 5

    .prologue
    .line 378
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 380
    invoke-virtual {p0}, Lcom/google/googlenav/layer/f;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/m;

    .line 381
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 382
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 386
    :cond_23
    return-object v1
.end method

.method public d()Ljava/util/List;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 395
    const-string v0, "__LAYERS"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/layer/f;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, Lcom/google/googlenav/layer/i;->b()[Ljava/lang/String;

    move-result-object v0

    .line 398
    :goto_d
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 399
    :goto_12
    array-length v3, v0

    if-ge v1, v3, :cond_32

    .line 400
    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v3

    .line 401
    if-eqz v3, :cond_2c

    invoke-virtual {v3}, Lcom/google/googlenav/layer/e;->c()Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 402
    invoke-virtual {v3}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v3

    .line 403
    if-eqz v3, :cond_2c

    .line 404
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 396
    :cond_2f
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_d

    .line 409
    :cond_32
    return-object v2
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 413
    invoke-virtual {p0, p1}, Lcom/google/googlenav/layer/f;->a(Ljava/lang/String;)Lcom/google/googlenav/layer/i;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public e()I
    .registers 2

    .prologue
    .line 417
    iget v0, p0, Lcom/google/googlenav/layer/f;->a:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()J
    .registers 3

    .prologue
    .line 425
    iget-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    return-wide v0
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 429
    iget-object v1, p0, Lcom/google/googlenav/layer/f;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_5
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 431
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 432
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_28

    .line 433
    iput v2, p0, Lcom/google/googlenav/layer/f;->a:I

    .line 434
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/layer/f;->b:Ljava/lang/String;

    .line 435
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/layer/f;->c:J

    .line 436
    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->i:Z

    .line 437
    iput-object v3, p0, Lcom/google/googlenav/layer/f;->k:Lcom/google/googlenav/layer/l;

    .line 438
    iput-object v3, p0, Lcom/google/googlenav/layer/f;->j:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 439
    iget-object v0, p0, Lcom/google/googlenav/layer/f;->g:Lcom/google/googlenav/ui/m;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/m;->b()V

    .line 440
    iput-boolean v2, p0, Lcom/google/googlenav/layer/f;->h:Z

    .line 441
    return-void

    .line 432
    :catchall_28
    move-exception v0

    :try_start_29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    throw v0
.end method
