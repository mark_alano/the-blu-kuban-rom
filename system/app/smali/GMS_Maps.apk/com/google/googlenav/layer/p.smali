.class public Lcom/google/googlenav/layer/p;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/googlenav/layer/q;

.field private final d:Z

.field private final e:Z

.field private f:Lcom/google/googlenav/W;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/layer/q;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/layer/q;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 99
    invoke-direct {p0}, Law/a;-><init>()V

    .line 100
    iput-object p1, p0, Lcom/google/googlenav/layer/p;->a:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    .line 102
    iput-object p5, p0, Lcom/google/googlenav/layer/p;->c:Lcom/google/googlenav/layer/q;

    .line 103
    iput-boolean p4, p0, Lcom/google/googlenav/layer/p;->e:Z

    .line 104
    iput-boolean p3, p0, Lcom/google/googlenav/layer/p;->d:Z

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/layer/p;->g:Z

    .line 106
    return-void
.end method


# virtual methods
.method public Z()V
    .registers 3

    .prologue
    .line 200
    invoke-super {p0}, Law/a;->Z()V

    .line 201
    iget-object v0, p0, Lcom/google/googlenav/layer/p;->c:Lcom/google/googlenav/layer/q;

    iget-object v1, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/layer/q;->a(Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 126
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dB;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 128
    iget-object v1, p0, Lcom/google/googlenav/layer/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 129
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 133
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 135
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/D;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 137
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 138
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 141
    invoke-virtual {p0}, Lcom/google/googlenav/layer/p;->b()I

    move-result v1

    invoke-static {v1, v0, v3}, Lcom/google/googlenav/ah;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 144
    iget-boolean v1, p0, Lcom/google/googlenav/layer/p;->e:Z

    if-eqz v1, :cond_43

    .line 145
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v1

    .line 146
    const/4 v2, 0x5

    invoke-virtual {v1}, Lcom/google/googlenav/ui/m;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 148
    :cond_43
    iget-boolean v1, p0, Lcom/google/googlenav/layer/p;->d:Z

    if-nez v1, :cond_4c

    .line 149
    const/16 v1, 0x2b

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 151
    :cond_4c
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 152
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 216
    iput-boolean p1, p0, Lcom/google/googlenav/layer/p;->g:Z

    .line 217
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 156
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dB;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 160
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    .line 164
    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_14

    .line 195
    :cond_13
    :goto_13
    return v0

    .line 168
    :cond_14
    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 170
    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/layer/p;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_26

    move v0, v1

    .line 171
    goto :goto_13

    .line 173
    :cond_26
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_35

    move v0, v1

    .line 174
    goto :goto_13

    .line 177
    :cond_35
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 178
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/layer/p;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    invoke-static {v1, v3, v4}, Lcom/google/googlenav/W;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/W;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    .line 180
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_70

    .line 181
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    .line 186
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    .line 187
    iget-object v1, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    add-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/W;->a(J)V

    .line 191
    :goto_65
    iget-object v1, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/W;->b(Ljava/lang/String;)V

    goto :goto_13

    .line 189
    :cond_70
    iget-object v1, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/W;->a(J)V

    goto :goto_65
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 110
    const/16 v0, 0x25

    return v0
.end method

.method public c_()Z
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method public d_()V
    .registers 5

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/googlenav/layer/p;->c:Lcom/google/googlenav/layer/q;

    iget-object v2, p0, Lcom/google/googlenav/layer/p;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/layer/p;->f:Lcom/google/googlenav/W;

    iget-boolean v0, p0, Lcom/google/googlenav/layer/p;->g:Z

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-interface {v1, v2, v3, v0}, Lcom/google/googlenav/layer/q;->a(Ljava/lang/String;Lcom/google/googlenav/W;Z)V

    .line 209
    return-void

    .line 208
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/google/googlenav/layer/p;->g:Z

    return v0
.end method
