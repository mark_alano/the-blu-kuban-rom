.class public Lcom/google/googlenav/o;
.super Law/b;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lcom/google/googlenav/h;

.field private final c:Ljava/util/List;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/googlenav/r;

.field private final f:J

.field private g:Lcom/google/googlenav/a;

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/google/googlenav/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 171
    invoke-direct {p0}, Law/b;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    .line 173
    iput-object p2, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    .line 174
    iput-object p3, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    .line 175
    iput-object p4, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    .line 176
    iput-object p5, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    .line 179
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/o;->f:J

    .line 180
    return-void
.end method

.method private declared-synchronized n()Ljava/lang/String;
    .registers 2

    .prologue
    .line 313
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/o;->i:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 5
    .parameter

    .prologue
    .line 204
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ib;->u:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 207
    invoke-virtual {p0}, Lcom/google/googlenav/o;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 210
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 213
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 214
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 263
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ib;->v:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 267
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 268
    if-nez v0, :cond_25

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/google/googlenav/o;->h:Z

    .line 270
    iget-boolean v0, p0, Lcom/google/googlenav/o;->h:Z

    if-eqz v0, :cond_24

    .line 271
    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 272
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 274
    new-instance v2, Lcom/google/googlenav/a;

    invoke-direct {v2, v0}, Lcom/google/googlenav/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v2, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    .line 281
    :cond_24
    return v1

    .line 268
    :cond_25
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public b()I
    .registers 2

    .prologue
    .line 184
    const/16 v0, 0x65

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .registers 6

    .prologue
    .line 286
    invoke-super {p0}, Law/b;->d_()V

    .line 287
    iget-object v0, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    if-eqz v0, :cond_14

    .line 288
    iget-object v0, p0, Lcom/google/googlenav/o;->e:Lcom/google/googlenav/r;

    iget-boolean v1, p0, Lcom/google/googlenav/o;->h:Z

    iget-object v2, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    iget-object v3, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    iget-object v4, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/r;->a(ZLcom/google/googlenav/a;Lcom/google/googlenav/h;Ljava/util/List;)V

    .line 290
    :cond_14
    return-void
.end method

.method i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 222
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbO/t;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 223
    const/4 v0, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 226
    iget-object v0, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 227
    iget-object v0, p0, Lcom/google/googlenav/o;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 232
    :cond_19
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/googlenav/o;->f:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 235
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    invoke-virtual {v2}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 241
    iget-object v0, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    if-eqz v0, :cond_45

    .line 242
    iget-object v0, p0, Lcom/google/googlenav/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 243
    const/16 v3, 0xc

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_33

    .line 249
    :cond_45
    iget-object v0, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    if-eqz v0, :cond_58

    .line 250
    iget-object v0, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/google/googlenav/q;->a(Ljava/util/List;)Lcom/google/googlenav/q;

    move-result-object v0

    .line 251
    const/16 v2, 0xa

    invoke-virtual {v0}, Lcom/google/googlenav/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 255
    :cond_58
    iget-object v0, p0, Lcom/google/googlenav/o;->i:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_68

    .line 256
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/googlenav/o;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 258
    :cond_68
    return-object v1
.end method

.method protected j()Lcom/google/googlenav/s;
    .registers 3

    .prologue
    .line 326
    new-instance v0, Lcom/google/googlenav/s;

    invoke-direct {v0}, Lcom/google/googlenav/s;-><init>()V

    .line 327
    iget-boolean v1, p0, Lcom/google/googlenav/o;->h:Z

    iput-boolean v1, v0, Lcom/google/googlenav/s;->a:Z

    .line 328
    iget-object v1, p0, Lcom/google/googlenav/o;->g:Lcom/google/googlenav/a;

    iput-object v1, v0, Lcom/google/googlenav/s;->b:Lcom/google/googlenav/a;

    .line 329
    iget-object v1, p0, Lcom/google/googlenav/o;->b:Lcom/google/googlenav/h;

    iput-object v1, v0, Lcom/google/googlenav/s;->c:Lcom/google/googlenav/h;

    .line 330
    iget-object v1, p0, Lcom/google/googlenav/o;->a:Ljava/util/List;

    iput-object v1, v0, Lcom/google/googlenav/s;->d:Ljava/util/List;

    .line 331
    return-object v0
.end method

.method protected synthetic k()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/googlenav/o;->j()Lcom/google/googlenav/s;

    move-result-object v0

    return-object v0
.end method
