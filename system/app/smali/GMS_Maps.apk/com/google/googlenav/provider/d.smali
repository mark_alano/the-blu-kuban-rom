.class Lcom/google/googlenav/provider/d;
.super Lcom/google/googlenav/provider/b;
.source "SourceFile"


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .registers 4
    .parameter

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/google/googlenav/provider/b;-><init>(Landroid/database/Cursor;)V

    .line 363
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/provider/d;->b:I

    .line 364
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    const-string v1, "displayQuery"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/provider/d;->c:I

    .line 366
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    const-string v1, "latitude"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/provider/d;->d:I

    .line 368
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    const-string v1, "longitude"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/provider/d;->e:I

    .line 370
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    const-string v1, "timestamp"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/provider/d;->f:I

    .line 372
    return-void
.end method


# virtual methods
.method public getInt(I)I
    .registers 4
    .parameter

    .prologue
    .line 400
    iget v0, p0, Lcom/google/googlenav/provider/d;->mPos:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_24

    .line 403
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->mPos:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 406
    const/4 v0, 0x5

    if-ne p1, v0, :cond_18

    .line 407
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 412
    :goto_17
    return v0

    .line 408
    :cond_18
    const/4 v0, 0x6

    if-ne p1, v0, :cond_24

    .line 409
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->e:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_17

    .line 412
    :cond_24
    const v0, 0xbebc200

    goto :goto_17
.end method

.method public getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 418
    iget v0, p0, Lcom/google/googlenav/provider/d;->mPos:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_18

    .line 421
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->mPos:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 424
    const/4 v0, 0x7

    if-ne p1, v0, :cond_18

    .line 425
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->f:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 428
    :goto_17
    return-wide v0

    :cond_18
    const-wide/16 v0, 0x0

    goto :goto_17
.end method

.method public getString(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 378
    iget v1, p0, Lcom/google/googlenav/provider/d;->mPos:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_18

    .line 381
    iget-object v1, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/googlenav/provider/d;->mPos:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 383
    const/4 v1, 0x3

    if-ne p1, v1, :cond_19

    .line 384
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 393
    :cond_18
    :goto_18
    return-object v0

    .line 385
    :cond_19
    const/4 v1, 0x1

    if-ne p1, v1, :cond_18

    .line 386
    iget-object v0, p0, Lcom/google/googlenav/provider/d;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/googlenav/provider/d;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18
.end method
