.class final Lcom/google/googlenav/provider/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:LaN/B;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;LaN/B;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/googlenav/provider/g;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/provider/g;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/provider/g;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/googlenav/provider/g;->d:LaN/B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    .prologue
    const v7, 0xbebc200

    const/4 v6, 0x0

    .line 354
    iget-object v0, p0, Lcom/google/googlenav/provider/g;->a:Ljava/lang/String;

    .line 355
    iget-object v1, p0, Lcom/google/googlenav/provider/g;->a:Ljava/lang/String;

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcom/google/googlenav/provider/g;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/provider/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    iget-object v1, p0, Lcom/google/googlenav/provider/g;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_21

    .line 357
    :cond_20
    const/4 v0, 0x0

    .line 362
    :cond_21
    iget-object v1, p0, Lcom/google/googlenav/provider/g;->b:Ljava/lang/String;

    invoke-static {v1, v6}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 363
    if-nez v0, :cond_33

    iget-object v2, p0, Lcom/google/googlenav/provider/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    .line 364
    iget-object v0, p0, Lcom/google/googlenav/provider/g;->b:Ljava/lang/String;

    .line 369
    :cond_33
    iget-object v2, p0, Lcom/google/googlenav/provider/g;->c:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 370
    iget-object v2, p0, Lcom/google/googlenav/provider/g;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/provider/SearchHistoryProvider;->a:Landroid/net/Uri;

    const-string v4, "data1=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 374
    :cond_4d
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 375
    const-string v3, "data1"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    if-eqz v0, :cond_5e

    .line 377
    const-string v1, "displayQuery"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_5e
    iget-object v0, p0, Lcom/google/googlenav/provider/g;->d:LaN/B;

    if-eqz v0, :cond_a1

    .line 380
    const-string v0, "latitude"

    iget-object v1, p0, Lcom/google/googlenav/provider/g;->d:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 381
    const-string v0, "longitude"

    iget-object v1, p0, Lcom/google/googlenav/provider/g;->d:LaN/B;

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 390
    :goto_80
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 391
    const-string v3, "timestamp"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 393
    iget-object v0, p0, Lcom/google/googlenav/provider/g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/SearchHistoryProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 395
    return-void

    .line 384
    :cond_a1
    const-string v0, "latitude"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 386
    const-string v0, "longitude"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_80
.end method
