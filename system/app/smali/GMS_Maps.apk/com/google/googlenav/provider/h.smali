.class Lcom/google/googlenav/provider/h;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 128
    const-string v0, "search_history.db"

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 129
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter

    .prologue
    .line 133
    const-string v0, "CREATE TABLE suggestions (_id INTEGER PRIMARY KEY AUTOINCREMENT, data1 TEXT, singleResult INTEGER, displayQuery TEXT, latitude INTEGER DEFAULT 200000000, longitude INTEGER DEFAULT 200000000, timestamp INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 148
    if-lt p2, p3, :cond_3

    .line 189
    :cond_2
    :goto_2
    return-void

    .line 152
    :cond_3
    const/4 v0, 0x4

    if-ge p2, v0, :cond_14

    .line 156
    const-string v0, "DROP TABLE IF EXISTS history"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 157
    const-string v0, "DROP TABLE IF EXISTS suggestions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/google/googlenav/provider/h;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_2

    .line 163
    :cond_14
    const/4 v0, 0x5

    if-ge p2, v0, :cond_1c

    .line 164
    const-string v0, "ALTER TABLE suggestions ADD COLUMN displayQuery TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    :cond_1c
    const/4 v0, 0x6

    if-ge p2, v0, :cond_24

    .line 171
    const-string v0, "DROP TABLE IF EXISTS history"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 174
    :cond_24
    const/4 v0, 0x7

    if-ge p2, v0, :cond_31

    .line 178
    const-string v0, "ALTER TABLE suggestions ADD COLUMN latitude INTEGER DEFAULT 200000000"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 180
    const-string v0, "ALTER TABLE suggestions ADD COLUMN longitude INTEGER DEFAULT 200000000"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 184
    :cond_31
    const/16 v0, 0x8

    if-ge p2, v0, :cond_2

    .line 186
    const-string v0, "ALTER TABLE suggestions ADD COLUMN timestamp INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_2
.end method
