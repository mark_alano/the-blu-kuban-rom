.class Lcom/google/googlenav/settings/N;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/settings/MapTileSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 595
    iput-object p1, p0, Lcom/google/googlenav/settings/N;->a:Lcom/google/googlenav/settings/MapTileSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;Lcom/google/googlenav/settings/F;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 595
    invoke-direct {p0, p1}, Lcom/google/googlenav/settings/N;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 4
    .parameter

    .prologue
    .line 598
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;)V

    .line 599
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 604
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/settings/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/O;-><init>(Lcom/google/googlenav/settings/N;)V

    invoke-virtual {v0, v1}, LaT/a;->a(LaT/m;)V

    .line 615
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-virtual {v0}, LaT/a;->l()V

    .line 619
    :goto_1e
    const/4 v0, 0x0

    return-object v0

    .line 617
    :cond_20
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->f()V

    goto :goto_1e
.end method

.method protected a(Ljava/lang/Void;)V
    .registers 4
    .parameter

    .prologue
    .line 624
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 626
    iget-object v0, p0, Lcom/google/googlenav/settings/N;->a:Lcom/google/googlenav/settings/MapTileSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->a(Lcom/google/googlenav/settings/MapTileSettingsActivity;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 627
    :try_start_a
    iget-object v0, p0, Lcom/google/googlenav/settings/N;->a:Lcom/google/googlenav/settings/MapTileSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b(Lcom/google/googlenav/settings/MapTileSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 628
    iget-object v0, p0, Lcom/google/googlenav/settings/N;->a:Lcom/google/googlenav/settings/MapTileSettingsActivity;

    invoke-static {v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    .line 630
    :cond_17
    monitor-exit v1

    .line 631
    return-void

    .line 630
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_a .. :try_end_1b} :catchall_19

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 595
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/settings/N;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 595
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/settings/N;->a(Ljava/lang/Void;)V

    return-void
.end method
