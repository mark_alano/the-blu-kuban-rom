.class public Lcom/google/googlenav/ui/aB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private final a:Lcom/google/googlenav/ui/aA;

.field private final b:Lcom/google/googlenav/ui/bs;

.field private final c:Lbf/m;

.field private final d:Lcom/google/googlenav/ui/ay;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;Lbf/m;Lcom/google/googlenav/ui/ay;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object p1, p0, Lcom/google/googlenav/ui/aB;->a:Lcom/google/googlenav/ui/aA;

    .line 126
    iput-object p2, p0, Lcom/google/googlenav/ui/aB;->b:Lcom/google/googlenav/ui/bs;

    .line 127
    iput-object p3, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    .line 128
    iput-object p4, p0, Lcom/google/googlenav/ui/aB;->d:Lcom/google/googlenav/ui/ay;

    .line 129
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ak()Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public Q_()V
    .registers 4

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/googlenav/ui/aB;->b()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 134
    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->d:Lcom/google/googlenav/ui/ay;

    iget-object v1, p0, Lcom/google/googlenav/ui/aB;->a:Lcom/google/googlenav/ui/aA;

    iget-object v2, p0, Lcom/google/googlenav/ui/aB;->b:Lcom/google/googlenav/ui/bs;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/ay;->a(Lcom/google/googlenav/ui/ay;Lcom/google/googlenav/ui/aA;Lcom/google/googlenav/ui/bs;)V

    .line 135
    iget-object v0, p0, Lcom/google/googlenav/ui/aB;->c:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bd()V

    .line 137
    :cond_14
    return-void
.end method
