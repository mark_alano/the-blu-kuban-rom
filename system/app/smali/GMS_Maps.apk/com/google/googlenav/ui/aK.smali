.class public Lcom/google/googlenav/ui/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaV/i;
.implements Landroid/location/GpsStatus$Listener;


# static fields
.field private static final y:Ljava/util/Map;


# instance fields
.field private final a:LaH/m;

.field private final b:LaN/p;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/ui/s;

.field private final e:Lcom/google/googlenav/android/A;

.field private f:LaH/h;

.field private final g:Lo/S;

.field private final h:LaN/v;

.field private i:Ljava/lang/Boolean;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:J

.field private q:J

.field private r:Z

.field private s:Lo/D;

.field private t:LaH/t;

.field private u:Landroid/location/GpsStatus;

.field private v:J

.field private w:Ljava/lang/String;

.field private x:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 253
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    .line 256
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "gps"

    const-string v2, "g"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "network"

    const-string v2, "wc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "wifi"

    const-string v2, "wc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "indoor"

    const-string v2, "i"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_cell"

    const-string v2, "fc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_wifi"

    const-string v2, "fw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_gps"

    const-string v2, "fg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    const-string v1, "fused_indoor"

    const-string v2, "fi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    return-void
.end method

.method public constructor <init>(LaH/m;Lcom/google/googlenav/ui/s;LaN/u;LaN/p;Lcom/google/googlenav/android/A;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const-wide/high16 v3, -0x8000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    .line 185
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->j:Z

    .line 188
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->k:Z

    .line 194
    iput v1, p0, Lcom/google/googlenav/ui/ak;->l:I

    .line 200
    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->m:Z

    .line 206
    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->n:Z

    .line 212
    iput-boolean v2, p0, Lcom/google/googlenav/ui/ak;->o:Z

    .line 219
    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->p:J

    .line 224
    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->q:J

    .line 235
    iput-object v5, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    .line 238
    iput-wide v3, p0, Lcom/google/googlenav/ui/ak;->v:J

    .line 239
    iput-object v5, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    .line 249
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    .line 276
    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    .line 277
    iput-object p2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    .line 278
    iput-object p3, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    .line 279
    iput-object p4, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    .line 280
    iput-object p5, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    .line 282
    new-instance v0, LaH/t;

    invoke-direct {v0}, LaH/t;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->t:LaH/t;

    .line 285
    new-instance v0, Lcom/google/googlenav/ui/al;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/al;-><init>(Lcom/google/googlenav/ui/ak;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->h:LaN/v;

    .line 305
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->h:LaN/v;

    invoke-virtual {p3, v0}, LaN/u;->a(LaN/v;)V

    .line 307
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    .line 311
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_5a

    .line 313
    invoke-virtual {v0, p0}, LaV/h;->a(LaV/i;)V

    .line 315
    :cond_5a
    return-void
.end method

.method private C()Ljava/lang/String;
    .registers 2

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->m:Z

    if-eqz v0, :cond_e

    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->m:Z

    .line 431
    const/16 v0, 0x5f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 433
    :goto_d
    return-object v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private D()V
    .registers 3

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lo/S;)V

    .line 468
    return-void
.end method

.method private E()Z
    .registers 2

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->h()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->k()Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private F()V
    .registers 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 587
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    .line 589
    const-string v0, "HAS_SHOWN_CALIBRATE_COMPASS_NOTIFICATION"

    new-instance v1, Lcom/google/googlenav/ui/ao;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/ao;-><init>(Lcom/google/googlenav/ui/ak;)V

    invoke-static {v0, v4, v1}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bb;)V

    .line 641
    :cond_10
    :goto_10
    return-void

    .line 612
    :cond_11
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 614
    invoke-virtual {v0}, LaV/h;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {v0}, LaV/h;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_10

    .line 620
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_10

    .line 625
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    .line 627
    const-string v0, "HAS_SHOWN_CALIBRATE_COMPASS_NOTIFICATION"

    const/4 v1, 0x1

    invoke-static {v0, v1, v5}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    .line 634
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x66

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x65

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_10
.end method

.method private G()Z
    .registers 4

    .prologue
    .line 765
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "GPS_FIX"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private H()V
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 780
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    if-nez v0, :cond_8

    .line 792
    :goto_7
    return-void

    .line 784
    :cond_8
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v5

    .line 785
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    invoke-static {v2}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    move-result-object v0

    .line 788
    invoke-virtual {v0, v4}, Lbf/C;->k(Z)V

    .line 791
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "GPS_FIX"

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Z)V

    goto :goto_7
.end method

.method private I()Z
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 801
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->N()J

    move-result-wide v3

    .line 802
    const-wide/16 v5, 0xfa0

    add-long/2addr v5, v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-lez v0, :cond_30

    move v0, v1

    .line 805
    :goto_1c
    iget-object v5, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->M()J

    move-result-wide v5

    const-wide/32 v7, 0x6ddd00

    add-long/2addr v5, v7

    cmp-long v3, v5, v3

    if-gez v3, :cond_32

    move v3, v1

    .line 808
    :goto_2b
    if-eqz v3, :cond_34

    if-eqz v0, :cond_34

    :goto_2f
    return v1

    :cond_30
    move v0, v2

    .line 802
    goto :goto_1c

    :cond_32
    move v3, v2

    .line 805
    goto :goto_2b

    :cond_34
    move v1, v2

    .line 808
    goto :goto_2f
.end method

.method private J()V
    .registers 3

    .prologue
    .line 1051
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    .line 1052
    return-void
.end method

.method private K()I
    .registers 4

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->r()LaH/h;

    move-result-object v1

    .line 1070
    invoke-static {v1}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    .line 1071
    if-ltz v0, :cond_2f

    .line 1073
    if-eqz v1, :cond_28

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2}, LaH/E;->e(LaN/B;)Z

    move-result v2

    if-eqz v2, :cond_28

    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v2

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v2, v1}, LaH/E;->b(LaN/B;)Z

    move-result v1

    if-nez v1, :cond_28

    .line 1075
    add-int/lit16 v0, v0, 0x275

    .line 1078
    :cond_28
    const/16 v1, 0x64

    if-le v0, v1, :cond_30

    .line 1080
    rem-int/lit8 v1, v0, 0x64

    sub-int/2addr v0, v1

    .line 1086
    :cond_2f
    :goto_2f
    return v0

    .line 1081
    :cond_30
    const/16 v1, 0xa

    if-le v0, v1, :cond_2f

    .line 1082
    rem-int/lit8 v1, v0, 0xa

    sub-int/2addr v0, v1

    goto :goto_2f
.end method

.method private L()V
    .registers 3

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    invoke-static {v0}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v0

    .line 1312
    if-eqz v0, :cond_d

    .line 1313
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->a(Lo/D;)V

    .line 1315
    :cond_d
    return-void
.end method

.method private M()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1322
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1323
    const-string v0, "c"

    .line 1329
    :goto_a
    return-object v0

    .line 1325
    :cond_b
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v0, :cond_12

    .line 1326
    const-string v0, "a"

    goto :goto_a

    .line 1329
    :cond_12
    const-string v0, "n"

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;LaH/h;)LaH/h;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    return-object p1
.end method

.method public static a(ILaN/B;)LaN/Y;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1011
    if-nez p1, :cond_4

    .line 1012
    const/4 v0, 0x0

    .line 1022
    :cond_3
    return-object v0

    .line 1014
    :cond_4
    int-to-long v0, p0

    int-to-long v2, p0

    mul-long v1, v0, v2

    .line 1015
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    .line 1017
    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    .line 1019
    :goto_24
    if-eqz v0, :cond_3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, v0}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v4

    invoke-virtual {p1, v4}, LaN/B;->b(LaN/B;)J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-lez v4, :cond_3

    .line 1020
    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v0

    goto :goto_24
.end method

.method private a(ZZ)LaN/Y;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x13

    .line 948
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_11

    .line 949
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    .line 981
    :cond_10
    :goto_10
    return-object v0

    .line 952
    :cond_11
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    .line 953
    if-eqz p1, :cond_50

    .line 956
    const/16 v0, 0xf

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    .line 958
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v1}, LaH/m;->r()LaH/h;

    move-result-object v1

    .line 959
    if-eqz v1, :cond_50

    .line 961
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2}, LaN/p;->g(LaN/B;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 962
    invoke-static {v3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    .line 966
    :cond_35
    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    if-eqz v1, :cond_50

    .line 967
    invoke-static {v3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    move-object v1, v0

    .line 972
    :goto_40
    if-eqz p2, :cond_4e

    .line 975
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->m()LaN/Y;

    move-result-object v0

    .line 976
    if-eqz v0, :cond_4e

    invoke-virtual {v1, v0}, LaN/Y;->b(LaN/Y;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_4e
    move-object v0, v1

    goto :goto_10

    :cond_50
    move-object v1, v0

    goto :goto_40
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/googlenav/ui/ak;->i:Ljava/lang/Boolean;

    return-object p1
.end method

.method private a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1097
    if-eqz p2, :cond_7

    .line 1098
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1100
    :cond_7
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1337
    const-string v0, "y"

    .line 1338
    const-string v0, "n"

    .line 1340
    const-string v1, "sw"

    invoke-static {}, Lcom/google/googlenav/ui/ak;->w()Z

    move-result v0

    if-eqz v0, :cond_59

    const-string v0, "n"

    :goto_f
    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1344
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->c()Ljava/util/Map;

    move-result-object v1

    .line 1347
    const/4 v0, 0x1

    .line 1348
    if-eqz v1, :cond_a2

    .line 1349
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_24
    :goto_24
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1350
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaH/B;

    .line 1351
    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v5

    if-eqz v5, :cond_3d

    move v2, v3

    .line 1354
    :cond_3d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1355
    const-string v5, "network"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5f

    .line 1356
    const-string v5, "sc"

    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v0

    if-eqz v0, :cond_5c

    const-string v0, "y"

    :goto_55
    invoke-static {v5, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_24

    .line 1340
    :cond_59
    const-string v0, "y"

    goto :goto_f

    .line 1356
    :cond_5c
    const-string v0, "n"

    goto :goto_55

    .line 1360
    :cond_5f
    const-string v5, "gps"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1362
    const-string v5, "sg"

    invoke-virtual {v1}, LaH/B;->a()Z

    move-result v0

    if-eqz v0, :cond_99

    const-string v0, "y"

    :goto_71
    invoke-static {v5, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1366
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    if-eqz v0, :cond_24

    .line 1369
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    invoke-virtual {v0}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v3

    :goto_83
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 1370
    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 1371
    add-int/lit8 v0, v1, 0x1

    :goto_97
    move v1, v0

    goto :goto_83

    .line 1362
    :cond_99
    const-string v0, "n"

    goto :goto_71

    .line 1374
    :cond_9c
    const-string v0, "ss"

    invoke-static {v0, v1, p1}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    goto :goto_24

    :cond_a2
    move v2, v0

    .line 1382
    :cond_a3
    const-string v1, "sl"

    if-eqz v2, :cond_ad

    const-string v0, "n"

    :goto_a9
    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1384
    return-void

    .line 1382
    :cond_ad
    const-string v0, "y"

    goto :goto_a9

    :cond_b0
    move v0, v1

    goto :goto_97
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->r:Z

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/ak;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ak;)LaH/m;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    return-object v0
.end method

.method private b(Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter

    .prologue
    .line 1450
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 1451
    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->v:J

    const-wide/high16 v4, -0x8000

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1d

    .line 1452
    const-string v2, "tl"

    iget-wide v3, p0, Lcom/google/googlenav/ui/ak;->v:J

    sub-long v3, v0, v3

    invoke-static {v2, v3, v4, p1}, Lbm/m;->a(Ljava/lang/String;JLjava/lang/StringBuilder;)V

    .line 1457
    :cond_1d
    iput-wide v0, p0, Lcom/google/googlenav/ui/ak;->v:J

    .line 1458
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/ak;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/ak;)LaH/h;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    return-object v0
.end method

.method private c(Ljava/lang/StringBuilder;)V
    .registers 4
    .parameter

    .prologue
    .line 1501
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v0

    .line 1502
    const-string v1, "o"

    invoke-static {v1, v0, p1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1506
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 1507
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/ak;->b(Ljava/lang/StringBuilder;)V

    .line 1508
    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    .line 1510
    :cond_16
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/ak;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/google/googlenav/ui/ak;->o:Z

    return p1
.end method

.method static synthetic d(Lcom/google/googlenav/ui/ak;)Lo/S;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/ak;)V
    .registers 1
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/ak;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->n:Z

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/ak;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->o:Z

    return v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/ak;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->C()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/ak;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/ak;)V
    .registers 1
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->F()V

    return-void
.end method

.method public static w()Z
    .registers 1

    .prologue
    .line 1278
    invoke-static {}, LaL/d;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, LaL/d;->k()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public A()V
    .registers 4

    .prologue
    .line 1409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1410
    const-string v1, "a"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1414
    const-string v1, "o"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1418
    const-string v1, "n"

    const-string v2, "p"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1422
    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1423
    return-void
.end method

.method public B()V
    .registers 4

    .prologue
    .line 1465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1466
    const-string v1, "a"

    const-string v2, "c"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1470
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->c(Ljava/lang/StringBuilder;)V

    .line 1471
    const-string v1, "n"

    const-string v2, "a"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1475
    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1476
    return-void
.end method

.method public a()V
    .registers 3

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lo/S;->c(Z)V

    .line 463
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    .line 464
    return-void
.end method

.method public a(FF)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v0

    if-nez v0, :cond_34

    .line 446
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0}, Lo/S;->b()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->c(F)F

    move-result v0

    .line 448
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1}, Lo/S;->e()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40a0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_34

    .line 450
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lo/S;->a(Z)V

    .line 451
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v0, p1}, Lo/S;->a(F)V

    .line 452
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->D()V

    .line 455
    :cond_34
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 459
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1163
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1190
    :cond_6
    :goto_6
    return-void

    .line 1185
    :cond_7
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->p()I

    move-result v0

    if-ne v0, p1, :cond_13

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->q()I

    move-result v0

    if-eq v0, p2, :cond_6

    .line 1187
    :cond_13
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->r()V

    goto :goto_6
.end method

.method public a(ILaH/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/an;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/googlenav/ui/an;-><init>(Lcom/google/googlenav/ui/ak;LaH/m;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 422
    return-void
.end method

.method public a(J)V
    .registers 7
    .parameter

    .prologue
    .line 1032
    iget-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    const-wide/high16 v2, -0x8000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_44

    iget-wide v0, p0, Lcom/google/googlenav/ui/ak;->p:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_44

    .line 1034
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_41

    .line 1035
    const/16 v0, 0x5f1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    .line 1036
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1039
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->z()V

    .line 1042
    :cond_41
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    .line 1044
    :cond_44
    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/am;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/am;-><init>(Lcom/google/googlenav/ui/ak;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 362
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1430
    const-string v1, "a"

    const-string v2, "l"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1434
    const-string v1, "o"

    invoke-static {v1, p1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1436
    const-string v1, "n"

    invoke-static {v1, p2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1438
    invoke-static {p1, p2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1439
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->b(Ljava/lang/StringBuilder;)V

    .line 1440
    iput-object p2, p0, Lcom/google/googlenav/ui/ak;->w:Ljava/lang/String;

    .line 1442
    :cond_21
    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1443
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1514
    if-nez p2, :cond_7

    .line 1515
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1519
    :cond_7
    const-string v0, "z"

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-static {v0, v1, p2}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    .line 1525
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/StringBuilder;)V

    .line 1528
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 1529
    if-eqz v0, :cond_2f

    .line 1530
    invoke-virtual {v0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 1531
    if-eqz v0, :cond_2f

    .line 1532
    sget-object v1, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_46

    .line 1544
    :cond_2f
    :goto_2f
    const-string v0, "y"

    .line 1545
    const-string v0, "n"

    .line 1548
    const-string v1, "rl"

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    if-eqz v0, :cond_54

    const-string v0, "y"

    :goto_3d
    invoke-static {v1, v0, p2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1554
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->t:LaH/t;

    invoke-virtual {v0, p2}, LaH/t;->a(Ljava/lang/StringBuilder;)V

    .line 1561
    return-void

    .line 1535
    :cond_46
    const-string v1, "ls"

    sget-object v2, Lcom/google/googlenav/ui/ak;->y:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_2f

    .line 1548
    :cond_54
    const-string v0, "n"

    goto :goto_3d
.end method

.method public a(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1109
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->o()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1126
    :goto_8
    return-void

    .line 1113
    :cond_9
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v3

    .line 1114
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    .line 1115
    if-eqz v2, :cond_33

    invoke-virtual {v2}, LaH/h;->b()Lo/D;

    move-result-object v2

    if-eqz v2, :cond_33

    move v2, v1

    .line 1119
    :goto_22
    if-nez v2, :cond_28

    const/16 v4, 0xa

    if-le v3, v4, :cond_2e

    :cond_28
    if-eqz v2, :cond_2f

    const/16 v2, 0x12

    if-gt v3, v2, :cond_2f

    :cond_2e
    move v0, v1

    .line 1125
    :cond_2f
    invoke-virtual {p0, v0, v1, p1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    goto :goto_8

    :cond_33
    move v2, v0

    .line 1115
    goto :goto_22
.end method

.method public a(ZZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 869
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v0

    .line 870
    iget-boolean v1, p0, Lcom/google/googlenav/ui/ak;->k:Z

    .line 871
    if-eqz v0, :cond_9c

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_9c

    .line 872
    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    .line 874
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/ak;->a(ZZ)LaN/Y;

    move-result-object v2

    .line 876
    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->r:Z

    .line 877
    iget-object v3, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v3, v0, v2}, LaN/u;->a(LaN/B;LaN/Y;)V

    .line 878
    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    if-eqz v0, :cond_2c

    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    if-eq v0, v4, :cond_2c

    iget v0, p0, Lcom/google/googlenav/ui/ak;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_49

    .line 881
    :cond_2c
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->K()I

    move-result v0

    .line 884
    if-ltz v0, :cond_47

    .line 885
    const/16 v2, 0x1cc

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 888
    invoke-direct {p0, v0, v5}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    .line 890
    :cond_47
    iput v4, p0, Lcom/google/googlenav/ui/ak;->l:I

    .line 896
    :cond_49
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_74

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v0

    if-nez v0, :cond_74

    .line 899
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    .line 901
    if-eqz v0, :cond_74

    .line 902
    invoke-virtual {v0}, Lbf/X;->bM()V

    .line 906
    :cond_74
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    .line 907
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    .line 930
    :cond_7a
    :goto_7a
    if-nez v1, :cond_ca

    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v0, :cond_ca

    .line 931
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    monitor-enter v1

    .line 932
    :try_start_83
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_89
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aq;

    .line 933
    invoke-interface {v0}, Lcom/google/googlenav/ui/aq;->a()V

    goto :goto_89

    .line 935
    :catchall_99
    move-exception v0

    monitor-exit v1
    :try_end_9b
    .catchall {:try_start_83 .. :try_end_9b} :catchall_99

    throw v0

    .line 908
    :cond_9c
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 912
    iput-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    .line 915
    const/16 v0, 0x291

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 916
    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    .line 923
    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->p:J

    const-wide/high16 v4, -0x8000

    cmp-long v0, v2, v4

    if-nez v0, :cond_7a

    .line 924
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/googlenav/ui/ak;->p:J

    goto :goto_7a

    .line 935
    :cond_c9
    :try_start_c9
    monitor-exit v1
    :try_end_ca
    .catchall {:try_start_c9 .. :try_end_ca} :catchall_99

    .line 937
    :cond_ca
    return-void
.end method

.method public a(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 848
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_d

    .line 849
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    .line 852
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public b()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 471
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1}, Lo/S;->f()Lo/D;

    move-result-object v1

    .line 472
    if-nez v1, :cond_f

    .line 475
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1, v0}, Lo/S;->b(Z)V

    .line 487
    :goto_e
    return-void

    .line 479
    :cond_f
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v2

    .line 480
    invoke-virtual {v2}, Ln/q;->e()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 483
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-virtual {v1, v0}, Lo/S;->b(Z)V

    goto :goto_e

    .line 485
    :cond_23
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->g:Lo/S;

    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v3

    invoke-virtual {v3, v1}, Ln/q;->c(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_30

    const/4 v0, 0x1

    :cond_30
    invoke-virtual {v2, v0}, Lo/S;->b(Z)V

    goto :goto_e
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1579
    if-eqz p1, :cond_e

    .line 1580
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    .line 1584
    :goto_d
    return-void

    .line 1582
    :cond_e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    goto :goto_d
.end method

.method public c()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 502
    sget-object v2, LaE/d;->a:LaE/d;

    invoke-virtual {v2}, LaE/d;->e()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 503
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->d()V

    .line 552
    :goto_11
    return-void

    .line 514
    :cond_12
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v2

    .line 516
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v3

    .line 518
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->c()Z

    move-result v4

    if-eqz v4, :cond_7b

    .line 519
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->E()Z

    move-result v4

    if-eqz v4, :cond_72

    .line 520
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->b()Z

    move-result v4

    if-nez v4, :cond_35

    .line 521
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->F()V

    .line 526
    :cond_35
    invoke-virtual {v2}, Lcom/google/googlenav/mylocationnotifier/k;->a()Z

    move-result v4

    if-nez v4, :cond_70

    .line 528
    :goto_3b
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->f()V

    .line 540
    :goto_40
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4}, Lbf/am;->x()Lbf/bU;

    move-result-object v4

    if-nez v4, :cond_58

    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4}, Lbf/am;->v()Lbf/O;

    move-result-object v4

    if-eqz v4, :cond_59

    :cond_58
    move v0, v1

    .line 545
    :cond_59
    if-eqz v0, :cond_68

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aJ()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 547
    invoke-virtual {v2}, Lcom/google/googlenav/mylocationnotifier/k;->g()V

    .line 550
    :cond_68
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->M()Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-virtual {p0, v3, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11

    :cond_70
    move v0, v1

    .line 526
    goto :goto_3b

    .line 531
    :cond_72
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    .line 532
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->e:Lcom/google/googlenav/android/A;

    invoke-virtual {v4}, Lcom/google/googlenav/android/A;->h()V

    goto :goto_40

    .line 536
    :cond_7b
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    goto :goto_40
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->j()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 657
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->j:Z

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->F()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->G()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->I()Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public f()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 668
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    if-nez v0, :cond_6

    .line 740
    :goto_5
    return-void

    .line 671
    :cond_6
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/googlenav/ui/ak;->q:J

    .line 673
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->f:LaH/h;

    invoke-static {v0}, LaH/h;->a(LaH/h;)LaN/B;

    move-result-object v2

    .line 683
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->e()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 686
    invoke-virtual {p0, v1, v1, v1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    .line 690
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->G()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 691
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->H()V

    .line 694
    :cond_2c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aJ()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 695
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v0

    .line 698
    invoke-virtual {v0}, Lcom/google/googlenav/mylocationnotifier/k;->g()V

    .line 702
    :cond_3f
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->j:Z

    .line 703
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v3

    .line 704
    invoke-virtual {v3}, LaH/h;->b()Lo/D;

    move-result-object v0

    if-eqz v0, :cond_81

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    if-nez v0, :cond_81

    move v0, v1

    .line 713
    :goto_52
    iget-boolean v4, p0, Lcom/google/googlenav/ui/ak;->k:Z

    if-eqz v4, :cond_83

    .line 714
    iget-object v4, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v4}, LaN/u;->c()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v4}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_64

    if-eqz v0, :cond_74

    .line 715
    :cond_64
    iput-boolean v1, p0, Lcom/google/googlenav/ui/ak;->r:Z

    .line 716
    const/4 v1, 0x0

    .line 718
    if-eqz v0, :cond_a5

    .line 719
    const/16 v0, 0x13

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    .line 721
    :goto_6f
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1, v2, v0}, LaN/u;->a(LaN/B;LaN/Y;)V

    .line 723
    :cond_74
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    .line 738
    :cond_77
    :goto_77
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    .line 739
    invoke-virtual {v3}, LaH/h;->b()Lo/D;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    goto :goto_5

    .line 704
    :cond_81
    const/4 v0, 0x0

    goto :goto_52

    .line 730
    :cond_83
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v1

    if-eqz v1, :cond_77

    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    if-eqz v1, :cond_9f

    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v1

    if-eqz v1, :cond_9f

    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->s:Lo/D;

    invoke-virtual {v1, v2}, Ln/q;->d(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_a1

    :cond_9f
    if-eqz v0, :cond_77

    .line 734
    :cond_a1
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->L()V

    goto :goto_77

    :cond_a5
    move-object v0, v1

    goto :goto_6f
.end method

.method public g()[Ljava/lang/String;
    .registers 5

    .prologue
    .line 747
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->K()I

    move-result v1

    .line 748
    if-ltz v1, :cond_2a

    .line 749
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->n()Z

    move-result v0

    if-eqz v0, :cond_27

    const/16 v0, 0x1ce

    .line 751
    :goto_e
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 756
    :goto_20
    const-string v1, "\n"

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 749
    :cond_27
    const/16 v0, 0x5df

    goto :goto_e

    .line 754
    :cond_2a
    const/16 v0, 0x5e0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_20
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public i()LaH/h;
    .registers 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    return-object v0
.end method

.method public j()LaN/B;
    .registers 2

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->a(LaH/h;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 833
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 834
    if-eqz v0, :cond_e

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 839
    iget-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    return v0
.end method

.method public m()LaN/Y;
    .registers 3

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 994
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_1d

    if-eqz v0, :cond_1d

    .line 995
    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v1

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/ak;->a(ILaN/B;)LaN/Y;

    move-result-object v0

    .line 998
    :goto_1c
    return-object v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public n()Z
    .registers 5

    .prologue
    .line 1060
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/ak;->q:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public o()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1133
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v2}, LaH/m;->e()Z

    move-result v2

    if-eqz v2, :cond_37

    .line 1135
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->e(Z)V

    .line 1137
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v1

    .line 1138
    invoke-static {v1}, LaH/x;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-static {v1}, LaH/x;->b(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2d

    .line 1143
    const/16 v1, 0x116

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    .line 1151
    :goto_2c
    return v0

    .line 1147
    :cond_2d
    const/16 v1, 0x1cb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Z)V

    goto :goto_2c

    :cond_37
    move v0, v1

    .line 1151
    goto :goto_2c
.end method

.method public onGpsStatusChanged(I)V
    .registers 4
    .parameter

    .prologue
    .line 1569
    packed-switch p1, :pswitch_data_14

    .line 1576
    :goto_3
    return-void

    .line 1571
    :pswitch_4
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/ak;->u:Landroid/location/GpsStatus;

    goto :goto_3

    .line 1569
    :pswitch_data_14
    .packed-switch 0x4
        :pswitch_4
    .end packed-switch
.end method

.method p()I
    .registers 2

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->t()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method q()I
    .registers 2

    .prologue
    .line 1199
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->b:LaN/p;

    invoke-virtual {v0}, LaN/p;->s()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public r()V
    .registers 4

    .prologue
    .line 1204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->k:Z

    .line 1205
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    monitor-enter v1

    .line 1206
    :try_start_6
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aq;

    .line 1207
    invoke-interface {v0}, Lcom/google/googlenav/ui/aq;->b()V

    goto :goto_c

    .line 1209
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_6 .. :try_end_1e} :catchall_1c

    throw v0

    :cond_1f
    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1c

    .line 1210
    return-void
.end method

.method public s()LaH/m;
    .registers 2

    .prologue
    .line 1219
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    return-object v0
.end method

.method public t()Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 1229
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    if-nez v1, :cond_e

    .line 1243
    :cond_d
    :goto_d
    return v0

    .line 1233
    :cond_e
    iget-object v1, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    invoke-virtual {v1}, LaN/u;->n()I

    move-result v1

    .line 1234
    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->c:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v3}, LaH/m;->s()LaH/h;

    move-result-object v3

    invoke-static {v3}, LaH/h;->a(Landroid/location/Location;)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaN/u;->a(ILaN/B;)F

    move-result v2

    const/high16 v3, 0x4000

    mul-float/2addr v2, v3

    float-to-long v2, v2

    .line 1243
    div-int/lit8 v1, v1, 0x2

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method public u()Z
    .registers 2

    .prologue
    .line 1258
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->v()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/googlenav/ui/ak;->w()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/google/googlenav/ui/ak;->a:LaH/m;

    invoke-interface {v0}, LaH/m;->f()Z

    move-result v0

    return v0
.end method

.method public x()V
    .registers 2

    .prologue
    .line 1283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/ak;->j:Z

    .line 1284
    return-void
.end method

.method public y()V
    .registers 7

    .prologue
    .line 1298
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/googlenav/ui/ak;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->N()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1a

    .line 1308
    :goto_19
    return-void

    .line 1304
    :cond_1a
    invoke-virtual {p0}, Lcom/google/googlenav/ui/ak;->x()V

    .line 1307
    invoke-direct {p0}, Lcom/google/googlenav/ui/ak;->J()V

    goto :goto_19
.end method

.method public z()V
    .registers 4

    .prologue
    .line 1391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1392
    const-string v1, "a"

    const-string v2, "d"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1396
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/ak;->c(Ljava/lang/StringBuilder;)V

    .line 1397
    const-string v1, "n"

    const-string v2, "b"

    invoke-static {v1, v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1401
    const-string v1, "u"

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1402
    return-void
.end method
