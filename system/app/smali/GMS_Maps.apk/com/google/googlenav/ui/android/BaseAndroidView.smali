.class public abstract Lcom/google/googlenav/ui/android/BaseAndroidView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected b:Lcom/google/googlenav/android/i;

.field protected c:Lcom/google/googlenav/ui/android/b;

.field protected d:Z

.field protected final e:LaD/g;

.field private f:Landroid/view/View$OnTouchListener;

.field private g:Ljava/util/List;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->d:Z

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    .line 60
    iput-object p1, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->a:Landroid/content/Context;

    .line 61
    new-instance v0, LaD/g;

    invoke-direct {v0}, LaD/g;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->e:LaD/g;

    .line 62
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
.end method

.method public declared-synchronized a(Lcom/google/googlenav/ui/android/D;)V
    .registers 3
    .parameter

    .prologue
    .line 156
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 157
    monitor-exit p0

    return-void

    .line 156
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b()V
.end method

.method public declared-synchronized b(Lcom/google/googlenav/ui/android/D;)V
    .registers 3
    .parameter

    .prologue
    .line 161
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 162
    monitor-exit p0

    return-void

    .line 161
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 141
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->C()V

    .line 142
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_d
.end method

.method public e()V
    .registers 1

    .prologue
    .line 198
    return-void
.end method

.method public abstract f()V
.end method

.method public g()V
    .registers 1

    .prologue
    .line 204
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 212
    return-void
.end method

.method public l()V
    .registers 1

    .prologue
    .line 219
    return-void
.end method

.method protected m()V
    .registers 2

    .prologue
    .line 69
    new-instance v0, Lcom/google/googlenav/ui/android/C;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/C;-><init>(Lcom/google/googlenav/ui/android/BaseAndroidView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->post(Ljava/lang/Runnable;)Z

    .line 86
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 170
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/b;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_5
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/android/b;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_5
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->c:Lcom/google/googlenav/ui/android/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/b;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_5
.end method

.method protected onSizeChanged(IIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 147
    monitor-enter p0

    .line 148
    :try_start_4
    iget-object v0, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/D;

    .line 149
    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/ui/android/D;->b(II)V

    goto :goto_a

    .line 151
    :catchall_1a
    move-exception v0

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_1a

    throw v0

    :cond_1d
    :try_start_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1a

    .line 152
    return-void
.end method

.method public setIsLongpressEnabledForTest(Z)V
    .registers 3
    .parameter

    .prologue
    .line 223
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 224
    check-cast p0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->y()LaD/g;

    move-result-object v0

    invoke-virtual {v0, p1}, LaD/g;->a(Z)V

    .line 226
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .registers 2
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/googlenav/ui/android/BaseAndroidView;->f:Landroid/view/View$OnTouchListener;

    .line 122
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    return-void
.end method
