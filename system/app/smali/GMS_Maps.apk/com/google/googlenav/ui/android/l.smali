.class Lcom/google/googlenav/ui/android/l;
.super Lcom/google/android/maps/driveabout/vector/VectorMapView;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private final c:LaO/a;

.field private d:Lcom/google/android/maps/driveabout/vector/aZ;

.field private final e:Lcom/google/android/maps/driveabout/vector/aZ;

.field private f:Lcom/google/android/maps/driveabout/vector/aq;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    .line 104
    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/driveabout/vector/VectorMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 105
    iput-object p4, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    .line 106
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/l;->B()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setDefaultLabelTheme(LG/a;)V

    .line 107
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/l;->C()V

    .line 108
    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(I)V

    .line 110
    invoke-virtual {p4}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 111
    sget-object v0, LA/c;->c:LA/c;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    .line 114
    invoke-super {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    .line 120
    :cond_28
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->q()Lg/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/c;LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->e:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->e:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 130
    invoke-virtual {p4}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_73

    sget-object v0, LA/c;->p:LA/c;

    :goto_4d
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 133
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 135
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 136
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v1

    invoke-static {p2, v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/Context;Landroid/content/res/Resources;Ln/q;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aq;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->f:Lcom/google/android/maps/driveabout/vector/aq;

    .line 145
    :cond_72
    return-void

    .line 130
    :cond_73
    sget-object v0, LA/c;->o:LA/c;

    goto :goto_4d
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;Lcom/google/googlenav/ui/android/f;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/android/l;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;)V

    return-void
.end method

.method private B()LG/a;
    .registers 5

    .prologue
    .line 149
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 150
    sget-object v0, LG/a;->u:LG/a;

    .line 157
    :goto_c
    return-object v0

    .line 153
    :cond_d
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1e

    .line 154
    sget-object v0, LG/a;->v:LG/a;

    goto :goto_c

    .line 157
    :cond_1e
    sget-object v0, LG/a;->t:LG/a;

    goto :goto_c
.end method

.method private C()V
    .registers 6

    .prologue
    .line 161
    sget-object v0, LaE/a;->a:LaE/a;

    invoke-virtual {v0}, LaE/a;->e()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 162
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->x()LG/a;

    move-result-object v0

    const/4 v1, 0x2

    const v2, 0x3e4ccccd

    const/4 v3, 0x0

    sget-object v4, LaE/a;->b:Lcom/google/android/maps/driveabout/vector/aX;

    invoke-static {v0, v1, v2, v3, v4}, LG/a;->a(LG/a;IFFLcom/google/android/maps/driveabout/vector/aX;)LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setLabelTheme(LG/a;)V

    .line 168
    :goto_1a
    return-void

    .line 166
    :cond_1b
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setLabelTheme(LG/a;)V

    goto :goto_1a
.end method


# virtual methods
.method public A()Lcom/google/android/maps/driveabout/vector/aq;
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->f:Lcom/google/android/maps/driveabout/vector/aq;

    return-object v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    invoke-virtual {v0}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 194
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->o:LA/c;

    if-ne v0, v1, :cond_2c

    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 196
    sget-object v0, LA/c;->p:LA/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 198
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 208
    :cond_2c
    :goto_2c
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    .line 209
    return-void

    .line 200
    :cond_30
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->p:LA/c;

    if-ne v0, v1, :cond_2c

    .line 201
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 202
    sget-object v0, LA/c;->o:LA/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 204
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_2c
.end method

.method public j()V
    .registers 2

    .prologue
    .line 217
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j()V

    .line 220
    invoke-static {}, Lah/e;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 221
    invoke-static {}, Lah/e;->b()Lah/e;

    move-result-object v0

    invoke-virtual {v0}, Lah/e;->d()V

    .line 223
    :cond_10
    return-void
.end method

.method public o_()V
    .registers 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o_()V

    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    invoke-virtual {v0}, LaO/a;->x()V

    .line 179
    return-void
.end method
