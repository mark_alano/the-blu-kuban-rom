.class Lcom/google/googlenav/ui/android/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/au;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/r;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/android/r;)V
    .registers 2
    .parameter

    .prologue
    .line 542
    iput-object p1, p0, Lcom/google/googlenav/ui/android/z;->a:Lcom/google/googlenav/ui/android/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543
    return-void
.end method


# virtual methods
.method public a(Landroid/view/Menu;)Z
    .registers 6
    .parameter

    .prologue
    .line 562
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_22

    .line 563
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 564
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f10033e

    if-ne v2, v3, :cond_1f

    .line 565
    iget-object v2, p0, Lcom/google/googlenav/ui/android/z;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/r;->d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/googlenav/ui/android/w;->i:Z

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 562
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 568
    :cond_22
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 548
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_c

    .line 557
    :cond_b
    return v4

    .line 551
    :cond_c
    const v0, 0x7f110001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 552
    const/4 v0, 0x0

    :goto_13
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 553
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 554
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v3, 0x7f10033e

    if-ne v1, v3, :cond_32

    const/16 v1, 0x347

    :goto_28
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 554
    :cond_32
    const/16 v1, 0x69

    goto :goto_28
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 573
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_16

    .line 583
    :goto_7
    const/4 v0, 0x1

    return v0

    .line 575
    :sswitch_9
    iget-object v0, p0, Lcom/google/googlenav/ui/android/z;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->b(Lcom/google/googlenav/ui/android/r;)V

    goto :goto_7

    .line 580
    :sswitch_f
    iget-object v0, p0, Lcom/google/googlenav/ui/android/z;->a:Lcom/google/googlenav/ui/android/r;

    invoke-static {v0}, Lcom/google/googlenav/ui/android/r;->c(Lcom/google/googlenav/ui/android/r;)V

    goto :goto_7

    .line 573
    nop

    :sswitch_data_16
    .sparse-switch
        0x102002c -> :sswitch_f
        0x7f100171 -> :sswitch_f
        0x7f10033e -> :sswitch_9
    .end sparse-switch
.end method
