.class public Lcom/google/googlenav/ui/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;

.field private final b:Lbf/am;

.field private final c:Lcom/google/googlenav/android/aa;

.field private d:Z

.field private e:LaM/g;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lbf/am;Lcom/google/googlenav/android/aa;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/av;->d:Z

    .line 30
    iput-object p1, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    .line 31
    iput-object p2, p0, Lcom/google/googlenav/ui/av;->b:Lbf/am;

    .line 32
    iput-object p3, p0, Lcom/google/googlenav/ui/av;->c:Lcom/google/googlenav/android/aa;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/av;)Lcom/google/googlenav/ui/wizard/jv;
    .registers 2
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/av;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/googlenav/ui/av;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/av;)Lbf/am;
    .registers 2
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->b:Lbf/am;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/av;)Z
    .registers 2
    .parameter

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/googlenav/ui/av;->d:Z

    return v0
.end method


# virtual methods
.method public a()LaM/g;
    .registers 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    if-nez v0, :cond_c

    .line 42
    new-instance v0, Lcom/google/googlenav/ui/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/ax;-><init>(Lcom/google/googlenav/ui/av;Lcom/google/googlenav/ui/aw;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    .line 44
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->e:LaM/g;

    return-object v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 51
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 52
    iget-object v0, p0, Lcom/google/googlenav/ui/av;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->g()V

    .line 62
    :goto_f
    return-void

    .line 54
    :cond_10
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, Lcom/google/googlenav/ui/av;->c:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/aw;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/aw;-><init>(Lcom/google/googlenav/ui/av;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_f
.end method
