.class Lcom/google/googlenav/ui/bm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:B

.field private final b:C

.field private final c:Lam/f;


# direct methods
.method constructor <init>(BCLam/f;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2819
    iput-byte p1, p0, Lcom/google/googlenav/ui/bm;->a:B

    .line 2820
    iput-char p2, p0, Lcom/google/googlenav/ui/bm;->b:C

    .line 2821
    iput-object p3, p0, Lcom/google/googlenav/ui/bm;->c:Lam/f;

    .line 2822
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 2836
    instance-of v0, p1, Lcom/google/googlenav/ui/bm;

    if-eqz v0, :cond_20

    iget-byte v1, p0, Lcom/google/googlenav/ui/bm;->a:B

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ui/bm;

    iget-byte v0, v0, Lcom/google/googlenav/ui/bm;->a:B

    if-ne v1, v0, :cond_20

    iget-char v1, p0, Lcom/google/googlenav/ui/bm;->b:C

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ui/bm;

    iget-char v0, v0, Lcom/google/googlenav/ui/bm;->b:C

    if-ne v1, v0, :cond_20

    iget-object v0, p0, Lcom/google/googlenav/ui/bm;->c:Lam/f;

    check-cast p1, Lcom/google/googlenav/ui/bm;

    iget-object v1, p1, Lcom/google/googlenav/ui/bm;->c:Lam/f;

    if-ne v0, v1, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 2826
    iget-byte v0, p0, Lcom/google/googlenav/ui/bm;->a:B

    add-int/lit8 v0, v0, 0x1f

    .line 2827
    mul-int/lit8 v0, v0, 0x1f

    iget-char v1, p0, Lcom/google/googlenav/ui/bm;->b:C

    add-int/2addr v0, v1

    .line 2828
    iget-object v1, p0, Lcom/google/googlenav/ui/bm;->c:Lam/f;

    if-eqz v1, :cond_16

    .line 2829
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/googlenav/ui/bm;->c:Lam/f;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2831
    :cond_16
    return v0
.end method
