.class public Lcom/google/googlenav/ui/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[B

.field b:Lam/f;

.field c:J

.field d:C


# direct methods
.method private constructor <init>([BJC)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    .line 603
    iput-char p4, p0, Lcom/google/googlenav/ui/o;->d:C

    .line 604
    iput-wide p2, p0, Lcom/google/googlenav/ui/o;->c:J

    .line 605
    iput-object p1, p0, Lcom/google/googlenav/ui/o;->a:[B

    .line 608
    return-void
.end method

.method synthetic constructor <init>([BJCLcom/google/googlenav/ui/n;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 596
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/o;-><init>([BJC)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/o;)C
    .registers 2
    .parameter

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/google/googlenav/ui/o;->c()C

    move-result v0

    return v0
.end method

.method private b()Lam/f;
    .registers 5

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    if-nez v0, :cond_18

    .line 614
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/o;->a:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/o;->a:[B

    array-length v3, v3

    invoke-interface {v0, v1, v2, v3}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    .line 618
    :cond_18
    iget-object v0, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/o;)Lam/f;
    .registers 2
    .parameter

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/google/googlenav/ui/o;->b()Lam/f;

    move-result-object v0

    return-object v0
.end method

.method private c()C
    .registers 2

    .prologue
    .line 622
    iget-char v0, p0, Lcom/google/googlenav/ui/o;->d:C

    return v0
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/googlenav/ui/o;->a:[B

    array-length v0, v0

    .line 628
    iget-object v1, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    if-eqz v1, :cond_e

    .line 629
    iget-object v1, p0, Lcom/google/googlenav/ui/o;->b:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 631
    :cond_e
    return v0
.end method
