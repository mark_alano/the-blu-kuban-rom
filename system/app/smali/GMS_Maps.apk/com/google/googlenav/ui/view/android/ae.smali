.class public Lcom/google/googlenav/ui/view/android/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/android/S;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/android/S;)V
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    .line 43
    return-void
.end method

.method public static a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ui/aW;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/util/List;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p3, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 67
    if-nez p1, :cond_e

    .line 68
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 74
    :goto_d
    return-object v0

    .line 70
    :cond_e
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_d
.end method

.method public static a(ILjava/util/List;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/ae;)Lcom/google/googlenav/ui/view/android/S;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/ui/android/ac;
    .registers 3
    .parameter

    .prologue
    .line 110
    new-instance v0, Lcom/google/googlenav/ui/view/android/ag;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/ag;-><init>(Lcom/google/googlenav/ui/view/android/ae;I)V

    return-object v0
.end method

.method public a()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 81
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 82
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    const v1, 0x7f1000fb

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 83
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/ae;->a:Lcom/google/googlenav/ui/view/android/S;

    iget-object v1, v1, Lcom/google/googlenav/ui/view/android/S;->f:Lcom/google/googlenav/ui/e;

    .line 84
    if-eqz v0, :cond_2d

    .line 85
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    new-instance v2, Lcom/google/googlenav/ui/view/android/af;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/ui/view/android/af;-><init>(Lcom/google/googlenav/ui/view/android/ae;Lcom/google/googlenav/ui/e;)V

    .line 98
    new-instance v1, Lcom/google/googlenav/ui/view/a;

    invoke-direct {v1, v3, v3}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 102
    :cond_2d
    return-void
.end method
