.class public final enum Lcom/google/googlenav/ui/view/android/bc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/googlenav/ui/view/android/bc;

.field public static final enum b:Lcom/google/googlenav/ui/view/android/bc;

.field private static final synthetic c:[Lcom/google/googlenav/ui/view/android/bc;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/google/googlenav/ui/view/android/bc;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/bc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    .line 34
    new-instance v0, Lcom/google/googlenav/ui/view/android/bc;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/view/android/bc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/view/android/bc;->b:Lcom/google/googlenav/ui/view/android/bc;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/googlenav/ui/view/android/bc;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->a:Lcom/google/googlenav/ui/view/android/bc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/googlenav/ui/view/android/bc;->b:Lcom/google/googlenav/ui/view/android/bc;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/ui/view/android/bc;->c:[Lcom/google/googlenav/ui/view/android/bc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bc;
    .registers 2
    .parameter

    .prologue
    .line 29
    const-class v0, Lcom/google/googlenav/ui/view/android/bc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/bc;

    return-object v0
.end method

.method public static values()[Lcom/google/googlenav/ui/view/android/bc;
    .registers 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/googlenav/ui/view/android/bc;->c:[Lcom/google/googlenav/ui/view/android/bc;

    invoke-virtual {v0}, [Lcom/google/googlenav/ui/view/android/bc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ui/view/android/bc;

    return-object v0
.end method
