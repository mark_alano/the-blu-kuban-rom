.class Lcom/google/googlenav/ui/view/android/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/bB;


# instance fields
.field A:Landroid/widget/ImageView;

.field B:Lcom/google/googlenav/ui/bD;

.field C:Lcom/google/googlenav/ui/view/android/bw;

.field D:Lcom/google/googlenav/ui/view/android/bw;

.field E:Lcom/google/googlenav/ui/view/android/bw;

.field F:Lcom/google/googlenav/ui/view/android/bu;

.field G:Lcom/google/googlenav/ui/view/android/bx;

.field H:Lcom/google/googlenav/ui/view/android/by;

.field I:Lcom/google/googlenav/ui/view/android/bz;

.field J:Lcom/google/googlenav/ui/view/android/bz;

.field a:Landroid/view/View;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/CheckBox;

.field d:[Landroid/widget/TextView;

.field e:[Landroid/widget/TextView;

.field f:[Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Lcom/google/googlenav/ui/view/android/DistanceView;

.field i:Lcom/google/googlenav/ui/view/android/HeadingView;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/TextView;

.field p:Landroid/view/View;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/TextView;

.field s:Landroid/widget/TextView;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/ImageView;

.field x:Landroid/widget/ImageView;

.field y:Landroid/widget/LinearLayout;

.field z:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    .line 461
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    .line 462
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 512
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_a

    .line 513
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 515
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_16

    .line 516
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 517
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    .line 519
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_1e

    .line 520
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 522
    :cond_1e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    if-eqz v0, :cond_26

    .line 523
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bx;->a:Ljava/lang/String;

    .line 525
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    if-eqz v0, :cond_2e

    .line 526
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/by;->a:Lcom/google/googlenav/ui/view/a;

    .line 528
    :cond_2e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_36

    .line 529
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    .line 531
    :cond_36
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_3e

    .line 532
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    .line 534
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    if-eqz v0, :cond_46

    .line 535
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bu;->a:Ljava/lang/String;

    .line 537
    :cond_46
    return-void
.end method
