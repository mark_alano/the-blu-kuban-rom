.class public Lcom/google/googlenav/ui/view/android/j;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/p;

.field private b:Lcom/google/googlenav/suggest/android/SuggestView;

.field private c:Lcom/google/googlenav/suggest/android/SuggestView;

.field private d:Landroid/view/View;

.field private l:Landroid/widget/RadioGroup;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Lo/D;

.field private q:LaN/B;

.field private final r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Lcom/google/googlenav/j;

.field private w:Z

.field private final x:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/p;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 149
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_23

    const v0, 0x7f0f0018

    :goto_d
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 137
    new-instance v0, Lcom/google/googlenav/ui/view/android/k;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/k;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->x:Ljava/lang/Runnable;

    .line 151
    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    .line 154
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gR;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 155
    return-void

    .line 149
    :cond_23
    const v0, 0x7f0f001f

    goto :goto_d
.end method

.method private A()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 826
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clear()V

    .line 829
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 833
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 834
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 836
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 837
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 850
    :cond_36
    :goto_36
    return-void

    .line 841
    :cond_37
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 842
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 844
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 845
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_36
.end method

.method private B()Z
    .registers 3

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    if-nez v0, :cond_20

    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    if-nez v0, :cond_15

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    if-nez v0, :cond_20

    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_22

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    if-eqz v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private static a(Lax/y;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 802
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gR;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 803
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 804
    const/4 v0, 0x3

    invoke-virtual {p0}, Lax/y;->q()Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 806
    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 807
    const/4 v0, 0x1

    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 810
    :cond_22
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_51

    .line 811
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v3

    invoke-virtual {p0}, Lax/y;->m()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_52

    invoke-virtual {p0}, Lax/y;->m()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_3a
    invoke-virtual {p0}, Lax/y;->n()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_48

    invoke-virtual {p0}, Lax/y;->n()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_48
    const/4 v4, -0x1

    invoke-static {v3, v0, v1, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 816
    const/4 v1, 0x2

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 819
    :cond_51
    return-object v2

    :cond_52
    move v0, v1

    .line 811
    goto :goto_3a
.end method

.method private a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;
    .registers 3
    .parameter

    .prologue
    .line 998
    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/w;

    return-object v0
.end method

.method private a(Lax/y;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 748
    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 749
    invoke-static {v0}, Lau/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 175
    const v0, 0x7f100173

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x108

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 177
    const v0, 0x7f100176

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x606

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 179
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x5c8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 181
    const v0, 0x7f100175

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 183
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 389
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    return-void
.end method

.method private a(Lcom/google/googlenav/suggest/android/SuggestView;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 703
    sget-object v0, Lcom/google/googlenav/ui/aV;->aE:Lcom/google/googlenav/ui/aV;

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 707
    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 708
    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    .line 709
    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 711
    invoke-virtual {p1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    .line 712
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;)V
    .registers 1
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->v()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->f(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    return-void
.end method

.method private a(ZLandroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V

    .line 694
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->n()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 695
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    .line 697
    :cond_14
    return-void
.end method

.method private a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 581
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    .line 583
    if-nez p2, :cond_1a

    .line 584
    const/4 v0, 0x1

    :try_start_7
    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setSuggestEnabled(Z)V

    .line 585
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    .line 586
    const-string v0, ""

    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_57

    .line 615
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    .line 617
    :goto_19
    return-void

    .line 590
    :cond_1a
    :try_start_1a
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 593
    invoke-virtual {p2}, Lax/y;->o()Z

    move-result v1

    invoke-virtual {p3, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSuggestEnabled(Z)V

    .line 594
    invoke-virtual {p2}, Lax/y;->o()Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 595
    if-eqz p1, :cond_3b

    .line 598
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    .line 599
    invoke-direct {p0, p3, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;Ljava/lang/String;)V
    :try_end_38
    .catchall {:try_start_1a .. :try_end_38} :catchall_57

    .line 615
    :cond_38
    :goto_38
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    goto :goto_19

    .line 600
    :cond_3b
    :try_start_3b
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/w;->a()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 605
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    .line 606
    invoke-virtual {p3}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aE:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/text/Editable;Lcom/google/googlenav/ui/aV;)V
    :try_end_56
    .catchall {:try_start_3b .. :try_end_56} :catchall_57

    goto :goto_38

    .line 615
    :catchall_57
    move-exception v0

    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    throw v0

    .line 609
    :cond_5b
    :try_start_5b
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/view/android/j;->a(Lcom/google/googlenav/suggest/android/SuggestView;)Lcom/google/googlenav/ui/view/android/w;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/w;->a(Z)V

    .line 610
    sget-object v1, Lcom/google/googlenav/ui/aV;->aF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 611
    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    .line 612
    const/4 v0, 0x0

    invoke-virtual {p3}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V
    :try_end_78
    .catchall {:try_start_5b .. :try_end_78} :catchall_57

    goto :goto_38
.end method

.method private a(LaN/B;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 644
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p1, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_36

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    .line 652
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    if-eqz v0, :cond_33

    .line 653
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    invoke-static {v0, v2}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 654
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;Lax/y;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 655
    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    .line 659
    :cond_33
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    return v0

    :cond_36
    move v0, v1

    .line 644
    goto :goto_f
.end method

.method private a(Lax/y;Lax/y;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 620
    invoke-virtual {p1}, Lax/y;->l()Lo/D;

    move-result-object v1

    .line 621
    invoke-virtual {p2}, Lax/y;->l()Lo/D;

    move-result-object v2

    .line 622
    if-eqz v1, :cond_d

    if-nez v2, :cond_e

    .line 637
    :cond_d
    :goto_d
    return v0

    .line 626
    :cond_e
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v3

    .line 627
    if-eqz v3, :cond_d

    .line 631
    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v3, v1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v1

    .line 632
    invoke-virtual {v2}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v3, v2}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v2

    .line 633
    if-eqz v1, :cond_d

    if-eqz v2, :cond_d

    .line 637
    invoke-virtual {v1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v2}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_d
.end method

.method private b(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const v2, 0x7f10016d

    const/4 v1, 0x1

    .line 454
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(Z)V

    .line 455
    invoke-direct {p0, v1, p1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLandroid/view/View;)V

    .line 458
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v0, v0, Lcom/google/googlenav/ui/view/p;->b:I

    packed-switch v0, :pswitch_data_b8

    .line 474
    :goto_11
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 475
    const v0, 0x7f100177

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 476
    if-eqz v0, :cond_29

    .line 477
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 479
    :cond_29
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    .line 519
    :goto_2d
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->c(Landroid/view/View;)V

    .line 520
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->A()V

    .line 521
    return-void

    .line 460
    :pswitch_34
    const v0, 0x7f100173

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_11

    .line 463
    :pswitch_41
    const v0, 0x7f100174

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_11

    .line 466
    :pswitch_4e
    const v0, 0x7f100176

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_11

    .line 469
    :pswitch_5b
    const v0, 0x7f100175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_11

    .line 481
    :cond_68
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_a3

    .line 482
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 483
    new-instance v1, Lcom/google/googlenav/ui/view/android/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/u;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492
    const v1, 0x7f100179

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 493
    const/16 v1, 0x1b9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    const v0, 0x7f100178

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/v;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/v;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 517
    :goto_9f
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->e(Landroid/view/View;)V

    goto :goto_2d

    .line 505
    :cond_a3
    const/16 v0, 0x1ca

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/android/l;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/l;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-static {v2, v0, v1, p1}, Lcom/google/googlenav/ui/view/android/ae;->a(ILcom/google/googlenav/ui/aW;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;

    goto :goto_9f

    .line 458
    :pswitch_data_b8
    .packed-switch 0x0
        :pswitch_34
        :pswitch_41
        :pswitch_4e
        :pswitch_5b
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/j;)V
    .registers 1
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->A()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->c(Landroid/view/View;)V

    return-void
.end method

.method private b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(ZLax/y;Lcom/google/googlenav/suggest/android/SuggestView;)V

    .line 689
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/j;)Lcom/google/googlenav/suggest/android/SuggestView;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 774
    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    .line 775
    invoke-virtual {v0}, Lax/o;->d()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 776
    new-instance v1, Lcom/google/googlenav/ui/view/android/n;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/android/n;-><init>(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lax/o;->a(Lax/r;)V

    .line 787
    :goto_12
    return-void

    .line 785
    :cond_13
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->d(Landroid/view/View;)V

    goto :goto_12
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/j;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->d(Landroid/view/View;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/android/j;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->x:Ljava/lang/Runnable;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->e()Z

    move-result v0

    if-nez v0, :cond_c

    .line 792
    invoke-static {p1}, Lcom/google/googlenav/ui/f;->a(Landroid/view/View;)V

    .line 799
    :goto_b
    return-void

    .line 794
    :cond_c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->o()Lcom/google/googlenav/ui/view/p;

    move-result-object v0

    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    invoke-virtual {v1, v2}, Lax/o;->a(I)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;Ljava/util/Vector;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/view/q;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/android/S;Landroid/view/View;Lcom/google/googlenav/ui/view/q;)V

    goto :goto_b
.end method

.method private e(I)Lcom/google/googlenav/ui/android/ac;
    .registers 3
    .parameter

    .prologue
    .line 753
    new-instance v0, Lcom/google/googlenav/ui/view/android/m;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/view/android/m;-><init>(Lcom/google/googlenav/ui/view/android/j;I)V

    return-object v0
.end method

.method private e(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 905
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 915
    :cond_a
    :goto_a
    return-void

    .line 909
    :cond_b
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 910
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->invalidateOptionsMenu()V

    goto :goto_a

    .line 911
    :cond_19
    if-eqz p1, :cond_a

    .line 912
    const v0, 0x7f100178

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->B()Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x0

    :goto_29
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a

    :cond_2d
    const/16 v0, 0x8

    goto :goto_29
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/android/j;)V
    .registers 1
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->z()V

    return-void
.end method

.method private f(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 978
    const v0, 0x7f10016a

    if-ne p1, v0, :cond_12

    .line 979
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V

    .line 983
    :goto_11
    return-void

    .line 981
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(II)V

    goto :goto_11
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/android/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/view/android/j;)V
    .registers 1
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->x()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/view/android/j;)Lcom/google/googlenav/ui/view/p;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    return-object v0
.end method

.method private v()V
    .registers 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 382
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->dismissDropDown()V

    .line 386
    :cond_11
    :goto_11
    return-void

    .line 383
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 384
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->dismissDropDown()V

    goto :goto_11
.end method

.method private static w()Z
    .registers 2

    .prologue
    .line 396
    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    const-string v1, "com.google.android.inputmethod.latin/com.android.inputmethod.latin.LatinIME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private x()V
    .registers 2

    .prologue
    .line 450
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->j()V

    .line 451
    return-void
.end method

.method private y()Z
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 541
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->z()V

    .line 542
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v1

    if-nez v1, :cond_23

    .line 543
    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const/16 v2, 0x3c4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/c;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 545
    const v1, 0x7f10016a

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 564
    :goto_22
    return v0

    .line 548
    :cond_23
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->c()Z

    move-result v1

    if-eqz v1, :cond_50

    .line 549
    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v3}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/maps/MapsActivity;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 553
    :cond_50
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v1

    if-nez v1, :cond_6e

    .line 554
    sget-object v1, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const/16 v2, 0x3c3

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/c;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 556
    const v1, 0x7f1000c4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_22

    .line 559
    :cond_6e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 560
    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/maps/MapsActivity;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 564
    :cond_9b
    const/4 v0, 0x1

    goto :goto_22
.end method

.method private z()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 719
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->w:Z

    if-eqz v0, :cond_8

    .line 743
    :cond_7
    :goto_7
    return-void

    .line 722
    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->b()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 723
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->e()Ljava/lang/String;

    move-result-object v2

    .line 725
    invoke-static {v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 726
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_37

    .line 727
    invoke-static {v2, v5, v5, v4}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v0

    .line 728
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_68

    move-object v0, v1

    :goto_31
    invoke-virtual {v2, v0}, Lax/j;->a(Lax/y;)V

    .line 730
    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/view/android/j;->b(Z)V

    .line 733
    :cond_37
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 734
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v2}, Lcom/google/googlenav/suggest/android/SuggestView;->e()Ljava/lang/String;

    move-result-object v2

    .line 736
    invoke-static {v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 737
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 738
    invoke-static {v2, v5, v5, v4}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v0

    .line 739
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6d

    :goto_5f
    invoke-virtual {v2, v1}, Lax/j;->b(Lax/y;)V

    .line 741
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->h:Landroid/view/View;

    invoke-direct {p0, v4, v0}, Lcom/google/googlenav/ui/view/android/j;->a(ZLandroid/view/View;)V

    goto :goto_7

    .line 728
    :cond_68
    invoke-static {v0}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    goto :goto_31

    .line 739
    :cond_6d
    invoke-static {v0}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    goto :goto_5f
.end method


# virtual methods
.method protected I_()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 241
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 242
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_17

    .line 243
    const/16 v1, 0x37

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 244
    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 248
    :cond_17
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->as()Z

    move-result v1

    if-nez v1, :cond_27

    .line 249
    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 251
    :cond_27
    return-void
.end method

.method protected O_()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 159
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 160
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setUiOptions(I)V

    .line 167
    :goto_12
    return-void

    .line 162
    :cond_13
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 163
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 165
    :cond_26
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->requestWindowFeature(I)Z

    goto :goto_12
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 930
    packed-switch p1, :pswitch_data_40

    .line 943
    :goto_4
    :pswitch_4
    return-void

    .line 932
    :pswitch_5
    const v0, 0x7f10016a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    .line 933
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_4

    .line 936
    :pswitch_1f
    const v0, 0x7f1000c4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    .line 937
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1}, Lcom/google/googlenav/suggest/android/SuggestView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_4

    .line 940
    :pswitch_39
    const v0, 0x7f10016d

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    goto :goto_4

    .line 930
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1f
        :pswitch_4
        :pswitch_39
    .end packed-switch
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 3
    .parameter

    .prologue
    .line 171
    const/16 v0, 0xfb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 172
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 883
    .line 884
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 885
    packed-switch v0, :pswitch_data_24

    move v0, v1

    .line 899
    :cond_9
    :goto_9
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->f:Lcom/google/googlenav/ui/e;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0

    .line 887
    :pswitch_11
    const/16 v0, 0xec

    .line 888
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->y()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 889
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->x()V

    goto :goto_9

    .line 893
    :pswitch_1d
    const/16 v0, 0xd6

    .line 894
    goto :goto_9

    .line 896
    :pswitch_20
    const/16 v0, 0xe4

    goto :goto_9

    .line 885
    nop

    :pswitch_data_24
    .packed-switch 0x7f1004ab
        :pswitch_20
        :pswitch_11
        :pswitch_1d
    .end packed-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 872
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 873
    const v0, 0x7f1004ac

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 874
    if-eqz v0, :cond_1a

    .line 875
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/j;->B()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 878
    :cond_1a
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)V
    .registers 4
    .parameter

    .prologue
    .line 953
    packed-switch p1, :pswitch_data_20

    .line 963
    :goto_3
    return-void

    .line 955
    :pswitch_4
    const v0, 0x7f10016a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    .line 956
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 959
    :pswitch_12
    const v0, 0x7f1000c4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->c(I)V

    .line 960
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 953
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_4
        :pswitch_12
    .end packed-switch
.end method

.method protected c()Landroid/view/View;
    .registers 9

    .prologue
    const v7, 0x7f100173

    const/4 v4, 0x0

    const v6, 0x7f100175

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 255
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->h()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 256
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->k()V

    .line 258
    const v0, 0x7f10016a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    .line 259
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    const/16 v1, 0x585

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setHint(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setEnoughToFilter(Z)V

    .line 261
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/w;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/ui/view/android/w;-><init>(Lcom/google/googlenav/ui/view/android/j;Lcom/google/googlenav/ui/view/android/k;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 262
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelectAllOnFocus(Z)V

    .line 263
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/suggest/android/SuggestView;->setInputIndex(I)V

    .line 264
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/o;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/o;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V

    .line 272
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/p;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/p;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 282
    const v0, 0x7f1000c4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    .line 283
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    const/16 v1, 0x117

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setHint(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setEnoughToFilter(Z)V

    .line 285
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/w;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/ui/view/android/w;-><init>(Lcom/google/googlenav/ui/view/android/j;Lcom/google/googlenav/ui/view/android/k;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 286
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelectAllOnFocus(Z)V

    .line 287
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/suggest/android/SuggestView;->setInputIndex(I)V

    .line 288
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/q;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V

    .line 296
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/r;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/r;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 308
    new-instance v0, Lcom/google/googlenav/ui/view/android/s;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/s;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    .line 322
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 323
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/SuggestView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 325
    const v0, 0x7f100168

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    .line 327
    const v0, 0x7f1000c5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->l:Landroid/widget/RadioGroup;

    .line 328
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->l:Landroid/widget/RadioGroup;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;)V

    .line 330
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 331
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v1

    if-eqz v1, :cond_19e

    move v1, v2

    :goto_d3
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 334
    const v0, 0x7f10016b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 335
    const/16 v1, 0xc3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xd4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 339
    const v0, 0x7f10016e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 340
    const/16 v1, 0xc1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xd5

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/android/j;->b(Landroid/view/View;)V

    .line 347
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_169

    .line 348
    const v0, 0x7f10016f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 349
    const/16 v1, 0x5a1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->g:Lcom/google/googlenav/ui/view/android/ae;

    const/16 v4, 0xe4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/ae;->a(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    const v0, 0x7f100171

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/googlenav/ui/view/android/t;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/android/t;-><init>(Lcom/google/googlenav/ui/view/android/j;)V

    invoke-static {v0, v1, v4, v3}, Lcom/google/googlenav/ui/view/android/ae;->a(ILjava/lang/CharSequence;Lcom/google/googlenav/ui/android/ac;Landroid/view/View;)Landroid/widget/Button;

    .line 360
    const/16 v0, 0x109

    invoke-direct {p0, v3, v7, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    .line 361
    const v0, 0x7f100174

    const/16 v1, 0x5d4

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    .line 362
    const v0, 0x7f100176

    const/16 v1, 0x5dd

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    .line 363
    const/16 v0, 0x231

    invoke-direct {p0, v3, v6, v0}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    .line 364
    const v0, 0x7f10001e

    const/16 v1, 0xfb

    invoke-direct {p0, v3, v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(Landroid/view/View;II)V

    .line 366
    :cond_169
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    const v0, 0x7f100174

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 370
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    const v0, 0x7f100176

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/j;->e(I)Lcom/google/googlenav/ui/android/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    return-object v3

    .line 331
    :cond_19e
    const/16 v1, 0x8

    goto/16 :goto_d3
.end method

.method public c(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 966
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 967
    if-eqz v0, :cond_a

    .line 968
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 970
    :cond_a
    const v0, 0x7f10016a

    if-ne p1, v0, :cond_15

    .line 971
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    .line 975
    :cond_14
    :goto_14
    return-void

    .line 972
    :cond_15
    const v0, 0x7f1000c4

    if-ne p1, v0, :cond_14

    .line 973
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_14
.end method

.method protected d()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 405
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    .line 412
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-static {}, Lcom/google/googlenav/android/a;->d()Z

    move-result v0

    if-nez v0, :cond_29

    const/4 v0, 0x1

    .line 414
    :goto_11
    if-eqz v0, :cond_2b

    invoke-static {}, Lcom/google/googlenav/ui/view/android/j;->w()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 417
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 430
    :goto_1e
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/j;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 433
    return-void

    :cond_29
    move v0, v1

    .line 412
    goto :goto_11

    .line 418
    :cond_2b
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->a()Z

    move-result v0

    if-nez v0, :cond_3e

    .line 419
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->requestFocus()Z

    .line 420
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_1e

    .line 421
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->c()Z

    move-result v0

    if-nez v0, :cond_51

    .line 423
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->requestFocus()Z

    .line 424
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/SuggestView;->setSelection(I)V

    goto :goto_1e

    .line 427
    :cond_51
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_1e
.end method

.method protected f()V
    .registers 2

    .prologue
    .line 437
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->j()V

    .line 439
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    .line 440
    return-void
.end method

.method protected h()I
    .registers 2

    .prologue
    .line 186
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 187
    const v0, 0x7f040061

    .line 191
    :goto_d
    return v0

    :cond_e
    const v0, 0x7f040062

    goto :goto_d
.end method

.method public k()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 196
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_7d

    .line 200
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    .line 201
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    .line 202
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    .line 203
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    .line 204
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    .line 205
    iput-boolean v3, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    .line 208
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-nez v0, :cond_31

    .line 209
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_31

    .line 211
    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    .line 215
    :cond_31
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    .line 216
    if-nez v0, :cond_7e

    move-object v4, v1

    .line 217
    :goto_38
    if-nez v4, :cond_84

    move-object v0, v1

    :goto_3b
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->p:Lo/D;

    .line 218
    if-nez v4, :cond_89

    :goto_3f
    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    .line 219
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-eqz v0, :cond_7d

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    if-eqz v0, :cond_7d

    .line 220
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v1, v4, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    .line 222
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    .line 223
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    .line 225
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    .line 229
    invoke-static {}, LO/T;->a()Z

    move-result v0

    if-eqz v0, :cond_8e

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/j;->q:LaN/B;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_8e

    move v0, v2

    :goto_77
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    .line 232
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    .line 235
    :cond_7d
    return-void

    .line 216
    :cond_7e
    invoke-interface {v0}, LaH/m;->r()LaH/h;

    move-result-object v0

    move-object v4, v0

    goto :goto_38

    .line 217
    :cond_84
    invoke-virtual {v4}, LaH/h;->b()Lo/D;

    move-result-object v0

    goto :goto_3b

    .line 218
    :cond_89
    invoke-virtual {v4}, LaH/h;->a()LaN/B;

    move-result-object v1

    goto :goto_3f

    :cond_8e
    move v0, v3

    .line 229
    goto :goto_77
.end method

.method public l()V
    .registers 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->h:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/j;->b(Landroid/view/View;)V

    .line 528
    return-void
.end method

.method protected m()V
    .registers 4

    .prologue
    .line 569
    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/BaseMapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 573
    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 574
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/j;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 576
    :cond_1e
    return-void
.end method

.method public n()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 670
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_4f

    .line 671
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    if-nez v0, :cond_41

    const/4 v0, 0x0

    move-object v3, v0

    .line 672
    :goto_16
    if-eqz v3, :cond_4f

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    if-eqz v0, :cond_4f

    .line 673
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/view/android/j;->a(LaN/B;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->s:Z

    .line 674
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->n:Z

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_4d

    move v0, v1

    :goto_2f
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->t:Z

    .line 677
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/j;->o:Z

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->v:Lcom/google/googlenav/j;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v0

    if-eqz v0, :cond_3e

    move v2, v1

    :cond_3e
    iput-boolean v2, p0, Lcom/google/googlenav/ui/view/android/j;->u:Z

    .line 684
    :goto_40
    return v1

    .line 671
    :cond_41
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/p;->d()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    move-object v3, v0

    goto :goto_16

    :cond_4d
    move v0, v2

    .line 674
    goto :goto_2f

    :cond_4f
    move v1, v2

    .line 684
    goto :goto_40
.end method

.method public o()Lcom/google/googlenav/ui/view/p;
    .registers 2

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->a:Lcom/google/googlenav/ui/view/p;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 855
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_52

    .line 856
    sget-object v0, Lcom/google/googlenav/ui/view/android/j;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 857
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 858
    const v0, 0x7f1004ac

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 859
    const/16 v1, 0x2f7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 860
    const v0, 0x7f1004ad

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 861
    const/16 v1, 0x1ba

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 863
    :cond_40
    const v0, 0x7f1004ab

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 864
    const/16 v1, 0x5a1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 865
    const/4 v0, 0x1

    .line 867
    :goto_51
    return v0

    :cond_52
    const/4 v0, 0x0

    goto :goto_51
.end method

.method public onStop()V
    .registers 2

    .prologue
    .line 532
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->onStop()V

    .line 533
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->b:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->f()V

    .line 534
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/j;->c:Lcom/google/googlenav/suggest/android/SuggestView;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/SuggestView;->f()V

    .line 535
    return-void
.end method
