.class public Lcom/google/googlenav/ui/view/dialog/X;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/android/J;

.field private final b:Lcom/google/googlenav/ui/view/dialog/ah;

.field private final c:Lcom/google/googlenav/friend/history/o;

.field private d:Landroid/view/View;

.field private l:Lbe/o;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/view/dialog/ah;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    .line 125
    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    .line 126
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->w_()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020218

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/CharSequence;II)V

    .line 127
    return-void
.end method

.method private a(Ljava/lang/String;)Lbe/g;
    .registers 4
    .parameter

    .prologue
    .line 312
    new-instance v0, Lbe/g;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ad;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ad;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v0, p1, v1}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/X;)Lcom/google/googlenav/friend/history/o;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/X;)Lcom/google/googlenav/ui/view/dialog/ah;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/X;)Lbe/o;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    return-object v0
.end method

.method private l()Ljava/util/List;
    .registers 6

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_d

    .line 260
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 308
    :goto_c
    return-object v0

    .line 263
    :cond_d
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 264
    new-instance v1, Lbj/bv;

    const/4 v2, 0x0

    const v3, 0x7f0400cc

    const/16 v4, 0xdb

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/o;->n()Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 270
    new-instance v1, Lbe/n;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/n;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    const/16 v1, 0x14d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/String;)Lbe/g;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 273
    :cond_4b
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/o;->o()Z

    move-result v1

    if-eqz v1, :cond_6f

    .line 274
    new-instance v1, Lbe/H;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    const/16 v1, 0x14e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/X;->a(Ljava/lang/String;)Lbe/g;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 278
    :cond_6f
    new-instance v1, Lbe/j;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    new-instance v1, Lbe/g;

    const/16 v2, 0x283

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/aa;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/aa;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    new-instance v1, Lbe/g;

    const/16 v2, 0x28b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/ab;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/ab;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    new-instance v1, Lbe/g;

    const/16 v2, 0x287

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/ac;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/ac;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3}, Lbe/g;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c
.end method

.method private m()Ljava/util/List;
    .registers 6

    .prologue
    .line 324
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 325
    new-instance v1, Lbe/u;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbe/u;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    new-instance v1, Lbj/bv;

    const/4 v2, 0x0

    const v3, 0x7f0400cc

    const/16 v4, 0x274

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    new-instance v1, Lbe/d;

    const/16 v2, 0x285

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020202

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/ae;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/dialog/ae;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3, v4}, Lbe/d;-><init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    return-object v0
.end method

.method private n()Ljava/util/List;
    .registers 12

    .prologue
    const v6, 0x7f0400cc

    const/4 v10, 0x7

    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 348
    if-nez v2, :cond_12

    .line 349
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->m()Ljava/util/List;

    move-result-object v0

    .line 388
    :goto_11
    return-object v0

    .line 352
    :cond_12
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 353
    new-instance v3, Lbj/bv;

    const/16 v4, 0x27b

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v6, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    new-instance v3, Lbe/G;

    invoke-direct {v3, v2}, Lbe/G;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    new-instance v3, Lbe/a;

    const/16 v4, 0x27a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/ui/view/dialog/af;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/view/dialog/af;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v3, v4, v5}, Lbe/a;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    new-instance v3, Lbj/bv;

    const/16 v4, 0x287

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v6, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/o;->s()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    array-length v4, v3

    :goto_5c
    if-ge v1, v4, :cond_77

    aget-object v5, v3, v1

    .line 373
    invoke-virtual {v5, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_74

    .line 375
    new-instance v6, Lbe/A;

    invoke-direct {v6, v5}, Lbe/A;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    :cond_74
    add-int/lit8 v1, v1, 0x1

    goto :goto_5c

    .line 379
    :cond_77
    new-instance v1, Lbe/d;

    const/16 v2, 0x284

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020202

    new-instance v4, Lcom/google/googlenav/ui/view/dialog/ag;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/view/dialog/ag;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-direct {v1, v2, v3, v4}, Lbe/d;-><init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11
.end method


# virtual methods
.method protected K_()Z
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    if-eqz v0, :cond_b

    .line 148
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/ah;->a()V

    .line 149
    const/4 v0, 0x1

    .line 151
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 143
    return-void
.end method

.method public b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/dialog/X;->c(Z)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    .line 233
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    if-eqz v0, :cond_15

    .line 234
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/J;->insert(Ljava/lang/Object;I)V

    .line 236
    :cond_15
    return-void
.end method

.method protected c()Landroid/view/View;
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040025

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 164
    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->d:Landroid/view/View;

    .line 166
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 167
    new-instance v0, Lcom/google/googlenav/friend/history/W;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/googlenav/friend/history/W;-><init>(Landroid/content/Context;)V

    .line 168
    new-instance v3, Lbe/o;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v4}, Lcom/google/googlenav/friend/history/o;->t()Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v5}, Lcom/google/googlenav/friend/history/o;->k()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/W;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Lbe/o;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    .line 173
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->l:Lbe/o;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 176
    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/X;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x7

    invoke-direct {v0, v3, v7, v2, v4}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    .line 179
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 180
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->a:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 182
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 183
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 185
    new-instance v2, Lcom/google/googlenav/ui/view/dialog/Y;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/Y;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 215
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/o;->u()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/Z;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/Z;-><init>(Lcom/google/googlenav/ui/view/dialog/X;)V

    invoke-interface {v0, v2, v3}, Lcom/google/googlenav/ui/view/dialog/ah;->a(Ljava/lang/String;Lcom/google/googlenav/ui/wizard/bj;)V

    .line 223
    return-object v1
.end method

.method c(Z)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 247
    if-eqz p1, :cond_7

    .line 248
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->l()Ljava/util/List;

    move-result-object v0

    .line 250
    :goto_6
    return-object v0

    :cond_7
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/X;->n()Ljava/util/List;

    move-result-object v0

    goto :goto_6
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method h()Ljava/util/List;
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->c:Lcom/google/googlenav/friend/history/o;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/o;->p()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/X;->c(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    if-eqz v0, :cond_9

    .line 157
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/X;->b:Lcom/google/googlenav/ui/view/dialog/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/ah;->a()V

    .line 159
    :cond_9
    return-void
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 131
    const/16 v0, 0xdb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
