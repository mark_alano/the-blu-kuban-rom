.class Lcom/google/googlenav/ui/view/dialog/aA;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/ax;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/view/dialog/ax;)V
    .registers 2
    .parameter

    .prologue
    .line 623
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aA;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 627
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2a

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->n()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/googlenav/friend/history/b;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-gez v0, :cond_2a

    .line 629
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aA;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x272

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 634
    :goto_29
    return-void

    .line 632
    :cond_2a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aA;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    new-instance v1, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/googlenav/friend/history/b;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/ax;->a(Lcom/google/googlenav/friend/history/b;)V

    goto :goto_29
.end method
