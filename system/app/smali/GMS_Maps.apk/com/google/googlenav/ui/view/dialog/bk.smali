.class public Lcom/google/googlenav/ui/view/dialog/bk;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ui/view/dialog/bl;

.field private final c:Lcom/google/googlenav/ui/view/dialog/bn;

.field private final d:Lcom/google/googlenav/ui/ak;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/view/dialog/bn;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    .line 70
    iput p1, p0, Lcom/google/googlenav/ui/view/dialog/bk;->a:I

    .line 71
    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/bk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 72
    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    .line 73
    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/bk;->d:Lcom/google/googlenav/ui/ak;

    .line 74
    return-void
.end method

.method static a(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 274
    const-string v0, "\n"

    .line 275
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.apps.offers.VIEW_OFFER_DETAILS"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    .line 277
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 278
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 280
    :cond_1a
    const-string v2, "offer_title"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const/4 v0, 0x1

    const-string v2, "offer_id"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 282
    const/16 v0, 0xc

    const-string v2, "offer_namespace"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 283
    const/16 v0, 0xf

    const-string v2, "offer_type"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 284
    const/16 v0, 0xa

    const-string v2, "offer_image_url"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 286
    const/16 v0, 0xb

    const-string v2, "offer_description"

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 288
    const-string v0, "offer_cluster_doc_id"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v0, "offer_merchant_name"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v0, "offer_website"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string v0, "offer_address"

    const-string v2, "\n"

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ai;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string v0, "offer_phone_number"

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_88

    .line 297
    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    invoke-virtual {v2, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    .line 298
    const-string v2, "selected_redeem_location"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 300
    :cond_88
    return-object v1
.end method

.method public static a(Ljava/lang/CharSequence;ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v4, 0x7f020351

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 82
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bk;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    .line 83
    if-nez v0, :cond_22

    move-object v0, v1

    .line 84
    :goto_e
    if-nez v0, :cond_27

    move-object v0, v1

    .line 86
    :goto_11
    new-instance v2, Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-direct {v2, p1, p2, v0, p3}, Lcom/google/googlenav/ui/view/dialog/bk;-><init>(ILcom/google/googlenav/ui/view/dialog/bl;Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/view/dialog/bn;)V

    .line 87
    new-array v0, v3, [I

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(ZILaA/f;[I)V

    .line 88
    const v0, 0x7f100025

    invoke-virtual {v2, p0, v0, v4}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/CharSequence;II)V

    .line 89
    return-object v2

    .line 83
    :cond_22
    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    goto :goto_e

    .line 84
    :cond_27
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    goto :goto_11
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/view/dialog/bn;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 175
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_15

    .line 176
    const v0, 0x7f100025

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 178
    :cond_15
    return-void
.end method

.method protected static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILandroid/content/Intent;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 305
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 306
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    :cond_d
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bk;Ljava/lang/CharSequence;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 260
    if-eqz p2, :cond_9

    .line 261
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/content/Intent;)V

    .line 263
    :cond_9
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->o()V

    return-void
.end method

.method private b(Landroid/content/Intent;)Z
    .registers 5
    .parameter

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 225
    const/16 v1, 0x40

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 227
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 228
    const-class v2, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 229
    const/4 v0, 0x1

    .line 232
    :goto_2f
    return v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->d:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method private o()V
    .registers 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->r()Z

    move-result v0

    if-nez v0, :cond_11

    .line 117
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    .line 119
    :cond_11
    return-void
.end method

.method private v()V
    .registers 3

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    .line 165
    invoke-static {}, Lcom/google/googlenav/offers/k;->g()Lcom/google/android/apps/common/offerslib/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;)V

    .line 166
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bm;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bm;-><init>(Lcom/google/googlenav/ui/view/dialog/bk;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/d;)V

    .line 167
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/offers/k;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 168
    if-eqz v1, :cond_20

    .line 169
    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/accounts/Account;)V

    .line 171
    :cond_20
    return-void
.end method


# virtual methods
.method protected K_()Z
    .registers 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->b(Lcom/google/googlenav/ui/view/dialog/bk;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 248
    if-eqz p1, :cond_b

    if-ltz p2, :cond_b

    .line 249
    invoke-virtual {p1, p2}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 251
    :cond_b
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->dismiss()V

    .line 211
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->c:Lcom/google/googlenav/ui/view/dialog/bn;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bn;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 217
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    .line 221
    :cond_20
    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 195
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 196
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/dialog/bk;->b(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 197
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    .line 199
    new-instance v3, Lcom/google/googlenav/android/M;

    new-instance v4, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;

    invoke-direct {v4, p0, v2, v1}, Lcom/google/googlenav/offers/OfferDetailsIntentProcessorHandler;-><init>(Lcom/google/googlenav/ui/view/dialog/bk;Lcom/google/googlenav/android/R;Landroid/content/Intent;)V

    invoke-virtual {v2}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v2

    invoke-direct {v3, v4, v1, v2}, Lcom/google/googlenav/android/M;-><init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V

    .line 203
    invoke-virtual {v3}, Lcom/google/googlenav/android/M;->a()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_38

    const/4 v0, 0x1

    .line 205
    :cond_38
    return v0
.end method

.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 146
    .line 147
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 150
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->h:Landroid/view/View;

    .line 152
    :goto_d
    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v2

    if-nez v2, :cond_27

    .line 156
    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->h()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 157
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->v()V

    .line 158
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/view/View;)V

    .line 160
    :cond_27
    return-object v0

    :cond_28
    move-object v0, v1

    goto :goto_d
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 313
    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .registers 3

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->m()Landroid/support/v4/app/k;

    move-result-object v0

    .line 124
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_14

    .line 126
    invoke-virtual {v0}, Landroid/support/v4/app/k;->a()Landroid/support/v4/app/t;

    move-result-object v0

    .line 127
    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Landroid/support/v4/app/f;)Landroid/support/v4/app/t;

    .line 128
    invoke-virtual {v0}, Landroid/support/v4/app/t;->b()I

    .line 131
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    .line 132
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    .line 133
    return-void
.end method

.method protected h()I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Lcom/google/googlenav/ui/view/dialog/bk;->a:I

    return v0
.end method

.method public l()Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;
    .registers 3

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->m()Landroid/support/v4/app/k;

    move-result-object v0

    .line 183
    const v1, 0x7f1002f7

    invoke-virtual {v0, v1}, Landroid/support/v4/app/k;->a(I)Landroid/support/v4/app/f;

    move-result-object v0

    .line 184
    check-cast v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    return-object v0
.end method

.method public m()Landroid/support/v4/app/k;
    .registers 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 191
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/k;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .registers 3

    .prologue
    .line 236
    const/16 v0, 0x58

    const-string v1, "m"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 238
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.offers.VIEW_MY_OFFERS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/content/Intent;)V

    .line 239
    return-void
.end method

.method public onBackPressed()V
    .registers 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/bk;->o()V

    .line 106
    return-void
.end method
