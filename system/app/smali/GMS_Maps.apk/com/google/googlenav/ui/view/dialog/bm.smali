.class public Lcom/google/googlenav/ui/view/dialog/bm;
.super Lcom/google/android/apps/common/offerslib/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/bk;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 2
    .parameter

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .registers 5
    .parameter

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.offers.VIEW_MY_OFFERS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/content/Intent;)V

    .line 339
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 354
    const/4 v0, 0x1

    invoke-static {p3, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 355
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/view/dialog/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bn;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    .line 356
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 327
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/net/Uri;)V

    .line 329
    :cond_b
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Lcom/google/android/apps/common/offerslib/t;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->i()LaH/h;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/common/offerslib/t;->a(Landroid/location/Location;)V

    .line 373
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ui/view/dialog/bk;Ljava/lang/CharSequence;)V

    .line 344
    return-void
.end method

.method public b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .registers 4
    .parameter

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/view/dialog/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bn;->a(Lcom/google/googlenav/ui/view/dialog/bk;)V

    .line 334
    return-void
.end method

.method public c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->b(Lcom/google/googlenav/ui/view/dialog/bk;)V

    .line 349
    return-void
.end method

.method public e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .registers 3
    .parameter

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    return v0
.end method

.method public h(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->o()Z

    .line 367
    return-void
.end method

.method public i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/location/Location;
    .registers 3
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bm;->a:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->c(Lcom/google/googlenav/ui/view/dialog/bk;)Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->i()LaH/h;

    move-result-object v0

    return-object v0
.end method
