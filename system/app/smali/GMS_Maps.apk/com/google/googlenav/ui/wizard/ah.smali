.class public Lcom/google/googlenav/ui/wizard/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/H;

.field private final b:Lbf/a;

.field private final c:Lcom/google/googlenav/android/aa;

.field private d:Lcom/google/googlenav/ui/wizard/al;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/j;Lbf/a;Lcom/google/googlenav/android/aa;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    new-instance v0, Lcom/google/googlenav/ui/wizard/H;

    invoke-direct {v0, p1, p3}, Lcom/google/googlenav/ui/wizard/H;-><init>(Lcom/google/googlenav/friend/j;Lcom/google/googlenav/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->a:Lcom/google/googlenav/ui/wizard/H;

    .line 121
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ah;->b:Lbf/a;

    .line 122
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ah;->c:Lcom/google/googlenav/android/aa;

    .line 123
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->d:Lcom/google/googlenav/ui/wizard/al;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->c:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/ah;)Lbf/a;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->b:Lbf/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->a:Lcom/google/googlenav/ui/wizard/H;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/H;->a()V

    .line 214
    return-void
.end method

.method public a(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 190
    new-instance v0, LaN/B;

    invoke-direct {v0, p1, p2}, LaN/B;-><init>(II)V

    .line 191
    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v3, v3, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;III)V

    .line 193
    new-instance v0, Lcom/google/googlenav/ui/wizard/ai;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ai;-><init>(Lcom/google/googlenav/ui/wizard/ah;)V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    .line 204
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 205
    return-void
.end method

.method public a(LaH/h;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 140
    const-string v0, "cz"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->c(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/google/googlenav/friend/bg;

    invoke-direct {v1}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/friend/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/am;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/wizard/am;-><init>(Lcom/google/googlenav/ui/wizard/ah;Lcom/google/googlenav/ui/wizard/ai;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->g(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/friend/bg;->a(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/friend/bg;->b(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    .line 154
    invoke-virtual {p1}, LaH/h;->c()Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 155
    invoke-virtual {p1}, LaH/h;->b()Lo/D;

    move-result-object v2

    invoke-virtual {v2}, Lo/D;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->c(I)Lcom/google/googlenav/friend/bg;

    .line 157
    :cond_5e
    invoke-virtual {p1}, LaH/h;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_6c

    .line 158
    invoke-virtual {p1}, LaH/h;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->f(I)Lcom/google/googlenav/friend/bg;

    .line 160
    :cond_6c
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 161
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v1

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 162
    return-void
.end method

.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->a:Lcom/google/googlenav/ui/wizard/H;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/H;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    .line 237
    return-void
.end method

.method public a(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 178
    new-instance v0, Lcom/google/googlenav/o;

    new-instance v5, Lcom/google/googlenav/ui/wizard/ak;

    const/4 v1, 0x0

    invoke-direct {v5, p0, v1}, Lcom/google/googlenav/ui/wizard/ak;-><init>(Lcom/google/googlenav/ui/wizard/ah;Lcom/google/googlenav/ui/wizard/ai;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/o;-><init>(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/google/googlenav/r;)V

    .line 181
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 182
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/al;Lcom/google/googlenav/ui/wizard/L;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ah;->d:Lcom/google/googlenav/ui/wizard/al;

    .line 131
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ah;->a:Lcom/google/googlenav/ui/wizard/H;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/wizard/H;->a(Lcom/google/googlenav/ui/wizard/L;)V

    .line 132
    return-void
.end method

.method public a(Ljava/util/List;LaG/h;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 245
    new-instance v0, LaG/a;

    invoke-direct {v0, p2}, LaG/a;-><init>(LaG/h;)V

    sget-object v1, LaG/g;->b:LaG/g;

    invoke-virtual {v0, v1}, LaG/a;->a(LaG/g;)LaG/a;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LaG/a;->a(I)LaG/a;

    move-result-object v0

    invoke-virtual {v0, p1}, LaG/a;->a(Ljava/util/List;)LaG/a;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/aj;

    invoke-direct {v1, p0, p2}, Lcom/google/googlenav/ui/wizard/aj;-><init>(Lcom/google/googlenav/ui/wizard/ah;LaG/h;)V

    invoke-virtual {v0, v1}, LaG/a;->a(LaG/c;)LaG/a;

    move-result-object v0

    .line 264
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 265
    return-void
.end method
