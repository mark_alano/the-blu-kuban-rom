.class Lcom/google/googlenav/ui/wizard/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/f;
.implements Lbf/g;
.implements Lcom/google/googlenav/r;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/ah;

.field private b:Lcom/google/googlenav/a;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/ah;)V
    .registers 2
    .parameter

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/ah;Lcom/google/googlenav/ui/wizard/ai;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 306
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ak;-><init>(Lcom/google/googlenav/ui/wizard/ah;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    .line 388
    return-void
.end method

.method public a(Lcom/google/googlenav/F;)V
    .registers 5
    .parameter

    .prologue
    .line 367
    if-eqz p1, :cond_22

    .line 368
    new-instance v0, Lcom/google/googlenav/friend/aK;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/aK;-><init>(Lcom/google/googlenav/F;)V

    .line 369
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    .line 370
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/a;I)V

    .line 371
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->c(Lcom/google/googlenav/ui/wizard/ah;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ah;->b(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2, p0}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    .line 376
    :goto_21
    return-void

    .line 374
    :cond_22
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    goto :goto_21
.end method

.method public a(ZLcom/google/googlenav/a;Lcom/google/googlenav/h;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    if-eqz p1, :cond_37

    .line 331
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    .line 332
    invoke-virtual {p3}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-nez v0, :cond_13

    .line 334
    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ak;->b:Lcom/google/googlenav/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aM;->a(Lcom/google/googlenav/a;)V

    .line 337
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->c(Lcom/google/googlenav/ui/wizard/ah;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ah;->b(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    .line 340
    invoke-static {p3}, Lcom/google/googlenav/RatingReminderManager;->a(Lcom/google/googlenav/h;)V

    .line 347
    :goto_26
    invoke-virtual {p3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 348
    invoke-virtual {p3}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 349
    invoke-static {}, Lcom/google/googlenav/friend/ad;->s()V

    .line 351
    :cond_36
    return-void

    .line 345
    :cond_37
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ak;->a:Lcom/google/googlenav/ui/wizard/ah;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/ah;)Lcom/google/googlenav/ui/wizard/al;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/al;->a(ZLcom/google/googlenav/a;)V

    goto :goto_26
.end method
