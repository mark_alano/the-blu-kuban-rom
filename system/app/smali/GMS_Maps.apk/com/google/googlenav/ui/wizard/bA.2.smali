.class public Lcom/google/googlenav/ui/wizard/bA;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/bx;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/bx;)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    .line 106
    invoke-static {}, Lcom/google/googlenav/ui/wizard/bA;->p()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 107
    return-void
.end method

.method private h()Lcom/google/googlenav/ui/view/android/J;
    .registers 8

    .prologue
    .line 143
    new-instance v1, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bA;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    .line 145
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 146
    const/4 v0, 0x0

    :goto_10
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v3}, Lcom/google/googlenav/bZ;->k()I

    move-result v3

    if-ge v0, v3, :cond_7e

    .line 147
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v3

    .line 149
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->n()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7b

    .line 152
    new-instance v4, Lcom/google/googlenav/ui/wizard/bG;

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->n()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/googlenav/ui/wizard/bG;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 155
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_57

    .line 156
    new-instance v4, Lcom/google/googlenav/ui/wizard/by;

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/googlenav/ui/wizard/bC;

    invoke-direct {v6, p0, v3}, Lcom/google/googlenav/ui/wizard/bC;-><init>(Lcom/google/googlenav/ui/wizard/bA;Lcom/google/googlenav/cm;)V

    invoke-direct {v4, v5, v6}, Lcom/google/googlenav/ui/wizard/by;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 167
    :cond_57
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_74

    .line 168
    new-instance v4, Lcom/google/googlenav/ui/wizard/by;

    const/16 v5, 0x5c7

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/googlenav/ui/wizard/bD;

    invoke-direct {v6, p0, v3}, Lcom/google/googlenav/ui/wizard/bD;-><init>(Lcom/google/googlenav/ui/wizard/bA;Lcom/google/googlenav/cm;)V

    invoke-direct {v4, v5, v6}, Lcom/google/googlenav/ui/wizard/by;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 176
    :cond_74
    invoke-virtual {v3}, Lcom/google/googlenav/cm;->n()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_7b
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 180
    :cond_7e
    return-object v1
.end method

.method private l()Lcom/google/googlenav/ui/view/android/J;
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 184
    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bA;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {v3, v0, v1}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    .line 186
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    .line 187
    :goto_11
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-ge v1, v0, :cond_dd

    .line 188
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    invoke-virtual {v0, v1}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->F()I

    move-result v5

    .line 189
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    invoke-virtual {v0, v5}, Lax/b;->j(I)Lax/d;

    move-result-object v6

    .line 190
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c4

    if-eqz v6, :cond_c4

    .line 193
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bx;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v7, v7, Lcom/google/googlenav/ui/wizard/bx;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v7}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v7

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v8, v8, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    invoke-virtual {v8, v1}, Lax/b;->n(I)Lax/t;

    move-result-object v8

    invoke-virtual {v8}, Lax/t;->y()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/m;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 195
    new-instance v7, Lcom/google/googlenav/ui/wizard/bG;

    invoke-virtual {v6}, Lax/d;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v0}, Lcom/google/googlenav/ui/wizard/bG;-><init>(Ljava/lang/String;Lan/f;)V

    invoke-virtual {v3, v7}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 198
    invoke-virtual {v6}, Lax/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a5

    .line 199
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bx;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 200
    new-instance v0, Lcom/google/googlenav/ui/wizard/by;

    const/16 v7, 0x67

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6}, Lax/d;->c()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/ui/wizard/bE;

    invoke-direct {v8, p0, v6}, Lcom/google/googlenav/ui/wizard/bE;-><init>(Lcom/google/googlenav/ui/wizard/bA;Lax/d;)V

    invoke-direct {v0, v7, v8}, Lcom/google/googlenav/ui/wizard/by;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 217
    :cond_a5
    :goto_a5
    invoke-virtual {v6}, Lax/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 218
    new-instance v0, Lcom/google/googlenav/ui/wizard/by;

    invoke-virtual {v6}, Lax/d;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lau/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/ui/wizard/bF;

    invoke-direct {v8, p0, v6}, Lcom/google/googlenav/ui/wizard/bF;-><init>(Lcom/google/googlenav/ui/wizard/bA;Lax/d;)V

    invoke-direct {v0, v7, v8}, Lcom/google/googlenav/ui/wizard/by;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 229
    :cond_c4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_11

    .line 212
    :cond_d0
    new-instance v0, Lcom/google/googlenav/ui/wizard/by;

    invoke-virtual {v6}, Lax/d;->c()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/google/googlenav/ui/wizard/by;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    goto :goto_a5

    .line 232
    :cond_dd
    return-object v3
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bA;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04005e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 112
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 115
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bA;->a:Lcom/google/googlenav/ui/wizard/bx;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    if-eqz v2, :cond_33

    .line 116
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bA;->l()Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 120
    :goto_22
    new-instance v2, Lcom/google/googlenav/ui/wizard/bB;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/bB;-><init>(Lcom/google/googlenav/ui/wizard/bA;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 137
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 138
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 139
    return-object v1

    .line 118
    :cond_33
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bA;->h()Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_22
.end method
