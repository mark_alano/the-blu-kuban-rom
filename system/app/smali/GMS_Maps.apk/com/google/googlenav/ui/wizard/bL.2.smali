.class Lcom/google/googlenav/ui/wizard/bL;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Ljava/lang/CharSequence;

.field final b:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    .line 216
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    .line 210
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    .line 211
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 3
    .parameter

    .prologue
    .line 235
    new-instance v0, Lcom/google/googlenav/ui/wizard/bM;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bM;-><init>(Lcom/google/googlenav/ui/wizard/bL;)V

    .line 236
    check-cast p1, Landroid/widget/TextView;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    .line 237
    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 242
    check-cast p2, Lcom/google/googlenav/ui/wizard/bM;

    .line 243
    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bL;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_14

    .line 245
    iget-object v0, p2, Lcom/google/googlenav/ui/wizard/bM;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bL;->b:Landroid/view/View$OnClickListener;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 247
    :cond_14
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 220
    const v0, 0x7f040170

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method
