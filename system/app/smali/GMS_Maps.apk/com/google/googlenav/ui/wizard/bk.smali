.class public Lcom/google/googlenav/ui/wizard/bk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/wizard/bq;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 91
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 148
    :goto_a
    return-void

    .line 145
    :cond_b
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    const v0, 0x7f1000b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    .line 146
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bk;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/bk;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const v1, 0x7f100031

    const v2, 0x7f100030

    const/4 v4, 0x0

    .line 152
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_59

    move v0, v1

    .line 153
    :goto_e
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_5b

    .line 155
    :goto_14
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 156
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 157
    new-instance v1, Lcom/google/googlenav/ui/wizard/bl;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bl;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4e

    .line 172
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 173
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 174
    new-instance v1, Lcom/google/googlenav/ui/wizard/bm;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bm;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 191
    :cond_4e
    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 192
    return-void

    :cond_59
    move v0, v2

    .line 152
    goto :goto_e

    :cond_5b
    move v2, v1

    .line 153
    goto :goto_14
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/bk;->b(Landroid/view/View;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 242
    new-instance v0, Lcom/google/googlenav/ui/wizard/bq;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/bq;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    .line 243
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/bq;->a:Ljava/lang/String;

    .line 244
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p2, v0, Lcom/google/googlenav/ui/wizard/bq;->b:Ljava/lang/CharSequence;

    .line 245
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-eqz p3, :cond_2c

    :goto_13
    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/bq;->c:Ljava/lang/String;

    .line 246
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-eqz p4, :cond_33

    :goto_19
    iput-object p4, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    .line 247
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-boolean p5, v0, Lcom/google/googlenav/ui/wizard/bq;->e:Z

    .line 248
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p6, v0, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    .line 249
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-boolean p7, v0, Lcom/google/googlenav/ui/wizard/bq;->g:Z

    .line 250
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p8, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    .line 251
    return-void

    .line 245
    :cond_2c
    const/16 v1, 0x35b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_13

    .line 246
    :cond_33
    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p4

    goto :goto_19
.end method

.method private g()V
    .registers 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-nez v0, :cond_8

    .line 316
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    .line 324
    :cond_7
    :goto_7
    return-void

    .line 317
    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/wizard/bq;->e:Z

    if-eqz v0, :cond_7

    .line 318
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    if-eqz v0, :cond_22

    .line 319
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    if-nez v0, :cond_26

    const/4 v0, 0x0

    .line 320
    :goto_1b
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/bo;->c(Z)V

    .line 322
    :cond_22
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    goto :goto_7

    .line 319
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_1b
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 331
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_b

    .line 332
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->h()V

    .line 334
    :cond_b
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    return v0
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 342
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 234
    invoke-direct/range {p0 .. p8}, Lcom/google/googlenav/ui/wizard/bk;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    .line 237
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 259
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-object v6, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    .line 263
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object v4, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    .line 264
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    .line 265
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 272
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    .line 273
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_8

    .line 290
    :goto_7
    return-void

    .line 280
    :cond_8
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->e()Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 283
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/bn;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bn;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 289
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_7
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    .line 295
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 296
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    .line 301
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    .line 302
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    .line 303
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    .line 304
    return-void
.end method

.method protected e()Lcom/google/googlenav/ui/view/android/S;
    .registers 2

    .prologue
    .line 94
    new-instance v0, Lcom/google/googlenav/ui/wizard/bp;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/bp;-><init>(Lcom/google/googlenav/ui/wizard/bk;Lcom/google/googlenav/ui/e;)V

    return-object v0
.end method

.method public f()V
    .registers 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    .line 202
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    if-nez v0, :cond_14

    const/4 v0, 0x0

    .line 203
    :goto_b
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    .line 204
    if-eqz v1, :cond_13

    .line 205
    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/bo;->a(Z)V

    .line 207
    :cond_13
    return-void

    .line 202
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_b
.end method

.method public h()V
    .registers 1

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bk;->g()V

    .line 312
    return-void
.end method
