.class public Lcom/google/googlenav/ui/wizard/bp;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/bk;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/bk;Lcom/google/googlenav/ui/e;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    .line 99
    const v0, 0x7f0f001b

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 100
    return-void
.end method


# virtual methods
.method public I_()V
    .registers 3

    .prologue
    .line 135
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 136
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bp;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 138
    :cond_10
    return-void
.end method

.method public O_()V
    .registers 2

    .prologue
    .line 130
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bp;->requestWindowFeature(I)Z

    .line 131
    return-void
.end method

.method protected c()Landroid/view/View;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bp;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 106
    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 107
    if-eqz v0, :cond_30

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bq;->a:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 108
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bq;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    :cond_30
    const v0, 0x7f1000bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bq;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-boolean v2, v2, Lcom/google/googlenav/ui/wizard/bq;->g:Z

    if-eqz v2, :cond_5e

    .line 115
    invoke-static {v0, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 120
    :goto_4d
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 123
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/bk;->a(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bp;->a:Lcom/google/googlenav/ui/wizard/bk;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/bk;->b(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V

    .line 125
    return-object v1

    .line 117
    :cond_5e
    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 118
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_4d
.end method
