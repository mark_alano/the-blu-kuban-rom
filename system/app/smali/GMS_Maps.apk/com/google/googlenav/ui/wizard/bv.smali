.class public Lcom/google/googlenav/ui/wizard/bv;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static j:Lcom/google/googlenav/common/Config;

.field private static k:Ljava/lang/String;


# instance fields
.field private a:Lax/b;

.field private b:Lax/b;

.field private final c:Lcom/google/googlenav/J;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 154
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    .line 172
    const-string v0, ""

    sput-object v0, Lcom/google/googlenav/ui/wizard/bv;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 176
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    .line 177
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 178
    return-void
.end method

.method private A()I
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 572
    iget v1, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    packed-switch v1, :pswitch_data_a

    .line 578
    :goto_6
    :pswitch_6
    return v0

    .line 574
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_6

    .line 572
    nop

    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method private a(I)I
    .registers 2
    .parameter

    .prologue
    .line 186
    iput p1, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    .line 187
    return p1
.end method

.method public static a(Lcom/google/googlenav/ui/m;)Lax/b;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 312
    sget-object v1, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 313
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->L()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 317
    const-string v2, "SAVED_DIRECTIONS"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 321
    const-string v2, "SAVED_DIRECTIONS_DB"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 323
    :cond_1b
    const-string v2, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-static {v1, v2}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v1

    .line 324
    if-nez v1, :cond_24

    .line 337
    :goto_23
    return-object v0

    .line 329
    :cond_24
    :try_start_24
    invoke-static {v1, p0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v1

    .line 330
    instance-of v2, v1, Lax/s;

    if-eqz v2, :cond_2f

    .line 331
    invoke-virtual {v1}, Lax/b;->G()V
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_2f} :catch_31

    :cond_2f
    move-object v0, v1

    .line 333
    goto :goto_23

    .line 334
    :catch_31
    move-exception v1

    .line 335
    const-string v2, "UI-TDW"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_23
.end method

.method private static a(Lax/l;Z)Ljava/lang/String;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x5

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    .line 698
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 706
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 707
    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 708
    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 710
    invoke-virtual {p0}, Lax/l;->c()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 711
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 712
    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 713
    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 715
    if-ne v5, v8, :cond_2d

    if-ne v4, v7, :cond_2d

    if-eq v0, v6, :cond_35

    :cond_2d
    move v0, v2

    .line 717
    :goto_2e
    if-eqz p1, :cond_37

    .line 718
    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 723
    :goto_34
    return-object v0

    :cond_35
    move v0, v1

    .line 715
    goto :goto_2e

    .line 719
    :cond_37
    if-eqz v0, :cond_52

    .line 720
    const/16 v0, 0x5b9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v9, [Ljava/lang/String;

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_34

    .line 723
    :cond_52
    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    goto :goto_34
.end method

.method private a(IZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->o()Z

    move-result v0

    if-nez v0, :cond_7

    .line 370
    :goto_6
    return-void

    .line 360
    :cond_7
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 361
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->s(I)V

    .line 363
    if-eqz p2, :cond_22

    .line 364
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 368
    :cond_22
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->e(Lax/b;)Lbf/O;

    .line 369
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    goto :goto_6
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bv;)V
    .registers 1
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->y()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 508
    const-string v0, ""

    const-string v1, ""

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v2}, Lax/b;->k()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 509
    return-void
.end method

.method public static b(Lax/l;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 665
    invoke-virtual {p0}, Lax/l;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 666
    const/16 v0, 0xe7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 684
    :goto_e
    return-object v0

    .line 669
    :cond_f
    const-string v0, ""

    .line 670
    invoke-virtual {p0}, Lax/l;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_44

    .line 682
    :goto_18
    invoke-virtual {p0}, Lax/l;->d()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_42

    move v1, v2

    :goto_20
    invoke-static {p0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/l;Z)Ljava/lang/String;

    move-result-object v1

    .line 684
    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 672
    :pswitch_2d
    const/16 v0, 0xe6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    .line 675
    :pswitch_34
    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    .line 678
    :pswitch_3b
    const/16 v0, 0x20f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    :cond_42
    move v1, v3

    .line 682
    goto :goto_20

    .line 670
    :pswitch_data_44
    .packed-switch 0x0
        :pswitch_2d
        :pswitch_34
        :pswitch_3b
    .end packed-switch
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 537
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    .line 538
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v1}, Lax/b;->q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 540
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v1}, Lax/b;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->e(I)V

    .line 541
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->o(I)V

    .line 542
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    .line 544
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    .line 545
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    .line 546
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 513
    const/4 v0, 0x4

    const-string v1, "sa"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 515
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 519
    const/16 v0, 0x38

    const-string v1, "ir"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 521
    return-void
.end method

.method private y()V
    .registers 5

    .prologue
    .line 275
    sget-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-eqz v1, :cond_2a

    .line 280
    :try_start_a
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 281
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 282
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v3, v2}, Lax/b;->b(Ljava/io/DataOutput;)V

    .line 284
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const-string v2, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_22} :catch_2e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_22} :catch_23

    .line 295
    :goto_22
    return-void

    .line 287
    :catch_23
    move-exception v0

    .line 290
    const-string v1, "UI-DL save"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_22

    .line 293
    :cond_2a
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    goto :goto_22

    .line 285
    :catch_2e
    move-exception v0

    goto :goto_22
.end method

.method private z()V
    .registers 3

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 382
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    .line 383
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    .line 385
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->h()Lcom/google/googlenav/ui/view/q;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    .line 387
    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x3

    const/16 v2, 0x36

    .line 393
    invoke-virtual {p1}, Lat/a;->b()I

    move-result v1

    .line 394
    if-eq v1, v2, :cond_d

    const/16 v0, 0x34

    if-ne v1, v0, :cond_22

    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->ax()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 396
    if-ne v1, v2, :cond_23

    const-string v0, "l"

    .line 399
    :goto_19
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    .line 400
    if-ne v1, v2, :cond_26

    const/4 v0, 0x1

    :goto_1f
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    .line 404
    :cond_22
    return v3

    .line 396
    :cond_23
    const-string v0, "e"

    goto :goto_19

    .line 400
    :cond_26
    const/4 v0, -0x1

    goto :goto_1f
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 583
    const/4 v0, 0x4

    return v0
.end method

.method public a(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-ne v0, p1, :cond_5

    .line 269
    :goto_4
    return-void

    .line 225
    :cond_5
    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 226
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    .line 228
    :cond_10
    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2e

    .line 229
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    if-eqz v0, :cond_1d

    .line 231
    iget-object p1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 233
    :cond_1d
    invoke-virtual {p1}, Lax/b;->aj()I

    move-result v0

    if-gez v0, :cond_6d

    .line 234
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x30e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 239
    :cond_2e
    :goto_2e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 240
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    .line 243
    invoke-virtual {p1}, Lax/b;->aa()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 244
    const-string v0, "ha"

    invoke-static {v0}, Lbf/O;->c(Ljava/lang/String;)V

    .line 250
    :cond_3e
    invoke-virtual {p1}, Lax/b;->C()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 251
    const-string v0, "sa"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    .line 254
    :cond_49
    invoke-virtual {p1}, Lax/b;->D()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_54

    .line 255
    const-string v0, "ea"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    .line 258
    :cond_54
    invoke-virtual {p1}, Lax/b;->aF()[I

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_60

    .line 259
    const-string v0, "a"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    .line 263
    :cond_60
    new-instance v0, Lcom/google/googlenav/ui/wizard/bw;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/bw;-><init>(Lcom/google/googlenav/ui/wizard/bv;Las/c;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bw;->g()V

    goto :goto_4

    .line 236
    :cond_6d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x30f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_2e
.end method

.method public a(Lax/l;)V
    .registers 3
    .parameter

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-nez v0, :cond_8

    .line 560
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 568
    :goto_7
    return-void

    .line 564
    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 565
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    .line 566
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    .line 567
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->a(Lax/l;)V

    goto :goto_7
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 409
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/ah;

    if-eqz v0, :cond_31

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v1, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v5

    move v0, p1

    invoke-static/range {v0 .. v6}, Lbf/O;->a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 415
    packed-switch p1, :pswitch_data_d0

    .line 502
    :goto_2c
    :pswitch_2c
    return v6

    .line 419
    :pswitch_2d
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    goto :goto_2c

    .line 425
    :cond_31
    sparse-switch p1, :sswitch_data_dc

    .line 502
    const/4 v6, 0x0

    goto :goto_2c

    .line 427
    :sswitch_36
    const-string v0, "o"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 430
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    .line 431
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->d(Lax/b;)V

    goto :goto_2c

    .line 435
    :sswitch_4a
    const-string v0, "t"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    .line 436
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 437
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 439
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v0

    .line 441
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v1}, Lax/b;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Lcom/google/googlenav/ui/wizard/bN;->a(Lax/l;Lcom/google/googlenav/ui/wizard/bO;)V

    goto :goto_2c

    .line 446
    :sswitch_6c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    .line 447
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v0

    invoke-virtual {v0, v7}, Lax/b;->r(I)V

    .line 451
    invoke-direct {p0, p2, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_2c

    .line 455
    :sswitch_81
    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    .line 456
    const-string v0, "e"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    goto :goto_2c

    .line 461
    :sswitch_8a
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    .line 462
    const-string v0, "l"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    goto :goto_2c

    .line 468
    :sswitch_93
    const-string v0, "od"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    .line 469
    invoke-direct {p0, v8}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 474
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 475
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto :goto_2c

    .line 479
    :sswitch_a4
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->z()V

    .line 480
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 481
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    .line 482
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    goto/16 :goto_2c

    .line 487
    :sswitch_b4
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-ne v0, v8, :cond_c8

    .line 488
    const-string v0, "nc"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_c8

    .line 493
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 494
    iput-object v9, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 497
    :cond_c8
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 498
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto/16 :goto_2c

    .line 415
    :pswitch_data_d0
    .packed-switch 0xe8
        :pswitch_2d
        :pswitch_2c
        :pswitch_2d
        :pswitch_2d
    .end packed-switch

    .line 425
    :sswitch_data_dc
    .sparse-switch
        0xd7 -> :sswitch_36
        0xd8 -> :sswitch_4a
        0xd9 -> :sswitch_6c
        0xda -> :sswitch_81
        0xdb -> :sswitch_8a
        0xdc -> :sswitch_93
        0xdd -> :sswitch_a4
        0x1f4 -> :sswitch_b4
    .end sparse-switch
.end method

.method protected b()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 588
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-static {v0}, Lbf/O;->b(Lax/b;)V

    .line 589
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->az()I

    move-result v0

    .line 592
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v1

    .line 593
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_24

    .line 594
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-interface {v2, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 598
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lax/b;->b(Ljava/lang/String;)V

    .line 600
    :cond_24
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2b

    .line 601
    invoke-direct {p0, v0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    .line 625
    :goto_2a
    return-void

    .line 602
    :cond_2b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    instance-of v0, v0, Lax/s;

    if-eqz v0, :cond_36

    .line 603
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_2a

    .line 604
    :cond_36
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4b

    .line 605
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    const/16 v1, 0x2bc

    invoke-static {v0, v1, p0}, Lcom/google/googlenav/ui/f;->a(Lax/b;ILcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/C;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 607
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_2a

    .line 610
    :cond_4b
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 614
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 615
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bN;->a()V

    .line 619
    :cond_67
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_70

    .line 620
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 622
    :cond_70
    new-instance v0, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/ah;-><init>(Lcom/google/googlenav/ui/e;Lax/b;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 623
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_2a
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 629
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 631
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 632
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    .line 634
    :cond_e
    return-void
.end method

.method public e()Lax/b;
    .registers 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    return-object v0
.end method

.method public f()V
    .registers 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    .line 212
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 301
    sget-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 302
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    .line 729
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 730
    const/16 v0, 0x1f4

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bv;->a(IILjava/lang/Object;)Z

    .line 765
    :cond_c
    :goto_c
    return-void

    .line 733
    :cond_d
    const-string v0, "b"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    .line 735
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->A()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    .line 742
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-nez v0, :cond_c

    .line 744
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    .line 746
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 747
    if-eqz v0, :cond_c

    .line 748
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_54

    .line 761
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_c

    .line 752
    :sswitch_39
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_c

    .line 755
    :sswitch_48
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    .line 756
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_c

    .line 748
    nop

    :sswitch_data_54
    .sparse-switch
        0x1 -> :sswitch_48
        0x5 -> :sswitch_39
    .end sparse-switch
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 638
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public p()Z
    .registers 3

    .prologue
    .line 342
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_18

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_18

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_18

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public x()Ljava/lang/String;
    .registers 6

    .prologue
    const/16 v4, 0x26

    const/16 v3, 0x3d

    .line 776
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 777
    const-string v0, "Directions"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    .line 781
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 784
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    .line 785
    if-eqz v1, :cond_32

    .line 786
    const-string v2, "saddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 787
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 788
    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 793
    :cond_32
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    .line 794
    if-eqz v1, :cond_4e

    .line 795
    const-string v2, "daddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 797
    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 802
    :cond_4e
    const-string v1, "dirflg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 804
    const-string v1, "r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
