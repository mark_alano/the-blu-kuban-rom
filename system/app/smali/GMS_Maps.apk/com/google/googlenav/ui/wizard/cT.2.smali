.class public Lcom/google/googlenav/ui/wizard/cT;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaB/s;

.field private final b:Lcom/google/googlenav/android/aa;

.field private c:Lcom/google/googlenav/ui/wizard/cY;

.field private d:Lcom/google/googlenav/friend/reporting/s;


# direct methods
.method public constructor <init>(LaB/s;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/friend/reporting/s;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cT;->a:LaB/s;

    .line 77
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cT;->b:Lcom/google/googlenav/android/aa;

    .line 78
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/cT;->d:Lcom/google/googlenav/friend/reporting/s;

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cT;)Lcom/google/googlenav/ui/wizard/cY;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->c:Lcom/google/googlenav/ui/wizard/cY;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cT;Lcom/google/googlenav/friend/af;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/cT;->e(Lcom/google/googlenav/friend/af;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/cT;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->b:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private e(Lcom/google/googlenav/friend/af;)V
    .registers 5
    .parameter

    .prologue
    .line 257
    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->f()Ljava/lang/String;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cT;->a:LaB/s;

    if-eqz v1, :cond_18

    if-eqz v0, :cond_18

    .line 259
    new-instance v1, Lcom/google/googlenav/ui/bs;

    const v2, 0x7f0b00b8

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    .line 261
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->a:LaB/s;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    .line 263
    :cond_18
    return-void
.end method


# virtual methods
.method a(Lcom/google/googlenav/friend/S;)Lcom/google/googlenav/friend/T;
    .registers 3
    .parameter

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/google/googlenav/friend/S;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/T;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/friend/af;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/af;->a(Z)V

    .line 93
    new-instance v0, Lcom/google/googlenav/ui/wizard/cU;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/ui/wizard/cU;-><init>(Lcom/google/googlenav/ui/wizard/cT;Las/c;Lcom/google/googlenav/friend/af;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cU;->g()V

    .line 109
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->c:Lcom/google/googlenav/ui/wizard/cY;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/cY;->a()V

    .line 110
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/cY;)V
    .registers 2
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cT;->c:Lcom/google/googlenav/ui/wizard/cY;

    .line 83
    return-void
.end method

.method b(Lcom/google/googlenav/friend/af;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 122
    new-instance v0, Lcom/google/googlenav/friend/S;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    .line 125
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/cT;->a(Lcom/google/googlenav/friend/S;)Lcom/google/googlenav/friend/T;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_37

    iget-boolean v1, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-eqz v1, :cond_37

    .line 127
    new-instance v1, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 129
    invoke-virtual {v1, v3}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/af;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 130
    invoke-virtual {v1, v4}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/af;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 131
    invoke-virtual {p1, v3}, Lcom/google/googlenav/friend/af;->a(Z)V

    .line 136
    :goto_36
    return-void

    .line 135
    :cond_37
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/af;->a(Z)V

    goto :goto_36
.end method

.method public c(Lcom/google/googlenav/friend/af;)V
    .registers 4
    .parameter

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/af;->a(Z)V

    .line 159
    new-instance v0, Lcom/google/googlenav/ui/wizard/cW;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/ui/wizard/cW;-><init>(Lcom/google/googlenav/ui/wizard/cT;Las/c;Lcom/google/googlenav/friend/af;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cW;->g()V

    .line 166
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->c:Lcom/google/googlenav/ui/wizard/cY;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/cY;->a()V

    .line 167
    return-void
.end method

.method d(Lcom/google/googlenav/friend/af;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 176
    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->i()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 177
    new-instance v0, Lcom/google/googlenav/friend/aP;

    invoke-direct {v0, v2, v2, v1, v6}, Lcom/google/googlenav/friend/aP;-><init>(ZIILcom/google/googlenav/friend/aQ;)V

    .line 182
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aP;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aR;

    .line 184
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aR;->a()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 185
    invoke-static {v2}, Lcom/google/googlenav/friend/as;->a(Z)Z

    .line 186
    invoke-static {}, Lcom/google/googlenav/friend/aH;->f()V

    .line 190
    :cond_22
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 192
    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->j()Z

    move-result v3

    if-eqz v3, :cond_5f

    .line 195
    const/4 v3, 0x7

    const/4 v4, 0x4

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 199
    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 203
    invoke-virtual {p1}, Lcom/google/googlenav/friend/af;->c()Z

    move-result v3

    if-nez v3, :cond_99

    .line 204
    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    .line 216
    :goto_41
    new-instance v3, Lcom/google/googlenav/friend/aM;

    invoke-direct {v3, v0, v6}, Lcom/google/googlenav/friend/aM;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/aN;)V

    .line 219
    invoke-virtual {v3}, Lcom/google/googlenav/friend/aM;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aO;

    .line 220
    if-eqz v0, :cond_54

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aO;->a()Z

    move-result v0

    if-nez v0, :cond_67

    .line 222
    :cond_54
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->b:Lcom/google/googlenav/android/aa;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cX;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/cX;-><init>(Lcom/google/googlenav/ui/wizard/cT;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 253
    :cond_5e
    :goto_5e
    return-void

    .line 209
    :cond_5f
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 213
    invoke-virtual {v0, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_41

    .line 232
    :cond_67
    if-eqz v1, :cond_6e

    .line 235
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cT;->d:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/reporting/s;->a(Z)V

    .line 241
    :cond_6e
    new-instance v0, Lcom/google/googlenav/friend/S;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1, v6, v2}, Lcom/google/googlenav/friend/S;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    .line 244
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/cT;->a(Lcom/google/googlenav/friend/S;)Lcom/google/googlenav/friend/T;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_5e

    iget-boolean v1, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-eqz v1, :cond_5e

    .line 246
    new-instance v1, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 248
    new-instance v0, Lcom/google/googlenav/friend/ae;

    invoke-virtual {v1, v7}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 251
    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->a(Lcom/google/googlenav/friend/ae;)V

    goto :goto_5e

    :cond_99
    move v1, v2

    goto :goto_41
.end method
