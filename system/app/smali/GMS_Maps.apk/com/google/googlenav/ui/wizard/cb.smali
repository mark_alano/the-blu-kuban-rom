.class public Lcom/google/googlenav/ui/wizard/cb;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Lcom/google/googlenav/ui/wizard/cc;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 36
    return-void
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 49
    iget v0, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    if-nez v0, :cond_6

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 3
    .parameter

    .prologue
    .line 93
    const/4 v0, 0x4

    return v0
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 101
    const/4 v0, 0x4

    return v0
.end method

.method public a(ILcom/google/googlenav/ui/wizard/cc;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    .line 41
    iput p1, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    .line 42
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->j()V

    .line 43
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/cd;)V
    .registers 3
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/wizard/cc;->a(Lcom/google/googlenav/ui/wizard/cd;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    .line 112
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->h()V

    .line 116
    if-eqz p1, :cond_e

    .line 117
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 119
    :cond_e
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cb;->e()Z

    move-result v0

    if-nez v0, :cond_15

    .line 63
    new-instance v0, Lcom/google/googlenav/ui/view/android/L;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/L;-><init>(Lcom/google/googlenav/ui/wizard/cb;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 70
    :goto_f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 71
    return-void

    .line 67
    :cond_15
    new-instance v0, Lcom/google/googlenav/ui/view/android/D;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/D;-><init>(Lcom/google/googlenav/ui/wizard/cb;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_f
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 77
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    if-eqz v0, :cond_c

    .line 78
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/cc;->a()V

    .line 80
    :cond_c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    .line 81
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 85
    iget v0, p0, Lcom/google/googlenav/ui/wizard/cb;->a:I

    .line 86
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cb;->b:Lcom/google/googlenav/ui/wizard/cc;

    .line 87
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    .line 88
    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/cb;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    .line 89
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cb;->a()V

    .line 107
    return-void
.end method
