.class public Lcom/google/googlenav/ui/wizard/dF;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Z

.field d:Ljava/lang/Boolean;

.field e:LaN/B;

.field f:I

.field g:Ljava/lang/String;

.field h:Lcom/google/googlenav/ui/wizard/dy;

.field i:I

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:B

.field m:Z

.field n:Z

.field o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:Ljava/util/List;

.field r:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    .line 1817
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    .line 1820
    const v0, 0x1869f

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dF;->f:I

    .line 1823
    iput v1, p0, Lcom/google/googlenav/ui/wizard/dF;->i:I

    return-void
.end method


# virtual methods
.method public a(B)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1943
    iput-byte p1, p0, Lcom/google/googlenav/ui/wizard/dF;->l:B

    .line 1944
    return-object p0
.end method

.method public a(I)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1877
    iput p1, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    .line 1878
    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1966
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1967
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1919
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    .line 1920
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1858
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->a:Ljava/lang/String;

    .line 1859
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1986
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    .line 1987
    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/ui/wizard/dF;
    .registers 3
    .parameter

    .prologue
    .line 1868
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->d:Ljava/lang/Boolean;

    .line 1869
    return-object p0
.end method

.method a()V
    .registers 3

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    if-nez v0, :cond_c

    .line 1837
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No callback provided to LocationSelectionWizard."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1839
    :cond_c
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dF;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_19

    .line 1840
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No sourceOption provided to LocationSelectionWizard."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1842
    :cond_19
    return-void
.end method

.method public b(I)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1925
    iput p1, p0, Lcom/google/googlenav/ui/wizard/dF;->i:I

    .line 1926
    return-object p0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1976
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1977
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1913
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->g:Ljava/lang/String;

    .line 1914
    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1888
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    .line 1889
    return-object p0
.end method

.method b()Z
    .registers 2

    .prologue
    .line 1853
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1931
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->j:Ljava/lang/String;

    .line 1932
    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1949
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->m:Z

    .line 1950
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1937
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dF;->k:Ljava/lang/String;

    .line 1938
    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1955
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->n:Z

    .line 1956
    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/ui/wizard/dF;
    .registers 2
    .parameter

    .prologue
    .line 1992
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dF;->r:Z

    .line 1993
    return-object p0
.end method
