.class public Lcom/google/googlenav/ui/wizard/dM;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1081
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dM;->a:Lcom/google/googlenav/ui/wizard/dg;

    .line 1082
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    .line 1083
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1079
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dM;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V

    return-void
.end method


# virtual methods
.method public c()Landroid/view/View;
    .registers 9

    .prologue
    const v2, 0x7f100031

    const v3, 0x7f100030

    const/4 v7, 0x0

    .line 1092
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dM;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040164

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 1094
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_46

    .line 1095
    const v0, 0x7f100025

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1096
    const/16 v1, 0x4fc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1097
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1098
    const v0, 0x7f100033

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1099
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_a7

    .line 1101
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1109
    :cond_46
    :goto_46
    const v0, 0x7f10018f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1112
    const v1, 0x7f10002e

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1113
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1116
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_b1

    move v1, v2

    :goto_60
    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1118
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1119
    const/16 v5, 0x4f5

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v6}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1121
    new-instance v5, Lcom/google/googlenav/ui/wizard/dN;

    invoke-direct {v5, p0, v0}, Lcom/google/googlenav/ui/wizard/dN;-><init>(Lcom/google/googlenav/ui/wizard/dM;Landroid/widget/EditText;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1130
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_b3

    :goto_86
    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1132
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1133
    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1135
    new-instance v1, Lcom/google/googlenav/ui/wizard/dO;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/dO;-><init>(Lcom/google/googlenav/ui/wizard/dM;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1142
    return-object v4

    .line 1103
    :cond_a7
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1104
    const v1, 0x7f020215

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_46

    :cond_b1
    move v1, v3

    .line 1116
    goto :goto_60

    :cond_b3
    move v3, v2

    .line 1130
    goto :goto_86
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1087
    const/16 v0, 0x4fc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
