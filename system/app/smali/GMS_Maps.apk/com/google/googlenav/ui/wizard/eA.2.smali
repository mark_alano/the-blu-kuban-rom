.class Lcom/google/googlenav/ui/wizard/eA;
.super Lcom/google/googlenav/ui/wizard/eI;
.source "SourceFile"


# instance fields
.field private final a:LaR/n;

.field private final b:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(LaR/n;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-static {p2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eA;->b:Ljava/text/DateFormat;

    .line 33
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eA;->a:LaR/n;

    .line 34
    return-void
.end method

.method private a(LaR/D;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eA;->b:Ljava/text/DateFormat;

    invoke-static {p1, v0}, LaR/G;->a(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .registers 15

    .prologue
    const/4 v8, 0x0

    .line 52
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eA;->a:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->b()Ljava/util/List;

    move-result-object v9

    .line 53
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    .line 54
    invoke-static {v10}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eA;->a:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v12

    move v7, v8

    .line 56
    :goto_1a
    if-ge v7, v10, :cond_54

    .line 57
    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v12, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LaR/D;

    .line 58
    if-eqz v6, :cond_35

    invoke-virtual {v6}, LaR/D;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 56
    :cond_35
    :goto_35
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1a

    .line 62
    :cond_39
    new-instance v0, Lcom/google/googlenav/ui/view/android/be;

    invoke-virtual {v6}, LaR/D;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/eA;->a(LaR/D;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, LaR/D;->i()J

    move-result-wide v3

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/4 v13, 0x1

    invoke-direct {v5, v13, v8, v6}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/be;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;JLcom/google/googlenav/ui/view/a;)V

    .line 70
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_35

    .line 72
    :cond_54
    return-object v11
.end method
