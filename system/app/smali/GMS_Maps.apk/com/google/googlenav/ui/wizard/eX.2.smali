.class abstract Lcom/google/googlenav/ui/wizard/eX;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/eP;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/eP;)V
    .registers 3
    .parameter

    .prologue
    .line 633
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eX;->a:Lcom/google/googlenav/ui/wizard/eP;

    .line 634
    const v0, 0x7f0f001b

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    .line 635
    return-void
.end method


# virtual methods
.method protected I_()V
    .registers 3

    .prologue
    .line 674
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 675
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 679
    :cond_10
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->w_()Ljava/lang/String;

    move-result-object v0

    .line 680
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 681
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eX;->h:Landroid/view/View;

    const v1, 0x7f10033f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 682
    if-eqz v0, :cond_2e

    .line 683
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    :cond_2e
    return-void
.end method

.method protected O_()V
    .registers 3

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 691
    return-void
.end method

.method public P_()Z
    .registers 2

    .prologue
    .line 700
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
.end method

.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 642
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 643
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eX;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/eX;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 644
    new-instance v2, Lcom/google/googlenav/ui/wizard/eY;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/eY;-><init>(Lcom/google/googlenav/ui/wizard/eX;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 661
    return-object v1
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 695
    const/4 v0, 0x0

    return v0
.end method

.method public abstract w_()Ljava/lang/String;
.end method
