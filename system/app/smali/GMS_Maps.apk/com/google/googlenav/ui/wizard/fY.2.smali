.class public Lcom/google/googlenav/ui/wizard/fY;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/ga;

.field private b:Lcom/google/googlenav/ui/wizard/gc;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 96
    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 249
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 250
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->h()V

    .line 251
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    .line 254
    :goto_d
    return v0

    :cond_e
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    goto :goto_d
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 259
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fY;->g:I

    return v0
.end method

.method a(Lcom/google/googlenav/h;)V
    .registers 3
    .parameter

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 213
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/wizard/gb;->a(Lcom/google/googlenav/h;)V

    .line 215
    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    .line 216
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/ga;)V
    .registers 2
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 105
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 106
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_c

    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 120
    :cond_c
    new-instance v0, Lcom/google/googlenav/ui/wizard/gc;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ga;->a(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/ga;->b(Lcom/google/googlenav/ui/wizard/ga;)LaH/h;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/wizard/fZ;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/wizard/fZ;-><init>(Lcom/google/googlenav/ui/wizard/fY;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/gc;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;LaH/h;Lcom/google/googlenav/ui/wizard/gh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    .line 166
    new-instance v0, Lcom/google/googlenav/ui/wizard/fM;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ga;->c(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/view/android/aB;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/ga;->d(Lcom/google/googlenav/ui/wizard/ga;)Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/wizard/fM;-><init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/android/aB;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 170
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gc;->a()Lcom/google/googlenav/ui/wizard/fV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->a(Lcom/google/googlenav/ui/wizard/fV;)V

    .line 173
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/gc;->a(Lcom/google/googlenav/ui/wizard/fM;)V

    .line 175
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/ga;->b(Lcom/google/googlenav/ui/wizard/ga;)LaH/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gc;->a(LaH/h;)V

    .line 179
    return-void
.end method

.method protected c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    if-eqz v0, :cond_c

    .line 184
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gc;->a(Lcom/google/googlenav/ui/wizard/fM;)V

    .line 185
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    .line 188
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_17

    .line 189
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->a(Lcom/google/googlenav/ui/wizard/fV;)V

    .line 192
    :cond_17
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_2a

    .line 197
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->a(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    .line 200
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 204
    :cond_2a
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 205
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 232
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 233
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    .line 235
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    .line 236
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->j()V

    .line 237
    return-void
.end method

.method e()V
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 224
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->a:Lcom/google/googlenav/ui/wizard/ga;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ga;->e(Lcom/google/googlenav/ui/wizard/ga;)Lcom/google/googlenav/ui/wizard/gb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gb;->a()V

    .line 226
    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->a()V

    .line 227
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fY;->b:Lcom/google/googlenav/ui/wizard/gc;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gc;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 243
    :cond_c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fY;->e()V

    .line 245
    :cond_f
    return-void
.end method
