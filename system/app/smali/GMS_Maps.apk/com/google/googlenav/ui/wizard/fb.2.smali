.class Lcom/google/googlenav/ui/wizard/fB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/af;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fq;

.field private b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/ui/wizard/fA;

.field private d:Lcom/google/googlenav/ui/wizard/fH;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/fq;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/fA;Lcom/google/googlenav/ui/wizard/fH;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 823
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 824
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    .line 825
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    .line 826
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/fB;->d:Lcom/google/googlenav/ui/wizard/fH;

    .line 827
    return-void
.end method

.method private a(ZZ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 840
    if-eqz p1, :cond_21

    .line 843
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->as()V

    .line 844
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 845
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    if-eqz v0, :cond_17

    .line 846
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->c:Lcom/google/googlenav/ui/wizard/fA;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->a()V

    .line 860
    :cond_17
    :goto_17
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    .line 861
    return-void

    .line 849
    :cond_21
    if-eqz p2, :cond_39

    const/16 v0, 0x389

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 854
    :goto_29
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fB;->d:Lcom/google/googlenav/ui/wizard/fH;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    .line 857
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/wizard/fq;->b(Lcom/google/googlenav/ui/wizard/fq;Z)Z

    .line 858
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;Z)V

    goto :goto_17

    .line 849
    :cond_39
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fB;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_48

    const/16 v0, 0x36c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_29

    :cond_48
    const/16 v0, 0x37f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fB;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_29
.end method


# virtual methods
.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 831
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/fB;->a(ZZ)V

    .line 832
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 836
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/wizard/fB;->a(ZZ)V

    .line 837
    return-void
.end method
