.class public Lcom/google/googlenav/ui/wizard/fm;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fl;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/fl;)V
    .registers 2
    .parameter

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    return-void
.end method


# virtual methods
.method protected K_()Z
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fl;->a()V

    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 135
    const v0, 0x7f020351

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    .line 136
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fp;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fp;-><init>(Lcom/google/googlenav/ui/wizard/fm;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    .line 146
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004cb

    if-ne v0, v1, :cond_16

    .line 162
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/fl;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    .line 163
    const/4 v0, 0x1

    .line 165
    :goto_15
    return v0

    :cond_16
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_15
.end method

.method protected c()Landroid/view/View;
    .registers 9

    .prologue
    const v7, 0x7f10001e

    const/4 v6, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fm;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 84
    const v1, 0x7f04010a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 86
    const v0, 0x7f100200

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 87
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_76

    .line 88
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fm;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/fm;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 103
    :goto_2f
    const v0, 0x7f100034

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    const v0, 0x7f100250

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 107
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 108
    const/16 v3, 0x323

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v0, 0x7f100023

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 112
    const/16 v3, 0x328

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    new-instance v0, Lcom/google/googlenav/ui/wizard/fo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fo;-><init>(Lcom/google/googlenav/ui/wizard/fm;)V

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 120
    return-object v2

    .line 91
    :cond_76
    const/4 v1, 0x1

    const v3, 0x7f020351

    new-instance v4, Lcom/google/googlenav/ui/wizard/fn;

    invoke-direct {v4, p0}, Lcom/google/googlenav/ui/wizard/fn;-><init>(Lcom/google/googlenav/ui/wizard/fm;)V

    new-array v5, v6, [I

    invoke-virtual {p0, v1, v3, v4, v5}, Lcom/google/googlenav/ui/wizard/fm;->a(ZILaA/f;[I)V

    .line 98
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 99
    const/16 v3, 0x31c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2f
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fm;->a:Lcom/google/googlenav/ui/wizard/fl;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fl;->a()V

    .line 177
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 150
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 152
    const v1, 0x7f110019

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 153
    const v0, 0x7f1004cb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 154
    const/16 v1, 0x2da

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 125
    const/16 v0, 0x31c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
