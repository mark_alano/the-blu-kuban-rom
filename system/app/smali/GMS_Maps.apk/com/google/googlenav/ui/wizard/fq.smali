.class public Lcom/google/googlenav/ui/wizard/fq;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static final c:I


# instance fields
.field protected a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/ui/wizard/fA;

.field private final i:Lcom/google/googlenav/J;

.field private final j:Lcom/google/googlenav/android/BaseMapsActivity;

.field private k:Lcom/google/googlenav/ai;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Lcom/google/googlenav/ui/wizard/fH;

.field private q:Z

.field private r:Landroid/view/LayoutInflater;

.field private s:Landroid/app/AlertDialog;

.field private final t:LaM/h;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 96
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x400

    :goto_c
    sput v0, Lcom/google/googlenav/ui/wizard/fq;->c:I

    return-void

    :cond_f
    const/16 v0, 0x200

    goto :goto_c
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;Lcom/google/googlenav/android/BaseMapsActivity;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    .line 145
    new-instance v0, Lcom/google/googlenav/ui/wizard/fr;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fr;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    .line 185
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fq;->i:Lcom/google/googlenav/J;

    .line 186
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    .line 187
    new-instance v0, Lcom/google/googlenav/ui/wizard/fs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fs;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    .line 193
    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 277
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bm;->v()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 283
    const/16 v0, 0x65

    const-string v1, "w"

    const-string v2, "o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/br;->b:Lcom/google/googlenav/br;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/br;)V

    .line 292
    :goto_1c
    return-void

    .line 290
    :cond_1d
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->C()V

    goto :goto_1c
.end method

.method private B()V
    .registers 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 330
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/fC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fC;->h()V

    .line 332
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_19

    .line 336
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 337
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->i()V

    .line 352
    :cond_19
    :goto_19
    return-void

    .line 344
    :cond_1a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 345
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    const/16 v1, 0x36c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 347
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    if-eqz v0, :cond_19

    .line 348
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    goto :goto_19
.end method

.method private C()V
    .registers 7

    .prologue
    const/16 v5, 0x36c

    const/4 v2, 0x0

    .line 355
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    if-eqz v0, :cond_e

    .line 357
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 358
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    .line 439
    :cond_d
    :goto_d
    return-void

    .line 359
    :cond_e
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_5a

    .line 362
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    .line 364
    :try_start_1b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ay;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    sget v3, Lcom/google/googlenav/ui/wizard/fq;->c:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbm/a;->a(Ljava/lang/String;IZ)Lam/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ay;-><init>(Lam/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Lcom/google/googlenav/ay;)Z
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_34} :catch_44
    .catch Ljava/lang/NullPointerException; {:try_start_1b .. :try_end_34} :catch_4f

    .line 374
    :goto_34
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 375
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    goto :goto_d

    .line 368
    :catch_44
    move-exception v0

    .line 369
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto :goto_34

    .line 370
    :catch_4f
    move-exception v0

    .line 371
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto :goto_34

    .line 378
    :cond_5a
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    .line 379
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v1, Lcom/google/googlenav/ui/wizard/ft;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/ft;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    goto :goto_d
.end method

.method private static D()Z
    .registers 2

    .prologue
    .line 457
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PHOTO_UPLOAD_LEGAL_INFO_DISMISS"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    .line 458
    if-eqz v0, :cond_19

    const/4 v1, 0x2

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v0

    if-ne v1, v0, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private E()V
    .registers 5

    .prologue
    .line 466
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 467
    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "support.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/gmm/bin/answer.py"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hl"

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "answer"

    const-string v3, "1650741"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 469
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    .line 470
    return-void
.end method

.method private F()V
    .registers 11

    .prologue
    const/4 v1, 0x1

    .line 681
    const/16 v0, 0x65

    const-string v2, "w"

    const-string v3, "s"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 685
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ae;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-eqz v0, :cond_3b

    move v0, v1

    :goto_17
    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    new-instance v6, Lcom/google/googlenav/ui/wizard/fB;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/google/googlenav/ui/wizard/fB;-><init>(Lcom/google/googlenav/ui/wizard/fq;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/fA;Lcom/google/googlenav/ui/wizard/fH;)V

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/google/googlenav/ae;-><init>(Lcom/google/googlenav/ai;ILjava/lang/String;Lcom/google/googlenav/af;)V

    invoke-virtual {v2, v3}, Law/h;->c(Law/g;)V

    .line 695
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    .line 696
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    .line 699
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    const/16 v1, 0x37e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    .line 700
    return-void

    .line 685
    :cond_3b
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private G()Ljava/lang/String;
    .registers 5

    .prologue
    .line 704
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    .line 705
    if-eqz v0, :cond_1d

    iget-object v1, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 706
    const/16 v1, 0x382

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 710
    :goto_1c
    return-object v0

    :cond_1d
    const/16 v0, 0x381

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1c
.end method

.method private H()V
    .registers 7

    .prologue
    .line 716
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->G()Ljava/lang/String;

    move-result-object v0

    .line 718
    const/16 v1, 0x383

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 719
    const/16 v2, 0x35c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 720
    const/16 v3, 0x42d

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 722
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fw;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fw;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fv;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fv;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/fu;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fu;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 748
    new-instance v1, Lcom/google/googlenav/ui/wizard/fx;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fx;-><init>(Lcom/google/googlenav/ui/wizard/fq;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 762
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 763
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fq;->r:Landroid/view/LayoutInflater;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->C()V

    return-void
.end method

.method private a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 773
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 774
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->invalidateOptionsMenu()V

    .line 781
    :goto_11
    return-void

    .line 776
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f10003f

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 777
    if-eqz p1, :cond_3a

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    if-nez v1, :cond_3a

    move v1, v2

    :goto_24
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 778
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f100031

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 779
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    if-nez v1, :cond_3c

    :goto_36
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_11

    :cond_3a
    move v1, v3

    .line 777
    goto :goto_24

    :cond_3c
    move v2, v3

    .line 779
    goto :goto_36
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fq;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/J;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->i:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/fq;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/fq;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fH;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->p:Lcom/google/googlenav/ui/wizard/fH;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/fq;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->q:Z

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->B()V

    return-void
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/fq;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->r:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public static g()V
    .registers 3

    .prologue
    .line 447
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v0

    .line 448
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "PHOTO_UPLOAD_LEGAL_INFO_DISMISS"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 450
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 451
    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/fq;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    return v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->F()V

    return-void
.end method

.method static synthetic k(Lcom/google/googlenav/ui/wizard/fq;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    return v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->A()V

    return-void
.end method

.method static synthetic m(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->E()V

    return-void
.end method

.method static synthetic z()I
    .registers 1

    .prologue
    .line 65
    sget v0, Lcom/google/googlenav/ui/wizard/fq;->c:I

    return v0
.end method


# virtual methods
.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 271
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 215
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 218
    :cond_9
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    .line 219
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    .line 222
    if-eqz p2, :cond_3a

    const/4 v0, 0x1

    :goto_10
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    .line 224
    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    .line 225
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    .line 227
    new-instance v0, Lcom/google/googlenav/ui/wizard/fC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fC;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 230
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 231
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    .line 234
    :cond_2d
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->j()V

    .line 237
    return-void

    .line 222
    :cond_3a
    const/4 v0, 0x0

    goto :goto_10
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 297
    invoke-static {}, Lcom/google/googlenav/ui/wizard/fq;->D()Z

    move-result v0

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-nez v0, :cond_e

    .line 300
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->H()V

    .line 304
    :goto_d
    return-void

    .line 302
    :cond_e
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fq;->A()V

    goto :goto_d
.end method

.method protected c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 251
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 253
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    .line 254
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    .line 255
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    .line 256
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    .line 257
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->n:Z

    .line 259
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1a

    .line 260
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 261
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    .line 266
    :cond_1a
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->t:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    .line 267
    return-void
.end method

.method public d()V
    .registers 5

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->k:Lcom/google/googlenav/ai;

    .line 242
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fq;->l:Ljava/lang/String;

    .line 243
    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/fq;->o:Z

    .line 244
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->b:Lcom/google/googlenav/ui/wizard/fA;

    .line 245
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 246
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V

    .line 247
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/fq;->m:Z

    if-nez v0, :cond_5

    .line 320
    :cond_4
    :goto_4
    return-void

    .line 315
    :cond_5
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/fC;

    if-eqz v0, :cond_4

    .line 317
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fq;->a(Z)V

    .line 318
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fq;->i()V

    goto :goto_4
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 324
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled()Z

    move-result v0

    return v0
.end method

.method public i()V
    .registers 5

    .prologue
    .line 784
    const/16 v0, 0x35c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 785
    const/16 v1, 0x36e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 787
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/fq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/aL;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/fz;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/fz;-><init>(Lcom/google/googlenav/ui/wizard/fq;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    .line 796
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->s:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 797
    return-void
.end method

.method protected y()Z
    .registers 2

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fq;->j:Lcom/google/googlenav/android/BaseMapsActivity;

    check-cast v0, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    return v0
.end method
