.class Lcom/google/googlenav/ui/wizard/ft;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/cc;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fq;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 2
    .parameter

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->o()Z

    move-result v0

    if-nez v0, :cond_9

    .line 436
    :cond_8
    :goto_8
    return-void

    .line 422
    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->e(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 426
    const/16 v0, 0x65

    const-string v1, "w"

    const-string v2, "cp"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    goto :goto_8

    .line 432
    :cond_20
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 433
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->f(Lcom/google/googlenav/ui/wizard/fq;)V

    goto :goto_8
.end method

.method public a(Lcom/google/googlenav/ui/wizard/cd;)V
    .registers 11
    .parameter

    .prologue
    const/16 v8, 0x37f

    const/16 v7, 0x65

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 383
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->o()Z

    move-result v0

    if-nez v0, :cond_f

    .line 414
    :goto_e
    return-void

    .line 388
    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    .line 390
    :try_start_1c
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ay;

    iget-object v2, p1, Lcom/google/googlenav/ui/wizard/cd;->c:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/fq;->z()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbm/a;->a(Ljava/lang/String;IZ)Lam/f;

    move-result-object v2

    iget-object v3, p1, Lcom/google/googlenav/ui/wizard/cd;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ay;-><init>(Lam/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Lcom/google/googlenav/ay;)Z

    .line 394
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ui/wizard/fq;Z)Z
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_41} :catch_42
    .catch Ljava/lang/NullPointerException; {:try_start_1c .. :try_end_41} :catch_6a

    goto :goto_e

    .line 395
    :catch_42
    move-exception v0

    .line 397
    const-string v0, "w"

    const-string v1, "fp"

    invoke-static {v7, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fH;

    move-result-object v0

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto :goto_e

    .line 404
    :catch_6a
    move-exception v0

    .line 406
    const-string v0, "w"

    const-string v1, "fp"

    invoke-static {v7, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fH;

    move-result-object v0

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ft;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/wizard/fH;->a(Ljava/lang/String;)V

    goto/16 :goto_e
.end method
