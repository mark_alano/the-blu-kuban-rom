.class public Lcom/google/googlenav/ui/wizard/gc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/gh;

.field private final b:Lcom/google/googlenav/ui/wizard/fV;

.field private final c:Lcom/google/googlenav/ui/wizard/gg;

.field private d:Lcom/google/googlenav/ui/wizard/fI;

.field private e:Lcom/google/googlenav/ui/wizard/fM;

.field private f:Z

.field private final g:Lcom/google/googlenav/ui/wizard/jv;

.field private h:Lcom/google/googlenav/aU;

.field private final i:Lcom/google/googlenav/ui/ak;

.field private j:Lcom/google/googlenav/ui/wizard/aL;

.field private final k:LaH/h;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;LaH/h;Lcom/google/googlenav/ui/wizard/gh;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    .line 141
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->g:Lcom/google/googlenav/ui/wizard/jv;

    .line 142
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    .line 143
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/gc;->a:Lcom/google/googlenav/ui/wizard/gh;

    .line 147
    new-instance v0, Lcom/google/googlenav/ui/wizard/ge;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ge;-><init>(Lcom/google/googlenav/ui/wizard/gc;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->b:Lcom/google/googlenav/ui/wizard/fV;

    .line 149
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    .line 153
    new-instance v0, Lcom/google/googlenav/ui/wizard/gg;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/gg;-><init>(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/ui/wizard/gd;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->c:Lcom/google/googlenav/ui/wizard/gg;

    .line 157
    new-instance v0, Lcom/google/googlenav/ui/wizard/fI;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/fI;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    .line 158
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->c:Lcom/google/googlenav/ui/wizard/gg;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fI;->a(Lcom/google/googlenav/ui/wizard/fK;)V

    .line 160
    invoke-virtual {p2}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    .line 161
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    .line 165
    new-instance v2, Lcom/google/googlenav/ui/wizard/aL;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/aL;-><init>(LaH/m;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gc;->j:Lcom/google/googlenav/ui/wizard/aL;

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/aU;)Lcom/google/googlenav/aU;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->h:Lcom/google/googlenav/aU;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/gh;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->a:Lcom/google/googlenav/ui/wizard/gh;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/aU;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->h:Lcom/google/googlenav/aU;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/ak;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method private c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 221
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->j:Lcom/google/googlenav/ui/wizard/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/gi;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/gi;-><init>(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/ui/wizard/gd;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/aL;->a(LaH/A;)V

    .line 223
    return-void
.end method

.method private d()LaH/h;
    .registers 6

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2f

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long v1, v1, v3

    if-gez v1, :cond_2f

    .line 257
    :goto_2e
    return-object v0

    :cond_2f
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/fM;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/jv;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->g:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/gc;)LaH/h;
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->d()LaH/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/fI;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/fV;
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->b:Lcom/google/googlenav/ui/wizard/fV;

    return-object v0
.end method

.method public a(LaH/h;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    .line 191
    if-nez p1, :cond_a

    .line 192
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->c()V

    .line 199
    :goto_9
    return-void

    .line 194
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/fI;->a(II)V

    .line 197
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v0, p1, v3}, Lcom/google/googlenav/ui/wizard/fI;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public a(Lcom/google/googlenav/ui/wizard/fM;)V
    .registers 2
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    .line 176
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    .line 231
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    if-nez v1, :cond_a

    .line 232
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->d()LaH/h;

    move-result-object v0

    .line 235
    :cond_a
    if-nez v0, :cond_14

    .line 236
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    const/16 v1, 0xbc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->b(I)V

    .line 243
    :goto_13
    return-void

    .line 238
    :cond_14
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/fI;->a(II)V

    .line 241
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/wizard/fI;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_13
.end method

.method public b()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 207
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->l()Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->n()Z

    move-result v1

    if-nez v1, :cond_12

    .line 216
    :cond_11
    :goto_11
    return v0

    .line 211
    :cond_12
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->n()Z

    move-result v1

    if-nez v1, :cond_11

    .line 212
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fM;->h()V

    .line 213
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fM;->m()V

    .line 214
    const/4 v0, 0x1

    goto :goto_11
.end method
