.class public Lcom/google/googlenav/ui/wizard/gk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaM/h;
.implements LaW/r;
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Lcom/google/googlenav/friend/bf;
.implements Lcom/google/googlenav/ui/view/c;
.implements Lcom/google/googlenav/ui/wizard/E;
.implements Lcom/google/googlenav/ui/wizard/dy;
.implements Lcom/google/googlenav/ui/wizard/gj;


# static fields
.field private static M:Ljava/util/List;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private A:LaN/H;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/Object;

.field private E:Lcom/google/googlenav/ui/wizard/gy;

.field private F:Z

.field private G:J

.field private H:Lcom/google/googlenav/bf;

.field private I:Z

.field private J:Lcom/google/googlenav/ui/view/android/S;

.field private K:Lcom/google/googlenav/ui/view/android/S;

.field private L:LaW/g;

.field private final N:Lcom/google/googlenav/actionbar/b;

.field private O:Lcom/google/googlenav/ui/wizard/dy;

.field protected a:Lcom/google/googlenav/a;

.field private i:I

.field private final j:Lcom/google/googlenav/J;

.field private final k:LaH/m;

.field private final l:Lcom/google/googlenav/ui/ak;

.field private final m:LaN/u;

.field private n:Lbf/am;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/googlenav/ai;

.field private t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private u:Ljava/util/List;

.field private v:Ljava/util/List;

.field private w:Lcom/google/googlenav/aV;

.field private x:Ljava/util/List;

.field private y:Z

.field private z:LaN/B;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 290
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 134
    iput v2, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    .line 209
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    .line 215
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    .line 234
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    .line 255
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    .line 918
    new-instance v0, Lcom/google/googlenav/ui/wizard/gw;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gw;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->O:Lcom/google/googlenav/ui/wizard/dy;

    .line 291
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    .line 292
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    .line 293
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    .line 294
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    .line 295
    new-instance v0, Lcom/google/googlenav/ui/wizard/gl;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gl;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->N:Lcom/google/googlenav/actionbar/b;

    .line 330
    invoke-static {}, Lcom/google/googlenav/aV;->a()Lcom/google/googlenav/aV;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->w:Lcom/google/googlenav/aV;

    .line 331
    return-void
.end method

.method public static C()V
    .registers 0

    .prologue
    .line 1615
    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->i()V

    .line 1616
    return-void
.end method

.method public static D()Ljava/lang/String;
    .registers 1

    .prologue
    .line 1782
    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static E()Ljava/lang/String;
    .registers 1

    .prologue
    .line 1790
    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    return-object v0
.end method

.method private F()Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 511
    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    if-eqz v2, :cond_7

    .line 545
    :cond_6
    :goto_6
    return v0

    .line 517
    :cond_7
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v3, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 518
    goto :goto_6

    .line 521
    :cond_15
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 525
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 526
    iget-wide v4, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_39

    iget-wide v4, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/32 v6, 0xdbba0

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    :cond_39
    move v0, v1

    .line 528
    goto :goto_6

    .line 535
    :cond_3b
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    if-eqz v2, :cond_43

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-nez v2, :cond_45

    :cond_43
    move v0, v1

    .line 536
    goto :goto_6

    .line 538
    :cond_45
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    .line 539
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    invoke-virtual {v3}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v4}, LaN/u;->f()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    if-ne v3, v4, :cond_71

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    invoke-virtual {v2, v3}, LaN/B;->a(LaN/B;)J

    move-result-wide v2

    const-wide/16 v4, 0x9c4

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    :cond_71
    move v0, v1

    .line 542
    goto :goto_6
.end method

.method private G()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 554
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 555
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    .line 556
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    .line 557
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    .line 558
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    .line 559
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 560
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    .line 561
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    .line 562
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    .line 563
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    .line 564
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    .line 565
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    .line 566
    return-void
.end method

.method private H()V
    .registers 2

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->p()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->o()Z

    move-result v0

    if-nez v0, :cond_d

    .line 592
    :cond_c
    :goto_c
    return-void

    .line 585
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_18

    .line 586
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->K()V

    .line 591
    :goto_14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    goto :goto_c

    .line 588
    :cond_18
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->y()V

    .line 589
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    goto :goto_14
.end method

.method private I()V
    .registers 4

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_10

    .line 604
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {v1, v2, v0}, LaW/g;->a(Ljava/lang/String;Z)V

    .line 606
    :cond_10
    return-void

    .line 604
    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private J()V
    .registers 5

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_5

    .line 621
    :goto_4
    return-void

    .line 617
    :cond_5
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    .line 619
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->W()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LaW/g;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private K()V
    .registers 14

    .prologue
    .line 630
    new-instance v0, LaW/g;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->W()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v7, :cond_38

    const/4 v7, 0x1

    :goto_21
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v8

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v11

    iget-object v12, p0, Lcom/google/googlenav/ui/wizard/gk;->w:Lcom/google/googlenav/aV;

    move-object v9, p0

    move-object v10, p0

    invoke-direct/range {v0 .. v12}, LaW/g;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZZLcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/bu;Lcom/google/googlenav/aV;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    .line 643
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, LaW/g;->show()V

    .line 644
    return-void

    .line 630
    :cond_38
    const/4 v7, 0x0

    goto :goto_21
.end method

.method private L()V
    .registers 4

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    .line 826
    :goto_5
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 827
    if-eqz v0, :cond_19

    .line 828
    new-instance v0, LaW/F;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bU()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LaW/F;-><init>(Lcom/google/googlenav/ui/wizard/gj;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    .line 831
    :cond_19
    return-void

    .line 825
    :cond_1a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private M()V
    .registers 4

    .prologue
    .line 834
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 851
    :goto_4
    return-void

    .line 837
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    .line 838
    new-instance v0, Lcom/google/googlenav/ui/wizard/gv;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gv;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    .line 849
    new-instance v1, Lcom/google/googlenav/f;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 850
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_4
.end method

.method private N()V
    .registers 2

    .prologue
    .line 983
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_c

    .line 984
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    .line 985
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    .line 987
    :cond_c
    return-void
.end method

.method private O()Z
    .registers 2

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_e

    .line 994
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    .line 995
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    .line 996
    const/4 v0, 0x1

    .line 998
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private P()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1327
    iget v1, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    if-eq v1, v0, :cond_11

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v1

    if-eqz v1, :cond_12

    :cond_11
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private Q()V
    .registers 2

    .prologue
    .line 1366
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1367
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->R()V

    .line 1371
    :goto_9
    return-void

    .line 1369
    :cond_a
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->S()V

    goto :goto_9
.end method

.method private R()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1377
    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    .line 1378
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1379
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->U()LaN/B;

    move-result-object v0

    .line 1380
    if-eqz v0, :cond_19

    .line 1383
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    .line 1384
    invoke-direct {p0, v0, v1, v1}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    .line 1387
    :cond_19
    return-void
.end method

.method private S()V
    .registers 4

    .prologue
    .line 1390
    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->a:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    .line 1391
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    .line 1392
    if-eqz v0, :cond_23

    .line 1393
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    .line 1394
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    .line 1397
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->I()V

    .line 1398
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    .line 1400
    :cond_23
    return-void
.end method

.method private static T()Ljava/util/List;
    .registers 3

    .prologue
    const/16 v2, 0xa

    .line 1486
    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    if-nez v0, :cond_38

    .line 1487
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1488
    const/4 v1, 0x2

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1490
    const/4 v1, 0x7

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1492
    const/4 v1, 0x6

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1494
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ag()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1495
    const/4 v1, 0x4

    invoke-static {v1, v2}, LaW/q;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1498
    :cond_36
    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    .line 1500
    :cond_38
    sget-object v0, Lcom/google/googlenav/ui/wizard/gk;->M:Ljava/util/List;

    return-object v0
.end method

.method private U()LaN/B;
    .registers 8

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v1

    .line 1563
    if-eqz v1, :cond_3a

    .line 1564
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 1565
    if-eqz v0, :cond_3a

    .line 1566
    invoke-virtual {v1}, LaH/h;->getTime()J

    move-result-wide v1

    .line 1567
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v3, :cond_35

    iget-wide v3, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_35

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    invoke-virtual {v0, v3}, LaN/B;->a(LaN/B;)J

    move-result-wide v3

    const-wide/16 v5, 0x9c4

    cmp-long v3, v3, v5

    if-lez v3, :cond_3a

    iget-wide v3, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    sub-long v3, v1, v3

    const-wide/32 v5, 0x493e0

    cmp-long v3, v3, v5

    if-lez v3, :cond_3a

    .line 1572
    :cond_35
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    .line 1573
    iput-wide v1, p0, Lcom/google/googlenav/ui/wizard/gk;->G:J

    .line 1578
    :goto_39
    return-object v0

    :cond_3a
    const/4 v0, 0x0

    goto :goto_39
.end method

.method private V()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1586
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    .line 1587
    :goto_5
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aT;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1586
    :cond_c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private W()Ljava/lang/String;
    .registers 5

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_14

    .line 1593
    :cond_12
    const/4 v0, 0x0

    .line 1595
    :goto_13
    return-object v0

    :cond_14
    const/16 v0, 0x3ba

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_13
.end method

.method private X()V
    .registers 2

    .prologue
    .line 1859
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1860
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->h()V

    .line 1862
    :cond_19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    .line 1863
    return-void
.end method

.method private Y()Lcom/google/googlenav/layer/m;
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2036
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    .line 2038
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2040
    const-string v1, "lmq:*"

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2041
    const-string v1, "*"

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2042
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2044
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2046
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2048
    const-string v2, "gr"

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2049
    const-string v2, "geo:coupons"

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2050
    const/16 v2, 0x12

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2052
    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2054
    return-object v1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;)Lbf/am;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/bg;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1076
    new-instance v0, Lcom/google/googlenav/bd;

    const-string v1, ""

    invoke-direct {v0, v5, v1, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 1079
    new-instance v1, Lcom/google/googlenav/bc;

    const/16 v2, 0x12f

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    aput-object p1, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v6, v2, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    .line 1083
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 1084
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086
    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x5f6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/bg;->k(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1096
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/bg;)V

    .line 1097
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/bg;)V

    .line 1098
    return-object v0
.end method

.method static a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)Lcom/google/googlenav/ui/wizard/gk;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 341
    new-instance v0, Lcom/google/googlenav/ui/wizard/gk;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/gk;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)V

    .line 343
    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x7

    .line 1697
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1698
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    .line 1701
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1702
    const/4 v0, 0x0

    :goto_13
    if-ge v0, v1, :cond_2c

    .line 1703
    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1704
    new-instance v4, LaW/S;

    const-string v5, "pl"

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v4, v0, v5, v6}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v3, v4}, LaW/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaW/S;)LaW/R;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1702
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1709
    :cond_2c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_65

    .line 1710
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    .line 1711
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->g()Ljava/util/List;

    move-result-object v0

    .line 1712
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_44
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    .line 1714
    new-instance v3, LaW/R;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    new-instance v4, LaW/S;

    const/4 v5, -0x1

    const-string v6, "pl"

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v0, v4}, LaW/R;-><init>(Ljava/lang/String;LaW/S;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_44

    .line 1719
    :cond_65
    return-object v2
.end method

.method private a(LaN/B;LaN/H;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1504
    new-instance v0, LaW/q;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/gk;->T()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, LaW/q;-><init>(Ljava/util/List;LaN/B;LaW/r;)V

    .line 1505
    if-eqz p2, :cond_1a

    .line 1506
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1, p2}, LaN/u;->a(LaN/H;)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->b(LaN/H;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaW/q;->b(II)V

    .line 1509
    :cond_1a
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 1511
    return-void
.end method

.method private a(LaN/B;LaN/H;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1432
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    if-eqz v0, :cond_a5

    .line 1435
    const-string v0, "n"

    .line 1439
    :goto_8
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    .line 1440
    iput-boolean v6, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    .line 1441
    const/16 v1, 0x57

    const-string v2, "stp"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "e="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1448
    new-instance v1, Lcom/google/googlenav/friend/bg;

    invoke-direct {v1}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v2

    invoke-virtual {v2}, LaM/f;->k()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/friend/bg;->d(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/friend/bg;->e(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    .line 1458
    if-eqz p2, :cond_90

    .line 1459
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->a(LaN/H;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->d(I)Lcom/google/googlenav/friend/bg;

    .line 1460
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, p2}, LaN/u;->b(LaN/H;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bg;->e(I)Lcom/google/googlenav/friend/bg;

    .line 1463
    :cond_90
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/H;)V

    .line 1466
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;)V

    .line 1468
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v1

    invoke-virtual {v2, v1}, Law/h;->c(Law/g;)V

    .line 1470
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1471
    return-void

    .line 1437
    :cond_a5
    const-string v0, "a"

    goto/16 :goto_8
.end method

.method private a(LaN/H;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1477
    if-eqz p1, :cond_16

    .line 1478
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->r()V

    .line 1479
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 1483
    :goto_15
    return-void

    .line 1481
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->l:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/googlenav/ui/ak;->a(ZZZ)V

    goto :goto_15
.end method

.method private a(Lcom/google/googlenav/aZ;)V
    .registers 8
    .parameter

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1755
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 1759
    :cond_8
    const-string v0, "Not available yet"

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    .line 1760
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    .line 1762
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    .line 1763
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    .line 1764
    const/4 v0, 0x0

    :goto_1d
    array-length v2, v1

    if-ge v0, v2, :cond_3f

    .line 1765
    aget-object v2, v1, v0

    .line 1766
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->an()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    .line 1767
    new-instance v3, LaW/S;

    const-string v4, "pl"

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/gk;->r:Ljava/lang/String;

    invoke-direct {v3, v0, v4, v5}, LaW/S;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 1770
    new-instance v4, LaW/R;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v2, v5, v3}, LaW/R;-><init>(Ljava/lang/String;Ljava/lang/String;ILaW/S;)V

    .line 1772
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1764
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 1774
    :cond_3f
    return-void
.end method

.method private a(Lcom/google/googlenav/bg;)V
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1128
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v3, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v3, :cond_71

    .line 1129
    invoke-virtual {p1, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    if-eqz v0, :cond_3d

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->V()Ljava/lang/String;

    move-result-object v0

    :goto_14
    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->d(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 1135
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1136
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v3

    .line 1137
    if-eqz v3, :cond_3f

    move v0, v1

    :goto_28
    move-object v1, v3

    .line 1144
    :goto_29
    if-eqz v0, :cond_3c

    .line 1145
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 1146
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    .line 1147
    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    .line 1152
    :cond_3c
    :goto_3c
    return-void

    .line 1129
    :cond_3d
    const/4 v0, 0x0

    goto :goto_14

    :cond_3f
    move v0, v2

    .line 1137
    goto :goto_28

    .line 1139
    :cond_41
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    .line 1140
    if-eqz v0, :cond_6f

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_6f

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xdbba0

    cmp-long v3, v3, v5

    if-gez v3, :cond_6f

    :goto_6b
    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_29

    :cond_6f
    move v1, v2

    goto :goto_6b

    .line 1149
    :cond_71
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->c:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_3c

    .line 1150
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    goto :goto_3c
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;LaN/B;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gk;->b(LaN/B;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ai;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/gz;)V
    .registers 9
    .parameter

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_a

    .line 687
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/wizard/gz;->a(Lcom/google/googlenav/ai;)V

    .line 708
    :goto_9
    return-void

    .line 689
    :cond_a
    new-instance v0, Lcom/google/googlenav/ui/wizard/gq;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/gq;-><init>(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ui/wizard/gz;)V

    .line 703
    new-instance v2, Lcom/google/googlenav/f;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 704
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 706
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto :goto_9
.end method

.method private a(Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1403
    sget-object v0, Lcom/google/googlenav/ui/wizard/gy;->c:Lcom/google/googlenav/ui/wizard/gy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    .line 1404
    invoke-virtual {p2}, LaN/H;->a()LaN/B;

    move-result-object v0

    .line 1405
    if-eqz v0, :cond_2b

    .line 1407
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->z:LaN/B;

    .line 1408
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->A:LaN/H;

    .line 1410
    if-eqz p3, :cond_2c

    invoke-virtual {p3}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2c

    invoke-virtual {p3}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_2c

    .line 1415
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    .line 1416
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/H;)V

    .line 1419
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/aZ;)V

    .line 1421
    invoke-direct {p0, v0, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;)V

    .line 1422
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    .line 1428
    :cond_2b
    :goto_2b
    return-void

    .line 1425
    :cond_2c
    invoke-direct {p0, v0, p2, p1}, Lcom/google/googlenav/ui/wizard/gk;->a(LaN/B;LaN/H;Ljava/lang/String;)V

    goto :goto_2b
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gk;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gk;)Lcom/google/googlenav/J;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gk;Lcom/google/googlenav/ai;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    return-object p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1105
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->d(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->f(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->g(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->a(LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1118
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/bg;)V

    .line 1119
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/bg;)V

    .line 1120
    return-object v0
.end method

.method private b(LaN/B;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1350
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    if-nez v0, :cond_6

    .line 1363
    :goto_5
    return-void

    .line 1354
    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    .line 1355
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 1356
    new-instance v1, Lcom/google/googlenav/bg;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    invoke-direct {v1, v2}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->d(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1359
    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    .line 1360
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1362
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_5
.end method

.method private b(Lcom/google/googlenav/bg;)V
    .registers 3
    .parameter

    .prologue
    .line 1155
    new-instance v0, Lcom/google/googlenav/ui/wizard/gx;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gx;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    .line 1183
    invoke-virtual {p1, v0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    .line 1184
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 1732
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 1733
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 1740
    :goto_8
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->b:Ljava/lang/String;

    .line 1741
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    .line 1742
    return-void

    .line 1735
    :cond_15
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    goto :goto_8
.end method

.method private b(Ljava/util/List;)V
    .registers 5
    .parameter

    .prologue
    .line 1972
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    .line 1973
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_11

    .line 1974
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->x:Ljava/util/List;

    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaW/g;->a(Ljava/util/List;Lcom/google/googlenav/bu;)V

    .line 1976
    :cond_11
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gk;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/ai;)V
    .registers 6
    .parameter

    .prologue
    .line 804
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 805
    if-eqz v0, :cond_b

    .line 806
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    .line 808
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    .line 809
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v2, v0}, LaN/u;->a(LaN/H;)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    invoke-virtual {v3, v0}, LaN/u;->b(LaN/H;)I

    move-result v3

    invoke-static {v1, v0, v2, v3}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 814
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 816
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Lcom/google/googlenav/aZ;)Lbf/x;

    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    .line 821
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->L()V

    .line 822
    return-void
.end method

.method private c(Lcom/google/googlenav/bg;)V
    .registers 11
    .parameter

    .prologue
    const/16 v8, 0x10

    const/4 v6, 0x0

    const/16 v5, 0x57

    .line 1248
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v7

    .line 1249
    if-eqz v7, :cond_6c

    .line 1251
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    invoke-virtual {v7}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1252
    const-string v2, "lkl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1257
    :goto_36
    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/bf;->f:LaN/H;

    if-eqz v0, :cond_74

    const/4 v0, 0x1

    .line 1258
    :goto_3f
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    if-eqz v1, :cond_47

    if-eqz v0, :cond_80

    .line 1261
    :cond_47
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    if-nez v1, :cond_76

    .line 1262
    const-string v0, "f"

    const-string v1, "v"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1272
    :cond_54
    :goto_54
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v1, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1274
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 1310
    :cond_6b
    :goto_6b
    return-void

    .line 1254
    :cond_6c
    const-string v0, "nlkl"

    const-string v1, ""

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_36

    :cond_74
    move v0, v6

    .line 1257
    goto :goto_3f

    .line 1265
    :cond_76
    if-eqz v0, :cond_54

    .line 1267
    const-string v0, "f"

    const-string v1, "h"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_54

    .line 1279
    :cond_80
    const-string v0, "f"

    const-string v1, "w"

    invoke-static {v5, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1288
    invoke-virtual {p1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    .line 1289
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x291

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 1292
    if-eqz v7, :cond_6b

    invoke-virtual {v7}, LaH/h;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 1293
    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/gm;

    invoke-direct {v2, p0, v7}, Lcom/google/googlenav/ui/wizard/gm;-><init>(Lcom/google/googlenav/ui/wizard/gk;LaH/h;)V

    invoke-direct {v0, v1, v2}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    .line 1307
    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    .line 1308
    invoke-virtual {v0}, Las/d;->g()V

    goto :goto_6b
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gk;)LaW/g;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    return-object v0
.end method

.method private d(Lcom/google/googlenav/bg;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x5

    .line 2025
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 2027
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/bc;

    const-string v3, ""

    const/4 v4, 0x0

    invoke-direct {v2, v5, v3, v4}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2029
    const-string v1, "*"

    invoke-virtual {p1, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    .line 2033
    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/gk;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->G()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/gk;)LaH/m;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/gk;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->R()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/gk;)LaN/u;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->m:LaN/u;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/gk;)Lcom/google/googlenav/bf;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/gk;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public A()V
    .registers 5

    .prologue
    .line 1524
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1556
    :goto_6
    return-void

    .line 1532
    :cond_7
    new-instance v0, Lcom/google/googlenav/ui/wizard/go;

    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bm;->h()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/go;-><init>(Lcom/google/googlenav/ui/wizard/gk;Ljava/util/List;Lcom/google/googlenav/friend/ao;Z)V

    .line 1555
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_6
.end method

.method public B()I
    .registers 2

    .prologue
    .line 1610
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    return v0
.end method

.method public C_()V
    .registers 3

    .prologue
    .line 2066
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    .line 2068
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v1, :cond_12

    .line 2069
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v1, v0}, LaW/g;->b(Lcom/google/googlenav/bu;)V

    .line 2070
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v1, v0}, LaW/g;->a(Lcom/google/googlenav/bu;)V

    .line 2075
    :cond_12
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Q()V

    .line 2076
    return-void
.end method

.method public D_()V
    .registers 1

    .prologue
    .line 2081
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 2091
    return-void
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 2062
    return-void
.end method

.method public N_()V
    .registers 1

    .prologue
    .line 2086
    return-void
.end method

.method public S_()Z
    .registers 2

    .prologue
    .line 1875
    const/4 v0, 0x1

    return v0
.end method

.method public T_()V
    .registers 1

    .prologue
    .line 1881
    return-void
.end method

.method public U_()V
    .registers 2

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/E;)V

    .line 677
    return-void
.end method

.method public V_()V
    .registers 3

    .prologue
    .line 742
    const/16 v0, 0x57

    const-string v1, "ph"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 747
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 749
    new-instance v0, Lcom/google/googlenav/ui/wizard/gs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gs;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    .line 756
    return-void
.end method

.method public W_()V
    .registers 4

    .prologue
    .line 780
    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    .line 781
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    .line 782
    new-instance v1, Lcom/google/googlenav/h;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-direct {v1, v2}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    .line 783
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    .line 784
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    .line 785
    return-void
.end method

.method public X_()V
    .registers 3

    .prologue
    .line 789
    const/16 v0, 0x57

    const-string v1, "p"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 793
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 795
    new-instance v0, Lcom/google/googlenav/ui/wizard/gu;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gu;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    .line 801
    return-void
.end method

.method public Y_()V
    .registers 7

    .prologue
    const/16 v5, 0xc7

    const/4 v4, 0x1

    .line 868
    const/16 v0, 0x57

    const-string v1, "d"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 870
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v2}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0x50d

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0x61b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->e(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 884
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 885
    return-void
.end method

.method public Z_()V
    .registers 4

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_18

    .line 890
    new-instance v0, LaW/F;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->s:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bU()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LaW/F;-><init>(Lcom/google/googlenav/ui/wizard/gj;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    .line 892
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->show()V

    .line 894
    :cond_18
    return-void
.end method

.method public a(LaN/B;)I
    .registers 3
    .parameter

    .prologue
    .line 1031
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x4

    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(LaN/B;)I

    move-result v0

    goto :goto_b
.end method

.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 1003
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 1004
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->h()V

    .line 1005
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    .line 1012
    :goto_d
    return v0

    .line 1007
    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_14

    .line 1009
    const/4 v0, 0x4

    goto :goto_d

    .line 1012
    :cond_14
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    goto :goto_d
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-nez v0, :cond_6

    .line 1019
    const/4 v0, 0x4

    .line 1022
    :goto_5
    return v0

    :cond_6
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gk;->g:I

    goto :goto_5
.end method

.method public a(ILaH/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1855
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->X()V

    .line 1856
    return-void
.end method

.method public a(J)V
    .registers 5
    .parameter

    .prologue
    .line 1796
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 1798
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    .line 1799
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    .line 1800
    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1821
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->E:Lcom/google/googlenav/ui/wizard/gy;

    sget-object v1, Lcom/google/googlenav/ui/wizard/gy;->b:Lcom/google/googlenav/ui/wizard/gy;

    if-ne v0, v1, :cond_f

    .line 1824
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 1825
    :try_start_a
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    if-eqz v0, :cond_10

    .line 1826
    monitor-exit v1

    .line 1851
    :cond_f
    :goto_f
    return-void

    .line 1828
    :cond_10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->C:Z

    .line 1829
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_23

    .line 1831
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/gp;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/gp;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_f

    .line 1829
    :catchall_23
    move-exception v0

    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    throw v0
.end method

.method public a(LaW/o;)V
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1980
    invoke-virtual {p1}, LaW/o;->c()Ljava/util/List;

    move-result-object v1

    .line 1981
    invoke-virtual {p1}, LaW/o;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1d

    const-string v0, ""

    .line 1982
    :goto_d
    if-eqz v1, :cond_15

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_22

    .line 1984
    :cond_15
    invoke-virtual {p1}, LaW/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V

    .line 2020
    :goto_1c
    return-void

    .line 1981
    :cond_1d
    invoke-virtual {p1}, LaW/o;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    .line 1988
    :cond_22
    invoke-virtual {p1}, LaW/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1991
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 1994
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LaW/o;->a(I)Z

    move-result v2

    if-eqz v2, :cond_7c

    .line 1997
    const/16 v2, 0x58

    const-string v3, "s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "t=l"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "ub=f"

    aput-object v6, v4, v5

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2006
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->d(Lcom/google/googlenav/bg;)V

    .line 2007
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Y()Lcom/google/googlenav/layer/m;

    move-result-object v3

    invoke-interface {v2, v1, v0, v3}, Lcom/google/googlenav/J;->a(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lbf/aJ;

    move-result-object v0

    .line 2013
    :goto_5f
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->f(Lbf/i;)V

    .line 2014
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/bk;->a(B)V

    .line 2016
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 2018
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    goto :goto_1c

    .line 2010
    :cond_7c
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/J;->a(Ljava/util/List;Lcom/google/googlenav/bf;)Lbf/bk;

    move-result-object v0

    goto :goto_5f
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1886
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    .line 1887
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->b()V

    .line 1888
    const/4 v0, 0x4

    invoke-static {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 1889
    const/4 v0, 0x3

    invoke-static {p3, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    .line 1890
    const/16 v0, 0xa

    invoke-static {p3, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    .line 1894
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_2b

    .line 1895
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->hide()V

    .line 1896
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->Z_()V

    .line 1900
    :cond_2b
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->M()V

    .line 1901
    return-void
.end method

.method public a(Lbf/am;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 347
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    .line 349
    const/16 v0, 0x57

    const-string v1, "ac"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 354
    const/4 v0, 0x4

    if-eq p2, v0, :cond_19

    .line 355
    iput p2, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    .line 363
    :goto_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->j()V

    .line 364
    return-void

    .line 359
    :cond_19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    goto :goto_15
.end method

.method public a(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 2122
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_a

    .line 2123
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 2125
    :cond_a
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 6
    .parameter

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 732
    const/16 v0, 0x57

    const-string v1, "rpc"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 736
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    const/4 v2, 0x0

    const-string v3, "plrp"

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    .line 738
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1649
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_70

    .line 1650
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gk;->t:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1651
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gk;->u:Ljava/util/List;

    .line 1652
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1653
    const/4 v1, 0x0

    .line 1654
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->a:Lcom/google/googlenav/a;

    invoke-static {v3}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/a;)Z

    move-result v3

    if-eqz v3, :cond_78

    .line 1655
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->a:Lcom/google/googlenav/a;

    invoke-virtual {v1}, Lcom/google/googlenav/a;->h()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 1657
    :goto_21
    if-eqz v4, :cond_41

    move v1, v2

    move-object v3, v0

    .line 1659
    :goto_25
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_42

    .line 1660
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1661
    const/4 v5, 0x2

    invoke-static {v0, v5}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    move-object v3, v0

    .line 1659
    :cond_3d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    :cond_41
    move-object v3, v0

    .line 1668
    :cond_42
    const/4 v0, 0x3

    invoke-static {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->p:Ljava/lang/String;

    .line 1669
    const/16 v0, 0xa

    invoke-static {v3, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->q:Ljava/lang/String;

    .line 1672
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1673
    invoke-direct {p0, p3}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    .line 1676
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    if-nez v0, :cond_69

    .line 1677
    const/4 v0, 0x4

    invoke-static {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    .line 1678
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->o:Ljava/lang/String;

    sput-object v0, Lcom/google/googlenav/ui/wizard/gk;->c:Ljava/lang/String;

    .line 1683
    :cond_69
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->M()V

    .line 1690
    :goto_6c
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    .line 1691
    return-void

    .line 1686
    :cond_70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->v:Ljava/util/List;

    goto :goto_6c

    :cond_78
    move-object v4, v1

    goto :goto_21
.end method

.method public a(Lcom/google/googlenav/ui/wizard/F;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x57

    .line 1055
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 1056
    iget v0, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_18

    .line 1057
    const-string v0, "u"

    iget-object v1, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1063
    :goto_12
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;)V

    .line 1065
    :cond_17
    return-void

    .line 1060
    :cond_18
    const-string v0, "c"

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_12
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1191
    invoke-virtual {p0, p1, v0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V

    .line 1192
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaW/R;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1229
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;Ljava/lang/String;LaW/R;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1235
    if-eqz p3, :cond_11

    .line 1236
    const/16 v1, 0x57

    const-string v2, "t"

    invoke-virtual {p3}, LaW/R;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1241
    :cond_11
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/bg;)V

    .line 1242
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 8
    .parameter

    .prologue
    .line 1945
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "t="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1947
    const/4 v0, 0x0

    .line 1949
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1950
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_16
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1951
    new-instance v5, LaW/o;

    invoke-direct {v5, v0, p0}, LaW/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/gj;)V

    .line 1952
    invoke-virtual {v5}, LaW/o;->e()LaW/w;

    move-result-object v0

    .line 1953
    if-eqz v0, :cond_52

    .line 1954
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1955
    if-eqz v1, :cond_37

    .line 1956
    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1958
    :cond_37
    const/4 v0, 0x1

    .line 1959
    invoke-virtual {v5}, LaW/o;->d()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_3f
    move v1, v0

    .line 1961
    goto :goto_16

    .line 1962
    :cond_41
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/util/List;)V

    .line 1965
    if-eqz v1, :cond_51

    .line 1966
    const/16 v0, 0x6f

    const-string v1, "pi"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1969
    :cond_51
    return-void

    :cond_52
    move v0, v1

    goto :goto_3f
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 654
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    .line 655
    sparse-switch p1, :sswitch_data_38

    .line 671
    const/4 v0, 0x0

    :goto_7
    return v0

    .line 657
    :sswitch_8
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    .line 660
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 661
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_7

    .line 665
    :sswitch_25
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto :goto_7

    .line 668
    :sswitch_33
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->U_()V

    goto :goto_7

    .line 655
    nop

    :sswitch_data_38
    .sparse-switch
        0x70e -> :sswitch_33
        0x9cd -> :sswitch_25
        0x9d4 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 5
    .parameter

    .prologue
    .line 1811
    check-cast p1, Lcom/google/googlenav/ui/view/a;

    .line 1812
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gk;->a(IILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public aa_()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 901
    const/16 v0, 0xc

    .line 904
    const/16 v1, 0x57

    const-string v2, "sloc"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 908
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/16 v4, 0xc7

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->e(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->O:Lcom/google/googlenav/ui/wizard/dy;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 916
    return-void
.end method

.method public ab_()Z
    .registers 3

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public ac_()V
    .registers 4

    .prologue
    .line 1199
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    .line 1200
    if-eqz v0, :cond_12

    .line 1201
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->l()Lcom/google/googlenav/L;

    move-result-object v1

    iget-object v0, v0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/L;->a(Ljava/lang/String;Lcom/google/googlenav/android/T;)V

    .line 1203
    :cond_12
    return-void
.end method

.method public ad_()V
    .registers 5

    .prologue
    .line 1210
    invoke-static {}, Lcom/google/googlenav/bm;->e()Lcom/google/googlenav/bu;

    move-result-object v0

    .line 1211
    if-eqz v0, :cond_24

    .line 1213
    iget-object v1, v0, Lcom/google/googlenav/bu;->b:Ljava/lang/String;

    .line 1214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/googlenav/bu;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1215
    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 1216
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->c(Lcom/google/googlenav/bg;)V

    .line 1218
    :cond_24
    return-void
.end method

.method public ae_()Z
    .registers 2

    .prologue
    .line 2133
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->o()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 422
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    if-nez v0, :cond_f

    .line 423
    const-string v0, "PLACES_NULL_LAYER"

    invoke-static {v0}, Lbm/m;->a(Ljava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 491
    :goto_e
    return-void

    .line 429
    :cond_f
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 430
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    .line 433
    :cond_1c
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->F()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 434
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->G()V

    .line 435
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_32

    .line 436
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->P()Z

    move-result v1

    invoke-virtual {v0, v3, v1, v2}, LaW/g;->a(Ljava/lang/String;ZZ)V

    .line 440
    :cond_32
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    .line 441
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    .line 442
    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    .line 443
    iput-boolean v2, p0, Lcom/google/googlenav/ui/wizard/gk;->I:Z

    .line 450
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_58

    .line 452
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 456
    :cond_58
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->F:Z

    if-nez v0, :cond_74

    .line 460
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->B:Ljava/lang/String;

    if-nez v0, :cond_63

    .line 461
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->Q()V

    .line 464
    :cond_63
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_74

    .line 475
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->h()V

    .line 478
    :cond_74
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->A()V

    .line 479
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->H()V

    .line 481
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_83

    .line 482
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->J:Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->show()V

    .line 485
    :cond_83
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->J()V

    .line 487
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->h()LaR/n;

    move-result-object v0

    sget-object v1, LaR/O;->h:LaR/O;

    invoke-interface {v0, v1}, LaR/n;->a(LaR/O;)V

    .line 489
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    sget-object v1, LaR/O;->h:LaR/O;

    invoke-interface {v0, v1}, LaR/n;->a(LaR/O;)V

    goto/16 :goto_e
.end method

.method public b(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_9

    .line 504
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0, p1}, LaW/g;->a(Lcom/google/googlenav/ai;)V

    .line 506
    :cond_9
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/wizard/F;)V
    .registers 3
    .parameter

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V

    .line 682
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1333
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1335
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 1336
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    .line 1337
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 1806
    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 1037
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    if-eqz v0, :cond_9

    .line 1038
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->H()V

    .line 1039
    const/4 v0, 0x1

    .line 1041
    :goto_8
    return v0

    :cond_9
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v0

    goto :goto_8
.end method

.method public c()V
    .registers 2

    .prologue
    .line 391
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 394
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 395
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    .line 398
    :cond_10
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    if-eqz v0, :cond_19

    .line 399
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->k:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->b(LaH/A;)V

    .line 403
    :cond_19
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 404
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_28

    .line 405
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, LaW/g;->hide()V

    .line 406
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    .line 409
    :cond_28
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->X()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 410
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    .line 417
    :cond_41
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->j:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    .line 418
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->n:Lbf/am;

    .line 496
    iget v1, p0, Lcom/google/googlenav/ui/wizard/gk;->i:I

    .line 498
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 499
    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/am;I)V

    .line 500
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 1935
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/util/List;)V

    .line 1936
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 2097
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gk;->y:Z

    .line 2098
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->y()V

    .line 2099
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 953
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    move-result v0

    .line 954
    if-eqz v0, :cond_7

    .line 974
    :goto_6
    return-void

    .line 959
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->K:Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_f

    .line 960
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->N()V

    goto :goto_6

    .line 966
    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_33

    .line 968
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 970
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->a()V

    .line 973
    :cond_33
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    goto :goto_6
.end method

.method public i()V
    .registers 3

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    .line 650
    return-void
.end method

.method public k()I
    .registers 3

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1e

    .line 381
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->j()V

    .line 383
    const/16 v0, 0xa

    .line 386
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public l()V
    .registers 3

    .prologue
    .line 712
    const/16 v0, 0x57

    const-string v1, "r"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 716
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 718
    new-instance v0, Lcom/google/googlenav/ui/wizard/gr;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gr;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    .line 725
    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 571
    return-void
.end method

.method public q()V
    .registers 2

    .prologue
    .line 761
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->O()Z

    .line 763
    new-instance v0, Lcom/google/googlenav/ui/wizard/gt;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/gt;-><init>(Lcom/google/googlenav/ui/wizard/gk;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gz;)V

    .line 776
    return-void
.end method

.method public w()Lcom/google/googlenav/actionbar/b;
    .registers 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->N:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public y()V
    .registers 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    if-eqz v0, :cond_17

    .line 596
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    .line 597
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gk;->L:LaW/g;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, LaW/g;->a(Ljava/util/List;)V

    .line 599
    :cond_17
    return-void
.end method

.method public z()V
    .registers 2

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gk;->H:Lcom/google/googlenav/bf;

    if-eqz v0, :cond_7

    .line 1049
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gk;->X()V

    .line 1051
    :cond_7
    return-void
.end method
