.class public Lcom/google/googlenav/ui/wizard/hZ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/g;


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/jv;

.field private final b:Lcom/google/googlenav/ui/wizard/hH;

.field private c:LaZ/a;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/hH;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 652
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    .line 653
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    .line 654
    return-void
.end method


# virtual methods
.method public B_()V
    .registers 4

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x457

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x458

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    .line 685
    return-void
.end method

.method public C_()V
    .registers 3

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    if-eqz v0, :cond_10

    .line 703
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    .line 704
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    .line 706
    :cond_10
    return-void
.end method

.method public D_()V
    .registers 3

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    if-eqz v0, :cond_12

    .line 714
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    .line 715
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    .line 716
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    .line 718
    :cond_12
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 732
    return-void
.end method

.method public M_()V
    .registers 3

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    if-eqz v0, :cond_18

    .line 693
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/d;->a(Z)V

    .line 695
    :cond_18
    return-void
.end method

.method public N_()V
    .registers 1

    .prologue
    .line 725
    return-void
.end method

.method public a(LaZ/a;)V
    .registers 4
    .parameter

    .prologue
    .line 740
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    .line 741
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 742
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hZ;->b:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hZ;->c:LaZ/a;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    .line 746
    :goto_13
    return-void

    .line 744
    :cond_14
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hZ;->B_()V

    goto :goto_13
.end method

.method public t()Z
    .registers 2

    .prologue
    .line 676
    const/4 v0, 0x0

    return v0
.end method
