.class public Lcom/google/googlenav/ui/wizard/iC;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/googlenav/ui/wizard/iG;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 55
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iC;->a:Landroid/content/Context;

    .line 56
    return-void
.end method

.method private a(Lcom/google/googlenav/android/T;)Lcom/google/googlenav/ui/view/dialog/o;
    .registers 3
    .parameter

    .prologue
    .line 84
    new-instance v0, Lcom/google/googlenav/ui/wizard/iD;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/iD;-><init>(Lcom/google/googlenav/ui/wizard/iC;Lcom/google/googlenav/android/T;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iC;)Lcom/google/googlenav/ui/wizard/iG;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/iC;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ui/wizard/iG;)Lcom/google/googlenav/android/T;
    .registers 3
    .parameter

    .prologue
    .line 122
    new-instance v0, Lcom/google/googlenav/ui/wizard/iE;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/iE;-><init>(Lcom/google/googlenav/ui/wizard/iC;Lcom/google/googlenav/ui/wizard/iG;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/wizard/iG;)V
    .registers 4
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    .line 67
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iC;->b(Lcom/google/googlenav/ui/wizard/iG;)Lcom/google/googlenav/android/T;

    move-result-object v0

    .line 68
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/android/T;)Lcom/google/googlenav/ui/view/dialog/o;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/k;

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/k;-><init>(Lcom/google/googlenav/ui/view/dialog/o;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iC;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 72
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iC;->j()V

    .line 73
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    if-eqz v0, :cond_9

    .line 166
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/iG;->a()V

    .line 168
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iC;->b:Lcom/google/googlenav/ui/wizard/iG;

    .line 169
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 170
    return-void
.end method
