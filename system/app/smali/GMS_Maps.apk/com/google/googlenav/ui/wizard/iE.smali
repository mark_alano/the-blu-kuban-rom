.class public Lcom/google/googlenav/ui/wizard/ie;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/ih;


# direct methods
.method static synthetic a(Lcom/google/googlenav/ui/wizard/ie;)Lcom/google/googlenav/ui/wizard/ih;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 184
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_b

    .line 185
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->h()V

    .line 187
    :cond_b
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    return v0
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 195
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    return v0
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 141
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ie;->g:I

    .line 142
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->e()Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 145
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/if;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/if;-><init>(Lcom/google/googlenav/ui/wizard/ie;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 151
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 152
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    .line 157
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 158
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    .line 163
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->a()V

    .line 164
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    .line 165
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->j()V

    .line 166
    return-void
.end method

.method protected e()Lcom/google/googlenav/ui/view/android/S;
    .registers 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/googlenav/ui/wizard/ii;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/ii;-><init>(Lcom/google/googlenav/ui/wizard/ie;Lcom/google/googlenav/ui/e;)V

    return-object v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    if-eqz v0, :cond_b

    .line 174
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ie;->a:Lcom/google/googlenav/ui/wizard/ih;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/ih;->a:Lcom/google/googlenav/ui/wizard/ig;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ig;->a()V

    .line 176
    :cond_b
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ie;->a()V

    .line 177
    return-void
.end method
