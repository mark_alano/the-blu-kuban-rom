.class public Lcom/google/googlenav/ui/wizard/iL;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/view/dialog/bw;

.field private b:Lcom/google/googlenav/ui/wizard/iS;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private i:Ljava/lang/String;

.field private j:LaN/B;

.field private k:LaC/a;

.field private l:Lcom/google/googlenav/ui/wizard/iT;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaC/a;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iL;->k:LaC/a;

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;)Lcom/google/googlenav/ui/wizard/iS;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/lang/String;JJ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/lang/String;JJ)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/iL;Ljava/util/List;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;JJ)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v8, 0xa

    const/4 v4, 0x3

    const/4 v6, 0x0

    const/16 v3, 0x15

    const/4 v0, 0x1

    .line 220
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    if-eqz v1, :cond_94

    .line 221
    invoke-static {}, Lcom/google/googlenav/friend/ad;->p()V

    .line 225
    :goto_e
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 227
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/bw;->hide()V

    .line 230
    :cond_1f
    new-instance v7, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 231
    const/4 v1, 0x4

    invoke-virtual {v7, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 232
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 233
    invoke-virtual {v1, v0, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 234
    const/4 v2, 0x2

    invoke-virtual {v1, v2, p4, p5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 235
    invoke-virtual {v7, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 236
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    if-ne v1, v2, :cond_60

    .line 237
    const/16 v1, 0x10

    invoke-virtual {v7, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 238
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 239
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 240
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 245
    :cond_60
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    if-ne v1, v2, :cond_67

    move v0, v6

    .line 247
    :cond_67
    div-long v1, p2, v8

    long-to-int v1, v1

    div-long v2, p4, v8

    long-to-int v2, v2

    invoke-static {v0, p1, v1, v2}, LaR/a;->a(ILjava/lang/String;II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 250
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x159

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 252
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    .line 254
    new-instance v1, Lcom/google/googlenav/ui/wizard/iQ;

    invoke-direct {v1, p0, v7}, Lcom/google/googlenav/ui/wizard/iQ;-><init>(Lcom/google/googlenav/ui/wizard/iL;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sget-object v2, LaR/O;->d:LaR/O;

    invoke-interface {v0, v8, v1, v2}, LaR/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    .line 274
    return-void

    .line 223
    :cond_94
    invoke-static {}, Lcom/google/googlenav/friend/ad;->q()V

    goto/16 :goto_e
.end method

.method private a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 180
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 181
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 182
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v3

    if-ltz v3, :cond_8

    .line 183
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 188
    :cond_1e
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bw;

    new-instance v2, Lcom/google/googlenav/ui/wizard/iP;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/iP;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-direct {v0, v1, v2, p0}, Lcom/google/googlenav/ui/view/dialog/bw;-><init>(Ljava/util/List;Lcom/google/googlenav/ui/view/dialog/by;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    .line 198
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 199
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->show()V

    .line 200
    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 397
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 398
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->h()V

    .line 399
    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    .line 401
    :goto_d
    return v0

    :cond_e
    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    goto :goto_d
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 406
    iget v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    return v0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    .line 338
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 339
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    .line 340
    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    .line 341
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->j()V

    .line 342
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 114
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    if-eqz v0, :cond_b

    .line 115
    const-string v0, "home_speedbump_ack"

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/friend/aU;->a(Ljava/lang/String;ILcom/google/googlenav/friend/bc;)V

    .line 117
    :cond_b
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 171
    :goto_13
    return-void

    .line 120
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 125
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 126
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 127
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 128
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 133
    :goto_33
    int-to-long v2, v1

    int-to-long v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/iL;->a(Ljava/lang/String;JJ)V

    goto :goto_13

    .line 130
    :cond_3b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    invoke-virtual {v0}, LaN/B;->d()I

    move-result v1

    .line 131
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    invoke-virtual {v0}, LaN/B;->f()I

    move-result v0

    goto :goto_33

    .line 135
    :cond_48
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 137
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->k:LaC/a;

    const/4 v1, 0x5

    new-instance v2, Lcom/google/googlenav/ui/wizard/iM;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/iM;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-virtual {v0, p1, v1, v2}, LaC/a;->a(Ljava/lang/String;ILaC/c;)V

    goto :goto_13
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 411
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    .line 412
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 346
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/iL;->g:I

    .line 347
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bY;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bY;-><init>(Lcom/google/googlenav/ui/wizard/iL;Lcom/google/googlenav/ui/wizard/iT;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 348
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 349
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->f()V

    .line 350
    return-void
.end method

.method protected c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 354
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 355
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 357
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->dismiss()V

    .line 359
    :cond_15
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 360
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    .line 361
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    .line 362
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    .line 363
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    .line 364
    return-void
.end method

.method public d()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 368
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    .line 369
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 370
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    .line 371
    iget-boolean v3, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    .line 372
    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    .line 373
    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 374
    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/iL;->l:Lcom/google/googlenav/ui/wizard/iT;

    .line 375
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/googlenav/ui/wizard/iL;->m:Z

    .line 380
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->a()V

    .line 381
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/iL;->a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V

    .line 382
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    if-eqz v0, :cond_9

    .line 207
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->b:Lcom/google/googlenav/ui/wizard/iS;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/iS;->a()V

    .line 209
    :cond_9
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->a()V

    .line 210
    return-void
.end method

.method public f()V
    .registers 4

    .prologue
    const/4 v2, 0x4

    .line 284
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 285
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v1

    .line 286
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/iL;->i:Ljava/lang/String;

    .line 287
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bY;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bY;->a(Ljava/lang/String;)V

    .line 322
    :cond_19
    :goto_19
    return-void

    .line 289
    :cond_1a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 290
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_4e

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 292
    :goto_36
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->j:LaN/B;

    .line 293
    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;I)V

    .line 294
    new-instance v0, Lcom/google/googlenav/ui/wizard/iR;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/iR;-><init>(Lcom/google/googlenav/ui/wizard/iL;)V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    .line 319
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_19

    .line 291
    :cond_4e
    const/4 v0, 0x0

    goto :goto_36
.end method

.method public h()V
    .registers 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 388
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->a:Lcom/google/googlenav/ui/view/dialog/bw;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bw;->dismiss()V

    .line 389
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iL;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 393
    :goto_16
    return-void

    .line 391
    :cond_17
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iL;->e()V

    goto :goto_16
.end method
