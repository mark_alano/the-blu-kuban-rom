.class public Lcom/google/googlenav/ui/wizard/jq;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/googlenav/ui/wizard/js;

.field private final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/js;Ljava/lang/String;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    const v0, 0x7f0f001b

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    .line 48
    new-instance v0, Lcom/google/googlenav/ui/wizard/jr;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/jr;-><init>(Lcom/google/googlenav/ui/wizard/jq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->d:Landroid/view/View$OnClickListener;

    .line 60
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jq;->a:Ljava/lang/String;

    .line 61
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/jq;->b:Ljava/util/List;

    .line 62
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/jq;->c:Lcom/google/googlenav/ui/wizard/js;

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/jq;)Lcom/google/googlenav/ui/wizard/js;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->c:Lcom/google/googlenav/ui/wizard/js;

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .registers 3

    .prologue
    .line 102
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 103
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jq;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 107
    :cond_10
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->a:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 108
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->h:Landroid/view/View;

    const v1, 0x7f10033f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    if-eqz v0, :cond_2c

    .line 110
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jq;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :cond_2c
    return-void
.end method

.method protected O_()V
    .registers 3

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jq;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 118
    return-void
.end method

.method public P_()Z
    .registers 2

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method protected c()Landroid/view/View;
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jq;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401a0

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 68
    const v0, 0x7f100419

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 70
    const/4 v1, 0x0

    move v4, v1

    :goto_17
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jq;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_67

    .line 71
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jq;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0401a1

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 73
    const v2, 0x7f10041a

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 74
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 75
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jq;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jq;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    add-int/lit8 v2, v4, 0x1

    .line 78
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jq;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_62

    .line 79
    const v3, 0x7f10041b

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 80
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 82
    :cond_62
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v4, v2

    .line 83
    goto :goto_17

    .line 84
    :cond_67
    return-object v5
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->onBackPressed()V

    .line 91
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->c:Lcom/google/googlenav/ui/wizard/js;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/js;->a()V

    .line 92
    return-void
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jq;->a:Ljava/lang/String;

    return-object v0
.end method
