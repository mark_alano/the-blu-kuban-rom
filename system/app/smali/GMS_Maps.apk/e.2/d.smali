.class public LE/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LE/e;


# instance fields
.field protected a:[S

.field protected b:I

.field c:I

.field d:Ljava/nio/ShortBuffer;

.field e:I

.field protected f:I

.field protected g:Li/j;

.field private h:Z


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/d;-><init>(IZ)V

    .line 76
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, LE/d;->f:I

    .line 85
    iput-boolean p2, p0, LE/d;->h:Z

    .line 86
    iput p1, p0, LE/d;->b:I

    .line 87
    invoke-direct {p0}, LE/d;->f()V

    .line 88
    return-void
.end method

.method private a([SII)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 242
    iget-object v0, p0, LE/d;->g:Li/j;

    if-nez v0, :cond_16

    .line 243
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    iget v0, p0, LE/d;->e:I

    add-int/2addr v0, p3

    iput v0, p0, LE/d;->e:I

    .line 256
    :cond_10
    iget v0, p0, LE/d;->c:I

    add-int/2addr v0, p3

    iput v0, p0, LE/d;->c:I

    .line 257
    return-void

    .line 246
    :cond_16
    add-int v0, p2, p3

    .line 247
    :goto_18
    if-ge p2, v0, :cond_10

    .line 248
    sub-int v1, v0, p2

    iget v2, p0, LE/d;->e:I

    rsub-int v2, v2, 0x800

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 250
    iget-object v2, p0, LE/d;->a:[S

    iget v3, p0, LE/d;->e:I

    invoke-static {p1, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    add-int/2addr p2, v1

    .line 252
    iget v2, p0, LE/d;->e:I

    add-int/2addr v1, v2

    iput v1, p0, LE/d;->e:I

    .line 253
    invoke-direct {p0}, LE/d;->e()V

    goto :goto_18
.end method

.method private e()V
    .registers 3

    .prologue
    .line 99
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_9

    .line 100
    invoke-virtual {p0}, LE/d;->a()V

    .line 102
    :cond_9
    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 270
    iput v2, p0, LE/d;->e:I

    .line 271
    iget-object v0, p0, LE/d;->a:[S

    if-nez v0, :cond_2a

    .line 272
    iget v0, p0, LE/d;->b:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_11

    iget-boolean v0, p0, LE/d;->h:Z

    if-eqz v0, :cond_1d

    .line 273
    :cond_11
    iget v0, p0, LE/d;->b:I

    new-array v0, v0, [S

    iput-object v0, p0, LE/d;->a:[S

    .line 283
    :cond_17
    :goto_17
    iput v2, p0, LE/d;->c:I

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    .line 285
    return-void

    .line 275
    :cond_1d
    new-instance v0, Li/j;

    iget v1, p0, LE/d;->b:I

    invoke-direct {v0, v1}, Li/j;-><init>(I)V

    iput-object v0, p0, LE/d;->g:Li/j;

    .line 276
    invoke-virtual {p0}, LE/d;->a()V

    goto :goto_17

    .line 278
    :cond_2a
    iget-object v0, p0, LE/d;->g:Li/j;

    if-eqz v0, :cond_17

    .line 279
    iget-object v0, p0, LE/d;->g:Li/j;

    invoke-virtual {v0}, Li/j;->a()V

    .line 280
    invoke-virtual {p0}, LE/d;->a()V

    goto :goto_17
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 91
    iget-object v0, p0, LE/d;->g:Li/j;

    if-eqz v0, :cond_19

    .line 92
    iget-object v0, p0, LE/d;->g:Li/j;

    iget v1, p0, LE/d;->e:I

    invoke-virtual {v0, v1}, Li/j;->b(I)V

    .line 93
    iget-object v0, p0, LE/d;->g:Li/j;

    iget-object v0, v0, Li/j;->c:Ljava/lang/Object;

    check-cast v0, [S

    iput-object v0, p0, LE/d;->a:[S

    .line 94
    iget-object v0, p0, LE/d;->g:Li/j;

    iget v0, v0, Li/j;->d:I

    iput v0, p0, LE/d;->e:I

    .line 96
    :cond_19
    return-void
.end method

.method public a(III)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p1

    aput-short v2, v0, v1

    .line 168
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p2

    aput-short v2, v0, v1

    .line 169
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p3

    aput-short v2, v0, v1

    .line 170
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, LE/d;->c:I

    .line 173
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_30

    .line 174
    invoke-virtual {p0}, LE/d;->a()V

    .line 176
    :cond_30
    return-void
.end method

.method public a(IIII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p1

    aput-short v2, v0, v1

    .line 186
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p2

    aput-short v2, v0, v1

    .line 187
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p3

    aput-short v2, v0, v1

    .line 188
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p3

    aput-short v2, v0, v1

    .line 189
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p2

    aput-short v2, v0, v1

    .line 190
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p4

    aput-short v2, v0, v1

    .line 191
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x6

    iput v0, p0, LE/d;->c:I

    .line 194
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_51

    .line 195
    invoke-virtual {p0}, LE/d;->a()V

    .line 197
    :cond_51
    return-void
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 289
    invoke-virtual {p0, p1}, LE/d;->b(LD/a;)V

    .line 290
    invoke-direct {p0}, LE/d;->f()V

    .line 291
    return-void
.end method

.method public a(LD/a;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 350
    iget-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    if-nez v0, :cond_7

    .line 351
    invoke-virtual {p0, p1}, LE/d;->d(LD/a;)V

    .line 353
    :cond_7
    iget-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->limit()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LE/d;->f:I

    .line 354
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, LE/d;->c:I

    const/16 v2, 0x1403

    iget-object v3, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    invoke-interface {v0, p2, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 355
    return-void
.end method

.method public a(LE/d;II)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/16 v4, 0x800

    const/4 v1, 0x0

    .line 208
    iget-object v0, p1, LE/d;->g:Li/j;

    if-eqz v0, :cond_c

    iget v0, p1, LE/d;->c:I

    if-ge v0, v4, :cond_39

    :cond_c
    move v0, v2

    .line 210
    :goto_d
    iget-object v3, p0, LE/d;->g:Li/j;

    if-eqz v3, :cond_16

    iget v3, p0, LE/d;->e:I

    add-int/2addr v3, p3

    if-gt v3, v4, :cond_3b

    .line 214
    :cond_16
    :goto_16
    if-eqz v0, :cond_3d

    if-eqz v2, :cond_3d

    .line 215
    iget-object v0, p1, LE/d;->a:[S

    iget-object v1, p0, LE/d;->a:[S

    iget v2, p0, LE/d;->e:I

    invoke-static {v0, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 216
    iget v0, p0, LE/d;->e:I

    add-int/2addr v0, p3

    iput v0, p0, LE/d;->e:I

    .line 217
    iget v0, p0, LE/d;->c:I

    add-int/2addr v0, p3

    iput v0, p0, LE/d;->c:I

    .line 218
    iget v0, p0, LE/d;->e:I

    if-lt v0, v4, :cond_38

    iget-object v0, p0, LE/d;->g:Li/j;

    if-eqz v0, :cond_38

    .line 219
    invoke-virtual {p0}, LE/d;->a()V

    .line 238
    :cond_38
    :goto_38
    return-void

    :cond_39
    move v0, v1

    .line 208
    goto :goto_d

    :cond_3b
    move v2, v1

    .line 210
    goto :goto_16

    .line 224
    :cond_3d
    iget-object v0, p1, LE/d;->g:Li/j;

    if-nez v0, :cond_47

    .line 225
    iget-object v0, p1, LE/d;->a:[S

    invoke-direct {p0, v0, p2, p3}, LE/d;->a([SII)V

    goto :goto_38

    .line 227
    :cond_47
    invoke-direct {p1}, LE/d;->e()V

    .line 228
    shr-int/lit8 v2, p2, 0xb

    .line 229
    and-int/lit16 v0, p2, 0x7ff

    move v3, v2

    move v2, v0

    .line 230
    :goto_50
    if-lez p3, :cond_38

    .line 231
    rsub-int v0, v2, 0x800

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 232
    iget-object v0, p1, LE/d;->g:Li/j;

    invoke-virtual {v0, v3}, Li/j;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [S

    invoke-direct {p0, v0, v2, v4}, LE/d;->a([SII)V

    .line 233
    add-int/lit8 v0, v3, 0x1

    .line 235
    sub-int/2addr p3, v4

    move v2, v1

    move v3, v0

    .line 236
    goto :goto_50
.end method

.method public a(S)V
    .registers 5
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p1, v0, v1

    .line 109
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/d;->c:I

    .line 112
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_19

    .line 113
    invoke-virtual {p0}, LE/d;->a()V

    .line 115
    :cond_19
    return-void
.end method

.method public a(SS)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p1, v0, v1

    .line 137
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p2, v0, v1

    .line 138
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, LE/d;->c:I

    .line 141
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_23

    .line 142
    invoke-virtual {p0}, LE/d;->a()V

    .line 144
    :cond_23
    return-void
.end method

.method public a(SSS)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p1, v0, v1

    .line 151
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p2, v0, v1

    .line 152
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    aput-short p3, v0, v1

    .line 153
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, LE/d;->c:I

    .line 156
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_2d

    .line 157
    invoke-virtual {p0}, LE/d;->a()V

    .line 159
    :cond_2d
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 265
    iget v0, p0, LE/d;->c:I

    return v0
.end method

.method public b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 316
    iget v0, p0, LE/d;->b:I

    if-le p1, v0, :cond_3e

    .line 317
    iget v0, p0, LE/d;->b:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 319
    iget-object v0, p0, LE/d;->g:Li/j;

    if-nez v0, :cond_5e

    .line 320
    const/16 v0, 0x800

    if-lt v1, v0, :cond_19

    iget-boolean v0, p0, LE/d;->h:Z

    if-eqz v0, :cond_3f

    .line 321
    :cond_19
    iget-boolean v0, p0, LE/d;->h:Z

    if-eqz v0, :cond_31

    .line 322
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_29

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 323
    :cond_29
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_31
    new-array v0, v1, [S

    .line 329
    iget-object v2, p0, LE/d;->a:[S

    iget v3, p0, LE/d;->e:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 330
    iput-object v0, p0, LE/d;->a:[S

    .line 340
    :goto_3c
    iput v1, p0, LE/d;->b:I

    .line 342
    :cond_3e
    return-void

    .line 332
    :cond_3f
    new-instance v0, Li/j;

    invoke-direct {v0, v1}, Li/j;-><init>(I)V

    iput-object v0, p0, LE/d;->g:Li/j;

    .line 333
    iget-object v0, p0, LE/d;->g:Li/j;

    iget-object v2, p0, LE/d;->a:[S

    iget v3, p0, LE/d;->e:I

    invoke-virtual {v0, v2, v3}, Li/j;->a(Ljava/lang/Object;I)V

    .line 334
    iget-object v0, p0, LE/d;->g:Li/j;

    iget-object v0, v0, Li/j;->c:Ljava/lang/Object;

    check-cast v0, [S

    iput-object v0, p0, LE/d;->a:[S

    .line 335
    iget-object v0, p0, LE/d;->g:Li/j;

    iget v0, v0, Li/j;->d:I

    iput v0, p0, LE/d;->e:I

    goto :goto_3c

    .line 338
    :cond_5e
    iget-object v0, p0, LE/d;->g:Li/j;

    invoke-virtual {v0, v1}, Li/j;->c(I)V

    goto :goto_3c
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 298
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 376
    iget v0, p0, LE/d;->f:I

    return v0
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 305
    invoke-virtual {p0, p1}, LE/d;->b(LD/a;)V

    .line 306
    iget-object v0, p0, LE/d;->g:Li/j;

    if-eqz v0, :cond_f

    .line 307
    iget-object v0, p0, LE/d;->g:Li/j;

    invoke-virtual {v0}, Li/j;->c()V

    .line 308
    iput-object v1, p0, LE/d;->g:Li/j;

    .line 310
    :cond_f
    iput-object v1, p0, LE/d;->a:[S

    .line 311
    return-void
.end method

.method public d()I
    .registers 3

    .prologue
    .line 384
    const/16 v0, 0x20

    .line 385
    iget-object v1, p0, LE/d;->g:Li/j;

    if-eqz v1, :cond_1d

    .line 387
    iget-object v1, p0, LE/d;->g:Li/j;

    invoke-virtual {v1}, Li/j;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 391
    :cond_f
    :goto_f
    iget-object v1, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_1c

    .line 392
    iget-object v1, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 394
    :cond_1c
    return v0

    .line 388
    :cond_1d
    iget-object v1, p0, LE/d;->a:[S

    if-eqz v1, :cond_f

    .line 389
    iget-object v1, p0, LE/d;->a:[S

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method public d(I)V
    .registers 5
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, LE/d;->a:[S

    iget v1, p0, LE/d;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/d;->e:I

    int-to-short v2, p1

    aput-short v2, v0, v1

    .line 124
    iget v0, p0, LE/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/d;->c:I

    .line 127
    iget v0, p0, LE/d;->e:I

    const/16 v1, 0x800

    if-lt v0, v1, :cond_1a

    .line 128
    invoke-virtual {p0}, LE/d;->a()V

    .line 130
    :cond_1a
    return-void
.end method

.method protected d(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 359
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v0

    iget v1, p0, LE/d;->c:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 360
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 361
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    .line 362
    iget-object v0, p0, LE/d;->g:Li/j;

    if-nez v0, :cond_30

    .line 363
    iget-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    iget-object v1, p0, LE/d;->a:[S

    iget v2, p0, LE/d;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    .line 370
    :goto_28
    iget-object v0, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 371
    iput-object v4, p0, LE/d;->a:[S

    .line 372
    return-void

    .line 365
    :cond_30
    invoke-virtual {p0}, LE/d;->a()V

    .line 366
    iget-object v0, p0, LE/d;->g:Li/j;

    iget-object v1, p0, LE/d;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Li/j;->a(Ljava/nio/ShortBuffer;)V

    .line 367
    iget-object v0, p0, LE/d;->g:Li/j;

    invoke-virtual {v0}, Li/j;->c()V

    .line 368
    iput-object v4, p0, LE/d;->g:Li/j;

    goto :goto_28
.end method
