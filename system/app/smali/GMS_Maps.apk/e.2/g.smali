.class public LE/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:I

.field private final c:Z

.field private final d:Z

.field private e:I

.field private final f:[I

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-array v0, v1, [I

    iput-object v0, p0, LE/g;->f:[I

    .line 70
    iput-boolean v2, p0, LE/g;->g:Z

    .line 82
    iput p1, p0, LE/g;->b:I

    .line 83
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_33

    move v0, v1

    :goto_12
    iput-boolean v0, p0, LE/g;->c:Z

    .line 84
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_35

    :goto_18
    iput-boolean v1, p0, LE/g;->d:Z

    .line 85
    const/16 v0, 0xc

    iput v0, p0, LE/g;->e:I

    .line 86
    iget-boolean v0, p0, LE/g;->d:Z

    if-eqz v0, :cond_28

    .line 87
    iget v0, p0, LE/g;->e:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, LE/g;->e:I

    .line 89
    :cond_28
    iget-boolean v0, p0, LE/g;->c:Z

    if-eqz v0, :cond_32

    .line 90
    iget v0, p0, LE/g;->e:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, LE/g;->e:I

    .line 92
    :cond_32
    return-void

    :cond_33
    move v0, v2

    .line 83
    goto :goto_12

    :cond_35
    move v1, v2

    .line 84
    goto :goto_18
.end method

.method private e(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 172
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 173
    const/4 v1, 0x1

    iget-object v2, p0, LE/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    .line 174
    const v1, 0x8892

    iget-object v2, p0, LE/g;->f:[I

    aget v2, v2, v3

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 175
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 107
    iget v0, p0, LE/g;->h:I

    return v0
.end method

.method public a(LD/a;)V
    .registers 9
    .parameter

    .prologue
    const v6, 0x8892

    const/16 v5, 0x1406

    const/4 v4, 0x0

    .line 114
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 115
    iget-object v1, p0, LE/g;->f:[I

    aget v1, v1, v4

    if-nez v1, :cond_52

    .line 116
    invoke-direct {p0, p1}, LE/g;->e(LD/a;)V

    .line 123
    :goto_15
    iget-boolean v1, p0, LE/g;->g:Z

    if-eqz v1, :cond_2e

    .line 124
    iget-object v1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 125
    iget-object v1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v6, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 127
    iput-boolean v4, p0, LE/g;->g:Z

    .line 137
    :cond_2e
    const/4 v1, 0x3

    iget v2, p0, LE/g;->e:I

    invoke-interface {v0, v1, v5, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 139
    const/16 v1, 0xc

    .line 141
    iget-boolean v2, p0, LE/g;->d:Z

    if-eqz v2, :cond_42

    .line 147
    const/4 v2, 0x4

    iget v3, p0, LE/g;->e:I

    invoke-interface {v0, v2, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    .line 149
    const/16 v1, 0x1c

    .line 154
    :cond_42
    iget-boolean v2, p0, LE/g;->c:Z

    if-eqz v2, :cond_4e

    .line 160
    const/4 v2, 0x2

    iget v3, p0, LE/g;->e:I

    invoke-interface {v0, v2, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 162
    add-int/lit8 v1, v1, 0x8

    .line 167
    :cond_4e
    invoke-interface {v0, v6, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 168
    return-void

    .line 118
    :cond_52
    iget-object v1, p0, LE/g;->f:[I

    aget v1, v1, v4

    invoke-interface {v0, v6, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_15
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, LE/g;->g:Z

    .line 100
    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LE/g;->e:I

    div-int/2addr v0, v1

    iput v0, p0, LE/g;->h:I

    .line 101
    return-void
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 179
    invoke-virtual {p0, p1}, LE/g;->d(LD/a;)V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    .line 181
    return-void
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 188
    invoke-virtual {p0, p1}, LE/g;->d(LD/a;)V

    .line 189
    return-void
.end method

.method public d(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 201
    iget-object v0, p0, LE/g;->f:[I

    aget v0, v0, v3

    if-eqz v0, :cond_22

    .line 205
    if-eqz p1, :cond_15

    .line 206
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 207
    const/4 v1, 0x1

    iget-object v2, p0, LE/g;->f:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    .line 209
    :cond_15
    iget-object v0, p0, LE/g;->f:[I

    aput v3, v0, v3

    .line 210
    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_22

    .line 211
    iget-object v0, p0, LE/g;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 214
    :cond_22
    return-void
.end method
