.class public LE/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LE/k;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/Buffer;

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field i:I

.field protected j:Li/h;

.field private k:Z


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    .line 127
    const/16 v0, 0x140c

    iput v0, p0, LE/i;->f:I

    .line 128
    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    .line 129
    const/4 v0, 0x1

    iput v0, p0, LE/i;->h:I

    .line 130
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/i;-><init>(IZ)V

    .line 106
    return-void
.end method

.method protected constructor <init>(IIIZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    .line 145
    iput-boolean p4, p0, LE/i;->k:Z

    .line 146
    iput p1, p0, LE/i;->c:I

    .line 147
    sparse-switch p2, :sswitch_data_28

    .line 159
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "glNativeType must be one of GL_FIXED, GL_SHORT or GL_BYTE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :sswitch_15
    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    .line 162
    :goto_18
    iput p2, p0, LE/i;->f:I

    .line 163
    iput p3, p0, LE/i;->h:I

    .line 164
    invoke-direct {p0}, LE/i;->d()V

    .line 165
    return-void

    .line 152
    :sswitch_20
    const/4 v0, 0x2

    iput v0, p0, LE/i;->g:I

    goto :goto_18

    .line 156
    :sswitch_24
    const/4 v0, 0x1

    iput v0, p0, LE/i;->g:I

    goto :goto_18

    .line 147
    :sswitch_data_28
    .sparse-switch
        0x1400 -> :sswitch_24
        0x1401 -> :sswitch_24
        0x1402 -> :sswitch_20
        0x140c -> :sswitch_15
    .end sparse-switch
.end method

.method public constructor <init>(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    .line 115
    iput-boolean p2, p0, LE/i;->k:Z

    .line 116
    iput p1, p0, LE/i;->c:I

    .line 117
    const/16 v0, 0x140c

    iput v0, p0, LE/i;->f:I

    .line 118
    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    .line 119
    const/4 v0, 0x1

    iput v0, p0, LE/i;->h:I

    .line 120
    invoke-direct {p0}, LE/i;->d()V

    .line 121
    return-void
.end method

.method private d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 256
    iput v2, p0, LE/i;->i:I

    .line 257
    iget-object v0, p0, LE/i;->b:[I

    if-nez v0, :cond_28

    .line 258
    iget v0, p0, LE/i;->c:I

    mul-int/lit8 v0, v0, 0x2

    .line 259
    const/16 v1, 0x400

    if-lt v0, v1, :cond_13

    iget-boolean v1, p0, LE/i;->k:Z

    if-eqz v1, :cond_1d

    .line 260
    :cond_13
    new-array v0, v0, [I

    iput-object v0, p0, LE/i;->b:[I

    .line 269
    :cond_17
    :goto_17
    iput v2, p0, LE/i;->d:I

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    .line 271
    return-void

    .line 262
    :cond_1d
    new-instance v1, Li/h;

    invoke-direct {v1, v0}, Li/h;-><init>(I)V

    iput-object v1, p0, LE/i;->j:Li/h;

    .line 263
    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_17

    .line 265
    :cond_28
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_17

    .line 266
    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->a()V

    .line 267
    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_17
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 168
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_19

    .line 169
    iget-object v0, p0, LE/i;->j:Li/h;

    iget v1, p0, LE/i;->i:I

    invoke-virtual {v0, v1}, Li/h;->b(I)V

    .line 170
    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/i;->b:[I

    .line 171
    iget-object v0, p0, LE/i;->j:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/i;->i:I

    .line 173
    :cond_19
    return-void
.end method

.method public a(FF)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x4780

    .line 194
    iget v0, p0, LE/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/i;->d:I

    .line 195
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    mul-float v2, p1, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    .line 196
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    mul-float v2, p2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    .line 197
    iget v0, p0, LE/i;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_31

    .line 198
    invoke-virtual {p0}, LE/i;->a()V

    .line 200
    :cond_31
    return-void
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 180
    iget v0, p0, LE/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/i;->d:I

    .line 181
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    aput p1, v0, v1

    .line 182
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    aput p2, v0, v1

    .line 183
    iget v0, p0, LE/i;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_23

    .line 184
    invoke-virtual {p0}, LE/i;->a()V

    .line 186
    :cond_23
    return-void
.end method

.method public a(III)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_23

    .line 228
    iget-object v1, p0, LE/i;->b:[I

    iget v2, p0, LE/i;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LE/i;->i:I

    aput p1, v1, v2

    .line 229
    iget-object v1, p0, LE/i;->b:[I

    iget v2, p0, LE/i;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LE/i;->i:I

    aput p2, v1, v2

    .line 230
    iget v1, p0, LE/i;->i:I

    const/16 v2, 0x400

    if-lt v1, v2, :cond_20

    .line 231
    invoke-virtual {p0}, LE/i;->a()V

    .line 227
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 234
    :cond_23
    iget v0, p0, LE/i;->d:I

    add-int/2addr v0, p3

    iput v0, p0, LE/i;->d:I

    .line 235
    return-void
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 275
    invoke-virtual {p0, p1}, LE/i;->b(LD/a;)V

    .line 276
    invoke-direct {p0}, LE/i;->d()V

    .line 277
    return-void
.end method

.method public a(LD/a;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_7

    .line 359
    invoke-virtual {p0, p1}, LE/i;->e(LD/a;)V

    .line 361
    :cond_7
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    mul-int/lit8 v1, p2, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    .line 362
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    sub-int/2addr v0, p2

    iget v1, p0, LE/i;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, LE/i;->a:I

    .line 368
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, p0, LE/i;->f:I

    const/4 v3, 0x0

    iget-object v4, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 370
    return-void
.end method

.method protected a(Ljava/nio/ByteBuffer;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 377
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_11

    .line 378
    iget-object v1, p0, LE/i;->b:[I

    aget v1, v1, v0

    iget v2, p0, LE/i;->h:I

    div-int/2addr v1, v2

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 380
    :cond_11
    return-void
.end method

.method protected a(Ljava/nio/ShortBuffer;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 387
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_11

    .line 388
    iget-object v1, p0, LE/i;->b:[I

    aget v1, v1, v0

    iget v2, p0, LE/i;->h:I

    div-int/2addr v1, v2

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 390
    :cond_11
    return-void
.end method

.method public a([I)V
    .registers 4
    .parameter

    .prologue
    .line 242
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LE/i;->a([III)V

    .line 243
    return-void
.end method

.method public a([III)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 207
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_b

    iget v0, p0, LE/i;->i:I

    add-int/2addr v0, p3

    const/16 v1, 0x400

    if-ge v0, v1, :cond_1f

    .line 208
    :cond_b
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 209
    iget v0, p0, LE/i;->i:I

    add-int/2addr v0, p3

    iput v0, p0, LE/i;->i:I

    .line 220
    :cond_17
    iget v0, p0, LE/i;->d:I

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    iput v0, p0, LE/i;->d:I

    .line 221
    return-void

    .line 211
    :cond_1f
    add-int v0, p2, p3

    .line 212
    :goto_21
    if-ge p2, v0, :cond_17

    .line 213
    sub-int v1, v0, p2

    iget v2, p0, LE/i;->i:I

    rsub-int v2, v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 214
    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-static {p1, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    add-int/2addr p2, v1

    .line 216
    iget v2, p0, LE/i;->i:I

    add-int/2addr v1, v2

    iput v1, p0, LE/i;->i:I

    .line 217
    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_21
.end method

.method public b()I
    .registers 2

    .prologue
    .line 442
    iget v0, p0, LE/i;->a:I

    return v0
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 284
    return-void
.end method

.method public c()I
    .registers 4

    .prologue
    .line 449
    const/16 v0, 0x2c

    .line 450
    iget-object v1, p0, LE/i;->j:Li/h;

    if-eqz v1, :cond_1e

    .line 452
    iget-object v1, p0, LE/i;->j:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 456
    :cond_f
    :goto_f
    iget-object v1, p0, LE/i;->e:Ljava/nio/Buffer;

    if-eqz v1, :cond_1d

    .line 457
    iget-object v1, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    iget v2, p0, LE/i;->g:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 459
    :cond_1d
    return v0

    .line 453
    :cond_1e
    iget-object v1, p0, LE/i;->b:[I

    if-eqz v1, :cond_f

    .line 454
    iget-object v1, p0, LE/i;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method public c(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 305
    iget v0, p0, LE/i;->c:I

    if-le p1, v0, :cond_40

    .line 306
    iget v0, p0, LE/i;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 307
    mul-int/lit8 v0, v1, 0x2

    .line 308
    iget-object v2, p0, LE/i;->j:Li/h;

    if-nez v2, :cond_60

    .line 309
    const/16 v2, 0x400

    if-lt v0, v2, :cond_1b

    iget-boolean v2, p0, LE/i;->k:Z

    if-eqz v2, :cond_41

    .line 310
    :cond_1b
    iget-boolean v2, p0, LE/i;->k:Z

    if-eqz v2, :cond_33

    .line 311
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_2b

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_33

    .line 312
    :cond_2b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_33
    new-array v0, v0, [I

    .line 318
    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 319
    iput-object v0, p0, LE/i;->b:[I

    .line 329
    :goto_3e
    iput v1, p0, LE/i;->c:I

    .line 331
    :cond_40
    return-void

    .line 321
    :cond_41
    new-instance v2, Li/h;

    invoke-direct {v2, v0}, Li/h;-><init>(I)V

    iput-object v2, p0, LE/i;->j:Li/h;

    .line 322
    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-virtual {v0, v2, v3}, Li/h;->a(Ljava/lang/Object;I)V

    .line 323
    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/i;->b:[I

    .line 324
    iget-object v0, p0, LE/i;->j:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/i;->i:I

    goto :goto_3e

    .line 327
    :cond_60
    iget-object v2, p0, LE/i;->j:Li/h;

    invoke-virtual {v2, v0}, Li/h;->c(I)V

    goto :goto_3e
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-virtual {p0, p1}, LE/i;->b(LD/a;)V

    .line 292
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_f

    .line 293
    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 294
    iput-object v1, p0, LE/i;->j:Li/h;

    .line 296
    :cond_f
    iput-object v1, p0, LE/i;->b:[I

    .line 297
    return-void
.end method

.method public d(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 338
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_8

    .line 339
    invoke-virtual {p0, p1}, LE/i;->e(LD/a;)V

    .line 341
    :cond_8
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    .line 342
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    iget v1, p0, LE/i;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, LE/i;->a:I

    .line 348
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, p0, LE/i;->f:I

    iget-object v3, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v1, v2, v4, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 350
    return-void
.end method

.method protected e(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 402
    iget v0, p0, LE/i;->d:I

    mul-int/lit8 v1, v0, 0x2

    .line 403
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v0

    iget v2, p0, LE/i;->g:I

    mul-int/2addr v2, v1

    invoke-virtual {v0, v2}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 404
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 407
    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1402

    if-ne v2, v3, :cond_4b

    .line 408
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    .line 409
    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_3c

    .line 410
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-virtual {p0, v0, v1}, LE/i;->a(Ljava/nio/ShortBuffer;I)V

    .line 433
    :goto_2e
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_39

    .line 434
    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 435
    iput-object v4, p0, LE/i;->j:Li/h;

    .line 437
    :cond_39
    iput-object v4, p0, LE/i;->b:[I

    .line 438
    return-void

    .line 412
    :cond_3c
    invoke-virtual {p0}, LE/i;->a()V

    .line 413
    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    iget v2, p0, LE/i;->h:I

    invoke-virtual {v1, v0, v2}, Li/h;->a(Ljava/nio/ShortBuffer;I)V

    goto :goto_2e

    .line 415
    :cond_4b
    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1400

    if-eq v2, v3, :cond_57

    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1401

    if-ne v2, v3, :cond_74

    .line 416
    :cond_57
    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    .line 417
    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_65

    .line 418
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0, v1}, LE/i;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_2e

    .line 420
    :cond_65
    invoke-virtual {p0}, LE/i;->a()V

    .line 421
    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    iget v2, p0, LE/i;->h:I

    invoke-virtual {v1, v0, v2}, Li/h;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_2e

    .line 424
    :cond_74
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    .line 425
    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_89

    .line 426
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/i;->b:[I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    goto :goto_2e

    .line 428
    :cond_89
    invoke-virtual {p0}, LE/i;->a()V

    .line 429
    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Li/h;->a(Ljava/nio/IntBuffer;)V

    goto :goto_2e
.end method

.method public g()I
    .registers 2

    .prologue
    .line 251
    iget v0, p0, LE/i;->d:I

    return v0
.end method
