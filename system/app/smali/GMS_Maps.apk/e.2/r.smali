.class public LE/r;
.super LE/o;
.source "SourceFile"


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, LE/o;-><init>(I)V

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/r;->h:[I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/r;->i:J

    .line 51
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, LE/o;-><init>(IZ)V

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/r;->h:[I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/r;->i:J

    .line 55
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-super {p0, p1}, LE/o;->a(LD/a;)V

    .line 60
    iget-object v0, p0, LE/r;->h:[I

    if-eqz v0, :cond_c

    .line 61
    iget-object v0, p0, LE/r;->h:[I

    aput v1, v0, v1

    .line 63
    :cond_c
    return-void
.end method

.method public b(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 75
    iget-object v0, p0, LE/r;->h:[I

    if-eqz v0, :cond_27

    iget-object v0, p0, LE/r;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_27

    .line 79
    iget-wide v0, p0, LE/r;->i:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 80
    if-ne v0, p1, :cond_21

    if-eqz v0, :cond_21

    .line 81
    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 82
    const/4 v1, 0x1

    iget-object v2, p0, LE/r;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    .line 84
    :cond_21
    iget-object v0, p0, LE/r;->h:[I

    aput v3, v0, v3

    .line 85
    iput v3, p0, LE/r;->a:I

    .line 87
    :cond_27
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/r;->i:J

    .line 88
    return-void
.end method

.method public d()I
    .registers 3

    .prologue
    .line 160
    const/16 v0, 0x38

    .line 161
    iget-object v1, p0, LE/r;->g:Li/h;

    if-eqz v1, :cond_10

    .line 163
    iget-object v1, p0, LE/r;->g:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 167
    :cond_f
    :goto_f
    return v0

    .line 164
    :cond_10
    iget-object v1, p0, LE/r;->b:[I

    if-eqz v1, :cond_f

    .line 165
    iget-object v1, p0, LE/r;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method public d(LD/a;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x8892

    const/4 v4, 0x0

    .line 120
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/r;->i:J

    .line 121
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_14

    .line 122
    invoke-super {p0, p1}, LE/o;->d(LD/a;)V

    .line 152
    :cond_13
    :goto_13
    return-void

    .line 126
    :cond_14
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 127
    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    if-nez v1, :cond_53

    .line 128
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    if-nez v1, :cond_27

    .line 129
    invoke-virtual {p0, p1}, LE/r;->e(LD/a;)V

    .line 131
    :cond_27
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_13

    .line 135
    const/4 v1, 0x1

    iget-object v2, p0, LE/r;->h:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    .line 136
    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 137
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    iput v1, p0, LE/r;->a:I

    .line 138
    iget v1, p0, LE/r;->a:I

    iget-object v2, p0, LE/r;->e:Ljava/nio/IntBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 139
    const/4 v1, 0x0

    iput-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    .line 142
    :cond_53
    iget-object v1, p0, LE/r;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 149
    const/4 v1, 0x3

    const/16 v2, 0x140c

    invoke-interface {v0, v1, v2, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 151
    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_13
.end method

.method protected e(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 93
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 94
    iget v0, p0, LE/r;->d:I

    mul-int/lit8 v0, v0, 0x3

    .line 95
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v1

    invoke-virtual {v1}, LE/n;->c()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    .line 96
    iget-object v1, p0, LE/r;->g:Li/h;

    if-nez v1, :cond_3d

    .line 97
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/r;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    .line 102
    :goto_21
    iget-object v1, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/IntBuffer;->limit(I)Ljava/nio/Buffer;

    .line 103
    iget-object v0, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 106
    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_3c

    .line 107
    iget-object v0, p0, LE/r;->g:Li/h;

    if-eqz v0, :cond_3a

    .line 108
    iget-object v0, p0, LE/r;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 109
    iput-object v4, p0, LE/r;->g:Li/h;

    .line 111
    :cond_3a
    iput-object v4, p0, LE/r;->b:[I

    .line 116
    :cond_3c
    :goto_3c
    return-void

    .line 99
    :cond_3d
    invoke-virtual {p0}, LE/r;->b()V

    .line 100
    iget-object v1, p0, LE/r;->g:Li/h;

    iget-object v2, p0, LE/r;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v2}, Li/h;->a(Ljava/nio/IntBuffer;)V

    goto :goto_21

    .line 114
    :cond_48
    invoke-super {p0, p1}, LE/o;->e(LD/a;)V

    goto :goto_3c
.end method
