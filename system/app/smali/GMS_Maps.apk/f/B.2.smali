.class LF/B;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:F

.field final b:F

.field final c:I

.field final d:[I

.field private final e:I


# direct methods
.method public constructor <init>(FLo/aj;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput p1, p0, LF/B;->a:F

    .line 181
    invoke-virtual {p2, p3}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->c()F

    move-result v1

    iput v1, p0, LF/B;->b:F

    .line 182
    invoke-virtual {p2, p3}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->b()I

    move-result v1

    iput v1, p0, LF/B;->c:I

    .line 184
    invoke-virtual {p2}, Lo/aj;->d()Z

    move-result v1

    if-eqz v1, :cond_65

    .line 185
    invoke-virtual {p2}, Lo/aj;->b()I

    move-result v1

    if-le v1, p3, :cond_3c

    .line 186
    invoke-virtual {p2, p3}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lo/ai;->d()[I

    move-result-object v1

    .line 188
    array-length v2, v1

    if-nez v2, :cond_3a

    :goto_31
    iput-object v0, p0, LF/B;->d:[I

    .line 199
    :goto_33
    invoke-direct {p0}, LF/B;->a()I

    move-result v0

    iput v0, p0, LF/B;->e:I

    .line 200
    return-void

    :cond_3a
    move-object v0, v1

    .line 188
    goto :goto_31

    .line 191
    :cond_3c
    const-string v1, "GLLineGroup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid stroke index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  available strokes : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lo/aj;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iput-object v0, p0, LF/B;->d:[I

    goto :goto_33

    .line 196
    :cond_65
    iput-object v0, p0, LF/B;->d:[I

    goto :goto_33
.end method

.method private a()I
    .registers 3

    .prologue
    .line 249
    iget v0, p0, LF/B;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 250
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/B;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/B;->c:I

    add-int/2addr v0, v1

    .line 252
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, LF/B;->d:[I

    if-eqz v0, :cond_22

    iget-object v0, p0, LF/B;->d:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    :goto_20
    add-int/2addr v0, v1

    .line 253
    return v0

    .line 252
    :cond_22
    const/4 v0, 0x0

    goto :goto_20
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218
    if-ne p0, p1, :cond_5

    .line 240
    :cond_4
    :goto_4
    return v0

    .line 221
    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    .line 222
    goto :goto_4

    .line 225
    :cond_13
    check-cast p1, LF/B;

    .line 227
    iget v2, p0, LF/B;->c:I

    iget v3, p1, LF/B;->c:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 228
    goto :goto_4

    .line 230
    :cond_1d
    iget v2, p1, LF/B;->a:F

    iget v3, p0, LF/B;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_29

    move v0, v1

    .line 231
    goto :goto_4

    .line 233
    :cond_29
    iget v2, p1, LF/B;->b:F

    iget v3, p0, LF/B;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_35

    move v0, v1

    .line 234
    goto :goto_4

    .line 236
    :cond_35
    iget-object v2, p0, LF/B;->d:[I

    iget-object v3, p1, LF/B;->d:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 237
    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 245
    iget v0, p0, LF/B;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "c:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LF/B;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " w:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LF/B;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LF/B;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " d:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LF/B;->d:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
