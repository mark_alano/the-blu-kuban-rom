.class LF/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:F

.field private final e:I


# direct methods
.method public constructor <init>(LF/A;)V
    .registers 4
    .parameter

    .prologue
    .line 1157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->a:Ljava/util/List;

    .line 1134
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->b:Ljava/util/List;

    .line 1158
    const/4 v0, 0x0

    iput v0, p0, LF/C;->d:F

    .line 1159
    const/4 v0, 0x0

    iput v0, p0, LF/C;->c:I

    .line 1160
    iget-object v0, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {p1}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1161
    iget-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {p1}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1162
    invoke-direct {p0}, LF/C;->a()I

    move-result v0

    iput v0, p0, LF/C;->e:I

    .line 1163
    return-void
.end method

.method public constructor <init>(Ljava/util/List;FIZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->a:Ljava/util/List;

    .line 1134
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/C;->b:Ljava/util/List;

    .line 1141
    iput p2, p0, LF/C;->d:F

    .line 1142
    iput p3, p0, LF/C;->c:I

    .line 1143
    if-eqz p4, :cond_35

    .line 1145
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    .line 1146
    iget-object v1, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1147
    iget-object v1, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1154
    :cond_2e
    invoke-direct {p0}, LF/C;->a()I

    move-result v0

    iput v0, p0, LF/C;->e:I

    .line 1155
    return-void

    .line 1149
    :cond_35
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_39
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    .line 1150
    iget-object v2, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1151
    iget-object v2, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_39
.end method

.method private a()I
    .registers 3

    .prologue
    .line 1198
    iget-object v0, p0, LF/C;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 1199
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LF/C;->b:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1200
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/C;->c:I

    add-int/2addr v0, v1

    .line 1201
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LF/C;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 1202
    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1167
    if-ne p0, p1, :cond_5

    .line 1189
    :cond_4
    :goto_4
    return v0

    .line 1170
    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    .line 1171
    goto :goto_4

    .line 1174
    :cond_13
    check-cast p1, LF/C;

    .line 1176
    iget v2, p1, LF/C;->d:F

    iget v3, p0, LF/C;->d:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_21

    move v0, v1

    .line 1177
    goto :goto_4

    .line 1179
    :cond_21
    iget v2, p0, LF/C;->c:I

    iget v3, p1, LF/C;->c:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 1180
    goto :goto_4

    .line 1182
    :cond_29
    iget-object v2, p0, LF/C;->a:Ljava/util/List;

    iget-object v3, p1, LF/C;->a:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    move v0, v1

    .line 1183
    goto :goto_4

    .line 1185
    :cond_35
    iget-object v2, p0, LF/C;->b:Ljava/util/List;

    iget-object v3, p1, LF/C;->b:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1186
    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 1194
    iget v0, p0, LF/C;->e:I

    return v0
.end method
