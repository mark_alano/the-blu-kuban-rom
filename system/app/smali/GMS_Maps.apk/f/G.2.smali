.class public LF/G;
.super LF/i;
.source "SourceFile"


# static fields
.field private static b:I

.field private static c:I

.field private static d:F

.field private static e:F


# instance fields
.field private final f:LE/o;

.field private final g:Lx/c;

.field private final h:I

.field private final i:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/high16 v1, 0x3f80

    .line 57
    const/16 v0, 0x4000

    sput v0, LF/G;->b:I

    .line 66
    const/4 v0, 0x1

    sput v0, LF/G;->c:I

    .line 67
    sput v1, LF/G;->d:F

    .line 68
    sput v1, LF/G;->e:F

    return-void
.end method

.method private constructor <init>(IILjava/util/Set;LD/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 236
    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    .line 243
    new-instance v0, LE/r;

    invoke-direct {v0, p1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/G;->f:LE/o;

    .line 244
    new-instance v0, Lx/c;

    invoke-virtual {p4}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/G;->g:Lx/c;

    .line 246
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/G;->i:Lo/T;

    .line 247
    const/high16 v0, 0x1

    mul-int/2addr v0, p2

    iput v0, p0, LF/G;->h:I

    .line 248
    return-void
.end method

.method private static a(Lo/M;)I
    .registers 2
    .parameter

    .prologue
    .line 191
    invoke-virtual {p0}, Lo/M;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v4

    .line 106
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 109
    const/4 v1, 0x0

    .line 110
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 112
    const/4 v0, -0x1

    move v2, v0

    move v3, v1

    .line 115
    :goto_12
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_df

    .line 116
    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    .line 117
    instance-of v0, v1, Lo/M;

    if-eqz v0, :cond_7f

    .line 118
    const/4 v0, 0x1

    if-le v2, v0, :cond_49

    move v0, v2

    .line 172
    :goto_24
    if-gez v0, :cond_27

    .line 173
    const/4 v0, 0x1

    .line 177
    :cond_27
    new-instance v1, LF/G;

    invoke-direct {v1, v3, v0, v5, p4}, LF/G;-><init>(IILjava/util/Set;LD/a;)V

    .line 178
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_30
    :goto_30
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_db

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    .line 179
    invoke-interface {v0}, Lo/n;->h()I

    move-result v3

    const/4 v5, 0x5

    if-ne v3, v5, :cond_cc

    .line 180
    check-cast v0, Lo/M;

    invoke-direct {v1, p0, v4, v0}, LF/G;->a(Lo/aq;Lo/ad;Lo/M;)V

    goto :goto_30

    .line 121
    :cond_49
    const/4 v2, 0x1

    move-object v0, v1

    .line 122
    check-cast v0, Lo/M;

    invoke-static {v0}, LF/G;->a(Lo/M;)I

    move-result v0

    .line 123
    sget v7, LF/G;->b:I

    if-le v0, v7, :cond_59

    .line 127
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_12

    .line 130
    :cond_59
    add-int v7, v0, v3

    sget v8, LF/G;->b:I

    if-le v7, v8, :cond_61

    move v0, v2

    .line 131
    goto :goto_24

    .line 133
    :cond_61
    add-int/2addr v0, v3

    .line 134
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    .line 164
    :goto_68
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v3

    array-length v7, v3

    const/4 v1, 0x0

    :goto_6e
    if-ge v1, v7, :cond_c5

    aget v8, v3, v1

    .line 165
    if-ltz v8, :cond_7c

    array-length v9, p1

    if-ge v8, v9, :cond_7c

    .line 166
    aget-object v8, p1, v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_7c
    add-int/lit8 v1, v1, 0x1

    goto :goto_6e

    .line 135
    :cond_7f
    instance-of v0, v1, Lo/K;

    if-eqz v0, :cond_df

    move-object v0, v1

    check-cast v0, Lo/K;

    invoke-static {v0}, LF/G;->a(Lo/K;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 136
    invoke-interface {v1}, Lo/n;->e()Lo/aj;

    move-result-object v0

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    invoke-static {v0}, LF/G;->b(F)I

    move-result v0

    .line 137
    if-eq v0, v2, :cond_a4

    .line 138
    if-lez v2, :cond_a3

    move v0, v2

    .line 139
    goto :goto_24

    :cond_a3
    move v2, v0

    :cond_a4
    move-object v0, v1

    .line 144
    check-cast v0, Lo/K;

    invoke-static {v0}, LF/G;->b(Lo/K;)I

    move-result v0

    .line 145
    sget v7, LF/G;->b:I

    if-le v0, v7, :cond_b4

    .line 149
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_12

    .line 152
    :cond_b4
    add-int v7, v0, v3

    sget v8, LF/G;->b:I

    if-le v7, v8, :cond_bd

    move v0, v2

    .line 153
    goto/16 :goto_24

    .line 155
    :cond_bd
    add-int/2addr v0, v3

    .line 156
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    goto :goto_68

    .line 169
    :cond_c5
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move v3, v2

    move v2, v0

    .line 170
    goto/16 :goto_12

    .line 181
    :cond_cc
    invoke-interface {v0}, Lo/n;->h()I

    move-result v3

    const/16 v5, 0x8

    if-ne v3, v5, :cond_30

    .line 182
    check-cast v0, Lo/K;

    invoke-direct {v1, p0, v4, v0}, LF/G;->a(Lo/aq;Lo/ad;Lo/K;)V

    goto/16 :goto_30

    .line 185
    :cond_db
    invoke-virtual {v1, p0, p3}, LF/G;->a(Lo/aq;I)V

    .line 186
    return-object v1

    :cond_df
    move v0, v2

    goto/16 :goto_24
.end method

.method public static declared-synchronized a(F)V
    .registers 5
    .parameter

    .prologue
    .line 223
    const-class v1, LF/G;

    monitor-enter v1

    :try_start_3
    sput p0, LF/G;->e:F

    .line 224
    const/high16 v0, 0x3f80

    const/4 v2, 0x5

    sget v3, LF/G;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, LF/G;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, LF/G;->d:F
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_1a

    .line 225
    monitor-exit v1

    return-void

    .line 223
    :catchall_1a
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(I)V
    .registers 5
    .parameter

    .prologue
    .line 214
    const-class v1, LF/G;

    monitor-enter v1

    :try_start_3
    sput p0, LF/G;->c:I

    .line 215
    const/high16 v0, 0x3f80

    const/4 v2, 0x5

    sget v3, LF/G;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, LF/G;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, LF/G;->d:F
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_1a

    .line 216
    monitor-exit v1

    return-void

    .line 214
    :catchall_1a
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lo/aq;Lo/ad;Lo/K;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 343
    invoke-virtual {p3}, Lo/K;->e()Lo/aj;

    move-result-object v2

    .line 344
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_22

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    if-nez v0, :cond_23

    .line 385
    :cond_22
    :goto_22
    return-void

    .line 349
    :cond_23
    invoke-virtual {p3}, Lo/K;->b()Lo/X;

    move-result-object v3

    .line 350
    invoke-virtual {v3}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 355
    mul-int/lit8 v5, v4, 0x2

    .line 360
    invoke-virtual {p2}, Lo/ad;->d()Lo/T;

    move-result-object v6

    .line 361
    invoke-virtual {p2}, Lo/ad;->g()I

    move-result v7

    move v0, v1

    .line 362
    :goto_38
    if-gt v0, v4, :cond_5b

    .line 363
    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-virtual {v3, v0, v8}, Lo/X;->a(ILo/T;)V

    .line 364
    iget-object v8, p0, LF/G;->i:Lo/T;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-static {v8, v6, v9}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 373
    iget-object v8, p0, LF/G;->f:LE/o;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-virtual {v8, v9, v7}, LE/o;->a(Lo/T;I)V

    .line 374
    if-lez v0, :cond_58

    if-ge v0, v4, :cond_58

    .line 375
    iget-object v8, p0, LF/G;->f:LE/o;

    iget-object v9, p0, LF/G;->i:Lo/T;

    invoke-virtual {v8, v9, v7}, LE/o;->a(Lo/T;I)V

    .line 362
    :cond_58
    add-int/lit8 v0, v0, 0x1

    goto :goto_38

    .line 383
    :cond_5b
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->b()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lx/c;->a(II)V

    goto :goto_22
.end method

.method private a(Lo/aq;Lo/ad;Lo/M;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 305
    invoke-virtual {p3}, Lo/M;->e()Lo/aj;

    move-result-object v2

    .line 306
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_22

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    if-nez v0, :cond_23

    .line 334
    :cond_22
    :goto_22
    return-void

    .line 311
    :cond_23
    invoke-virtual {p3}, Lo/M;->b()Lo/X;

    move-result-object v3

    .line 312
    invoke-virtual {v3}, Lo/X;->b()I

    move-result v4

    .line 317
    invoke-virtual {p2}, Lo/ad;->d()Lo/T;

    move-result-object v5

    .line 318
    invoke-virtual {p2}, Lo/ad;->g()I

    move-result v6

    move v0, v1

    .line 319
    :goto_34
    if-ge v0, v4, :cond_4c

    .line 320
    iget-object v7, p0, LF/G;->i:Lo/T;

    invoke-virtual {v3, v0, v7}, Lo/X;->a(ILo/T;)V

    .line 321
    iget-object v7, p0, LF/G;->i:Lo/T;

    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-static {v7, v5, v8}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 326
    iget-object v7, p0, LF/G;->f:LE/o;

    iget-object v8, p0, LF/G;->i:Lo/T;

    invoke-virtual {v7, v8, v6}, LE/o;->a(Lo/T;I)V

    .line 319
    add-int/lit8 v0, v0, 0x1

    goto :goto_34

    .line 332
    :cond_4c
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v2, v1}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    invoke-virtual {v1}, Lo/ai;->b()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lx/c;->a(II)V

    goto :goto_22
.end method

.method public static declared-synchronized a(Lo/K;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 201
    const-class v3, LF/G;

    monitor-enter v3

    :try_start_5
    invoke-virtual {p0}, Lo/K;->e()Lo/aj;

    move-result-object v4

    .line 202
    invoke-virtual {v4}, Lo/aj;->d()Z

    move-result v2

    if-eqz v2, :cond_35

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->d()[I

    move-result-object v2

    array-length v2, v2

    if-eqz v2, :cond_35

    move v2, v0

    .line 204
    :goto_1c
    invoke-virtual {v4}, Lo/aj;->b()I

    move-result v5

    if-ne v5, v0, :cond_37

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v4

    invoke-virtual {v4}, Lo/ai;->c()F

    move-result v4

    sget v5, LF/G;->d:F
    :try_end_2d
    .catchall {:try_start_5 .. :try_end_2d} :catchall_39

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_37

    if-nez v2, :cond_37

    :goto_33
    monitor-exit v3

    return v0

    :cond_35
    move v2, v1

    .line 202
    goto :goto_1c

    :cond_37
    move v0, v1

    .line 204
    goto :goto_33

    .line 201
    :catchall_39
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method static declared-synchronized b(F)I
    .registers 5
    .parameter

    .prologue
    .line 232
    const-class v1, LF/G;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_4
    sget v2, LF/G;->c:I

    sget v3, LF/G;->e:F

    mul-float/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_17

    move-result v0

    monitor-exit v1

    return v0

    :catchall_17
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lo/K;)I
    .registers 2
    .parameter

    .prologue
    .line 196
    invoke-virtual {p0}, Lo/K;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 408
    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 258
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    .line 260
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x1

    .line 281
    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-eqz v0, :cond_35

    .line 286
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 287
    iget v1, p0, LF/G;->h:I

    if-le v1, v4, :cond_17

    .line 288
    iget v1, p0, LF/G;->h:I

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    .line 290
    :cond_17
    iget-object v1, p0, LF/G;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 291
    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    .line 292
    invoke-static {p1}, Lx/a;->c(LD/a;)V

    .line 293
    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, LF/G;->f:LE/o;

    invoke-virtual {v3}, LE/o;->a()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 294
    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    .line 295
    invoke-static {p1}, Lx/a;->d(LD/a;)V

    .line 296
    :cond_35
    return-void
.end method

.method public a(Lo/aq;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 389
    return-void
.end method

.method public b()I
    .registers 3

    .prologue
    .line 417
    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x78

    iget-object v1, p0, LF/G;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, LF/G;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 269
    iget-object v0, p0, LF/G;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    .line 271
    return-void
.end method
