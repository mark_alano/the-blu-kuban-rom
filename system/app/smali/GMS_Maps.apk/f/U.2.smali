.class public LF/U;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# static fields
.field static final a:LF/U;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    new-instance v0, LF/U;

    invoke-direct {v0}, LF/U;-><init>()V

    sput-object v0, LF/U;->a:LF/U;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LD/a;LC/a;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x1

    .line 60
    invoke-virtual {p0}, LD/a;->A()V

    .line 61
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 63
    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    .line 65
    invoke-virtual {p0}, LD/a;->o()V

    .line 66
    invoke-virtual {p0}, LD/a;->q()V

    .line 67
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 68
    invoke-virtual {p0}, LD/a;->B()V

    .line 69
    return-void
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 32
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, LA/c;->i()Z

    move-result v2

    sput-boolean v2, LF/U;->b:Z

    .line 34
    sget-boolean v2, LF/U;->b:Z

    if-eqz v2, :cond_29

    .line 35
    invoke-virtual {v0, v1}, LA/c;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 36
    const/high16 v0, 0x40a0

    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    .line 37
    iget-object v0, p0, LD/a;->j:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    .line 39
    :cond_29
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 73
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    sget-boolean v0, LF/U;->b:Z

    if-eqz v0, :cond_e

    .line 49
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 50
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 52
    :cond_e
    return-void
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    return-void
.end method
