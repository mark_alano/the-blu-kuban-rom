.class public LF/V;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static final c:I

.field private static volatile d:Z

.field private static final j:F


# instance fields
.field private final e:Lo/aq;

.field private final f:LE/o;

.field private final g:LE/a;

.field private final h:LE/i;

.field private final i:LE/d;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, LF/V;->b:[I

    .line 62
    sget-object v0, LF/V;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x64

    sput v0, LF/V;->c:I

    .line 65
    const/4 v0, 0x0

    sput-boolean v0, LF/V;->d:Z

    .line 94
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, LF/V;->j:F

    return-void

    .line 47
    nop

    :array_1e
    .array-data 0x4
        0xct 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x12t 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private constructor <init>(Lo/aq;LF/X;Ljava/util/Set;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    .line 155
    iput-object p1, p0, LF/V;->e:Lo/aq;

    .line 164
    new-instance v0, LE/o;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    iput-object v0, p0, LF/V;->f:LE/o;

    .line 165
    new-instance v0, LE/a;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/a;-><init>(I)V

    iput-object v0, p0, LF/V;->g:LE/a;

    .line 166
    new-instance v0, LE/i;

    iget v1, p2, LF/X;->a:I

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, LF/V;->h:LE/i;

    .line 167
    new-instance v0, LE/d;

    iget v1, p2, LF/X;->b:I

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    iput-object v0, p0, LF/V;->i:LE/d;

    .line 169
    return-void
.end method

.method private static a(FI)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 297
    int-to-float v0, p1

    mul-float/2addr v0, p0

    const/high16 v1, 0x3fa0

    mul-float/2addr v0, v1

    const/high16 v1, 0x4380

    div-float/2addr v0, v1

    return v0
.end method

.method private static a(IF)F
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 390
    packed-switch p0, :pswitch_data_14

    .line 398
    const/high16 v0, 0x42a0

    div-float/2addr v0, p1

    :goto_6
    return v0

    .line 392
    :pswitch_7
    const/high16 v0, 0x43b4

    div-float/2addr v0, p1

    goto :goto_6

    .line 394
    :pswitch_b
    const/high16 v0, 0x4370

    div-float/2addr v0, p1

    goto :goto_6

    .line 396
    :pswitch_f
    const/high16 v0, 0x4320

    div-float/2addr v0, p1

    goto :goto_6

    .line 390
    nop

    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_7
        :pswitch_b
        :pswitch_f
    .end packed-switch
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/V;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 116
    new-instance v3, LF/X;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, LF/X;-><init>(LF/W;)V

    .line 117
    invoke-interface {p2}, Lo/aO;->c()V

    .line 118
    :cond_e
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 119
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    .line 120
    instance-of v1, v0, Lo/aC;

    if-eqz v1, :cond_27

    move-object v1, v0

    check-cast v1, Lo/aC;

    invoke-static {v1, v3}, LF/V;->a(Lo/aC;LF/X;)Z

    move-result v1

    if-nez v1, :cond_51

    .line 129
    :cond_27
    invoke-interface {p2}, Lo/aO;->d()V

    .line 131
    new-instance v1, LF/V;

    invoke-direct {v1, p0, v3, v2}, LF/V;-><init>(Lo/aq;LF/X;Ljava/util/Set;)V

    .line 132
    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v2

    .line 134
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v3

    .line 136
    :goto_37
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 137
    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v0

    .line 138
    instance-of v4, v0, Lo/aC;

    if-eqz v4, :cond_4d

    .line 139
    check-cast v0, Lo/aC;

    .line 140
    invoke-direct {v1, v2, v0, v3}, LF/V;->a(Lo/ad;Lo/aC;Lx/h;)Z

    move-result v0

    if-nez v0, :cond_68

    .line 148
    :cond_4d
    invoke-direct {v1, p0}, LF/V;->a(Lo/aq;)V

    .line 149
    return-object v1

    .line 123
    :cond_51
    invoke-interface {v0}, Lo/n;->l()[I

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_57
    if-ge v0, v4, :cond_e

    aget v5, v1, v0

    .line 124
    if-ltz v5, :cond_65

    array-length v6, p1

    if-ge v5, v6, :cond_65

    .line 125
    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_65
    add-int/lit8 v0, v0, 0x1

    goto :goto_57

    .line 146
    :cond_68
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_37
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 173
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 174
    invoke-virtual {p0}, LD/a;->p()V

    .line 175
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 176
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 180
    return-void
.end method

.method private a(Lo/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 347
    return-void
.end method

.method public static a(Z)V
    .registers 1
    .parameter

    .prologue
    .line 103
    sput-boolean p0, LF/V;->d:Z

    .line 104
    return-void
.end method

.method public static a(Lo/aC;LF/X;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 367
    invoke-virtual {p0}, Lo/aC;->b()Lo/X;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    .line 369
    add-int/lit8 v0, v0, -0x1

    .line 370
    mul-int/lit8 v1, v0, 0x4

    .line 371
    iget v2, p1, LF/X;->a:I

    add-int/2addr v2, v1

    const/16 v3, 0x1000

    if-le v2, v3, :cond_15

    .line 372
    const/4 v0, 0x0

    .line 380
    :goto_14
    return v0

    .line 374
    :cond_15
    iget v2, p1, LF/X;->a:I

    add-int/2addr v1, v2

    iput v1, p1, LF/X;->a:I

    .line 377
    mul-int/lit8 v1, v0, 0x2

    .line 378
    add-int/lit8 v0, v0, -0x1

    .line 379
    iget v2, p1, LF/X;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v2

    iput v0, p1, LF/X;->b:I

    .line 380
    const/4 v0, 0x1

    goto :goto_14
.end method

.method private a(Lo/ad;Lo/aC;Lx/h;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    .line 301
    invoke-virtual {p2}, Lo/aC;->b()Lo/X;

    move-result-object v0

    .line 302
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f80

    mul-float/2addr v1, v2

    const/high16 v2, 0x4380

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lo/X;->b(F)Lo/X;

    move-result-object v1

    .line 304
    invoke-virtual {v1}, Lo/X;->b()I

    move-result v0

    .line 305
    add-int/lit8 v0, v0, -0x1

    .line 306
    iget-object v2, p0, LF/V;->f:LE/o;

    invoke-virtual {v2}, LE/o;->a()I

    move-result v2

    .line 308
    mul-int/lit8 v3, v0, 0x4

    .line 309
    add-int v5, v2, v3

    .line 311
    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    .line 312
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v4

    .line 314
    invoke-virtual {p2}, Lo/aC;->e()Lo/aj;

    move-result-object v2

    .line 316
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v6

    if-gtz v6, :cond_37

    .line 342
    :goto_36
    return v10

    .line 319
    :cond_37
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lo/aj;->b(I)Lo/ai;

    move-result-object v6

    invoke-virtual {v6}, Lo/ai;->c()F

    move-result v6

    .line 320
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v2, v7}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->b()I

    move-result v7

    .line 323
    invoke-static {v6, v4}, LF/V;->a(FI)F

    move-result v2

    .line 324
    invoke-virtual {p2}, Lo/aC;->c()Z

    move-result v8

    if-nez v8, :cond_59

    .line 325
    neg-float v2, v2

    .line 336
    :cond_59
    iget-object v8, p0, LF/V;->g:LE/a;

    invoke-virtual {v8, v5}, LE/a;->b(I)V

    .line 337
    iget-object v5, p0, LF/V;->g:LE/a;

    mul-int/lit8 v0, v0, 0x4

    invoke-virtual {v5, v7, v0}, LE/a;->b(II)V

    .line 338
    invoke-virtual {p2}, Lo/aC;->d()I

    move-result v0

    invoke-static {v0, v6}, LF/V;->a(IF)F

    move-result v5

    .line 339
    iget-object v6, p0, LF/V;->f:LE/o;

    iget-object v7, p0, LF/V;->i:LE/d;

    iget-object v8, p0, LF/V;->h:LE/i;

    const/4 v9, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FLo/T;IFLE/q;LE/e;LE/k;LE/k;)V

    goto :goto_36
.end method

.method private b(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x4

    const/high16 v2, 0x1

    .line 270
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_b

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->d:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_24

    .line 271
    :cond_b
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 272
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 274
    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1, v3}, LE/d;->a(LD/a;I)V

    .line 277
    :cond_24
    const/16 v0, 0xb

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 278
    invoke-virtual {p1}, LD/a;->n()V

    .line 280
    sget-boolean v0, LF/V;->d:Z

    if-eqz v0, :cond_3f

    .line 281
    iget-object v0, p0, LF/V;->g:LE/a;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, LE/a;->a(I)V

    .line 284
    :cond_3f
    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    .line 285
    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1, v3}, LE/d;->a(LD/a;I)V

    .line 286
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 407
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/V;->g:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->h:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->i:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 190
    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 191
    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 192
    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 194
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x1702

    const/16 v4, 0x1700

    const/4 v3, 0x0

    .line 215
    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0}, LE/d;->b()I

    move-result v0

    if-nez v0, :cond_e

    .line 243
    :cond_d
    :goto_d
    return-void

    .line 219
    :cond_e
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 220
    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 223
    invoke-virtual {p2}, LC/a;->r()F

    move-result v0

    iget-object v1, p0, LF/V;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f00

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_6a

    const/4 v0, 0x1

    .line 224
    :goto_2b
    if-eqz v0, :cond_4b

    .line 225
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 226
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 227
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget v2, LF/V;->j:F

    invoke-interface {v1, v2, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 228
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 234
    :cond_4b
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    invoke-direct {p0, p1, v1}, LF/V;->b(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    .line 237
    if-eqz v0, :cond_d

    .line 239
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 240
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 241
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto :goto_d

    .line 223
    :cond_6a
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method public b()I
    .registers 3

    .prologue
    .line 417
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0xb8

    iget-object v1, p0, LF/V;->g:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->h:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/V;->i:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, LF/V;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 203
    iget-object v0, p0, LF/V;->g:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 204
    iget-object v0, p0, LF/V;->h:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 205
    iget-object v0, p0, LF/V;->i:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 207
    return-void
.end method
