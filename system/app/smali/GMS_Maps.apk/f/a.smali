.class public LF/a;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static c:I

.field private static final k:Lo/aj;

.field private static final l:Lo/aj;

.field private static final m:Ljava/lang/ThreadLocal;


# instance fields
.field private final d:LE/o;

.field private final e:LE/o;

.field private final f:Lx/c;

.field private final g:Lx/c;

.field private h:Lz/v;

.field private i:Lz/v;

.field private final j:LF/c;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v2, 0x2

    const/4 v10, 0x1

    const v9, -0x45749f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 72
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_4c

    sput-object v0, LF/a;->b:[I

    .line 78
    const/16 v0, 0x4000

    sput v0, LF/a;->c:I

    .line 123
    new-instance v0, Lo/aj;

    new-array v3, v1, [I

    new-array v4, v10, [Lo/ai;

    new-instance v6, Lo/ai;

    const/high16 v7, 0x4000

    new-array v8, v1, [I

    invoke-direct {v6, v9, v7, v8, v1}, Lo/ai;-><init>(IF[II)V

    aput-object v6, v4, v1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LF/a;->k:Lo/aj;

    .line 126
    new-instance v0, Lo/aj;

    new-array v3, v1, [I

    new-array v4, v10, [Lo/ai;

    new-instance v6, Lo/ai;

    const/high16 v7, 0x3fc0

    new-array v8, v1, [I

    invoke-direct {v6, v9, v7, v8, v1}, Lo/ai;-><init>(IF[II)V

    aput-object v6, v4, v1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LF/a;->l:Lo/aj;

    .line 141
    new-instance v0, LF/b;

    invoke-direct {v0}, LF/b;-><init>()V

    sput-object v0, LF/a;->m:Ljava/lang/ThreadLocal;

    return-void

    .line 72
    :array_4c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private constructor <init>(IILjava/util/Set;LF/c;LD/a;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 310
    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    .line 322
    iput-object v0, p0, LF/a;->i:Lz/v;

    .line 323
    iput-object v0, p0, LF/a;->h:Lz/v;

    .line 324
    const/4 v0, 0x1

    .line 325
    new-instance v1, LE/r;

    invoke-direct {v1, p1, v0}, LE/r;-><init>(IZ)V

    iput-object v1, p0, LF/a;->e:LE/o;

    .line 326
    new-instance v1, LE/r;

    invoke-direct {v1, p2, v0}, LE/r;-><init>(IZ)V

    iput-object v1, p0, LF/a;->d:LE/o;

    .line 329
    new-instance v0, Lx/c;

    invoke-virtual {p5}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/a;->g:Lx/c;

    .line 331
    new-instance v0, Lx/c;

    invoke-virtual {p5}, LD/a;->G()Lx/a;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/a;->f:Lx/c;

    .line 334
    iput-object p4, p0, LF/a;->j:LF/c;

    .line 335
    return-void
.end method

.method static a(Lo/f;)I
    .registers 2
    .parameter

    .prologue
    .line 437
    invoke-virtual {p0}, Lo/f;->e()Lo/aj;

    move-result-object v0

    invoke-virtual {v0}, Lo/aj;->c()I

    move-result v0

    if-nez v0, :cond_c

    .line 438
    const/4 v0, 0x0

    .line 440
    :goto_b
    return v0

    :cond_c
    invoke-virtual {p0}, Lo/f;->b()Lo/aF;

    move-result-object v0

    invoke-virtual {v0}, Lo/aF;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    goto :goto_b
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v7

    .line 220
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 221
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 222
    const/4 v1, 0x0

    .line 223
    if-eqz p4, :cond_fa

    .line 224
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    move-object v6, v1

    .line 229
    :goto_16
    const/4 v2, 0x0

    .line 230
    const/4 v3, 0x0

    .line 232
    :goto_18
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_49

    .line 233
    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v5

    .line 234
    instance-of v1, v5, Lo/f;

    if-eqz v1, :cond_49

    move-object v1, v5

    .line 235
    check-cast v1, Lo/f;

    .line 236
    invoke-static {v1}, LF/a;->a(Lo/f;)I

    move-result v9

    .line 237
    invoke-static {v1}, LF/a;->b(Lo/f;)I

    move-result v10

    .line 238
    sget v11, LF/a;->c:I

    if-gt v9, v11, :cond_39

    sget v11, LF/a;->c:I

    if-le v10, v11, :cond_3d

    .line 245
    :cond_39
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_18

    .line 248
    :cond_3d
    add-int v11, v2, v9

    sget v12, LF/a;->c:I

    if-gt v11, v12, :cond_49

    add-int v11, v3, v10

    sget v12, LF/a;->c:I

    if-le v11, v12, :cond_70

    .line 287
    :cond_49
    const/4 v5, 0x0

    .line 288
    if-eqz v6, :cond_d9

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d9

    .line 289
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v9

    .line 290
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5a
    :goto_5a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/f;

    .line 291
    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v10

    if-eqz v10, :cond_5a

    .line 292
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5a

    .line 252
    :cond_70
    add-int/2addr v2, v9

    .line 253
    add-int/2addr v3, v10

    .line 254
    invoke-interface {v5}, Lo/n;->l()[I

    move-result-object v9

    array-length v10, v9

    const/4 v5, 0x0

    :goto_78
    if-ge v5, v10, :cond_89

    aget v11, v9, v5

    .line 255
    if-ltz v11, :cond_86

    array-length v12, p1

    if-ge v11, v12, :cond_86

    .line 256
    aget-object v11, p1, v11

    invoke-virtual {v4, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_86
    add-int/lit8 v5, v5, 0x1

    goto :goto_78

    .line 259
    :cond_89
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    if-eqz p4, :cond_d0

    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v5

    if-eqz v5, :cond_d0

    const/4 v5, 0x1

    .line 266
    :goto_95
    invoke-static {v1}, LF/a;->c(Lo/f;)Z

    move-result v9

    .line 267
    if-nez v5, :cond_9d

    if-nez v9, :cond_cb

    :cond_9d
    invoke-virtual {v1}, Lo/f;->c()Z

    move-result v10

    if-eqz v10, :cond_cb

    .line 268
    invoke-virtual {v1}, Lo/f;->b()Lo/aF;

    move-result-object v10

    invoke-virtual {v1}, Lo/f;->d()[B

    move-result-object v11

    invoke-static {v10, v11}, Lx/p;->a(Lo/aF;[B)Ljava/util/List;

    move-result-object v10

    .line 271
    if-eqz p3, :cond_b9

    if-nez v9, :cond_b9

    .line 272
    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, v9}, LF/a;->a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V

    .line 274
    :cond_b9
    if-eqz p3, :cond_c6

    invoke-virtual {v1}, Lo/f;->j()Z

    move-result v9

    if-eqz v9, :cond_c6

    .line 275
    move-object/from16 v0, p3

    invoke-static {v0, v1, v10, p0}, LF/a;->a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V

    .line 277
    :cond_c6
    if-eqz v5, :cond_cb

    .line 278
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 284
    :cond_cb
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_18

    .line 265
    :cond_d0
    const/4 v5, 0x0

    goto :goto_95

    .line 295
    :cond_d2
    new-instance v5, LF/c;

    move-object/from16 v0, p4

    invoke-direct {v5, v7, v6, v9, v0}, LF/c;-><init>(Lo/ad;Ljava/util/List;Ljava/util/List;LF/d;)V

    .line 299
    :cond_d9
    new-instance v1, LF/a;

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, LF/a;-><init>(IILjava/util/Set;LF/c;LD/a;)V

    .line 301
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/f;

    .line 302
    invoke-direct {v1, v7, v2}, LF/a;->a(Lo/ad;Lo/f;)V

    goto :goto_e4

    .line 304
    :cond_f4
    move/from16 v0, p5

    invoke-direct {v1, p0, v0}, LF/a;->a(Lo/aq;I)V

    .line 305
    return-object v1

    :cond_fa
    move-object v6, v1

    goto/16 :goto_16
.end method

.method private static a(F)Lo/aj;
    .registers 2
    .parameter

    .prologue
    .line 137
    const/high16 v0, 0x4190

    cmpl-float v0, p0, v0

    if-lez v0, :cond_9

    sget-object v0, LF/a;->k:Lo/aj;

    :goto_8
    return-object v0

    :cond_9
    sget-object v0, LF/a;->l:Lo/aj;

    goto :goto_8
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x1

    .line 378
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 380
    invoke-virtual {p0}, LD/a;->p()V

    .line 382
    :cond_f
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    .line 383
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 386
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 387
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 389
    return-void
.end method

.method private static a(LF/y;Lo/f;Ljava/util/List;Lo/aq;)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 169
    if-eqz p3, :cond_30

    .line 170
    sget-object v1, Lo/av;->c:Lo/av;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    .line 171
    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v2

    .line 172
    invoke-virtual/range {p3 .. p3}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, LF/a;->a(F)Lo/aj;

    move-result-object v5

    .line 178
    :goto_19
    invoke-virtual {p1}, Lo/f;->b()Lo/aF;

    move-result-object v1

    invoke-virtual {v1}, Lo/aF;->a()I

    move-result v1

    .line 179
    if-eqz v1, :cond_2f

    invoke-virtual {v5}, Lo/aj;->b()I

    move-result v1

    if-eqz v1, :cond_2f

    invoke-virtual {p1}, Lo/f;->c()Z

    move-result v1

    if-nez v1, :cond_39

    .line 196
    :cond_2f
    return-void

    .line 174
    :cond_30
    invoke-virtual {p1}, Lo/f;->a()Lo/o;

    move-result-object v2

    .line 175
    invoke-virtual {p1}, Lo/f;->e()Lo/aj;

    move-result-object v5

    goto :goto_19

    .line 184
    :cond_39
    invoke-virtual {p1}, Lo/f;->i()I

    move-result v8

    .line 187
    invoke-virtual {p1}, Lo/f;->f()I

    move-result v6

    .line 188
    invoke-virtual {p1}, Lo/f;->g()Ljava/lang/String;

    move-result-object v7

    .line 189
    invoke-virtual {p1}, Lo/f;->l()[I

    move-result-object v11

    .line 191
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4d
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lo/X;

    .line 192
    new-instance v1, Lo/K;

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    if-eqz v2, :cond_68

    const/4 v12, 0x1

    :goto_61
    invoke-direct/range {v1 .. v12}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V

    .line 194
    invoke-virtual {p0, v1}, LF/y;->a(Lo/n;)V

    goto :goto_4d

    .line 192
    :cond_68
    const/4 v12, 0x0

    goto :goto_61
.end method

.method private a(Lo/ad;Lo/f;)V
    .registers 20
    .parameter
    .parameter

    .prologue
    .line 493
    invoke-virtual/range {p2 .. p2}, Lo/f;->e()Lo/aj;

    move-result-object v4

    .line 494
    invoke-virtual/range {p2 .. p2}, Lo/f;->b()Lo/aF;

    move-result-object v1

    .line 495
    invoke-virtual {v1}, Lo/aF;->a()I

    move-result v12

    .line 496
    if-nez v12, :cond_f

    .line 588
    :cond_e
    :goto_e
    return-void

    .line 499
    :cond_f
    invoke-virtual {v4}, Lo/aj;->c()I

    move-result v2

    if-lez v2, :cond_ed

    const/4 v2, 0x1

    move v7, v2

    .line 500
    :goto_17
    invoke-static/range {p2 .. p2}, LF/a;->c(Lo/f;)Z

    move-result v13

    .line 501
    if-nez v7, :cond_1f

    if-eqz v13, :cond_e

    .line 508
    :cond_1f
    invoke-virtual/range {p1 .. p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    .line 509
    invoke-virtual/range {p1 .. p1}, Lo/ad;->g()I

    move-result v14

    .line 510
    if-eqz v7, :cond_f1

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->a(I)I

    move-result v2

    move v11, v2

    .line 511
    :goto_2f
    if-eqz v13, :cond_f5

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->b()I

    move-result v2

    move v8, v2

    .line 512
    :goto_3b
    invoke-virtual/range {p2 .. p2}, Lo/f;->d()[B

    move-result-object v15

    .line 514
    const/4 v10, 0x0

    .line 515
    const/4 v9, 0x0

    .line 516
    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    .line 517
    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    .line 518
    sget-object v2, LF/a;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lo/T;

    const/4 v6, 0x2

    aget-object v6, v2, v6

    .line 519
    const/4 v2, 0x0

    :goto_63
    if-ge v2, v12, :cond_f9

    .line 521
    invoke-virtual/range {v1 .. v6}, Lo/aF;->a(ILo/T;Lo/T;Lo/T;Lo/T;)V

    .line 523
    if-eqz v7, :cond_8d

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->e:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    .line 537
    add-int/lit8 v10, v10, 0x3

    .line 539
    :cond_8d
    if-eqz v13, :cond_e9

    .line 540
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x1

    if-eqz v16, :cond_ad

    .line 548
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    .line 551
    add-int/lit8 v9, v9, 0x2

    .line 553
    :cond_ad
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x2

    if-eqz v16, :cond_cb

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v14}, LE/o;->a(Lo/T;I)V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    .line 564
    add-int/lit8 v9, v9, 0x2

    .line 566
    :cond_cb
    aget-byte v16, v15, v2

    and-int/lit8 v16, v16, 0x4

    if-eqz v16, :cond_e9

    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v14}, LE/o;->a(Lo/T;I)V

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, LF/a;->d:LE/o;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v14}, LE/o;->a(Lo/T;I)V

    .line 577
    add-int/lit8 v9, v9, 0x2

    .line 519
    :cond_e9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_63

    .line 499
    :cond_ed
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_17

    .line 510
    :cond_f1
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_2f

    .line 511
    :cond_f5
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_3b

    .line 585
    :cond_f9
    move-object/from16 v0, p0

    iget-object v1, v0, LF/a;->g:Lx/c;

    invoke-virtual {v1, v11, v10}, Lx/c;->a(II)V

    .line 586
    move-object/from16 v0, p0

    iget-object v1, v0, LF/a;->f:Lx/c;

    invoke-virtual {v1, v8, v9}, Lx/c;->a(II)V

    goto/16 :goto_e
.end method

.method private a(Lo/aq;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 592
    return-void
.end method

.method static b(Lo/f;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 476
    invoke-static {p0}, LF/a;->c(Lo/f;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 485
    :goto_7
    return v0

    .line 480
    :cond_8
    invoke-virtual {p0}, Lo/f;->d()[B

    move-result-object v2

    move v1, v0

    .line 481
    :goto_d
    array-length v3, v2

    if-ge v0, v3, :cond_1c

    .line 482
    aget-byte v3, v2, v0

    and-int/lit8 v3, v3, 0x7

    .line 483
    sget-object v4, LF/a;->b:[I

    aget v3, v4, v3

    add-int/2addr v1, v3

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_1c
    move v0, v1

    .line 485
    goto :goto_7
.end method

.method static synthetic c()Ljava/lang/ThreadLocal;
    .registers 1

    .prologue
    .line 65
    sget-object v0, LF/a;->m:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method private static c(Lo/f;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 453
    invoke-virtual {p0}, Lo/f;->e()Lo/aj;

    move-result-object v2

    .line 455
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lo/f;->c()Z

    move-result v0

    if-nez v0, :cond_12

    .line 468
    :cond_11
    :goto_11
    return v1

    :cond_12
    move v0, v1

    .line 459
    :goto_13
    invoke-virtual {v2}, Lo/aj;->b()I

    move-result v3

    if-ge v0, v3, :cond_34

    .line 460
    invoke-virtual {v2, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v3

    invoke-virtual {v3}, Lo/ai;->c()F

    move-result v3

    const/high16 v4, 0x3f80

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_11

    .line 463
    invoke-virtual {v2, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v3

    invoke-virtual {v3}, Lo/ai;->e()Z

    move-result v3

    if-nez v3, :cond_11

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 468
    :cond_34
    const/4 v1, 0x1

    goto :goto_11
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 630
    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/a;->j:LF/c;

    if-nez v0, :cond_22

    const/4 v0, 0x0

    :goto_20
    add-int/2addr v0, v1

    return v0

    :cond_22
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0}, LF/c;->a()I

    move-result v0

    goto :goto_20
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 346
    iget-object v0, p0, LF/a;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 347
    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 348
    iget-object v0, p0, LF/a;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    .line 349
    iget-object v0, p0, LF/a;->f:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    .line 351
    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_1d

    .line 352
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1}, LF/c;->a(LD/a;)V

    .line 354
    :cond_1d
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 401
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 402
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    .line 403
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->c()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 404
    invoke-static {p1}, Lx/a;->c(LD/a;)V

    .line 405
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_2f

    .line 406
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 407
    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    .line 408
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 410
    :cond_2f
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_4a

    .line 411
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 412
    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1, p1}, Lx/c;->a(LD/a;)V

    .line 413
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 415
    :cond_4a
    invoke-static {p1}, Lx/a;->d(LD/a;)V

    .line 430
    :cond_4d
    :goto_4d
    return-void

    .line 416
    :cond_4e
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->e()Z

    move-result v2

    if-eqz v2, :cond_81

    .line 417
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_6a

    .line 418
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 419
    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v5, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 421
    :cond_6a
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_4d

    .line 422
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 423
    iget-object v1, p0, LF/a;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    invoke-interface {v0, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    goto :goto_4d

    .line 425
    :cond_81
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->d()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 426
    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_4d

    .line 427
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1, p2, p3}, LF/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_4d
.end method

.method public b()I
    .registers 3

    .prologue
    .line 643
    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x9c

    iget-object v1, p0, LF/a;->e:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->f:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/a;->g:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/a;->j:LF/c;

    if-nez v0, :cond_24

    const/4 v0, 0x0

    :goto_22
    add-int/2addr v0, v1

    return v0

    :cond_24
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0}, LF/c;->b()I

    move-result v0

    goto :goto_22
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 363
    iget-object v0, p0, LF/a;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 364
    iget-object v0, p0, LF/a;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 365
    iget-object v0, p0, LF/a;->g:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    .line 366
    iget-object v0, p0, LF/a;->f:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    .line 368
    iget-object v0, p0, LF/a;->j:LF/c;

    if-eqz v0, :cond_1d

    .line 369
    iget-object v0, p0, LF/a;->j:LF/c;

    invoke-virtual {v0, p1}, LF/c;->b(LD/a;)V

    .line 371
    :cond_1d
    return-void
.end method
