.class public LF/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/u;


# instance fields
.field private final a:Landroid/graphics/Bitmap;

.field private final b:LD/c;

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method constructor <init>(Landroid/graphics/Bitmap;FLD/c;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 604
    iput-object p1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    .line 605
    iput-object p3, p0, LF/q;->b:LD/c;

    .line 606
    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, LF/q;->c:I

    .line 607
    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, LF/q;->d:I

    .line 608
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 611
    iget v0, p0, LF/q;->c:I

    int-to-float v0, v0

    return v0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 623
    iget-object v0, p0, LF/q;->b:LD/c;

    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 624
    if-nez v0, :cond_36

    .line 625
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    .line 626
    iget-boolean v1, p0, LF/q;->e:Z

    if-eqz v1, :cond_19

    .line 627
    invoke-virtual {v0, v2}, LD/b;->e(Z)V

    .line 629
    :cond_19
    invoke-virtual {v0, v2}, LD/b;->c(Z)V

    .line 633
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LD/b;->d(Z)V

    .line 637
    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v1

    if-eqz v1, :cond_3a

    sget-boolean v1, Lcom/google/googlenav/android/E;->f:Z

    if-nez v1, :cond_3a

    .line 639
    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;)V

    .line 643
    :goto_2f
    iget-object v1, p0, LF/q;->b:LD/c;

    iget-object v2, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0}, LD/c;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 645
    :cond_36
    invoke-virtual {v0}, LD/b;->f()V

    .line 646
    return-object v0

    .line 641
    :cond_3a
    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/b;->b(Landroid/graphics/Bitmap;)V

    goto :goto_2f
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .registers 4
    .parameter

    .prologue
    .line 651
    iget-object v0, p0, LF/q;->b:LD/c;

    iget-object v1, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/c;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 652
    if-eqz v0, :cond_f

    .line 653
    invoke-virtual {v0}, LD/b;->f()V

    .line 655
    :cond_f
    return-object v0
.end method

.method public b()F
    .registers 2

    .prologue
    .line 613
    iget v0, p0, LF/q;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 615
    const/4 v0, 0x0

    return v0
.end method

.method public d()F
    .registers 2

    .prologue
    .line 617
    const/4 v0, 0x0

    return v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 619
    iget v0, p0, LF/q;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public f()V
    .registers 2

    .prologue
    .line 672
    iget-boolean v0, p0, LF/q;->e:Z

    if-eqz v0, :cond_9

    .line 673
    iget-object v0, p0, LF/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 675
    :cond_9
    return-void
.end method
