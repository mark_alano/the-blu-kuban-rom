.class public LF/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/u;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/aV;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lo/aj;

.field private final e:Lcom/google/android/maps/driveabout/vector/aX;

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:F

.field private final j:I


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    iput-object p1, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    .line 482
    iput-object p2, p0, LF/t;->b:Ljava/lang/String;

    .line 483
    iput p3, p0, LF/t;->c:I

    .line 484
    iput-object p4, p0, LF/t;->d:Lo/aj;

    .line 485
    const/high16 v6, 0x3f80

    .line 486
    invoke-virtual {p4}, Lo/aj;->f()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-virtual {p4}, Lo/aj;->i()Lo/an;

    move-result-object v0

    invoke-virtual {v0}, Lo/an;->b()I

    move-result v0

    :goto_1d
    iput v0, p0, LF/t;->j:I

    .line 490
    invoke-virtual {p4}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 491
    invoke-virtual {p4}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->g()F

    move-result v6

    .line 493
    :cond_2d
    iput-object p5, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    .line 494
    iget-object v0, p0, LF/t;->d:Lo/aj;

    if-eqz v0, :cond_5e

    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v0}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_39
    int-to-float v4, p3

    move-object v0, p1

    move-object v1, p2

    move-object v2, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZF)[F

    move-result-object v0

    .line 496
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->c:Lcom/google/android/maps/driveabout/vector/aX;

    if-ne p5, v1, :cond_60

    .line 499
    aget v1, v0, v7

    const v2, 0x3f4ccccd

    mul-float/2addr v1, v2

    iput v1, p0, LF/t;->f:F

    .line 503
    :goto_4d
    aget v1, v0, v5

    iput v1, p0, LF/t;->g:F

    .line 504
    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, LF/t;->h:F

    .line 505
    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, LF/t;->i:F

    .line 506
    return-void

    :cond_5c
    move v0, v7

    .line 486
    goto :goto_1d

    .line 494
    :cond_5e
    const/4 v3, 0x0

    goto :goto_39

    .line 501
    :cond_60
    aget v1, v0, v7

    iput v1, p0, LF/t;->f:F

    goto :goto_4d
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 509
    iget v0, p0, LF/t;->f:F

    return v0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 539
    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-static {v0, p2}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v7

    .line 540
    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-static {v0, p2}, LF/m;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    .line 541
    iget v0, p0, LF/t;->j:I

    if-eqz v0, :cond_1f

    .line 542
    const/4 v7, 0x0

    .line 547
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_19

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_1f

    .line 548
    :cond_19
    iget v0, p0, LF/t;->j:I

    invoke-static {v0}, LF/m;->c(I)I

    move-result v6

    .line 551
    :cond_1f
    iget-object v0, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LF/t;->b:Ljava/lang/String;

    iget-object v3, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v1, p0, LF/t;->d:Lo/aj;

    if-eqz v1, :cond_3a

    iget-object v1, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v1}, Lo/aj;->h()Lo/ao;

    move-result-object v4

    :goto_2f
    iget v1, p0, LF/t;->c:I

    int-to-float v5, v1

    iget v8, p0, LF/t;->j:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    return-object v0

    :cond_3a
    const/4 v4, 0x0

    goto :goto_2f
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .registers 10
    .parameter

    .prologue
    .line 560
    iget-object v0, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LF/t;->b:Ljava/lang/String;

    iget-object v2, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v3, p0, LF/t;->d:Lo/aj;

    if-eqz v3, :cond_26

    iget-object v3, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_10
    iget v4, p0, LF/t;->c:I

    int-to-float v4, v4

    iget-object v5, p0, LF/t;->d:Lo/aj;

    invoke-static {v5, p1}, LF/m;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v5

    iget-object v6, p0, LF/t;->d:Lo/aj;

    invoke-static {v6, p1}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    iget v7, p0, LF/t;->j:I

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    return-object v0

    :cond_26
    const/4 v3, 0x0

    goto :goto_10
.end method

.method public b()F
    .registers 2

    .prologue
    .line 511
    iget v0, p0, LF/t;->g:F

    return v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 514
    iget v0, p0, LF/t;->h:F

    return v0
.end method

.method public d()F
    .registers 2

    .prologue
    .line 516
    iget v0, p0, LF/t;->i:F

    return v0
.end method

.method public e()F
    .registers 3

    .prologue
    .line 531
    iget v0, p0, LF/t;->g:F

    iget v1, p0, LF/t;->h:F

    sub-float/2addr v0, v1

    iget v1, p0, LF/t;->i:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public f()V
    .registers 1

    .prologue
    .line 570
    return-void
.end method
