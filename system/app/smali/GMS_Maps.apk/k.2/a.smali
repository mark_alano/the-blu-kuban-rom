.class public Lk/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lg/a;


# instance fields
.field private final a:Lg/a;

.field private final b:Ln/e;

.field private final c:Lr/n;

.field private volatile d:Z

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Ljava/util/Set;

.field private h:Ljava/util/Set;

.field private i:Lo/r;

.field private j:Lo/E;

.field private final k:LR/h;

.field private final l:Ln/q;

.field private m:J


# direct methods
.method public constructor <init>(Lg/a;Ln/e;Lr/n;ILn/q;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lk/a;->d:Z

    .line 74
    iput-object v1, p0, Lk/a;->i:Lo/r;

    .line 80
    iput-object v1, p0, Lk/a;->j:Lo/E;

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lk/a;->m:J

    .line 106
    iput-object p1, p0, Lk/a;->a:Lg/a;

    .line 107
    iput-object p2, p0, Lk/a;->b:Ln/e;

    .line 108
    iput-object p3, p0, Lk/a;->c:Lr/n;

    .line 109
    new-instance v0, LR/h;

    invoke-direct {v0, p4}, LR/h;-><init>(I)V

    iput-object v0, p0, Lk/a;->k:LR/h;

    .line 110
    iput-object p5, p0, Lk/a;->l:Ln/q;

    .line 111
    return-void
.end method

.method private a(Ljava/util/List;LC/a;)Lo/aq;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 122
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 125
    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v2

    invoke-virtual {p2}, LC/a;->i()Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/ad;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 130
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method private f(LC/a;)V
    .registers 18
    .parameter

    .prologue
    .line 134
    move-object/from16 v0, p0

    iget-object v1, v0, Lk/a;->a:Lg/a;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v6

    .line 135
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lk/a;->d:Z

    if-nez v1, :cond_1d

    if-eqz v6, :cond_1d

    move-object/from16 v0, p0

    iget-object v1, v0, Lk/a;->e:Ljava/util/List;

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 218
    :goto_1c
    return-void

    .line 139
    :cond_1d
    move-object/from16 v0, p0

    iget-wide v1, v0, Lk/a;->m:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    move-object/from16 v0, p0

    iput-wide v1, v0, Lk/a;->m:J

    .line 143
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lk/a;->d:Z

    .line 149
    move-object/from16 v0, p0

    iget-object v1, v0, Lk/a;->l:Ln/q;

    invoke-virtual {v1}, Ln/q;->c()Lo/y;

    move-result-object v7

    .line 150
    if-eqz v7, :cond_105

    invoke-virtual {v7}, Lo/y;->a()Lo/r;

    move-result-object v1

    move-object v3, v1

    .line 151
    :goto_3c
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v8

    .line 152
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v9

    .line 153
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v10

    .line 155
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_4c
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_136

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/aq;

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lk/a;->k:LR/h;

    invoke-virtual {v2, v1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    .line 157
    if-nez v2, :cond_77

    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lk/a;->b:Ln/e;

    invoke-interface {v2, v1}, Ln/e;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v2

    .line 161
    sget-object v4, Ln/e;->a:Ljava/util/Collection;

    if-eq v2, v4, :cond_77

    .line 162
    move-object/from16 v0, p0

    iget-object v4, v0, Lk/a;->k:LR/h;

    invoke-virtual {v4, v1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 165
    :cond_77
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_7b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ln/a;

    .line 166
    invoke-virtual {v2}, Ln/a;->a()Lo/r;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Lk/a;->l:Ln/q;

    invoke-virtual {v2}, Ln/a;->a()Lo/r;

    move-result-object v5

    invoke-virtual {v4, v5}, Ln/q;->b(Lo/r;)Lo/E;

    move-result-object v4

    .line 169
    if-eqz v4, :cond_ab

    .line 170
    new-instance v5, Lo/aB;

    invoke-direct {v5}, Lo/aB;-><init>()V

    .line 171
    invoke-virtual {v5, v4}, Lo/aB;->a(Lo/at;)V

    .line 172
    invoke-virtual {v1, v5}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_ab
    monitor-enter p0

    .line 175
    :try_start_ac
    invoke-virtual {v2}, Ln/a;->a()Lo/r;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lk/a;->i:Lo/r;

    invoke-virtual {v5, v13}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_cd

    .line 176
    new-instance v5, Lo/aB;

    invoke-direct {v5}, Lo/aB;-><init>()V

    .line 177
    move-object/from16 v0, p0

    iget-object v13, v0, Lk/a;->j:Lo/E;

    invoke-virtual {v5, v13}, Lo/aB;->a(Lo/at;)V

    .line 178
    invoke-virtual {v1, v5}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_cd
    monitor-exit p0
    :try_end_ce
    .catchall {:try_start_ac .. :try_end_ce} :catchall_109

    .line 184
    if-eqz v4, :cond_7b

    invoke-virtual {v2}, Ln/a;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v2, v3}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7b

    .line 185
    invoke-virtual {v4}, Lo/E;->c()Lo/D;

    move-result-object v2

    invoke-virtual {v7, v2}, Lo/y;->b(Lo/D;)I

    move-result v13

    .line 186
    const/4 v2, -0x1

    if-eq v13, v2, :cond_7b

    .line 187
    invoke-virtual {v7}, Lo/y;->b()Ljava/util/List;

    move-result-object v14

    .line 188
    add-int/lit8 v2, v13, -0x1

    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 189
    add-int/lit8 v4, v13, 0x1

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v15

    move v5, v2

    .line 191
    :goto_fd
    if-ge v5, v15, :cond_7b

    .line 192
    if-ne v5, v13, :cond_10c

    .line 191
    :goto_101
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_fd

    .line 150
    :cond_105
    const/4 v1, 0x0

    move-object v3, v1

    goto/16 :goto_3c

    .line 180
    :catchall_109
    move-exception v1

    :try_start_10a
    monitor-exit p0
    :try_end_10b
    .catchall {:try_start_10a .. :try_end_10b} :catchall_109

    throw v1

    .line 195
    :cond_10c
    new-instance v4, Lo/aB;

    invoke-direct {v4}, Lo/aB;-><init>()V

    .line 196
    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/z;

    invoke-virtual {v2}, Lo/z;->a()Lo/D;

    move-result-object v2

    invoke-static {v2}, Lo/E;->a(Lo/D;)Lo/E;

    move-result-object v2

    invoke-virtual {v4, v2}, Lo/aB;->a(Lo/at;)V

    .line 198
    invoke-virtual {v1, v4}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v2

    .line 199
    invoke-virtual/range {p1 .. p1}, LC/a;->h()Lo/T;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lk/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v4

    .line 201
    if-nez v4, :cond_14c

    .line 204
    :goto_132
    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_101

    .line 214
    :cond_136
    move-object/from16 v0, p0

    iput-object v6, v0, Lk/a;->e:Ljava/util/List;

    .line 215
    invoke-static {v8}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lk/a;->f:Ljava/util/List;

    .line 216
    move-object/from16 v0, p0

    iput-object v9, v0, Lk/a;->g:Ljava/util/Set;

    .line 217
    move-object/from16 v0, p0

    iput-object v10, v0, Lk/a;->h:Ljava/util/Set;

    goto/16 :goto_1c

    :cond_14c
    move-object v2, v4

    goto :goto_132
.end method


# virtual methods
.method public a(Lo/T;)F
    .registers 3
    .parameter

    .prologue
    .line 336
    iget-object v0, p0, Lk/a;->a:Lg/a;

    invoke-interface {v0, p1}, Lg/a;->a(Lo/T;)F

    move-result v0

    return v0
.end method

.method public a()J
    .registers 3

    .prologue
    .line 228
    iget-wide v0, p0, Lk/a;->m:J

    return-wide v0
.end method

.method public a(ILo/T;)Ljava/util/List;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 342
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public a(LC/a;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lk/a;->f(LC/a;)V

    .line 223
    iget-object v0, p0, Lk/a;->f:Ljava/util/List;

    return-object v0
.end method

.method public a(Lo/aq;Lo/T;)Lo/aq;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lk/a;->a:Lg/a;

    invoke-interface {v0, p1, p2}, Lg/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(Lo/r;Lo/D;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 313
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lk/a;->i:Lo/r;

    .line 314
    new-instance v0, Lo/F;

    invoke-direct {v0}, Lo/F;-><init>()V

    invoke-virtual {v0, p2}, Lo/F;->a(Lo/D;)Lo/F;

    move-result-object v0

    invoke-virtual {v0}, Lo/F;->a()Lo/E;

    move-result-object v0

    iput-object v0, p0, Lk/a;->j:Lo/E;

    .line 315
    invoke-virtual {p0}, Lk/a;->b()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 316
    monitor-exit p0

    return-void

    .line 313
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LC/a;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lk/a;->f(LC/a;)V

    .line 237
    iget-object v0, p0, Lk/a;->g:Ljava/util/Set;

    return-object v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lk/a;->d:Z

    .line 306
    return-void
.end method

.method public c(LC/a;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lk/a;->f(LC/a;)V

    .line 246
    iget-object v0, p0, Lk/a;->h:Ljava/util/Set;

    return-object v0
.end method

.method public declared-synchronized c()V
    .registers 2

    .prologue
    .line 319
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lk/a;->i:Lo/r;

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lk/a;->j:Lo/E;

    .line 321
    invoke-virtual {p0}, Lk/a;->b()V
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_c

    .line 322
    monitor-exit p0

    return-void

    .line 319
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(LC/a;)D
    .registers 10
    .parameter

    .prologue
    const-wide/high16 v6, 0x4033

    .line 254
    const-wide/high16 v0, 0x4020

    .line 258
    invoke-virtual {p1}, LC/a;->s()F

    move-result v2

    float-to-double v2, v2

    .line 259
    cmpl-double v4, v2, v6

    if-lez v4, :cond_15

    .line 260
    const-wide/high16 v4, 0x4000

    sub-double/2addr v2, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 261
    div-double/2addr v0, v2

    .line 264
    :cond_15
    return-wide v0
.end method

.method public d()Ljava/util/List;
    .registers 2

    .prologue
    .line 350
    iget-object v0, p0, Lk/a;->f:Ljava/util/List;

    return-object v0
.end method

.method public e(LC/a;)Lo/r;
    .registers 6
    .parameter

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lk/a;->f(LC/a;)V

    .line 273
    iget-object v0, p0, Lk/a;->e:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lk/a;->a(Ljava/util/List;LC/a;)Lo/aq;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lk/a;->k:LR/h;

    invoke-virtual {v1, v0}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 276
    if-eqz v0, :cond_20

    .line 277
    invoke-virtual {p1}, LC/a;->i()Lo/T;

    move-result-object v1

    invoke-virtual {p0, p1}, Lk/a;->d(LC/a;)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ln/a;->a(Ljava/util/Collection;Lo/T;D)Lo/r;

    move-result-object v0

    .line 281
    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method
