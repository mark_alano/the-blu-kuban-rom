.class public Lk/b;
.super Lk/f;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/maps/driveabout/vector/bc;


# direct methods
.method public constructor <init>(LA/b;)V
    .registers 3
    .parameter

    .prologue
    .line 28
    sget-object v0, LA/c;->h:LA/c;

    invoke-direct {p0, v0, p1}, Lk/f;-><init>(LA/c;LA/b;)V

    .line 25
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bc;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/bc;-><init>()V

    iput-object v0, p0, Lk/b;->d:Lcom/google/android/maps/driveabout/vector/bc;

    .line 29
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 59
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_e

    move v0, v1

    .line 67
    :goto_d
    return v0

    :cond_e
    move v3, v2

    .line 62
    :goto_f
    if-ge v3, v4, :cond_27

    .line 63
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    move v0, v1

    .line 64
    goto :goto_d

    .line 62
    :cond_23
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_f

    :cond_27
    move v0, v2

    .line 67
    goto :goto_d
.end method


# virtual methods
.method public a(LC/a;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x10

    .line 43
    invoke-super {p0, p1}, Lk/f;->a(LC/a;)Ljava/util/List;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lk/b;->d:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-virtual {p1}, LC/a;->i()Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/bc;->a(Lo/T;)V

    .line 45
    iget-object v1, p0, Lk/b;->d:Lcom/google/android/maps/driveabout/vector/bc;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 46
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_1f

    .line 47
    const/4 v1, 0x0

    invoke-interface {v0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 49
    :cond_1f
    return-object v0
.end method
