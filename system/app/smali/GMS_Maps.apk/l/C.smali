.class public LL/C;
.super Ll/j;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 449
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LL/C;->b:Ljava/util/HashMap;

    .line 452
    sget-object v0, LL/C;->b:Ljava/util/HashMap;

    const-string v1, "layers"

    const-string v2, "clear"

    invoke-static {v2}, Lcom/google/common/collect/ImmutableSet;->b(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 456
    invoke-direct {p0}, Ll/j;-><init>()V

    .line 454
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LL/C;->c:Ljava/util/HashMap;

    .line 457
    iput-object p1, p0, LL/C;->a:Ljava/lang/String;

    .line 458
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Node;)V
    .registers 6
    .parameter

    .prologue
    .line 461
    invoke-direct {p0, p1}, Ll/j;-><init>(Lorg/w3c/dom/Node;)V

    .line 454
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LL/C;->c:Ljava/util/HashMap;

    .line 462
    const-string v0, "action"

    invoke-static {p1, v0}, LL/C;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LL/C;->a:Ljava/lang/String;

    .line 463
    sget-object v0, LL/C;->b:Ljava/util/HashMap;

    iget-object v1, p0, LL/C;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 464
    if-eqz v0, :cond_38

    .line 465
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 466
    iget-object v2, p0, LL/C;->c:Ljava/util/HashMap;

    invoke-static {p1, v0}, LL/C;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_22

    .line 469
    :cond_38
    return-void
.end method

.method public static b(Lorg/w3c/dom/Node;)LL/C;
    .registers 2
    .parameter

    .prologue
    .line 473
    const-string v0, "param2"

    invoke-static {p0, v0}, LL/C;->b(Lorg/w3c/dom/Node;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 474
    new-instance v0, LL/B;

    invoke-direct {v0, p0}, LL/B;-><init>(Lorg/w3c/dom/Node;)V

    .line 478
    :goto_d
    return-object v0

    .line 475
    :cond_e
    const-string v0, "param"

    invoke-static {p0, v0}, LL/C;->b(Lorg/w3c/dom/Node;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 476
    new-instance v0, LL/D;

    invoke-direct {v0, p0}, LL/D;-><init>(Lorg/w3c/dom/Node;)V

    goto :goto_d

    .line 478
    :cond_1c
    new-instance v0, LL/C;

    invoke-direct {v0, p0}, LL/C;-><init>(Lorg/w3c/dom/Node;)V

    goto :goto_d
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 501
    iget-object v0, p0, LL/C;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected a(Ljava/io/Writer;)V
    .registers 4
    .parameter

    .prologue
    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<ui action=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/C;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p0, p1}, LL/C;->c(Ljava/io/Writer;)V

    .line 493
    const-string v0, "/>"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 494
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    .line 505
    const-string v0, "true"

    invoke-virtual {p0, p1}, LL/C;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected c(Ljava/io/Writer;)V
    .registers 2
    .parameter

    .prologue
    .line 487
    return-void
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 509
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 497
    iget-object v0, p0, LL/C;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 519
    const-string v0, "true"

    invoke-virtual {p0}, LL/C;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
