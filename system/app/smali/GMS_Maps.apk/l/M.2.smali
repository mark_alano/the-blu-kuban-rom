.class public LL/M;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/maps/driveabout/app/dM;

.field private final c:LQ/p;

.field private final d:Ljava/lang/Runnable;

.field private final e:Ljava/util/Random;

.field private final f:J

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 39
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NONE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BACK_TO_CAR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LIST_VIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SATELLITE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ROUTE_OVERVIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TRAFFIC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "STREET_VIEW"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ALTERNATE_ROUTES"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ROUTE_AROUND_TRAFFIC_OVERVIEW"

    aput-object v2, v0, v1

    sput-object v0, LL/M;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/app/dM;JII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 70
    iput-object p1, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/dM;->a()LQ/p;

    move-result-object v1

    iput-object v1, p0, LL/M;->c:LQ/p;

    .line 72
    iput-wide p2, p0, LL/M;->f:J

    .line 73
    new-instance v1, Ljava/util/Random;

    iget-wide v2, p0, LL/M;->f:J

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v1, p0, LL/M;->e:Ljava/util/Random;

    .line 74
    new-instance v1, LL/N;

    invoke-direct {v1, p0, v0, p5}, LL/N;-><init>(LL/M;Landroid/os/Handler;I)V

    iput-object v1, p0, LL/M;->d:Ljava/lang/Runnable;

    .line 82
    iget-object v1, p0, LL/M;->d:Ljava/lang/Runnable;

    mul-int/lit16 v2, p4, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    return-void
.end method

.method private a()I
    .registers 8

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x6

    .line 87
    iget-object v1, p0, LL/M;->c:LQ/p;

    invoke-virtual {v1}, LQ/p;->g()LQ/s;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, LQ/s;->G()LQ/x;

    move-result-object v2

    .line 91
    instance-of v1, v1, LQ/E;

    if-nez v1, :cond_1f

    iget-object v1, p0, LL/M;->e:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const/high16 v3, 0x3f00

    cmpg-float v1, v1, v3

    if-gez v1, :cond_1f

    .line 161
    :goto_1e
    return v0

    .line 98
    :cond_1f
    sget-object v1, LQ/x;->i:LQ/x;

    if-ne v2, v1, :cond_32

    .line 99
    new-array v0, v4, [I

    fill-array-data v0, :array_80

    .line 161
    :goto_28
    iget-object v1, p0, LL/M;->e:Ljava/util/Random;

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget v0, v0, v1

    goto :goto_1e

    .line 107
    :cond_32
    sget-object v1, LQ/x;->l:LQ/x;

    if-ne v2, v1, :cond_3c

    .line 108
    new-array v0, v6, [I

    fill-array-data v0, :array_90

    goto :goto_28

    .line 113
    :cond_3c
    sget-object v1, LQ/x;->k:LQ/x;

    if-ne v2, v1, :cond_46

    .line 114
    new-array v0, v4, [I

    fill-array-data v0, :array_9a

    goto :goto_28

    .line 122
    :cond_46
    sget-object v1, LQ/x;->m:LQ/x;

    if-ne v2, v1, :cond_50

    .line 123
    new-array v0, v6, [I

    fill-array-data v0, :array_aa

    goto :goto_28

    .line 128
    :cond_50
    sget-object v1, LQ/x;->j:LQ/x;

    if-ne v2, v1, :cond_5b

    .line 129
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_b4

    goto :goto_28

    .line 136
    :cond_5b
    sget-object v1, LQ/x;->f:LQ/x;

    if-ne v2, v1, :cond_66

    .line 137
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_c2

    goto :goto_28

    .line 143
    :cond_66
    sget-object v1, LQ/x;->h:LQ/x;

    if-ne v2, v1, :cond_71

    .line 144
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_ce

    goto :goto_28

    .line 148
    :cond_71
    sget-object v1, LQ/x;->g:LQ/x;

    if-ne v2, v1, :cond_7b

    .line 149
    new-array v0, v4, [I

    fill-array-data v0, :array_d6

    goto :goto_28

    .line 158
    :cond_7b
    new-array v0, v0, [I

    aput v5, v0, v5

    goto :goto_28

    .line 99
    :array_80
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 108
    :array_90
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 114
    :array_9a
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 123
    :array_aa
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 129
    :array_b4
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 137
    :array_c2
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 144
    :array_ce
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 149
    :array_d6
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic a(LL/M;)I
    .registers 2
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, LL/M;->a()I

    move-result v0

    return v0
.end method

.method private a(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 169
    packed-switch p1, :pswitch_data_54

    .line 197
    :goto_5
    return-void

    .line 171
    :pswitch_6
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->h()V

    goto :goto_5

    .line 174
    :pswitch_c
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->e()V

    goto :goto_5

    .line 177
    :pswitch_12
    iget-boolean v2, p0, LL/M;->g:Z

    if-nez v2, :cond_20

    :goto_16
    iput-boolean v0, p0, LL/M;->g:Z

    .line 178
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    iget-boolean v1, p0, LL/M;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dM;->c(Z)V

    goto :goto_5

    :cond_20
    move v0, v1

    .line 177
    goto :goto_16

    .line 181
    :pswitch_22
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->j()V

    goto :goto_5

    .line 184
    :pswitch_28
    iget-object v2, p0, LL/M;->c:LQ/p;

    invoke-virtual {v2}, LQ/p;->g()LQ/s;

    move-result-object v2

    iget-object v3, p0, LL/M;->c:LQ/p;

    sget-object v4, LQ/x;->l:LQ/x;

    invoke-virtual {v3, v4}, LQ/p;->b(LQ/x;)Z

    move-result v3

    if-nez v3, :cond_3c

    :goto_38
    invoke-virtual {v2, v0}, LQ/s;->d(Z)V

    goto :goto_5

    :cond_3c
    move v0, v1

    goto :goto_38

    .line 188
    :pswitch_3e
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->f()V

    goto :goto_5

    .line 191
    :pswitch_44
    iget-object v0, p0, LL/M;->c:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    goto :goto_5

    .line 194
    :pswitch_4e
    iget-object v0, p0, LL/M;->b:Lcom/google/android/maps/driveabout/app/dM;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->k()V

    goto :goto_5

    .line 169
    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_6
        :pswitch_c
        :pswitch_12
        :pswitch_22
        :pswitch_28
        :pswitch_3e
        :pswitch_44
        :pswitch_4e
    .end packed-switch
.end method

.method static synthetic a(LL/M;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1}, LL/M;->a(I)V

    return-void
.end method

.method static synthetic b(LL/M;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LL/M;->d:Ljava/lang/Runnable;

    return-object v0
.end method
