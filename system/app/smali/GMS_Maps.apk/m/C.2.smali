.class public LM/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(LM/C;)V
    .registers 4
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iget-object v0, p1, LM/C;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, LM/C;->a(Ljava/lang/String;)V

    .line 28
    iget-wide v0, p1, LM/C;->b:J

    invoke-virtual {p0, v0, v1}, LM/C;->a(J)V

    .line 29
    iget v0, p1, LM/C;->c:F

    invoke-virtual {p0, v0}, LM/C;->a(F)V

    .line 30
    iget v0, p1, LM/C;->d:F

    invoke-virtual {p0, v0}, LM/C;->b(F)V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JFF)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0, p1}, LM/C;->a(Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0, p2, p3}, LM/C;->a(J)V

    .line 21
    invoke-virtual {p0, p4}, LM/C;->a(F)V

    .line 22
    invoke-virtual {p0, p5}, LM/C;->b(F)V

    .line 23
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, LM/C;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 69
    invoke-static {p1}, Lcom/google/googlenav/common/util/j;->a(F)F

    move-result v0

    iput v0, p0, LM/C;->c:F

    .line 70
    return-void
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 55
    iput-wide p1, p0, LM/C;->b:J

    .line 56
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, LM/C;->a:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public b()J
    .registers 3

    .prologue
    .line 61
    iget-wide v0, p0, LM/C;->b:J

    return-wide v0
.end method

.method public b(F)V
    .registers 2
    .parameter

    .prologue
    .line 84
    iput p1, p0, LM/C;->d:F

    .line 85
    return-void
.end method

.method public c()F
    .registers 2

    .prologue
    .line 76
    iget v0, p0, LM/C;->c:F

    return v0
.end method

.method public d()F
    .registers 2

    .prologue
    .line 93
    iget v0, p0, LM/C;->d:F

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[provider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LM/C;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/C;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " orientation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LM/C;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
