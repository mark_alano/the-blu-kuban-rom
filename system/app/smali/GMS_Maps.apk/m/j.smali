.class LM/j;
.super LR/c;
.source "SourceFile"

# interfaces
.implements LM/b;


# instance fields
.field private a:J

.field private b:Ljava/lang/Runnable;

.field private c:I

.field private d:Landroid/os/Handler;

.field private e:Landroid/os/Looper;

.field private final f:Ljava/util/LinkedList;

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private i:Z

.field private j:I

.field private k:LM/C;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 66
    const-string v0, "LocationDispatcher"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LM/j;->a:J

    .line 42
    iput v2, p0, LM/j;->c:I

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LM/j;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 56
    iput-boolean v2, p0, LM/j;->i:Z

    .line 67
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    .line 69
    invoke-virtual {p0}, LM/j;->a()V

    .line 70
    return-void
.end method

.method static synthetic a(LM/j;)I
    .registers 2
    .parameter

    .prologue
    .line 37
    iget v0, p0, LM/j;->c:I

    return v0
.end method

.method static synthetic a(LM/j;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput p1, p0, LM/j;->c:I

    return p1
.end method

.method static synthetic a(LM/j;Landroid/os/Message;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, LM/j;->a(Landroid/os/Message;)V

    return-void
.end method

.method private a(LM/m;)V
    .registers 4
    .parameter

    .prologue
    .line 356
    invoke-direct {p0}, LM/j;->g()V

    .line 357
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 358
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 359
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {p1}, LM/m;->b()Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 360
    return-void
.end method

.method private a(Landroid/location/Location;)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 374
    invoke-direct {p0}, LM/j;->g()V

    .line 375
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 377
    iget-object v0, p0, LM/j;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 378
    if-lez v0, :cond_23

    iget-boolean v0, p0, LM/j;->i:Z

    if-eqz v0, :cond_23

    .line 379
    iget v0, p0, LM/j;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LM/j;->j:I

    .line 406
    :cond_22
    return-void

    .line 382
    :cond_23
    iget v0, p0, LM/j;->j:I

    if-lez v0, :cond_2a

    .line 384
    const/4 v0, 0x0

    iput v0, p0, LM/j;->j:I

    .line 389
    :cond_2a
    iget v0, p0, LM/j;->c:I

    if-eq v0, v2, :cond_36

    .line 390
    iput v2, p0, LM/j;->c:I

    .line 391
    iget v0, p0, LM/j;->c:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LM/j;->a(ILandroid/os/Bundle;)V

    .line 401
    :cond_36
    :goto_36
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3c
    :goto_3c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/m;

    .line 402
    invoke-virtual {v0}, LM/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 403
    invoke-virtual {v0}, LM/m;->b()Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_3c

    .line 393
    :cond_5e
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "driveabout_base_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_76

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "da_tunnel_heartbeat"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    :cond_76
    move-object v0, p1

    .line 395
    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->g()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 397
    invoke-virtual {p0}, LM/j;->d()V

    .line 398
    invoke-direct {p0}, LM/j;->e()V

    goto :goto_36
.end method

.method private a(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    .line 317
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_44

    .line 343
    :goto_5
    return-void

    .line 319
    :pswitch_6
    invoke-direct {p0}, LM/j;->f()V

    goto :goto_5

    .line 322
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LM/m;

    invoke-direct {p0, v0}, LM/j;->a(LM/m;)V

    goto :goto_5

    .line 325
    :pswitch_12
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LM/m;

    invoke-direct {p0, v0}, LM/j;->b(LM/m;)V

    goto :goto_5

    .line 328
    :pswitch_1a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-direct {p0, v0}, LM/j;->a(Landroid/location/Location;)V

    goto :goto_5

    .line 331
    :pswitch_22
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LM/j;->a(Ljava/lang/String;)V

    goto :goto_5

    .line 334
    :pswitch_2a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LM/j;->b(Ljava/lang/String;)V

    goto :goto_5

    .line 337
    :pswitch_32
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, LM/j;->a(Ljava/lang/String;I)V

    goto :goto_5

    .line 340
    :pswitch_3c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LM/C;

    invoke-direct {p0, v0}, LM/j;->b(LM/C;)V

    goto :goto_5

    .line 317
    :pswitch_data_44
    .packed-switch 0xa
        :pswitch_6
        :pswitch_a
        :pswitch_12
        :pswitch_1a
        :pswitch_22
        :pswitch_2a
        :pswitch_32
        :pswitch_3c
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 409
    invoke-direct {p0}, LM/j;->g()V

    .line 410
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationListener;

    .line 411
    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    goto :goto_9

    .line 413
    :cond_19
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 423
    invoke-direct {p0}, LM/j;->g()V

    .line 424
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationListener;

    .line 425
    const/4 v2, 0x0

    invoke-interface {v0, p1, p2, v2}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_9

    .line 427
    :cond_1a
    return-void
.end method

.method private b(LM/C;)V
    .registers 6
    .parameter

    .prologue
    .line 430
    invoke-virtual {p1}, LM/C;->a()Ljava/lang/String;

    move-result-object v1

    .line 431
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/m;

    .line 432
    invoke-virtual {v0}, LM/m;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 433
    invoke-static {v0}, LM/m;->a(LM/m;)LM/b;

    move-result-object v0

    invoke-interface {v0, p1}, LM/b;->a(LM/C;)V

    goto :goto_a

    .line 436
    :cond_28
    return-void
.end method

.method private b(LM/m;)V
    .registers 5
    .parameter

    .prologue
    .line 363
    invoke-direct {p0}, LM/j;->g()V

    .line 364
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/m;

    .line 366
    invoke-virtual {v0}, LM/m;->b()Landroid/location/LocationListener;

    move-result-object v0

    invoke-virtual {p1}, LM/m;->b()Landroid/location/LocationListener;

    move-result-object v2

    if-ne v0, v2, :cond_e

    .line 371
    :goto_24
    return-void

    .line 370
    :cond_25
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {p1}, LM/m;->b()Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_24
.end method

.method private b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 416
    invoke-direct {p0}, LM/j;->g()V

    .line 417
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationListener;

    .line 418
    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    goto :goto_9

    .line 420
    :cond_19
    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    .line 278
    iget-object v0, p0, LM/j;->b:Ljava/lang/Runnable;

    if-nez v0, :cond_b

    .line 279
    new-instance v0, LM/l;

    invoke-direct {v0, p0}, LM/l;-><init>(LM/j;)V

    iput-object v0, p0, LM/j;->b:Ljava/lang/Runnable;

    .line 288
    :cond_b
    iget-wide v0, p0, LM/j;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1d

    iget-wide v0, p0, LM/j;->a:J

    .line 291
    :goto_15
    iget-object v2, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v3, p0, LM/j;->b:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 292
    return-void

    .line 288
    :cond_1d
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->k()I

    move-result v0

    int-to-long v0, v0

    goto :goto_15
.end method

.method private f()V
    .registers 2

    .prologue
    .line 346
    invoke-direct {p0}, LM/j;->g()V

    .line 347
    iget-object v0, p0, LM/j;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 348
    iget-object v0, p0, LM/j;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 349
    iget-object v0, p0, LM/j;->e:Landroid/os/Looper;

    if-eqz v0, :cond_19

    .line 350
    iget-object v0, p0, LM/j;->e:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, LM/j;->e:Landroid/os/Looper;

    .line 353
    :cond_19
    return-void
.end method

.method private final g()V
    .registers 4

    .prologue
    .line 443
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    if-eqz v0, :cond_31

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_31

    .line 444
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Operation must be called on location thread. Called on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_31
    return-void
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 451
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    if-eqz v0, :cond_a

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-ne v0, p0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 73
    invoke-virtual {p0}, LM/j;->start()V

    .line 76
    monitor-enter p0

    .line 78
    :goto_4
    :try_start_4
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    if-nez v0, :cond_d

    .line 79
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_f
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_b} :catch_c

    goto :goto_4

    .line 81
    :catch_c
    move-exception v0

    .line 84
    :cond_d
    :try_start_d
    monitor-exit p0

    .line 85
    return-void

    .line 84
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_d .. :try_end_11} :catchall_f

    throw v0
.end method

.method public a(ILandroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 238
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 239
    const-string v0, "gps"

    invoke-direct {p0, v0, p1}, LM/j;->a(Ljava/lang/String;I)V

    .line 245
    :goto_b
    return-void

    .line 241
    :cond_c
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v1, 0x10

    const/4 v2, 0x0

    const-string v3, "gps"

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 243
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_b
.end method

.method public a(LM/C;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x11

    .line 216
    invoke-virtual {p1}, LM/C;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "driveabout_base_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-boolean v0, p0, LM/j;->i:Z

    if-eqz v0, :cond_1b

    .line 224
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v1, p0, LM/j;->k:LM/C;

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 225
    iput-object p1, p0, LM/j;->k:LM/C;

    .line 227
    :cond_1b
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 228
    invoke-direct {p0, p1}, LM/j;->b(LM/C;)V

    .line 233
    :goto_24
    return-void

    .line 230
    :cond_25
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 231
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_24
.end method

.method public a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    if-eqz v0, :cond_a

    .line 140
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 144
    :goto_9
    return-void

    .line 142
    :cond_a
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_9
.end method

.method public a(Ljava/lang/String;LM/b;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 109
    new-instance v0, LM/m;

    invoke-direct {v0, p1, p2}, LM/m;-><init>(Ljava/lang/String;LM/b;)V

    .line 110
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v2, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v3, 0xb

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 112
    return-void
.end method

.method public b()V
    .registers 4

    .prologue
    .line 93
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 96
    :try_start_d
    invoke-virtual {p0}, LM/j;->join()V
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_10} :catch_11

    .line 100
    :goto_10
    return-void

    .line 97
    :catch_11
    move-exception v0

    goto :goto_10
.end method

.method public b(Ljava/lang/String;LM/b;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 121
    new-instance v0, LM/m;

    invoke-direct {v0, p1, p2}, LM/m;-><init>(Ljava/lang/String;LM/b;)V

    .line 122
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v2, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 124
    return-void
.end method

.method public c()Landroid/os/Handler;
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    return-object v0
.end method

.method public d()V
    .registers 3

    .prologue
    .line 269
    iget-object v0, p0, LM/j;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 270
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    iget-object v1, p0, LM/j;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 272
    :cond_b
    return-void
.end method

.method public l()V
    .registers 2

    .prologue
    .line 249
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 250
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LM/j;->e:Landroid/os/Looper;

    .line 251
    new-instance v0, LM/k;

    invoke-direct {v0, p0}, LM/k;-><init>(LM/j;)V

    iput-object v0, p0, LM/j;->d:Landroid/os/Handler;

    .line 259
    monitor-enter p0

    .line 260
    :try_start_11
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 261
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_11 .. :try_end_15} :catchall_19

    .line 262
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 263
    return-void

    .line 261
    :catchall_19
    move-exception v0

    :try_start_1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    throw v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 7
    .parameter

    .prologue
    const-wide v3, 0x3eb0c6f7a0b5ed8dL

    .line 150
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 151
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 154
    iget-object v1, p0, LM/j;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 159
    :cond_16
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, "network"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 161
    :cond_26
    invoke-static {p1}, LR/e;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    .line 162
    invoke-static {v0}, LaH/E;->e(LaN/B;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 163
    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/E;->d(LaN/B;)LaN/B;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-virtual {p1, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 167
    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 171
    :cond_4a
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 172
    invoke-direct {p0, p1}, LM/j;->a(Landroid/location/Location;)V

    .line 177
    :goto_53
    return-void

    .line 174
    :cond_54
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 175
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_53
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 181
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 182
    invoke-direct {p0, p1}, LM/j;->a(Ljava/lang/String;)V

    .line 187
    :goto_9
    return-void

    .line 184
    :cond_a
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 185
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_9
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 191
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 192
    invoke-direct {p0, p1}, LM/j;->b(Ljava/lang/String;)V

    .line 197
    :goto_9
    return-void

    .line 194
    :cond_a
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 195
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_9
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 203
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 204
    invoke-direct {p0}, LM/j;->h()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 205
    invoke-direct {p0, p1, p2}, LM/j;->a(Ljava/lang/String;I)V

    .line 212
    :cond_11
    :goto_11
    return-void

    .line 207
    :cond_12
    iget-object v0, p0, LM/j;->d:Landroid/os/Handler;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 209
    iget-object v1, p0, LM/j;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_11
.end method
