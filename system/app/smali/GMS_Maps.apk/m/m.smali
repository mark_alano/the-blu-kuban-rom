.class LM/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LM/b;


# direct methods
.method constructor <init>(Ljava/lang/String;LM/b;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470
    iput-object p1, p0, LM/m;->a:Ljava/lang/String;

    .line 471
    iput-object p2, p0, LM/m;->b:LM/b;

    .line 472
    return-void
.end method

.method static synthetic a(LM/m;)LM/b;
    .registers 2
    .parameter

    .prologue
    .line 465
    iget-object v0, p0, LM/m;->b:LM/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, LM/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Landroid/location/LocationListener;
    .registers 2

    .prologue
    .line 479
    iget-object v0, p0, LM/m;->b:LM/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 484
    instance-of v1, p1, LM/m;

    if-eqz v1, :cond_1c

    .line 485
    check-cast p1, LM/m;

    .line 486
    iget-object v1, p0, LM/m;->a:Ljava/lang/String;

    iget-object v2, p1, LM/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, LM/m;->b:LM/b;

    iget-object v2, p1, LM/m;->b:LM/b;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 489
    :cond_1c
    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 494
    iget-object v0, p0, LM/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, LM/m;->b:LM/b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
