.class public Ln/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/e;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Z

.field private d:Z

.field private volatile e:Ln/e;

.field private volatile f:Ln/e;

.field private final g:Ljava/util/List;

.field private final h:Ln/i;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ln/g;->b:Ljava/lang/Object;

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Ln/g;->c:Z

    .line 51
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ln/g;->g:Ljava/util/List;

    .line 155
    new-instance v0, Ln/h;

    invoke-direct {v0, p0}, Ln/h;-><init>(Ln/g;)V

    iput-object v0, p0, Ln/g;->h:Ln/i;

    .line 170
    invoke-direct {p0}, Ln/g;->b()Z

    .line 171
    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80
    .line 82
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->F()Z

    move-result v3

    if-eqz v3, :cond_33

    move v2, v1

    .line 92
    :goto_e
    if-eqz v1, :cond_31

    .line 93
    iget-object v1, p0, Ln/g;->h:Ln/i;

    invoke-interface {v1}, Ln/i;->a()Ln/e;

    move-result-object v1

    .line 97
    :goto_16
    if-eqz v2, :cond_20

    .line 98
    iget-object v0, p0, Ln/g;->h:Ln/i;

    const-string v2, "/new.building.list"

    invoke-interface {v0, v2}, Ln/i;->a(Ljava/lang/String;)Ln/e;

    move-result-object v0

    .line 101
    :cond_20
    if-nez v0, :cond_2c

    if-nez v1, :cond_2c

    .line 102
    iget-object v0, p0, Ln/g;->h:Ln/i;

    const-string v2, "/building.list"

    invoke-interface {v0, v2}, Ln/i;->a(Ljava/lang/String;)Ln/e;

    move-result-object v0

    .line 106
    :cond_2c
    iput-object v0, p0, Ln/g;->f:Ln/e;

    .line 107
    iput-object v1, p0, Ln/g;->e:Ln/e;

    .line 108
    return-void

    :cond_31
    move-object v1, v0

    goto :goto_16

    :cond_33
    move v1, v2

    goto :goto_e
.end method

.method private b(Ln/f;)V
    .registers 3
    .parameter

    .prologue
    .line 242
    iget-object v0, p0, Ln/g;->f:Ln/e;

    if-eqz v0, :cond_9

    .line 243
    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Ln/f;)V

    .line 245
    :cond_9
    iget-object v0, p0, Ln/g;->e:Ln/e;

    if-eqz v0, :cond_12

    .line 246
    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Ln/f;)V

    .line 248
    :cond_12
    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 114
    .line 118
    iget-object v3, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v3

    .line 119
    :try_start_5
    iget-boolean v2, p0, Ln/g;->d:Z

    if-eqz v2, :cond_c

    .line 120
    monitor-exit v3

    move v0, v1

    .line 148
    :cond_b
    :goto_b
    return v0

    .line 123
    :cond_c
    iget-boolean v2, p0, Ln/g;->c:Z

    if-eqz v2, :cond_4a

    invoke-static {}, LR/o;->c()Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 124
    const/4 v2, 0x0

    iput-boolean v2, p0, Ln/g;->c:Z

    move v2, v1

    .line 127
    :goto_1a
    monitor-exit v3
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_3f

    .line 129
    if-eqz v2, :cond_b

    .line 135
    invoke-direct {p0}, Ln/g;->a()V

    .line 137
    iget-object v2, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 138
    const/4 v0, 0x1

    :try_start_24
    iput-boolean v0, p0, Ln/g;->d:Z

    .line 142
    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/f;

    .line 143
    invoke-direct {p0, v0}, Ln/g;->b(Ln/f;)V

    goto :goto_2c

    .line 146
    :catchall_3c
    move-exception v0

    monitor-exit v2
    :try_end_3e
    .catchall {:try_start_24 .. :try_end_3e} :catchall_3c

    throw v0

    .line 127
    :catchall_3f
    move-exception v0

    :try_start_40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_40 .. :try_end_41} :catchall_3f

    throw v0

    .line 145
    :cond_42
    :try_start_42
    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 146
    monitor-exit v2
    :try_end_48
    .catchall {:try_start_42 .. :try_end_48} :catchall_3c

    move v0, v1

    .line 148
    goto :goto_b

    :cond_4a
    move v2, v0

    goto :goto_1a
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .registers 7
    .parameter

    .prologue
    .line 181
    invoke-direct {p0}, Ln/g;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 182
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    .line 221
    :goto_8
    return-object v0

    .line 185
    :cond_9
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 186
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    .line 188
    iget-object v1, p0, Ln/g;->f:Ln/e;

    if-eqz v1, :cond_83

    .line 189
    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    .line 192
    :goto_1c
    iget-object v0, p0, Ln/g;->e:Ln/e;

    if-eqz v0, :cond_27

    .line 193
    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    move-object v2, v0

    .line 197
    :cond_27
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    if-eq v1, v0, :cond_2f

    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    if-ne v2, v0, :cond_32

    .line 199
    :cond_2f
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    goto :goto_8

    .line 203
    :cond_32
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 204
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_8

    .line 209
    :cond_43
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 210
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    .line 211
    invoke-virtual {v0}, Ln/a;->a()Lo/r;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4b

    .line 214
    :cond_5f
    invoke-static {v1}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 215
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_67
    :goto_67
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_81

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    .line 216
    invoke-virtual {v0}, Ln/a;->a()Lo/r;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_67

    .line 217
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_67

    :cond_81
    move-object v0, v1

    .line 221
    goto :goto_8

    :cond_83
    move-object v1, v0

    goto :goto_1c
.end method

.method public a(Ln/f;)V
    .registers 4
    .parameter

    .prologue
    .line 261
    invoke-direct {p0}, Ln/g;->b()Z

    .line 262
    iget-object v1, p0, Ln/g;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 263
    :try_start_6
    iget-boolean v0, p0, Ln/g;->d:Z

    if-nez v0, :cond_11

    .line 264
    iget-object v0, p0, Ln/g;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    monitor-exit v1

    .line 269
    :goto_10
    return-void

    .line 267
    :cond_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_16

    .line 268
    invoke-direct {p0, p1}, Ln/g;->b(Ln/f;)V

    goto :goto_10

    .line 267
    :catchall_16
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public a(Lo/o;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 226
    invoke-direct {p0}, Ln/g;->b()Z

    move-result v1

    if-nez v1, :cond_8

    .line 238
    :cond_7
    :goto_7
    return v0

    .line 232
    :cond_8
    iget-object v1, p0, Ln/g;->f:Ln/e;

    if-eqz v1, :cond_12

    .line 233
    iget-object v0, p0, Ln/g;->f:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/o;)Z

    move-result v0

    .line 235
    :cond_12
    if-nez v0, :cond_7

    iget-object v1, p0, Ln/g;->e:Ln/e;

    if-eqz v1, :cond_7

    .line 236
    iget-object v0, p0, Ln/g;->e:Ln/e;

    invoke-interface {v0, p1}, Ln/e;->a(Lo/o;)Z

    move-result v0

    goto :goto_7
.end method
