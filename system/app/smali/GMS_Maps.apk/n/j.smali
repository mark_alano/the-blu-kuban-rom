.class public Ln/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/e;


# instance fields
.field private final b:Lcom/google/common/collect/cV;

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-static {}, Lcom/google/common/collect/aG;->e()Lcom/google/common/collect/aG;

    move-result-object v0

    iput-object v0, p0, Ln/j;->b:Lcom/google/common/collect/cV;

    .line 91
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Ln/j;->c:Ljava/util/Set;

    .line 92
    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;Lo/aR;)V
    .registers 18
    .parameter
    .parameter

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/google/common/collect/aG;->f()Lcom/google/common/collect/aH;

    move-result-object v5

    .line 103
    const/4 v2, 0x0

    .line 104
    const/4 v1, 0x0

    .line 105
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v6

    .line 109
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v2

    move v2, v1

    :cond_13
    :goto_13
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_88

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ln/a;

    .line 110
    invoke-virtual {v1}, Ln/a;->b()Lo/ad;

    move-result-object v3

    invoke-static {v3}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v8

    .line 111
    invoke-virtual {v8}, Lo/aR;->e()I

    move-result v3

    int-to-double v9, v3

    invoke-virtual {v8}, Lo/aR;->g()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v11

    div-double/2addr v9, v11

    .line 112
    invoke-virtual {v8}, Lo/aR;->f()I

    move-result v3

    int-to-double v11, v3

    invoke-virtual {v8}, Lo/aR;->g()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v13

    div-double/2addr v11, v13

    .line 113
    const-wide v13, 0x40bb580000000000L

    cmpl-double v3, v9, v13

    if-gez v3, :cond_55

    const-wide v9, 0x40bb580000000000L

    cmpl-double v3, v11, v9

    if-ltz v3, :cond_59

    .line 116
    :cond_55
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    .line 117
    goto :goto_13

    .line 119
    :cond_59
    invoke-virtual {v1, v6}, Ln/a;->a(Ljava/util/Set;)V

    .line 121
    if-eqz p2, :cond_6a

    invoke-virtual {v1}, Ln/a;->b()Lo/ad;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lo/aR;->b(Lo/ae;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 124
    :cond_6a
    add-int/lit8 v3, v2, 0x1

    .line 125
    const/16 v2, 0xf

    invoke-static {v8, v2}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_76
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_86

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/aq;

    .line 126
    invoke-virtual {v5, v2, v1}, Lcom/google/common/collect/aH;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aH;

    goto :goto_76

    :cond_86
    move v2, v3

    .line 128
    goto :goto_13

    .line 129
    :cond_88
    invoke-static {v6}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    iput-object v1, p0, Ln/j;->c:Ljava/util/Set;

    .line 130
    invoke-virtual {v5}, Lcom/google/common/collect/aH;->a()Lcom/google/common/collect/aG;

    move-result-object v1

    iput-object v1, p0, Ln/j;->b:Lcom/google/common/collect/cV;

    .line 133
    return-void
.end method

.method static a(Ljava/io/Reader;)Ljava/util/List;
    .registers 4
    .parameter

    .prologue
    .line 76
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 77
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v2

    .line 79
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_d
    if-eqz v0, :cond_1d

    .line 80
    invoke-static {v0}, Ln/a;->a(Ljava/lang/String;)Ln/a;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_18

    .line 83
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_18
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    .line 86
    :cond_1d
    return-object v2
.end method

.method public static a(Ljava/io/Reader;Lo/aR;)Ln/j;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-static {p0}, Ln/j;->a(Ljava/io/Reader;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Ln/j;->a(Ljava/util/Collection;Lo/aR;)Ln/j;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Collection;Lo/aR;)Ln/j;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 71
    new-instance v0, Ln/j;

    invoke-direct {v0, p0, p1}, Ln/j;-><init>(Ljava/util/Collection;Lo/aR;)V

    return-object v0
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0xf

    .line 139
    invoke-virtual {p1}, Lo/aq;->a()Lo/aq;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v1

    .line 142
    if-ge v1, v2, :cond_11

    .line 143
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 148
    :goto_10
    return-object v0

    .line 144
    :cond_11
    if-ne v1, v2, :cond_1a

    .line 145
    iget-object v1, p0, Ln/j;->b:Lcom/google/common/collect/cV;

    invoke-interface {v1, v0}, Lcom/google/common/collect/cV;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_10

    .line 147
    :cond_1a
    invoke-virtual {v0, v2}, Lo/aq;->a(I)Lo/aq;

    move-result-object v1

    .line 148
    iget-object v2, p0, Ln/j;->b:Lcom/google/common/collect/cV;

    invoke-interface {v2, v1}, Lcom/google/common/collect/cV;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    invoke-static {v1, v0}, Ln/a;->a(Ljava/util/Collection;Lo/ae;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_10
.end method

.method public a(Ln/f;)V
    .registers 2
    .parameter

    .prologue
    .line 160
    return-void
.end method

.method public a(Lo/o;)Z
    .registers 3
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Ln/j;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
