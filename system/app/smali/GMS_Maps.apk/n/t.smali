.class public Ln/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/e;
.implements Lv/c;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lo/T;

.field private final d:Lo/T;

.field private volatile e:Z

.field private volatile f:Lo/T;

.field private final g:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile h:Ln/v;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const v1, 0x4c4b40

    const v2, 0x3d0900

    .line 154
    new-instance v0, Lo/T;

    invoke-direct {v0, v1, v1}, Lo/T;-><init>(II)V

    new-instance v1, Lo/T;

    invoke-direct {v1, v2, v2}, Lo/T;-><init>(II)V

    invoke-direct {p0, p1, v0, v1}, Ln/t;-><init>(Ljava/lang/String;Lo/T;Lo/T;)V

    .line 155
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lo/T;Lo/T;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Ln/t;->b:Ljava/lang/String;

    .line 134
    iput-object p2, p0, Ln/t;->c:Lo/T;

    .line 135
    invoke-virtual {p2}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p3}, Lo/T;->f()I

    move-result v1

    if-lt v0, v1, :cond_33

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v0

    invoke-virtual {p3}, Lo/T;->g()I

    move-result v1

    if-lt v0, v1, :cond_33

    .line 137
    iput-object p3, p0, Ln/t;->d:Lo/T;

    .line 141
    :goto_1d
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ln/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 142
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Ln/t;->f:Lo/T;

    .line 143
    new-instance v0, Ln/v;

    invoke-direct {v0}, Ln/v;-><init>()V

    iput-object v0, p0, Ln/t;->h:Ln/v;

    .line 144
    return-void

    .line 139
    :cond_33
    iget-object v0, p0, Ln/t;->c:Lo/T;

    iput-object v0, p0, Ln/t;->d:Lo/T;

    goto :goto_1d
.end method

.method static a(Lo/T;Lo/T;)Lo/aR;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v2, 0xf

    .line 184
    invoke-virtual {p0, p1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->b(ILo/T;)Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->g()Lo/T;

    move-result-object v1

    .line 186
    invoke-virtual {p0, p1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->b(ILo/T;)Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->h()Lo/T;

    move-result-object v0

    .line 188
    invoke-virtual {v1}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v3

    if-le v2, v3, :cond_30

    .line 189
    new-instance v2, Lo/T;

    const/high16 v3, 0x4000

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lo/T;-><init>(II)V

    invoke-virtual {v0, v2}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    .line 191
    :cond_30
    new-instance v2, Lo/ad;

    invoke-direct {v2, v1, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-static {v2}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .registers 3

    .prologue
    .line 175
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_f

    .line 177
    iget-object v1, p0, Ln/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v0

    .line 178
    invoke-virtual {p0, v0}, Ln/t;->a(Lv/a;)V

    .line 180
    :cond_f
    return-void
.end method

.method static synthetic a(Ln/t;)V
    .registers 1
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ln/t;->a()V

    return-void
.end method

.method private a(Lo/T;)V
    .registers 4
    .parameter

    .prologue
    .line 158
    monitor-enter p0

    .line 159
    :try_start_1
    iget-boolean v0, p0, Ln/t;->e:Z

    if-eqz v0, :cond_7

    .line 160
    monitor-exit p0

    .line 172
    :goto_6
    return-void

    .line 162
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Ln/t;->e:Z

    .line 163
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_1a

    .line 164
    iput-object p1, p0, Ln/t;->f:Lo/T;

    .line 166
    new-instance v0, Ln/u;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ln/u;-><init>(Ln/t;Las/c;)V

    invoke-virtual {v0}, Ln/u;->g()V

    goto :goto_6

    .line 163
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private b(Lv/a;)V
    .registers 7
    .parameter

    .prologue
    .line 197
    invoke-virtual {p1}, Lv/a;->e()[B

    move-result-object v0

    .line 198
    if-eqz v0, :cond_30

    array-length v1, v0

    if-lez v1, :cond_30

    .line 200
    :try_start_9
    iget-object v1, p0, Ln/t;->f:Lo/T;

    iget-object v2, p0, Ln/t;->c:Lo/T;

    invoke-static {v1, v2}, Ln/t;->a(Lo/T;Lo/T;)Lo/aR;

    move-result-object v1

    .line 201
    iget-object v2, p0, Ln/t;->f:Lo/T;

    iget-object v3, p0, Ln/t;->d:Lo/T;

    invoke-static {v2, v3}, Ln/t;->a(Lo/T;Lo/T;)Lo/aR;

    move-result-object v2

    .line 210
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 211
    new-instance v0, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 212
    invoke-static {v0, v1}, Ln/j;->a(Ljava/io/Reader;Lo/aR;)Ln/j;

    move-result-object v0

    .line 215
    new-instance v3, Ln/v;

    invoke-direct {v3, v0, v1, v2}, Ln/v;-><init>(Ln/j;Lo/aR;Lo/aR;)V

    iput-object v3, p0, Ln/t;->h:Ln/v;
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_30} :catch_34

    .line 221
    :cond_30
    :goto_30
    const/4 v0, 0x0

    iput-boolean v0, p0, Ln/t;->e:Z

    .line 222
    return-void

    .line 216
    :catch_34
    move-exception v0

    .line 217
    const-string v1, "LazyBuildingBoundProvider"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_30
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .registers 5
    .parameter

    .prologue
    .line 226
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    .line 227
    const/16 v1, 0xf

    if-ge v0, v1, :cond_d

    .line 228
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 242
    :goto_c
    return-object v0

    .line 231
    :cond_d
    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    .line 232
    iget-object v1, p0, Ln/t;->h:Ln/v;

    .line 234
    iget-boolean v2, p0, Ln/t;->e:Z

    if-nez v2, :cond_26

    iget-object v2, v1, Ln/v;->c:Lo/aR;

    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/ae;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 235
    invoke-virtual {v0}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-direct {p0, v2}, Ln/t;->a(Lo/T;)V

    .line 238
    :cond_26
    iget-object v2, v1, Ln/v;->b:Lo/aR;

    invoke-virtual {v2, v0}, Lo/aR;->a(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 239
    iget-object v0, v1, Ln/v;->a:Ln/j;

    invoke-virtual {v0, p1}, Ln/j;->a(Lo/aq;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_c

    .line 242
    :cond_35
    sget-object v0, Ln/t;->a:Ljava/util/Collection;

    goto :goto_c
.end method

.method public a(Ln/f;)V
    .registers 3
    .parameter

    .prologue
    .line 254
    if-eqz p1, :cond_7

    .line 255
    iget-object v0, p0, Ln/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    :cond_7
    return-void
.end method

.method public a(Lv/a;)V
    .registers 4
    .parameter

    .prologue
    .line 269
    if-nez p1, :cond_3

    .line 283
    :goto_2
    return-void

    .line 272
    :cond_3
    monitor-enter p1

    .line 273
    :try_start_4
    iget-boolean v0, p0, Ln/t;->e:Z

    if-nez v0, :cond_d

    .line 274
    monitor-exit p1

    goto :goto_2

    .line 282
    :catchall_a
    move-exception v0

    monitor-exit p1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    throw v0

    .line 276
    :cond_d
    :try_start_d
    invoke-virtual {p1}, Lv/a;->b()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 277
    invoke-direct {p0, p1}, Ln/t;->b(Lv/a;)V

    .line 278
    iget-object v0, p0, Ln/t;->g:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/f;

    .line 279
    invoke-interface {v0}, Ln/f;->a()V

    goto :goto_1c

    .line 282
    :cond_2c
    monitor-exit p1
    :try_end_2d
    .catchall {:try_start_d .. :try_end_2d} :catchall_a

    goto :goto_2
.end method

.method public a(Lo/o;)Z
    .registers 3
    .parameter

    .prologue
    .line 247
    iget-object v0, p0, Ln/t;->h:Ln/v;

    .line 248
    iget-object v0, v0, Ln/v;->a:Ln/j;

    invoke-virtual {v0, p1}, Ln/j;->a(Lo/o;)Z

    move-result v0

    return v0
.end method
