.class Ln/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaC/e;


# instance fields
.field final synthetic a:Ln/y;


# direct methods
.method constructor <init>(Ln/y;)V
    .registers 2
    .parameter

    .prologue
    .line 855
    iput-object p1, p0, Ln/z;->a:Ln/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .registers 3

    .prologue
    .line 858
    iget-object v0, p0, Ln/z;->a:Ln/y;

    invoke-static {v0}, Ln/y;->a(Ln/y;)Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 859
    iget-object v0, p0, Ln/z;->a:Ln/y;

    invoke-static {v0}, Ln/y;->a(Ln/y;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 861
    if-eqz v0, :cond_21

    .line 869
    :goto_20
    return-object v0

    .line 866
    :cond_21
    iget-object v0, p0, Ln/z;->a:Ln/y;

    invoke-static {v0}, Ln/y;->a(Ln/y;)Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 867
    iget-object v0, p0, Ln/z;->a:Ln/y;

    invoke-static {v0}, Ln/y;->a(Ln/y;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    goto :goto_20

    .line 869
    :cond_40
    const/4 v0, 0x0

    goto :goto_20
.end method
