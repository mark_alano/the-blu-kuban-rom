.class public Lo/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/T;

.field private final b:I

.field private final c:F

.field private final d:Lo/T;

.field private final e:F

.field private final f:F

.field private final g:F


# direct methods
.method public constructor <init>(Lo/T;IFLo/T;FFF)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lo/a;->a:Lo/T;

    .line 42
    iput p2, p0, Lo/a;->b:I

    .line 43
    iput p3, p0, Lo/a;->c:F

    .line 44
    iput-object p4, p0, Lo/a;->d:Lo/T;

    .line 45
    iput p5, p0, Lo/a;->e:F

    .line 46
    iput p6, p0, Lo/a;->f:F

    .line 47
    iput p7, p0, Lo/a;->g:F

    .line 48
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/aq;I)Lo/a;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x7fc0

    .line 61
    invoke-static {p0, p1}, Lo/T;->a(Ljava/io/DataInput;Lo/aq;)Lo/T;

    move-result-object v1

    .line 62
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    .line 66
    invoke-static {v2}, Lo/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 69
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    invoke-static {v3}, Lo/O;->d(I)F

    move-result v3

    .line 73
    :goto_18
    const/4 v4, 0x0

    .line 77
    invoke-static {v2}, Lo/a;->b(I)Z

    move-result v5

    if-eqz v5, :cond_41

    .line 78
    invoke-static {p0, p1}, Lo/T;->a(Ljava/io/DataInput;Lo/aq;)Lo/T;

    move-result-object v4

    .line 81
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {v0}, Lo/O;->d(I)F

    move-result v5

    .line 82
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {v0}, Lo/O;->a(I)F

    move-result v6

    .line 83
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {v0}, Lo/O;->a(I)F

    move-result v7

    .line 86
    :goto_3b
    new-instance v0, Lo/a;

    invoke-direct/range {v0 .. v7}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    return-object v0

    :cond_41
    move v7, v0

    move v6, v0

    move v5, v0

    goto :goto_3b

    :cond_45
    move v3, v0

    goto :goto_18
.end method

.method private static a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 100
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .registers 2
    .parameter

    .prologue
    .line 104
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Z
    .registers 2

    .prologue
    .line 92
    iget v0, p0, Lo/a;->b:I

    invoke-static {v0}, Lo/a;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()Lo/T;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lo/a;->a:Lo/T;

    return-object v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 118
    iget v0, p0, Lo/a;->c:F

    return v0
.end method

.method public d()I
    .registers 3

    .prologue
    .line 197
    iget-object v0, p0, Lo/a;->a:Lo/T;

    invoke-static {v0}, Lo/O;->a(Lo/T;)I

    move-result v0

    add-int/lit8 v0, v0, 0x28

    iget-object v1, p0, Lo/a;->d:Lo/T;

    invoke-static {v1}, Lo/O;->a(Lo/T;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157
    if-ne p0, p1, :cond_6

    move v1, v0

    .line 193
    :cond_5
    :goto_5
    return v1

    .line 160
    :cond_6
    if-eqz p1, :cond_5

    .line 163
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_5

    .line 166
    check-cast p1, Lo/a;

    .line 167
    iget-object v2, p0, Lo/a;->d:Lo/T;

    if-nez v2, :cond_64

    .line 168
    iget-object v2, p1, Lo/a;->d:Lo/T;

    if-nez v2, :cond_5

    .line 174
    :cond_1c
    iget v2, p0, Lo/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/a;->f:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 177
    iget v2, p0, Lo/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/a;->e:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 180
    iget v2, p0, Lo/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/a;->g:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 183
    iget v2, p0, Lo/a;->b:I

    iget v3, p1, Lo/a;->b:I

    if-ne v2, v3, :cond_5

    .line 186
    iget-object v2, p0, Lo/a;->a:Lo/T;

    if-nez v2, :cond_6f

    .line 187
    iget-object v2, p1, Lo/a;->a:Lo/T;

    if-nez v2, :cond_5

    .line 193
    :cond_54
    iget v2, p0, Lo/a;->c:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/a;->c:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_7a

    :goto_62
    move v1, v0

    goto :goto_5

    .line 171
    :cond_64
    iget-object v2, p0, Lo/a;->d:Lo/T;

    iget-object v3, p1, Lo/a;->d:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    goto :goto_5

    .line 190
    :cond_6f
    iget-object v2, p0, Lo/a;->a:Lo/T;

    iget-object v3, p1, Lo/a;->a:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_54

    goto :goto_5

    :cond_7a
    move v0, v1

    .line 193
    goto :goto_62
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 143
    .line 145
    iget-object v0, p0, Lo/a;->d:Lo/T;

    if-nez v0, :cond_39

    move v0, v1

    :goto_6
    add-int/lit8 v0, v0, 0x1f

    .line 146
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/a;->b:I

    add-int/2addr v0, v2

    .line 150
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/a;->a:Lo/T;

    if-nez v2, :cond_40

    :goto_2e
    add-int/2addr v0, v1

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    return v0

    .line 145
    :cond_39
    iget-object v0, p0, Lo/a;->d:Lo/T;

    invoke-virtual {v0}, Lo/T;->hashCode()I

    move-result v0

    goto :goto_6

    .line 150
    :cond_40
    iget-object v1, p0, Lo/a;->a:Lo/T;

    invoke-virtual {v1}, Lo/T;->hashCode()I

    move-result v1

    goto :goto_2e
.end method
