.class public Lo/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# static fields
.field private static final m:[B

.field private static final n:[I


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/aF;

.field private final c:Lo/aF;

.field private final d:[B

.field private final e:Lo/aj;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_12

    sput-object v0, Lo/g;->m:[B

    .line 55
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lo/g;->n:[I

    return-void

    .line 45
    :array_12
    .array-data 0x1
        0x1t
        0x2t
        0x4t
    .end array-data

    .line 55
    :array_18
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lo/o;Lo/aF;Lo/aF;[BIILo/aj;ILjava/lang/String;II[I)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lo/g;->a:Lo/o;

    .line 74
    iput-object p2, p0, Lo/g;->b:Lo/aF;

    .line 75
    iput-object p3, p0, Lo/g;->c:Lo/aF;

    .line 76
    iput-object p4, p0, Lo/g;->d:[B

    .line 77
    iput p5, p0, Lo/g;->h:I

    .line 78
    iput p6, p0, Lo/g;->i:I

    .line 79
    iput-object p7, p0, Lo/g;->e:Lo/aj;

    .line 80
    iput p8, p0, Lo/g;->f:I

    .line 81
    iput-object p9, p0, Lo/g;->g:Ljava/lang/String;

    .line 82
    iput p10, p0, Lo/g;->j:I

    .line 83
    iput p11, p0, Lo/g;->k:I

    .line 84
    iput-object p12, p0, Lo/g;->l:[I

    .line 85
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/g;
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 104
    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v0

    invoke-static {p0, v0}, Lo/aF;->a(Ljava/io/DataInput;Lo/aq;)Lo/aF;

    move-result-object v2

    .line 105
    invoke-static {p0, p1}, Lo/aF;->a(Ljava/io/DataInput;Lo/as;)Lo/aF;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Lo/aF;->a()I

    move-result v0

    if-eqz v0, :cond_51

    move v0, v8

    .line 109
    :goto_15
    if-eqz v0, :cond_53

    invoke-virtual {v3}, Lo/aF;->a()I

    move-result v0

    :goto_1b
    new-array v4, v0, [B

    .line 112
    invoke-interface {p0, v4}, Ljava/io/DataInput;->readFully([B)V

    .line 115
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 118
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 121
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v9

    .line 124
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v10

    .line 127
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v11

    .line 130
    const/4 v1, 0x0

    .line 131
    invoke-static {v8, v11}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 132
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    .line 138
    :cond_3f
    :goto_3f
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v8

    .line 139
    new-array v12, v8, [I

    move v0, v7

    .line 140
    :goto_46
    if-ge v0, v8, :cond_64

    .line 141
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v12, v0

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_46

    :cond_51
    move v0, v7

    .line 106
    goto :goto_15

    .line 109
    :cond_53
    invoke-virtual {v2}, Lo/aF;->a()I

    move-result v0

    goto :goto_1b

    .line 133
    :cond_58
    const/4 v0, 0x2

    invoke-static {v0, v11}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 134
    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_3f

    .line 144
    :cond_64
    new-instance v0, Lo/g;

    invoke-virtual {v9}, Lo/ak;->a()Lo/aj;

    move-result-object v7

    invoke-virtual {v9}, Lo/ak;->c()I

    move-result v8

    invoke-virtual {v9}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v0 .. v12}, Lo/g;-><init>(Lo/o;Lo/aF;Lo/aF;[BIILo/aj;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lo/g;->a:Lo/o;

    return-object v0
.end method

.method public a(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lo/g;->d:[B

    aget-byte v0, v0, p1

    sget-object v1, Lo/g;->m:[B

    aget-byte v1, v1, p2

    and-int/2addr v0, v1

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public b()Lo/aF;
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lo/g;->b:Lo/aF;

    return-object v0
.end method

.method public c()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 182
    move v1, v0

    .line 183
    :goto_2
    iget-object v2, p0, Lo/g;->d:[B

    array-length v2, v2

    if-ge v0, v2, :cond_15

    .line 184
    sget-object v2, Lo/g;->n:[I

    iget-object v3, p0, Lo/g;->d:[B

    aget-byte v3, v3, v0

    and-int/lit8 v3, v3, 0x7

    aget v2, v2, v3

    add-int/2addr v1, v2

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 186
    :cond_15
    return v1
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 190
    iget v0, p0, Lo/g;->k:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lo/g;->e:Lo/aj;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 198
    iget v0, p0, Lo/g;->h:I

    return v0
.end method

.method public g()I
    .registers 2

    .prologue
    .line 202
    iget v0, p0, Lo/g;->i:I

    return v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 222
    const/4 v0, 0x4

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 227
    iget v0, p0, Lo/g;->j:I

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 238
    iget-object v0, p0, Lo/g;->l:[I

    return-object v0
.end method

.method public m()I
    .registers 4

    .prologue
    .line 244
    iget-object v0, p0, Lo/g;->b:Lo/aF;

    invoke-virtual {v0}, Lo/aF;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x40

    iget-object v1, p0, Lo/g;->d:[B

    array-length v1, v1

    add-int/2addr v1, v0

    .line 245
    iget-object v0, p0, Lo/g;->c:Lo/aF;

    if-nez v0, :cond_28

    const/4 v0, 0x0

    .line 248
    :goto_11
    iget-object v2, p0, Lo/g;->a:Lo/o;

    invoke-static {v2}, Lo/O;->a(Lo/o;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lo/g;->g:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lo/g;->e:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 252
    return v0

    .line 245
    :cond_28
    iget-object v0, p0, Lo/g;->c:Lo/aF;

    invoke-virtual {v0}, Lo/aF;->b()I

    move-result v0

    goto :goto_11
.end method
