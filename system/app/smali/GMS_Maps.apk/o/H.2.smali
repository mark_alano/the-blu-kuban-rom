.class public Lo/H;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lo/ak;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Lo/b;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 26
    new-instance v0, Lo/ak;

    const/4 v1, -0x1

    invoke-direct {v0, v2, v2, v1}, Lo/ak;-><init>(Lo/aj;Ljava/lang/String;I)V

    sput-object v0, Lo/H;->a:Lo/ak;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lo/b;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-nez p1, :cond_d

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter labelElements can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_16
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    .line 51
    invoke-virtual {v0}, Lo/I;->c()Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 52
    invoke-virtual {v0}, Lo/I;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    :cond_2f
    invoke-virtual {v0}, Lo/I;->f()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 55
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_16

    .line 58
    :cond_3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo/H;->d:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lo/H;->c:Lo/b;

    .line 61
    iput-object p1, p0, Lo/H;->b:Ljava/util/List;

    .line 62
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v1, :cond_12

    .line 85
    invoke-static {p0, p1, p2, v2}, Lo/I;->a(Ljava/io/DataInput;Lo/as;Lo/ak;Ljava/util/List;)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 91
    :cond_12
    const/4 v0, 0x1

    if-le v1, v0, :cond_1f

    .line 92
    invoke-static {p0, p1}, Lo/b;->a(Ljava/io/DataInput;Lo/as;)Lo/b;

    move-result-object v0

    .line 97
    :goto_19
    new-instance v1, Lo/H;

    invoke-direct {v1, v2, v0}, Lo/H;-><init>(Ljava/util/List;Lo/b;)V

    return-object v1

    .line 94
    :cond_1f
    sget-object v0, Lo/b;->b:Lo/b;

    goto :goto_19
.end method

.method static synthetic e()Lo/ak;
    .registers 1

    .prologue
    .line 18
    sget-object v0, Lo/H;->a:Lo/ak;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Lo/H;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Lo/I;
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public c()Lo/b;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lo/H;->c:Lo/b;

    return-object v0
.end method

.method public d()I
    .registers 4

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    iget-object v1, p0, Lo/H;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/I;

    .line 158
    invoke-virtual {v0}, Lo/I;->l()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_8

    .line 160
    :cond_1b
    add-int/lit8 v0, v1, 0x18

    iget-object v1, p0, Lo/H;->d:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/H;->c:Lo/b;

    invoke-virtual {v1}, Lo/b;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 135
    if-ne p0, p1, :cond_5

    .line 136
    const/4 v0, 0x1

    .line 152
    :cond_4
    :goto_4
    return v0

    .line 138
    :cond_5
    if-eqz p1, :cond_4

    .line 141
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_4

    .line 144
    check-cast p1, Lo/H;

    .line 145
    iget-object v1, p0, Lo/H;->c:Lo/b;

    if-nez v1, :cond_24

    .line 146
    iget-object v1, p1, Lo/H;->c:Lo/b;

    if-nez v1, :cond_4

    .line 152
    :cond_1b
    iget-object v0, p0, Lo/H;->b:Ljava/util/List;

    iget-object v1, p1, Lo/H;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4

    .line 149
    :cond_24
    iget-object v1, p0, Lo/H;->c:Lo/b;

    iget-object v2, p1, Lo/H;->c:Lo/b;

    invoke-virtual {v1, v2}, Lo/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 126
    .line 128
    iget-object v0, p0, Lo/H;->c:Lo/b;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    :goto_5
    add-int/lit8 v0, v0, 0x1f

    .line 129
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/H;->b:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    return v0

    .line 128
    :cond_11
    iget-object v0, p0, Lo/H;->c:Lo/b;

    invoke-virtual {v0}, Lo/b;->hashCode()I

    move-result v0

    goto :goto_5
.end method
