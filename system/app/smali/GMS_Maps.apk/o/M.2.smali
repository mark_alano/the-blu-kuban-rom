.class public Lo/M;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private a:Lo/X;

.field private b:Lo/aj;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private final g:[I


# direct methods
.method public constructor <init>(Lo/X;Lo/aj;ILjava/lang/String;II[I)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lo/M;->a:Lo/X;

    .line 46
    iput-object p2, p0, Lo/M;->b:Lo/aj;

    .line 47
    iput p3, p0, Lo/M;->c:I

    .line 48
    iput-object p4, p0, Lo/M;->d:Ljava/lang/String;

    .line 49
    iput p5, p0, Lo/M;->e:I

    .line 50
    iput p6, p0, Lo/M;->f:I

    .line 51
    iput-object p7, p0, Lo/M;->g:[I

    .line 52
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/M;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v0

    invoke-static {p0, v0}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v1

    .line 69
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v4

    .line 72
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v5

    .line 75
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v6

    .line 78
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 79
    new-array v7, v2, [I

    .line 80
    const/4 v0, 0x0

    :goto_1b
    if-ge v0, v2, :cond_26

    .line 81
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    aput v3, v7, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 84
    :cond_26
    new-instance v0, Lo/M;

    invoke-virtual {v4}, Lo/ak;->a()Lo/aj;

    move-result-object v2

    invoke-virtual {v4}, Lo/ak;->c()I

    move-result v3

    invoke-virtual {v4}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v7}, Lo/M;-><init>(Lo/X;Lo/aj;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .registers 2

    .prologue
    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Lo/X;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lo/M;->a:Lo/X;

    return-object v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lo/M;->b:Lo/aj;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 120
    const/4 v0, 0x5

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 115
    iget v0, p0, Lo/M;->e:I

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lo/M;->g:[I

    return-object v0
.end method

.method public m()I
    .registers 3

    .prologue
    .line 140
    iget-object v0, p0, Lo/M;->a:Lo/X;

    invoke-virtual {v0}, Lo/X;->h()I

    move-result v0

    add-int/lit8 v0, v0, 0x28

    iget-object v1, p0, Lo/M;->d:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/M;->b:Lo/aj;

    invoke-static {v1}, Lo/O;->a(Lo/aj;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
