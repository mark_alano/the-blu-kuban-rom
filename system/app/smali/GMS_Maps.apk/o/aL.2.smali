.class public Lo/aL;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/m;


# instance fields
.field private final a:Lo/aq;

.field private final b:I

.field private final c:B

.field private final d:[Lo/n;

.field private final e:Lo/al;

.field private final f:[Ljava/lang/String;

.field private final g:J

.field private final h:[Ljava/lang/String;

.field private final i:[Ljava/lang/String;

.field private final j:[Ljava/lang/String;

.field private final k:I

.field private final l:LA/c;

.field private final m:[Lo/aI;

.field private final n:I

.field private o:J


# direct methods
.method protected constructor <init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lo/aL;->i:[Ljava/lang/String;

    .line 93
    iput-object p1, p0, Lo/aL;->e:Lo/al;

    .line 94
    iput-object p2, p0, Lo/aL;->f:[Ljava/lang/String;

    .line 95
    iput-object p3, p0, Lo/aL;->a:Lo/aq;

    .line 96
    iput p4, p0, Lo/aL;->b:I

    .line 97
    iput-byte p5, p0, Lo/aL;->c:B

    .line 98
    iput-object p7, p0, Lo/aL;->h:[Ljava/lang/String;

    .line 99
    iput-object p8, p0, Lo/aL;->j:[Ljava/lang/String;

    .line 100
    iput p9, p0, Lo/aL;->k:I

    .line 101
    iput-object p10, p0, Lo/aL;->d:[Lo/n;

    .line 102
    iput-object p11, p0, Lo/aL;->l:LA/c;

    .line 103
    iput-object p12, p0, Lo/aL;->m:[Lo/aI;

    .line 104
    iput p6, p0, Lo/aL;->n:I

    .line 105
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lo/aL;->g:J

    .line 106
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lo/aL;->o:J

    .line 107
    return-void
.end method

.method public static a(Lo/aq;Ljava/io/DataInput;IBIILA/c;JJ)Lo/aL;
    .registers 30
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 225
    invoke-static/range {p1 .. p1}, Lo/aL;->a(Ljava/io/DataInput;)V

    .line 230
    invoke-static/range {p1 .. p1}, Lo/aq;->a(Ljava/io/DataInput;)Lo/aq;

    move-result-object v2

    .line 231
    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->c()I

    move-result v4

    if-ne v3, v4, :cond_25

    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->d()I

    move-result v4

    if-ne v3, v4, :cond_25

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lo/aq;->b()I

    move-result v4

    if-eq v3, v4, :cond_4a

    .line 234
    :cond_25
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Expected tile coords: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 242
    :cond_4a
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v11

    .line 243
    if-lez v11, :cond_52

    .line 244
    add-int/lit16 v11, v11, 0x7d0

    .line 246
    :cond_52
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 247
    new-array v9, v3, [Ljava/lang/String;

    .line 248
    const/4 v2, 0x0

    :goto_59
    if-ge v2, v3, :cond_68

    .line 249
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v2

    .line 248
    add-int/lit8 v2, v2, 0x1

    goto :goto_59

    .line 251
    :cond_68
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 252
    new-array v10, v3, [Ljava/lang/String;

    .line 253
    const/4 v2, 0x0

    :goto_6f
    if-ge v2, v3, :cond_7e

    .line 254
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v2

    .line 253
    add-int/lit8 v2, v2, 0x1

    goto :goto_6f

    .line 257
    :cond_7e
    invoke-static/range {p1 .. p2}, Lo/al;->a(Ljava/io/DataInput;I)Lo/al;

    move-result-object v3

    .line 260
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 261
    new-array v4, v5, [Ljava/lang/String;

    .line 262
    const/4 v2, 0x0

    :goto_89
    if-ge v2, v5, :cond_98

    .line 263
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_89

    .line 266
    :cond_98
    new-instance v5, Lo/as;

    move/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1, v3, v4}, Lo/as;-><init>(ILo/aq;Lo/al;[Ljava/lang/String;)V

    .line 270
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 271
    new-array v12, v6, [Lo/n;

    .line 272
    const/4 v2, 0x0

    :goto_a8
    if-ge v2, v6, :cond_b5

    .line 273
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lo/aL;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v7

    aput-object v7, v12, v2

    .line 272
    add-int/lit8 v2, v2, 0x1

    goto :goto_a8

    .line 276
    :cond_b5
    invoke-static/range {p1 .. p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 277
    new-array v14, v6, [Lo/aI;

    .line 278
    const/4 v2, 0x0

    :goto_bc
    if-ge v2, v6, :cond_c9

    .line 279
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lo/aI;->a(Ljava/io/DataInput;Lo/as;)Lo/aI;

    move-result-object v7

    aput-object v7, v14, v2

    .line 278
    add-int/lit8 v2, v2, 0x1

    goto :goto_bc

    .line 282
    :cond_c9
    new-instance v2, Lo/aL;

    move-object/from16 v5, p0

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p5

    move-object/from16 v13, p6

    move-wide/from16 v15, p7

    move-wide/from16 v17, p9

    invoke-direct/range {v2 .. v18}, Lo/aL;-><init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V

    return-object v2
.end method

.method public static a(Lo/aq;[BILA/c;JJ)Lo/aL;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-static/range {p1 .. p2}, Lo/aL;->a([BI)[J

    move-result-object v0

    .line 308
    const/4 v1, 0x0

    .line 309
    const/4 v2, 0x1

    aget-wide v3, v0, v1

    long-to-int v8, v3

    .line 311
    const/4 v1, 0x2

    aget-wide v2, v0, v2

    long-to-int v9, v2

    .line 312
    const/4 v2, 0x3

    aget-wide v3, v0, v1

    long-to-int v1, v3

    .line 313
    const/4 v3, 0x4

    aget-wide v4, v0, v2

    long-to-int v2, v4

    .line 314
    const/4 v5, 0x5

    aget-wide v3, v0, v3

    .line 317
    aget-wide v5, v0, v5

    long-to-int v0, v5

    int-to-byte v10, v0

    .line 320
    packed-switch v1, :pswitch_data_80

    .line 326
    const/16 v0, 0x1a

    .line 328
    :goto_21
    add-int v6, p2, v0

    .line 329
    array-length v0, p1

    sub-int v7, v0, v6

    move-object v0, p0

    move-object v5, p1

    .line 331
    invoke-static/range {v0 .. v7}, Lo/aL;->a(Lo/aq;IIJ[BII)V

    .line 333
    :try_start_2b
    invoke-static {p1, v6, v7}, LR/f;->a([BII)LR/g;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, LR/g;->a()[B

    move-result-object v13

    .line 335
    invoke-virtual {v0}, LR/g;->b()I

    move-result v0

    .line 336
    new-instance v3, Laq/a;

    invoke-direct {v3, v13}, Laq/a;-><init>([B)V

    move-object v2, p0

    move v4, v1

    move v5, v10

    move v6, v8

    move v7, v9

    move-object/from16 v8, p3

    move-wide/from16 v9, p4

    move-wide/from16 v11, p6

    .line 337
    invoke-static/range {v2 .. v12}, Lo/aL;->a(Lo/aq;Ljava/io/DataInput;IBIILA/c;JJ)Lo/aL;

    move-result-object v1

    .line 339
    invoke-virtual {v3}, Laq/a;->a()I

    move-result v2

    if-eq v2, v0, :cond_7c

    .line 340
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Byte stream not fully read for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lo/aL;->d()Lo/aq;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6e
    .catch Ljava/util/zip/DataFormatException; {:try_start_2b .. :try_end_6e} :catch_6e

    .line 347
    :catch_6e
    move-exception v0

    .line 348
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 322
    :pswitch_79
    const/16 v0, 0x1b

    .line 323
    goto :goto_21

    .line 345
    :cond_7c
    :try_start_7c
    invoke-static {v13}, Lbm/g;->a([B)V
    :try_end_7f
    .catch Ljava/util/zip/DataFormatException; {:try_start_7c .. :try_end_7f} :catch_6e

    .line 346
    return-object v1

    .line 320
    :pswitch_data_80
    .packed-switch 0xa
        :pswitch_79
    .end packed-switch
.end method

.method static a(Ljava/io/DataInput;Lo/as;)Lo/n;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 361
    packed-switch v0, :pswitch_data_52

    .line 383
    :pswitch_7
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown feature type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 363
    :pswitch_20
    invoke-static {p0, p1}, Lo/l;->a(Ljava/io/DataInput;Lo/as;)Lo/l;

    move-result-object v0

    .line 381
    :goto_24
    return-object v0

    .line 365
    :pswitch_25
    invoke-static {p0, p1}, Lo/af;->a(Ljava/io/DataInput;Lo/as;)Lo/af;

    move-result-object v0

    goto :goto_24

    .line 367
    :pswitch_2a
    invoke-static {p0, p1}, Lo/f;->a(Ljava/io/DataInput;Lo/as;)Lo/f;

    move-result-object v0

    goto :goto_24

    .line 369
    :pswitch_2f
    invoke-static {p0, p1}, Lo/g;->a(Ljava/io/DataInput;Lo/as;)Lo/g;

    move-result-object v0

    goto :goto_24

    .line 371
    :pswitch_34
    invoke-static {p0, p1}, Lo/M;->a(Ljava/io/DataInput;Lo/as;)Lo/M;

    move-result-object v0

    goto :goto_24

    .line 373
    :pswitch_39
    invoke-static {p0, p1}, Lo/ac;->a(Ljava/io/DataInput;Lo/as;)Lo/ac;

    move-result-object v0

    goto :goto_24

    .line 375
    :pswitch_3e
    invoke-static {p0, p1}, Lo/U;->a(Ljava/io/DataInput;Lo/as;)Lo/U;

    move-result-object v0

    goto :goto_24

    .line 377
    :pswitch_43
    invoke-static {p0, p1}, Lo/K;->a(Ljava/io/DataInput;Lo/as;)Lo/K;

    move-result-object v0

    goto :goto_24

    .line 379
    :pswitch_48
    invoke-static {p0, p1}, Lo/aC;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v0

    goto :goto_24

    .line 381
    :pswitch_4d
    invoke-static {p0, p1}, Lo/L;->b(Ljava/io/DataInput;Lo/as;)Lo/K;

    move-result-object v0

    goto :goto_24

    .line 361
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_20
        :pswitch_25
        :pswitch_2a
        :pswitch_2f
        :pswitch_34
        :pswitch_39
        :pswitch_3e
        :pswitch_43
        :pswitch_48
        :pswitch_7
        :pswitch_4d
    .end packed-switch
.end method

.method public static a(II[B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 615
    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lo/O;->a(I[BI)V

    .line 616
    const/4 v0, 0x4

    invoke-static {p1, p2, v0}, Lo/O;->a(I[BI)V

    .line 617
    return-void
.end method

.method private static a(Ljava/io/DataInput;)V
    .registers 5
    .parameter

    .prologue
    .line 555
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 556
    const v1, 0x44524154

    if-eq v0, v1, :cond_22

    .line 557
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TILE_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 559
    :cond_22
    return-void
.end method

.method private static a(Lo/aq;IIJ[BII)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 542
    new-instance v11, Lr/y;

    invoke-direct {v11}, Lr/y;-><init>()V

    .line 543
    const/16 v3, 0x28

    new-array v10, v3, [B

    .line 544
    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v3

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v4

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v5

    move v6, p1

    move v7, p2

    move-wide v8, p3

    invoke-static/range {v3 .. v10}, Lr/y;->a(IIIIIJ[B)V

    .line 547
    const/16 v3, 0x100

    invoke-virtual {v11, v10, v3}, Lr/y;->a([BI)V

    .line 548
    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {v11, v0, v1, v2}, Lr/y;->a([BII)V

    .line 549
    return-void
.end method

.method public static a(Lo/ap;)Z
    .registers 3
    .parameter

    .prologue
    .line 703
    invoke-interface {p0}, Lo/ap;->g()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->q:LA/c;

    if-ne v0, v1, :cond_11

    move-object v0, p0

    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->r()I

    move-result v0

    if-eqz v0, :cond_15

    :cond_11
    instance-of v0, p0, Lo/N;

    if-eqz v0, :cond_17

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private static a([BI)[J
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v8, 0xa

    const/4 v1, 0x0

    .line 576
    new-instance v0, Laq/a;

    invoke-direct {v0, p0}, Laq/a;-><init>([B)V

    .line 577
    invoke-virtual {v0, p1}, Laq/a;->skipBytes(I)I

    .line 585
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v2

    .line 587
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v3

    .line 589
    invoke-static {v0}, Lo/aL;->a(Ljava/io/DataInput;)V

    .line 591
    invoke-virtual {v0}, Laq/a;->readUnsignedShort()I

    move-result v4

    .line 593
    const/16 v5, 0x9

    if-eq v4, v5, :cond_3f

    if-eq v4, v8, :cond_3f

    .line 595
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version mismatch: 9 or 10 expected, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 600
    :cond_3f
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v5

    .line 603
    invoke-virtual {v0}, Laq/a;->readLong()J

    move-result-wide v6

    .line 607
    if-ne v4, v8, :cond_67

    .line 608
    invoke-virtual {v0}, Laq/a;->readUnsignedByte()I

    move-result v0

    .line 610
    :goto_4d
    const/4 v8, 0x6

    new-array v8, v8, [J

    int-to-long v9, v2

    aput-wide v9, v8, v1

    const/4 v1, 0x1

    int-to-long v2, v3

    aput-wide v2, v8, v1

    const/4 v1, 0x2

    int-to-long v2, v4

    aput-wide v2, v8, v1

    const/4 v1, 0x3

    int-to-long v2, v5

    aput-wide v2, v8, v1

    const/4 v1, 0x4

    aput-wide v6, v8, v1

    const/4 v1, 0x5

    int-to-long v2, v0

    aput-wide v2, v8, v1

    return-object v8

    :cond_67
    move v0, v1

    goto :goto_4d
.end method

.method static synthetic b(Lo/aL;)[Lo/n;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    return-object v0
.end method

.method public static s()J
    .registers 2

    .prologue
    .line 673
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->u()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->e()J

    move-result-wide v0

    :goto_18
    return-wide v0

    :cond_19
    const-wide/16 v0, -0x1

    goto :goto_18
.end method

.method public static t()I
    .registers 4

    .prologue
    .line 685
    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_c

    .line 686
    const/4 v0, -0x1

    .line 688
    :goto_b
    return v0

    :cond_c
    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_b
.end method

.method public static u()I
    .registers 1

    .prologue
    .line 693
    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 629
    iget-wide v0, p0, Lo/aL;->g:J

    return-wide v0
.end method

.method public a(I)Lo/n;
    .registers 3
    .parameter

    .prologue
    .line 459
    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .registers 6
    .parameter

    .prologue
    .line 634
    iget-wide v0, p0, Lo/aL;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lo/aL;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b()J
    .registers 3

    .prologue
    .line 654
    iget-wide v0, p0, Lo/aL;->o:J

    return-wide v0
.end method

.method public b(I)Lo/aI;
    .registers 3
    .parameter

    .prologue
    .line 477
    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    aget-object v0, v0, p1

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .registers 6
    .parameter

    .prologue
    .line 649
    iget-wide v0, p0, Lo/aL;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lo/aL;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public c(Lcom/google/googlenav/common/a;)V
    .registers 6
    .parameter

    .prologue
    .line 659
    invoke-static {}, Lo/aL;->s()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_16

    .line 660
    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-static {}, Lo/aL;->s()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lo/aL;->o:J

    .line 665
    :goto_15
    return-void

    .line 663
    :cond_16
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lo/aL;->o:J

    goto :goto_15
.end method

.method public c()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 437
    iget-object v0, p0, Lo/aL;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public d()Lo/aq;
    .registers 2

    .prologue
    .line 400
    iget-object v0, p0, Lo/aL;->a:Lo/aq;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 405
    iget v0, p0, Lo/aL;->b:I

    return v0
.end method

.method public f()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 451
    iget-object v0, p0, Lo/aL;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public g()LA/c;
    .registers 2

    .prologue
    .line 639
    iget-object v0, p0, Lo/aL;->l:LA/c;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 644
    iget v0, p0, Lo/aL;->n:I

    return v0
.end method

.method public i()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 424
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/k;->h()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 427
    :cond_12
    invoke-virtual {p0}, Lo/aL;->n()Z

    move-result v2

    if-nez v2, :cond_1e

    invoke-virtual {p0}, Lo/aL;->o()Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1e
    move v0, v1

    :cond_1f
    move v1, v0

    .line 431
    :cond_20
    :goto_20
    return v1

    :cond_21
    invoke-virtual {p0}, Lo/aL;->o()Z

    move-result v2

    if-eqz v2, :cond_20

    move v1, v0

    goto :goto_20
.end method

.method public j()I
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    array-length v0, v0

    return v0
.end method

.method public k()Lo/aO;
    .registers 3

    .prologue
    .line 467
    new-instance v0, Lo/aP;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lo/aP;-><init>(Lo/aL;Lo/aM;)V

    return-object v0
.end method

.method public m()B
    .registers 2

    .prologue
    .line 409
    iget-byte v0, p0, Lo/aL;->c:B

    return v0
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 414
    iget-byte v0, p0, Lo/aL;->c:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 419
    iget-byte v0, p0, Lo/aL;->c:B

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public p()I
    .registers 2

    .prologue
    .line 447
    iget v0, p0, Lo/aL;->k:I

    return v0
.end method

.method protected q()[Lo/n;
    .registers 2

    .prologue
    .line 463
    iget-object v0, p0, Lo/aL;->d:[Lo/n;

    return-object v0
.end method

.method public r()I
    .registers 2

    .prologue
    .line 471
    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lo/aL;->m:[Lo/aI;

    array-length v0, v0

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
