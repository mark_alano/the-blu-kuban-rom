.class public Lo/aN;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lo/aq;

.field private b:I

.field private c:B

.field private d:I

.field private e:[Lo/n;

.field private f:Lo/al;

.field private g:[Ljava/lang/String;

.field private h:J

.field private i:[Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:I

.field private l:LA/c;

.field private m:[Lo/aI;

.field private n:J


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput v0, p0, Lo/aN;->d:I

    .line 118
    iput-wide v1, p0, Lo/aN;->h:J

    .line 121
    iput v0, p0, Lo/aN;->k:I

    .line 122
    sget-object v0, LA/c;->a:LA/c;

    iput-object v0, p0, Lo/aN;->l:LA/c;

    .line 124
    iput-wide v1, p0, Lo/aN;->n:J

    return-void
.end method


# virtual methods
.method public a()Lo/aL;
    .registers 20

    .prologue
    .line 197
    new-instance v2, Lo/aL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/aN;->f:Lo/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lo/aN;->g:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lo/aN;->a:Lo/aq;

    move-object/from16 v0, p0

    iget v6, v0, Lo/aN;->b:I

    move-object/from16 v0, p0

    iget-byte v7, v0, Lo/aN;->c:B

    move-object/from16 v0, p0

    iget v8, v0, Lo/aN;->d:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lo/aN;->i:[Ljava/lang/String;

    if-nez v9, :cond_4f

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    :goto_23
    move-object/from16 v0, p0

    iget-object v10, v0, Lo/aN;->j:[Ljava/lang/String;

    if-nez v10, :cond_54

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    :goto_2c
    move-object/from16 v0, p0

    iget v11, v0, Lo/aN;->k:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lo/aN;->e:[Lo/n;

    if-nez v12, :cond_59

    const/4 v12, 0x0

    new-array v12, v12, [Lo/n;

    :goto_39
    move-object/from16 v0, p0

    iget-object v13, v0, Lo/aN;->l:LA/c;

    move-object/from16 v0, p0

    iget-object v14, v0, Lo/aN;->m:[Lo/aI;

    move-object/from16 v0, p0

    iget-wide v15, v0, Lo/aN;->h:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lo/aN;->n:J

    move-wide/from16 v17, v0

    invoke-direct/range {v2 .. v18}, Lo/aL;-><init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V

    return-object v2

    :cond_4f
    move-object/from16 v0, p0

    iget-object v9, v0, Lo/aN;->i:[Ljava/lang/String;

    goto :goto_23

    :cond_54
    move-object/from16 v0, p0

    iget-object v10, v0, Lo/aN;->j:[Ljava/lang/String;

    goto :goto_2c

    :cond_59
    move-object/from16 v0, p0

    iget-object v12, v0, Lo/aN;->e:[Lo/n;

    goto :goto_39
.end method

.method public a(I)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 142
    iput p1, p0, Lo/aN;->d:I

    .line 143
    return-object p0
.end method

.method public a(J)Lo/aN;
    .registers 3
    .parameter

    .prologue
    .line 137
    iput-wide p1, p0, Lo/aN;->h:J

    .line 138
    return-object p0
.end method

.method public a(LA/c;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lo/aN;->l:LA/c;

    .line 183
    return-object p0
.end method

.method public a(Lo/al;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lo/aN;->f:Lo/al;

    .line 128
    return-object p0
.end method

.method public a(Lo/aq;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lo/aN;->a:Lo/aq;

    .line 153
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Lo/aN;->i:[Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public a([Lo/n;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lo/aN;->e:[Lo/n;

    .line 178
    return-object p0
.end method

.method public b(I)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 157
    iput p1, p0, Lo/aN;->b:I

    .line 158
    return-object p0
.end method

.method public b(J)Lo/aN;
    .registers 3
    .parameter

    .prologue
    .line 147
    iput-wide p1, p0, Lo/aN;->n:J

    .line 148
    return-object p0
.end method

.method public b([Ljava/lang/String;)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 167
    iput-object p1, p0, Lo/aN;->j:[Ljava/lang/String;

    .line 168
    return-object p0
.end method

.method public c(I)Lo/aN;
    .registers 2
    .parameter

    .prologue
    .line 172
    iput p1, p0, Lo/aN;->k:I

    .line 173
    return-object p0
.end method
