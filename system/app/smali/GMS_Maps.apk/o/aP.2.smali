.class Lo/aP;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/aO;


# instance fields
.field final synthetic a:Lo/aL;

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>(Lo/aL;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 498
    iput-object p1, p0, Lo/aP;->a:Lo/aL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500
    iput v0, p0, Lo/aP;->b:I

    .line 503
    iput v0, p0, Lo/aP;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lo/aL;Lo/aM;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lo/aP;-><init>(Lo/aL;)V

    return-void
.end method


# virtual methods
.method public a()Lo/n;
    .registers 4

    .prologue
    .line 512
    iget-object v0, p0, Lo/aP;->a:Lo/aL;

    invoke-static {v0}, Lo/aL;->b(Lo/aL;)[Lo/n;

    move-result-object v0

    iget v1, p0, Lo/aP;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lo/aP;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public b()Lo/n;
    .registers 3

    .prologue
    .line 523
    iget-object v0, p0, Lo/aP;->a:Lo/aL;

    invoke-static {v0}, Lo/aL;->b(Lo/aL;)[Lo/n;

    move-result-object v0

    iget v1, p0, Lo/aP;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 528
    iget v0, p0, Lo/aP;->b:I

    iput v0, p0, Lo/aP;->c:I

    .line 529
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 533
    iget v0, p0, Lo/aP;->c:I

    iput v0, p0, Lo/aP;->b:I

    .line 534
    return-void
.end method

.method public hasNext()Z
    .registers 3

    .prologue
    .line 507
    iget v0, p0, Lo/aP;->b:I

    iget-object v1, p0, Lo/aP;->a:Lo/aL;

    invoke-static {v1}, Lo/aL;->b(Lo/aL;)[Lo/n;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 498
    invoke-virtual {p0}, Lo/aP;->a()Lo/n;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 518
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
