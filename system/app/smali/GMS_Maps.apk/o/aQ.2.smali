.class public final Lo/aQ;
.super Lo/aS;
.source "SourceFile"


# instance fields
.field private final b:[Lo/T;

.field private final c:[Lo/T;

.field private final d:Lo/t;

.field private final e:Lo/aR;

.field private final f:Lo/ad;

.field private g:[[Lo/T;


# direct methods
.method private constructor <init>([Lo/T;)V
    .registers 5
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lo/aS;-><init>()V

    .line 31
    array-length v0, p1

    new-array v0, v0, [Lo/T;

    iput-object v0, p0, Lo/aQ;->c:[Lo/T;

    .line 32
    const/4 v0, 0x0

    :goto_9
    const/4 v1, 0x4

    if-ge v0, v1, :cond_21

    .line 33
    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    aput-object v2, v1, v0

    .line 34
    aget-object v1, p1, v0

    iget-object v2, p0, Lo/aQ;->c:[Lo/T;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lo/T;->i(Lo/T;)V

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 37
    :cond_21
    iput-object p1, p0, Lo/aQ;->b:[Lo/T;

    .line 38
    new-instance v0, Lo/t;

    invoke-direct {v0, p1}, Lo/t;-><init>([Lo/T;)V

    iput-object v0, p0, Lo/aQ;->d:Lo/t;

    .line 39
    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    invoke-virtual {v0}, Lo/t;->a()Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lo/aQ;->f:Lo/ad;

    .line 40
    iget-object v0, p0, Lo/aQ;->f:Lo/ad;

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    iput-object v0, p0, Lo/aQ;->e:Lo/aR;

    .line 41
    iget-object v0, p0, Lo/aQ;->e:Lo/aR;

    iget-boolean v0, v0, Lo/aR;->a:Z

    iput-boolean v0, p0, Lo/aQ;->a:Z

    .line 42
    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_47

    .line 43
    invoke-direct {p0}, Lo/aQ;->i()V

    .line 45
    :cond_47
    return-void
.end method

.method public static a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x4

    new-array v0, v0, [Lo/T;

    .line 106
    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 107
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 108
    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 109
    const/4 v1, 0x3

    aput-object p2, v0, v1

    .line 110
    new-instance v1, Lo/aQ;

    invoke-direct {v1, v0}, Lo/aQ;-><init>([Lo/T;)V

    return-object v1
.end method

.method private a(Lo/T;Lo/T;I)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const v1, 0x20000001

    const v2, -0x20000001

    .line 80
    iget v0, p2, Lo/T;->a:I

    if-lez v0, :cond_30

    move v0, v1

    .line 81
    :goto_d
    invoke-static {p1, p2, v0}, Lo/aQ;->b(Lo/T;Lo/T;I)I

    move-result v0

    .line 83
    iget v3, p1, Lo/T;->a:I

    iget v4, p2, Lo/T;->a:I

    if-le v3, v4, :cond_32

    .line 85
    iget-object v3, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lo/T;

    invoke-direct {v4, v2, v0}, Lo/T;-><init>(II)V

    aput-object v4, v3, v6

    .line 86
    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v2, v2, p3

    new-instance v3, Lo/T;

    invoke-direct {v3, v1, v0}, Lo/T;-><init>(II)V

    aput-object v3, v2, v5

    .line 92
    :goto_2f
    return-void

    :cond_30
    move v0, v2

    .line 80
    goto :goto_d

    .line 89
    :cond_32
    iget-object v3, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lo/T;

    invoke-direct {v4, v1, v0}, Lo/T;-><init>(II)V

    aput-object v4, v3, v6

    .line 90
    iget-object v1, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v1, v1, p3

    new-instance v3, Lo/T;

    invoke-direct {v3, v2, v0}, Lo/T;-><init>(II)V

    aput-object v3, v1, v5

    goto :goto_2f
.end method

.method private static b(Lo/T;Lo/T;I)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    iget v0, p0, Lo/T;->a:I

    sub-int v0, p2, v0

    int-to-double v0, v0

    iget v2, p1, Lo/T;->a:I

    iget v3, p0, Lo/T;->a:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p1, Lo/T;->b:I

    iget v3, p0, Lo/T;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lo/T;->b:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private i()V
    .registers 10

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 52
    const/4 v0, 0x2

    filled-new-array {v8, v0}, [I

    move-result-object v0

    const-class v1, Lo/T;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lo/T;

    iput-object v0, p0, Lo/aQ;->g:[[Lo/T;

    move v5, v4

    move v0, v4

    move v2, v4

    .line 56
    :goto_16
    const/4 v1, 0x4

    if-ge v5, v1, :cond_5f

    .line 57
    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    aget-object v1, v1, v5

    iget-object v6, p0, Lo/aQ;->b:[Lo/T;

    aget-object v6, v6, v5

    invoke-virtual {v1, v6}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5d

    move v1, v3

    .line 58
    :goto_28
    if-eq v1, v2, :cond_7a

    .line 59
    if-lez v5, :cond_3d

    if-ge v0, v7, :cond_3d

    .line 60
    iget-object v2, p0, Lo/aQ;->b:[Lo/T;

    add-int/lit8 v6, v5, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lo/aQ;->b:[Lo/T;

    aget-object v6, v6, v5

    invoke-direct {p0, v2, v6, v0}, Lo/aQ;->a(Lo/T;Lo/T;I)V

    .line 62
    add-int/lit8 v0, v0, 0x1

    .line 66
    :cond_3d
    :goto_3d
    if-lez v5, :cond_4b

    .line 67
    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v6, v0, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lo/aQ;->c:[Lo/T;

    aget-object v6, v6, v5

    aput-object v6, v2, v3

    .line 69
    :cond_4b
    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v2, v2, v0

    iget-object v6, p0, Lo/aQ;->c:[Lo/T;

    aget-object v6, v6, v5

    aput-object v6, v2, v4

    .line 70
    add-int/lit8 v2, v0, 0x1

    .line 56
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v2

    move v2, v1

    goto :goto_16

    :cond_5d
    move v1, v4

    .line 57
    goto :goto_28

    .line 72
    :cond_5f
    if-ge v0, v8, :cond_6f

    .line 73
    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lo/aQ;->b:[Lo/T;

    aget-object v2, v2, v4

    invoke-direct {p0, v1, v2, v0}, Lo/aQ;->a(Lo/T;Lo/T;I)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    .line 76
    :cond_6f
    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, v7

    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    aget-object v1, v1, v4

    aput-object v1, v0, v3

    .line 77
    return-void

    :cond_7a
    move v1, v2

    goto :goto_3d
.end method


# virtual methods
.method public a(I)Lo/T;
    .registers 3
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lo/aR;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lo/aQ;->e:Lo/aR;

    return-object v0
.end method

.method public a(I[Lo/T;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 182
    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_17

    .line 183
    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, p1

    aget-object v0, v0, v1

    aput-object v0, p2, v1

    .line 184
    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, p1

    aget-object v0, v0, v2

    aput-object v0, p2, v2

    .line 189
    :goto_16
    return-void

    .line 186
    :cond_17
    invoke-virtual {p0, p1}, Lo/aQ;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v1

    .line 187
    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lo/aQ;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v2

    goto :goto_16
.end method

.method public a(Lo/T;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 249
    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-nez v0, :cond_d

    .line 250
    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    invoke-virtual {v0, p1}, Lo/t;->a(Lo/T;)Z

    move-result v0

    .line 258
    :goto_c
    return v0

    :cond_d
    move v3, v2

    move v0, v2

    .line 253
    :goto_f
    const/4 v4, 0x6

    if-ge v3, v4, :cond_29

    .line 254
    iget-object v4, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v4, v4, v3

    aget-object v4, v4, v2

    iget-object v5, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v5, v5, v3

    aget-object v5, v5, v1

    invoke-static {v4, v5, p1}, Lo/V;->b(Lo/T;Lo/T;Lo/T;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 255
    add-int/lit8 v0, v0, 0x1

    .line 253
    :cond_26
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    .line 258
    :cond_29
    if-ne v0, v1, :cond_2d

    move v0, v1

    goto :goto_c

    :cond_2d
    move v0, v2

    goto :goto_c
.end method

.method public a(Lo/ae;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 268
    invoke-virtual {p0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/aR;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 276
    :cond_f
    :goto_f
    return v1

    :cond_10
    move v0, v1

    .line 271
    :goto_11
    invoke-virtual {p1}, Lo/ae;->b()I

    move-result v2

    if-ge v0, v2, :cond_24

    .line 272
    invoke-virtual {p1, v0}, Lo/ae;->a(I)Lo/T;

    move-result-object v2

    invoke-virtual {p0, v2}, Lo/aQ;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 276
    :cond_24
    const/4 v1, 0x1

    goto :goto_f
.end method

.method public b()Lo/ad;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lo/aQ;->f:Lo/ad;

    return-object v0
.end method

.method public c()Lo/ae;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    return-object v0
.end method

.method public d()Lo/T;
    .registers 3

    .prologue
    .line 143
    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public e()Lo/T;
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 231
    if-ne p0, p1, :cond_4

    .line 232
    const/4 v0, 0x1

    .line 238
    :goto_3
    return v0

    .line 234
    :cond_4
    instance-of v0, p1, Lo/aQ;

    if-eqz v0, :cond_13

    .line 235
    check-cast p1, Lo/aQ;

    .line 236
    iget-object v0, p0, Lo/aQ;->b:[Lo/T;

    iget-object v1, p1, Lo/aQ;->b:[Lo/T;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 238
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public f()Lo/T;
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public g()Lo/T;
    .registers 3

    .prologue
    .line 167
    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 177
    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x4

    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 226
    iget-object v0, p0, Lo/aQ;->b:[Lo/T;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
