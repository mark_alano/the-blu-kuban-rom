.class public Lo/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:I

.field private final d:Lo/aj;

.field private final e:[I


# direct methods
.method public constructor <init>(I[BILo/aj;[I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lo/ac;->a:I

    .line 36
    iput-object p2, p0, Lo/ac;->b:[B

    .line 37
    iput p3, p0, Lo/ac;->c:I

    .line 38
    iput-object p4, p0, Lo/ac;->d:Lo/aj;

    .line 39
    iput-object p5, p0, Lo/ac;->e:[I

    .line 40
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/ac;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 54
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 55
    new-array v2, v0, [B

    .line 56
    invoke-interface {p0, v2}, Ljava/io/DataInput;->readFully([B)V

    .line 58
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v3

    .line 61
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 62
    new-array v5, v4, [I

    .line 63
    const/4 v0, 0x0

    :goto_18
    if-ge v0, v4, :cond_23

    .line 64
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v6

    aput v6, v5, v0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 68
    :cond_23
    new-instance v0, Lo/ac;

    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lo/ac;-><init>(I[BILo/aj;[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .registers 2

    .prologue
    .line 108
    sget-object v0, Lo/o;->a:Lo/o;

    return-object v0
.end method

.method public b()[B
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lo/ac;->b:[B

    return-object v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lo/ac;->d:Lo/aj;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 73
    const/4 v0, 0x6

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 93
    iget v0, p0, Lo/ac;->c:I

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lo/ac;->e:[I

    return-object v0
.end method

.method public m()I
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lo/ac;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x20

    return v0
.end method
