.class public Lo/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lo/ai;


# instance fields
.field private final a:I

.field private final b:F

.field private final c:[I

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 35
    new-instance v0, Lo/ai;

    const/high16 v1, 0x3f80

    new-array v2, v3, [I

    invoke-direct {v0, v3, v1, v2, v3}, Lo/ai;-><init>(IF[II)V

    sput-object v0, Lo/ai;->e:Lo/ai;

    return-void
.end method

.method public constructor <init>(IF[II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lo/ai;->a:I

    .line 40
    iput p2, p0, Lo/ai;->b:F

    .line 41
    iput-object p3, p0, Lo/ai;->c:[I

    .line 42
    iput p4, p0, Lo/ai;->d:I

    .line 43
    return-void
.end method

.method public static a()Lo/ai;
    .registers 1

    .prologue
    .line 76
    sget-object v0, Lo/ai;->e:Lo/ai;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;I)Lo/ai;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    .line 50
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    invoke-static {v0}, Lo/O;->a(I)F

    move-result v4

    .line 53
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 54
    new-array v0, v5, [I

    move v1, v2

    .line 55
    :goto_14
    if-ge v1, v5, :cond_1f

    .line 56
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v6

    aput v6, v0, v1

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 58
    :cond_1f
    sget-boolean v1, Lcom/google/googlenav/android/E;->c:Z

    if-eqz v1, :cond_25

    .line 59
    new-array v0, v2, [I

    .line 63
    :cond_25
    array-length v5, v0

    move v1, v2

    :goto_27
    if-ge v1, v5, :cond_2f

    aget v6, v0, v1

    .line 64
    if-nez v6, :cond_39

    .line 65
    new-array v0, v2, [I

    .line 70
    :cond_2f
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    .line 72
    new-instance v2, Lo/ai;

    invoke-direct {v2, v3, v4, v0, v1}, Lo/ai;-><init>(IF[II)V

    return-object v2

    .line 63
    :cond_39
    add-int/lit8 v1, v1, 0x1

    goto :goto_27
.end method


# virtual methods
.method public b()I
    .registers 2

    .prologue
    .line 80
    iget v0, p0, Lo/ai;->a:I

    return v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 84
    iget v0, p0, Lo/ai;->b:F

    return v0
.end method

.method public d()[I
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Lo/ai;->c:[I

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Lo/ai;->c:[I

    if-eqz v0, :cond_b

    iget-object v0, p0, Lo/ai;->c:[I

    array-length v0, v0

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121
    if-ne p0, p1, :cond_6

    move v1, v0

    .line 140
    :cond_5
    :goto_5
    return v1

    .line 124
    :cond_6
    if-eqz p1, :cond_5

    .line 127
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_5

    .line 130
    check-cast p1, Lo/ai;

    .line 131
    iget v2, p0, Lo/ai;->a:I

    iget v3, p1, Lo/ai;->a:I

    if-ne v2, v3, :cond_5

    .line 134
    iget-object v2, p0, Lo/ai;->c:[I

    iget-object v3, p1, Lo/ai;->c:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 137
    iget v2, p0, Lo/ai;->d:I

    iget v3, p1, Lo/ai;->d:I

    if-ne v2, v3, :cond_5

    .line 140
    iget v2, p0, Lo/ai;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/ai;->b:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_3a

    :goto_38
    move v1, v0

    goto :goto_5

    :cond_3a
    move v0, v1

    goto :goto_38
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Lo/ai;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 100
    iget v0, p0, Lo/ai;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public h()I
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lo/ai;->c:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 110
    .line 112
    iget v0, p0, Lo/ai;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 113
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/ai;->c:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ai;->d:I

    add-int/2addr v0, v1

    .line 115
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ai;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    const-string v1, "Stroke{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "color="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ai;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ai;->b:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dashes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lo/ai;->c:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", endCaps="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    iget v1, p0, Lo/ai;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_47

    .line 156
    const-string v1, "S"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_47
    iget v1, p0, Lo/ai;->d:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_52

    .line 159
    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_52
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
