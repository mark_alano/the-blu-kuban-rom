.class public final Lo/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static b:Lo/aj;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:[I

.field private final f:[Lo/ai;

.field private final g:Lo/ao;

.field private final h:Lo/an;

.field private final i:Lo/ai;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 32
    new-array v0, v1, [I

    sput-object v0, Lo/aj;->a:[I

    .line 34
    new-instance v0, Lo/aj;

    sget-object v3, Lo/aj;->a:[I

    new-array v4, v1, [Lo/ai;

    invoke-static {}, Lo/ao;->a()Lo/ao;

    move-result-object v5

    invoke-static {}, Lo/an;->a()Lo/an;

    move-result-object v6

    invoke-static {}, Lo/ai;->a()Lo/ai;

    move-result-object v7

    move v2, v1

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, Lo/aj;->b:Lo/aj;

    return-void
.end method

.method public constructor <init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput p1, p0, Lo/aj;->c:I

    .line 129
    iput p2, p0, Lo/aj;->d:I

    .line 130
    iput-object p3, p0, Lo/aj;->e:[I

    .line 131
    iput-object p4, p0, Lo/aj;->f:[Lo/ai;

    .line 132
    iput-object p5, p0, Lo/aj;->g:Lo/ao;

    .line 133
    iput-object p6, p0, Lo/aj;->h:Lo/an;

    .line 134
    iput-object p7, p0, Lo/aj;->i:Lo/ai;

    .line 135
    return-void
.end method

.method public static a()Lo/aj;
    .registers 1

    .prologue
    .line 118
    sget-object v0, Lo/aj;->b:Lo/aj;

    return-object v0
.end method

.method public static a(ILjava/io/DataInput;I)Lo/aj;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 74
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    .line 78
    invoke-static {v2}, Lo/aj;->c(I)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 79
    invoke-static {p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 80
    new-array v3, v5, [I

    move v4, v1

    .line 81
    :goto_13
    if-ge v4, v5, :cond_1f

    .line 82
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v6

    aput v6, v3, v4

    .line 81
    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    :cond_1e
    move-object v3, v0

    .line 88
    :cond_1f
    invoke-static {v2}, Lo/aj;->d(I)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 89
    invoke-static {p1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 90
    new-array v4, v5, [Lo/ai;

    .line 91
    :goto_2b
    if-ge v1, v5, :cond_37

    .line 92
    invoke-static {p1, p2}, Lo/ai;->a(Ljava/io/DataInput;I)Lo/ai;

    move-result-object v6

    aput-object v6, v4, v1

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_2b

    :cond_36
    move-object v4, v0

    .line 98
    :cond_37
    invoke-static {v2}, Lo/aj;->e(I)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 99
    invoke-static {p1, p2}, Lo/ao;->a(Ljava/io/DataInput;I)Lo/ao;

    move-result-object v5

    .line 104
    :goto_41
    invoke-static {v2}, Lo/aj;->f(I)Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 105
    invoke-static {p1, p2}, Lo/an;->a(Ljava/io/DataInput;I)Lo/an;

    move-result-object v6

    .line 110
    :goto_4b
    invoke-static {v2}, Lo/aj;->g(I)Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 111
    invoke-static {p1, p2}, Lo/ai;->a(Ljava/io/DataInput;I)Lo/ai;

    move-result-object v7

    .line 113
    :goto_55
    new-instance v0, Lo/aj;

    move v1, p0

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    return-object v0

    :cond_5c
    move-object v7, v0

    goto :goto_55

    :cond_5e
    move-object v6, v0

    goto :goto_4b

    :cond_60
    move-object v5, v0

    goto :goto_41
.end method

.method private static a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 271
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    if-nez p1, :cond_10

    .line 273
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :goto_f
    return-void

    .line 276
    :cond_10
    const-string v0, "["

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const/4 v0, 0x1

    .line 278
    array-length v3, p1

    move v2, v1

    :goto_18
    if-ge v2, v3, :cond_2f

    aget v4, p1, v2

    .line 279
    if-eqz v0, :cond_29

    move v0, v1

    .line 284
    :goto_1f
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 282
    :cond_29
    const-string v5, ","

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1f

    .line 286
    :cond_2f
    const-string v0, "]"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_f
.end method

.method private static c(I)Z
    .registers 2
    .parameter

    .prologue
    .line 173
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static d(I)Z
    .registers 2
    .parameter

    .prologue
    .line 177
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static e(I)Z
    .registers 2
    .parameter

    .prologue
    .line 181
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static f(I)Z
    .registers 2
    .parameter

    .prologue
    .line 185
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static g(I)Z
    .registers 2
    .parameter

    .prologue
    .line 189
    const/16 v0, 0x10

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lo/aj;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    array-length v0, v0

    goto :goto_5
.end method

.method public b(I)Lo/ai;
    .registers 3
    .parameter

    .prologue
    .line 218
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, Lo/aj;->e:[I

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lo/aj;->e:[I

    array-length v0, v0

    goto :goto_5
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 197
    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->d(I)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 201
    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->e(I)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 297
    if-ne p0, p1, :cond_5

    .line 340
    :cond_4
    :goto_4
    return v0

    .line 300
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 301
    goto :goto_4

    .line 303
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 304
    goto :goto_4

    .line 306
    :cond_15
    check-cast p1, Lo/aj;

    .line 307
    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    if-nez v2, :cond_21

    .line 308
    iget-object v2, p1, Lo/aj;->i:Lo/ai;

    if-eqz v2, :cond_2d

    move v0, v1

    .line 309
    goto :goto_4

    .line 311
    :cond_21
    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    iget-object v3, p1, Lo/aj;->i:Lo/ai;

    invoke-virtual {v2, v3}, Lo/ai;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    move v0, v1

    .line 312
    goto :goto_4

    .line 314
    :cond_2d
    iget v2, p0, Lo/aj;->d:I

    iget v3, p1, Lo/aj;->d:I

    if-eq v2, v3, :cond_35

    move v0, v1

    .line 315
    goto :goto_4

    .line 317
    :cond_35
    iget-object v2, p0, Lo/aj;->e:[I

    iget-object v3, p1, Lo/aj;->e:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_41

    move v0, v1

    .line 318
    goto :goto_4

    .line 320
    :cond_41
    iget v2, p0, Lo/aj;->c:I

    iget v3, p1, Lo/aj;->c:I

    if-eq v2, v3, :cond_49

    move v0, v1

    .line 321
    goto :goto_4

    .line 323
    :cond_49
    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    iget-object v3, p1, Lo/aj;->f:[Lo/ai;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_55

    move v0, v1

    .line 324
    goto :goto_4

    .line 326
    :cond_55
    iget-object v2, p0, Lo/aj;->h:Lo/an;

    if-nez v2, :cond_5f

    .line 327
    iget-object v2, p1, Lo/aj;->h:Lo/an;

    if-eqz v2, :cond_6b

    move v0, v1

    .line 328
    goto :goto_4

    .line 330
    :cond_5f
    iget-object v2, p0, Lo/aj;->h:Lo/an;

    iget-object v3, p1, Lo/aj;->h:Lo/an;

    invoke-virtual {v2, v3}, Lo/an;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6b

    move v0, v1

    .line 331
    goto :goto_4

    .line 333
    :cond_6b
    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    if-nez v2, :cond_75

    .line 334
    iget-object v2, p1, Lo/aj;->g:Lo/ao;

    if-eqz v2, :cond_4

    move v0, v1

    .line 335
    goto :goto_4

    .line 337
    :cond_75
    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    iget-object v3, p1, Lo/aj;->g:Lo/ao;

    invoke-virtual {v2, v3}, Lo/ao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 338
    goto :goto_4
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 205
    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->f(I)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 209
    iget v0, p0, Lo/aj;->d:I

    invoke-static {v0}, Lo/aj;->g(I)Z

    move-result v0

    return v0
.end method

.method public h()Lo/ao;
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lo/aj;->g:Lo/ao;

    return-object v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 235
    .line 237
    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    if-nez v0, :cond_34

    move v0, v1

    :goto_6
    add-int/lit8 v0, v0, 0x1f

    .line 238
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/aj;->d:I

    add-int/2addr v0, v2

    .line 239
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->e:[I

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 240
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/aj;->c:I

    add-int/2addr v0, v2

    .line 241
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 242
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lo/aj;->h:Lo/an;

    if-nez v0, :cond_3b

    move v0, v1

    :goto_2b
    add-int/2addr v0, v2

    .line 243
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    if-nez v2, :cond_42

    :goto_32
    add-int/2addr v0, v1

    .line 244
    return v0

    .line 237
    :cond_34
    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    invoke-virtual {v0}, Lo/ai;->hashCode()I

    move-result v0

    goto :goto_6

    .line 242
    :cond_3b
    iget-object v0, p0, Lo/aj;->h:Lo/an;

    invoke-virtual {v0}, Lo/an;->hashCode()I

    move-result v0

    goto :goto_2b

    .line 243
    :cond_42
    iget-object v1, p0, Lo/aj;->g:Lo/ao;

    invoke-virtual {v1}, Lo/ao;->hashCode()I

    move-result v1

    goto :goto_32
.end method

.method public i()Lo/an;
    .registers 2

    .prologue
    .line 226
    iget-object v0, p0, Lo/aj;->h:Lo/an;

    return-object v0
.end method

.method public j()Lo/ai;
    .registers 2

    .prologue
    .line 230
    iget-object v0, p0, Lo/aj;->i:Lo/ai;

    return-object v0
.end method

.method public k()I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 344
    iget-object v0, p0, Lo/aj;->e:[I

    if-nez v0, :cond_1c

    move v0, v1

    .line 346
    :goto_6
    iget-object v2, p0, Lo/aj;->f:[Lo/ai;

    if-eqz v2, :cond_22

    .line 347
    iget-object v4, p0, Lo/aj;->f:[Lo/ai;

    array-length v5, v4

    move v2, v1

    :goto_e
    if-ge v2, v5, :cond_22

    aget-object v3, v4, v2

    .line 348
    invoke-virtual {v3}, Lo/ai;->h()I

    move-result v3

    add-int/2addr v3, v1

    .line 347
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_e

    .line 344
    :cond_1c
    iget-object v0, p0, Lo/aj;->e:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    goto :goto_6

    .line 351
    :cond_22
    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    invoke-static {v2}, Lo/O;->a(Lo/ai;)I

    move-result v2

    .line 352
    add-int/lit8 v0, v0, 0x38

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    const-string v0, "Style{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lo/aj;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string v0, "fillColors"

    iget-object v2, p0, Lo/aj;->e:[I

    invoke-static {v0, v2, v1}, Lo/aj;->a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V

    .line 253
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string v0, ", components="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lo/aj;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", strokes="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    if-nez v0, :cond_71

    const/4 v0, 0x0

    :goto_3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", textStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->g:Lo/ao;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", textBoxStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->h:Lo/an;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", arrowStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lo/aj;->i:Lo/ai;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 254
    :cond_71
    iget-object v0, p0, Lo/aj;->f:[Lo/ai;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_3f
.end method
