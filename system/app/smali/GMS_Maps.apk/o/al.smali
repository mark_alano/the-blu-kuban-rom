.class public Lo/al;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lo/al;->a:Ljava/util/ArrayList;

    .line 17
    return-void
.end method

.method public static a(Ljava/io/DataInput;I)Lo/al;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 26
    new-instance v1, Lo/al;

    invoke-direct {v1}, Lo/al;-><init>()V

    .line 27
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 28
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_18

    .line 29
    iget-object v3, v1, Lo/al;->a:Ljava/util/ArrayList;

    invoke-static {v0, p0, p1}, Lo/aj;->a(ILjava/io/DataInput;I)Lo/aj;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 31
    :cond_18
    return-object v1
.end method


# virtual methods
.method public a()Lo/aj;
    .registers 2

    .prologue
    .line 61
    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lo/aj;
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lo/al;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_d

    .line 55
    invoke-static {}, Lo/aj;->a()Lo/aj;

    move-result-object v0

    .line 57
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lo/al;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aj;

    goto :goto_c
.end method
