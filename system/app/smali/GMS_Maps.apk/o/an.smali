.class public Lo/an;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Lo/an;


# instance fields
.field private final a:I

.field private final b:Lo/ai;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 25
    new-instance v0, Lo/an;

    const/4 v1, 0x0

    invoke-static {}, Lo/ai;->a()Lo/ai;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, Lo/an;->c:Lo/an;

    return-void
.end method

.method public constructor <init>(ILo/ai;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lo/an;->a:I

    .line 30
    iput-object p2, p0, Lo/an;->b:Lo/ai;

    .line 31
    return-void
.end method

.method public static a()Lo/an;
    .registers 1

    .prologue
    .line 40
    sget-object v0, Lo/an;->c:Lo/an;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;I)Lo/an;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 35
    invoke-static {p0, p1}, Lo/ai;->a(Ljava/io/DataInput;I)Lo/ai;

    move-result-object v1

    .line 36
    new-instance v2, Lo/an;

    invoke-direct {v2, v0, v1}, Lo/an;-><init>(ILo/ai;)V

    return-object v2
.end method


# virtual methods
.method public b()I
    .registers 2

    .prologue
    .line 44
    iget v0, p0, Lo/an;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p0, p1, :cond_5

    .line 82
    :cond_4
    :goto_4
    return v0

    .line 65
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 66
    goto :goto_4

    .line 68
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 69
    goto :goto_4

    .line 71
    :cond_15
    check-cast p1, Lo/an;

    .line 72
    iget v2, p0, Lo/an;->a:I

    iget v3, p1, Lo/an;->a:I

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 73
    goto :goto_4

    .line 75
    :cond_1f
    iget-object v2, p0, Lo/an;->b:Lo/ai;

    if-nez v2, :cond_29

    .line 76
    iget-object v2, p1, Lo/an;->b:Lo/ai;

    if-eqz v2, :cond_4

    move v0, v1

    .line 77
    goto :goto_4

    .line 79
    :cond_29
    iget-object v2, p0, Lo/an;->b:Lo/ai;

    iget-object v3, p1, Lo/an;->b:Lo/ai;

    invoke-virtual {v2, v3}, Lo/ai;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 80
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 53
    .line 55
    iget v0, p0, Lo/an;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 56
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lo/an;->b:Lo/ai;

    if-nez v0, :cond_d

    const/4 v0, 0x0

    :goto_b
    add-int/2addr v0, v1

    .line 57
    return v0

    .line 56
    :cond_d
    iget-object v0, p0, Lo/an;->b:Lo/ai;

    invoke-virtual {v0}, Lo/ai;->hashCode()I

    move-result v0

    goto :goto_b
.end method
