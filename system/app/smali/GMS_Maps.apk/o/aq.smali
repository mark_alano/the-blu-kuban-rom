.class public Lo/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Lo/aB;

.field private h:Lo/aq;


# direct methods
.method public constructor <init>(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lo/aq;-><init>(IIILo/aB;)V

    .line 74
    return-void
.end method

.method public constructor <init>(IIILo/aB;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x2000

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lo/aq;->h:Lo/aq;

    .line 60
    iput p1, p0, Lo/aq;->d:I

    .line 61
    iput p2, p0, Lo/aq;->e:I

    .line 62
    iput p3, p0, Lo/aq;->f:I

    .line 63
    if-nez p4, :cond_15

    new-instance p4, Lo/aB;

    invoke-direct {p4}, Lo/aB;-><init>()V

    :cond_15
    iput-object p4, p0, Lo/aq;->g:Lo/aB;

    .line 66
    rsub-int/lit8 v0, p1, 0x12

    iput v0, p0, Lo/aq;->c:I

    .line 67
    const/high16 v0, 0x4000

    shr-int/2addr v0, p1

    .line 68
    iget v1, p0, Lo/aq;->e:I

    mul-int/2addr v1, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lo/aq;->a:I

    .line 69
    iget v1, p0, Lo/aq;->f:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    sub-int/2addr v0, v2

    neg-int v0, v0

    iput v0, p0, Lo/aq;->b:I

    .line 70
    return-void
.end method

.method public static a(Lo/aR;I)Ljava/util/ArrayList;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 332
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v3, 0x0

    .line 341
    if-gez p1, :cond_a

    .line 344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 394
    :cond_9
    :goto_9
    return-object v0

    .line 346
    :cond_a
    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v5

    .line 349
    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v6

    .line 352
    invoke-virtual {v5}, Lo/aq;->c()I

    move-result v1

    .line 353
    invoke-virtual {v5}, Lo/aq;->d()I

    move-result v2

    .line 354
    invoke-virtual {v6}, Lo/aq;->c()I

    move-result v7

    .line 355
    invoke-virtual {v6}, Lo/aq;->d()I

    move-result v8

    .line 357
    const/4 v0, 0x1

    shl-int v9, v0, p1

    .line 359
    if-le v1, v7, :cond_5e

    .line 360
    sub-int v0, v9, v1

    add-int/2addr v0, v7

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    .line 366
    :goto_56
    if-gez v4, :cond_69

    .line 367
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_9

    .line 362
    :cond_5e
    sub-int v0, v7, v1

    add-int/lit8 v0, v0, 0x1

    sub-int v4, v8, v2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    move v4, v0

    goto :goto_56

    .line 370
    :cond_69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 371
    if-gt v4, v10, :cond_79

    .line 372
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    if-ne v4, v10, :cond_9

    .line 374
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 376
    :cond_79
    if-le v1, v7, :cond_a3

    move v4, v1

    .line 377
    :goto_7c
    if-ge v4, v9, :cond_93

    move v1, v2

    .line 378
    :goto_7f
    if-gt v1, v8, :cond_8c

    .line 379
    new-instance v5, Lo/aq;

    invoke-direct {v5, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    add-int/lit8 v1, v1, 0x1

    goto :goto_7f

    .line 377
    :cond_8c
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7c

    .line 382
    :cond_90
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    :cond_93
    if-gt v3, v7, :cond_9

    move v1, v2

    .line 383
    :goto_96
    if-gt v1, v8, :cond_90

    .line 384
    new-instance v4, Lo/aq;

    invoke-direct {v4, p1, v3, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    add-int/lit8 v1, v1, 0x1

    goto :goto_96

    :cond_a3
    move v3, v1

    .line 388
    :goto_a4
    if-gt v3, v7, :cond_9

    move v1, v2

    .line 389
    :goto_a7
    if-gt v1, v8, :cond_b4

    .line 390
    new-instance v4, Lo/aq;

    invoke-direct {v4, p1, v3, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    add-int/lit8 v1, v1, 0x1

    goto :goto_a7

    .line 388
    :cond_b4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_a4
.end method

.method public static a(IIILo/aB;)Lo/aq;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x2000

    const/16 v1, 0x1e

    const/4 v0, 0x0

    .line 139
    if-gtz p0, :cond_e

    .line 141
    new-instance v1, Lo/aq;

    invoke-direct {v1, v0, v0, v0}, Lo/aq;-><init>(III)V

    move-object v0, v1

    .line 165
    :goto_d
    return-object v0

    .line 142
    :cond_e
    if-le p0, v1, :cond_11

    move p0, v1

    .line 145
    :cond_11
    rsub-int/lit8 v1, p0, 0x1e

    .line 147
    add-int v2, p1, v4

    shr-int/2addr v2, v1

    .line 148
    neg-int v3, p2

    add-int/2addr v3, v4

    shr-int v1, v3, v1

    .line 150
    const/4 v3, 0x1

    shl-int/2addr v3, p0

    .line 153
    if-gez v2, :cond_28

    .line 154
    add-int/2addr v2, v3

    .line 160
    :cond_1f
    :goto_1f
    if-gez v1, :cond_2c

    .line 165
    :goto_21
    new-instance v1, Lo/aq;

    invoke-direct {v1, p0, v2, v0, p3}, Lo/aq;-><init>(IIILo/aB;)V

    move-object v0, v1

    goto :goto_d

    .line 155
    :cond_28
    if-lt v2, v3, :cond_1f

    .line 156
    sub-int/2addr v2, v3

    goto :goto_1f

    .line 162
    :cond_2c
    if-lt v1, v3, :cond_31

    .line 163
    add-int/lit8 v0, v3, -0x1

    goto :goto_21

    :cond_31
    move v0, v1

    goto :goto_21
.end method

.method public static a(ILo/T;)Lo/aq;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v1

    invoke-static {p0, v0, v1}, Lo/aq;->b(III)Lo/aq;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/aq;
    .registers 5
    .parameter

    .prologue
    .line 549
    new-instance v0, Lo/aq;

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lo/aq;-><init>(III)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lo/aq;
    .registers 5
    .parameter

    .prologue
    .line 324
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 325
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 326
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 327
    new-instance v3, Lo/aq;

    invoke-direct {v3, v0, v1, v2}, Lo/aq;-><init>(III)V

    return-object v3
.end method

.method public static b(Lo/aR;I)Ljava/util/ArrayList;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 399
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/aq;->b(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lo/aR;ILo/aB;)Ljava/util/ArrayList;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 409
    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lo/aq;->b(IIILo/aB;)Lo/aq;

    move-result-object v0

    .line 412
    invoke-virtual {p0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lo/aR;->g()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v1, v2, p2}, Lo/aq;->b(IIILo/aB;)Lo/aq;

    move-result-object v2

    .line 415
    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v1

    .line 416
    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v3

    .line 417
    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v5

    .line 418
    invoke-virtual {v2}, Lo/aq;->d()I

    move-result v6

    .line 420
    invoke-virtual {v0}, Lo/aq;->e()I

    move-result v4

    invoke-virtual {v0}, Lo/aq;->f()I

    move-result v0

    invoke-static {v4, v0}, Lo/T;->f(II)Z

    move-result v0

    if-nez v0, :cond_5e

    invoke-virtual {v2}, Lo/aq;->e()I

    move-result v0

    invoke-virtual {v2}, Lo/aq;->f()I

    move-result v2

    invoke-static {v0, v2}, Lo/T;->f(II)Z

    move-result v0

    if-nez v0, :cond_5e

    .line 422
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 455
    :cond_5d
    return-object v0

    .line 425
    :cond_5e
    const/4 v0, 0x1

    shl-int v2, v0, p1

    .line 426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 428
    if-le v1, v5, :cond_af

    move v4, v1

    .line 429
    :goto_69
    if-ge v4, v2, :cond_8b

    move v1, v3

    .line 430
    :goto_6c
    if-gez v1, :cond_79

    .line 431
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    add-int/lit8 v1, v1, 0x1

    goto :goto_6c

    :cond_79
    move v1, v2

    .line 433
    :goto_7a
    if-gt v1, v6, :cond_87

    .line 434
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    add-int/lit8 v1, v1, 0x1

    goto :goto_7a

    .line 429
    :cond_87
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_69

    .line 437
    :cond_8b
    const/4 v1, 0x0

    move v4, v1

    :goto_8d
    if-gt v4, v5, :cond_5d

    move v1, v3

    .line 438
    :goto_90
    if-gez v1, :cond_9d

    .line 439
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    add-int/lit8 v1, v1, 0x1

    goto :goto_90

    :cond_9d
    move v1, v2

    .line 441
    :goto_9e
    if-gt v1, v6, :cond_ab

    .line 442
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    add-int/lit8 v1, v1, 0x1

    goto :goto_9e

    .line 437
    :cond_ab
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8d

    :cond_af
    move v4, v1

    .line 446
    :goto_b0
    if-gt v4, v5, :cond_5d

    move v1, v3

    .line 447
    :goto_b3
    if-gez v1, :cond_c0

    .line 448
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    add-int/lit8 v1, v1, 0x1

    goto :goto_b3

    :cond_c0
    move v1, v2

    .line 450
    :goto_c1
    if-gt v1, v6, :cond_ce

    .line 451
    new-instance v7, Lo/aq;

    invoke-direct {v7, p1, v4, v1, p2}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    add-int/lit8 v1, v1, 0x1

    goto :goto_c1

    .line 446
    :cond_ce
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_b0
.end method

.method public static b(III)Lo/aq;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x2000

    .line 98
    if-ltz p0, :cond_e

    const/16 v0, 0x1e

    if-gt p0, v0, :cond_e

    const/high16 v0, -0x2000

    if-le p2, v0, :cond_e

    if-le p2, v3, :cond_10

    .line 101
    :cond_e
    const/4 v0, 0x0

    .line 116
    :goto_f
    return-object v0

    .line 104
    :cond_10
    rsub-int/lit8 v1, p0, 0x1e

    .line 106
    add-int v0, p1, v3

    shr-int/2addr v0, v1

    .line 107
    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v1

    .line 110
    const/4 v1, 0x1

    shl-int/2addr v1, p0

    .line 111
    if-gez v0, :cond_24

    .line 112
    add-int/2addr v0, v1

    .line 116
    :cond_1d
    :goto_1d
    new-instance v1, Lo/aq;

    invoke-direct {v1, p0, v0, v2}, Lo/aq;-><init>(III)V

    move-object v0, v1

    goto :goto_f

    .line 113
    :cond_24
    if-lt v0, v1, :cond_1d

    .line 114
    sub-int/2addr v0, v1

    goto :goto_1d
.end method

.method private static b(IIILo/aB;)Lo/aq;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x2000

    const/16 v0, 0x1e

    const/4 v1, 0x0

    .line 181
    if-gtz p0, :cond_d

    .line 183
    new-instance v0, Lo/aq;

    invoke-direct {v0, v1, v1, v1}, Lo/aq;-><init>(III)V

    .line 191
    :goto_c
    return-object v0

    .line 184
    :cond_d
    if-le p0, v0, :cond_10

    move p0, v0

    .line 187
    :cond_10
    rsub-int/lit8 v0, p0, 0x1e

    .line 189
    add-int v1, p1, v3

    shr-int/2addr v1, v0

    .line 190
    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int/2addr v2, v0

    .line 191
    new-instance v0, Lo/aq;

    invoke-direct {v0, p0, v1, v2, p3}, Lo/aq;-><init>(IIILo/aB;)V

    goto :goto_c
.end method

.method public static b(ILo/T;)Lo/aq;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lo/aq;->a(IIILo/aB;)Lo/aq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lo/aq;)I
    .registers 4
    .parameter

    .prologue
    .line 506
    iget v0, p0, Lo/aq;->d:I

    iget v1, p1, Lo/aq;->d:I

    if-ne v0, v1, :cond_27

    .line 507
    iget v0, p0, Lo/aq;->e:I

    iget v1, p1, Lo/aq;->e:I

    if-ne v0, v1, :cond_21

    .line 508
    iget v0, p0, Lo/aq;->f:I

    iget v1, p1, Lo/aq;->f:I

    if-ne v0, v1, :cond_1b

    .line 509
    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    iget-object v1, p1, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, v1}, Lo/aB;->a(Lo/aB;)I

    move-result v0

    .line 517
    :goto_1a
    return v0

    .line 511
    :cond_1b
    iget v0, p0, Lo/aq;->f:I

    iget v1, p1, Lo/aq;->f:I

    sub-int/2addr v0, v1

    goto :goto_1a

    .line 514
    :cond_21
    iget v0, p0, Lo/aq;->e:I

    iget v1, p1, Lo/aq;->e:I

    sub-int/2addr v0, v1

    goto :goto_1a

    .line 517
    :cond_27
    iget v0, p0, Lo/aq;->d:I

    iget v1, p1, Lo/aq;->d:I

    sub-int/2addr v0, v1

    goto :goto_1a
.end method

.method public a()Lo/aq;
    .registers 6

    .prologue
    .line 87
    iget-object v0, p0, Lo/aq;->h:Lo/aq;

    if-nez v0, :cond_12

    .line 88
    new-instance v0, Lo/aq;

    iget v1, p0, Lo/aq;->d:I

    iget v2, p0, Lo/aq;->e:I

    iget v3, p0, Lo/aq;->f:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lo/aq;-><init>(IIILo/aB;)V

    iput-object v0, p0, Lo/aq;->h:Lo/aq;

    .line 90
    :cond_12
    iget-object v0, p0, Lo/aq;->h:Lo/aq;

    return-object v0
.end method

.method public a(I)Lo/aq;
    .registers 5
    .parameter

    .prologue
    .line 255
    iget v0, p0, Lo/aq;->d:I

    sub-int/2addr v0, p1

    .line 256
    if-gtz v0, :cond_6

    .line 259
    :goto_5
    return-object p0

    :cond_6
    iget v1, p0, Lo/aq;->e:I

    shr-int/2addr v1, v0

    iget v2, p0, Lo/aq;->f:I

    shr-int v0, v2, v0

    invoke-virtual {p0, p1, v1, v0}, Lo/aq;->a(III)Lo/aq;

    move-result-object p0

    goto :goto_5
.end method

.method public a(III)Lo/aq;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    new-instance v0, Lo/aq;

    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-direct {v0, p1, p2, p3, v1}, Lo/aq;-><init>(IIILo/aB;)V

    return-object v0
.end method

.method public a(LA/c;)Lo/aq;
    .registers 3
    .parameter

    .prologue
    .line 537
    invoke-virtual {p0}, Lo/aq;->k()Lo/aB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/aB;->a(LA/c;)Lo/aB;

    move-result-object v0

    invoke-virtual {p0, v0}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aB;)Lo/aq;
    .registers 6
    .parameter

    .prologue
    .line 527
    new-instance v0, Lo/aq;

    iget v1, p0, Lo/aq;->d:I

    iget v2, p0, Lo/aq;->e:I

    iget v3, p0, Lo/aq;->f:I

    invoke-direct {v0, v1, v2, v3, p1}, Lo/aq;-><init>(IIILo/aB;)V

    return-object v0
.end method

.method public a(Lo/av;)Lo/at;
    .registers 3
    .parameter

    .prologue
    .line 493
    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, p1}, Lo/aB;->a(Lo/av;)Lo/at;

    move-result-object v0

    return-object v0
.end method

.method public a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 562
    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, p1, p2}, Lo/aB;->a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 563
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 313
    iget v0, p0, Lo/aq;->d:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    .line 314
    iget v0, p0, Lo/aq;->e:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    .line 315
    iget v0, p0, Lo/aq;->f:I

    invoke-static {p1, v0}, Lo/aG;->a(Ljava/io/DataOutput;I)V

    .line 316
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 195
    iget v0, p0, Lo/aq;->d:I

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 199
    iget v0, p0, Lo/aq;->e:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 31
    check-cast p1, Lo/aq;

    invoke-virtual {p0, p1}, Lo/aq;->a(Lo/aq;)I

    move-result v0

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 203
    iget v0, p0, Lo/aq;->f:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 208
    iget v0, p0, Lo/aq;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 460
    if-ne p0, p1, :cond_5

    .line 461
    const/4 v0, 0x1

    .line 478
    :cond_4
    :goto_4
    return v0

    .line 463
    :cond_5
    instance-of v1, p1, Lo/aq;

    if-eqz v1, :cond_4

    .line 467
    check-cast p1, Lo/aq;

    .line 469
    iget v1, p0, Lo/aq;->e:I

    iget v2, p1, Lo/aq;->e:I

    if-ne v1, v2, :cond_4

    .line 472
    iget v1, p0, Lo/aq;->f:I

    iget v2, p1, Lo/aq;->f:I

    if-ne v1, v2, :cond_4

    .line 475
    iget v1, p0, Lo/aq;->d:I

    iget v2, p1, Lo/aq;->d:I

    if-ne v1, v2, :cond_4

    .line 478
    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    iget-object v1, p1, Lo/aq;->g:Lo/aB;

    invoke-virtual {v0, v1}, Lo/aB;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public f()I
    .registers 2

    .prologue
    .line 213
    iget v0, p0, Lo/aq;->b:I

    return v0
.end method

.method public g()Lo/T;
    .registers 4

    .prologue
    .line 227
    new-instance v0, Lo/T;

    iget v1, p0, Lo/aq;->a:I

    iget v2, p0, Lo/aq;->b:I

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    return-object v0
.end method

.method public h()Lo/T;
    .registers 5

    .prologue
    .line 232
    const/high16 v0, 0x4000

    iget v1, p0, Lo/aq;->d:I

    shr-int/2addr v0, v1

    .line 233
    new-instance v1, Lo/T;

    iget v2, p0, Lo/aq;->a:I

    add-int/2addr v2, v0

    iget v3, p0, Lo/aq;->b:I

    add-int/2addr v0, v3

    invoke-direct {v1, v2, v0}, Lo/T;-><init>(II)V

    return-object v1
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 483
    iget v0, p0, Lo/aq;->d:I

    .line 484
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/aq;->e:I

    add-int/2addr v0, v1

    .line 485
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/aq;->f:I

    add-int/2addr v0, v1

    .line 486
    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1}, Lo/aB;->b()Z

    move-result v1

    if-nez v1, :cond_1d

    .line 487
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1}, Lo/aB;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_1d
    return v0
.end method

.method public i()Lo/ad;
    .registers 7

    .prologue
    .line 243
    const/high16 v0, 0x4000

    iget v1, p0, Lo/aq;->d:I

    shr-int/2addr v0, v1

    .line 244
    new-instance v1, Lo/ad;

    new-instance v2, Lo/T;

    iget v3, p0, Lo/aq;->a:I

    iget v4, p0, Lo/aq;->b:I

    invoke-direct {v2, v3, v4}, Lo/T;-><init>(II)V

    new-instance v3, Lo/T;

    iget v4, p0, Lo/aq;->a:I

    add-int/2addr v4, v0

    iget v5, p0, Lo/aq;->b:I

    add-int/2addr v0, v5

    invoke-direct {v3, v4, v0}, Lo/T;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v1
.end method

.method public j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 541
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->H:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 542
    const/4 v1, 0x1

    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 543
    const/4 v1, 0x2

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 544
    const/4 v1, 0x3

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 545
    return-object v0
.end method

.method public k()Lo/aB;
    .registers 2

    .prologue
    .line 569
    iget-object v0, p0, Lo/aq;->g:Lo/aB;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 499
    iget v1, p0, Lo/aq;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/aq;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/aq;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lo/aq;->g:Lo/aB;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
