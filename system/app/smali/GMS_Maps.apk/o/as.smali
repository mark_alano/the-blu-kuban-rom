.class public abstract Lo/aS;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Lo/ae;)Z
    .registers 14
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p0}, Lo/aS;->h()I

    move-result v7

    .line 116
    invoke-virtual {p1}, Lo/ae;->b()I

    move-result v8

    .line 117
    if-eqz v7, :cond_e

    if-nez v8, :cond_10

    :cond_e
    move v0, v1

    .line 135
    :cond_f
    :goto_f
    return v0

    .line 120
    :cond_10
    const/4 v2, 0x2

    new-array v9, v2, [Lo/T;

    .line 121
    invoke-virtual {p1}, Lo/ae;->c()Lo/T;

    move-result-object v4

    move v6, v1

    .line 122
    :goto_18
    if-ge v6, v7, :cond_37

    .line 123
    invoke-virtual {p0, v6, v9}, Lo/aS;->a(I[Lo/T;)V

    move v2, v1

    move-object v3, v4

    .line 125
    :goto_1f
    if-ge v2, v8, :cond_33

    .line 126
    invoke-virtual {p1, v2}, Lo/ae;->a(I)Lo/T;

    move-result-object v5

    .line 127
    aget-object v10, v9, v1

    aget-object v11, v9, v0

    invoke-static {v10, v11, v3, v5}, Lo/V;->b(Lo/T;Lo/T;Lo/T;Lo/T;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 125
    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    goto :goto_1f

    .line 122
    :cond_33
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_18

    :cond_37
    move v0, v1

    .line 135
    goto :goto_f
.end method


# virtual methods
.method public abstract a(I)Lo/T;
.end method

.method public a()Lo/aR;
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lo/aS;->c()Lo/ae;

    move-result-object v0

    invoke-virtual {v0}, Lo/ae;->a()Lo/ad;

    move-result-object v0

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(I[Lo/T;)V
.end method

.method public abstract a(Lo/T;)Z
.end method

.method public a(Lo/ae;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-virtual {p0}, Lo/aS;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/aR;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 87
    :cond_f
    :goto_f
    return v1

    :cond_10
    move v0, v1

    .line 82
    :goto_11
    invoke-virtual {p1}, Lo/ae;->b()I

    move-result v2

    if-ge v0, v2, :cond_24

    .line 83
    invoke-virtual {p1, v0}, Lo/ae;->a(I)Lo/T;

    move-result-object v2

    invoke-virtual {p0, v2}, Lo/aS;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 87
    :cond_24
    invoke-direct {p0, p1}, Lo/aS;->c(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v1, 0x1

    goto :goto_f
.end method

.method public b(Lo/ae;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0}, Lo/aS;->a()Lo/aR;

    move-result-object v1

    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/aR;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_2a

    invoke-virtual {p1, v0}, Lo/ae;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {p0, v1}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-nez v1, :cond_29

    invoke-virtual {p0, v0}, Lo/aS;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/ae;->a(Lo/T;)Z

    move-result v1

    if-nez v1, :cond_29

    invoke-direct {p0, p1}, Lo/aS;->c(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_2a

    :cond_29
    const/4 v0, 0x1

    :cond_2a
    return v0
.end method

.method public abstract c()Lo/ae;
.end method

.method public abstract h()I
.end method
