.class public Lo/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lo/D;

.field private final f:Lo/aR;


# direct methods
.method private constructor <init>(Lo/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILo/aR;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p2, p0, Lo/z;->a:Ljava/util/List;

    .line 66
    iput-object p3, p0, Lo/z;->b:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lo/z;->c:Ljava/lang/String;

    .line 68
    iput p5, p0, Lo/z;->d:I

    .line 69
    new-instance v0, Lo/D;

    invoke-direct {v0, p1, p6}, Lo/D;-><init>(Lo/r;I)V

    iput-object v0, p0, Lo/z;->e:Lo/D;

    .line 70
    iput-object p7, p0, Lo/z;->f:Lo/aR;

    .line 71
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/z;
    .registers 12
    .parameter

    .prologue
    const/4 v10, 0x7

    const/4 v6, 0x5

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x2

    .line 80
    invoke-virtual {p0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    .line 82
    if-nez v1, :cond_11

    .line 84
    const/4 v0, 0x0

    .line 132
    :goto_10
    return-object v0

    .line 87
    :cond_11
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 88
    invoke-static {v3}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v0, v5

    .line 89
    :goto_1a
    if-ge v0, v3, :cond_2c

    .line 90
    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v4

    .line 92
    if-eqz v4, :cond_29

    .line 93
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 100
    :cond_2c
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 101
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 102
    if-nez v3, :cond_3c

    .line 103
    if-eqz v4, :cond_98

    move-object v0, v4

    :goto_3b
    move-object v3, v0

    .line 105
    :cond_3c
    if-nez v4, :cond_3f

    move-object v4, v3

    .line 112
    :cond_3f
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 113
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    .line 116
    :cond_49
    const/high16 v6, -0x8000

    .line 117
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 118
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 121
    :cond_59
    const/4 v7, 0x0

    .line 122
    invoke-virtual {p0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 123
    invoke-virtual {p0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 124
    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lo/T;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v7

    .line 125
    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/T;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v0

    .line 127
    invoke-virtual {v7}, Lo/T;->f()I

    move-result v8

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v9

    if-le v8, v9, :cond_88

    .line 128
    invoke-virtual {v0}, Lo/T;->f()I

    move-result v8

    const/high16 v9, 0x4000

    add-int/2addr v8, v9

    invoke-virtual {v0, v8}, Lo/T;->a(I)V

    .line 130
    :cond_88
    new-instance v8, Lo/ad;

    invoke-direct {v8, v7, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    invoke-static {v8}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v7

    .line 132
    :cond_91
    new-instance v0, Lo/z;

    invoke-direct/range {v0 .. v7}, Lo/z;-><init>(Lo/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IILo/aR;)V

    goto/16 :goto_10

    .line 103
    :cond_98
    const-string v0, ""

    goto :goto_3b
.end method


# virtual methods
.method public a()Lo/D;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Lo/z;->e:Lo/D;

    return-object v0
.end method

.method public b()Lo/r;
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, Lo/z;->a:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 185
    iget-object v0, p0, Lo/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 192
    iget v0, p0, Lo/z;->d:I

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0}, Lo/D;->b()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/z;->e:Lo/D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
