.class public Lp/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lp/y;

.field private b:Lo/Q;

.field private c:D

.field private d:D

.field private e:I


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lp/z;)V
    .registers 2
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Lp/C;-><init>()V

    return-void
.end method

.method static synthetic a(Lp/C;D)D
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 94
    iput-wide p1, p0, Lp/C;->d:D

    return-wide p1
.end method

.method static synthetic a(Lp/C;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 94
    iput p1, p0, Lp/C;->e:I

    return p1
.end method

.method static synthetic a(Lp/C;)Lo/Q;
    .registers 2
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lp/C;->b:Lo/Q;

    return-object v0
.end method

.method static synthetic a(Lp/C;Lo/Q;)Lo/Q;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lp/C;->b:Lo/Q;

    return-object p1
.end method

.method static synthetic a(Lp/C;Lp/y;)Lp/y;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lp/C;->a:Lp/y;

    return-object p1
.end method

.method static synthetic b(Lp/C;)D
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-wide v0, p0, Lp/C;->d:D

    return-wide v0
.end method

.method static synthetic b(Lp/C;D)D
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 94
    iput-wide p1, p0, Lp/C;->c:D

    return-wide p1
.end method


# virtual methods
.method public a(D)Lp/C;
    .registers 6
    .parameter

    .prologue
    .line 121
    new-instance v0, Lp/C;

    invoke-direct {v0}, Lp/C;-><init>()V

    .line 122
    iget-object v1, p0, Lp/C;->a:Lp/y;

    iput-object v1, v0, Lp/C;->a:Lp/y;

    .line 123
    iget-object v1, p0, Lp/C;->b:Lo/Q;

    iput-object v1, v0, Lp/C;->b:Lo/Q;

    .line 124
    iget-wide v1, p0, Lp/C;->c:D

    iput-wide v1, v0, Lp/C;->c:D

    .line 125
    iput-wide p1, v0, Lp/C;->d:D

    .line 126
    iget v1, p0, Lp/C;->e:I

    iput v1, v0, Lp/C;->e:I

    .line 127
    return-object v0
.end method

.method public a()Lp/y;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lp/C;->a:Lp/y;

    return-object v0
.end method

.method public b()Lo/Q;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Lp/C;->b:Lo/Q;

    return-object v0
.end method

.method public c()D
    .registers 3

    .prologue
    .line 111
    iget-wide v0, p0, Lp/C;->c:D

    return-wide v0
.end method

.method public d()D
    .registers 3

    .prologue
    .line 114
    iget-wide v0, p0, Lp/C;->d:D

    return-wide v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 117
    iget v0, p0, Lp/C;->e:I

    return v0
.end method
