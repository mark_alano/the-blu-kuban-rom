.class public Lp/N;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/Q;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:F

.field private final i:F

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/util/List;

.field private final o:Ljava/util/List;

.field private final p:Ljava/util/List;

.field private final q:Ljava/util/List;


# direct methods
.method public constructor <init>(Lo/Q;IIIIIIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lp/N;->a:Lo/Q;

    .line 75
    iput p2, p0, Lp/N;->b:I

    .line 76
    iput p3, p0, Lp/N;->c:I

    .line 77
    iput p4, p0, Lp/N;->d:I

    .line 78
    iput p5, p0, Lp/N;->e:I

    .line 79
    iput p6, p0, Lp/N;->f:I

    .line 80
    iput p7, p0, Lp/N;->g:I

    .line 81
    iput p8, p0, Lp/N;->h:F

    .line 82
    iput p9, p0, Lp/N;->i:F

    .line 83
    iput-object p10, p0, Lp/N;->j:Ljava/lang/String;

    .line 84
    iput-object p11, p0, Lp/N;->k:Ljava/lang/String;

    .line 85
    iput-object p12, p0, Lp/N;->l:Ljava/lang/String;

    .line 86
    iput-object p13, p0, Lp/N;->m:Ljava/lang/String;

    .line 87
    iput-object p14, p0, Lp/N;->n:Ljava/util/List;

    .line 88
    move-object/from16 v0, p15

    iput-object v0, p0, Lp/N;->o:Ljava/util/List;

    .line 89
    move-object/from16 v0, p16

    iput-object v0, p0, Lp/N;->p:Ljava/util/List;

    .line 90
    move-object/from16 v0, p17

    iput-object v0, p0, Lp/N;->q:Ljava/util/List;

    .line 91
    return-void
.end method


# virtual methods
.method public a()Lo/Q;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lp/N;->a:Lo/Q;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 98
    iget v0, p0, Lp/N;->b:I

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 102
    iget v0, p0, Lp/N;->c:I

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 106
    iget v0, p0, Lp/N;->d:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 110
    iget v0, p0, Lp/N;->e:I

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 114
    iget v0, p0, Lp/N;->f:I

    return v0
.end method

.method public g()I
    .registers 2

    .prologue
    .line 118
    iget v0, p0, Lp/N;->g:I

    return v0
.end method

.method public h()F
    .registers 2

    .prologue
    .line 122
    iget v0, p0, Lp/N;->h:F

    return v0
.end method

.method public i()F
    .registers 2

    .prologue
    .line 126
    iget v0, p0, Lp/N;->i:F

    return v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lp/N;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lp/N;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 138
    iget-object v0, p0, Lp/N;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lp/N;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/util/List;
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lp/N;->n:Ljava/util/List;

    return-object v0
.end method

.method public o()Ljava/util/List;
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Lp/N;->o:Ljava/util/List;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Lp/N;->p:Ljava/util/List;

    return-object v0
.end method

.method public q()Ljava/util/List;
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, Lp/N;->q:Ljava/util/List;

    return-object v0
.end method
