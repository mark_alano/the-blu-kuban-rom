.class public Lp/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:I

.field b:Lp/d;

.field c:Ljava/util/Iterator;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .registers 3
    .parameter

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lp/c;->c:Ljava/util/Iterator;

    .line 272
    invoke-direct {p0}, Lp/c;->b()V

    .line 273
    return-void
.end method

.method private b()V
    .registers 2

    .prologue
    .line 290
    iget-object v0, p0, Lp/c;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 291
    :cond_8
    iget-object v0, p0, Lp/c;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 292
    iget-object v0, p0, Lp/c;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp/d;

    iput-object v0, p0, Lp/c;->b:Lp/d;

    .line 293
    iget-object v0, p0, Lp/c;->b:Lp/d;

    invoke-virtual {v0}, Lp/d;->c()I

    move-result v0

    if-lez v0, :cond_8

    .line 300
    :cond_22
    :goto_22
    const/4 v0, 0x0

    iput v0, p0, Lp/c;->a:I

    .line 301
    return-void

    .line 298
    :cond_26
    const/4 v0, 0x0

    iput-object v0, p0, Lp/c;->b:Lp/d;

    goto :goto_22
.end method


# virtual methods
.method public a()Lp/e;
    .registers 4

    .prologue
    .line 282
    iget-object v0, p0, Lp/c;->b:Lp/d;

    iget v1, p0, Lp/c;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lp/c;->a:I

    invoke-virtual {v0, v1}, Lp/d;->a(I)Lp/e;

    move-result-object v0

    .line 283
    iget v1, p0, Lp/c;->a:I

    iget-object v2, p0, Lp/c;->b:Lp/d;

    invoke-virtual {v2}, Lp/d;->c()I

    move-result v2

    if-lt v1, v2, :cond_19

    .line 284
    invoke-direct {p0}, Lp/c;->b()V

    .line 286
    :cond_19
    return-object v0
.end method

.method public hasNext()Z
    .registers 3

    .prologue
    .line 277
    iget-object v0, p0, Lp/c;->b:Lp/d;

    if-eqz v0, :cond_10

    iget v0, p0, Lp/c;->a:I

    iget-object v1, p0, Lp/c;->b:Lp/d;

    invoke-virtual {v1}, Lp/d;->c()I

    move-result v1

    if-ge v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 260
    invoke-virtual {p0}, Lp/c;->a()Lp/e;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 305
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
