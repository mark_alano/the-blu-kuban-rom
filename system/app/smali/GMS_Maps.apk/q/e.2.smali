.class public LQ/e;
.super LQ/f;
.source "SourceFile"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 18
    invoke-direct {p0}, LQ/f;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/e;->c:Z

    return-void
.end method

.method private c(LO/N;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-virtual {p0, p1}, LQ/e;->a(LO/N;)V

    .line 80
    invoke-virtual {p0}, LQ/e;->ah()V

    .line 81
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    .line 83
    if-eqz p1, :cond_26

    invoke-virtual {p1}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 84
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->z()V

    .line 88
    :goto_25
    return-void

    .line 86
    :cond_26
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    goto :goto_25
.end method


# virtual methods
.method protected a(LO/N;LO/N;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 53
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-ne p1, v0, :cond_10

    .line 54
    invoke-direct {p0, p2, v1}, LQ/e;->c(LO/N;Z)V

    .line 56
    :cond_10
    iput-boolean v1, p0, LQ/e;->c:Z

    .line 57
    return-void
.end method

.method protected a(LO/N;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 75
    if-nez p2, :cond_7

    const/4 v0, 0x1

    :goto_3
    invoke-direct {p0, p1, v0}, LQ/e;->c(LO/N;Z)V

    .line 76
    return-void

    .line 75
    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    .line 94
    return-void
.end method

.method protected a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 38
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_21

    iget-boolean v1, p0, LQ/e;->c:Z

    if-eqz v1, :cond_21

    .line 40
    iget-object v1, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    .line 41
    iput-boolean v3, p0, LQ/e;->c:Z

    .line 43
    :cond_21
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 23
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    .line 24
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    .line 25
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    .line 26
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    .line 27
    invoke-virtual {p0}, LQ/e;->ah()V

    .line 28
    iput-boolean v2, p0, LQ/e;->c:Z

    .line 29
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 30
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    new-array v2, v2, [LO/z;

    const/4 v3, 0x0

    iget-object v4, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    .line 34
    :cond_58
    return-void
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 47
    invoke-super {p0}, LQ/f;->g()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/e;->c:Z

    .line 49
    return-void
.end method

.method protected n()V
    .registers 4

    .prologue
    .line 61
    iget-object v0, p0, LQ/e;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_26

    .line 63
    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_27

    invoke-virtual {v0}, LO/N;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 65
    iget-object v0, p0, LQ/e;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 71
    :cond_26
    :goto_26
    return-void

    .line 68
    :cond_27
    iget-object v0, p0, LQ/e;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_26
.end method
