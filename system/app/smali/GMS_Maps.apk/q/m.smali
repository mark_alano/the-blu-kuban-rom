.class public LQ/m;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:Z

.field private d:LQ/r;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:LO/z;

.field private j:[LO/z;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 34
    invoke-direct {p0}, LQ/s;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->c:Z

    return-void
.end method

.method static synthetic a(LQ/m;)LO/z;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, LQ/m;->i:LO/z;

    return-object v0
.end method

.method private a(LO/z;LO/z;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-virtual {p1}, LO/z;->v()[LO/W;

    move-result-object v2

    .line 229
    invoke-virtual {p2}, LO/z;->v()[LO/W;

    move-result-object v3

    .line 230
    array-length v0, v2

    array-length v4, v3

    if-eq v0, v4, :cond_e

    .line 238
    :cond_d
    :goto_d
    return v1

    :cond_e
    move v0, v1

    .line 233
    :goto_f
    array-length v4, v2

    if-ge v0, v4, :cond_27

    .line 234
    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/W;->c()Lo/u;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v5}, LO/W;->c()Lo/u;

    move-result-object v5

    invoke-virtual {v4, v5}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 238
    :cond_27
    const/4 v1, 0x1

    goto :goto_d
.end method

.method static synthetic a(LQ/m;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    iput-boolean p1, p0, LQ/m;->g:Z

    return p1
.end method

.method private ap()V
    .registers 4

    .prologue
    .line 243
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_1d

    .line 244
    iget-object v0, p0, LQ/m;->j:[LO/z;

    .line 248
    :goto_6
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    .line 249
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->i:LO/z;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    .line 250
    return-void

    .line 246
    :cond_1d
    const/4 v0, 0x1

    new-array v0, v0, [LO/z;

    const/4 v1, 0x0

    iget-object v2, p0, LQ/m;->i:LO/z;

    aput-object v2, v0, v1

    goto :goto_6
.end method

.method private aq()V
    .registers 4

    .prologue
    .line 253
    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_44

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->E()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 254
    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, LO/c;->a([LO/b;I)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 256
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00e7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/CharSequence;)V

    .line 266
    :goto_2f
    return-void

    .line 260
    :cond_30
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dx;->b(Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/CharSequence;)V

    goto :goto_2f

    .line 264
    :cond_44
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    goto :goto_2f
.end method

.method private ar()V
    .registers 4

    .prologue
    .line 297
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_1f

    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v0

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-virtual {v2}, LO/z;->D()[LO/b;

    move-result-object v2

    invoke-virtual {v0, v2}, LO/c;->b([LO/b;)I

    move-result v0

    if-lez v0, :cond_1f

    const/4 v0, 0x1

    :goto_1b
    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->e(Z)V

    .line 299
    return-void

    .line 297
    :cond_1f
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private c(LO/z;[LO/z;)LO/z;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 217
    const/4 v0, 0x0

    :goto_1
    array-length v1, p2

    if-ge v0, v1, :cond_12

    .line 218
    aget-object v1, p2, v0

    invoke-direct {p0, p1, v1}, LQ/m;->a(LO/z;LO/z;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 219
    aget-object v0, p2, v0

    .line 223
    :goto_e
    return-object v0

    .line 217
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 223
    :cond_12
    const/4 v0, 0x0

    goto :goto_e
.end method


# virtual methods
.method public C()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 327
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->o()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 328
    iput-boolean v1, p0, LQ/m;->f:Z

    .line 329
    const v0, 0x7f0d006d

    invoke-virtual {p0, v0}, LQ/m;->c(I)V

    .line 331
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LO/t;->a(LaH/h;LO/z;)V

    .line 345
    :goto_33
    invoke-virtual {p0}, LQ/m;->m()V

    .line 346
    return-void

    .line 334
    :cond_37
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v2}, LO/t;->b(Z)V

    .line 335
    iget-boolean v0, p0, LQ/m;->c:Z

    if-nez v0, :cond_74

    move v0, v1

    :goto_45
    iput-boolean v0, p0, LQ/m;->c:Z

    .line 336
    iput-boolean v1, p0, LQ/m;->h:Z

    .line 337
    invoke-virtual {p0}, LQ/m;->u()V

    .line 338
    invoke-virtual {p0, v2}, LQ/m;->a(Z)V

    .line 339
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->i:LO/z;

    .line 340
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->j:[LO/z;

    .line 341
    invoke-direct {p0}, LQ/m;->ap()V

    .line 343
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    goto :goto_33

    :cond_74
    move v0, v2

    .line 335
    goto :goto_45
.end method

.method public D()V
    .registers 4

    .prologue
    .line 350
    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->D()[LO/b;

    move-result-object v0

    .line 352
    new-instance v1, LQ/n;

    invoke-direct {v1, p0, v0}, LQ/n;-><init>(LQ/m;[LO/b;)V

    .line 385
    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    .line 386
    return-void
.end method

.method public E()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 390
    iget-object v2, p0, LQ/m;->d:LQ/r;

    if-eqz v2, :cond_d

    .line 391
    invoke-virtual {p0}, LQ/m;->ak()V

    .line 392
    const/4 v1, 0x0

    iput-object v1, p0, LQ/m;->d:LQ/r;

    .line 406
    :goto_c
    return v0

    .line 394
    :cond_d
    iget-boolean v2, p0, LQ/m;->f:Z

    if-eqz v2, :cond_22

    .line 395
    invoke-virtual {p0}, LQ/m;->ak()V

    .line 396
    iput-boolean v1, p0, LQ/m;->c:Z

    .line 397
    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    .line 398
    iput-boolean v1, p0, LQ/m;->f:Z

    goto :goto_c

    .line 400
    :cond_22
    iget-boolean v2, p0, LQ/m;->g:Z

    if-eqz v2, :cond_35

    .line 401
    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->f()LO/t;

    move-result-object v2

    invoke-virtual {v2}, LO/t;->b()V

    .line 402
    invoke-virtual {p0}, LQ/m;->ak()V

    .line 403
    iput-boolean v1, p0, LQ/m;->g:Z

    goto :goto_c

    :cond_35
    move v0, v1

    .line 406
    goto :goto_c
.end method

.method public a()V
    .registers 2

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->c:Z

    .line 70
    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, LQ/m;->b()V

    .line 72
    return-void
.end method

.method protected a(LO/g;)V
    .registers 2
    .parameter

    .prologue
    .line 412
    invoke-super {p0, p1}, LQ/s;->a(LO/g;)V

    .line 413
    invoke-virtual {p0}, LQ/m;->ak()V

    .line 414
    return-void
.end method

.method public a(LO/z;)V
    .registers 2
    .parameter

    .prologue
    .line 312
    iput-object p1, p0, LQ/m;->i:LO/z;

    .line 313
    invoke-direct {p0}, LQ/m;->ap()V

    .line 314
    invoke-direct {p0}, LQ/m;->aq()V

    .line 315
    invoke-virtual {p0}, LQ/m;->u()V

    .line 316
    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 173
    invoke-virtual {p0}, LQ/m;->ak()V

    .line 174
    iget-boolean v0, p0, LQ/m;->f:Z

    if-eqz v0, :cond_5f

    .line 175
    iput-boolean v3, p0, LQ/m;->f:Z

    .line 176
    array-length v0, p2

    if-le v0, v2, :cond_1a

    .line 177
    iput-boolean v2, p0, LQ/m;->c:Z

    .line 178
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v3}, LO/t;->b(Z)V

    .line 189
    :cond_1a
    :goto_1a
    iput-boolean v2, p0, LQ/m;->h:Z

    .line 191
    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_78

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-direct {p0, v0, p2}, LQ/m;->c(LO/z;[LO/z;)LO/z;

    move-result-object v0

    :goto_26
    iput-object v0, p0, LQ/m;->i:LO/z;

    .line 192
    iget-object v0, p0, LQ/m;->i:LO/z;

    if-nez v0, :cond_2e

    .line 193
    iput-object p1, p0, LQ/m;->i:LO/z;

    .line 195
    :cond_2e
    iput-object p2, p0, LQ/m;->j:[LO/z;

    .line 197
    invoke-direct {p0}, LQ/m;->ap()V

    .line 198
    invoke-direct {p0}, LQ/m;->aq()V

    .line 199
    invoke-virtual {p0}, LQ/m;->m()V

    .line 200
    invoke-direct {p0}, LQ/m;->ar()V

    .line 201
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_49

    .line 202
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    .line 204
    :cond_49
    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->y()Z

    move-result v0

    if-nez v0, :cond_5e

    iget-object v0, p0, LQ/m;->d:LQ/r;

    if-eqz v0, :cond_5e

    .line 205
    iget-object v0, p0, LQ/m;->a:LQ/p;

    iget-object v2, p0, LQ/m;->d:LQ/r;

    invoke-virtual {v0, v2}, LQ/p;->a(LQ/r;)V

    .line 206
    iput-object v1, p0, LQ/m;->d:LQ/r;

    .line 208
    :cond_5e
    return-void

    .line 180
    :cond_5f
    iget-boolean v0, p0, LQ/m;->g:Z

    if-eqz v0, :cond_1a

    .line 181
    iput-boolean v3, p0, LQ/m;->g:Z

    .line 184
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_1a

    array-length v0, p2

    if-ne v0, v2, :cond_1a

    .line 185
    iput-boolean v3, p0, LQ/m;->c:Z

    .line 186
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0, v2}, LO/t;->b(Z)V

    goto :goto_1a

    :cond_78
    move-object v0, v1

    .line 191
    goto :goto_26
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    .line 323
    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .registers 3
    .parameter

    .prologue
    .line 459
    invoke-super {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    .line 460
    const v0, 0x7f100117

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 461
    const v0, 0x7f1004a1

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 462
    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 2
    .parameter

    .prologue
    .line 455
    return-void
.end method

.method protected a(Lo/T;)V
    .registers 18
    .parameter

    .prologue
    .line 418
    if-nez p1, :cond_3

    .line 450
    :cond_2
    :goto_2
    return-void

    .line 421
    :cond_3
    const-wide/high16 v1, 0x3fc4

    const-wide/high16 v3, 0x4000

    const/high16 v5, 0x41f0

    move-object/from16 v0, p0

    iget-object v6, v0, LQ/m;->a:LQ/p;

    invoke-virtual {v6}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v6

    invoke-virtual {v6}, LC/b;->a()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    mul-double v9, v1, v3

    .line 424
    const-wide v4, 0x7fefffffffffffffL

    .line 425
    const/4 v2, 0x0

    .line 426
    move-object/from16 v0, p0

    iget-boolean v1, v0, LQ/m;->c:Z

    if-eqz v1, :cond_6f

    .line 427
    const/4 v1, 0x0

    move v6, v1

    :goto_33
    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->j:[LO/z;

    array-length v1, v1

    if-ge v6, v1, :cond_6f

    .line 428
    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->j:[LO/z;

    aget-object v3, v1, v6

    .line 429
    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v9, v10}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v1

    check-cast v1, LO/D;

    .line 430
    if-nez v1, :cond_52

    move-object v1, v2

    move-wide v2, v4

    .line 427
    :goto_4c
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-wide v4, v2

    move-object v2, v1

    goto :goto_33

    .line 433
    :cond_52
    invoke-virtual {v1}, LO/D;->d()D

    move-result-wide v7

    .line 434
    move-object/from16 v0, p0

    iget-object v11, v0, LQ/m;->i:LO/z;

    if-ne v3, v11, :cond_62

    .line 439
    const-wide v11, 0x3fe999999999999aL

    mul-double/2addr v7, v11

    .line 441
    :cond_62
    cmpg-double v7, v7, v4

    if-gez v7, :cond_7d

    .line 442
    invoke-virtual {v1}, LO/D;->d()D

    move-result-wide v1

    move-object v13, v3

    move-wide v14, v1

    move-wide v2, v14

    move-object v1, v13

    .line 443
    goto :goto_4c

    .line 447
    :cond_6f
    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, LQ/m;->i:LO/z;

    if-eq v2, v1, :cond_2

    .line 448
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LQ/m;->a(LO/z;)V

    goto :goto_2

    :cond_7d
    move-object v1, v2

    move-wide v2, v4

    goto :goto_4c
.end method

.method protected a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_2c

    iget-boolean v0, p0, LQ/m;->h:Z

    if-nez v0, :cond_12

    if-eqz p1, :cond_2c

    .line 159
    :cond_12
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/m;->h:Z

    .line 160
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_2d

    .line 161
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 168
    :cond_2c
    :goto_2c
    return-void

    .line 164
    :cond_2d
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->c(Lcom/google/android/maps/driveabout/app/aQ;)V

    goto :goto_2c
.end method

.method public a(LQ/r;)Z
    .registers 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, LQ/m;->i:LO/z;

    if-eqz v0, :cond_16

    iget-object v0, p0, LQ/m;->i:LO/z;

    invoke-virtual {v0}, LO/z;->y()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 59
    const v0, 0x7f0d006c

    invoke-virtual {p0, v0}, LQ/m;->c(I)V

    .line 60
    iput-object p1, p0, LQ/m;->d:LQ/r;

    .line 61
    const/4 v0, 0x0

    .line 63
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x1

    goto :goto_15
.end method

.method public b()V
    .registers 3

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/m;->h:Z

    .line 77
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    .line 78
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->E()V

    .line 79
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    .line 80
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    .line 81
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    .line 83
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->j:[LO/z;

    .line 84
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iput-object v0, p0, LQ/m;->i:LO/z;

    .line 86
    invoke-virtual {p0}, LQ/m;->m()V

    .line 87
    invoke-direct {p0}, LQ/m;->ar()V

    .line 89
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 90
    invoke-direct {p0}, LQ/m;->ap()V

    .line 92
    :cond_5e
    return-void
.end method

.method public d()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    if-eqz v1, :cond_a2

    iget-object v1, p0, LQ/m;->i:LO/z;

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    if-eq v1, v2, :cond_a2

    .line 100
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->f()LO/t;

    move-result-object v1

    iget-object v2, p0, LQ/m;->i:LO/z;

    invoke-virtual {v1, v2}, LO/t;->a(LO/z;)V

    .line 103
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->f()LO/t;

    move-result-object v1

    invoke-virtual {v1, v0}, LO/t;->a(Z)V

    .line 105
    iget-object v1, p0, LQ/m;->i:LO/z;

    if-eqz v1, :cond_a2

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    const-string v2, "|r="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LQ/m;->i:LO/z;

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 108
    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v2

    .line 109
    :goto_52
    array-length v3, v2

    if-ge v0, v3, :cond_99

    .line 110
    const-string v3, "|d"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/z;->p()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const-string v3, "|t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, LO/z;->o()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    aget-object v3, v2, v0

    iget-object v4, p0, LQ/m;->i:LO/z;

    if-ne v3, v4, :cond_96

    .line 113
    const-string v3, "|s="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    :cond_96
    add-int/lit8 v0, v0, 0x1

    goto :goto_52

    .line 116
    :cond_99
    const-string v0, "L"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_a2
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LO/t;->b(Z)V

    .line 121
    invoke-super {p0}, LQ/s;->d()V

    .line 122
    iput-object v5, p0, LQ/m;->i:LO/z;

    .line 123
    iput-object v5, p0, LQ/m;->j:[LO/z;

    .line 124
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    .line 129
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    .line 130
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->J()V

    .line 131
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    .line 132
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->s()V

    .line 133
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    .line 134
    return-void
.end method

.method protected m()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 286
    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-boolean v2, p0, LQ/m;->f:Z

    if-nez v2, :cond_2f

    iget-object v2, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->o()Z

    move-result v2

    if-nez v2, :cond_20

    iget-object v2, p0, LQ/m;->j:[LO/z;

    if-eqz v2, :cond_2f

    iget-object v2, p0, LQ/m;->j:[LO/z;

    array-length v2, v2

    if-le v2, v0, :cond_2f

    :cond_20
    :goto_20
    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    .line 291
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-boolean v1, p0, LQ/m;->c:Z

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->d(Z)V

    .line 292
    return-void

    .line 286
    :cond_2f
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public p()V
    .registers 2

    .prologue
    .line 152
    iget-boolean v0, p0, LQ/m;->c:Z

    iput-boolean v0, p0, LQ/m;->e:Z

    .line 153
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/m;->h:Z

    .line 139
    iget-boolean v0, p0, LQ/m;->e:Z

    iput-boolean v0, p0, LQ/m;->c:Z

    .line 140
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_21

    .line 141
    invoke-virtual {p0}, LQ/m;->m()V

    .line 142
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    invoke-virtual {v0}, LO/t;->f()V

    .line 143
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/t;->b(Z)V

    .line 145
    :cond_21
    invoke-direct {p0}, LQ/m;->ap()V

    .line 146
    invoke-direct {p0}, LQ/m;->ar()V

    .line 147
    invoke-virtual {p0}, LQ/m;->u()V

    .line 148
    return-void
.end method

.method protected r()V
    .registers 1

    .prologue
    .line 308
    return-void
.end method

.method protected u()V
    .registers 4

    .prologue
    .line 270
    iget-boolean v0, p0, LQ/m;->c:Z

    if-eqz v0, :cond_12

    .line 271
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->i:LO/z;

    iget-object v2, p0, LQ/m;->j:[LO/z;

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/z;[LO/z;)V

    .line 275
    :goto_11
    return-void

    .line 273
    :cond_12
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    goto :goto_11
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 303
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    .line 304
    return-void
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 279
    iget-object v0, p0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method
