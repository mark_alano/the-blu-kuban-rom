.class LQ/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/aH;


# instance fields
.field final synthetic a:[LO/b;

.field final synthetic b:LQ/m;


# direct methods
.method constructor <init>(LQ/m;[LO/b;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 353
    iput-object p1, p0, LQ/n;->b:LQ/m;

    iput-object p2, p0, LQ/n;->a:[LO/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([LO/b;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 356
    .line 357
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v0

    .line 358
    :goto_8
    iget-object v4, p0, LQ/n;->a:[LO/b;

    array-length v4, v4

    if-ge v0, v4, :cond_4e

    .line 359
    iget-object v4, p0, LQ/n;->a:[LO/b;

    aget-object v4, v4, v0

    aget-object v5, p1, v0

    invoke-virtual {v4, v5}, LO/b;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4b

    .line 361
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_24

    .line 362
    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    :cond_24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, p1, v0

    invoke-virtual {v4}, LO/b;->b()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v4, p1, v0

    invoke-virtual {v4}, LO/b;->c()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 358
    :cond_4b
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 367
    :cond_4e
    if-eqz v1, :cond_91

    .line 368
    const-string v0, "R"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    new-instance v0, LL/D;

    const-string v1, "changedRouteOptions"

    invoke-static {p1}, LO/c;->c([LO/b;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, LL/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 372
    iget-object v0, p0, LQ/n;->b:LQ/m;

    invoke-static {v0, v2}, LQ/m;->a(LQ/m;Z)Z

    .line 373
    iget-object v0, p0, LQ/n;->b:LQ/m;

    const v1, 0x7f0d00d7

    invoke-virtual {v0, v1}, LQ/m;->c(I)V

    .line 378
    iget-object v0, p0, LQ/n;->b:LQ/m;

    iget-object v0, v0, LQ/m;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    iget-object v1, p0, LQ/n;->b:LQ/m;

    iget-object v1, v1, LQ/m;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    iget-object v2, p0, LQ/n;->b:LQ/m;

    invoke-static {v2}, LQ/m;->a(LQ/m;)LO/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LO/t;->a(LaH/h;LO/z;[LO/b;)V

    .line 382
    :cond_91
    return-void
.end method
