.class public abstract LQ/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:I

.field private static final e:[LQ/s;


# instance fields
.field protected a:LQ/p;

.field private c:LQ/x;

.field private d:I

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 110
    invoke-static {}, LQ/x;->values()[LQ/x;

    move-result-object v0

    array-length v0, v0

    sput v0, LQ/s;->b:I

    .line 118
    sget v0, LQ/s;->b:I

    new-array v0, v0, [LQ/s;

    sput-object v0, LQ/s;->e:[LQ/s;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/s;->f:Z

    .line 1960
    return-void
.end method

.method static synthetic a(LQ/s;LQ/x;)LQ/x;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, LQ/s;->c:LQ/x;

    return-object p1
.end method

.method private a(LO/U;I[LO/b;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 876
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 877
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 878
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 879
    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;I[LO/b;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 880
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 881
    const-string v1, "UserRequestedReroute"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 882
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 883
    return-void
.end method

.method static synthetic a(LQ/s;Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, LQ/s;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    return-void
.end method

.method private final a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1933
    new-instance v0, Lcom/google/android/maps/driveabout/app/eE;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/eE;-><init>(Lcom/google/android/maps/driveabout/app/cQ;)V

    .line 1934
    new-instance v1, LQ/v;

    invoke-direct {v1, p0, p2}, LQ/v;-><init>(LQ/s;Lcom/google/android/maps/driveabout/app/da;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/eE;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/eN;)V

    .line 1935
    return-void
.end method

.method private aA()V
    .registers 3

    .prologue
    .line 1728
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    if-eqz v0, :cond_17

    .line 1729
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    .line 1735
    :goto_16
    return-void

    .line 1730
    :cond_17
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 1731
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    goto :goto_16

    .line 1733
    :cond_2e
    invoke-virtual {p0}, LQ/s;->v()V

    goto :goto_16
.end method

.method private aB()V
    .registers 4

    .prologue
    .line 1753
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-nez v0, :cond_23

    .line 1754
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0064

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    .line 1764
    :goto_22
    return-void

    .line 1756
    :cond_23
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 1757
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0065

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    goto :goto_22

    .line 1760
    :cond_46
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, "__route_status"

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    .line 1762
    invoke-virtual {p0}, LQ/s;->w()V

    goto :goto_22
.end method

.method private aC()Z
    .registers 3

    .prologue
    .line 1783
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v0

    const/16 v1, 0x1324

    if-le v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private aD()V
    .registers 2

    .prologue
    .line 1791
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 1792
    return-void
.end method

.method private final aE()V
    .registers 2

    .prologue
    .line 1798
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->j()V

    .line 1799
    return-void
.end method

.method private ap()V
    .registers 3

    .prologue
    .line 300
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_19

    .line 301
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 302
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_19

    .line 304
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->invalidateOptionsMenu()V

    .line 311
    :cond_19
    :goto_19
    return-void

    .line 307
    :cond_1a
    const-string v0, "UIState"

    const-string v1, "NavigationApplicationDelegate.getInstance() should never be null."

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_19
.end method

.method private aq()V
    .registers 4

    .prologue
    .line 482
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    .line 483
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->a()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZZ)V

    .line 485
    return-void
.end method

.method private ar()V
    .registers 3

    .prologue
    .line 513
    iget v0, p0, LQ/s;->d:I

    if-lez v0, :cond_e

    .line 514
    const-string v0, "z"

    iget v1, p0, LQ/s;->d:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 515
    const/4 v0, 0x0

    iput v0, p0, LQ/s;->d:I

    .line 517
    :cond_e
    return-void
.end method

.method private as()V
    .registers 4

    .prologue
    .line 1398
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    .line 1399
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    .line 1402
    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v2, :cond_21

    .line 1403
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    .line 1407
    :cond_21
    invoke-static {v1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_5d

    .line 1408
    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_3f

    .line 1409
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 1410
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->t()V

    .line 1437
    :cond_3f
    :goto_3f
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->i()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V

    .line 1439
    return-void

    .line 1412
    :cond_5d
    invoke-static {v1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_ab

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->r()Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 1417
    invoke-virtual {v1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-virtual {v1}, LaH/h;->k()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 1418
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    .line 1421
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->s()I

    move-result v0

    .line 1423
    const-string v1, "T"

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 1428
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 1429
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_3f

    .line 1432
    :cond_ab
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-eqz v1, :cond_c8

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_c8

    .line 1433
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto/16 :goto_3f

    .line 1434
    :cond_c8
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-nez v1, :cond_3f

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_3f

    .line 1435
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto/16 :goto_3f
.end method

.method private at()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 1442
    invoke-virtual {p0}, LQ/s;->af()Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 1443
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v1

    .line 1444
    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v3

    invoke-virtual {v2, v3}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v2

    invoke-virtual {v2, v1}, LaH/j;->b(F)LaH/j;

    move-result-object v1

    .line 1447
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    .line 1453
    :goto_35
    invoke-virtual {p0}, LQ/s;->ag()V

    .line 1454
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setCurrentRoadName(Landroid/location/Location;)V

    .line 1455
    return-void

    .line 1449
    :cond_4c
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    .line 1450
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-eqz v3, :cond_72

    invoke-virtual {v1}, LaH/h;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_72

    :goto_6e
    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    goto :goto_35

    :cond_72
    const/4 v0, 0x0

    goto :goto_6e
.end method

.method private au()I
    .registers 2

    .prologue
    .line 1486
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1487
    const/4 v0, 0x2

    .line 1489
    :goto_d
    return v0

    :cond_e
    invoke-virtual {p0}, LQ/s;->f()I

    move-result v0

    goto :goto_d
.end method

.method private av()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1517
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->G()V

    .line 1544
    :goto_17
    invoke-virtual {p0}, LQ/s;->x()V

    .line 1545
    return-void

    .line 1518
    :cond_1b
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 1519
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d0091

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_17

    .line 1521
    :cond_3e
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 1523
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_cc

    .line 1524
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->m()LO/U;

    move-result-object v0

    .line 1525
    if-eqz v0, :cond_cc

    .line 1526
    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    .line 1527
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v3

    invoke-virtual {v3, v0}, LaH/h;->b(Lo/u;)F

    move-result v0

    const/high16 v3, 0x4270

    cmpl-float v0, v0, v3

    if-lez v0, :cond_c3

    move v0, v1

    .line 1531
    :goto_7f
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v4

    .line 1532
    invoke-virtual {v4}, LO/U;->k()Z

    move-result v5

    .line 1533
    const/4 v3, 0x0

    .line 1534
    if-eqz v5, :cond_ac

    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v5

    if-eqz v5, :cond_ac

    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v5

    invoke-virtual {v5}, Lbl/h;->d()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_ac

    .line 1536
    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v3

    invoke-virtual {v3}, Lbl/h;->b()Ljava/lang/String;

    move-result-object v3

    .line 1538
    :cond_ac
    iget-object v5, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v5

    invoke-virtual {v4}, LO/U;->i()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v0, v3, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1540
    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c5

    :goto_bf
    iput-boolean v1, p0, LQ/s;->f:Z

    goto/16 :goto_17

    :cond_c3
    move v0, v2

    .line 1527
    goto :goto_7f

    :cond_c5
    move v1, v2

    .line 1540
    goto :goto_bf

    .line 1542
    :cond_c7
    invoke-virtual {p0}, LQ/s;->u()V

    goto/16 :goto_17

    :cond_cc
    move v0, v2

    goto :goto_7f
.end method

.method private aw()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1580
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    .line 1581
    if-eqz v0, :cond_32

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v1

    if-eqz v1, :cond_32

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    if-eqz v1, :cond_32

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    invoke-virtual {v1}, LO/V;->a()I

    move-result v1

    if-lez v1, :cond_32

    .line 1584
    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1608
    :goto_31
    return-object v0

    :cond_32
    const/4 v0, 0x0

    goto :goto_31
.end method

.method private ax()V
    .registers 1

    .prologue
    .line 1660
    invoke-virtual {p0}, LQ/s;->ai()V

    .line 1661
    return-void
.end method

.method private ay()V
    .registers 3

    .prologue
    .line 1707
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->d()I

    move-result v0

    .line 1708
    const/4 v1, -0x1

    if-eq v0, v1, :cond_17

    .line 1709
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(I)V

    .line 1713
    :goto_16
    return-void

    .line 1711
    :cond_17
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->j()V

    goto :goto_16
.end method

.method private az()V
    .registers 5

    .prologue
    .line 1719
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->p()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cQ;->a(IIZ)V

    .line 1723
    return-void
.end method

.method private j(Z)V
    .registers 6
    .parameter

    .prologue
    .line 1472
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-direct {p0}, LQ/s;->au()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cq;->a(ILcom/google/android/maps/driveabout/vector/q;I)V

    .line 1475
    invoke-virtual {p0, p1}, LQ/s;->a(Z)V

    .line 1476
    return-void
.end method


# virtual methods
.method protected A()V
    .registers 1

    .prologue
    .line 1281
    return-void
.end method

.method protected B()V
    .registers 3

    .prologue
    .line 1310
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->h(Z)V

    .line 1311
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1312
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    .line 1314
    :cond_1f
    return-void
.end method

.method public C()V
    .registers 4

    .prologue
    .line 1805
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->f:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1806
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    .line 1808
    :cond_14
    return-void
.end method

.method public D()V
    .registers 1

    .prologue
    .line 1816
    return-void
.end method

.method public E()Z
    .registers 2

    .prologue
    .line 1824
    const/4 v0, 0x0

    return v0
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 198
    const/4 v0, 0x1

    return v0
.end method

.method public final G()LQ/x;
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, LQ/s;->c:LQ/x;

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, LQ/s;->c:LQ/x;

    invoke-virtual {v0}, LQ/x;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()V
    .registers 2

    .prologue
    .line 278
    invoke-direct {p0}, LQ/s;->aE()V

    .line 279
    invoke-direct {p0}, LQ/s;->aq()V

    .line 280
    invoke-direct {p0}, LQ/s;->as()V

    .line 281
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    .line 282
    invoke-direct {p0}, LQ/s;->av()V

    .line 283
    invoke-direct {p0}, LQ/s;->ap()V

    .line 284
    invoke-direct {p0}, LQ/s;->aB()V

    .line 285
    invoke-direct {p0}, LQ/s;->ax()V

    .line 286
    invoke-direct {p0}, LQ/s;->ay()V

    .line 287
    invoke-direct {p0}, LQ/s;->az()V

    .line 288
    invoke-direct {p0}, LQ/s;->aA()V

    .line 289
    invoke-virtual {p0}, LQ/s;->m()V

    .line 290
    return-void
.end method

.method public J()V
    .registers 2

    .prologue
    .line 320
    invoke-direct {p0}, LQ/s;->at()V

    .line 321
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    .line 322
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 323
    invoke-direct {p0}, LQ/s;->av()V

    .line 325
    :cond_16
    return-void
.end method

.method public final K()V
    .registers 4

    .prologue
    .line 332
    invoke-virtual {p0}, LQ/s;->af()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 333
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v0

    .line 334
    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {v1, v2}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->b(F)LaH/j;

    move-result-object v0

    .line 337
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    .line 339
    :cond_35
    return-void
.end method

.method protected L()Z
    .registers 2

    .prologue
    .line 371
    const/4 v0, 0x1

    return v0
.end method

.method public final M()V
    .registers 4

    .prologue
    .line 523
    invoke-direct {p0}, LQ/s;->aE()V

    .line 524
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->e()Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x1

    .line 525
    :goto_10
    new-instance v1, LL/D;

    const-string v2, "compassButton"

    invoke-direct {v1, v2, v0}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v1}, Ll/f;->b(Ll/j;)V

    .line 527
    if-eqz v0, :cond_2a

    const-string v0, "3"

    :goto_1e
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 531
    invoke-virtual {p0}, LQ/s;->g()V

    .line 532
    invoke-direct {p0}, LQ/s;->aD()V

    .line 533
    return-void

    .line 524
    :cond_28
    const/4 v0, 0x0

    goto :goto_10

    .line 527
    :cond_2a
    const-string v0, "2"

    goto :goto_1e
.end method

.method public final N()V
    .registers 3

    .prologue
    .line 583
    invoke-direct {p0}, LQ/s;->aE()V

    .line 584
    new-instance v0, LL/C;

    const-string v1, "zoomOut"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 586
    iget v0, p0, LQ/s;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LQ/s;->d:I

    .line 588
    invoke-virtual {p0}, LQ/s;->i()V

    .line 589
    return-void
.end method

.method public final O()V
    .registers 3

    .prologue
    .line 595
    invoke-direct {p0}, LQ/s;->aE()V

    .line 596
    new-instance v0, LL/C;

    const-string v1, "zoomIn"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 598
    iget v0, p0, LQ/s;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LQ/s;->d:I

    .line 600
    invoke-virtual {p0}, LQ/s;->h()V

    .line 601
    return-void
.end method

.method public final P()Z
    .registers 3

    .prologue
    .line 609
    invoke-direct {p0}, LQ/s;->aE()V

    .line 610
    new-instance v0, LL/C;

    const-string v1, "back"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 612
    invoke-virtual {p0}, LQ/s;->j()Z

    move-result v0

    return v0
.end method

.method public final Q()V
    .registers 4

    .prologue
    .line 619
    invoke-direct {p0}, LQ/s;->aE()V

    .line 620
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 629
    :cond_1b
    :goto_1b
    return-void

    .line 624
    :cond_1c
    new-instance v0, LL/C;

    const-string v1, "routeOverview"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 626
    const-string v0, "r"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->f:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_1b
.end method

.method public final R()V
    .registers 4

    .prologue
    .line 635
    invoke-direct {p0}, LQ/s;->aE()V

    .line 636
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 643
    :cond_1b
    :goto_1b
    return-void

    .line 640
    :cond_1c
    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficOverview"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 642
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->g:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_1b
.end method

.method public S()V
    .registers 3

    .prologue
    .line 649
    invoke-direct {p0}, LQ/s;->aE()V

    .line 650
    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficConfirm"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 652
    invoke-virtual {p0}, LQ/s;->y()V

    .line 653
    return-void
.end method

.method public T()V
    .registers 3

    .prologue
    .line 659
    invoke-direct {p0}, LQ/s;->aE()V

    .line 660
    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficCancel"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 662
    invoke-virtual {p0}, LQ/s;->z()V

    .line 663
    return-void
.end method

.method public U()V
    .registers 3

    .prologue
    .line 669
    invoke-direct {p0}, LQ/s;->aE()V

    .line 670
    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficTimeout"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 672
    invoke-virtual {p0}, LQ/s;->A()V

    .line 673
    return-void
.end method

.method public final V()V
    .registers 4

    .prologue
    .line 698
    invoke-direct {p0}, LQ/s;->aE()V

    .line 699
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 707
    :cond_1b
    :goto_1b
    return-void

    .line 703
    :cond_1c
    new-instance v0, LL/C;

    const-string v1, "viewList"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 705
    const-string v0, "l"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 706
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->h:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_1b
.end method

.method public final W()V
    .registers 3

    .prologue
    .line 766
    invoke-direct {p0}, LQ/s;->aE()V

    .line 767
    new-instance v0, LL/C;

    const-string v1, "backToLocation"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 769
    const-string v0, "n"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 771
    invoke-virtual {p0}, LQ/s;->k()V

    .line 772
    return-void
.end method

.method public final X()V
    .registers 4

    .prologue
    .line 818
    invoke-direct {p0}, LQ/s;->aE()V

    .line 821
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 822
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 823
    const-string v1, "TravelMode"

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 825
    const-string v1, "ForceNewDestination"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 826
    const/high16 v1, 0x1002

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 827
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 828
    return-void
.end method

.method public final Y()V
    .registers 2

    .prologue
    .line 889
    invoke-direct {p0}, LQ/s;->aE()V

    .line 890
    const-string v0, "+"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 892
    invoke-virtual {p0}, LQ/s;->r()V

    .line 893
    invoke-direct {p0}, LQ/s;->aD()V

    .line 894
    return-void
.end method

.method public final Z()V
    .registers 2

    .prologue
    .line 901
    invoke-direct {p0}, LQ/s;->aE()V

    .line 902
    const-string v0, "-"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 904
    invoke-virtual {p0}, LQ/s;->B()V

    .line 905
    invoke-direct {p0}, LQ/s;->aD()V

    .line 907
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 908
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->g()V

    .line 910
    :cond_27
    return-void
.end method

.method public a()V
    .registers 2

    .prologue
    .line 233
    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p0}, LQ/s;->b()V

    .line 235
    return-void
.end method

.method protected a(FF)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1221
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 1222
    return-void
.end method

.method protected a(FFF)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1235
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 1236
    return-void
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 1158
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1159
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(IIZ)V

    .line 1162
    invoke-virtual {p0, p1}, LQ/s;->b(I)V

    .line 1163
    return-void
.end method

.method public final a(IIFIIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    .line 344
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/cq;->a(IIF)V

    .line 345
    invoke-virtual {v0, p7}, Lcom/google/android/maps/driveabout/app/cq;->a(I)V

    .line 346
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    .line 347
    return-void
.end method

.method protected final a(LO/N;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1617
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1618
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->a(LO/N;)V

    .line 1619
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    .line 1620
    if-eqz p1, :cond_23

    if-eqz v0, :cond_23

    invoke-virtual {p1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2d

    .line 1623
    :cond_23
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3}, Lcom/google/android/maps/driveabout/app/cQ;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    .line 1632
    :goto_2c
    return-void

    .line 1625
    :cond_2d
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    new-instance v2, LQ/t;

    invoke-direct {v2, p0, p1}, LQ/t;-><init>(LQ/s;LO/N;)V

    invoke-interface {v1, v0, p1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    goto :goto_2c
.end method

.method protected a(LO/N;LO/N;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1328
    invoke-virtual {p0, p2}, LQ/s;->a(LO/N;)V

    .line 1329
    return-void
.end method

.method protected a(LO/N;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1332
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->k:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1333
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LQ/s;->a(LO/N;Z)V

    .line 1335
    :cond_14
    return-void
.end method

.method public final a(LO/U;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 839
    invoke-direct {p0}, LQ/s;->aE()V

    .line 840
    new-instance v0, LL/D;

    const-string v1, "newDestination"

    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LL/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 843
    if-eqz p2, :cond_18

    .line 844
    const-string v0, "d"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 847
    :cond_18
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->H()[LO/b;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LQ/s;->a(LO/U;I[LO/b;)V

    .line 850
    return-void
.end method

.method protected a(LO/g;)V
    .registers 5
    .parameter

    .prologue
    .line 1347
    invoke-virtual {p0, p1}, LQ/s;->c(LO/g;)I

    move-result v0

    .line 1348
    if-lez v0, :cond_1a

    .line 1349
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;I)V

    .line 1352
    :cond_1a
    return-void
.end method

.method public final a(LO/j;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 952
    invoke-direct {p0}, LQ/s;->aE()V

    .line 953
    invoke-direct {p0}, LQ/s;->ar()V

    .line 955
    invoke-virtual {p0, p1, p2}, LQ/s;->c(LO/j;I)V

    .line 956
    invoke-direct {p0}, LQ/s;->aD()V

    .line 957
    return-void
.end method

.method public a(LO/z;)V
    .registers 2
    .parameter

    .prologue
    .line 1802
    return-void
.end method

.method protected a(LO/z;[LO/z;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1342
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [LO/z;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    .line 1343
    return-void
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1383
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    .line 1384
    return-void
.end method

.method public a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eC;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1177
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1178
    new-instance v0, LL/C;

    const-string v1, "search"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 1180
    const-string v0, "?"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 1182
    invoke-virtual {p0, p1, p2}, LQ/s;->a(Landroid/os/Bundle;Lo/ae;)V

    .line 1183
    invoke-interface {p3, p1}, Lcom/google/android/maps/driveabout/app/eC;->a(Landroid/os/Bundle;)V

    .line 1184
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .registers 5
    .parameter

    .prologue
    .line 382
    const v0, 0x7f100119

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;

    .line 385
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 386
    const v1, 0x7f020113

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 387
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0076

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    .line 393
    :goto_2f
    return-void

    .line 389
    :cond_30
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 390
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0077

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    goto :goto_2f
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .registers 4
    .parameter

    .prologue
    const v1, 0x7f100118

    .line 1390
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_13

    .line 1391
    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 1395
    :goto_12
    return-void

    .line 1393
    :cond_13
    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_12
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 5
    .parameter

    .prologue
    .line 1240
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1241
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 1242
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 1243
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1245
    :cond_24
    return-void
.end method

.method public final a(Ll/f;)V
    .registers 6
    .parameter

    .prologue
    .line 1885
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1886
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->n()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1910
    :goto_f
    return-void

    .line 1889
    :cond_10
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->d(Z)V

    .line 1892
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "da_voice-rmi.3gp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1893
    new-instance v1, Lcom/google/android/maps/driveabout/app/da;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/app/da;-><init>(Ljava/io/File;)V

    .line 1894
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/app/da;->a(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/cS;)V

    .line 1895
    if-eqz p1, :cond_4a

    .line 1896
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/da;->a(Ll/f;Lcom/google/android/maps/driveabout/app/cQ;)V

    .line 1898
    :cond_4a
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 1899
    invoke-direct {p0, v0, v1}, LQ/s;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    goto :goto_f

    .line 1901
    :cond_5e
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    new-instance v3, LQ/u;

    invoke-direct {v3, p0, v0, v1}, LQ/u;-><init>(LQ/s;Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_f
.end method

.method protected a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 1225
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->H()V

    .line 1226
    invoke-virtual {p0}, LQ/s;->ah()V

    .line 1227
    return-void
.end method

.method public final a(Lo/aR;Lcom/google/android/maps/driveabout/app/by;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 677
    invoke-direct {p0}, LQ/s;->aE()V

    .line 678
    new-instance v0, LL/C;

    const-string v1, "layers"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 680
    const-string v1, "s"

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_6b

    move v0, v4

    :goto_1e
    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 683
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v2

    .line 684
    invoke-virtual {v2, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(Lo/aR;)V

    .line 685
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->l:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->b(LQ/x;)Z

    move-result v5

    .line 686
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bC;->b()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bC;->c()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    iget-object v7, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v7}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v7

    sget-object v8, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v7, v8, :cond_6d

    :goto_5c
    iget-object v6, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v6}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cS;->i()Z

    move-result v6

    move-object v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    .line 692
    return-void

    :cond_6b
    move v0, v6

    .line 680
    goto :goto_1e

    :cond_6d
    move v4, v6

    .line 686
    goto :goto_5c
.end method

.method protected a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1507
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 1509
    return-void
.end method

.method public a(LQ/r;)Z
    .registers 3
    .parameter

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 359
    invoke-virtual {p0}, LQ/s;->L()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 567
    invoke-direct {p0}, LQ/s;->aE()V

    .line 576
    const/4 v0, 0x0

    return v0
.end method

.method public final aa()V
    .registers 2

    .prologue
    .line 917
    invoke-direct {p0}, LQ/s;->aE()V

    .line 919
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 920
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->i()V

    .line 922
    :cond_1c
    return-void
.end method

.method ab()Z
    .registers 2

    .prologue
    .line 965
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->g()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public final ac()V
    .registers 1

    .prologue
    .line 1032
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1033
    invoke-virtual {p0}, LQ/s;->l()V

    .line 1034
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1035
    return-void
.end method

.method public final ad()V
    .registers 1

    .prologue
    .line 1106
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1107
    invoke-virtual {p0}, LQ/s;->t()V

    .line 1108
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1109
    return-void
.end method

.method public ae()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1117
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1118
    const-string v0, "."

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 1120
    const v0, 0x7f0d0012

    .line 1121
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-eqz v1, :cond_2c

    .line 1122
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    if-ne v1, v3, :cond_72

    .line 1123
    const v0, 0x7f0d0010

    .line 1128
    :cond_2c
    :goto_2c
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;I)V

    .line 1131
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cQ;->g(Z)V

    .line 1132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    .line 1133
    invoke-direct {p0}, LQ/s;->av()V

    .line 1134
    invoke-direct {p0}, LQ/s;->aB()V

    .line 1136
    invoke-virtual {p0}, LQ/s;->n()V

    .line 1138
    iget-boolean v0, p0, LQ/s;->f:Z

    if-eqz v0, :cond_6e

    .line 1139
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    .line 1140
    invoke-virtual {v0}, LO/U;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LO/U;->h()Lbl/h;

    move-result-object v0

    invoke-static {v1, v0, v4}, Lbl/a;->a(Ljava/lang/String;Lbl/h;Z)V

    .line 1144
    :cond_6e
    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dp;->b(Z)V

    .line 1145
    return-void

    .line 1124
    :cond_72
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2c

    .line 1125
    const v0, 0x7f0d0011

    goto :goto_2c
.end method

.method protected af()Z
    .registers 3

    .prologue
    .line 1463
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_27

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->d()Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_27

    const/4 v0, 0x1

    :goto_26
    return v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method protected ag()V
    .registers 1

    .prologue
    .line 1469
    return-void
.end method

.method protected final ah()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1643
    .line 1645
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v3

    .line 1646
    if-eqz v3, :cond_42

    .line 1647
    invoke-virtual {v3}, LO/N;->k()LO/N;

    move-result-object v0

    if-eqz v0, :cond_3c

    move v0, v1

    .line 1648
    :goto_15
    invoke-virtual {v3}, LO/N;->j()LO/N;

    move-result-object v3

    if-eqz v3, :cond_3e

    move v3, v1

    .line 1651
    :goto_1c
    iget-object v4, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->g()LQ/s;

    move-result-object v4

    invoke-virtual {v4}, LQ/s;->G()LQ/x;

    move-result-object v4

    .line 1652
    sget-object v5, LQ/x;->m:LQ/x;

    if-eq v4, v5, :cond_40

    sget-object v5, LQ/x;->f:LQ/x;

    if-eq v4, v5, :cond_40

    sget-object v5, LQ/x;->h:LQ/x;

    if-eq v4, v5, :cond_40

    .line 1656
    :goto_32
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZZZ)V

    .line 1657
    return-void

    :cond_3c
    move v0, v2

    .line 1647
    goto :goto_15

    :cond_3e
    move v3, v2

    .line 1648
    goto :goto_1c

    :cond_40
    move v1, v2

    .line 1652
    goto :goto_32

    :cond_42
    move v3, v2

    move v0, v2

    goto :goto_1c
.end method

.method protected ai()V
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 1669
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-nez v1, :cond_f

    .line 1704
    :cond_e
    :goto_e
    return-void

    .line 1673
    :cond_f
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dx;->a()Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v2

    .line 1674
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->E()[LO/U;

    move-result-object v1

    .line 1675
    if-eqz v1, :cond_e

    array-length v3, v1

    if-eqz v3, :cond_e

    .line 1679
    const/4 v4, 0x0

    .line 1682
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->Q()Z

    move-result v3

    if-eqz v3, :cond_5f

    .line 1683
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d0047

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    .line 1700
    :goto_3d
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->l()Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v0

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->H()[LO/b;

    move-result-object v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cT;->a([LO/U;I[LO/b;Ljava/lang/CharSequence;ZZ)V

    goto :goto_e

    .line 1686
    :cond_5f
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-nez v3, :cond_7b

    .line 1687
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0064

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    .line 1688
    goto :goto_3d

    .line 1689
    :cond_7b
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v3

    if-eqz v3, :cond_97

    .line 1690
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0065

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    .line 1691
    goto :goto_3d

    .line 1692
    :cond_97
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v3

    if-eqz v3, :cond_cd

    .line 1693
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v3

    iget-object v4, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v4

    iget-object v5, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v5

    invoke-virtual {v5}, LO/z;->q()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/dx;->a(ILO/N;I)Landroid/text/Spannable;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_3d

    .line 1697
    :cond_cd
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v2

    if-eqz v2, :cond_ea

    .line 1698
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0091

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_3d

    :cond_ea
    move v6, v0

    move v5, v0

    goto/16 :goto_3d
.end method

.method public final aj()V
    .registers 3

    .prologue
    .line 1835
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1836
    new-instance v0, LL/C;

    const-string v1, "showStreetView"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 1838
    invoke-virtual {p0}, LQ/s;->o()V

    .line 1839
    return-void
.end method

.method protected ak()V
    .registers 2

    .prologue
    .line 1857
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->c()V

    .line 1858
    invoke-direct {p0}, LQ/s;->ay()V

    .line 1859
    return-void
.end method

.method public final al()V
    .registers 3

    .prologue
    .line 1866
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->c(Z)V

    .line 1867
    invoke-virtual {p0}, LQ/s;->am()V

    .line 1868
    return-void
.end method

.method protected am()V
    .registers 2

    .prologue
    .line 1871
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->l()V

    .line 1872
    return-void
.end method

.method public an()V
    .registers 5

    .prologue
    .line 1913
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    .line 1914
    invoke-virtual {v0}, LO/U;->g()Ljava/lang/String;

    move-result-object v1

    .line 1915
    invoke-static {v1}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v2

    .line 1916
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lo/o;)V

    .line 1919
    invoke-virtual {v0}, LO/U;->h()Lbl/h;

    move-result-object v2

    .line 1920
    invoke-virtual {v0}, LO/U;->j()Lbl/c;

    move-result-object v0

    .line 1921
    if-eqz v2, :cond_2e

    if-eqz v0, :cond_2e

    .line 1922
    invoke-virtual {v0}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lbl/a;->a(Lbl/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925
    :cond_2e
    return-void
.end method

.method public ao()V
    .registers 3

    .prologue
    .line 1928
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/U;)V

    .line 1930
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 243
    return-void
.end method

.method public final b(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 716
    invoke-direct {p0}, LQ/s;->aE()V

    .line 725
    invoke-virtual {p0, p1, p2}, LQ/s;->a(FF)V

    .line 726
    return-void
.end method

.method public final b(FFF)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 741
    invoke-direct {p0}, LQ/s;->aE()V

    .line 744
    invoke-virtual {p0, p1, p2, p3}, LQ/s;->a(FFF)V

    .line 745
    return-void
.end method

.method protected b(I)V
    .registers 2
    .parameter

    .prologue
    .line 1380
    return-void
.end method

.method public final b(LO/N;LO/N;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 998
    invoke-direct {p0}, LQ/s;->aE()V

    .line 999
    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/N;LO/N;)V

    .line 1000
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1001
    return-void
.end method

.method public final b(LO/N;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1015
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1016
    if-eqz p1, :cond_1e

    .line 1017
    new-instance v0, LL/D;

    const-string v1, "step"

    invoke-virtual {p1}, LO/N;->i()I

    move-result v2

    invoke-direct {v0, v1, v2}, LL/D;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 1019
    const-string v0, "p"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 1021
    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/N;Z)V

    .line 1022
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1024
    :cond_1e
    return-void
.end method

.method public final b(LO/g;)V
    .registers 2
    .parameter

    .prologue
    .line 1078
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1079
    invoke-virtual {p0, p1}, LQ/s;->a(LO/g;)V

    .line 1080
    return-void
.end method

.method public final b(LO/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 981
    invoke-direct {p0}, LQ/s;->aE()V

    .line 982
    invoke-virtual {p0}, LQ/s;->ab()Z

    move-result v0

    if-nez v0, :cond_24

    .line 984
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->b(LO/j;II)V

    .line 988
    :cond_24
    return-void
.end method

.method public b(LO/z;[LO/z;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1046
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1047
    invoke-virtual {p1}, LO/z;->i()Z

    move-result v0

    if-eqz v0, :cond_3d

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 1048
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->i()I

    move-result v0

    .line 1049
    invoke-virtual {p1}, LO/z;->k()I

    move-result v1

    if-le v1, v0, :cond_34

    .line 1051
    invoke-virtual {p1, v0}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    .line 1062
    :goto_30
    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/z;[LO/z;)V

    .line 1067
    return-void

    .line 1056
    :cond_34
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_30

    .line 1059
    :cond_3d
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(LO/N;)V

    .line 1060
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->f()V

    goto :goto_30
.end method

.method public final b(Lcom/google/android/maps/driveabout/app/bS;)V
    .registers 9
    .parameter

    .prologue
    const v6, 0x7f1000f5

    const v5, 0x7f1000f2

    const v4, 0x7f1000f1

    const v3, 0x7f1004a6

    const v2, 0x7f1004a5

    .line 403
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 405
    const v0, 0x7f10011f

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 411
    :goto_25
    const v0, 0x7f1004a1

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 412
    const v0, 0x7f1004a2

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 417
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_99

    .line 418
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 419
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 420
    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 421
    const v0, 0x7f10011d

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 422
    const v0, 0x7f100117

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 423
    const v0, 0x7f1004a3

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 424
    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 425
    const v0, 0x7f1004a4

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 426
    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 457
    :goto_64
    const v0, 0x7f1004a9

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 463
    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 470
    invoke-virtual {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    .line 473
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_91

    .line 474
    const v0, 0x7f1004a9

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 475
    const v0, 0x7f1004a8

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 476
    const v0, 0x7f1004a7

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 477
    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 479
    :cond_91
    return-void

    .line 407
    :cond_92
    const v0, 0x7f10011f

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    goto :goto_25

    .line 428
    :cond_99
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 429
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 430
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 435
    :goto_af
    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 436
    const v0, 0x7f10011d

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 437
    const v0, 0x7f100117

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 438
    const v0, 0x7f1004a3

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 439
    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 440
    const v0, 0x7f1004a4

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 441
    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_64

    .line 432
    :cond_d1
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 433
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_af
.end method

.method public final b(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 540
    invoke-direct {p0}, LQ/s;->aE()V

    .line 541
    new-instance v0, LL/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LL/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 545
    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 546
    invoke-virtual {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 547
    return-void
.end method

.method public final b(Lo/T;)V
    .registers 2
    .parameter

    .prologue
    .line 733
    invoke-direct {p0}, LQ/s;->aE()V

    .line 734
    invoke-virtual {p0, p1}, LQ/s;->a(Lo/T;)V

    .line 735
    return-void
.end method

.method protected b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1265
    if-eqz p1, :cond_a

    .line 1266
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->l:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 1268
    :cond_a
    return-void
.end method

.method protected final c(LO/g;)I
    .registers 4
    .parameter

    .prologue
    .line 1358
    invoke-virtual {p1}, LO/g;->m()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, LO/g;->n()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1359
    :cond_c
    const v0, 0x7f0d00c3

    .line 1366
    :goto_f
    return v0

    .line 1360
    :cond_10
    invoke-virtual {p1}, LO/g;->p()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1361
    const v0, 0x7f0d00c2

    goto :goto_f

    .line 1362
    :cond_1a
    invoke-virtual {p1}, LO/g;->q()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_31

    invoke-virtual {p1}, LO/g;->l()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 1364
    const v0, 0x7f0d00c8

    goto :goto_f

    .line 1366
    :cond_31
    const v0, 0x7f0d00c1

    goto :goto_f
.end method

.method public c()V
    .registers 1

    .prologue
    .line 270
    return-void
.end method

.method protected c(I)V
    .registers 3
    .parameter

    .prologue
    .line 1852
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->a(I)V

    .line 1853
    invoke-direct {p0}, LQ/s;->ay()V

    .line 1854
    return-void
.end method

.method protected c(LO/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1320
    invoke-virtual {p0}, LQ/s;->ab()Z

    move-result v0

    if-nez v0, :cond_21

    .line 1321
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;II)V

    .line 1325
    :cond_21
    return-void
.end method

.method public final c(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 551
    invoke-direct {p0}, LQ/s;->aE()V

    .line 552
    new-instance v0, LL/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LL/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 556
    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 557
    invoke-virtual {p0, p1}, LQ/s;->d(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 558
    return-void
.end method

.method public final c(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 493
    invoke-direct {p0}, LQ/s;->aE()V

    .line 498
    new-instance v3, LL/D;

    const-string v4, "mute"

    if-nez p1, :cond_3c

    move v0, v1

    :goto_c
    invoke-direct {v3, v4, v0}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v3}, Ll/f;->b(Ll/j;)V

    .line 500
    const-string v0, "u"

    if-nez p1, :cond_3e

    :goto_16
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 502
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/a;->c(Z)V

    .line 503
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->o()V

    .line 504
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Z)V

    .line 505
    invoke-direct {p0}, LQ/s;->aq()V

    .line 506
    return-void

    :cond_3c
    move v0, v2

    .line 498
    goto :goto_c

    :cond_3e
    move v1, v2

    .line 500
    goto :goto_16
.end method

.method public d()V
    .registers 1

    .prologue
    .line 250
    invoke-virtual {p0}, LQ/s;->e()V

    .line 251
    return-void
.end method

.method protected d(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 5
    .parameter

    .prologue
    .line 1249
    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/cu;

    if-eqz v0, :cond_34

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 1254
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    .line 1255
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    .line 1256
    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    .line 1257
    invoke-virtual {p0}, LQ/s;->aj()V

    .line 1262
    :cond_33
    :goto_33
    return-void

    .line 1258
    :cond_34
    instance-of v0, p1, Lo/G;

    if-eqz v0, :cond_33

    .line 1259
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    check-cast p1, Lo/G;

    invoke-interface {p1}, Lo/G;->a()Lo/U;

    move-result-object v2

    invoke-virtual {v2}, Lo/U;->a()Lo/o;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bp;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cQ;Lo/o;)V

    goto :goto_33
.end method

.method public final d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 752
    invoke-direct {p0}, LQ/s;->aE()V

    .line 753
    new-instance v0, LL/D;

    const-string v1, "viewTraffic"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 755
    const-string v0, "f"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 757
    invoke-virtual {p0, p1}, LQ/s;->b(Z)V

    .line 760
    return-void
.end method

.method public e()V
    .registers 1

    .prologue
    .line 262
    return-void
.end method

.method public final e(Z)V
    .registers 4
    .parameter

    .prologue
    .line 781
    invoke-direct {p0}, LQ/s;->aE()V

    .line 782
    new-instance v0, LL/D;

    const-string v1, "viewSatellite"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 784
    const-string v0, "S"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 785
    invoke-virtual {p0, p1}, LQ/s;->h(Z)V

    .line 786
    return-void
.end method

.method protected f()I
    .registers 2

    .prologue
    .line 1499
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->hasBearing()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1501
    :cond_1c
    const/4 v0, 0x2

    .line 1503
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method public final f(Z)V
    .registers 4
    .parameter

    .prologue
    .line 794
    invoke-direct {p0}, LQ/s;->aE()V

    .line 795
    new-instance v0, LL/D;

    const-string v1, "viewBicycling"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 797
    const-string v0, "b"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 798
    invoke-virtual {p0, p1}, LQ/s;->i(Z)V

    .line 799
    return-void
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 1191
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->f()V

    .line 1192
    return-void
.end method

.method public final g(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1089
    invoke-direct {p0}, LQ/s;->aE()V

    .line 1090
    const-string v0, ","

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 1092
    invoke-virtual {p0}, LQ/s;->s()V

    .line 1093
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1095
    if-eqz p1, :cond_1d

    .line 1096
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->h()V

    .line 1098
    :cond_1d
    return-void
.end method

.method protected h()V
    .registers 4

    .prologue
    .line 1199
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1200
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(F)F

    .line 1201
    invoke-virtual {p0}, LQ/s;->ah()V

    .line 1203
    :cond_1d
    return-void
.end method

.method protected h(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1284
    if-eqz p1, :cond_32

    .line 1285
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 1291
    :goto_d
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "SatelliteImagery"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 1294
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1295
    return-void

    .line 1286
    :cond_32
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 1287
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_d

    .line 1289
    :cond_4a
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_d
.end method

.method protected i()V
    .registers 4

    .prologue
    .line 1210
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1211
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/high16 v1, -0x4080

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(F)F

    .line 1212
    invoke-virtual {p0}, LQ/s;->ah()V

    .line 1214
    :cond_1d
    return-void
.end method

.method protected i(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1298
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->b(Z)V

    .line 1299
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "BicyclingLayer"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 1302
    invoke-direct {p0}, LQ/s;->aD()V

    .line 1303
    return-void
.end method

.method protected j()Z
    .registers 2

    .prologue
    .line 1217
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    move-result v0

    return v0
.end method

.method protected k()V
    .registers 4

    .prologue
    .line 1271
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 1272
    return-void
.end method

.method protected l()V
    .registers 1

    .prologue
    .line 1338
    return-void
.end method

.method protected m()V
    .registers 1

    .prologue
    .line 1716
    return-void
.end method

.method protected n()V
    .registers 1

    .prologue
    .line 1377
    return-void
.end method

.method protected o()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1846
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v0

    check-cast v0, LQ/d;

    invoke-virtual {v0, v2}, LQ/d;->b_(Z)V

    .line 1848
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 1849
    return-void
.end method

.method public p()V
    .registers 1

    .prologue
    .line 205
    return-void
.end method

.method public q()V
    .registers 1

    .prologue
    .line 211
    return-void
.end method

.method protected r()V
    .registers 2

    .prologue
    .line 1306
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    .line 1307
    return-void
.end method

.method protected s()V
    .registers 1

    .prologue
    .line 1371
    return-void
.end method

.method protected t()V
    .registers 1

    .prologue
    .line 1374
    return-void
.end method

.method protected u()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1552
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->y()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1553
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0066

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    .line 1567
    :goto_24
    return-void

    .line 1555
    :cond_25
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    if-nez v0, :cond_3d

    .line 1556
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_24

    .line 1558
    :cond_3d
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-nez v0, :cond_55

    .line 1559
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_24

    .line 1561
    :cond_55
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v2

    invoke-direct {p0}, LQ/s;->aC()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V

    goto :goto_24
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 1741
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    .line 1743
    return-void
.end method

.method protected w()V
    .registers 6

    .prologue
    .line 1767
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    .line 1768
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, -0x1

    :goto_13
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/cQ;->setTimeRemaining(IZIZ)V

    .line 1772
    return-void

    .line 1768
    :cond_20
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v0

    goto :goto_13
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 1575
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-direct {p0}, LQ/s;->aw()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 1576
    return-void
.end method

.method protected y()V
    .registers 1

    .prologue
    .line 1275
    return-void
.end method

.method protected z()V
    .registers 1

    .prologue
    .line 1278
    return-void
.end method
