.class public Lr/B;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lt/M;

.field b:Lt/f;

.field volatile c:Z

.field private d:I

.field private final e:Z

.field private f:Ljava/util/Locale;

.field private final g:Ljava/lang/String;

.field private h:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lt/M;Lt/f;ZLjava/util/Locale;Ljava/io/File;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lr/B;->g:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lr/B;->a:Lt/M;

    .line 62
    iput-object p3, p0, Lr/B;->b:Lt/f;

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lr/B;->d:I

    .line 64
    iput-boolean p4, p0, Lr/B;->e:Z

    .line 65
    iput-object p5, p0, Lr/B;->f:Ljava/util/Locale;

    .line 66
    iput-object p6, p0, Lr/B;->h:Ljava/io/File;

    .line 67
    return-void
.end method


# virtual methods
.method a()V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lr/B;->b:Lt/f;

    iget-object v1, p0, Lr/B;->h:Ljava/io/File;

    invoke-interface {v0, v1}, Lt/f;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    .line 76
    :cond_11
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_2d

    .line 77
    iget-object v0, p0, Lr/B;->f:Ljava/util/Locale;

    iget-object v1, p0, Lr/B;->b:Lt/f;

    invoke-interface {v1}, Lt/f;->f()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 78
    iget-object v0, p0, Lr/B;->b:Lt/f;

    iget-object v1, p0, Lr/B;->f:Ljava/util/Locale;

    invoke-interface {v0, v1}, Lt/f;->a(Ljava/util/Locale;)Z

    .line 80
    :cond_2a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/B;->c:Z

    .line 86
    :cond_2d
    monitor-enter p0

    .line 87
    :try_start_2e
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 88
    monitor-exit p0

    .line 89
    return-void

    .line 88
    :catchall_33
    move-exception v0

    monitor-exit p0
    :try_end_35
    .catchall {:try_start_2e .. :try_end_35} :catchall_33

    throw v0
.end method

.method a(Ljava/util/Locale;)V
    .registers 3
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lr/B;->f:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 168
    :goto_8
    return-void

    .line 160
    :cond_9
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_18

    invoke-interface {v0, p1}, Lt/f;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    .line 164
    :cond_18
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_21

    .line 165
    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    .line 167
    :cond_21
    iput-object p1, p0, Lr/B;->f:Ljava/util/Locale;

    goto :goto_8
.end method

.method a(I)Z
    .registers 4
    .parameter

    .prologue
    .line 137
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_d

    invoke-interface {v0, p1}, Lt/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_d

    .line 139
    const/4 v0, 0x0

    .line 141
    :cond_d
    iput p1, p0, Lr/B;->d:I

    .line 143
    iget-boolean v1, p0, Lr/B;->e:Z

    if-eqz v1, :cond_26

    .line 144
    if-eqz v0, :cond_1b

    invoke-interface {v0}, Lt/f;->a()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 147
    :cond_1b
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_24

    .line 148
    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    .line 150
    :cond_24
    const/4 v0, 0x1

    .line 152
    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method a(Lo/aq;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 221
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 222
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v1

    invoke-interface {v1, p1}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_20

    invoke-interface {v1}, Lo/ap;->e()I

    move-result v1

    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v2

    invoke-interface {v2}, Lt/f;->c()I

    move-result v2

    if-ne v1, v2, :cond_20

    const/4 v0, 0x1

    .line 226
    :cond_20
    return v0
.end method

.method public b()Lt/f;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_21

    iget-boolean v0, p0, Lr/B;->c:Z

    if-nez v0, :cond_21

    .line 96
    monitor-enter p0

    .line 98
    :goto_9
    :try_start_9
    iget-object v0, p0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_20

    iget-boolean v0, p0, Lr/B;->c:Z

    if-nez v0, :cond_20

    .line 99
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_14
    .catchall {:try_start_9 .. :try_end_14} :catchall_24
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_14} :catch_15

    goto :goto_9

    .line 101
    :catch_15
    move-exception v0

    .line 102
    :try_start_16
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 103
    const/4 v0, 0x0

    monitor-exit p0

    .line 107
    :goto_1f
    return-object v0

    .line 105
    :cond_20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_16 .. :try_end_21} :catchall_24

    .line 107
    :cond_21
    iget-object v0, p0, Lr/B;->b:Lt/f;

    goto :goto_1f

    .line 105
    :catchall_24
    move-exception v0

    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    throw v0
.end method

.method protected c()V
    .registers 3

    .prologue
    .line 111
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    .line 114
    :cond_9
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_22

    invoke-interface {v0}, Lt/f;->a()Z

    move-result v1

    if-nez v1, :cond_22

    .line 116
    invoke-interface {v0}, Lt/f;->g()V

    .line 117
    iget-object v0, p0, Lr/B;->g:Ljava/lang/String;

    const-string v1, "Unable to clear disk cache"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lr/B;->b:Lt/f;

    .line 120
    :cond_22
    return-void
.end method

.method d()I
    .registers 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_b

    .line 125
    invoke-interface {v0}, Lt/f;->c()I

    move-result v0

    .line 127
    :goto_a
    return v0

    :cond_b
    iget v0, p0, Lr/B;->d:I

    goto :goto_a
.end method

.method e()J
    .registers 3

    .prologue
    .line 171
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 172
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-interface {v0}, Lt/f;->e()J

    move-result-wide v0

    .line 174
    :goto_e
    return-wide v0

    :cond_f
    const-wide/16 v0, 0x0

    goto :goto_e
.end method

.method f()J
    .registers 3

    .prologue
    .line 179
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 180
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-interface {v0}, Lt/f;->d()J

    move-result-wide v0

    .line 182
    :goto_e
    return-wide v0

    :cond_f
    const-wide/16 v0, 0x0

    goto :goto_e
.end method

.method g()Z
    .registers 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public h()V
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_9

    .line 206
    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    .line 208
    :cond_9
    invoke-virtual {p0}, Lr/B;->b()Lt/f;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_12

    .line 210
    invoke-interface {v0}, Lt/f;->g()V

    .line 212
    :cond_12
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_9

    .line 216
    iget-object v0, p0, Lr/B;->a:Lt/M;

    invoke-interface {v0}, Lt/M;->a()Z

    .line 218
    :cond_9
    return-void
.end method
