.class public Lr/I;
.super Lr/a;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/List;


# instance fields
.field protected j:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lr/I;->i:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Law/p;LA/c;IFLjava/util/Locale;ZLjava/io/File;Lt/g;)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    iget-object v2, v0, LA/c;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Lr/I;->a(Law/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x100

    sget-object v6, Lr/I;->i:Ljava/util/List;

    const/4 v7, 0x1

    const/4 v10, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    invoke-direct/range {v1 .. v14}, Lr/a;-><init>(Law/p;Ljava/lang/String;LA/c;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lt/g;)V

    .line 35
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lr/I;->j:J

    .line 60
    return-void
.end method

.method private static a(Law/p;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 69
    if-eqz p0, :cond_14

    invoke-interface {p0}, Law/p;->x()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    const-string v0, "DriveAbout"

    invoke-interface {p0}, Law/p;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 71
    :cond_14
    const-string v0, ""

    .line 76
    :goto_16
    return-object v0

    :cond_17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Law/p;->x()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3a

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_16
.end method


# virtual methods
.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 152
    iput-wide p1, p0, Lr/I;->j:J

    .line 153
    return-void
.end method

.method public l()V
    .registers 2

    .prologue
    .line 101
    invoke-static {}, Lbm/g;->a()V

    .line 104
    :try_start_3
    invoke-super {p0}, Lr/a;->l()V
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_a

    .line 106
    invoke-static {}, Lbm/g;->b()V

    .line 108
    return-void

    .line 106
    :catchall_a
    move-exception v0

    invoke-static {}, Lbm/g;->b()V

    throw v0
.end method

.method protected m()Lr/h;
    .registers 2

    .prologue
    .line 112
    new-instance v0, Lr/J;

    invoke-direct {v0, p0}, Lr/J;-><init>(Lr/I;)V

    return-object v0
.end method
