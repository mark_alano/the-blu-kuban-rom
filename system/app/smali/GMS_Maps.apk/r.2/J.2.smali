.class Lr/J;
.super Lr/b;
.source "SourceFile"


# instance fields
.field final synthetic d:Lr/I;


# direct methods
.method constructor <init>(Lr/I;)V
    .registers 2
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lr/J;->d:Lr/I;

    invoke-direct {p0, p1}, Lr/b;-><init>(Lr/a;)V

    return-void
.end method


# virtual methods
.method protected a(II)[B
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 139
    invoke-static {}, Lo/aL;->u()I

    move-result v0

    add-int/2addr v0, p1

    new-array v0, v0, [B

    .line 140
    iget v1, p0, Lr/J;->a:I

    invoke-static {v1, p2, v0}, Lo/aL;->a(II[B)V

    .line 141
    return-object v0
.end method

.method protected b(I)Lo/ap;
    .registers 10
    .parameter

    .prologue
    const-wide/16 v6, -0x1

    .line 115
    iget-object v0, p0, Lr/J;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_a

    .line 116
    const/4 v0, 0x0

    .line 129
    :goto_9
    return-object v0

    .line 120
    :cond_a
    iget-object v0, p0, Lr/J;->d:Lr/I;

    iget-wide v0, v0, Lr/I;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_43

    .line 121
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-object v2, p0, Lr/J;->d:Lr/I;

    iget-wide v2, v2, Lr/I;->j:J

    add-long v4, v0, v2

    .line 124
    :goto_26
    invoke-virtual {p0, p1}, Lr/J;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    iget-object v1, p0, Lr/J;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    iget-object v3, p0, Lr/J;->d:Lr/I;

    iget-object v3, v3, Lr/I;->a:LA/c;

    invoke-static/range {v0 .. v7}, Lo/aL;->a(Lo/aq;[BILA/c;JJ)Lo/aL;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lr/J;->d:Lr/I;

    iget-object v1, v1, Lr/I;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v0, v1}, Lo/ap;->c(Lcom/google/googlenav/common/a;)V

    goto :goto_9

    :cond_43
    move-wide v4, v6

    goto :goto_26
.end method

.method protected c(I)[B
    .registers 3
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lr/J;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
