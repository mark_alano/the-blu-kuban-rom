.class public abstract Lr/a;
.super Lr/d;
.source "SourceFile"


# instance fields
.field protected final a:LA/c;

.field private volatile i:Z

.field private final j:I

.field private final k:Ljava/util/List;

.field private final l:I

.field private final m:I

.field private final n:F

.field private o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method protected constructor <init>(Law/p;Ljava/lang/String;LA/c;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lt/g;)V
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    invoke-virtual {p3}, LA/c;->d()Lt/M;

    move-result-object v5

    move/from16 v0, p11

    move-object/from16 v1, p13

    invoke-static {p2, p3, v0, v1}, Lr/a;->a(Ljava/lang/String;LA/c;ZLt/g;)Lt/f;

    move-result-object v6

    invoke-static {p3}, Lr/a;->a(LA/c;)I

    move-result v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v8, p9

    move/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p12

    invoke-direct/range {v2 .. v11}, Lr/d;-><init>(Law/p;Ljava/lang/String;Lt/M;Lt/f;IZILjava/util/Locale;Ljava/io/File;)V

    .line 165
    const/4 v2, 0x0

    iput-boolean v2, p0, Lr/a;->i:Z

    .line 235
    iput-object p3, p0, Lr/a;->a:LA/c;

    .line 236
    move/from16 v0, p4

    iput v0, p0, Lr/a;->j:I

    .line 237
    move-object/from16 v0, p5

    iput-object v0, p0, Lr/a;->k:Ljava/util/List;

    .line 238
    move/from16 v0, p6

    iput v0, p0, Lr/a;->m:I

    .line 243
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_74

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_74

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_74

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_74

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7c

    .line 248
    :cond_74
    const/4 v2, 0x0

    iput v2, p0, Lr/a;->l:I

    .line 252
    :goto_77
    move/from16 v0, p8

    iput v0, p0, Lr/a;->n:F

    .line 253
    return-void

    .line 250
    :cond_7c
    invoke-static/range {p4 .. p4}, Lr/a;->a(I)I

    move-result v2

    iput v2, p0, Lr/a;->l:I

    goto :goto_77
.end method

.method static a(I)I
    .registers 4
    .parameter

    .prologue
    const/16 v2, 0x80

    .line 285
    const/4 v0, 0x0

    move v1, p0

    .line 286
    :goto_4
    if-le v1, v2, :cond_b

    .line 287
    shr-int/lit8 v1, v1, 0x1

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 290
    :cond_b
    :goto_b
    if-ge v1, v2, :cond_12

    .line 291
    shl-int/lit8 v1, v1, 0x1

    .line 292
    add-int/lit8 v0, v0, -0x1

    goto :goto_b

    .line 294
    :cond_12
    return v0
.end method

.method private static a(LA/c;)I
    .registers 2
    .parameter

    .prologue
    .line 275
    sget-object v0, LA/c;->d:LA/c;

    if-ne p0, v0, :cond_7

    const/16 v0, 0x3e8

    :goto_6
    return v0

    :cond_7
    const/16 v0, 0xbb8

    goto :goto_6
.end method

.method static synthetic a(Lr/a;)I
    .registers 2
    .parameter

    .prologue
    .line 48
    iget v0, p0, Lr/a;->j:I

    return v0
.end method

.method private static a(Ljava/lang/String;LA/c;ZLt/g;)Lt/f;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 260
    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 263
    invoke-virtual {p1, p0, p2, p3}, LA/c;->a(Ljava/lang/String;ZLt/g;)Lt/f;

    move-result-object v0

    .line 265
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method static synthetic b(Lr/a;)I
    .registers 2
    .parameter

    .prologue
    .line 48
    iget v0, p0, Lr/a;->m:I

    return v0
.end method

.method static synthetic c(Lr/a;)F
    .registers 2
    .parameter

    .prologue
    .line 48
    iget v0, p0, Lr/a;->n:F

    return v0
.end method

.method static synthetic d(Lr/a;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lr/a;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lr/a;)Z
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-boolean v0, p0, Lr/a;->i:Z

    return v0
.end method

.method static synthetic f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic g(Lr/a;)I
    .registers 2
    .parameter

    .prologue
    .line 48
    iget v0, p0, Lr/a;->l:I

    return v0
.end method


# virtual methods
.method public a()LA/c;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, Lr/a;->a:LA/c;

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 568
    iget-object v0, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, p1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 569
    const-string v0, "jrg"

    const-string v1, "setting spolightDescription"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    iput-object p1, p0, Lr/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 571
    invoke-virtual {p0}, Lr/a;->b()V

    .line 573
    :cond_14
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 298
    iput-boolean p1, p0, Lr/a;->i:Z

    .line 299
    return-void
.end method
