.class public abstract Lr/b;
.super Lr/h;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:[[B

.field final synthetic c:Lr/a;


# direct methods
.method protected constructor <init>(Lr/a;)V
    .registers 3
    .parameter

    .prologue
    const/16 v0, 0x8

    .line 314
    iput-object p1, p0, Lr/b;->c:Lr/a;

    .line 315
    invoke-direct {p0, v0}, Lr/h;-><init>(I)V

    .line 316
    new-array v0, v0, [[B

    iput-object v0, p0, Lr/b;->b:[[B

    .line 317
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;
    .registers 10
    .parameter

    .prologue
    .line 535
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 536
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 537
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iget-object v3, p0, Lr/b;->c:Lr/a;

    invoke-static {v3}, Lr/a;->g(Lr/a;)I

    move-result v3

    sub-int v3, v0, v3

    .line 539
    new-instance v4, Lo/aB;

    invoke-direct {v4}, Lo/aB;-><init>()V

    .line 540
    invoke-static {}, Lo/av;->values()[Lo/av;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    :goto_22
    if-ge v0, v6, :cond_32

    aget-object v7, v5, v0

    .line 541
    invoke-virtual {v7, p1}, Lo/av;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/at;

    move-result-object v7

    .line 542
    if-eqz v7, :cond_2f

    .line 543
    invoke-virtual {v4, v7}, Lo/aB;->a(Lo/at;)V

    .line 540
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 546
    :cond_32
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, LA/c;->a(I)LA/c;

    move-result-object v0

    .line 547
    new-instance v5, Lo/aq;

    invoke-direct {v5, v3, v1, v2, v4}, Lo/aq;-><init>(IIILo/aB;)V

    .line 548
    invoke-static {v0, v5}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)V
    .registers 6
    .parameter

    .prologue
    .line 449
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 450
    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hs;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    .line 455
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, Lr/b;->a:I

    .line 456
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 457
    if-eqz v0, :cond_35

    .line 458
    iget-object v1, p0, Lr/b;->c:Lr/a;

    invoke-virtual {v1}, Lr/a;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received tile response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_35
    return-void
.end method

.method private b(Ljava/io/InputStream;)V
    .registers 12
    .parameter

    .prologue
    const/16 v9, 0x8

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 463
    invoke-virtual {p0}, Lr/b;->c()I

    move-result v5

    move v0, v1

    .line 468
    :goto_9
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 469
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hs;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v2

    .line 472
    if-ne v2, v3, :cond_1a

    .line 519
    if-eq v0, v5, :cond_19

    .line 523
    :cond_19
    return-void

    .line 476
    :cond_1a
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 477
    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_41

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 481
    :goto_29
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v7

    .line 482
    if-eqz v7, :cond_43

    array-length v4, v7

    .line 489
    :goto_31
    invoke-virtual {p0, v4, v2}, Lr/b;->a(II)[B

    move-result-object v2

    .line 491
    if-eqz v7, :cond_3c

    .line 492
    array-length v8, v2

    sub-int/2addr v8, v4

    invoke-static {v7, v1, v2, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 495
    :cond_3c
    if-lt v0, v5, :cond_45

    .line 516
    :cond_3e
    :goto_3e
    add-int/lit8 v0, v0, 0x1

    .line 517
    goto :goto_9

    :cond_41
    move v2, v3

    .line 477
    goto :goto_29

    :cond_43
    move v4, v1

    .line 482
    goto :goto_31

    .line 498
    :cond_45
    invoke-direct {p0, v6}, Lr/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;

    move-result-object v6

    .line 499
    invoke-virtual {p0, v6}, Lr/b;->a(Landroid/util/Pair;)Ljava/lang/Integer;

    move-result-object v6

    .line 500
    if-nez v6, :cond_5b

    .line 501
    iget-object v2, p0, Lr/b;->c:Lr/a;

    invoke-virtual {v2}, Lr/a;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Received wrong tile"

    invoke-static {v2, v4}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3e

    .line 502
    :cond_5b
    if-eqz v4, :cond_3e

    .line 513
    iget-object v4, p0, Lr/b;->b:[[B

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput-object v2, v4, v6

    goto :goto_3e
.end method

.method private k()Z
    .registers 2

    .prologue
    .line 414
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->F()Z

    move-result v0

    return v0
.end method

.method private l()Lr/c;
    .registers 6

    .prologue
    .line 423
    sget-object v1, Lr/c;->a:Lr/c;

    .line 425
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lr/b;->c()I

    move-result v2

    if-ge v0, v2, :cond_23

    .line 426
    invoke-virtual {p0, v0}, Lr/b;->a(I)Lr/k;

    move-result-object v2

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v2

    .line 427
    sget-object v3, Lr/c;->a:Lr/c;

    if-eq v1, v3, :cond_1f

    invoke-virtual {v2}, Lr/c;->a()I

    move-result v3

    invoke-virtual {v1}, Lr/c;->a()I

    move-result v4

    if-ge v3, v4, :cond_20

    :cond_1f
    move-object v1, v2

    .line 425
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 432
    :cond_23
    return-object v1
.end method


# virtual methods
.method protected a()I
    .registers 2

    .prologue
    .line 321
    iget v0, p0, Lr/b;->a:I

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 14
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x3

    .line 334
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 336
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hs;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 339
    invoke-virtual {v2, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 340
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->a(Lr/a;)I

    move-result v0

    invoke-virtual {v1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 341
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->b(Lr/a;)I

    move-result v0

    invoke-virtual {v1, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 342
    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    invoke-virtual {v0}, Lr/c;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 343
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->c(Lr/a;)F

    move-result v0

    const/high16 v3, 0x3f80

    cmpl-float v0, v0, v3

    if-lez v0, :cond_49

    .line 344
    const/4 v0, 0x6

    iget-object v3, p0, Lr/b;->c:Lr/a;

    invoke-static {v3}, Lr/a;->c(Lr/a;)F

    move-result v3

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 347
    :cond_49
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->d(Lr/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_53
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 348
    invoke-virtual {v1, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_53

    .line 351
    :cond_67
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->e(Lr/a;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 352
    invoke-virtual {v1, v8, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 356
    :cond_72
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 357
    const/4 v0, 0x0

    invoke-virtual {v1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 361
    :cond_7c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->u()Z

    move-result v0

    if-eqz v0, :cond_89

    .line 362
    invoke-virtual {v1, v8, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 366
    :cond_89
    invoke-direct {p0}, Lr/b;->k()Z

    move-result v0

    if-eqz v0, :cond_92

    .line 367
    invoke-virtual {v1, v8, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 371
    :cond_92
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_9d

    .line 372
    invoke-virtual {v1, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 376
    :cond_9d
    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    sget-object v3, Lr/c;->a:Lr/c;

    if-eq v0, v3, :cond_b0

    .line 377
    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    invoke-virtual {v0}, Lr/c;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 385
    :cond_b0
    invoke-virtual {p0}, Lr/b;->c()I

    move-result v1

    .line 386
    const/4 v0, 0x0

    :goto_b5
    if-ge v0, v1, :cond_12a

    .line 387
    invoke-virtual {p0, v0}, Lr/b;->a(I)Lr/k;

    move-result-object v3

    .line 388
    invoke-virtual {v3}, Lr/k;->c()Lo/aq;

    move-result-object v4

    .line 389
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/hs;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 390
    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v6

    invoke-virtual {v5, v10, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 391
    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v6

    invoke-virtual {v5, v8, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 392
    invoke-virtual {v4}, Lo/aq;->b()I

    move-result v6

    iget-object v7, p0, Lr/b;->c:Lr/a;

    invoke-static {v7}, Lr/a;->g(Lr/a;)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v5, v11, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 393
    iget-object v6, p0, Lr/b;->c:Lr/a;

    iget-object v6, v6, Lr/a;->a:LA/c;

    iget v6, v6, LA/c;->v:I

    invoke-virtual {v5, v9, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 394
    const/4 v6, 0x7

    iget-object v7, p0, Lr/b;->c:Lr/a;

    iget-object v7, v7, Lr/a;->a:LA/c;

    iget v7, v7, LA/c;->w:I

    invoke-virtual {v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 395
    iget-object v6, p0, Lr/b;->c:Lr/a;

    invoke-static {v6}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    if-eqz v6, :cond_108

    .line 396
    const/16 v6, 0x1b

    iget-object v7, p0, Lr/b;->c:Lr/a;

    invoke-static {v7}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 398
    :cond_108
    iget-object v6, p0, Lr/b;->c:Lr/a;

    iget-object v6, v6, Lr/a;->a:LA/c;

    invoke-virtual {v4, v6, v5}, Lo/aq;->a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 399
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->u()Z

    move-result v4

    if-eqz v4, :cond_122

    .line 402
    const/16 v4, 0x8

    invoke-virtual {v3}, Lr/k;->g()I

    move-result v3

    invoke-virtual {v5, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 404
    :cond_122
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_b5

    .line 407
    :cond_12a
    invoke-static {p1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 408
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 4
    .parameter

    .prologue
    .line 553
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    .line 556
    :try_start_4
    invoke-direct {p0, v1}, Lr/b;->a(Ljava/io/InputStream;)V

    .line 557
    invoke-direct {p0, v1}, Lr/b;->b(Ljava/io/InputStream;)V
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_f

    .line 559
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 562
    const/4 v0, 0x1

    return v0

    .line 559
    :catchall_f
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method protected a(Lr/k;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 442
    invoke-virtual {p0}, Lr/b;->c()I

    move-result v2

    if-nez v2, :cond_9

    .line 445
    :cond_8
    :goto_8
    return v0

    :cond_9
    invoke-virtual {p0, v1}, Lr/b;->a(I)Lr/k;

    move-result-object v2

    invoke-virtual {v2}, Lr/k;->c()Lo/aq;

    move-result-object v2

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->b()I

    move-result v3

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_8
.end method

.method protected a(II)[B
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 436
    new-array v0, p1, [B

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 329
    const/16 v0, 0x6c

    return v0
.end method
