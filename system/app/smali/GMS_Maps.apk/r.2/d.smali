.class public abstract Lr/d;
.super LR/c;
.source "SourceFile"

# interfaces
.implements Law/q;
.implements Lr/t;
.implements Lr/z;


# instance fields
.field private a:Ljava/util/Locale;

.field protected b:Lr/B;

.field protected c:Lr/h;

.field volatile d:I

.field protected e:Lcom/google/googlenav/common/a;

.field volatile f:I

.field volatile g:I

.field private volatile i:Lr/i;

.field private final j:Ljava/util/concurrent/locks/ReentrantLock;

.field private final k:Law/p;

.field private l:Landroid/os/Handler;

.field private m:Landroid/os/Looper;

.field private n:Z

.field private final o:Ljava/util/List;

.field private final p:LR/h;

.field private final q:Ljava/util/Map;

.field private final r:I

.field private s:Z

.field private t:Lcom/google/googlenav/bE;

.field private final u:Ljava/util/ArrayList;

.field private volatile v:Z

.field private w:Ls/e;


# direct methods
.method protected constructor <init>(Law/p;Ljava/lang/String;Lt/M;Lt/f;IZILjava/util/Locale;Ljava/io/File;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 206
    invoke-direct {p0, p2}, LR/c;-><init>(Ljava/lang/String;)V

    .line 86
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    .line 106
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lr/d;->o:Ljava/util/List;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lr/d;->q:Ljava/util/Map;

    .line 133
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->s:Z

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->v:Z

    .line 167
    new-instance v0, Lr/e;

    invoke-direct {v0, p0}, Lr/e;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->w:Ls/e;

    .line 207
    new-instance v0, Lr/B;

    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v2, p3

    move-object v3, p4

    move v4, p6

    move-object v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Lr/B;-><init>(Ljava/lang/String;Lt/M;Lt/f;ZLjava/util/Locale;Ljava/io/File;)V

    iput-object v0, p0, Lr/d;->b:Lr/B;

    .line 213
    iput p5, p0, Lr/d;->r:I

    .line 214
    iput-object p8, p0, Lr/d;->a:Ljava/util/Locale;

    .line 215
    iput-object p1, p0, Lr/d;->k:Law/p;

    .line 216
    invoke-virtual {p0}, Lr/d;->m()Lr/h;

    move-result-object v0

    iput-object v0, p0, Lr/d;->c:Lr/h;

    .line 217
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-static {v0, p0}, Lr/h;->a(Lr/h;Lr/d;)Lr/d;

    .line 218
    new-instance v0, Lr/f;

    invoke-direct {v0, p0, p7}, Lr/f;-><init>(Lr/d;I)V

    iput-object v0, p0, Lr/d;->p:LR/h;

    .line 226
    return-void
.end method

.method private a(Lr/k;Z)Landroid/util/Pair;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 1136
    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v1

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v2

    .line 1140
    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    if-eqz v1, :cond_62

    .line 1141
    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    invoke-interface {v1, v2}, Lt/M;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    .line 1142
    if-eqz v1, :cond_62

    iget-object v3, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v3}, Lo/ap;->a(Lcom/google/googlenav/common/a;)Z

    move-result v3

    if-nez v3, :cond_62

    .line 1143
    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    invoke-interface {v2, v1}, Lt/M;->a(Lo/ap;)Z

    move-result v2

    if-eqz v2, :cond_3e

    .line 1144
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 1154
    :goto_35
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 1209
    :goto_3d
    return-object v0

    .line 1146
    :cond_3e
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    if-nez v2, :cond_4d

    iget-object v2, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v2, :cond_4d

    .line 1147
    iget-object v2, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v2}, Lcom/google/googlenav/bE;->a()V

    .line 1149
    :cond_4d
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lr/d;->a(Lo/ap;Z)Lr/k;

    move-result-object v2

    .line 1150
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v3

    if-eqz v3, :cond_60

    .line 1151
    :goto_5b
    invoke-virtual {p0, p1, v5, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    move-object v0, v2

    goto :goto_35

    :cond_60
    move-object v0, v1

    .line 1150
    goto :goto_5b

    .line 1157
    :cond_62
    if-eqz p2, :cond_db

    .line 1159
    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    .line 1160
    if-eqz v1, :cond_db

    .line 1161
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v3

    if-eqz v3, :cond_84

    .line 1162
    invoke-interface {v1, v2}, Lt/f;->b(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_db

    .line 1163
    invoke-virtual {p0, p1, v5, v0}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 1165
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_3d

    .line 1168
    :cond_84
    invoke-interface {v1, v2}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v3

    .line 1169
    if-eqz v3, :cond_db

    iget-object v4, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v3, v4}, Lo/ap;->a(Lcom/google/googlenav/common/a;)Z

    move-result v4

    if-nez v4, :cond_db

    .line 1170
    invoke-interface {v1, v3}, Lt/f;->a(Lo/ap;)Z

    move-result v1

    if-eqz v1, :cond_b3

    .line 1174
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v1

    if-nez v1, :cond_a7

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v1, :cond_a7

    .line 1175
    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v1}, Lcom/google/googlenav/bE;->c()V

    .line 1177
    :cond_a7
    invoke-direct {p0, p1, v2}, Lr/d;->a(Lr/k;Lo/aq;)V

    .line 1192
    :goto_aa
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_3d

    .line 1182
    :cond_b3
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_c2

    iget-object v0, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v0, :cond_c2

    .line 1183
    iget-object v0, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v0}, Lcom/google/googlenav/bE;->b()V

    .line 1185
    :cond_c2
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_cf

    .line 1186
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, v2, v3}, Lt/M;->a(Lo/aq;Lo/ap;)V

    .line 1188
    :cond_cf
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v0

    invoke-direct {p0, v3, v0}, Lr/d;->a(Lo/ap;Z)Lr/k;

    move-result-object v0

    .line 1189
    invoke-virtual {p0, p1, v5, v3}, Lr/d;->a(Lr/k;ILo/ap;)V

    goto :goto_aa

    .line 1197
    :cond_db
    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v1

    if-nez v1, :cond_ea

    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    if-eqz v1, :cond_ea

    .line 1198
    iget-object v1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    invoke-virtual {v1}, Lcom/google/googlenav/bE;->c()V

    .line 1208
    :cond_ea
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lr/k;->a(I)V

    .line 1209
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_3d
.end method

.method private a(Lo/ap;Z)Lr/k;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v4, 0x1

    .line 1220
    invoke-virtual {p0}, Lr/d;->c()I

    move-result v1

    .line 1223
    const/4 v0, 0x0

    .line 1225
    if-eq v1, v6, :cond_37

    invoke-interface {p1}, Lo/ap;->e()I

    move-result v2

    if-eq v1, v2, :cond_37

    move v1, v4

    .line 1241
    :goto_11
    if-eqz v1, :cond_36

    .line 1248
    new-instance v0, Lr/k;

    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    iget-object v2, p0, Lr/d;->w:Ls/e;

    sget-object v3, Lr/c;->b:Lr/c;

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    .line 1252
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1253
    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    invoke-direct {p0, v6, v1}, Lr/d;->a(II)V

    .line 1256
    :cond_36
    return-object v0

    .line 1229
    :cond_37
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_51

    if-nez p2, :cond_51

    .line 1233
    iget-object v1, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {p1, v1}, Lo/ap;->b(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 1236
    invoke-interface {p1}, Lo/ap;->h()I

    move-result v6

    move v1, v4

    .line 1237
    goto :goto_11

    :cond_51
    move v1, v5

    goto :goto_11
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 349
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 350
    invoke-direct {p0}, Lr/d;->n()V

    .line 352
    :cond_b
    return-void
.end method

.method private a(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1718
    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0}, Law/p;->w()J

    move-result-wide v3

    const-wide/16 v5, 0x64

    rem-long/2addr v3, v5

    const-wide/16 v5, 0x8

    cmp-long v0, v3, v5

    if-eqz v0, :cond_1e

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1733
    :goto_1d
    return-void

    .line 1723
    :cond_1e
    const/4 v0, -0x1

    if-eq p1, v0, :cond_75

    move v0, v1

    .line 1724
    :goto_22
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1725
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lo/aL;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1727
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "z="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1728
    const/16 v5, 0x6d

    const-string v6, "u"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    aput-object v0, v7, v2

    aput-object v3, v7, v1

    const/4 v0, 0x2

    aput-object v4, v7, v0

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1d

    :cond_75
    move v0, v2

    .line 1723
    goto :goto_22
.end method

.method private a(IIIIII)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1680
    iget-object v1, p0, Lr/d;->k:Law/p;

    invoke-interface {v1}, Law/p;->w()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x8

    cmp-long v1, v1, v3

    if-eqz v1, :cond_16

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-nez v1, :cond_16

    .line 1705
    :goto_15
    return-void

    .line 1685
    :cond_16
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1687
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "f="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1689
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1691
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1693
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "n="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1695
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1697
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "d="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lo/aL;->t()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1699
    const/16 v8, 0x6d

    const-string v9, "b"

    const/4 v10, 0x7

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    const/4 v1, 0x1

    aput-object v2, v10, v1

    const/4 v1, 0x2

    aput-object v3, v10, v1

    const/4 v1, 0x3

    aput-object v4, v10, v1

    const/4 v1, 0x4

    aput-object v5, v10, v1

    const/4 v1, 0x5

    aput-object v6, v10, v1

    const/4 v1, 0x6

    aput-object v7, v10, v1

    invoke-static {v10}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v9, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_15
.end method

.method static synthetic a(Lr/d;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lr/d;->o()V

    return-void
.end method

.method static synthetic a(Lr/d;Lo/aq;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lr/d;->b(Lo/aq;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lo/ar;Lr/c;Ls/e;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lr/d;->b(Lo/ar;Lr/c;Ls/e;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lr/h;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lr/d;->a(Lr/h;)V

    return-void
.end method

.method static synthetic a(Lr/d;Lr/k;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lr/d;->b(Lr/k;)V

    return-void
.end method

.method private a(Lr/h;)V
    .registers 18
    .parameter

    .prologue
    .line 826
    invoke-direct/range {p0 .. p0}, Lr/d;->s()V

    .line 828
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lr/d;->s:Z

    if-eqz v1, :cond_28

    .line 830
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lr/d;->s:Z

    .line 831
    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->p:LR/h;

    invoke-virtual {v1}, LR/h;->f()I

    move-result v1

    if-eqz v1, :cond_28

    .line 832
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->p:LR/h;

    invoke-virtual {v1}, LR/h;->h()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr/k;

    .line 833
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lr/d;->b(Lr/k;)V

    goto :goto_e

    .line 839
    :cond_28
    invoke-virtual/range {p1 .. p1}, Lr/h;->a()I

    move-result v1

    .line 840
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3a

    invoke-virtual/range {p0 .. p0}, Lr/d;->c()I

    move-result v2

    if-eq v1, v2, :cond_3a

    .line 845
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lr/d;->a(I)V

    .line 847
    :cond_3a
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->o:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_47

    .line 947
    :cond_46
    :goto_46
    return-void

    .line 853
    :cond_47
    move-object/from16 v0, p0

    iget-object v1, v0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v9

    .line 855
    invoke-virtual/range {p1 .. p1}, Lr/h;->c()I

    move-result v2

    .line 856
    const/4 v3, 0x0

    .line 857
    const/4 v4, 0x0

    .line 858
    const/4 v5, 0x0

    .line 859
    const/4 v7, 0x0

    .line 860
    const/4 v6, 0x0

    .line 862
    const/4 v1, 0x0

    :goto_59
    invoke-virtual/range {p1 .. p1}, Lr/h;->c()I

    move-result v8

    if-ge v1, v8, :cond_13f

    .line 863
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v10

    .line 864
    invoke-virtual {v10}, Lr/k;->c()Lo/aq;

    move-result-object v8

    .line 865
    invoke-virtual/range {p0 .. p0}, Lr/d;->a()LA/c;

    move-result-object v11

    invoke-virtual {v8, v11}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v11

    .line 866
    invoke-virtual {v10}, Lr/k;->g()I

    move-result v8

    const/4 v12, -0x1

    if-eq v8, v12, :cond_7a

    .line 867
    add-int/lit8 v7, v7, 0x1

    .line 869
    :cond_7a
    move-object/from16 v0, p0

    iget-object v8, v0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v8, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->d:I

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->d:I

    .line 872
    :try_start_8b
    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v8

    if-eqz v8, :cond_e5

    .line 873
    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->g:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->g:I

    .line 882
    :goto_9b
    const/4 v8, 0x0

    .line 883
    if-eqz v9, :cond_af

    .line 884
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->c(I)[B

    move-result-object v12

    .line 885
    if-eqz v12, :cond_af

    .line 886
    array-length v8, v12

    new-array v8, v8, [B

    .line 887
    const/4 v13, 0x0

    const/4 v14, 0x0

    array-length v15, v12

    invoke-static {v12, v13, v8, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 891
    :cond_af
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lr/h;->b(I)Lo/ap;

    move-result-object v12

    .line 892
    if-eqz v12, :cond_11a

    .line 893
    move-object/from16 v0, p0

    iget-object v13, v0, Lr/d;->b:Lr/B;

    iget-object v13, v13, Lr/B;->a:Lt/M;

    if-eqz v13, :cond_ce

    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v13

    if-nez v13, :cond_ce

    .line 894
    move-object/from16 v0, p0

    iget-object v13, v0, Lr/d;->b:Lr/B;

    iget-object v13, v13, Lr/B;->a:Lt/M;

    invoke-interface {v13, v11, v12}, Lt/M;->a(Lo/aq;Lo/ap;)V

    .line 896
    :cond_ce
    if-eqz v9, :cond_d3

    .line 897
    invoke-interface {v9, v11, v12, v8}, Lt/f;->a(Lo/aq;Lo/ap;[B)V

    .line 899
    :cond_d3
    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8, v12}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 900
    invoke-virtual {v10}, Lr/k;->e()Z

    move-result v8

    if-eqz v8, :cond_117

    .line 901
    add-int/lit8 v4, v4, 0x1

    .line 862
    :goto_e1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_59

    .line 875
    :cond_e5
    move-object/from16 v0, p0

    iget v8, v0, Lr/d;->f:I

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lr/d;->f:I
    :try_end_ef
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_ef} :catch_f0

    goto :goto_9b

    .line 933
    :catch_f0
    move-exception v8

    .line 934
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": Could not parse tile: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v8}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 935
    const/4 v8, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8, v11}, Lr/d;->a(Lr/k;ILo/ap;)V

    goto :goto_e1

    .line 903
    :cond_117
    add-int/lit8 v3, v3, 0x1

    goto :goto_e1

    .line 909
    :cond_11a
    :try_start_11a
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/K;->u()Z

    move-result v8

    if-eqz v8, :cond_137

    .line 910
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->b(Lr/k;Lo/aq;)Z

    move-result v8

    if-eqz v8, :cond_12f

    .line 911
    add-int/lit8 v5, v5, 0x1

    goto :goto_e1

    .line 925
    :cond_12f
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->a(Lr/k;Lo/aq;)V

    .line 926
    add-int/lit8 v6, v6, 0x1

    goto :goto_e1

    .line 929
    :cond_137
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lr/d;->a(Lr/k;Lo/aq;)V
    :try_end_13c
    .catch Ljava/io/IOException; {:try_start_11a .. :try_end_13c} :catch_f0

    .line 930
    add-int/lit8 v6, v6, 0x1

    goto :goto_e1

    .line 941
    :cond_13f
    invoke-direct/range {p0 .. p0}, Lr/d;->t()V

    .line 943
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->u()Z

    move-result v1

    if-eqz v1, :cond_46

    invoke-direct/range {p0 .. p0}, Lr/d;->u()Z

    move-result v1

    if-eqz v1, :cond_46

    move-object/from16 v1, p0

    .line 944
    invoke-direct/range {v1 .. v7}, Lr/d;->a(IIIIII)V

    goto/16 :goto_46
.end method

.method private a(Lr/k;)V
    .registers 4
    .parameter

    .prologue
    .line 302
    iget-object v0, p0, Lr/d;->i:Lr/i;

    if-eqz v0, :cond_9

    .line 303
    iget-object v0, p0, Lr/d;->i:Lr/i;

    invoke-virtual {v0}, Lr/i;->a()V

    .line 305
    :cond_9
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 307
    return-void
.end method

.method private a(Lr/k;Lo/aq;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 955
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    if-eqz v0, :cond_d

    .line 956
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, p2}, Lt/M;->a_(Lo/aq;)V

    .line 958
    :cond_d
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 959
    return-void
.end method

.method static synthetic a(Lr/d;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput-boolean p1, p0, Lr/d;->v:Z

    return p1
.end method

.method private b(Lo/aq;)V
    .registers 6
    .parameter

    .prologue
    .line 438
    iget-object v2, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v2

    .line 439
    const/4 v1, 0x0

    :goto_4
    :try_start_4
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2e

    .line 440
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/A;

    .line 441
    if-eqz v0, :cond_23

    .line 442
    invoke-interface {v0, p0, p1}, Lr/A;->a(Lr/z;Lo/aq;)V

    move v0, v1

    .line 439
    :goto_20
    add-int/lit8 v1, v0, 0x1

    goto :goto_4

    .line 444
    :cond_23
    iget-object v3, p0, Lr/d;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_20

    .line 447
    :catchall_2b
    move-exception v0

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_4 .. :try_end_2d} :catchall_2b

    throw v0

    :cond_2e
    :try_start_2e
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2b

    .line 448
    return-void
.end method

.method private b(Lo/ar;Lr/c;Ls/e;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 639
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lr/u;->a(Lr/c;Z)I

    move-result v0

    .line 640
    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    .line 642
    :goto_c
    invoke-interface {p1}, Lo/ar;->c()Lo/aq;

    move-result-object v2

    if-eqz v2, :cond_1d

    .line 643
    if-eqz v1, :cond_18

    .line 644
    invoke-interface {v1, v2, p3, v0}, Lt/f;->a(Lo/aq;Ls/e;I)V

    goto :goto_c

    .line 648
    :cond_18
    const/4 v3, 0x0

    invoke-interface {p3, v2, v4, v3}, Ls/e;->a(Lo/aq;ILo/ap;)V

    goto :goto_c

    .line 653
    :cond_1d
    iput-boolean v4, p0, Lr/d;->v:Z

    .line 654
    invoke-direct {p0}, Lr/d;->t()V

    .line 655
    return-void
.end method

.method static synthetic b(Lr/d;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lr/d;->q()V

    return-void
.end method

.method private b(Lr/k;)V
    .registers 11
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 677
    invoke-direct {p0}, Lr/d;->s()V

    .line 678
    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v0

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/aq;->a(LA/c;)Lo/aq;

    move-result-object v3

    .line 680
    sget-object v0, Lr/z;->h:Lo/aq;

    invoke-virtual {v0, v3}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 684
    invoke-virtual {p0, p1, v6, v7}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 781
    :cond_1e
    :goto_1e
    return-void

    .line 688
    :cond_1f
    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/k;

    .line 698
    invoke-virtual {p1}, Lr/k;->h()Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 700
    invoke-virtual {p1}, Lr/k;->f()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 703
    :cond_33
    invoke-direct {p0, p1, v5}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v4

    .line 705
    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 707
    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lr/k;

    move v8, v2

    move-object v2, v1

    move v1, v8

    .line 733
    :goto_46
    if-nez v1, :cond_4b

    .line 735
    invoke-virtual {p0, p1, v6, v7}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 738
    :cond_4b
    if-eqz v2, :cond_1e

    .line 744
    invoke-static {v2}, Lr/d;->c(Lr/k;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 747
    if-eqz v0, :cond_90

    .line 748
    invoke-virtual {v2}, Lr/k;->j()Z

    move-result v1

    if-nez v1, :cond_1e

    .line 751
    invoke-virtual {v0, v2}, Lr/k;->a(Lr/k;)V

    goto :goto_1e

    .line 709
    :cond_5f
    invoke-virtual {p1}, Lr/k;->f()Z

    move-result v2

    if-eqz v2, :cond_67

    move-object v2, p1

    .line 712
    goto :goto_46

    .line 713
    :cond_67
    if-eqz v0, :cond_77

    invoke-virtual {v0}, Lr/k;->k()Z

    move-result v2

    if-nez v2, :cond_75

    invoke-virtual {v0}, Lr/k;->j()Z

    move-result v2

    if-nez v2, :cond_77

    :cond_75
    move-object v2, p1

    .line 718
    goto :goto_46

    .line 720
    :cond_77
    invoke-direct {p0, p1, v5}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v4

    .line 723
    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 724
    if-eqz v2, :cond_8d

    .line 725
    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lr/k;

    move v8, v2

    move-object v2, v1

    move v1, v8

    goto :goto_46

    :cond_8d
    move v1, v2

    move-object v2, p1

    .line 728
    goto :goto_46

    .line 753
    :cond_90
    iget-boolean v0, p0, Lr/d;->s:Z

    if-eqz v0, :cond_b8

    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b8

    invoke-virtual {v2}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_b8

    .line 760
    iget-object v0, p0, Lr/d;->p:LR/h;

    invoke-virtual {v0, v3}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/k;

    .line 761
    if-eqz v0, :cond_b1

    .line 762
    invoke-virtual {v0, v2}, Lr/k;->a(Lr/k;)V

    goto/16 :goto_1e

    .line 764
    :cond_b1
    iget-object v0, p0, Lr/d;->p:LR/h;

    invoke-virtual {v0, v3, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1e

    .line 767
    :cond_b8
    iget-object v0, p0, Lr/d;->q:Ljava/util/Map;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 768
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0, v2}, Lr/h;->a(Lr/k;)Z

    move-result v0

    if-nez v0, :cond_c8

    .line 769
    invoke-direct {p0}, Lr/d;->p()V

    .line 771
    :cond_c8
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v1

    invoke-static {v1, v3}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lr/h;->a(Landroid/util/Pair;Lr/k;)V

    .line 772
    iget v0, p0, Lr/d;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lr/d;->d:I

    .line 773
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0}, Lr/h;->d()Z

    move-result v0

    if-nez v0, :cond_e9

    invoke-virtual {v2}, Lr/k;->f()Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 774
    :cond_e9
    invoke-direct {p0}, Lr/d;->p()V

    goto/16 :goto_1e

    .line 775
    :cond_ee
    iget-boolean v0, p0, Lr/d;->n:Z

    if-nez v0, :cond_1e

    .line 776
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 777
    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 778
    iput-boolean v5, p0, Lr/d;->n:Z

    goto/16 :goto_1e
.end method

.method private b(Lr/k;Lo/aq;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 966
    invoke-direct {p0, p2}, Lr/d;->c(Lo/aq;)Lo/ap;

    move-result-object v1

    .line 967
    if-eqz v1, :cond_3b

    invoke-interface {v1}, Lo/ap;->h()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3b

    .line 968
    iget-object v2, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v2}, Lo/ap;->c(Lcom/google/googlenav/common/a;)V

    .line 969
    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    if-eqz v2, :cond_26

    invoke-virtual {p1}, Lr/k;->e()Z

    move-result v2

    if-nez v2, :cond_26

    .line 970
    iget-object v2, p0, Lr/d;->b:Lr/B;

    iget-object v2, v2, Lr/B;->a:Lt/M;

    invoke-interface {v2, p2, v1}, Lt/M;->a(Lo/aq;Lo/ap;)V

    .line 972
    :cond_26
    iget-object v2, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v2}, Lr/B;->b()Lt/f;

    move-result-object v2

    .line 973
    if-eqz v2, :cond_37

    .line 979
    invoke-interface {v2, p2}, Lt/f;->a(Lo/aq;)[B

    move-result-object v3

    .line 980
    if-eqz v3, :cond_37

    .line 981
    invoke-interface {v2, p2, v1, v3}, Lt/f;->a(Lo/aq;Lo/ap;[B)V

    .line 984
    :cond_37
    invoke-virtual {p0, p1, v0, v1}, Lr/d;->a(Lr/k;ILo/ap;)V

    .line 985
    const/4 v0, 0x1

    .line 987
    :cond_3b
    return v0
.end method

.method private c(Lo/aq;)Lo/ap;
    .registers 5
    .parameter

    .prologue
    .line 995
    const/4 v0, 0x0

    .line 996
    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    if-eqz v1, :cond_1a

    .line 997
    iget-object v1, p0, Lr/d;->b:Lr/B;

    iget-object v1, v1, Lr/B;->a:Lt/M;

    invoke-interface {v1, p1}, Lt/M;->b(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 998
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->a:Lt/M;

    invoke-interface {v0, p1}, Lt/M;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    .line 1008
    :cond_19
    :goto_19
    return-object v0

    .line 1002
    :cond_1a
    iget-object v1, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v1}, Lr/B;->b()Lt/f;

    move-result-object v1

    .line 1003
    if-eqz v1, :cond_19

    .line 1004
    invoke-interface {v1, p1}, Lt/f;->b(Lo/aq;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1005
    invoke-interface {v1, p1}, Lt/f;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    goto :goto_19
.end method

.method static synthetic c(Lr/d;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lr/d;->r()V

    return-void
.end method

.method private static c(Lr/k;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 785
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    .line 787
    :goto_8
    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_10
    return v1

    :cond_11
    move v0, v1

    .line 785
    goto :goto_8
.end method

.method static synthetic d(Lr/d;)I
    .registers 2
    .parameter

    .prologue
    .line 64
    iget v0, p0, Lr/d;->r:I

    return v0
.end method

.method static synthetic e(Lr/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-boolean v0, p0, Lr/d;->v:Z

    return v0
.end method

.method static synthetic f(Lr/d;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lr/d;->t()V

    return-void
.end method

.method private n()V
    .registers 5

    .prologue
    .line 425
    iget-object v2, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v2

    .line 426
    const/4 v1, 0x0

    :goto_4
    :try_start_4
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2e

    .line 427
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/A;

    .line 428
    if-eqz v0, :cond_23

    .line 429
    invoke-interface {v0, p0}, Lr/A;->a(Lr/z;)V

    move v0, v1

    .line 426
    :goto_20
    add-int/lit8 v1, v0, 0x1

    goto :goto_4

    .line 431
    :cond_23
    iget-object v3, p0, Lr/d;->u:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_20

    .line 434
    :catchall_2b
    move-exception v0

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_4 .. :try_end_2d} :catchall_2b

    throw v0

    :cond_2e
    :try_start_2e
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2b

    .line 435
    return-void
.end method

.method private o()V
    .registers 2

    .prologue
    .line 798
    invoke-direct {p0}, Lr/d;->s()V

    .line 799
    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/d;->n:Z

    .line 800
    invoke-direct {p0}, Lr/d;->p()V

    .line 801
    return-void
.end method

.method private p()V
    .registers 4

    .prologue
    .line 808
    invoke-direct {p0}, Lr/d;->s()V

    .line 809
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-virtual {v0}, Lr/h;->c()I

    move-result v0

    if-lez v0, :cond_30

    .line 810
    new-instance v0, Ll/g;

    const-string v1, "addRequest"

    iget-object v2, p0, Lr/d;->c:Lr/h;

    invoke-direct {v0, v1, v2}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 813
    iget-object v0, p0, Lr/d;->k:Law/p;

    iget-object v1, p0, Lr/d;->c:Lr/h;

    invoke-interface {v0, v1}, Law/p;->c(Law/g;)V

    .line 814
    iget-object v0, p0, Lr/d;->o:Ljava/util/List;

    iget-object v1, p0, Lr/d;->c:Lr/h;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 815
    invoke-virtual {p0}, Lr/d;->m()Lr/h;

    move-result-object v0

    iput-object v0, p0, Lr/d;->c:Lr/h;

    .line 816
    iget-object v0, p0, Lr/d;->c:Lr/h;

    invoke-static {v0, p0}, Lr/h;->a(Lr/h;Lr/d;)Lr/d;

    .line 818
    :cond_30
    return-void
.end method

.method private q()V
    .registers 2

    .prologue
    .line 1088
    invoke-direct {p0}, Lr/d;->s()V

    .line 1090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/d;->s:Z

    .line 1091
    return-void
.end method

.method private r()V
    .registers 6

    .prologue
    .line 1097
    invoke-direct {p0}, Lr/d;->s()V

    .line 1101
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lr/d;->o:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1103
    iget-object v1, p0, Lr/d;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1105
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/h;

    .line 1106
    const/4 v1, 0x0

    :goto_20
    invoke-virtual {v0}, Lr/h;->c()I

    move-result v3

    if-ge v1, v3, :cond_13

    .line 1107
    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v3

    .line 1108
    iget-object v4, p0, Lr/d;->q:Ljava/util/Map;

    invoke-virtual {v3}, Lr/k;->c()Lo/aq;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    iget v3, p0, Lr/d;->d:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lr/d;->d:I

    .line 1110
    invoke-virtual {v0, v1}, Lr/h;->a(I)Lr/k;

    move-result-object v3

    invoke-direct {p0, v3}, Lr/d;->b(Lr/k;)V

    .line 1106
    add-int/lit8 v1, v1, 0x1

    goto :goto_20

    .line 1113
    :cond_43
    return-void
.end method

.method private final s()V
    .registers 3

    .prologue
    .line 1264
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_14

    .line 1265
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on DashServerTileStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1267
    :cond_14
    return-void
.end method

.method private t()V
    .registers 3

    .prologue
    .line 1271
    :try_start_0
    iget-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1276
    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->b:Lt/f;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lr/d;->b:Lr/B;

    iget-object v0, v0, Lr/B;->b:Lt/f;

    invoke-interface {v0}, Lt/f;->b()Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Lr/d;->i:Lr/i;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lr/d;->i:Lr/i;

    invoke-virtual {v0}, Lr/i;->b()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 1278
    :cond_21
    new-instance v0, Lr/i;

    invoke-direct {v0, p0}, Lr/i;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->i:Lr/i;
    :try_end_28
    .catchall {:try_start_0 .. :try_end_28} :catchall_2e

    .line 1281
    :cond_28
    iget-object v0, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1283
    return-void

    .line 1281
    :catchall_2e
    move-exception v0

    iget-object v1, p0, Lr/d;->j:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private u()Z
    .registers 3

    .prologue
    .line 1660
    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->b:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    invoke-virtual {p0}, Lr/d;->a()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->o:LA/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method


# virtual methods
.method public a(Lo/aq;Z)Lo/ap;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 285
    new-instance v1, Lr/j;

    invoke-direct {v1}, Lr/j;-><init>()V

    .line 286
    new-instance v0, Lr/k;

    invoke-direct {v0, p1, v1}, Lr/k;-><init>(Lo/aq;Ls/e;)V

    .line 287
    invoke-direct {p0, v0, p2}, Lr/d;->a(Lr/k;Z)Landroid/util/Pair;

    move-result-object v0

    .line 290
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lr/k;

    .line 291
    if-eqz v0, :cond_20

    .line 293
    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v3, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 295
    :cond_20
    invoke-static {v1}, Lr/j;->a(Lr/j;)Lo/ap;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 493
    .line 495
    new-instance v0, Lr/k;

    const/4 v6, -0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    .line 497
    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 498
    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 499
    return-object v0
.end method

.method public a(IZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 539
    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Network Error! "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_4a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 542
    return-void

    .line 539
    :cond_4a
    const-string v0, ""

    goto :goto_32
.end method

.method public a(Law/g;)V
    .registers 5
    .parameter

    .prologue
    .line 525
    instance-of v0, p1, Lr/h;

    if-eqz v0, :cond_19

    move-object v0, p1

    check-cast v0, Lr/h;

    invoke-static {v0}, Lr/h;->a(Lr/h;)Lr/d;

    move-result-object v0

    if-ne v0, p0, :cond_19

    .line 530
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 532
    :cond_19
    return-void
.end method

.method public a(Lcom/google/googlenav/bE;)V
    .registers 2
    .parameter

    .prologue
    .line 1652
    iput-object p1, p0, Lr/d;->t:Lcom/google/googlenav/bE;

    .line 1653
    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public a(Ljava/util/Locale;)V
    .registers 6
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, Lr/d;->a:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 403
    :goto_8
    return-void

    .line 389
    :cond_9
    :try_start_9
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 390
    monitor-enter v1
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_f} :catch_2d

    .line 391
    :try_start_f
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    iget-object v2, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 393
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 394
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_f .. :try_end_1f} :catchall_2a

    .line 399
    :goto_1f
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(Ljava/util/Locale;)V

    .line 401
    iput-object p1, p0, Lr/d;->a:Ljava/util/Locale;

    .line 402
    invoke-direct {p0}, Lr/d;->n()V

    goto :goto_8

    .line 394
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    :try_start_2c
    throw v0
    :try_end_2d
    .catch Ljava/lang/InterruptedException; {:try_start_2c .. :try_end_2d} :catch_2d

    .line 395
    :catch_2d
    move-exception v0

    .line 396
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1f
.end method

.method public a(Lo/aq;Ls/e;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 257
    new-instance v0, Lr/k;

    invoke-direct {v0, p1, p2}, Lr/k;-><init>(Lo/aq;Ls/e;)V

    .line 258
    invoke-direct {p0, v0}, Lr/d;->a(Lr/k;)V

    .line 259
    return-void
.end method

.method public a(Lo/ar;Lr/c;Ls/e;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 632
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 634
    iget-object v1, p0, Lr/d;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 635
    return-void
.end method

.method public a(Lr/A;)V
    .registers 5
    .parameter

    .prologue
    .line 407
    iget-object v1, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v1

    .line 408
    :try_start_3
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    monitor-exit v1

    .line 410
    return-void

    .line 409
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method a(Lr/k;ILo/ap;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1292
    const/4 v0, 0x0

    move-object v2, p1

    .line 1293
    :goto_3
    if-eqz v2, :cond_43

    .line 1297
    if-nez p2, :cond_3f

    invoke-virtual {v2}, Lr/k;->b()Z

    move-result v3

    if-nez v3, :cond_3f

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v3

    invoke-static {v3}, Lr/u;->a(Lr/c;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 1302
    invoke-interface {p3}, Lo/ap;->i()Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 1305
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->b()Lt/f;

    move-result-object v0

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v3

    iget-object v4, v2, Lr/k;->b:Ls/e;

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v5

    invoke-static {v5, v1}, Lr/u;->a(Lr/c;Z)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lt/f;->a(Lo/aq;Ls/e;I)V

    move v0, v1

    .line 1293
    :goto_35
    invoke-static {v2}, Lr/k;->b(Lr/k;)Lr/k;

    move-result-object v2

    goto :goto_3

    .line 1311
    :cond_3a
    const/4 v3, 0x4

    invoke-static {v2, v3, p3}, Lr/k;->a(Lr/k;ILo/ap;)V

    goto :goto_35

    .line 1314
    :cond_3f
    invoke-static {v2, p2, p3}, Lr/k;->a(Lr/k;ILo/ap;)V

    goto :goto_35

    .line 1317
    :cond_43
    if-eqz v0, :cond_4a

    .line 1320
    iput-boolean v1, p0, Lr/d;->v:Z

    .line 1321
    invoke-direct {p0}, Lr/d;->t()V

    .line 1323
    :cond_4a
    return-void
.end method

.method public a(Lo/aq;)Z
    .registers 3
    .parameter

    .prologue
    .line 371
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0, p1}, Lr/B;->a(Lo/aq;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 326
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->c()V

    .line 327
    invoke-direct {p0}, Lr/d;->n()V

    .line 328
    return-void
.end method

.method public b(Law/g;)V
    .registers 2
    .parameter

    .prologue
    .line 556
    return-void
.end method

.method public b(Lo/aq;Ls/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    const/4 v1, 0x1

    .line 265
    new-instance v2, Lr/k;

    invoke-direct {v2, p1, p2, v0, v1}, Lr/k;-><init>(Lo/aq;Ls/e;ZZ)V

    .line 267
    invoke-direct {p0, v2}, Lr/d;->a(Lr/k;)V

    .line 268
    return-void
.end method

.method public b(Lr/A;)V
    .registers 5
    .parameter

    .prologue
    .line 414
    iget-object v1, p0, Lr/d;->u:Ljava/util/ArrayList;

    monitor-enter v1

    .line 415
    :try_start_3
    iget-object v0, p0, Lr/d;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 416
    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 417
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_9

    .line 418
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_9

    .line 421
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0

    :cond_22
    :try_start_22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_1f

    .line 422
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->d()I

    move-result v0

    return v0
.end method

.method public c(Lo/aq;Ls/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 272
    const/4 v0, 0x1

    .line 273
    new-instance v1, Lr/k;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v0, v2}, Lr/k;-><init>(Lo/aq;Ls/e;ZZ)V

    .line 275
    invoke-direct {p0, v1}, Lr/d;->a(Lr/k;)V

    .line 276
    return-void
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 344
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->g()Z

    move-result v0

    return v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 361
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .registers 3

    .prologue
    .line 366
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .registers 2

    .prologue
    .line 452
    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0, p0}, Law/p;->a(Law/q;)V

    .line 453
    invoke-virtual {p0}, Lr/d;->start()V

    .line 456
    :try_start_8
    monitor-enter p0
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_9} :catch_14

    .line 457
    :goto_9
    :try_start_9
    iget-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    if-nez v0, :cond_1d

    .line 458
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_9

    .line 460
    :catchall_11
    move-exception v0

    monitor-exit p0
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_11

    :try_start_13
    throw v0
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_14} :catch_14

    .line 461
    :catch_14
    move-exception v0

    .line 462
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 464
    :goto_1c
    return-void

    .line 460
    :cond_1d
    :try_start_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_11

    goto :goto_1c
.end method

.method public h()V
    .registers 2

    .prologue
    .line 468
    iget-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    if-eqz v0, :cond_c

    .line 469
    iget-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 470
    const/4 v0, 0x0

    iput-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    .line 473
    :cond_c
    :try_start_c
    invoke-virtual {p0}, Lr/d;->join()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_f} :catch_1a

    .line 477
    :goto_f
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->h()V

    .line 478
    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0, p0}, Law/p;->b(Law/q;)V

    .line 479
    return-void

    .line 474
    :catch_1a
    move-exception v0

    .line 475
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_f
.end method

.method public i()V
    .registers 2

    .prologue
    .line 483
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->i()V

    .line 484
    return-void
.end method

.method public j()J
    .registers 5

    .prologue
    .line 504
    iget-object v0, p0, Lr/d;->k:Law/p;

    invoke-interface {v0}, Law/p;->p()J

    move-result-wide v0

    .line 505
    const-wide/high16 v2, -0x8000

    cmp-long v2, v0, v2

    if-eqz v2, :cond_21

    iget-object v2, p0, Lr/d;->k:Law/p;

    invoke-interface {v2}, Law/p;->n()Z

    move-result v2

    if-eqz v2, :cond_21

    iget v2, p0, Lr/d;->d:I

    if-nez v2, :cond_21

    .line 508
    iget-object v2, p0, Lr/d;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 510
    :goto_20
    return-wide v0

    :cond_21
    const-wide/16 v0, 0x0

    goto :goto_20
.end method

.method public k()V
    .registers 1

    .prologue
    .line 547
    return-void
.end method

.method public l()V
    .registers 5

    .prologue
    .line 570
    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_7} :catch_25

    .line 576
    :goto_7
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 577
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lr/d;->m:Landroid/os/Looper;

    .line 578
    new-instance v0, Lr/g;

    invoke-direct {v0, p0}, Lr/g;-><init>(Lr/d;)V

    iput-object v0, p0, Lr/d;->l:Landroid/os/Handler;

    .line 618
    monitor-enter p0

    .line 619
    :try_start_18
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 620
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_18 .. :try_end_1c} :catchall_41

    .line 622
    iget-object v0, p0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->a()V

    .line 624
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 625
    return-void

    .line 571
    :catch_25
    move-exception v0

    .line 572
    invoke-virtual {p0}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 620
    :catchall_41
    move-exception v0

    :try_start_42
    monitor-exit p0
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_41

    throw v0
.end method

.method protected abstract m()Lr/h;
.end method
