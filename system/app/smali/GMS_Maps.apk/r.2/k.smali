.class public Lr/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lr/v;


# instance fields
.field final a:Lo/aq;

.field final b:Ls/e;

.field final c:Z

.field final d:Lr/c;

.field final e:Z

.field final f:Z

.field final g:Z

.field h:I

.field volatile i:Z

.field private j:Lr/k;


# direct methods
.method protected constructor <init>(Lo/aq;Ls/e;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1376
    sget-object v3, Lr/c;->b:Lr/c;

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    .line 1379
    return-void
.end method

.method protected constructor <init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341
    iput-boolean v0, p0, Lr/k;->i:Z

    .line 1347
    const/4 v1, 0x0

    iput-object v1, p0, Lr/k;->j:Lr/k;

    .line 1364
    iput-object p1, p0, Lr/k;->a:Lo/aq;

    .line 1365
    iput-object p2, p0, Lr/k;->b:Ls/e;

    .line 1366
    iput-object p3, p0, Lr/k;->d:Lr/c;

    .line 1367
    sget-object v1, Lr/c;->e:Lr/c;

    invoke-virtual {p3, v1}, Lr/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1f

    sget-object v1, Lr/c;->d:Lr/c;

    invoke-virtual {p3, v1}, Lr/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    :cond_1f
    const/4 v0, 0x1

    :cond_20
    iput-boolean v0, p0, Lr/k;->c:Z

    .line 1369
    iput-boolean p4, p0, Lr/k;->e:Z

    .line 1370
    iput p6, p0, Lr/k;->h:I

    .line 1371
    iput-boolean p5, p0, Lr/k;->f:Z

    .line 1372
    iput-boolean p7, p0, Lr/k;->g:Z

    .line 1373
    return-void
.end method

.method protected constructor <init>(Lo/aq;Ls/e;ZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1390
    sget-object v3, Lr/c;->b:Lr/c;

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p3

    invoke-direct/range {v0 .. v7}, Lr/k;-><init>(Lo/aq;Ls/e;Lr/c;ZZIZ)V

    .line 1393
    return-void
.end method

.method private a(ILo/ap;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1478
    iget-object v0, p0, Lr/k;->b:Ls/e;

    iget-object v1, p0, Lr/k;->a:Lo/aq;

    invoke-interface {v0, v1, p1, p2}, Ls/e;->a(Lo/aq;ILo/ap;)V

    .line 1479
    return-void
.end method

.method static synthetic a(Lr/k;ILo/ap;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1330
    invoke-direct {p0, p1, p2}, Lr/k;->a(ILo/ap;)V

    return-void
.end method

.method static synthetic b(Lr/k;)Lr/k;
    .registers 2
    .parameter

    .prologue
    .line 1330
    iget-object v0, p0, Lr/k;->j:Lr/k;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 1397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/k;->i:Z

    .line 1398
    return-void
.end method

.method protected a(I)V
    .registers 2
    .parameter

    .prologue
    .line 1467
    iput p1, p0, Lr/k;->h:I

    .line 1468
    return-void
.end method

.method a(Lr/k;)V
    .registers 3
    .parameter

    .prologue
    .line 1473
    iget-object v0, p0, Lr/k;->j:Lr/k;

    iput-object v0, p1, Lr/k;->j:Lr/k;

    .line 1474
    iput-object p1, p0, Lr/k;->j:Lr/k;

    .line 1475
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 1401
    iget-boolean v0, p0, Lr/k;->i:Z

    return v0
.end method

.method public c()Lo/aq;
    .registers 2

    .prologue
    .line 1406
    iget-object v0, p0, Lr/k;->a:Lo/aq;

    return-object v0
.end method

.method protected d()Lr/c;
    .registers 2

    .prologue
    .line 1410
    iget-object v0, p0, Lr/k;->d:Lr/c;

    return-object v0
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 1414
    iget-boolean v0, p0, Lr/k;->c:Z

    return v0
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 1418
    iget-boolean v0, p0, Lr/k;->e:Z

    return v0
.end method

.method protected g()I
    .registers 2

    .prologue
    .line 1422
    iget v0, p0, Lr/k;->h:I

    return v0
.end method

.method protected h()Z
    .registers 2

    .prologue
    .line 1426
    iget-boolean v0, p0, Lr/k;->f:Z

    return v0
.end method

.method protected i()Z
    .registers 2

    .prologue
    .line 1450
    :goto_0
    if-eqz p0, :cond_d

    .line 1451
    invoke-virtual {p0}, Lr/k;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1452
    const/4 v0, 0x0

    .line 1455
    :goto_9
    return v0

    .line 1450
    :cond_a
    iget-object p0, p0, Lr/k;->j:Lr/k;

    goto :goto_0

    .line 1455
    :cond_d
    const/4 v0, 0x1

    goto :goto_9
.end method

.method protected j()Z
    .registers 2

    .prologue
    .line 1459
    iget-boolean v0, p0, Lr/k;->g:Z

    return v0
.end method

.method protected k()Z
    .registers 2

    .prologue
    .line 1463
    iget-object v0, p0, Lr/k;->j:Lr/k;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method
