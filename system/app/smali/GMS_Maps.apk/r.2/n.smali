.class public Lr/n;
.super LR/c;
.source "SourceFile"

# interfaces
.implements Law/q;


# static fields
.field private static a:Lr/n;


# instance fields
.field private final b:Lcom/google/googlenav/common/a;

.field private final c:Law/h;

.field private final d:Lt/u;

.field private final e:Ljava/io/File;

.field private f:Z

.field private g:Landroid/os/Handler;

.field private final h:Ljava/util/Map;

.field private i:Z


# direct methods
.method protected constructor <init>(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 141
    const-string v0, "ibs"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 143
    iput-object p1, p0, Lr/n;->c:Law/h;

    .line 144
    iput-object p4, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    .line 145
    new-instance v0, Lt/u;

    iget-object v1, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p3, v1}, Lt/u;-><init>(Ljava/util/Locale;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Lr/n;->d:Lt/u;

    .line 146
    iput-object p2, p0, Lr/n;->e:Ljava/io/File;

    .line 147
    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lr/n;->h:Ljava/util/Map;

    .line 148
    return-void
.end method

.method public static a()Lr/n;
    .registers 1

    .prologue
    .line 178
    sget-object v0, Lr/n;->a:Lr/n;

    return-object v0
.end method

.method public static a(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)Lr/n;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    sget-object v0, Lr/n;->a:Lr/n;

    if-nez v0, :cond_b

    .line 169
    new-instance v0, Lr/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lr/n;-><init>(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)V

    sput-object v0, Lr/n;->a:Lr/n;

    .line 171
    :cond_b
    sget-object v0, Lr/n;->a:Lr/n;

    return-object v0
.end method

.method static synthetic a(Lr/n;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lr/n;->h()V

    return-void
.end method

.method static synthetic a(Lr/n;Lr/p;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lr/n;->a(Lr/p;)V

    return-void
.end method

.method static synthetic a(Lr/n;Ls/d;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lr/n;->a(Ls/d;)V

    return-void
.end method

.method private a(Lr/p;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 346
    invoke-direct {p0}, Lr/n;->j()V

    .line 348
    iget-object v1, p1, Lr/p;->a:Lo/r;

    .line 349
    iget-object v2, p1, Lr/p;->b:Ls/c;

    .line 352
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, v1}, Lt/u;->a(Lo/r;)Lo/y;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_2d

    .line 355
    if-eqz v2, :cond_1f

    .line 356
    iget-object v3, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v3, v0}, Lt/u;->a(Lo/y;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 357
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v1, v3, v4}, Ls/c;->a(Lo/r;ILo/y;)V

    .line 366
    :cond_1f
    :goto_1f
    iget-object v3, p0, Lr/n;->b:Lcom/google/googlenav/common/a;

    invoke-virtual {v0, v3}, Lo/y;->a(Lcom/google/googlenav/common/a;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 394
    :cond_27
    :goto_27
    return-void

    .line 360
    :cond_28
    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, v0}, Ls/c;->a(Lo/r;ILo/y;)V

    goto :goto_1f

    .line 378
    :cond_2d
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/d;

    .line 380
    if-nez v0, :cond_41

    .line 381
    new-instance v0, Ls/d;

    invoke-direct {v0, v1}, Ls/d;-><init>(Lo/r;)V

    .line 382
    iget-object v3, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    :cond_41
    if-eqz v2, :cond_46

    .line 386
    invoke-virtual {v0, v2}, Ls/d;->a(Ls/c;)V

    .line 389
    :cond_46
    invoke-virtual {v0}, Ls/d;->d()Z

    move-result v0

    if-nez v0, :cond_27

    iget-boolean v0, p0, Lr/n;->i:Z

    if-nez v0, :cond_27

    .line 391
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v5, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 392
    iput-boolean v5, p0, Lr/n;->i:Z

    goto :goto_27
.end method

.method private a(Ls/d;)V
    .registers 5
    .parameter

    .prologue
    .line 423
    invoke-direct {p0}, Lr/n;->j()V

    .line 424
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_f

    .line 430
    :cond_f
    const/4 v0, 0x0

    .line 431
    invoke-virtual {p1}, Ls/d;->e_()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 432
    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lt/u;->c(Lo/r;)V

    .line 440
    :cond_1f
    :goto_1f
    invoke-virtual {p1, v0}, Ls/d;->a(Lo/y;)V

    .line 441
    return-void

    .line 434
    :cond_23
    invoke-virtual {p1}, Ls/d;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 435
    if-eqz v1, :cond_1f

    .line 436
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lt/u;->a(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/y;

    move-result-object v0

    goto :goto_1f
.end method

.method public static b()V
    .registers 1

    .prologue
    .line 185
    sget-object v0, Lr/n;->a:Lr/n;

    if-eqz v0, :cond_c

    .line 186
    sget-object v0, Lr/n;->a:Lr/n;

    invoke-virtual {v0}, Lr/n;->d()V

    .line 187
    const/4 v0, 0x0

    sput-object v0, Lr/n;->a:Lr/n;

    .line 189
    :cond_c
    return-void
.end method

.method static synthetic b(Lr/n;Ls/d;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lr/n;->b(Ls/d;)V

    return-void
.end method

.method private b(Ls/d;)V
    .registers 4
    .parameter

    .prologue
    .line 449
    invoke-direct {p0}, Lr/n;->j()V

    .line 450
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-virtual {p1}, Ls/d;->a()Lo/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_f

    .line 454
    :cond_f
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ls/d;->a(Lo/y;)V

    .line 455
    return-void
.end method

.method private h()V
    .registers 4

    .prologue
    .line 400
    invoke-direct {p0}, Lr/n;->j()V

    .line 401
    const/4 v0, 0x0

    iput-boolean v0, p0, Lr/n;->i:Z

    .line 404
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    .line 406
    :try_start_b
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_15
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/d;

    .line 407
    invoke-virtual {v0}, Ls/d;->d()Z

    move-result v2

    if-nez v2, :cond_15

    .line 408
    iget-object v2, p0, Lr/n;->c:Law/h;

    invoke-virtual {v2, v0}, Law/h;->c(Law/g;)V

    .line 409
    invoke-virtual {v0}, Ls/d;->c()V
    :try_end_2f
    .catchall {:try_start_b .. :try_end_2f} :catchall_30

    goto :goto_15

    .line 413
    :catchall_30
    move-exception v0

    iget-object v1, p0, Lr/n;->c:Law/h;

    invoke-virtual {v1}, Law/h;->h()V

    throw v0

    :cond_37
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0}, Law/h;->h()V

    .line 415
    return-void
.end method

.method private i()V
    .registers 2

    .prologue
    .line 489
    :try_start_0
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_1} :catch_c

    .line 490
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Lr/n;->f:Z

    if-nez v0, :cond_15

    .line 491
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    .line 493
    :catchall_9
    move-exception v0

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_9

    :try_start_b
    throw v0
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_c} :catch_c

    .line 494
    :catch_c
    move-exception v0

    .line 495
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 497
    :goto_14
    return-void

    .line 493
    :cond_15
    :try_start_15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_9

    goto :goto_14
.end method

.method private j()V
    .registers 3

    .prologue
    .line 564
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_14

    .line 565
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on IndoorBuildingStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 567
    :cond_14
    return-void
.end method


# virtual methods
.method public a(Lo/r;)Lo/y;
    .registers 4
    .parameter

    .prologue
    .line 307
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, p1}, Lt/u;->b(Lo/r;)Lo/y;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v1, v0}, Lt/u;->a(Lo/y;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 310
    const/4 v0, 0x0

    .line 313
    :cond_f
    return-object v0
.end method

.method public a(IZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 469
    return-void
.end method

.method public a(Law/g;)V
    .registers 5
    .parameter

    .prologue
    .line 460
    invoke-interface {p1}, Law/g;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_14

    .line 461
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 463
    :cond_14
    return-void
.end method

.method public a(Lo/r;Ls/c;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lr/p;

    invoke-direct {v3, p1, p2}, Lr/p;-><init>(Lo/r;Ls/c;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 296
    return-void
.end method

.method public b(Law/g;)V
    .registers 5
    .parameter

    .prologue
    .line 479
    invoke-interface {p1}, Law/g;->b()I

    move-result v0

    const/16 v1, 0x76

    if-ne v0, v1, :cond_14

    .line 480
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    iget-object v1, p0, Lr/n;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 482
    :cond_14
    return-void
.end method

.method public b(Lo/r;)Z
    .registers 4
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0, p1}, Lt/u;->b(Lo/r;)Lo/y;

    move-result-object v0

    .line 325
    iget-object v1, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v1, v0}, Lt/u;->a(Lo/y;)Z

    move-result v0

    return v0
.end method

.method public c(Lo/r;)Lo/z;
    .registers 3
    .parameter

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v0

    .line 333
    if-eqz v0, :cond_b

    .line 334
    invoke-virtual {v0, p1}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    .line 336
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public c()V
    .registers 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lr/n;->start()V

    .line 199
    :try_start_3
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_4} :catch_f

    .line 200
    :goto_4
    :try_start_4
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    if-nez v0, :cond_1d

    .line 201
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_4

    .line 203
    :catchall_c
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_c

    :try_start_e
    throw v0
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_f} :catch_f

    .line 204
    :catch_f
    move-exception v0

    .line 205
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 208
    :goto_17
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0, p0}, Law/h;->a(Law/q;)V

    .line 209
    return-void

    .line 203
    :cond_1d
    :try_start_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_c

    goto :goto_17
.end method

.method public d()V
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Lr/n;->c:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    .line 217
    iget-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 219
    :try_start_e
    invoke-virtual {p0}, Lr/n;->join()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_11} :catch_1f

    .line 224
    :goto_11
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->c()V

    .line 226
    iget-object v0, p0, Lr/n;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1e

    .line 229
    :cond_1e
    return-void

    .line 220
    :catch_1f
    move-exception v0

    .line 221
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_11
.end method

.method public e()V
    .registers 2

    .prologue
    .line 503
    invoke-direct {p0}, Lr/n;->i()V

    .line 504
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->a()V

    .line 505
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 512
    invoke-direct {p0}, Lr/n;->i()V

    .line 513
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->b()V

    .line 514
    return-void
.end method

.method public g()J
    .registers 3

    .prologue
    .line 520
    invoke-direct {p0}, Lr/n;->i()V

    .line 521
    iget-object v0, p0, Lr/n;->d:Lt/u;

    invoke-virtual {v0}, Lt/u;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public k()V
    .registers 1

    .prologue
    .line 474
    return-void
.end method

.method public l()V
    .registers 5

    .prologue
    .line 234
    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_7} :catch_2f

    .line 239
    :goto_7
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 241
    new-instance v0, Lr/o;

    invoke-direct {v0, p0}, Lr/o;-><init>(Lr/n;)V

    iput-object v0, p0, Lr/n;->g:Landroid/os/Handler;

    .line 265
    monitor-enter p0

    .line 266
    :try_start_12
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 267
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_12 .. :try_end_16} :catchall_4b

    .line 269
    invoke-static {}, LJ/a;->b()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 270
    iget-object v0, p0, Lr/n;->d:Lt/u;

    iget-object v1, p0, Lr/n;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Lt/u;->a(Ljava/io/File;)V

    .line 274
    :cond_23
    monitor-enter p0

    .line 275
    const/4 v0, 0x1

    :try_start_25
    iput-boolean v0, p0, Lr/n;->f:Z

    .line 276
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 277
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_25 .. :try_end_2b} :catchall_4e

    .line 279
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 280
    return-void

    .line 235
    :catch_2f
    move-exception v0

    .line 236
    invoke-virtual {p0}, Lr/n;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 267
    :catchall_4b
    move-exception v0

    :try_start_4c
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    throw v0

    .line 277
    :catchall_4e
    move-exception v0

    :try_start_4f
    monitor-exit p0
    :try_end_50
    .catchall {:try_start_4f .. :try_end_50} :catchall_4e

    throw v0
.end method
