.class public Lr/q;
.super Lr/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Law/p;ILjava/util/Locale;Ljava/io/File;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 120
    const-string v2, "lts"

    new-instance v3, Lt/t;

    const/16 v0, 0x100

    invoke-direct {v3, v0}, Lt/t;-><init>(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v7, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lr/d;-><init>(Law/p;Ljava/lang/String;Lt/M;Lt/f;IZILjava/util/Locale;Ljava/io/File;)V

    .line 122
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 347
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const-string v0, ""

    goto :goto_a
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/aj;Lo/aq;)Lo/n;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x7

    const/4 v0, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v9, 0x0

    .line 288
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 289
    if-nez v1, :cond_18

    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-nez v1, :cond_19

    .line 326
    :cond_18
    :goto_18
    return-object v0

    .line 296
    :cond_19
    invoke-virtual {p0, v5, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 297
    const/16 v1, 0x1f

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 300
    const/16 v1, 0x1f

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 303
    invoke-static {v1}, LR/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/T;

    move-result-object v1

    .line 304
    invoke-virtual {p2}, Lo/aq;->i()Lo/ad;

    move-result-object v2

    invoke-virtual {v2, v1}, Lo/ad;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 309
    invoke-virtual {p0, v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 310
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 311
    invoke-static {v0, v5}, Lr/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 312
    const/4 v4, 0x4

    invoke-static {v0, v4}, Lr/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    .line 313
    const/16 v5, 0xa

    invoke-static {v0, v5}, Lr/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v6

    .line 315
    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_6f

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    div-int/lit8 v8, v0, 0xa

    .line 317
    :goto_5c
    invoke-static {v3}, Lr/q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    .line 318
    new-array v10, v9, [I

    .line 320
    sget-object v5, Lo/o;->a:Lo/o;

    .line 322
    :try_start_64
    invoke-static {v7}, Lo/o;->a(Ljava/lang/String;)Lo/o;
    :try_end_67
    .catch Ljava/lang/IllegalArgumentException; {:try_start_64 .. :try_end_67} :catch_71

    move-result-object v5

    :goto_68
    move-object v0, p2

    move-object v7, p1

    .line 326
    invoke-static/range {v0 .. v10}, Lo/U;->a(Lo/aq;Lo/T;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/o;Ljava/lang/String;Lo/aj;II[I)Lo/U;

    move-result-object v0

    goto :goto_18

    :cond_6f
    move v8, v9

    .line 315
    goto :goto_5c

    .line 323
    :catch_71
    move-exception v0

    goto :goto_68
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/16 v2, 0x22

    .line 334
    const-string v0, ""

    .line 335
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 336
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 337
    if-ltz v1, :cond_14

    .line 340
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 343
    :cond_14
    return-object v0
.end method


# virtual methods
.method public a()LA/c;
    .registers 2

    .prologue
    .line 142
    sget-object v0, LA/c;->h:LA/c;

    return-object v0
.end method

.method public a(Lo/aq;Z)Lo/ap;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 134
    instance-of v0, p1, Lr/s;

    if-nez v0, :cond_c

    .line 135
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "DashServerLayerTileStore only supports LayerCoords"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_c
    invoke-super {p0, p1, p2}, Lr/d;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 360
    return-void
.end method

.method public a(Lo/aq;Ls/e;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 126
    instance-of v0, p1, Lr/s;

    if-nez v0, :cond_c

    .line 127
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "DashServerLayerTileStore only supports LayerCoords"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_c
    invoke-super {p0, p1, p2}, Lr/d;->a(Lo/aq;Ls/e;)V

    .line 130
    return-void
.end method

.method protected m()Lr/h;
    .registers 3

    .prologue
    .line 147
    new-instance v0, Lr/r;

    iget-object v1, p0, Lr/q;->e:Lcom/google/googlenav/common/a;

    invoke-direct {v0, v1}, Lr/r;-><init>(Lcom/google/googlenav/common/a;)V

    return-object v0
.end method
