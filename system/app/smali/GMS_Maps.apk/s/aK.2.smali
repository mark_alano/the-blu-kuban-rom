.class public Ls/aK;
.super Ls/aP;
.source "SourceFile"


# static fields
.field public static final h:Ls/aO;


# instance fields
.field private j:Z

.field private volatile k:Ls/aO;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 39
    new-instance v0, Ls/aL;

    invoke-direct {v0}, Ls/aL;-><init>()V

    sput-object v0, Ls/aK;->h:Ls/aO;

    return-void
.end method

.method public constructor <init>(Lad/p;Lcom/google/android/maps/driveabout/vector/di;IFLjava/util/Locale;ZLjava/io/File;Ls/aO;Ls/t;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Ls/aP;-><init>(Lad/p;Lcom/google/android/maps/driveabout/vector/di;IFLjava/util/Locale;ZLjava/io/File;Ls/t;)V

    .line 27
    const/4 v1, 0x0

    iput-boolean v1, p0, Ls/aK;->j:Z

    .line 73
    move-object/from16 v0, p8

    iput-object v0, p0, Ls/aK;->k:Ls/aO;

    .line 74
    return-void
.end method

.method public static a(IZ)Ls/aO;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 135
    new-instance v0, Ls/aM;

    invoke-direct {v0, p0, p1}, Ls/aM;-><init>(IZ)V

    return-object v0
.end method

.method private b(Lo/am;Lo/aF;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Ls/aK;->k:Ls/aO;

    if-nez v0, :cond_6

    .line 122
    const/4 v0, 0x1

    .line 124
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Ls/aK;->k:Ls/aO;

    invoke-interface {v0, p1, p2}, Ls/aO;->a(Lo/am;Lo/aF;)Z

    move-result v0

    goto :goto_5
.end method

.method public static n()Ls/aO;
    .registers 1

    .prologue
    .line 186
    new-instance v0, Ls/aN;

    invoke-direct {v0}, Ls/aN;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Lo/am;Lo/aF;Ls/aF;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ls/aK;->b(Lo/am;Lo/aF;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 110
    invoke-super {p0, p1, p3}, Ls/aP;->a(Lo/am;Ls/aF;)V

    .line 114
    :goto_9
    return-void

    .line 112
    :cond_a
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p3, p1, v0, v1}, Ls/aF;->a(Lo/am;ILo/al;)V

    goto :goto_9
.end method

.method public a(Lo/am;Ls/aF;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Ls/aK;->a(Lo/am;Lo/aF;Ls/aF;)V

    .line 97
    return-void
.end method

.method public a(Ls/aO;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Ls/aK;->k:Ls/aO;

    .line 78
    return-void
.end method

.method public a(Lo/am;Lo/aF;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 104
    iget-boolean v0, p0, Ls/aK;->j:Z

    if-eqz v0, :cond_c

    invoke-direct {p0, p1, p2}, Ls/aK;->b(Lo/am;Lo/aF;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 91
    iput-boolean p1, p0, Ls/aK;->j:Z

    .line 92
    return-void
.end method
