.class Ls/o;
.super Lu/c;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Ls/n;


# direct methods
.method constructor <init>(Ljava/lang/String;ILs/n;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheCommitter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lu/c;-><init>(Ljava/lang/String;)V

    .line 526
    iput p2, p0, Ls/o;->a:I

    .line 527
    iput-object p3, p0, Ls/o;->b:Ls/n;

    .line 528
    invoke-virtual {p0}, Ls/o;->start()V

    .line 529
    return-void
.end method


# virtual methods
.method public n_()V
    .registers 5

    .prologue
    .line 536
    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dF;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_9
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_9} :catch_18

    .line 543
    :cond_9
    :goto_9
    :try_start_9
    iget v0, p0, Ls/o;->a:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ls/o;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_f} :catch_34

    .line 548
    iget-object v0, p0, Ls/o;->b:Ls/n;

    invoke-static {v0}, Ls/n;->a(Ls/n;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 552
    :goto_17
    return-void

    .line 537
    :catch_18
    move-exception v0

    .line 538
    invoke-virtual {p0}, Ls/o;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 544
    :catch_34
    move-exception v0

    goto :goto_17
.end method
