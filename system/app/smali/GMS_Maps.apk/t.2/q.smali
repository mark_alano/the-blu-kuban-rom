.class public abstract Lt/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:I

.field private static final e:[Lt/q;


# instance fields
.field protected a:Lt/n;

.field private c:Lt/v;

.field private d:I

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 110
    invoke-static {}, Lt/v;->values()[Lt/v;

    move-result-object v0

    array-length v0, v0

    sput v0, Lt/q;->b:I

    .line 118
    sget v0, Lt/q;->b:I

    new-array v0, v0, [Lt/q;

    sput-object v0, Lt/q;->e:[Lt/q;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt/q;->f:Z

    .line 1906
    return-void
.end method

.method static synthetic a(Lt/q;Lt/v;)Lt/v;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lt/q;->c:Lt/v;

    return-object p1
.end method

.method private final a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/cX;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1879
    new-instance v0, Lcom/google/android/maps/driveabout/app/ey;

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/ey;-><init>(Lcom/google/android/maps/driveabout/app/cN;)V

    .line 1880
    new-instance v1, Lt/t;

    invoke-direct {v1, p0, p2}, Lt/t;-><init>(Lt/q;Lcom/google/android/maps/driveabout/app/cX;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/ey;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/eH;)V

    .line 1881
    return-void
.end method

.method private a(Lp/Q;I[Lp/b;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 831
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 832
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 833
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bm;->a(Lp/Q;I[Lp/b;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 835
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 836
    const-string v1, "UserRequestedReroute"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 837
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 838
    return-void
.end method

.method static synthetic a(Lt/q;Ljava/io/File;Lcom/google/android/maps/driveabout/app/cX;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lt/q;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/cX;)V

    return-void
.end method

.method private ai()V
    .registers 3

    .prologue
    .line 299
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_19

    .line 300
    invoke-static {}, Lcom/google/android/maps/driveabout/app/cf;->a()Lcom/google/android/maps/driveabout/app/cf;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 301
    invoke-static {}, Lcom/google/android/maps/driveabout/app/cf;->a()Lcom/google/android/maps/driveabout/app/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cf;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_19

    .line 303
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->invalidateOptionsMenu()V

    .line 310
    :cond_19
    :goto_19
    return-void

    .line 306
    :cond_1a
    const-string v0, "UIState"

    const-string v1, "NavigationApplicationDelegate.getInstance() should never be null."

    invoke-static {v0, v1}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_19
.end method

.method private aj()V
    .registers 4

    .prologue
    .line 481
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    .line 482
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->a()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cN;->a(ZZ)V

    .line 484
    return-void
.end method

.method private ak()V
    .registers 3

    .prologue
    .line 512
    iget v0, p0, Lt/q;->d:I

    if-lez v0, :cond_e

    .line 513
    const-string v0, "z"

    iget v1, p0, Lt/q;->d:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;I)V

    .line 514
    const/4 v0, 0x0

    iput v0, p0, Lt/q;->d:I

    .line 516
    :cond_e
    return-void
.end method

.method private al()V
    .registers 4

    .prologue
    .line 1344
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->h()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    .line 1345
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v1

    .line 1348
    sget-object v2, Lcom/google/android/maps/driveabout/vector/D;->b:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, v2, :cond_21

    .line 1349
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    .line 1353
    :cond_21
    invoke-static {v1}, Ln/F;->a(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_5d

    .line 1354
    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, v1, :cond_3f

    .line 1355
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1356
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->t()V

    .line 1383
    :cond_3f
    :goto_3f
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cP;->h()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cP;->i()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/D;Z)V

    .line 1385
    return-void

    .line 1358
    :cond_5d
    invoke-static {v1}, Ln/F;->a(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_ab

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->r()Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 1363
    invoke-virtual {v1}, Ln/b;->b()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-virtual {v1}, Ln/b;->g()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 1364
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    .line 1367
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->s()I

    move-result v0

    .line 1369
    const-string v1, "T"

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;I)V

    .line 1374
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 1375
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_3f

    .line 1378
    :cond_ab
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-eqz v1, :cond_c8

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, v1, :cond_c8

    .line 1379
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto/16 :goto_3f

    .line 1380
    :cond_c8
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-nez v1, :cond_3f

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v0, v1, :cond_3f

    .line 1381
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto/16 :goto_3f
.end method

.method private am()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 1388
    invoke-virtual {p0}, Lt/q;->Y()Z

    move-result v1

    if-eqz v1, :cond_43

    .line 1389
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v1

    .line 1390
    new-instance v2, Ln/b;

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v3

    invoke-direct {v2, v3}, Ln/b;-><init>(Landroid/location/Location;)V

    .line 1392
    invoke-virtual {v2, v1}, Ln/b;->setBearing(F)V

    .line 1393
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cN;->setMyLocation(Landroid/location/Location;Z)V

    .line 1399
    :goto_2c
    invoke-virtual {p0}, Lt/q;->Z()V

    .line 1400
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setCurrentRoadName(Landroid/location/Location;)V

    .line 1401
    return-void

    .line 1395
    :cond_43
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v1

    .line 1396
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-eqz v3, :cond_69

    invoke-virtual {v1}, Ln/b;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_69

    :goto_65
    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cN;->setMyLocation(Landroid/location/Location;Z)V

    goto :goto_2c

    :cond_69
    const/4 v0, 0x0

    goto :goto_65
.end method

.method private an()I
    .registers 2

    .prologue
    .line 1432
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1433
    const/4 v0, 0x2

    .line 1435
    :goto_d
    return v0

    :cond_e
    invoke-virtual {p0}, Lt/q;->f()I

    move-result v0

    goto :goto_d
.end method

.method private ao()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1462
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1463
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->E()V

    .line 1490
    :goto_17
    invoke-virtual {p0}, Lt/q;->x()V

    .line 1491
    return-void

    .line 1464
    :cond_1b
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 1465
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d008b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;Z)V

    goto :goto_17

    .line 1467
    :cond_3e
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 1469
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    if-eqz v0, :cond_cc

    .line 1470
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    invoke-virtual {v0}, Lp/y;->m()Lp/Q;

    move-result-object v0

    .line 1471
    if-eqz v0, :cond_cc

    .line 1472
    invoke-virtual {v0}, Lp/Q;->c()Lo/s;

    move-result-object v0

    .line 1473
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v3

    invoke-virtual {v3, v0}, Ln/b;->b(Lo/s;)F

    move-result v0

    const/high16 v3, 0x4270

    cmpl-float v0, v0, v3

    if-lez v0, :cond_c3

    move v0, v1

    .line 1477
    :goto_7f
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->C()Lp/Q;

    move-result-object v4

    .line 1478
    invoke-virtual {v4}, Lp/Q;->k()Z

    move-result v5

    .line 1479
    const/4 v3, 0x0

    .line 1480
    if-eqz v5, :cond_ac

    invoke-virtual {v4}, Lp/Q;->h()LaT/h;

    move-result-object v5

    if-eqz v5, :cond_ac

    invoke-virtual {v4}, Lp/Q;->h()LaT/h;

    move-result-object v5

    invoke-virtual {v5}, LaT/h;->d()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_ac

    .line 1482
    invoke-virtual {v4}, Lp/Q;->h()LaT/h;

    move-result-object v3

    invoke-virtual {v3}, LaT/h;->b()Ljava/lang/String;

    move-result-object v3

    .line 1484
    :cond_ac
    iget-object v5, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v5}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v5

    invoke-virtual {v4}, Lp/Q;->i()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v0, v3, v4}, Lcom/google/android/maps/driveabout/app/cN;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1486
    invoke-static {v3}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c5

    :goto_bf
    iput-boolean v1, p0, Lt/q;->f:Z

    goto/16 :goto_17

    :cond_c3
    move v0, v2

    .line 1473
    goto :goto_7f

    :cond_c5
    move v1, v2

    .line 1486
    goto :goto_bf

    .line 1488
    :cond_c7
    invoke-virtual {p0}, Lt/q;->u()V

    goto/16 :goto_17

    :cond_cc
    move v0, v2

    goto :goto_7f
.end method

.method private ap()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1526
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()Lp/Q;

    move-result-object v0

    .line 1527
    if-eqz v0, :cond_32

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v1

    if-eqz v1, :cond_32

    invoke-virtual {v0}, Lp/Q;->d()Lp/R;

    move-result-object v1

    if-eqz v1, :cond_32

    invoke-virtual {v0}, Lp/Q;->d()Lp/R;

    move-result-object v1

    invoke-virtual {v1}, Lp/R;->a()I

    move-result v1

    if-lez v1, :cond_32

    .line 1530
    invoke-virtual {v0}, Lp/Q;->d()Lp/R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lp/R;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1554
    :goto_31
    return-object v0

    :cond_32
    const/4 v0, 0x0

    goto :goto_31
.end method

.method private aq()V
    .registers 1

    .prologue
    .line 1606
    invoke-virtual {p0}, Lt/q;->ab()V

    .line 1607
    return-void
.end method

.method private ar()V
    .registers 3

    .prologue
    .line 1653
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->d()I

    move-result v0

    .line 1654
    const/4 v1, -0x1

    if-eq v0, v1, :cond_17

    .line 1655
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cN;->a(I)V

    .line 1659
    :goto_16
    return-void

    .line 1657
    :cond_17
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->j()V

    goto :goto_16
.end method

.method private as()V
    .registers 5

    .prologue
    .line 1665
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->p()I

    move-result v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cN;->a(IIZ)V

    .line 1669
    return-void
.end method

.method private at()V
    .registers 3

    .prologue
    .line 1674
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    if-eqz v0, :cond_17

    .line 1675
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setTrafficMode(I)V

    .line 1681
    :goto_16
    return-void

    .line 1676
    :cond_17
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 1677
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setTrafficMode(I)V

    goto :goto_16

    .line 1679
    :cond_2e
    invoke-virtual {p0}, Lt/q;->v()V

    goto :goto_16
.end method

.method private au()V
    .registers 4

    .prologue
    .line 1699
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-nez v0, :cond_23

    .line 1700
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0062

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setStatusBarContent(Ljava/lang/CharSequence;)V

    .line 1710
    :goto_22
    return-void

    .line 1702
    :cond_23
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 1703
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0063

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setStatusBarContent(Ljava/lang/CharSequence;)V

    goto :goto_22

    .line 1706
    :cond_46
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const-string v1, "__route_status"

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setStatusBarContent(Ljava/lang/CharSequence;)V

    .line 1708
    invoke-virtual {p0}, Lt/q;->w()V

    goto :goto_22
.end method

.method private av()Z
    .registers 3

    .prologue
    .line 1729
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v0

    const/16 v1, 0x1324

    if-le v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private aw()V
    .registers 2

    .prologue
    .line 1737
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->g()Lt/q;

    move-result-object v0

    invoke-virtual {v0}, Lt/q;->E()V

    .line 1738
    return-void
.end method

.method private final ax()V
    .registers 2

    .prologue
    .line 1744
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->j()V

    .line 1745
    return-void
.end method

.method private j(Z)V
    .registers 6
    .parameter

    .prologue
    .line 1418
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    invoke-direct {p0}, Lt/q;->an()I

    move-result v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cP;->h()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cn;->a(ILcom/google/android/maps/driveabout/vector/D;I)V

    .line 1421
    invoke-virtual {p0, p1}, Lt/q;->a(Z)V

    .line 1422
    return-void
.end method


# virtual methods
.method public A()Z
    .registers 2

    .prologue
    .line 1770
    const/4 v0, 0x0

    return v0
.end method

.method public B()Z
    .registers 2

    .prologue
    .line 197
    const/4 v0, 0x1

    return v0
.end method

.method public final C()Lt/v;
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, Lt/q;->c:Lt/v;

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Lt/q;->c:Lt/v;

    invoke-virtual {v0}, Lt/v;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public E()V
    .registers 2

    .prologue
    .line 277
    invoke-direct {p0}, Lt/q;->ax()V

    .line 278
    invoke-direct {p0}, Lt/q;->aj()V

    .line 279
    invoke-direct {p0}, Lt/q;->al()V

    .line 280
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lt/q;->j(Z)V

    .line 281
    invoke-direct {p0}, Lt/q;->ao()V

    .line 282
    invoke-direct {p0}, Lt/q;->ai()V

    .line 283
    invoke-direct {p0}, Lt/q;->au()V

    .line 284
    invoke-direct {p0}, Lt/q;->aq()V

    .line 285
    invoke-direct {p0}, Lt/q;->ar()V

    .line 286
    invoke-direct {p0}, Lt/q;->as()V

    .line 287
    invoke-direct {p0}, Lt/q;->at()V

    .line 288
    invoke-virtual {p0}, Lt/q;->m()V

    .line 289
    return-void
.end method

.method public F()V
    .registers 2

    .prologue
    .line 319
    invoke-direct {p0}, Lt/q;->am()V

    .line 320
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lt/q;->j(Z)V

    .line 321
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 322
    invoke-direct {p0}, Lt/q;->ao()V

    .line 324
    :cond_16
    return-void
.end method

.method public final G()V
    .registers 4

    .prologue
    .line 331
    invoke-virtual {p0}, Lt/q;->Y()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 332
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v0

    .line 333
    new-instance v1, Ln/b;

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v2

    invoke-direct {v1, v2}, Ln/b;-><init>(Landroid/location/Location;)V

    .line 335
    invoke-virtual {v1, v0}, Ln/b;->setBearing(F)V

    .line 336
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->setMyLocation(Landroid/location/Location;Z)V

    .line 338
    :cond_2c
    return-void
.end method

.method protected H()Z
    .registers 2

    .prologue
    .line 370
    const/4 v0, 0x1

    return v0
.end method

.method public final I()V
    .registers 4

    .prologue
    .line 522
    invoke-direct {p0}, Lt/q;->ax()V

    .line 523
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->e()Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x1

    .line 524
    :goto_10
    new-instance v1, Ll/D;

    const-string v2, "compassButton"

    invoke-direct {v1, v2, v0}, Ll/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v1}, LB/f;->b(LB/j;)V

    .line 526
    if-eqz v0, :cond_2a

    const-string v0, "3"

    :goto_1e
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 530
    invoke-virtual {p0}, Lt/q;->g()V

    .line 531
    invoke-direct {p0}, Lt/q;->aw()V

    .line 532
    return-void

    .line 523
    :cond_28
    const/4 v0, 0x0

    goto :goto_10

    .line 526
    :cond_2a
    const-string v0, "2"

    goto :goto_1e
.end method

.method public final J()V
    .registers 3

    .prologue
    .line 582
    invoke-direct {p0}, Lt/q;->ax()V

    .line 583
    new-instance v0, Ll/C;

    const-string v1, "zoomOut"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 585
    iget v0, p0, Lt/q;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/q;->d:I

    .line 587
    invoke-virtual {p0}, Lt/q;->i()V

    .line 588
    return-void
.end method

.method public final K()V
    .registers 3

    .prologue
    .line 594
    invoke-direct {p0}, Lt/q;->ax()V

    .line 595
    new-instance v0, Ll/C;

    const-string v1, "zoomIn"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 597
    iget v0, p0, Lt/q;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/q;->d:I

    .line 599
    invoke-virtual {p0}, Lt/q;->h()V

    .line 600
    return-void
.end method

.method public final L()Z
    .registers 3

    .prologue
    .line 608
    invoke-direct {p0}, Lt/q;->ax()V

    .line 609
    new-instance v0, Ll/C;

    const-string v1, "back"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 611
    invoke-virtual {p0}, Lt/q;->j()Z

    move-result v0

    return v0
.end method

.method public final M()V
    .registers 4

    .prologue
    .line 618
    invoke-direct {p0}, Lt/q;->ax()V

    .line 619
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 628
    :cond_1b
    :goto_1b
    return-void

    .line 623
    :cond_1c
    new-instance v0, Ll/C;

    const-string v1, "routeOverview"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 625
    const-string v0, "r"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->f:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    goto :goto_1b
.end method

.method public final N()V
    .registers 4

    .prologue
    .line 653
    invoke-direct {p0}, Lt/q;->ax()V

    .line 654
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    if-nez v0, :cond_1c

    .line 662
    :cond_1b
    :goto_1b
    return-void

    .line 658
    :cond_1c
    new-instance v0, Ll/C;

    const-string v1, "viewList"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 660
    const-string v0, "l"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 661
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->g:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    goto :goto_1b
.end method

.method public final O()V
    .registers 3

    .prologue
    .line 721
    invoke-direct {p0}, Lt/q;->ax()V

    .line 722
    new-instance v0, Ll/C;

    const-string v1, "backToLocation"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 724
    const-string v0, "n"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 726
    invoke-virtual {p0}, Lt/q;->k()V

    .line 727
    return-void
.end method

.method public final P()V
    .registers 4

    .prologue
    .line 773
    invoke-direct {p0}, Lt/q;->ax()V

    .line 776
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 777
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 778
    const-string v1, "TravelMode"

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 780
    const-string v1, "ForceNewDestination"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 781
    const/high16 v1, 0x1002

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 782
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 783
    return-void
.end method

.method public final Q()V
    .registers 2

    .prologue
    .line 844
    invoke-direct {p0}, Lt/q;->ax()V

    .line 845
    const-string v0, "+"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 847
    invoke-virtual {p0}, Lt/q;->r()V

    .line 848
    invoke-direct {p0}, Lt/q;->aw()V

    .line 849
    return-void
.end method

.method public final R()V
    .registers 2

    .prologue
    .line 856
    invoke-direct {p0}, Lt/q;->ax()V

    .line 857
    const-string v0, "-"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 859
    invoke-virtual {p0}, Lt/q;->X()V

    .line 860
    invoke-direct {p0}, Lt/q;->aw()V

    .line 862
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 863
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->g()V

    .line 865
    :cond_27
    return-void
.end method

.method public final S()V
    .registers 2

    .prologue
    .line 872
    invoke-direct {p0}, Lt/q;->ax()V

    .line 874
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 875
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->i()V

    .line 877
    :cond_1c
    return-void
.end method

.method T()Z
    .registers 2

    .prologue
    .line 920
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->g()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    if-nez v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public final U()V
    .registers 1

    .prologue
    .line 987
    invoke-direct {p0}, Lt/q;->ax()V

    .line 988
    invoke-virtual {p0}, Lt/q;->l()V

    .line 989
    invoke-direct {p0}, Lt/q;->aw()V

    .line 990
    return-void
.end method

.method public final V()V
    .registers 1

    .prologue
    .line 1061
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1062
    invoke-virtual {p0}, Lt/q;->t()V

    .line 1063
    invoke-direct {p0}, Lt/q;->aw()V

    .line 1064
    return-void
.end method

.method public W()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1072
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1073
    const-string v0, "."

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 1075
    const v0, 0x7f0d0012

    .line 1076
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v1

    if-eqz v1, :cond_2c

    .line 1077
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v1

    invoke-virtual {v1}, Lp/J;->c()I

    move-result v1

    if-ne v1, v3, :cond_72

    .line 1078
    const v0, 0x7f0d0010

    .line 1083
    :cond_2c
    :goto_2c
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;I)V

    .line 1086
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cN;->g(Z)V

    .line 1087
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lt/q;->a(Lp/J;)V

    .line 1088
    invoke-direct {p0}, Lt/q;->ao()V

    .line 1089
    invoke-direct {p0}, Lt/q;->au()V

    .line 1091
    invoke-virtual {p0}, Lt/q;->n()V

    .line 1093
    iget-boolean v0, p0, Lt/q;->f:Z

    if-eqz v0, :cond_6e

    .line 1094
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()Lp/Q;

    move-result-object v0

    .line 1095
    invoke-virtual {v0}, Lp/Q;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lp/Q;->h()LaT/h;

    move-result-object v0

    invoke-static {v1, v0, v4}, LaT/a;->a(Ljava/lang/String;LaT/h;Z)V

    .line 1099
    :cond_6e
    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dm;->b(Z)V

    .line 1100
    return-void

    .line 1079
    :cond_72
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v1

    invoke-virtual {v1}, Lp/J;->c()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2c

    .line 1080
    const v0, 0x7f0d0011

    goto :goto_2c
.end method

.method protected X()V
    .registers 3

    .prologue
    .line 1256
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->h(Z)V

    .line 1257
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1258
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->v()V

    .line 1260
    :cond_1f
    return-void
.end method

.method protected Y()Z
    .registers 3

    .prologue
    .line 1409
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->d()Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_27

    const/4 v0, 0x1

    :goto_26
    return v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method protected Z()V
    .registers 1

    .prologue
    .line 1415
    return-void
.end method

.method public a()V
    .registers 2

    .prologue
    .line 232
    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lt/q;->b()V

    .line 234
    return-void
.end method

.method protected a(FF)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1176
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->i:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 1177
    return-void
.end method

.method protected a(FFF)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1190
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->i:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 1191
    return-void
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 1113
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1114
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(IIZ)V

    .line 1117
    invoke-virtual {p0, p1}, Lt/q;->b(I)V

    .line 1118
    return-void
.end method

.method public final a(IIFIIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    .line 343
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/cn;->a(IIF)V

    .line 344
    invoke-virtual {v0, p7}, Lcom/google/android/maps/driveabout/app/cn;->a(I)V

    .line 345
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lt/q;->j(Z)V

    .line 346
    return-void
.end method

.method public final a(LB/f;)V
    .registers 6
    .parameter

    .prologue
    .line 1831
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1832
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->n()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1856
    :goto_f
    return-void

    .line 1835
    :cond_10
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->d(Z)V

    .line 1838
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "da_voice-rmi.3gp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1839
    new-instance v1, Lcom/google/android/maps/driveabout/app/cX;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/app/cX;-><init>(Ljava/io/File;)V

    .line 1840
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cX;->a(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/cP;)V

    .line 1841
    if-eqz p1, :cond_4a

    .line 1842
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/cX;->a(LB/f;Lcom/google/android/maps/driveabout/app/cN;)V

    .line 1844
    :cond_4a
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 1845
    invoke-direct {p0, v0, v1}, Lt/q;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/cX;)V

    goto :goto_f

    .line 1847
    :cond_5e
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    new-instance v3, Lt/s;

    invoke-direct {v3, p0, v0, v1}, Lt/s;-><init>(Lt/q;Ljava/io/File;Lcom/google/android/maps/driveabout/app/cX;)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_f
.end method

.method protected a(Landroid/os/Bundle;Lo/ab;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1329
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ab;)V

    .line 1330
    return-void
.end method

.method public a(Landroid/os/Bundle;Lo/ab;Lcom/google/android/maps/driveabout/app/ew;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1132
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1133
    new-instance v0, Ll/C;

    const-string v1, "search"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 1135
    const-string v0, "?"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 1137
    invoke-virtual {p0, p1, p2}, Lt/q;->a(Landroid/os/Bundle;Lo/ab;)V

    .line 1138
    invoke-interface {p3, p1}, Lcom/google/android/maps/driveabout/app/ew;->a(Landroid/os/Bundle;)V

    .line 1139
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .registers 5
    .parameter

    .prologue
    .line 381
    const v0, 0x7f10011c

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;

    .line 384
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 385
    const v1, 0x7f020113

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 386
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0074

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    .line 392
    :goto_2f
    return-void

    .line 388
    :cond_30
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 389
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0075

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    goto :goto_2f
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bP;)V
    .registers 4
    .parameter

    .prologue
    const v1, 0x7f10011b

    .line 1336
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_13

    .line 1337
    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 1341
    :goto_12
    return-void

    .line 1339
    :cond_13
    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    goto :goto_12
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/d;)V
    .registers 5
    .parameter

    .prologue
    .line 1195
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->i:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1196
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cN;->a(Lcom/google/android/maps/driveabout/vector/d;)V

    .line 1197
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cn;->a(Lcom/google/android/maps/driveabout/vector/d;)V

    .line 1198
    invoke-direct {p0}, Lt/q;->aw()V

    .line 1200
    :cond_24
    return-void
.end method

.method protected a(Lo/Q;)V
    .registers 3
    .parameter

    .prologue
    .line 1180
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->F()V

    .line 1181
    invoke-virtual {p0}, Lt/q;->aa()V

    .line 1182
    return-void
.end method

.method public final a(Lo/aL;Lcom/google/android/maps/driveabout/app/bv;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 632
    invoke-direct {p0}, Lt/q;->ax()V

    .line 633
    new-instance v0, Ll/C;

    const-string v1, "layers"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 635
    const-string v1, "s"

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_6b

    move v0, v4

    :goto_1e
    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 638
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->k()Lcom/google/android/maps/driveabout/app/bz;

    move-result-object v2

    .line 639
    invoke-virtual {v2, p1}, Lcom/google/android/maps/driveabout/app/bz;->a(Lo/aL;)V

    .line 640
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->k:Lt/v;

    invoke-virtual {v0, v1}, Lt/n;->b(Lt/v;)Z

    move-result v5

    .line 641
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bz;->b()Lcom/google/android/maps/driveabout/app/bF;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bz;->c()Lcom/google/android/maps/driveabout/app/bF;

    move-result-object v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    iget-object v7, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v7}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/cP;->h()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v7

    sget-object v8, Lcom/google/android/maps/driveabout/vector/D;->b:Lcom/google/android/maps/driveabout/vector/D;

    if-ne v7, v8, :cond_6d

    :goto_5c
    iget-object v6, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v6}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cP;->i()Z

    move-result v6

    move-object v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/cN;->a(Lcom/google/android/maps/driveabout/app/bF;Lcom/google/android/maps/driveabout/app/bF;IZZZLcom/google/android/maps/driveabout/app/bv;)V

    .line 647
    return-void

    :cond_6b
    move v0, v6

    .line 635
    goto :goto_1e

    :cond_6d
    move v4, v6

    .line 641
    goto :goto_5c
.end method

.method protected final a(Lp/J;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1563
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1564
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lp/J;)V

    .line 1565
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    .line 1566
    if-eqz p1, :cond_23

    if-eqz v0, :cond_23

    invoke-virtual {p1}, Lp/J;->b()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2d

    .line 1569
    :cond_23
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3}, Lcom/google/android/maps/driveabout/app/cN;->setTurnArrowOverlay(Lp/y;Lp/J;Lcom/google/android/maps/driveabout/vector/aE;)V

    .line 1578
    :goto_2c
    return-void

    .line 1571
    :cond_2d
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    new-instance v2, Lt/r;

    invoke-direct {v2, p0, p1}, Lt/r;-><init>(Lt/q;Lp/J;)V

    invoke-interface {v1, v0, p1, v2}, Lcom/google/android/maps/driveabout/app/cN;->setTurnArrowOverlay(Lp/y;Lp/J;Lcom/google/android/maps/driveabout/vector/aE;)V

    goto :goto_2c
.end method

.method protected a(Lp/J;Lp/J;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1274
    invoke-virtual {p0, p2}, Lt/q;->a(Lp/J;)V

    .line 1275
    return-void
.end method

.method protected a(Lp/J;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1278
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->j:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1279
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->g()Lt/q;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lt/q;->a(Lp/J;Z)V

    .line 1281
    :cond_14
    return-void
.end method

.method public final a(Lp/Q;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 794
    invoke-direct {p0}, Lt/q;->ax()V

    .line 795
    new-instance v0, Ll/D;

    const-string v1, "newDestination"

    invoke-virtual {p1}, Lp/Q;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ll/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 798
    if-eqz p2, :cond_18

    .line 799
    const-string v0, "d"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 802
    :cond_18
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->H()[Lp/b;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lt/q;->a(Lp/Q;I[Lp/b;)V

    .line 805
    return-void
.end method

.method protected a(Lp/g;)V
    .registers 5
    .parameter

    .prologue
    .line 1293
    invoke-virtual {p0, p1}, Lt/q;->c(Lp/g;)I

    move-result v0

    .line 1294
    if-lez v0, :cond_1a

    .line 1295
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;I)V

    .line 1298
    :cond_1a
    return-void
.end method

.method public final a(Lp/j;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 907
    invoke-direct {p0}, Lt/q;->ax()V

    .line 908
    invoke-direct {p0}, Lt/q;->ak()V

    .line 910
    invoke-virtual {p0, p1, p2}, Lt/q;->c(Lp/j;I)V

    .line 911
    invoke-direct {p0}, Lt/q;->aw()V

    .line 912
    return-void
.end method

.method public a(Lp/y;)V
    .registers 2
    .parameter

    .prologue
    .line 1748
    return-void
.end method

.method protected a(Lp/y;[Lp/y;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1288
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lp/y;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/cN;->b(Lp/y;[Lp/y;)V

    .line 1289
    return-void
.end method

.method protected a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1453
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cn;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 1455
    return-void
.end method

.method public final a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 358
    invoke-virtual {p0}, Lt/q;->H()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 566
    invoke-direct {p0}, Lt/q;->ax()V

    .line 575
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lt/p;)Z
    .registers 3
    .parameter

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method protected final aa()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1589
    .line 1591
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v3

    .line 1592
    if-eqz v3, :cond_42

    .line 1593
    invoke-virtual {v3}, Lp/J;->k()Lp/J;

    move-result-object v0

    if-eqz v0, :cond_3c

    move v0, v1

    .line 1594
    :goto_15
    invoke-virtual {v3}, Lp/J;->j()Lp/J;

    move-result-object v3

    if-eqz v3, :cond_3e

    move v3, v1

    .line 1597
    :goto_1c
    iget-object v4, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v4}, Lt/n;->g()Lt/q;

    move-result-object v4

    invoke-virtual {v4}, Lt/q;->C()Lt/v;

    move-result-object v4

    .line 1598
    sget-object v5, Lt/v;->l:Lt/v;

    if-eq v4, v5, :cond_40

    sget-object v5, Lt/v;->f:Lt/v;

    if-eq v4, v5, :cond_40

    sget-object v5, Lt/v;->g:Lt/v;

    if-eq v4, v5, :cond_40

    .line 1602
    :goto_32
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v2

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/maps/driveabout/app/cN;->a(ZZZ)V

    .line 1603
    return-void

    :cond_3c
    move v0, v2

    .line 1593
    goto :goto_15

    :cond_3e
    move v3, v2

    .line 1594
    goto :goto_1c

    :cond_40
    move v1, v2

    .line 1598
    goto :goto_32

    :cond_42
    move v3, v2

    move v0, v2

    goto :goto_1c
.end method

.method protected ab()V
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 1615
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-nez v1, :cond_f

    .line 1650
    :cond_e
    :goto_e
    return-void

    .line 1619
    :cond_f
    invoke-static {}, Lcom/google/android/maps/driveabout/app/du;->a()Lcom/google/android/maps/driveabout/app/du;

    move-result-object v2

    .line 1620
    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->E()[Lp/Q;

    move-result-object v1

    .line 1621
    if-eqz v1, :cond_e

    array-length v3, v1

    if-eqz v3, :cond_e

    .line 1625
    const/4 v4, 0x0

    .line 1628
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->Q()Z

    move-result v3

    if-eqz v3, :cond_5f

    .line 1629
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->b()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d0045

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    .line 1646
    :goto_3d
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->l()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->H()[Lp/b;

    move-result-object v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cQ;->a([Lp/Q;I[Lp/b;Ljava/lang/CharSequence;ZZ)V

    goto :goto_e

    .line 1632
    :cond_5f
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-nez v3, :cond_7b

    .line 1633
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0062

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    .line 1634
    goto :goto_3d

    .line 1635
    :cond_7b
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v3

    if-eqz v3, :cond_97

    .line 1636
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0063

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    .line 1637
    goto :goto_3d

    .line 1638
    :cond_97
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v3

    if-eqz v3, :cond_cd

    .line 1639
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v3

    iget-object v4, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v4}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v4

    iget-object v5, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v5}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v5

    invoke-virtual {v5}, Lp/y;->q()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/du;->a(ILp/J;I)Landroid/text/Spannable;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_3d

    .line 1643
    :cond_cd
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v2

    if-eqz v2, :cond_ea

    .line 1644
    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d008b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_3d

    :cond_ea
    move v6, v0

    move v5, v0

    goto/16 :goto_3d
.end method

.method public final ac()V
    .registers 3

    .prologue
    .line 1781
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1782
    new-instance v0, Ll/C;

    const-string v1, "showStreetView"

    invoke-direct {v0, v1}, Ll/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 1784
    invoke-virtual {p0}, Lt/q;->o()V

    .line 1785
    return-void
.end method

.method protected ad()V
    .registers 2

    .prologue
    .line 1803
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->c()V

    .line 1804
    invoke-direct {p0}, Lt/q;->ar()V

    .line 1805
    return-void
.end method

.method public final ae()V
    .registers 3

    .prologue
    .line 1812
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->c(Z)V

    .line 1813
    invoke-virtual {p0}, Lt/q;->af()V

    .line 1814
    return-void
.end method

.method protected af()V
    .registers 2

    .prologue
    .line 1817
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->l()V

    .line 1818
    return-void
.end method

.method public ag()V
    .registers 5

    .prologue
    .line 1859
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()Lp/Q;

    move-result-object v0

    .line 1860
    invoke-virtual {v0}, Lp/Q;->g()Ljava/lang/String;

    move-result-object v1

    .line 1861
    invoke-static {v1}, Lo/m;->a(Ljava/lang/String;)Lo/m;

    move-result-object v2

    .line 1862
    iget-object v3, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v3}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(Lo/m;)V

    .line 1865
    invoke-virtual {v0}, Lp/Q;->h()LaT/h;

    move-result-object v2

    .line 1866
    invoke-virtual {v0}, Lp/Q;->j()LaT/c;

    move-result-object v0

    .line 1867
    if-eqz v2, :cond_2e

    if-eqz v0, :cond_2e

    .line 1868
    invoke-virtual {v0}, LaT/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    :cond_2e
    return-void
.end method

.method public ah()V
    .registers 3

    .prologue
    .line 1874
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->C()Lp/Q;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->a(Lp/Q;)V

    .line 1876
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 242
    return-void
.end method

.method public final b(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 671
    invoke-direct {p0}, Lt/q;->ax()V

    .line 680
    invoke-virtual {p0, p1, p2}, Lt/q;->a(FF)V

    .line 681
    return-void
.end method

.method public final b(FFF)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 696
    invoke-direct {p0}, Lt/q;->ax()V

    .line 699
    invoke-virtual {p0, p1, p2, p3}, Lt/q;->a(FFF)V

    .line 700
    return-void
.end method

.method protected b(I)V
    .registers 2
    .parameter

    .prologue
    .line 1326
    return-void
.end method

.method public final b(Lcom/google/android/maps/driveabout/app/bP;)V
    .registers 9
    .parameter

    .prologue
    const v6, 0x7f1000f8

    const v5, 0x7f1000f5

    const v4, 0x7f1000f4

    const v3, 0x7f1004ac

    const v2, 0x7f1004ab

    .line 402
    invoke-static {}, Lcom/google/android/maps/driveabout/app/cf;->a()Lcom/google/android/maps/driveabout/app/cf;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cf;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 404
    const v0, 0x7f100122

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 410
    :goto_25
    const v0, 0x7f1004a7

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 411
    const v0, 0x7f1004a8

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 416
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_99

    .line 417
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 418
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 419
    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 420
    const v0, 0x7f100120

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 421
    const v0, 0x7f10011a

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 422
    const v0, 0x7f1004a9

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 423
    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 424
    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 425
    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 456
    :goto_64
    const v0, 0x7f1004af

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 462
    const v0, 0x7f1004b0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 469
    invoke-virtual {p0, p1}, Lt/q;->a(Lcom/google/android/maps/driveabout/app/bP;)V

    .line 472
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_91

    .line 473
    const v0, 0x7f1004af

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 474
    const v0, 0x7f1004ae

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 475
    const v0, 0x7f1004ad

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 476
    const v0, 0x7f1004b0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 478
    :cond_91
    return-void

    .line 406
    :cond_92
    const v0, 0x7f100122

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    goto :goto_25

    .line 427
    :cond_99
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 428
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 429
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 434
    :goto_af
    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 435
    const v0, 0x7f100120

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 436
    const v0, 0x7f10011a

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 437
    const v0, 0x7f1004a9

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 438
    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 439
    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 440
    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    goto :goto_64

    .line 431
    :cond_d1
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 432
    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    goto :goto_af
.end method

.method public final b(Lcom/google/android/maps/driveabout/vector/d;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 539
    invoke-direct {p0}, Lt/q;->ax()V

    .line 540
    new-instance v0, Ll/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Ll/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 544
    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 545
    invoke-virtual {p0, p1}, Lt/q;->a(Lcom/google/android/maps/driveabout/vector/d;)V

    .line 546
    return-void
.end method

.method public final b(Lo/Q;)V
    .registers 2
    .parameter

    .prologue
    .line 688
    invoke-direct {p0}, Lt/q;->ax()V

    .line 689
    invoke-virtual {p0, p1}, Lt/q;->a(Lo/Q;)V

    .line 690
    return-void
.end method

.method public final b(Lp/J;Lp/J;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 953
    invoke-direct {p0}, Lt/q;->ax()V

    .line 954
    invoke-virtual {p0, p1, p2}, Lt/q;->a(Lp/J;Lp/J;)V

    .line 955
    invoke-direct {p0}, Lt/q;->aw()V

    .line 956
    return-void
.end method

.method public final b(Lp/J;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 970
    invoke-direct {p0}, Lt/q;->ax()V

    .line 971
    if-eqz p1, :cond_1e

    .line 972
    new-instance v0, Ll/D;

    const-string v1, "step"

    invoke-virtual {p1}, Lp/J;->i()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ll/D;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 974
    const-string v0, "p"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 976
    invoke-virtual {p0, p1, p2}, Lt/q;->a(Lp/J;Z)V

    .line 977
    invoke-direct {p0}, Lt/q;->aw()V

    .line 979
    :cond_1e
    return-void
.end method

.method public final b(Lp/g;)V
    .registers 2
    .parameter

    .prologue
    .line 1033
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1034
    invoke-virtual {p0, p1}, Lt/q;->a(Lp/g;)V

    .line 1035
    return-void
.end method

.method public final b(Lp/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 936
    invoke-direct {p0}, Lt/q;->ax()V

    .line 937
    invoke-virtual {p0}, Lt/q;->T()Z

    move-result v0

    if-nez v0, :cond_24

    .line 939
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v1

    invoke-virtual {v1}, Lp/y;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->b(Lp/j;II)V

    .line 943
    :cond_24
    return-void
.end method

.method public b(Lp/y;[Lp/y;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1001
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1002
    invoke-virtual {p1}, Lp/y;->i()Z

    move-result v0

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 1003
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v0

    invoke-virtual {v0}, Lp/J;->i()I

    move-result v0

    .line 1004
    invoke-virtual {p1}, Lp/y;->k()I

    move-result v1

    if-le v1, v0, :cond_34

    .line 1006
    invoke-virtual {p1, v0}, Lp/y;->a(I)Lp/J;

    move-result-object v0

    invoke-virtual {p0, v0}, Lt/q;->a(Lp/J;)V

    .line 1017
    :goto_30
    invoke-virtual {p0, p1, p2}, Lt/q;->a(Lp/y;[Lp/y;)V

    .line 1022
    return-void

    .line 1011
    :cond_34
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    goto :goto_30

    .line 1014
    :cond_3d
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lp/J;)V

    .line 1015
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->f()V

    goto :goto_30
.end method

.method protected b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1220
    if-eqz p1, :cond_a

    .line 1221
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->k:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 1223
    :cond_a
    return-void
.end method

.method protected final c(Lp/g;)I
    .registers 4
    .parameter

    .prologue
    .line 1304
    invoke-virtual {p1}, Lp/g;->k()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, Lp/g;->l()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1305
    :cond_c
    const v0, 0x7f0d00bc

    .line 1312
    :goto_f
    return v0

    .line 1306
    :cond_10
    invoke-virtual {p1}, Lp/g;->n()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1307
    const v0, 0x7f0d00bb

    goto :goto_f

    .line 1308
    :cond_1a
    invoke-virtual {p1}, Lp/g;->o()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_31

    invoke-virtual {p1}, Lp/g;->g_()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-virtual {p1}, Lp/g;->c()[Lp/y;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 1310
    const v0, 0x7f0d00c1

    goto :goto_f

    .line 1312
    :cond_31
    const v0, 0x7f0d00ba

    goto :goto_f
.end method

.method public c()V
    .registers 1

    .prologue
    .line 269
    return-void
.end method

.method protected c(I)V
    .registers 3
    .parameter

    .prologue
    .line 1798
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cP;->a(I)V

    .line 1799
    invoke-direct {p0}, Lt/q;->ar()V

    .line 1800
    return-void
.end method

.method public final c(Lcom/google/android/maps/driveabout/vector/d;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 550
    invoke-direct {p0}, Lt/q;->ax()V

    .line 551
    new-instance v0, Ll/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Ll/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 555
    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 556
    invoke-virtual {p0, p1}, Lt/q;->d(Lcom/google/android/maps/driveabout/vector/d;)V

    .line 557
    return-void
.end method

.method protected c(Lp/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1266
    invoke-virtual {p0}, Lt/q;->T()Z

    move-result v0

    if-nez v0, :cond_21

    .line 1267
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v1

    invoke-virtual {v1}, Lp/y;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Lp/j;II)V

    .line 1271
    :cond_21
    return-void
.end method

.method public final c(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 492
    invoke-direct {p0}, Lt/q;->ax()V

    .line 497
    new-instance v3, Ll/D;

    const-string v4, "mute"

    if-nez p1, :cond_3c

    move v0, v1

    :goto_c
    invoke-direct {v3, v4, v0}, Ll/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v3}, LB/f;->b(LB/j;)V

    .line 499
    const-string v0, "u"

    if-nez p1, :cond_3e

    :goto_16
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 501
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/a;->c(Z)V

    .line 502
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->o()V

    .line 503
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cN;->b(Z)V

    .line 504
    invoke-direct {p0}, Lt/q;->aj()V

    .line 505
    return-void

    :cond_3c
    move v0, v2

    .line 497
    goto :goto_c

    :cond_3e
    move v1, v2

    .line 499
    goto :goto_16
.end method

.method public d()V
    .registers 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lt/q;->e()V

    .line 250
    return-void
.end method

.method protected d(Lcom/google/android/maps/driveabout/vector/d;)V
    .registers 5
    .parameter

    .prologue
    .line 1204
    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/cr;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 1209
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->G()V

    .line 1210
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v0

    .line 1211
    invoke-virtual {v0}, Lp/y;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lp/y;->a(I)Lp/J;

    move-result-object v0

    invoke-virtual {p0, v0}, Lt/q;->a(Lp/J;)V

    .line 1212
    invoke-virtual {p0}, Lt/q;->ac()V

    .line 1217
    :cond_33
    :goto_33
    return-void

    .line 1213
    :cond_34
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/bS;

    if-eqz v0, :cond_33

    .line 1214
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    check-cast p1, Lcom/google/android/maps/driveabout/vector/bS;

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/bS;->m()Lo/R;

    move-result-object v2

    invoke-virtual {v2}, Lo/R;->a()Lo/m;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bo;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cN;Lo/m;)V

    goto :goto_33
.end method

.method public final d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 707
    invoke-direct {p0}, Lt/q;->ax()V

    .line 708
    new-instance v0, Ll/D;

    const-string v1, "viewTraffic"

    invoke-direct {v0, v1, p1}, Ll/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 710
    const-string v0, "f"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 712
    invoke-virtual {p0, p1}, Lt/q;->b(Z)V

    .line 715
    return-void
.end method

.method public e()V
    .registers 1

    .prologue
    .line 261
    return-void
.end method

.method public final e(Z)V
    .registers 4
    .parameter

    .prologue
    .line 736
    invoke-direct {p0}, Lt/q;->ax()V

    .line 737
    new-instance v0, Ll/D;

    const-string v1, "viewSatellite"

    invoke-direct {v0, v1, p1}, Ll/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 739
    const-string v0, "S"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 740
    invoke-virtual {p0, p1}, Lt/q;->h(Z)V

    .line 741
    return-void
.end method

.method protected f()I
    .registers 2

    .prologue
    .line 1445
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v0

    invoke-virtual {v0}, Ln/b;->hasBearing()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1447
    :cond_1c
    const/4 v0, 0x2

    .line 1449
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method public final f(Z)V
    .registers 4
    .parameter

    .prologue
    .line 749
    invoke-direct {p0}, Lt/q;->ax()V

    .line 750
    new-instance v0, Ll/D;

    const-string v1, "viewBicycling"

    invoke-direct {v0, v1, p1}, Ll/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 752
    const-string v0, "b"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Z)V

    .line 753
    invoke-virtual {p0, p1}, Lt/q;->i(Z)V

    .line 754
    return-void
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 1146
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->f()V

    .line 1147
    return-void
.end method

.method public final g(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1044
    invoke-direct {p0}, Lt/q;->ax()V

    .line 1045
    const-string v0, ","

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;)V

    .line 1047
    invoke-virtual {p0}, Lt/q;->s()V

    .line 1048
    invoke-direct {p0}, Lt/q;->aw()V

    .line 1050
    if-eqz p1, :cond_1d

    .line 1051
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->h()V

    .line 1053
    :cond_1d
    return-void
.end method

.method protected h()V
    .registers 4

    .prologue
    .line 1154
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->i:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1155
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cn;->a(F)F

    .line 1156
    invoke-virtual {p0}, Lt/q;->aa()V

    .line 1158
    :cond_1d
    return-void
.end method

.method protected h(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1230
    if-eqz p1, :cond_32

    .line 1231
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->b:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1237
    :goto_d
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, Lp/P;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lu/q;->b(Landroid/content/Context;Ljava/lang/String;)Lu/s;

    move-result-object v0

    const-string v1, "SatelliteImagery"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lu/s;->a(Ljava/lang/String;Ljava/lang/Object;)Lu/s;

    .line 1240
    invoke-direct {p0}, Lt/q;->aw()V

    .line 1241
    return-void

    .line 1232
    :cond_32
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 1233
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_d

    .line 1235
    :cond_4a
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cP;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_d
.end method

.method protected i()V
    .registers 4

    .prologue
    .line 1165
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->i:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1166
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    const/high16 v1, -0x4080

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cn;->a(F)F

    .line 1167
    invoke-virtual {p0}, Lt/q;->aa()V

    .line 1169
    :cond_1d
    return-void
.end method

.method protected i(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1244
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cP;->b(Z)V

    .line 1245
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, Lp/P;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lu/q;->b(Landroid/content/Context;Ljava/lang/String;)Lu/s;

    move-result-object v0

    const-string v1, "BicyclingLayer"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lu/s;->a(Ljava/lang/String;Ljava/lang/Object;)Lu/s;

    .line 1248
    invoke-direct {p0}, Lt/q;->aw()V

    .line 1249
    return-void
.end method

.method protected j()Z
    .registers 2

    .prologue
    .line 1172
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->i()Z

    move-result v0

    return v0
.end method

.method protected k()V
    .registers 4

    .prologue
    .line 1226
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 1227
    return-void
.end method

.method protected l()V
    .registers 1

    .prologue
    .line 1284
    return-void
.end method

.method protected m()V
    .registers 1

    .prologue
    .line 1662
    return-void
.end method

.method protected n()V
    .registers 1

    .prologue
    .line 1323
    return-void
.end method

.method protected o()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1792
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->l:Lt/v;

    invoke-virtual {v0, v1}, Lt/n;->a(Lt/v;)Lt/q;

    move-result-object v0

    check-cast v0, Lt/d;

    invoke-virtual {v0, v2}, Lt/d;->a_(Z)V

    .line 1794
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->l:Lt/v;

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 1795
    return-void
.end method

.method public p()V
    .registers 1

    .prologue
    .line 204
    return-void
.end method

.method public q()V
    .registers 1

    .prologue
    .line 210
    return-void
.end method

.method protected r()V
    .registers 2

    .prologue
    .line 1252
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->w()V

    .line 1253
    return-void
.end method

.method protected s()V
    .registers 1

    .prologue
    .line 1317
    return-void
.end method

.method protected t()V
    .registers 1

    .prologue
    .line 1320
    return-void
.end method

.method protected u()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1498
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->y()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1499
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0064

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;Z)V

    .line 1513
    :goto_24
    return-void

    .line 1501
    :cond_25
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v0

    if-nez v0, :cond_3d

    .line 1502
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;Z)V

    goto :goto_24

    .line 1504
    :cond_3d
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v0

    if-nez v0, :cond_55

    .line 1505
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->a(Ljava/lang/String;Z)V

    goto :goto_24

    .line 1507
    :cond_55
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    iget-object v2, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v2}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v2

    invoke-direct {p0}, Lt/q;->av()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cN;->a(Lcom/google/android/maps/driveabout/app/aQ;Lp/J;ZZ)V

    goto :goto_24
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 1687
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setTrafficMode(I)V

    .line 1689
    return-void
.end method

.method protected w()V
    .registers 6

    .prologue
    .line 1713
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    .line 1714
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, -0x1

    :goto_13
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/cN;->setTimeRemaining(IZIZ)V

    .line 1718
    return-void

    .line 1714
    :cond_20
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v0

    goto :goto_13
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 1521
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-direct {p0}, Lt/q;->ap()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 1522
    return-void
.end method

.method public y()V
    .registers 4

    .prologue
    .line 1751
    iget-object v0, p0, Lt/q;->a:Lt/n;

    sget-object v1, Lt/v;->f:Lt/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1752
    iget-object v0, p0, Lt/q;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->g()Lt/q;

    move-result-object v0

    invoke-virtual {v0}, Lt/q;->y()V

    .line 1754
    :cond_14
    return-void
.end method

.method public z()V
    .registers 1

    .prologue
    .line 1762
    return-void
.end method
