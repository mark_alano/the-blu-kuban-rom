.class Lu/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:Lu/d;


# direct methods
.method private constructor <init>(Lu/d;)V
    .registers 2
    .parameter

    .prologue
    .line 982
    iput-object p1, p0, Lu/f;->a:Lu/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lu/d;Lu/e;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 982
    invoke-direct {p0, p1}, Lu/f;-><init>(Lu/d;)V

    return-void
.end method


# virtual methods
.method public a(Lo/aq;ILo/ap;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 986
    const/4 v0, 0x3

    if-ne p2, v0, :cond_4

    .line 1001
    :cond_3
    :goto_3
    return-void

    .line 990
    :cond_4
    iget-object v0, p0, Lu/f;->a:Lu/d;

    invoke-static {v0, p1, p2, p3}, Lu/d;->a(Lu/d;Lo/aq;ILo/ap;)LF/T;

    move-result-object v1

    .line 992
    if-eqz v1, :cond_3

    .line 993
    iget-object v0, p0, Lu/f;->a:Lu/d;

    invoke-static {v0}, Lu/d;->c(Lu/d;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 994
    if-nez v0, :cond_33

    .line 995
    const-string v0, "TileFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received an unknown tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 998
    :cond_33
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 999
    iget-object v0, p0, Lu/f;->a:Lu/d;

    invoke-static {v0, p1, v1, v2, v3}, Lu/d;->a(Lu/d;Lo/aq;LF/T;J)V

    goto :goto_3
.end method
