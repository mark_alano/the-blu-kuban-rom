.class Lu/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:Lu/d;


# direct methods
.method private constructor <init>(Lu/d;)V
    .registers 2
    .parameter

    .prologue
    .line 1016
    iput-object p1, p0, Lu/g;->a:Lu/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lu/d;Lu/e;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1016
    invoke-direct {p0, p1}, Lu/g;-><init>(Lu/d;)V

    return-void
.end method

.method private a(Lu/c;)LF/T;
    .registers 8
    .parameter

    .prologue
    .line 1027
    iget-object v0, p0, Lu/g;->a:Lu/d;

    iget-object v1, p1, Lu/c;->a:Lo/aq;

    invoke-static {v0, v1}, Lu/d;->a(Lu/d;Lo/aq;)LF/T;

    move-result-object v1

    .line 1028
    if-eqz v1, :cond_1f

    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->d(Lu/d;)Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v1, v0}, LF/T;->a(Lcom/google/googlenav/common/a;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1030
    iget-object v0, p0, Lu/g;->a:Lu/d;

    iget-object v2, p1, Lu/c;->a:Lo/aq;

    invoke-static {v0, v2, v1}, Lu/d;->a(Lu/d;Lo/aq;LF/T;)Z

    move-object v0, v1

    .line 1082
    :goto_1e
    return-object v0

    .line 1031
    :cond_1f
    if-nez v1, :cond_a7

    .line 1032
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p1, Lu/c;->a:Lo/aq;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1034
    if-eqz v0, :cond_62

    .line 1040
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 1055
    :cond_37
    iget-boolean v1, p1, Lu/c;->b:Z

    if-eqz v1, :cond_50

    .line 1058
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Lu/c;->a:Lo/aq;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1059
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->f(Lu/d;)I

    .line 1067
    :goto_4b
    invoke-static {}, Lu/d;->l()LF/T;

    move-result-object v0

    goto :goto_1e

    .line 1063
    :cond_50
    iget-object v1, p0, Lu/g;->a:Lu/d;

    invoke-static {v1}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p1, Lu/c;->a:Lo/aq;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4b

    .line 1070
    :cond_62
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->g(Lu/d;)Lu/a;

    move-result-object v2

    monitor-enter v2

    .line 1073
    :try_start_69
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->g(Lu/d;)Lu/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lu/a;->a(Lu/c;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 1074
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p1, Lu/c;->a:Lo/aq;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->h(Lu/d;)I

    .line 1077
    iget-object v0, p0, Lu/g;->a:Lu/d;

    iget-object v3, p1, Lu/c;->a:Lo/aq;

    iget-boolean v4, p1, Lu/c;->b:Z

    iget-object v5, p0, Lu/g;->a:Lu/d;

    invoke-static {v5}, Lu/d;->i(Lu/d;)Ls/e;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lu/d;->a(Lu/d;Lo/aq;ZLs/e;)V

    .line 1079
    :cond_a0
    monitor-exit v2

    move-object v0, v1

    goto/16 :goto_1e

    :catchall_a4
    move-exception v0

    monitor-exit v2
    :try_end_a6
    .catchall {:try_start_69 .. :try_end_a6} :catchall_a4

    throw v0

    :cond_a7
    move-object v0, v1

    goto/16 :goto_1e
.end method

.method private a(Lu/c;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1091
    :goto_0
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->g(Lu/d;)Lu/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lu/a;->a(Lu/c;Z)Lu/c;

    move-result-object p1

    .line 1092
    if-nez p1, :cond_d

    .line 1112
    :cond_c
    return-void

    .line 1101
    :cond_d
    invoke-direct {p0, p1}, Lu/g;->a(Lu/c;)LF/T;

    move-result-object v0

    .line 1105
    if-eqz v0, :cond_c

    .line 1110
    invoke-static {}, Lu/d;->l()LF/T;

    move-result-object v1

    if-eq v0, v1, :cond_1b

    const/4 p2, 0x1

    goto :goto_0

    :cond_1b
    const/4 p2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/aq;ILo/ap;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1117
    sget-object v0, Lr/z;->h:Lo/aq;

    if-ne p1, v0, :cond_22

    .line 1119
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->g(Lu/d;)Lu/a;

    move-result-object v1

    monitor-enter v1

    .line 1120
    :try_start_e
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->j(Lu/d;)Lu/c;

    move-result-object v0

    .line 1121
    iget-object v3, p0, Lu/g;->a:Lu/d;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lu/d;->a(Lu/d;Lu/c;)Lu/c;

    .line 1122
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_e .. :try_end_1b} :catchall_1f

    .line 1124
    invoke-direct {p0, v0, v2}, Lu/g;->a(Lu/c;Z)V

    .line 1170
    :cond_1e
    :goto_1e
    return-void

    .line 1122
    :catchall_1f
    move-exception v0

    :try_start_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    throw v0

    .line 1129
    :cond_22
    iget-object v0, p0, Lu/g;->a:Lu/d;

    invoke-static {v0}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1130
    if-nez v0, :cond_49

    .line 1131
    const-string v0, "TileFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received an unknown tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 1140
    :cond_49
    iget-object v1, p0, Lu/g;->a:Lu/d;

    invoke-static {v1}, Lu/d;->g(Lu/d;)Lu/a;

    move-result-object v5

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lu/c;

    invoke-virtual {v5, v1}, Lu/a;->a(Lu/c;)Z

    move-result v1

    if-nez v1, :cond_8a

    move v1, v2

    move v2, v3

    .line 1157
    :goto_5b
    if-eqz v1, :cond_6b

    .line 1158
    iget-object v1, p0, Lu/g;->a:Lu/d;

    invoke-static {v1}, Lu/d;->e(Lu/d;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1159
    iget-object v1, p0, Lu/g;->a:Lu/d;

    invoke-static {v1}, Lu/d;->f(Lu/d;)I

    .line 1162
    :cond_6b
    if-eqz v2, :cond_74

    .line 1163
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lu/c;

    invoke-direct {p0, v1, v3}, Lu/g;->a(Lu/c;Z)V

    .line 1166
    :cond_74
    if-eqz v4, :cond_1e

    .line 1167
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v0, v1, v5

    .line 1168
    iget-object v2, p0, Lu/g;->a:Lu/d;

    invoke-static {v2, p1, v4, v0, v1}, Lu/d;->a(Lu/d;Lo/aq;LF/T;J)V

    goto :goto_1e

    .line 1145
    :cond_8a
    const/4 v1, 0x3

    if-ne p2, v1, :cond_94

    .line 1147
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lu/c;

    iget-boolean v1, v1, Lu/c;->b:Z

    goto :goto_5b

    .line 1151
    :cond_94
    iget-object v1, p0, Lu/g;->a:Lu/d;

    invoke-static {v1, p1, p2, p3}, Lu/d;->a(Lu/d;Lo/aq;ILo/ap;)LF/T;

    move-result-object v4

    .line 1154
    if-eqz v4, :cond_a6

    invoke-static {}, Lu/d;->l()LF/T;

    move-result-object v1

    if-eq v4, v1, :cond_a6

    move v1, v2

    :goto_a3
    move v3, v1

    move v1, v2

    goto :goto_5b

    :cond_a6
    move v1, v3

    goto :goto_a3
.end method
