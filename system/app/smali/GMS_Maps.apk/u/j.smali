.class public Lu/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lad/q;


# instance fields
.field private volatile a:I

.field private final b:Lad/h;


# direct methods
.method public constructor <init>(Lad/h;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lu/j;->a:I

    .line 35
    iput-object p1, p0, Lu/j;->b:Lad/h;

    .line 36
    return-void
.end method

.method private b()V
    .registers 8

    .prologue
    .line 80
    new-instance v0, LB/i;

    iget v1, p0, Lu/j;->a:I

    iget-object v2, p0, Lu/j;->b:Lad/h;

    invoke-virtual {v2}, Lad/h;->r()I

    move-result v2

    iget-object v3, p0, Lu/j;->b:Lad/h;

    invoke-virtual {v3}, Lad/h;->s()I

    move-result v3

    iget-object v4, p0, Lu/j;->b:Lad/h;

    invoke-virtual {v4}, Lad/h;->o()J

    move-result-wide v4

    iget-object v6, p0, Lu/j;->b:Lad/h;

    invoke-virtual {v6}, Lad/h;->t()I

    move-result v6

    invoke-direct/range {v0 .. v6}, LB/i;-><init>(IIIJI)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 86
    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 72
    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    new-instance v0, LB/h;

    invoke-direct {v0, p1, p2}, LB/h;-><init>(IZ)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 66
    invoke-direct {p0}, Lu/j;->b()V

    .line 67
    return-void
.end method

.method public a(Lad/g;)V
    .registers 4
    .parameter

    .prologue
    .line 40
    new-instance v0, LB/g;

    const-string v1, "onComplete"

    invoke-direct {v0, v1, p1}, LB/g;-><init>(Ljava/lang/String;Lad/g;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 42
    iget v0, p0, Lu/j;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/j;->a:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_17

    .line 43
    invoke-direct {p0}, Lu/j;->b()V

    .line 59
    :cond_17
    return-void
.end method

.method public b(Lad/g;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    return-void
.end method
