.class public final enum Ly/m;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ly/m;

.field public static final enum b:Ly/m;

.field public static final enum c:Ly/m;

.field private static final synthetic d:[Ly/m;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Ly/m;

    const-string v1, "EMPTY_MESH"

    invoke-direct {v0, v1, v2}, Ly/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ly/m;->a:Ly/m;

    new-instance v0, Ly/m;

    const-string v1, "TESSELLATE_MONOTONE"

    invoke-direct {v0, v1, v3}, Ly/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ly/m;->b:Ly/m;

    new-instance v0, Ly/m;

    const-string v1, "CUT_AND_TESSELLATE"

    invoke-direct {v0, v1, v4}, Ly/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ly/m;->c:Ly/m;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Ly/m;

    sget-object v1, Ly/m;->a:Ly/m;

    aput-object v1, v0, v2

    sget-object v1, Ly/m;->b:Ly/m;

    aput-object v1, v0, v3

    sget-object v1, Ly/m;->c:Ly/m;

    aput-object v1, v0, v4

    sput-object v0, Ly/m;->d:[Ly/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ly/m;
    .registers 2
    .parameter

    .prologue
    .line 72
    const-class v0, Ly/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ly/m;

    return-object v0
.end method

.method public static values()[Ly/m;
    .registers 1

    .prologue
    .line 72
    sget-object v0, Ly/m;->d:[Ly/m;

    invoke-virtual {v0}, [Ly/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ly/m;

    return-object v0
.end method
