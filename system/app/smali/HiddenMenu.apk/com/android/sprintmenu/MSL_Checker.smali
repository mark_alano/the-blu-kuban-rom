.class public Lcom/android/sprintmenu/MSL_Checker;
.super Landroid/app/Activity;
.source "MSL_Checker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field public static mPhone:Lcom/android/internal/telephony/Phone;


# instance fields
.field displayDialog:Z

.field eroorMsg:Ljava/lang/String;

.field flag_HFA:Z

.field flag_Restore:Z

.field i:Landroid/content/Intent;

.field keyString:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mInputPwd:Landroid/widget/EditText;

.field userInput:Ljava/lang/String;

.field private visilePasswordsInitialStatus:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 32
    const-class v0, Lcom/android/sprintmenu/MSL_Checker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/android/sprintmenu/MSL_Checker;->mPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/String;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->userInput:Ljava/lang/String;

    .line 54
    iput-boolean v2, p0, Lcom/android/sprintmenu/MSL_Checker;->visilePasswordsInitialStatus:Z

    .line 58
    iput-boolean v2, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 59
    iput-boolean v2, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_Restore:Z

    .line 60
    iput-boolean v2, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_HFA:Z

    .line 264
    new-instance v0, Lcom/android/sprintmenu/MSL_Checker$1;

    invoke-direct {v0, p0}, Lcom/android/sprintmenu/MSL_Checker$1;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    iput-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .prologue
    .line 31
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private checkMSLCode()V
    .registers 9

    .prologue
    .line 64
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 65
    .local v0, bos:Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 69
    .local v1, dos:Ljava/io/DataOutputStream;
    const/4 v3, 0x3

    .line 70
    .local v3, fileSize:I
    const/16 v4, 0x51

    :try_start_d
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 71
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 72
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_18} :catch_2a

    .line 78
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/android/sprintmenu/MSL_Checker;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x7ce

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 79
    :goto_29
    return-void

    .line 74
    :catch_2a
    move-exception v2

    .line 76
    .local v2, e:Ljava/io/IOException;
    goto :goto_29
.end method

.method private checkSPRINTCode(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 87
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v1, "checkSPRINTCode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->userInput:Ljava/lang/String;

    const-string v1, "777468"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 91
    const-string v0, "DEBUG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 92
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v1, "User Input Ok, Launching DEBUG Screen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    .line 94
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/DEBUGMENU;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->initiateActivity()V

    .line 111
    :cond_33
    :goto_33
    return-void

    .line 96
    :cond_34
    const-string v0, "CLEAR_Reset"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 97
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/sprintmenu/MSL_Checker;->showDialog(I)V

    goto :goto_33

    .line 103
    :cond_41
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v1, "User Input Incorrect, Displaying Error Toast "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-string v0, "Invalid Lock Code!"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_33
.end method


# virtual methods
.method initiateActivity()V
    .registers 3

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    if-eqz v0, :cond_15

    .line 247
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Restore"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/RestoreNAI;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 255
    :cond_15
    :goto_15
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 256
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/sprintmenu/MSL_Checker;->startActivity(Landroid/content/Intent;)V

    .line 257
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "CLEAR_Reset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 258
    const v0, 0x7f0500ca

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 260
    :cond_36
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->finish()V

    .line 261
    return-void

    .line 249
    :cond_3a
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Enable_HFA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/Enable_HFA;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_15

    .line 251
    :cond_4c
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "RTN_Reset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/RTN_Reset;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_15

    .line 253
    :cond_5e
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "CLEAR_Reset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/CLEAR_Reset;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_15
.end method

.method public notifyResult(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 118
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MSL result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->userInput:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e6

    .line 124
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    .line 125
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "DEBUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 127
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/DEBUGMENU;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 235
    :cond_42
    :goto_42
    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    if-nez v0, :cond_49

    .line 236
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->initiateActivity()V

    .line 242
    :cond_49
    :goto_49
    return-void

    .line 131
    :cond_4a
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "AKEY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 133
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/AKEY2;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_42

    .line 137
    :cond_63
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "GPSCLRX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 139
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/GPSCLRX;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_42

    .line 143
    :cond_7c
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "SCRTN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 145
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/SCRTN;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_42

    .line 149
    :cond_95
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Advanced"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 151
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/TerminalMode;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 154
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-string v1, "keyString"

    const-string v2, "DATA_ADV"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_42

    .line 157
    :cond_b7
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "UsernameEdit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 159
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/Username_Edit;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 163
    :cond_d1
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "DSA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 165
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-string v1, "com.android.sprintmenu"

    const-string v2, "com.android.sprintmenu.DSA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 170
    :cond_ed
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "MMSC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_107

    .line 172
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/MMSC;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 176
    :cond_107
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Restore"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_121

    .line 178
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_Restore:Z

    .line 180
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 181
    invoke-virtual {p0, v3}, Lcom/android/sprintmenu/MSL_Checker;->showDialog(I)V

    goto/16 :goto_42

    .line 184
    :cond_121
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Enable_HFA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13b

    .line 186
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_HFA:Z

    .line 188
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 189
    invoke-virtual {p0, v3}, Lcom/android/sprintmenu/MSL_Checker;->showDialog(I)V

    goto/16 :goto_42

    .line 192
    :cond_13b
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "RTN_Reset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_153

    .line 194
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 196
    invoke-virtual {p0, v3}, Lcom/android/sprintmenu/MSL_Checker;->showDialog(I)V

    goto/16 :goto_42

    .line 199
    :cond_153
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Multimedia"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16f

    .line 201
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-string v1, "com.android.sprintmenu"

    const-string v2, "com.android.sprintmenu.Multimedia"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 206
    :cond_16f
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "Work_Mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18b

    .line 208
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-string v1, "com.android.sprintmenu"

    const-string v2, "com.android.sprintmenu.WORK_Mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 212
    :cond_18b
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "WiMAX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b3

    .line 214
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 216
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.provider.Telephony.SECRET_CODE"

    const-string v2, "android_secret_code://WiMAX_CFG"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/android/sprintmenu/MSL_Checker;->sendBroadcast(Landroid/content/Intent;)V

    .line 218
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->finish()V

    goto/16 :goto_42

    .line 221
    :cond_1b3
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "hdata_edit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1cd

    .line 223
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->i:Landroid/content/Intent;

    const-class v1, Lcom/android/sprintmenu/hdata_edit;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_42

    .line 227
    :cond_1cd
    iget-object v0, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v1, "CLEAR_Reset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 229
    sget-object v0, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iput-boolean v3, p0, Lcom/android/sprintmenu/MSL_Checker;->displayDialog:Z

    .line 231
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/sprintmenu/MSL_Checker;->showDialog(I)V

    goto/16 :goto_42

    .line 241
    :cond_1e6
    const-string v0, "Invalid Lock Code!"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_49
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    .prologue
    .line 452
    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->mInputPwd:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, str:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_4e

    .line 484
    :goto_11
    return-void

    .line 458
    :sswitch_12
    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->mInputPwd:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->userInput:Ljava/lang/String;

    .line 461
    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v2, "DEBUG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v2, "CLEAR_Reset"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 464
    :cond_32
    sget-object v1, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Checking SPRINT for DEBUG"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/sprintmenu/MSL_Checker;->checkSPRINTCode(Ljava/lang/String;)V

    goto :goto_11

    .line 469
    :cond_3f
    sget-object v1, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Checking for MSL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-direct {p0}, Lcom/android/sprintmenu/MSL_Checker;->checkMSLCode()V

    goto :goto_11

    .line 480
    :sswitch_4a
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->finish()V

    goto :goto_11

    .line 455
    :sswitch_data_4e
    .sparse-switch
        0x7f070003 -> :sswitch_4a
        0x7f070009 -> :sswitch_12
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    .line 309
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 311
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    sput-object v4, Lcom/android/sprintmenu/MSL_Checker;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 314
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onCreate1"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 317
    .local v1, intent:Landroid/content/Intent;
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onCreate2"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const-string v4, "keyString"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    .line 319
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v4, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v5, "DEBUG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_45

    iget-object v4, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    const-string v5, "CLEAR_Reset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_96

    .line 326
    :cond_45
    const-string v4, "Check Lock Code"

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->setTitle(Ljava/lang/CharSequence;)V

    .line 327
    const v4, 0x7f030007

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->setContentView(I)V

    .line 328
    const v4, 0x7f07000b

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 329
    .local v3, titleText:Landroid/widget/TextView;
    const v4, 0x7f0500c9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 336
    .end local v3           #titleText:Landroid/widget/TextView;
    :goto_5f
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const v4, 0x7f070008

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/sprintmenu/MSL_Checker;->mInputPwd:Landroid/widget/EditText;

    .line 340
    iget-object v4, p0, Lcom/android/sprintmenu/MSL_Checker;->mInputPwd:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 342
    sget-object v4, Lcom/android/sprintmenu/MSL_Checker;->LOG_TAG:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/sprintmenu/MSL_Checker;->keyString:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const v4, 0x7f070009

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 344
    .local v2, ok_button:Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 345
    const v4, 0x7f070003

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 346
    .local v0, cancel_button:Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    return-void

    .line 333
    .end local v0           #cancel_button:Landroid/view/View;
    .end local v2           #ok_button:Landroid/view/View;
    :cond_96
    const v4, 0x7f030005

    invoke-virtual {p0, v4}, Lcom/android/sprintmenu/MSL_Checker;->setContentView(I)V

    goto :goto_5f
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 6
    .parameter "id"

    .prologue
    const v3, 0x7f05003e

    const v2, 0x7f05003d

    .line 353
    packed-switch p1, :pswitch_data_9c

    .line 429
    const/4 v0, 0x0

    :goto_a
    return-object v0

    .line 357
    :pswitch_b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0500c4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "WARNING: Do you want to reset your phone\'s network settings and still retain your personal information?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/sprintmenu/MSL_Checker$3;

    invoke-direct {v1, p0}, Lcom/android/sprintmenu/MSL_Checker$3;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/sprintmenu/MSL_Checker$2;

    invoke-direct {v1, p0}, Lcom/android/sprintmenu/MSL_Checker$2;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_a

    .line 385
    :pswitch_34
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_Restore:Z

    if-eqz v0, :cond_65

    const v0, 0x7f05003c

    :goto_40
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_Restore:Z

    if-eqz v0, :cond_71

    const-string v0, "This phone will be restarted."

    :goto_4a
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/sprintmenu/MSL_Checker$5;

    invoke-direct {v1, p0}, Lcom/android/sprintmenu/MSL_Checker$5;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/sprintmenu/MSL_Checker$4;

    invoke-direct {v1, p0}, Lcom/android/sprintmenu/MSL_Checker$4;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_a

    :cond_65
    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_HFA:Z

    if-eqz v0, :cond_6d

    const v0, 0x7f05003f

    goto :goto_40

    :cond_6d
    const v0, 0x7f050008

    goto :goto_40

    :cond_71
    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->flag_HFA:Z

    if-eqz v0, :cond_78

    const-string v0, "If you select \'Yes\', this phone will be restarted."

    goto :goto_4a

    :cond_78
    const-string v0, "Reset the phone to manufacture\'s default?"

    goto :goto_4a

    .line 412
    :pswitch_7b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f05001b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sprintmenu/MSL_Checker;->eroorMsg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/sprintmenu/MSL_Checker$6;

    invoke-direct {v1, p0}, Lcom/android/sprintmenu/MSL_Checker$6;-><init>(Lcom/android/sprintmenu/MSL_Checker;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_a

    .line 353
    :pswitch_data_9c
    .packed-switch 0x0
        :pswitch_7b
        :pswitch_34
        :pswitch_b
    .end packed-switch
.end method

.method protected onPause()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 444
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 445
    iget-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->visilePasswordsInitialStatus:Z

    if-ne v0, v2, :cond_11

    .line 446
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "show_password"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 448
    :cond_11
    return-void
.end method

.method protected onResume()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 433
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 436
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "show_password"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1f

    :goto_11
    iput-boolean v0, p0, Lcom/android/sprintmenu/MSL_Checker;->visilePasswordsInitialStatus:Z

    if-eqz v0, :cond_1e

    .line 438
    invoke-virtual {p0}, Lcom/android/sprintmenu/MSL_Checker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "show_password"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 440
    :cond_1e
    return-void

    :cond_1f
    move v0, v1

    .line 436
    goto :goto_11
.end method
