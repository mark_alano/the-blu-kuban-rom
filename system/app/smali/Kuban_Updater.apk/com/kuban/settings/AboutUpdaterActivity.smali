.class public Lcom/kuban/settings/AboutUpdaterActivity;
.super Landroid/app/Activity;
.source "AboutUpdaterActivity.java"


# static fields
.field public static final KEY_NAME:Ljava/lang/String; = "Name"


# instance fields
.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 23
    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lcom/kuban/settings/AboutUpdaterActivity;->setTitle(I)V

    .line 24
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/kuban/settings/AboutUpdaterActivity;->setContentView(I)V

    .line 25
    const v0, 0x7f0c0046

    invoke-virtual {p0, v0}, Lcom/kuban/settings/AboutUpdaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/kuban/settings/AboutUpdaterActivity;->webView:Landroid/webkit/WebView;

    .line 26
    iget-object v0, p0, Lcom/kuban/settings/AboutUpdaterActivity;->webView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/info.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 32
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 33
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 38
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 39
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 40
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 41
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 42
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 43
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 110
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 47
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 48
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v2, in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 51
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 54
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 55
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 58
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 61
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_92

    .line 62
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 65
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 68
    .end local v2           #in:Landroid/content/Intent;
    :cond_92
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_b5

    .line 69
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 72
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 75
    .end local v2           #in:Landroid/content/Intent;
    :cond_b5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_d8

    .line 76
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 79
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 82
    .end local v2           #in:Landroid/content/Intent;
    :cond_d8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_fb

    .line 83
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 86
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 89
    .end local v2           #in:Landroid/content/Intent;
    :cond_fb
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_11e

    .line 90
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 93
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 96
    .end local v2           #in:Landroid/content/Intent;
    :cond_11e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_141

    .line 97
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 100
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 103
    .end local v2           #in:Landroid/content/Intent;
    :cond_141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_162

    .line 104
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0}, Lcom/kuban/settings/AboutUpdaterActivity;->finish()V

    .line 107
    invoke-virtual {p0, v2}, Lcom/kuban/settings/AboutUpdaterActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 110
    .end local v2           #in:Landroid/content/Intent;
    :cond_162
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
