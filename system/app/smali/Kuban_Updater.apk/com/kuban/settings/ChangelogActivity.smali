.class public Lcom/kuban/settings/ChangelogActivity;
.super Landroid/app/Activity;
.source "ChangelogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/ChangelogActivity$HomeAsync;
    }
.end annotation


# static fields
.field public static final KEY_CHANGE:Ljava/lang/String; = "Changelog"

.field public static final KEY_DATE:Ljava/lang/String; = "Date"

.field public static final KEY_ITEM:Ljava/lang/String; = "Element"

.field public static final KEY_NAME:Ljava/lang/String; = "Name"


# instance fields
.field URL:Ljava/lang/String;

.field adapter:Lcom/kuban/settings/adapters/ChangelogAdapter;

.field list:Landroid/widget/ListView;

.field final menuItems2:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/kuban/settings/ChangelogActivity;->URL:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/ChangelogActivity;->menuItems2:Ljava/util/ArrayList;

    .line 28
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 41
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ChangelogActivity;->setContentView(I)V

    .line 42
    const v0, 0x7f0c0023

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ChangelogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kuban/settings/ChangelogActivity;->list:Landroid/widget/ListView;

    .line 43
    new-instance v0, Lcom/kuban/settings/adapters/ChangelogAdapter;

    iget-object v1, p0, Lcom/kuban/settings/ChangelogActivity;->menuItems2:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/kuban/settings/adapters/ChangelogAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/kuban/settings/ChangelogActivity;->adapter:Lcom/kuban/settings/adapters/ChangelogAdapter;

    .line 44
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kuban/settings/ChangelogActivity;->adapter:Lcom/kuban/settings/adapters/ChangelogAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 45
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 48
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ChangelogActivity;->requestWindowFeature(I)Z

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ChangelogActivity;->setContentView(I)V

    .line 51
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 52
    new-instance v0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/kuban/settings/ChangelogActivity$HomeAsync;-><init>(Lcom/kuban/settings/ChangelogActivity;Lcom/kuban/settings/ChangelogActivity$HomeAsync;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 53
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 93
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 100
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 101
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 102
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 103
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 104
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 167
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :cond_2b
    :goto_2b
    return v3

    .line 108
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 109
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v2, in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 112
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 115
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 116
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 117
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 119
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 122
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-eq v4, v5, :cond_2b

    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_9b

    .line 126
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 129
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 132
    .end local v2           #in:Landroid/content/Intent;
    :cond_9b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_be

    .line 133
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 136
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 139
    .end local v2           #in:Landroid/content/Intent;
    :cond_be
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_e1

    .line 140
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 141
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 143
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 146
    .end local v2           #in:Landroid/content/Intent;
    :cond_e1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_104

    .line 147
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 148
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 150
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 153
    .end local v2           #in:Landroid/content/Intent;
    :cond_104
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_127

    .line 154
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "Name"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 157
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 160
    .end local v2           #in:Landroid/content/Intent;
    :cond_127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_148

    .line 161
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0}, Lcom/kuban/settings/ChangelogActivity;->finish()V

    .line 164
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ChangelogActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 167
    .end local v2           #in:Landroid/content/Intent;
    :cond_148
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
