.class public Lcom/kuban/settings/CompatibleModsActivity;
.super Landroid/app/Dialog;
.source "CompatibleModsActivity.java"


# static fields
.field static final KEY_ITEM:Ljava/lang/String; = "Mod"

.field static final KEY_NAME:Ljava/lang/String; = "ModName"


# instance fields
.field adapter:Landroid/widget/SimpleAdapter;

.field private context:Landroid/content/Context;

.field private lv:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 29
    const v0, 0x7f0a0001

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 25
    iput-object v1, p0, Lcom/kuban/settings/CompatibleModsActivity;->lv:Landroid/widget/ListView;

    .line 26
    iput-object v1, p0, Lcom/kuban/settings/CompatibleModsActivity;->adapter:Landroid/widget/SimpleAdapter;

    .line 30
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/kuban/settings/CompatibleModsActivity;->setContentView(I)V

    .line 31
    iput-object p1, p0, Lcom/kuban/settings/CompatibleModsActivity;->context:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private getPriorityList()Ljava/util/List;
    .registers 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 46
    const-string v12, "kuban.updater.parts"

    invoke-static {v12}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, check:Ljava/lang/String;
    const/4 v0, 0x0

    .line 49
    .local v0, base:Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v6, items:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 51
    .local v10, url:Ljava/net/URL;
    invoke-virtual {v10}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v10}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x2f

    invoke-virtual {v14, v15}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 52
    .local v9, path:Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "://"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "mods_files.xml"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    new-instance v8, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v8}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 54
    .local v8, parser:Lcom/kuban/settings/utils/XMLParser;
    invoke-virtual {v8, v0}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 55
    .local v11, xml:Ljava/lang/String;
    invoke-virtual {v8, v11}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 56
    .local v2, doc:Lorg/w3c/dom/Document;
    const-string v12, "Mod"

    invoke-interface {v2, v12}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 57
    .local v7, nl:Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_67
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-lt v4, v12, :cond_6e

    .line 63
    return-object v6

    .line 58
    :cond_6e
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 59
    .local v5, iMaps:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    check-cast v3, Lorg/w3c/dom/Element;

    .line 60
    .local v3, e:Lorg/w3c/dom/Element;
    const-string v12, "ModName"

    const-string v13, "ModName"

    invoke-virtual {v8, v3, v13}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v4, v4, 0x1

    goto :goto_67
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 37
    new-array v4, v2, [Ljava/lang/String;

    const-string v0, "ModName"

    aput-object v0, v4, v1

    .line 38
    .local v4, from:[Ljava/lang/String;
    new-array v5, v2, [I

    const v0, 0x7f0c0014

    aput v0, v5, v1

    .line 40
    .local v5, to:[I
    :try_start_12
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v1, p0, Lcom/kuban/settings/CompatibleModsActivity;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/kuban/settings/CompatibleModsActivity;->getPriorityList()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030005

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/kuban/settings/CompatibleModsActivity;->adapter:Landroid/widget/SimpleAdapter;

    .line 41
    const v0, 0x7f0c0013

    invoke-virtual {p0, v0}, Lcom/kuban/settings/CompatibleModsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kuban/settings/CompatibleModsActivity;->lv:Landroid/widget/ListView;

    .line 42
    iget-object v0, p0, Lcom/kuban/settings/CompatibleModsActivity;->lv:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kuban/settings/CompatibleModsActivity;->adapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_34} :catch_35

    .line 44
    :goto_34
    return-void

    .line 43
    :catch_35
    move-exception v0

    goto :goto_34
.end method
