.class Lcom/kuban/settings/Home$HomeAsync;
.super Landroid/os/AsyncTask;
.source "Home.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/Home;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomeAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/Home;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/Home;)V
    .registers 2
    .parameter

    .prologue
    .line 220
    iput-object p1, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/Home;Lcom/kuban/settings/Home$HomeAsync;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/kuban/settings/Home$HomeAsync;-><init>(Lcom/kuban/settings/Home;)V

    return-void
.end method

.method static synthetic access$3(Lcom/kuban/settings/Home$HomeAsync;)Lcom/kuban/settings/Home;
    .registers 2
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/Home$HomeAsync;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 14
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 227
    const-string v10, "kuban.updater.parts"

    invoke-static {v10}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    .local v0, URL:Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 229
    .local v5, menuItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v7, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v7}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 230
    .local v7, parser:Lcom/kuban/settings/utils/XMLParser;
    invoke-virtual {v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 231
    .local v9, xml:Ljava/lang/String;
    invoke-virtual {v7, v9}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 232
    .local v1, doc:Lorg/w3c/dom/Document;
    const-string v10, "Tab"

    invoke-interface {v1, v10}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 234
    .local v6, nl:Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1f
    :try_start_1f
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-lt v3, v10, :cond_26

    .line 304
    :goto_25
    return-object v5

    .line 235
    :cond_26
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 236
    .local v4, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 237
    .local v2, e:Lorg/w3c/dom/Element;
    const-string v10, "Icon"

    invoke-virtual {v7, v2, v10}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 238
    .local v8, value:Ljava/lang/String;
    const-string v10, "1"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_46

    .line 239
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020021

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 241
    :cond_46
    const-string v10, "2"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_55

    .line 242
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002c

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 244
    :cond_55
    const-string v10, "3"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_64

    .line 245
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002e

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 247
    :cond_64
    const-string v10, "4"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_73

    .line 248
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002f

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 250
    :cond_73
    const-string v10, "5"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_82

    .line 251
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020030

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 253
    :cond_82
    const-string v10, "6"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_91

    .line 254
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020031

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 256
    :cond_91
    const-string v10, "7"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a0

    .line 257
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020032

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 259
    :cond_a0
    const-string v10, "8"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_af

    .line 260
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020033

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 262
    :cond_af
    const-string v10, "9"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_be

    .line 263
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020034

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 265
    :cond_be
    const-string v10, "10"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_cd

    .line 266
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020022

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 268
    :cond_cd
    const-string v10, "11"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_dc

    .line 269
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020023

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 271
    :cond_dc
    const-string v10, "12"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_eb

    .line 272
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020024

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 274
    :cond_eb
    const-string v10, "13"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_fa

    .line 275
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020025

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 277
    :cond_fa
    const-string v10, "14"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_109

    .line 278
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020026

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 280
    :cond_109
    const-string v10, "15"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_118

    .line 281
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020027

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 283
    :cond_118
    const-string v10, "16"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_127

    .line 284
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020028

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 286
    :cond_127
    const-string v10, "17"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_136

    .line 287
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f020029

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 289
    :cond_136
    const-string v10, "18"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_145

    .line 290
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002a

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 292
    :cond_145
    const-string v10, "19"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_154

    .line 293
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002b

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 295
    :cond_154
    const-string v10, "20"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_163

    .line 296
    iget-object v10, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v11, 0x7f02002d

    iput v11, v10, Lcom/kuban/settings/Home;->icon:I

    .line 298
    :cond_163
    const-string v10, "TabName"

    const-string v11, "TabName"

    invoke-virtual {v7, v2, v11}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    const-string v10, "Url"

    const-string v11, "Url"

    invoke-virtual {v7, v2, v11}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const-string v10, "Icon"

    iget-object v11, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    iget v11, v11, Lcom/kuban/settings/Home;->icon:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_189
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_189} :catch_18d

    .line 234
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1f

    .line 303
    .end local v2           #e:Lorg/w3c/dom/Element;
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8           #value:Ljava/lang/String;
    :catch_18d
    move-exception v10

    goto/16 :goto_25
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/Home$HomeAsync;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v6, 0x3

    const/4 v3, 0x0

    .line 308
    iget-object v0, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    invoke-virtual {v0, v3}, Lcom/kuban/settings/Home;->setProgressBarIndeterminateVisibility(Z)V

    .line 309
    iget-object v0, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const v1, 0x7f0c0022

    invoke-virtual {v0, v1}, Lcom/kuban/settings/Home;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/GridView;

    .line 310
    .local v7, gridView:Landroid/widget/GridView;
    iget-object v8, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    new-instance v0, Lcom/kuban/settings/Home$HomeAsync$1;

    iget-object v1, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    invoke-virtual {v1}, Lcom/kuban/settings/Home;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f030008

    .line 311
    new-array v5, v6, [Ljava/lang/String;

    const-string v1, "TabName"

    aput-object v1, v5, v3

    const/4 v1, 0x1

    const-string v3, "Url"

    aput-object v3, v5, v1

    const/4 v1, 0x2

    const-string v3, "Icon"

    aput-object v3, v5, v1

    .line 312
    new-array v6, v6, [I

    fill-array-data v6, :array_4c

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/Home$HomeAsync$1;-><init>(Lcom/kuban/settings/Home$HomeAsync;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 310
    iput-object v0, v8, Lcom/kuban/settings/Home;->adapter:Landroid/widget/SimpleAdapter;

    .line 319
    iget-object v0, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    iget-object v0, v0, Lcom/kuban/settings/Home;->adapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 320
    new-instance v0, Lcom/kuban/settings/Home$HomeAsync$2;

    invoke-direct {v0, p0}, Lcom/kuban/settings/Home$HomeAsync$2;-><init>(Lcom/kuban/settings/Home$HomeAsync;)V

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 332
    return-void

    .line 312
    nop

    :array_4c
    .array-data 0x4
        0x1at 0x0t 0xct 0x7ft
        0x1ct 0x0t 0xct 0x7ft
        0x19t 0x0t 0xct 0x7ft
    .end array-data
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 222
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 223
    iget-object v0, p0, Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/Home;->setProgressBarIndeterminateVisibility(Z)V

    .line 224
    return-void
.end method
