.class Lcom/kuban/settings/InstalledApplicationActivity$3;
.super Ljava/lang/Object;
.source "InstalledApplicationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/InstalledApplicationActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/InstalledApplicationActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/InstalledApplicationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/InstalledApplicationActivity$3;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "id"

    .prologue
    .line 199
    iget-object v2, p0, Lcom/kuban/settings/InstalledApplicationActivity$3;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v2}, Lcom/kuban/settings/InstalledApplicationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 201
    .local v0, i:Landroid/content/Intent;
    if-eqz v0, :cond_1a

    .line 202
    :try_start_14
    iget-object v2, p0, Lcom/kuban/settings/InstalledApplicationActivity$3;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v2, v0}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    .line 208
    :goto_19
    return-void

    .line 204
    :cond_1a
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_27} :catch_2e

    .line 205
    .end local v0           #i:Landroid/content/Intent;
    .local v1, i:Landroid/content/Intent;
    :try_start_27
    iget-object v2, p0, Lcom/kuban/settings/InstalledApplicationActivity$3;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v2, v1}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2c} :catch_30

    move-object v0, v1

    .end local v1           #i:Landroid/content/Intent;
    .restart local v0       #i:Landroid/content/Intent;
    goto :goto_19

    .line 207
    :catch_2e
    move-exception v2

    goto :goto_19

    .end local v0           #i:Landroid/content/Intent;
    .restart local v1       #i:Landroid/content/Intent;
    :catch_30
    move-exception v2

    move-object v0, v1

    .end local v1           #i:Landroid/content/Intent;
    .restart local v0       #i:Landroid/content/Intent;
    goto :goto_19
.end method
