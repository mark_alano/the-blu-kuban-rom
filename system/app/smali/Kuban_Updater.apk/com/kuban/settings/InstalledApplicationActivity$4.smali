.class Lcom/kuban/settings/InstalledApplicationActivity$4;
.super Ljava/lang/Object;
.source "InstalledApplicationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/InstalledApplicationActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/InstalledApplicationActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/InstalledApplicationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/InstalledApplicationActivity$4;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 14
    .parameter "dialog"
    .parameter "id"

    .prologue
    .line 212
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/kuban/settings/utils/AppInfo;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, getDir:Ljava/lang/String;
    const-string v7, "kuban"

    .line 215
    .local v7, regex:Ljava/lang/String;
    const/4 v9, 0x2

    .line 214
    invoke-static {v7, v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 216
    .local v6, pattern:Ljava/util/regex/Pattern;
    invoke-virtual {v6, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 217
    .local v3, nokuban:Ljava/util/regex/Matcher;
    const/16 v2, 0x8

    .line 218
    .local v2, n:I
    const/4 v9, 0x0

    invoke-virtual {v0, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 219
    .local v1, isSystem:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 220
    iget-object v9, p0, Lcom/kuban/settings/InstalledApplicationActivity$4;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v9}, Lcom/kuban/settings/InstalledApplicationActivity;->nokubandelete()V

    .line 232
    :goto_29
    return-void

    .line 222
    :cond_2a
    const-string v9, "/system/"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_38

    .line 223
    iget-object v9, p0, Lcom/kuban/settings/InstalledApplicationActivity$4;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v9}, Lcom/kuban/settings/InstalledApplicationActivity;->systemPopUp()V

    goto :goto_29

    .line 225
    :cond_38
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 226
    .local v5, packname:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "package:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 227
    .local v4, packageURI:Landroid/net/Uri;
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.DELETE"

    invoke-direct {v8, v9, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 228
    .local v8, uninstallIntent:Landroid/content/Intent;
    iget-object v9, p0, Lcom/kuban/settings/InstalledApplicationActivity$4;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v9, v8}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    .line 229
    iget-object v9, p0, Lcom/kuban/settings/InstalledApplicationActivity$4;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v9}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    goto :goto_29
.end method
