.class Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "InstalledApplicationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/InstalledApplicationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "anContext"

    .prologue
    .line 135
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;->mContext:Landroid/content/Context;

    .line 137
    iget-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 138
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .prologue
    .line 141
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$1()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 145
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$1()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 149
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const v9, 0x7f0c0020

    .line 154
    if-nez p2, :cond_65

    .line 155
    iget-object v6, p0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03000b

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 156
    new-instance v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;-><init>()V

    .line 157
    .local v2, holder:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;
    const v6, 0x7f0c001f

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 158
    const v6, 0x7f0c001e

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 159
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 164
    :goto_2d
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$1()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kuban/settings/utils/AppInfo;

    .line 165
    .local v0, appInfo:Lcom/kuban/settings/utils/AppInfo;
    iget-object v6, v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v7, v0, Lcom/kuban/settings/utils/AppInfo;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 166
    iget-object v6, v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v7, v0, Lcom/kuban/settings/utils/AppInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    invoke-virtual {v0}, Lcom/kuban/settings/utils/AppInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, getDir:Ljava/lang/String;
    const/16 v4, 0x8

    .line 169
    .local v4, n:I
    const/4 v6, 0x0

    invoke-virtual {v1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 170
    .local v3, isSystem:Ljava/lang/String;
    const-string v6, "/system/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6c

    .line 171
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 172
    .local v5, system:Landroid/widget/ImageView;
    const v6, 0x7f020045

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    :goto_64
    return-object p2

    .line 161
    .end local v0           #appInfo:Lcom/kuban/settings/utils/AppInfo;
    .end local v1           #getDir:Ljava/lang/String;
    .end local v2           #holder:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;
    .end local v3           #isSystem:Ljava/lang/String;
    .end local v4           #n:I
    .end local v5           #system:Landroid/widget/ImageView;
    :cond_65
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;

    .restart local v2       #holder:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter$ViewHolder;
    goto :goto_2d

    .line 174
    .restart local v0       #appInfo:Lcom/kuban/settings/utils/AppInfo;
    .restart local v1       #getDir:Ljava/lang/String;
    .restart local v3       #isSystem:Ljava/lang/String;
    .restart local v4       #n:I
    :cond_6c
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 175
    .restart local v5       #system:Landroid/widget/ImageView;
    const v6, 0x7f020041

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_64
.end method
