.class Lcom/kuban/settings/ModListActivity$ModAsync$4;
.super Ljava/lang/Object;
.source "ModListActivity.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/ModListActivity$ModAsync;->onPostExecute(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kuban/settings/ModListActivity$ModAsync;


# direct methods
.method constructor <init>(Lcom/kuban/settings/ModListActivity$ModAsync;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/ModListActivity$ModAsync$4;->this$1:Lcom/kuban/settings/ModListActivity$ModAsync;

    .line 328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNavigationItemSelected(IJ)Z
    .registers 9
    .parameter "itemPosition"
    .parameter "itemId"

    .prologue
    const/4 v4, 0x1

    .line 331
    sget v2, Lcom/kuban/settings/ModListActivity;->menuposition:I

    if-ne p1, v2, :cond_6

    .line 343
    :goto_5
    return v4

    .line 335
    :cond_6
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/kuban/settings/ModListActivity$ModAsync$4;->this$1:Lcom/kuban/settings/ModListActivity$ModAsync;

    #getter for: Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;
    invoke-static {v2}, Lcom/kuban/settings/ModListActivity$ModAsync;->access$3(Lcom/kuban/settings/ModListActivity$ModAsync;)Lcom/kuban/settings/ModListActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/kuban/settings/ModListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 336
    .local v1, in:Landroid/content/Intent;
    iget-object v2, p0, Lcom/kuban/settings/ModListActivity$ModAsync$4;->this$1:Lcom/kuban/settings/ModListActivity$ModAsync;

    #getter for: Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;
    invoke-static {v2}, Lcom/kuban/settings/ModListActivity$ModAsync;->access$3(Lcom/kuban/settings/ModListActivity$ModAsync;)Lcom/kuban/settings/ModListActivity;

    move-result-object v2

    const v3, 0x7f0c0015

    invoke-virtual {v2, v3}, Lcom/kuban/settings/ModListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, Title:Ljava/lang/String;
    const-string v2, "TabName"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const-string v2, " "

    const-string v3, "_"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    const-string v2, "Url"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    iget-object v2, p0, Lcom/kuban/settings/ModListActivity$ModAsync$4;->this$1:Lcom/kuban/settings/ModListActivity$ModAsync;

    #getter for: Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;
    invoke-static {v2}, Lcom/kuban/settings/ModListActivity$ModAsync;->access$3(Lcom/kuban/settings/ModListActivity$ModAsync;)Lcom/kuban/settings/ModListActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    .line 342
    iget-object v2, p0, Lcom/kuban/settings/ModListActivity$ModAsync$4;->this$1:Lcom/kuban/settings/ModListActivity$ModAsync;

    #getter for: Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;
    invoke-static {v2}, Lcom/kuban/settings/ModListActivity$ModAsync;->access$3(Lcom/kuban/settings/ModListActivity$ModAsync;)Lcom/kuban/settings/ModListActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kuban/settings/ModListActivity;->finish()V

    goto :goto_5
.end method
