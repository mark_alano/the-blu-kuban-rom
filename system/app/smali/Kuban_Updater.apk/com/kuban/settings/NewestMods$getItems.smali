.class Lcom/kuban/settings/NewestMods$getItems;
.super Landroid/os/AsyncTask;
.source "NewestMods.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/NewestMods;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "getItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field Items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field URL:Ljava/lang/String;

.field downloadFolder:Ljava/lang/String;

.field menuItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field parser:Lcom/kuban/settings/utils/XMLParser;

.field prefs:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/kuban/settings/NewestMods;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/NewestMods;)V
    .registers 5
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 150
    const-string v0, "kuban.updater.parts"

    invoke-static {v0}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->URL:Ljava/lang/String;

    .line 151
    new-instance v0, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v0}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    .line 152
    invoke-virtual {p1}, Lcom/kuban/settings/NewestMods;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->prefs:Landroid/content/SharedPreferences;

    .line 153
    iget-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "downloadDir"

    const-string v2, "kubandownloads"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->downloadFolder:Ljava/lang/String;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->menuItems:Ljava/util/ArrayList;

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->Items:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/NewestMods;Lcom/kuban/settings/NewestMods$getItems;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/kuban/settings/NewestMods$getItems;-><init>(Lcom/kuban/settings/NewestMods;)V

    return-void
.end method

.method static synthetic access$3(Lcom/kuban/settings/NewestMods$getItems;)Lcom/kuban/settings/NewestMods;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/NewestMods$getItems;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 32
    .parameter "urls"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 159
    const-string v25, "kuban.updater.parts"

    invoke-static/range {v25 .. v25}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 160
    .local v3, BASEURL:Ljava/lang/String;
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 162
    :cond_b
    const/16 v25, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v26

    add-int/lit8 v26, v26, -0x8

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->URL:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 164
    .local v24, xml:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v12

    .line 165
    .local v12, doc:Lorg/w3c/dom/Document;
    const-string v25, "Tab"

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 166
    .local v20, nl:Lorg/w3c/dom/NodeList;
    new-instance v21, Ljava/text/SimpleDateFormat;

    const-string v25, "MM-dd-yyyy"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 167
    .local v21, sdf:Ljava/text/SimpleDateFormat;
    new-instance v16, Ljava/util/Date;

    invoke-direct/range {v16 .. v16}, Ljava/util/Date;-><init>()V

    .line 168
    .local v16, itemDate:Ljava/util/Date;
    const/16 v22, 0x0

    .local v22, t:I
    :goto_53
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    if-lt v0, v1, :cond_74

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_67
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_bd

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->Items:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    return-object v25

    .line 169
    :cond_74
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 170
    .local v18, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    check-cast v13, Lorg/w3c/dom/Element;

    .line 171
    .local v13, e:Lorg/w3c/dom/Element;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v26, v0

    const-string v27, "Url"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 172
    .local v5, NEWURL:Ljava/lang/String;
    const-string v25, "Url"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v22, v22, 0x1

    goto :goto_53

    .line 175
    .end local v5           #NEWURL:Ljava/lang/String;
    .end local v13           #e:Lorg/w3c/dom/Element;
    .end local v18           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_bd
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/HashMap;

    .line 176
    .restart local v18       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_cb
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_67

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 177
    .local v4, FINALURL:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v12

    .line 179
    const-string v27, "Element"

    move-object/from16 v0, v27

    invoke-interface {v12, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 180
    const/4 v15, 0x0

    .local v15, i:I
    :goto_fa
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v27

    move/from16 v0, v27

    if-ge v15, v0, :cond_cb

    .line 181
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 182
    .local v19, map3:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    .line 183
    .local v14, e2:Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v27, v0

    const-string v28, "Date"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 185
    .local v17, itemdate:Ljava/lang/String;
    :try_start_11f
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_126
    .catch Ljava/lang/Exception; {:try_start_11f .. :try_end_126} :catch_260

    move-result-object v16

    .line 187
    :goto_127
    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 188
    .local v8, date:J
    new-instance v27, Ljava/util/Date;

    invoke-direct/range {v27 .. v27}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v27 .. v27}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 189
    .local v6, currentDate:J
    const v23, 0x48190800

    .line 190
    .local v23, time:I
    sub-long v10, v6, v8

    .line 191
    .local v10, diff:J
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v27, v0

    cmp-long v27, v10, v27

    if-gez v27, :cond_25c

    .line 192
    const-string v27, "Name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Name"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const-string v27, "Desc"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Desc"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const-string v27, "Url"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Url"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const-string v27, "Date"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Date"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    const-string v27, "No"

    const-string v28, "No"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-string v27, "ThumbnailUrl"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "ThumbnailUrl"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    const-string v27, "Preview"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Preview"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const-string v27, "Preview2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Preview2"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    const-string v27, "Preview3"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Preview3"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v14, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    const-string v27, "Folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->downloadFolder:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    const-string v27, "604800000"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/kuban/settings/NewestMods;->daydiff:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/NewestMods$getItems;->Items:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_25c
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_fa

    .line 186
    .end local v6           #currentDate:J
    .end local v8           #date:J
    .end local v10           #diff:J
    .end local v23           #time:I
    :catch_260
    move-exception v27

    goto/16 :goto_127
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/NewestMods$getItems;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v8, 0x2

    const/4 v6, 0x0

    .line 212
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    const v2, 0x7f03000e

    invoke-virtual {v1, v2}, Lcom/kuban/settings/NewestMods;->setContentView(I)V

    .line 213
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-virtual {v1, v6}, Lcom/kuban/settings/NewestMods;->setProgressBarIndeterminateVisibility(Z)V

    .line 214
    new-instance v0, Lcom/kuban/settings/NewestMods$getItems$1;

    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-virtual {v1}, Lcom/kuban/settings/NewestMods;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/kuban/settings/NewestMods;->dataFromAsyncTask:Ljava/util/ArrayList;

    const v4, 0x7f030007

    .line 215
    new-array v5, v8, [Ljava/lang/String;

    const-string v1, "Url"

    aput-object v1, v5, v6

    const/4 v1, 0x1

    const-string v6, "Icon"

    aput-object v6, v5, v1

    .line 216
    new-array v6, v8, [I

    fill-array-data v6, :array_7e

    move-object v1, p0

    .line 214
    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/NewestMods$getItems$1;-><init>(Lcom/kuban/settings/NewestMods$getItems;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 223
    .local v0, adapter2:Landroid/widget/SimpleAdapter;
    new-instance v7, Lcom/kuban/settings/NewestMods$getItems$2;

    invoke-direct {v7, p0}, Lcom/kuban/settings/NewestMods$getItems$2;-><init>(Lcom/kuban/settings/NewestMods$getItems;)V

    .line 242
    .local v7, navigationListener:Landroid/app/ActionBar$OnNavigationListener;
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-virtual {v1}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0, v7}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 243
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-virtual {v1}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    iget v2, v2, Lcom/kuban/settings/NewestMods;->menuposition:I

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 244
    iget-object v2, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    const v3, 0x7f0c0023

    invoke-virtual {v1, v3}, Lcom/kuban/settings/NewestMods;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, v2, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    .line 245
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    new-instance v2, Lcom/kuban/settings/adapters/NewItemListAdapter;

    iget-object v3, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v3, p1}, Lcom/kuban/settings/adapters/NewItemListAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v2, v1, Lcom/kuban/settings/NewestMods;->adapter:Lcom/kuban/settings/adapters/NewItemListAdapter;

    .line 246
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    iget-object v1, v1, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    iget-object v2, v2, Lcom/kuban/settings/NewestMods;->adapter:Lcom/kuban/settings/adapters/NewItemListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 247
    iget-object v1, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    iget-object v1, v1, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    new-instance v2, Lcom/kuban/settings/NewestMods$getItems$3;

    invoke-direct {v2, p0}, Lcom/kuban/settings/NewestMods$getItems$3;-><init>(Lcom/kuban/settings/NewestMods$getItems;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 273
    return-void

    .line 216
    nop

    :array_7e
    .array-data 0x4
        0x15t 0x0t 0xct 0x7ft
        0x18t 0x0t 0xct 0x7ft
    .end array-data
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 147
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 148
    iget-object v0, p0, Lcom/kuban/settings/NewestMods$getItems;->this$0:Lcom/kuban/settings/NewestMods;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/NewestMods;->setProgressBarIndeterminateVisibility(Z)V

    .line 149
    return-void
.end method
