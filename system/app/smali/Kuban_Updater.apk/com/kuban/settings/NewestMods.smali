.class public Lcom/kuban/settings/NewestMods;
.super Landroid/app/Activity;
.source "NewestMods.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/NewestMods$getItems;
    }
.end annotation


# static fields
.field public static final DAYS_DIFF:Ljava/lang/String; = "604800000"

.field public static final DIALOG_DOWNLOAD_PROGRESS:I = 0x0

.field static final ELEMENT_ITEM:Ljava/lang/String; = "Element"

.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_ITEM:Ljava/lang/String; = "Tab"

.field public static final KEY_ITEM_DESC:Ljava/lang/String; = "Desc"

.field static final KEY_ITEM_DL:Ljava/lang/String; = "No"

.field public static final KEY_ITEM_FOLDER:Ljava/lang/String; = "Folder"

.field static final KEY_ITEM_INSTALL:Ljava/lang/String; = "No"

.field public static final KEY_ITEM_NAME:Ljava/lang/String; = "Name"

.field public static final KEY_ITEM_NEW:Ljava/lang/String; = "Date"

.field public static final KEY_ITEM_PREVIEW:Ljava/lang/String; = "Preview"

.field public static final KEY_ITEM_PREVIEW2:Ljava/lang/String; = "Preview2"

.field public static final KEY_ITEM_PREVIEW3:Ljava/lang/String; = "Preview3"

.field public static final KEY_ITEM_THUMB:Ljava/lang/String; = "ThumbnailUrl"

.field public static final KEY_ITEM_URL:Ljava/lang/String; = "Url"

.field static final KEY_NAME:Ljava/lang/String; = "TabName"

.field static final KEY_URL:Ljava/lang/String; = "Url"

.field public static dataFromAsyncTask:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field adapter:Lcom/kuban/settings/adapters/NewItemListAdapter;

.field daydiff:Ljava/lang/String;

.field list:Landroid/widget/ListView;

.field menuposition:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    const-string v0, "0"

    iput-object v0, p0, Lcom/kuban/settings/NewestMods;->daydiff:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/kuban/settings/NewestMods;->menuposition:I

    .line 41
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 11
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 69
    if-eqz p1, :cond_60

    .line 70
    const v1, 0x7f03000e

    invoke-virtual {p0, v1}, Lcom/kuban/settings/NewestMods;->setContentView(I)V

    .line 71
    invoke-virtual {p0, v6}, Lcom/kuban/settings/NewestMods;->setProgressBarIndeterminateVisibility(Z)V

    .line 72
    new-instance v0, Lcom/kuban/settings/NewestMods$1;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/kuban/settings/NewestMods;->dataFromAsyncTask:Ljava/util/ArrayList;

    const v4, 0x7f030007

    .line 73
    new-array v5, v8, [Ljava/lang/String;

    const-string v1, "Url"

    aput-object v1, v5, v6

    const/4 v1, 0x1

    const-string v6, "Icon"

    aput-object v6, v5, v1

    .line 74
    new-array v6, v8, [I

    fill-array-data v6, :array_62

    move-object v1, p0

    .line 72
    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/NewestMods$1;-><init>(Lcom/kuban/settings/NewestMods;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 81
    .local v0, adapter2:Landroid/widget/SimpleAdapter;
    new-instance v7, Lcom/kuban/settings/NewestMods$2;

    invoke-direct {v7, p0}, Lcom/kuban/settings/NewestMods$2;-><init>(Lcom/kuban/settings/NewestMods;)V

    .line 100
    .local v7, navigationListener:Landroid/app/ActionBar$OnNavigationListener;
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0, v7}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 101
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget v2, p0, Lcom/kuban/settings/NewestMods;->menuposition:I

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 102
    const v1, 0x7f0c0023

    invoke-virtual {p0, v1}, Lcom/kuban/settings/NewestMods;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    .line 103
    iget-object v1, p0, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/kuban/settings/NewestMods;->adapter:Lcom/kuban/settings/adapters/NewItemListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    iget-object v1, p0, Lcom/kuban/settings/NewestMods;->list:Landroid/widget/ListView;

    new-instance v2, Lcom/kuban/settings/NewestMods$3;

    invoke-direct {v2, p0}, Lcom/kuban/settings/NewestMods$3;-><init>(Lcom/kuban/settings/NewestMods;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    .end local v0           #adapter2:Landroid/widget/SimpleAdapter;
    .end local v7           #navigationListener:Landroid/app/ActionBar$OnNavigationListener;
    :cond_60
    return-void

    .line 74
    nop

    :array_62
    .array-data 0x4
        0x15t 0x0t 0xct 0x7ft
        0x18t 0x0t 0xct 0x7ft
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 134
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/kuban/settings/NewestMods;->requestWindowFeature(I)Z

    .line 136
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 137
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 139
    const v1, 0x7f03000e

    invoke-virtual {p0, v1}, Lcom/kuban/settings/NewestMods;->setContentView(I)V

    .line 140
    const-string v0, "Gerneral Mods"

    .line 141
    .local v0, passed_name:Ljava/lang/String;
    new-instance v1, Lcom/kuban/settings/adapters/MenuAsync;

    invoke-direct {v1}, Lcom/kuban/settings/adapters/MenuAsync;-><init>()V

    new-array v2, v3, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/kuban/settings/adapters/MenuAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    new-instance v1, Lcom/kuban/settings/NewestMods$getItems;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/kuban/settings/NewestMods$getItems;-><init>(Lcom/kuban/settings/NewestMods;Lcom/kuban/settings/NewestMods$getItems;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/kuban/settings/NewestMods$getItems;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 143
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 279
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 280
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 285
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 286
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 287
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 288
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 289
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 290
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 291
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 357
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 294
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 295
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 296
    .local v2, in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 298
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 301
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 302
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 305
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 308
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_92

    .line 309
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 310
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 312
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 315
    .end local v2           #in:Landroid/content/Intent;
    :cond_92
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_b5

    .line 316
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 317
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 319
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 322
    .end local v2           #in:Landroid/content/Intent;
    :cond_b5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_d8

    .line 323
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 326
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 329
    .end local v2           #in:Landroid/content/Intent;
    :cond_d8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_fb

    .line 330
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 333
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 336
    .end local v2           #in:Landroid/content/Intent;
    :cond_fb
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_11e

    .line 337
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 338
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 340
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 343
    .end local v2           #in:Landroid/content/Intent;
    :cond_11e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_141

    .line 344
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 347
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 350
    .end local v2           #in:Landroid/content/Intent;
    :cond_141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_162

    .line 351
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 352
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 353
    invoke-virtual {p0}, Lcom/kuban/settings/NewestMods;->finish()V

    .line 354
    invoke-virtual {p0, v2}, Lcom/kuban/settings/NewestMods;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 357
    .end local v2           #in:Landroid/content/Intent;
    :cond_162
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
