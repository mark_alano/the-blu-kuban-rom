.class public Lcom/kuban/settings/RestoreActivity;
.super Landroid/app/ListActivity;
.source "RestoreActivity.java"


# static fields
.field static final DL_ITEM:Ljava/lang/String; = "filename"


# instance fields
.field installbtn:Landroid/widget/Button;

.field tozipdir:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    return-void
.end method


# virtual methods
.method ListDir(Ljava/io/File;)V
    .registers 11
    .parameter "dl_dir"

    .prologue
    const/4 v8, 0x0

    .line 45
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, dirs:[Ljava/lang/String;
    if-eqz v1, :cond_3a

    .line 48
    const v6, 0x102000a

    invoke-virtual {p0, v6}, Lcom/kuban/settings/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 49
    .local v2, dl_listview:Landroid/widget/ListView;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v3, fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v4, 0x0

    .local v4, i:I
    :goto_16
    array-length v6, v1

    if-lt v4, v6, :cond_3b

    .line 55
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v6, 0x7f03000c

    invoke-direct {v0, p0, v6, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 56
    .local v0, adapter:Landroid/widget/ListAdapter;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 57
    invoke-virtual {v2, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 58
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    iget-object v6, p0, Lcom/kuban/settings/RestoreActivity;->installbtn:Landroid/widget/Button;

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 60
    iget-object v6, p0, Lcom/kuban/settings/RestoreActivity;->installbtn:Landroid/widget/Button;

    new-instance v7, Lcom/kuban/settings/RestoreActivity$1;

    invoke-direct {v7, p0, v2}, Lcom/kuban/settings/RestoreActivity$1;-><init>(Lcom/kuban/settings/RestoreActivity;Landroid/widget/ListView;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v2           #dl_listview:Landroid/widget/ListView;
    .end local v3           #fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4           #i:I
    :cond_3a
    return-void

    .line 51
    .restart local v2       #dl_listview:Landroid/widget/ListView;
    .restart local v3       #fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v4       #i:I
    :cond_3b
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 52
    .local v5, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "filename"

    aget-object v7, v1, v4

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto :goto_16
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/kuban/settings/RestoreActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 34
    const v4, 0x7f030001

    invoke-virtual {p0, v4}, Lcom/kuban/settings/RestoreActivity;->setContentView(I)V

    .line 35
    const v4, 0x7f0c0002

    invoke-virtual {p0, v4}, Lcom/kuban/settings/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/kuban/settings/RestoreActivity;->installbtn:Landroid/widget/Button;

    .line 36
    iget-object v4, p0, Lcom/kuban/settings/RestoreActivity;->installbtn:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 37
    invoke-virtual {p0}, Lcom/kuban/settings/RestoreActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 38
    .local v1, prefs:Landroid/content/SharedPreferences;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, root:Ljava/lang/String;
    const-string v4, "/mnt"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, rootNoMnt:Ljava/lang/String;
    const-string v4, "backupDir"

    const-string v5, "KubanModBackup/"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, backupDir:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/kuban/settings/RestoreActivity;->tozipdir:Ljava/io/File;

    .line 42
    iget-object v4, p0, Lcom/kuban/settings/RestoreActivity;->tozipdir:Ljava/io/File;

    invoke-virtual {p0, v4}, Lcom/kuban/settings/RestoreActivity;->ListDir(Ljava/io/File;)V

    .line 43
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 8
    .parameter "parent"
    .parameter "view"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 95
    const v1, 0x7f0c0002

    invoke-virtual {p0, v1}, Lcom/kuban/settings/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 96
    .local v0, installbtn:Landroid/widget/Button;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/RestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v0, in:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0}, Lcom/kuban/settings/RestoreActivity;->finish()V

    .line 103
    invoke-virtual {p0, v0}, Lcom/kuban/settings/RestoreActivity;->startActivity(Landroid/content/Intent;)V

    .line 104
    const/4 v1, 0x1

    return v1
.end method
