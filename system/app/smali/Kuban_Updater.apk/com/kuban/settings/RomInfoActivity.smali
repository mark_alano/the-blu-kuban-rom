.class public Lcom/kuban/settings/RomInfoActivity;
.super Landroid/app/Activity;
.source "RomInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;
    }
.end annotation


# static fields
.field public static final DIALOG_DOWNLOAD_PROGRESS:I = 0x0

.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_NAME:Ljava/lang/String; = "ModName"

.field static final KEY_TAB:Ljava/lang/String; = "Tab"

.field static final KEY_UPDATE:Ljava/lang/String; = "Update"

.field static final KEY_URL:Ljava/lang/String; = "Url"

.field static final KEY_VERSION:Ljava/lang/String; = "Version"

.field public static URL2:Ljava/lang/String;


# instance fields
.field final URL:Ljava/lang/String;

.field adapter:Lcom/kuban/settings/adapters/ListItemAdapter;

.field cacheDirectory:Ljava/io/File;

.field final context:Landroid/content/Context;

.field downloadFolder:Ljava/lang/String;

.field final handler:Landroid/os/Handler;

.field info:Ljava/lang/String;

.field list:Landroid/widget/ListView;

.field mLongPressed:Ljava/lang/Runnable;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field menuposition:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    const-string v0, "kuban.updater.check"

    invoke-static {v0}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/RomInfoActivity;->URL:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/kuban/settings/RomInfoActivity;->menuposition:I

    .line 75
    iput-object p0, p0, Lcom/kuban/settings/RomInfoActivity;->context:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/RomInfoActivity;->handler:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/kuban/settings/RomInfoActivity$1;

    invoke-direct {v0, p0}, Lcom/kuban/settings/RomInfoActivity$1;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    iput-object v0, p0, Lcom/kuban/settings/RomInfoActivity;->mLongPressed:Ljava/lang/Runnable;

    .line 59
    return-void
.end method

.method static synthetic access$0(Lcom/kuban/settings/RomInfoActivity;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public easter()V
    .registers 4

    .prologue
    .line 469
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/RomInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/TestActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 470
    .local v0, in:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/kuban/settings/RomInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 471
    return-void
.end method

.method public manualRomCheck(Landroid/view/View;Ljava/lang/String;)V
    .registers 32
    .parameter "view"
    .parameter "URL"

    .prologue
    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/RomInfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v26

    const/16 v27, 0x80

    invoke-virtual/range {v26 .. v27}, Landroid/view/Window;->addFlags(I)V

    .line 170
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/RomInfoActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v19

    .line 171
    .local v19, prefs:Landroid/content/SharedPreferences;
    const-string v26, "downloadDir"

    const-string v27, "kubandownloads"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    .line 172
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v3, UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v18, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct/range {v18 .. v18}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 174
    .local v18, parser:Lcom/kuban/settings/utils/XMLParser;
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 175
    .local v25, xml:Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v10

    .line 176
    .local v10, doc:Lorg/w3c/dom/Document;
    const-string v26, "Update"

    move-object/from16 v0, v26

    invoke-interface {v10, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v21

    .line 177
    .local v21, update:Lorg/w3c/dom/NodeList;
    const/4 v14, 0x0

    .local v14, i:I
    :goto_48
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v26

    move/from16 v0, v26

    if-lt v14, v0, :cond_51

    .line 232
    .end local v3           #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v10           #doc:Lorg/w3c/dom/Document;
    .end local v14           #i:I
    .end local v18           #parser:Lcom/kuban/settings/utils/XMLParser;
    .end local v19           #prefs:Landroid/content/SharedPreferences;
    .end local v21           #update:Lorg/w3c/dom/NodeList;
    .end local v25           #xml:Ljava/lang/String;
    :goto_50
    return-void

    .line 178
    .restart local v3       #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v10       #doc:Lorg/w3c/dom/Document;
    .restart local v14       #i:I
    .restart local v18       #parser:Lcom/kuban/settings/utils/XMLParser;
    .restart local v19       #prefs:Landroid/content/SharedPreferences;
    .restart local v21       #update:Lorg/w3c/dom/NodeList;
    .restart local v25       #xml:Ljava/lang/String;
    :cond_51
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 179
    .local v22, updateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v21

    invoke-interface {v0, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    .line 180
    .local v12, e:Lorg/w3c/dom/Element;
    const-string v26, "Version"

    const-string v27, "Version"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-string v26, "Url"

    const-string v27, "Url"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    const-string v26, "Version"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 184
    .local v4, Version:Ljava/lang/String;
    const-string v26, "Url"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    sput-object v26, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    .line 185
    sget-object v26, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 186
    .local v13, fileExtenstion:Ljava/lang/String;
    sget-object v26, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v13}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 187
    .local v15, name:Ljava/lang/String;
    sget-object v9, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    .line 188
    .local v9, currentversion:Ljava/lang/String;
    const-string v26, "[^0-9.]"

    const-string v27, ""

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 189
    const/16 v26, 0x0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x7

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->max(II)I

    move-result v26

    move/from16 v0, v26

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    .line 190
    .local v24, version:Ljava/lang/String;
    const-string v26, "\\."

    const-string v27, ""

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 191
    const-string v26, "\\."

    const-string v27, ""

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 192
    .local v16, onlineVersion:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 193
    .local v23, v:I
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 194
    .local v17, ov:I
    move/from16 v0, v17

    move/from16 v1, v23

    if-le v0, v1, :cond_172

    .line 195
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 196
    .local v6, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "New Update Available ("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 198
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "\nWould you like to download "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " now?\n"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v26

    .line 199
    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v26

    .line 200
    const-string v27, "Yes"

    new-instance v28, Lcom/kuban/settings/RomInfoActivity$6;

    invoke-direct/range {v28 .. v29}, Lcom/kuban/settings/RomInfoActivity$6;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual/range {v26 .. v28}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v26

    .line 205
    const-string v27, "No"

    new-instance v28, Lcom/kuban/settings/RomInfoActivity$7;

    invoke-direct/range {v28 .. v29}, Lcom/kuban/settings/RomInfoActivity$7;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual/range {v26 .. v28}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v26

    .line 211
    const-string v27, "ChangeLog"

    new-instance v28, Lcom/kuban/settings/RomInfoActivity$8;

    invoke-direct/range {v28 .. v29}, Lcom/kuban/settings/RomInfoActivity$8;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual/range {v26 .. v28}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 219
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 220
    .local v5, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 177
    .end local v5           #alertDialog:Landroid/app/AlertDialog;
    .end local v6           #alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    :goto_16e
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_48

    .line 223
    :cond_172
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/RomInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 224
    .local v8, context:Landroid/content/Context;
    const-string v7, "ROM is up to date!"

    .line 225
    .local v7, content:Ljava/lang/CharSequence;
    const/4 v11, 0x0

    .line 226
    .local v11, duration:I
    invoke-static {v8, v7, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v20

    .line 227
    .local v20, toast:Landroid/widget/Toast;
    invoke-virtual/range {v20 .. v20}, Landroid/widget/Toast;->show()V
    :try_end_180
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_180} :catch_181

    goto :goto_16e

    .line 230
    .end local v3           #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4           #Version:Ljava/lang/String;
    .end local v7           #content:Ljava/lang/CharSequence;
    .end local v8           #context:Landroid/content/Context;
    .end local v9           #currentversion:Ljava/lang/String;
    .end local v10           #doc:Lorg/w3c/dom/Document;
    .end local v11           #duration:I
    .end local v12           #e:Lorg/w3c/dom/Element;
    .end local v13           #fileExtenstion:Ljava/lang/String;
    .end local v14           #i:I
    .end local v15           #name:Ljava/lang/String;
    .end local v16           #onlineVersion:Ljava/lang/String;
    .end local v17           #ov:I
    .end local v18           #parser:Lcom/kuban/settings/utils/XMLParser;
    .end local v19           #prefs:Landroid/content/SharedPreferences;
    .end local v20           #toast:Landroid/widget/Toast;
    .end local v21           #update:Lorg/w3c/dom/NodeList;
    .end local v22           #updateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v23           #v:I
    .end local v24           #version:Ljava/lang/String;
    .end local v25           #xml:Ljava/lang/String;
    :catch_181
    move-exception v26

    goto/16 :goto_50
.end method

.method public md5Match()V
    .registers 9

    .prologue
    .line 380
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 381
    .local v3, fileExtenstion:Ljava/lang/String;
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 382
    .local v4, name:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 383
    const-string v5, "_"

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 384
    .local v2, display_name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 385
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v5, "MD5 Match: File ready"

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 387
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\nWould you like to reboot and install "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " now?\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 388
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 389
    const-string v6, "Yes"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$9;

    invoke-direct {v7, p0, v4}, Lcom/kuban/settings/RomInfoActivity$9;-><init>(Lcom/kuban/settings/RomInfoActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 397
    const-string v6, "No"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$10;

    invoke-direct {v7, p0}, Lcom/kuban/settings/RomInfoActivity$10;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 404
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 405
    return-void
.end method

.method public md5NotMatch()V
    .registers 9

    .prologue
    .line 407
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 408
    .local v3, fileExtenstion:Ljava/lang/String;
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 409
    .local v4, name:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 410
    const-string v5, "_"

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 411
    .local v2, display_name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 412
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v5, "MD5 Does not Match"

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 414
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\nWould you still like to reboot and install "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " now?\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 415
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 416
    const-string v6, "Yes"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$11;

    invoke-direct {v7, p0, v4}, Lcom/kuban/settings/RomInfoActivity$11;-><init>(Lcom/kuban/settings/RomInfoActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 424
    const-string v6, "No"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$12;

    invoke-direct {v7, p0}, Lcom/kuban/settings/RomInfoActivity$12;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 431
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 432
    return-void
.end method

.method public noMD5tomatch()V
    .registers 9

    .prologue
    .line 434
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 435
    .local v3, fileExtenstion:Ljava/lang/String;
    sget-object v5, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 436
    .local v4, name:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 437
    const-string v5, "_"

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 438
    .local v2, display_name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/kuban/settings/RomInfoActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 439
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v5, "No MD5 online to match the file to!!"

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 441
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\nWould you still like to reboot and install "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " now?\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 442
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 443
    const-string v6, "Yes"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$13;

    invoke-direct {v7, p0, v4}, Lcom/kuban/settings/RomInfoActivity$13;-><init>(Lcom/kuban/settings/RomInfoActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 451
    const-string v6, "No"

    new-instance v7, Lcom/kuban/settings/RomInfoActivity$14;

    invoke-direct {v7, p0}, Lcom/kuban/settings/RomInfoActivity$14;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 457
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 458
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 459
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 6
    .parameter "newConfig"

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 86
    const v2, 0x7f03000e

    invoke-virtual {p0, v2}, Lcom/kuban/settings/RomInfoActivity;->setContentView(I)V

    .line 87
    const v2, 0x7f0c0023

    invoke-virtual {p0, v2}, Lcom/kuban/settings/RomInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/kuban/settings/RomInfoActivity;->list:Landroid/widget/ListView;

    .line 88
    new-instance v2, Lcom/kuban/settings/adapters/ListItemAdapter;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/kuban/settings/adapters/ListItemAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v2, p0, Lcom/kuban/settings/RomInfoActivity;->adapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    .line 89
    iget-object v2, p0, Lcom/kuban/settings/RomInfoActivity;->list:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/kuban/settings/RomInfoActivity;->adapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    const v2, 0x7f030011

    invoke-virtual {p0, v2}, Lcom/kuban/settings/RomInfoActivity;->setContentView(I)V

    .line 91
    const v2, 0x7f0c0031

    invoke-virtual {p0, v2}, Lcom/kuban/settings/RomInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 92
    .local v1, rominfo:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const v2, 0x7f0c0032

    invoke-virtual {p0, v2}, Lcom/kuban/settings/RomInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 94
    .local v0, manualcheck:Landroid/widget/Button;
    new-instance v2, Lcom/kuban/settings/RomInfoActivity$2;

    invoke-direct {v2, p0}, Lcom/kuban/settings/RomInfoActivity$2;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    new-instance v2, Lcom/kuban/settings/RomInfoActivity$3;

    invoke-direct {v2, p0}, Lcom/kuban/settings/RomInfoActivity$3;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 114
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 12
    .parameter "savedInstanceState"

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0}, Lcom/kuban/settings/RomInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 118
    invoke-virtual {p0}, Lcom/kuban/settings/RomInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    const-string v9, "ROM Information"

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 119
    const v8, 0x7f030011

    invoke-virtual {p0, v8}, Lcom/kuban/settings/RomInfoActivity;->setContentView(I)V

    .line 120
    const v8, 0x7f0c0032

    invoke-virtual {p0, v8}, Lcom/kuban/settings/RomInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 121
    .local v5, manualcheck:Landroid/widget/Button;
    new-instance v8, Lcom/kuban/settings/RomInfoActivity$4;

    invoke-direct {v8, p0}, Lcom/kuban/settings/RomInfoActivity$4;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    new-instance v8, Lcom/kuban/settings/RomInfoActivity$5;

    invoke-direct {v8, p0}, Lcom/kuban/settings/RomInfoActivity$5;-><init>(Lcom/kuban/settings/RomInfoActivity;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 141
    const-string v8, "gsm.version.baseband"

    invoke-static {v8}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, basebandVersion:Ljava/lang/String;
    const-string v8, "gsm.version.baseband"

    invoke-static {v8}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, carrier:Ljava/lang/String;
    :try_start_3f
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    const-string v9, "cat /proc/version"

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v6

    .line 145
    .local v6, p:Ljava/lang/Process;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v6}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 146
    .local v3, in2:Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 147
    .local v4, line:Ljava/lang/String;
    :goto_58
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5f

    .line 166
    .end local v3           #in2:Ljava/io/BufferedReader;
    .end local v4           #line:Ljava/lang/String;
    .end local v6           #p:Ljava/lang/Process;
    :goto_5e
    return-void

    .line 148
    .restart local v3       #in2:Ljava/io/BufferedReader;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v6       #p:Ljava/lang/Process;
    :cond_5f
    const-string v8, " ) "

    const-string v9, ")\n  "

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 149
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\nPhone Information:"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 150
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  Manufacture: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 151
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  Model: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 152
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  Carrier: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 153
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  Modem Version: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 154
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n\nKernel Information:"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 155
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 156
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 157
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\nROM Information:"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 158
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 159
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n  Version: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    .line 160
    const v8, 0x7f0c0031

    invoke-virtual {p0, v8}, Lcom/kuban/settings/RomInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 161
    .local v7, rominfo:Landroid/widget/TextView;
    iget-object v8, p0, Lcom/kuban/settings/RomInfoActivity;->info:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_196
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_196} :catch_198

    goto/16 :goto_58

    .line 163
    .end local v3           #in2:Ljava/io/BufferedReader;
    .end local v4           #line:Ljava/lang/String;
    .end local v6           #p:Ljava/lang/Process;
    .end local v7           #rominfo:Landroid/widget/TextView;
    :catch_198
    move-exception v2

    .line 164
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5e
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 25
    .parameter "id"

    .prologue
    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/RomInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    .line 237
    .local v16, orintation:Ljava/lang/String;
    const-string v20, "2"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2d

    .line 238
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/kuban/settings/RomInfoActivity;->setRequestedOrientation(I)V

    .line 243
    :cond_27
    :goto_27
    packed-switch p1, :pswitch_data_132

    .line 272
    const/16 v20, 0x0

    :goto_2c
    return-object v20

    .line 240
    :cond_2d
    const-string v20, "1"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_27

    .line 241
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/kuban/settings/RomInfoActivity;->setRequestedOrientation(I)V

    goto :goto_27

    .line 245
    :pswitch_43
    new-instance v20, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-direct/range {v20 .. v22}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_6d

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->mkdirs()Z

    .line 248
    :cond_6d
    sget-object v20, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 249
    .local v8, fileExtenstion:Ljava/lang/String;
    sget-object v20, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v8}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 250
    .local v11, name:Ljava/lang/String;
    const-string v20, "_"

    const-string v21, " "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 251
    .local v7, display_name:Ljava/lang/String;
    new-instance v20, Landroid/app/ProgressDialog;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Downloading "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/ProgressDialog;->show()V

    .line 256
    const-string v15, "notification"

    .line 257
    .local v15, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/kuban/settings/RomInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/NotificationManager;

    .line 258
    .local v10, mNotificationManager:Landroid/app/NotificationManager;
    const v9, 0x7f02001d

    .line 259
    .local v9, icon:I
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Now Downloading "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 260
    .local v17, tickerText:Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 261
    .local v18, when:J
    new-instance v13, Landroid/app/Notification;

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-direct {v13, v9, v0, v1, v2}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 262
    .local v13, notification:Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/RomInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 263
    .local v6, context:Landroid/content/Context;
    const-string v5, "Blu Kuban Updater"

    .line 264
    .local v5, contentTitle:Ljava/lang/CharSequence;
    const-string v4, "Download in progress"

    .line 265
    .local v4, contentText:Ljava/lang/CharSequence;
    new-instance v14, Landroid/content/Intent;

    const-class v20, Lcom/kuban/settings/Home;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 266
    .local v14, notificationIntent:Landroid/content/Intent;
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v14, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 267
    .local v3, contentIntent:Landroid/app/PendingIntent;
    invoke-virtual {v13, v6, v5, v4, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 268
    const/4 v12, 0x1

    .line 269
    .local v12, not_ID:I
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v10, v0, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v20, v0

    goto/16 :goto_2c

    .line 243
    :pswitch_data_132
    .packed-switch 0x0
        :pswitch_43
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 462
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/RomInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 463
    .local v0, in:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 464
    invoke-virtual {p0}, Lcom/kuban/settings/RomInfoActivity;->finish()V

    .line 465
    invoke-virtual {p0, v0}, Lcom/kuban/settings/RomInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 466
    const/4 v1, 0x1

    return v1
.end method
