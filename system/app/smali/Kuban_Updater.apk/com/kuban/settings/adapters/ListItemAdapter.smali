.class public Lcom/kuban/settings/adapters/ListItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListItemAdapter.java"


# static fields
.field private static inflater:Landroid/view/LayoutInflater;


# instance fields
.field private activity:Landroid/app/Activity;

.field cacheDirectory:Ljava/io/File;

.field private data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field file:Ljava/io/File;

.field public imageLoader:Lcom/kuban/settings/utils/ImageLoader;

.field reader:Ljava/io/LineNumberReader;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/kuban/settings/adapters/ListItemAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .registers 5
    .parameter "a"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, d:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    .line 37
    iput-object p1, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->activity:Landroid/app/Activity;

    .line 38
    iput-object p2, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->data:Ljava/util/ArrayList;

    .line 39
    iget-object v0, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->activity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sput-object v0, Lcom/kuban/settings/adapters/ListItemAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 40
    new-instance v0, Lcom/kuban/settings/utils/ImageLoader;

    iget-object v1, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/kuban/settings/utils/ImageLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->imageLoader:Lcom/kuban/settings/utils/ImageLoader;

    .line 41
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/kuban/settings/adapters/ListItemAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 46
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 49
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 46
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 52
    move-object/from16 v38, p2

    .line 53
    .local v38, vi:Landroid/view/View;
    if-nez p2, :cond_f

    .line 54
    sget-object v39, Lcom/kuban/settings/adapters/ListItemAdapter;->inflater:Landroid/view/LayoutInflater;

    const v40, 0x7f03000f

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v38

    .line 55
    :cond_f
    const v39, 0x7f0c001a

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/TextView;

    .line 56
    .local v37, title:Landroid/widget/TextView;
    const v39, 0x7f0c002d

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 57
    .local v8, desc:Landroid/widget/TextView;
    const v39, 0x7f0c0024

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/ImageView;

    .line 58
    .local v36, thumb_image:Landroid/widget/ImageView;
    const v39, 0x7f0c002e

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 59
    .local v13, dl_image:Landroid/widget/ImageView;
    const v39, 0x7f0c002f

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 60
    .local v21, install_image:Landroid/widget/ImageView;
    const v39, 0x7f0c0026

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 61
    .local v12, dl:Landroid/widget/TextView;
    const v39, 0x7f0c0027

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 62
    .local v22, installed:Landroid/widget/TextView;
    const v39, 0x7f0c0025

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 63
    .local v26, link:Landroid/widget/TextView;
    const v39, 0x7f0c0010

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 64
    .local v7, date:Landroid/widget/TextView;
    const v39, 0x7f0c0028

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    .line 65
    .local v32, preview:Landroid/widget/TextView;
    const v39, 0x7f0c0029

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    .line 66
    .local v33, preview2:Landroid/widget/TextView;
    const v39, 0x7f0c002a

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 67
    .local v34, preview3:Landroid/widget/TextView;
    const v39, 0x7f0c002b

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 68
    .local v19, folder:Landroid/widget/TextView;
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 69
    .local v24, item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->data:Ljava/util/ArrayList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    .end local v24           #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v24, Ljava/util/HashMap;

    .line 70
    .restart local v24       #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v35, Ljava/text/SimpleDateFormat;

    const-string v39, "MM-dd-yyyy"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 71
    .local v35, sdf:Ljava/text/SimpleDateFormat;
    new-instance v39, Ljava/util/Date;

    invoke-direct/range {v39 .. v39}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, currentDate:Ljava/lang/String;
    const-string v39, "Date"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 73
    .local v25, itemdate:Ljava/lang/String;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 74
    .local v4, convertedDate:Ljava/util/Date;
    new-instance v40, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v41

    const-string v39, "Folder"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/ListItemAdapter;->cacheDirectory:Ljava/io/File;

    .line 75
    const-string v39, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    const-string v40, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    const/16 v41, 0x2f

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v40

    add-int/lit8 v41, v40, 0x1

    const-string v40, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    move-object/from16 v0, v39

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 76
    .local v18, fileName:Ljava/lang/String;
    new-instance v39, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->cacheDirectory:Ljava/io/File;

    move-object/from16 v40, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/ListItemAdapter;->file:Ljava/io/File;

    .line 77
    const-string v39, "604800000"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 78
    .local v11, diffdays:Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 79
    .local v20, i:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->file:Ljava/io/File;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v39

    if-eqz v39, :cond_3f3

    .line 80
    const v39, 0x7f020018

    move/from16 v0, v39

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    const-string v14, "Yes"

    .line 82
    .local v14, downloaded:Ljava/lang/String;
    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :goto_15d
    :try_start_15d
    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_164
    .catch Landroid/net/ParseException; {:try_start_15d .. :try_end_164} :catch_4b0
    .catch Ljava/text/ParseException; {:try_start_15d .. :try_end_164} :catch_4b3

    move-result-object v4

    .line 94
    :goto_165
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 96
    .local v6, currentconvertedDate:Ljava/util/Date;
    :try_start_16a
    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_16f
    .catch Landroid/net/ParseException; {:try_start_16a .. :try_end_16f} :catch_4b6
    .catch Ljava/text/ParseException; {:try_start_16a .. :try_end_16f} :catch_4b9

    move-result-object v6

    .line 100
    :goto_170
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v27

    .line 101
    .local v27, milis1:J
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v29

    .line 102
    .local v29, milis2:J
    sub-long v9, v29, v27

    .line 103
    .local v9, diff:J
    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v39, v0

    cmp-long v39, v9, v39

    if-gez v39, :cond_402

    .line 104
    const v39, -0x7feaab6a

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->setBackgroundColor(I)V

    .line 108
    :goto_189
    const-string v39, ".zip"

    const-string v40, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v31

    .line 110
    .local v31, onlineModName:Ljava/lang/String;
    :try_start_197
    new-instance v39, Ljava/io/LineNumberReader;

    new-instance v40, Ljava/io/FileReader;

    const-string v41, "/data/mods.txt"

    invoke-direct/range {v40 .. v41}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v39 .. v40}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    .line 112
    const-string v3, "No"

    .line 113
    .local v3, caught:Ljava/lang/String;
    :cond_1ab
    :goto_1ab
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_1b4
    .catchall {:try_start_197 .. :try_end_1b4} :catchall_492
    .catch Ljava/lang/Exception; {:try_start_197 .. :try_end_1b4} :catch_478

    move-result-object v17

    .local v17, fileModLine:Ljava/lang/String;
    if-nez v17, :cond_409

    .line 130
    :try_start_1b7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v39, v0

    if-eqz v39, :cond_1c8

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/io/LineNumberReader;->close()V
    :try_end_1c8
    .catch Ljava/io/IOException; {:try_start_1b7 .. :try_end_1c8} :catch_4aa

    .line 137
    .end local v3           #caught:Ljava/lang/String;
    .end local v17           #fileModLine:Ljava/lang/String;
    :cond_1c8
    :goto_1c8
    const-string v39, "Date"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_1f7

    const-string v39, "Date"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_1f7

    .line 138
    const-string v39, "Date"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_1f7
    const-string v39, "Name"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_228

    const-string v39, "Name"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_228

    .line 141
    const-string v39, "Name"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :cond_228
    const-string v39, "Desc"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_257

    const-string v39, "Desc"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_257

    .line 144
    const-string v39, "Desc"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v39

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :cond_257
    const-string v39, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_288

    const-string v39, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_288

    .line 147
    const-string v39, "Url"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v26

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_288
    const-string v39, "Preview"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_2b9

    const-string v39, "Preview"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_2b9

    .line 150
    const-string v39, "Preview"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_2b9
    const-string v39, "Preview"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-eqz v39, :cond_2d4

    .line 153
    const-string v39, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_2d4
    const-string v39, "Preview2"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_305

    const-string v39, "Preview2"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_305

    .line 156
    const-string v39, "Preview2"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :cond_305
    const-string v39, "Preview2"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-eqz v39, :cond_320

    .line 159
    const-string v39, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    :cond_320
    const-string v39, "Preview3"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_351

    const-string v39, "Preview3"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_351

    .line 162
    const-string v39, "Preview3"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :cond_351
    const-string v39, "Preview3"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-eqz v39, :cond_36c

    .line 165
    const-string v39, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :cond_36c
    const-string v39, "Folder"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_39d

    const-string v39, "Folder"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_39d

    .line 168
    const-string v39, "Folder"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    :cond_39d
    const-string v39, "ThumbnailUrl"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    if-eqz v39, :cond_3d6

    const-string v39, "ThumbnailUrl"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_3d6

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->imageLoader:Lcom/kuban/settings/utils/ImageLoader;

    move-object/from16 v40, v0

    const-string v39, "ThumbnailUrl"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/kuban/settings/utils/ImageLoader;->DisplayImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 173
    :cond_3d6
    const-string v39, "ThumbnailUrl"

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/String;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-eqz v39, :cond_3f2

    .line 174
    const v39, 0x7f02001f

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    :cond_3f2
    return-object v38

    .line 85
    .end local v6           #currentconvertedDate:Ljava/util/Date;
    .end local v9           #diff:J
    .end local v14           #downloaded:Ljava/lang/String;
    .end local v27           #milis1:J
    .end local v29           #milis2:J
    .end local v31           #onlineModName:Ljava/lang/String;
    :cond_3f3
    const v39, 0x7f020041

    move/from16 v0, v39

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    const-string v14, "No"

    .line 87
    .restart local v14       #downloaded:Ljava/lang/String;
    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_15d

    .line 106
    .restart local v6       #currentconvertedDate:Ljava/util/Date;
    .restart local v9       #diff:J
    .restart local v27       #milis1:J
    .restart local v29       #milis2:J
    :cond_402
    const/16 v39, 0x0

    invoke-virtual/range {v38 .. v39}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_189

    .line 114
    .restart local v3       #caught:Ljava/lang/String;
    .restart local v17       #fileModLine:Ljava/lang/String;
    .restart local v31       #onlineModName:Ljava/lang/String;
    :cond_409
    const/16 v39, 0x0

    :try_start_40b
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v40

    add-int/lit8 v40, v40, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 115
    .local v16, fileMod:Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_458

    .line 116
    const/16 v39, 0x3d

    move-object/from16 v0, v17

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v39

    add-int/lit8 v39, v39, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 117
    const-string v39, "1"

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_1ab

    .line 118
    const v39, 0x7f02003d

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    const-string v23, "Yes"

    .line 120
    .local v23, isInstalled:Ljava/lang/String;
    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    const-string v3, "1"

    goto/16 :goto_1ab

    .line 123
    .end local v23           #isInstalled:Ljava/lang/String;
    :cond_458
    move-object/from16 v0, v16

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_1ab

    const-string v39, "1"

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_1ab

    .line 124
    const v39, 0x7f020041

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_476
    .catchall {:try_start_40b .. :try_end_476} :catchall_492
    .catch Ljava/lang/Exception; {:try_start_40b .. :try_end_476} :catch_478

    goto/16 :goto_1ab

    .line 127
    .end local v3           #caught:Ljava/lang/String;
    .end local v16           #fileMod:Ljava/lang/String;
    .end local v17           #fileModLine:Ljava/lang/String;
    :catch_478
    move-exception v39

    .line 130
    :try_start_479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v39, v0

    if-eqz v39, :cond_1c8

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/io/LineNumberReader;->close()V
    :try_end_48a
    .catch Ljava/io/IOException; {:try_start_479 .. :try_end_48a} :catch_48c

    goto/16 :goto_1c8

    .line 133
    :catch_48c
    move-exception v15

    .line 134
    .local v15, ex:Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1c8

    .line 128
    .end local v15           #ex:Ljava/io/IOException;
    :catchall_492
    move-exception v39

    .line 130
    :try_start_493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v40, v0

    if-eqz v40, :cond_4a4

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/ListItemAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/io/LineNumberReader;->close()V
    :try_end_4a4
    .catch Ljava/io/IOException; {:try_start_493 .. :try_end_4a4} :catch_4a5

    .line 136
    :cond_4a4
    :goto_4a4
    throw v39

    .line 133
    :catch_4a5
    move-exception v15

    .line 134
    .restart local v15       #ex:Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4a4

    .line 133
    .end local v15           #ex:Ljava/io/IOException;
    .restart local v3       #caught:Ljava/lang/String;
    .restart local v17       #fileModLine:Ljava/lang/String;
    :catch_4aa
    move-exception v15

    .line 134
    .restart local v15       #ex:Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1c8

    .line 92
    .end local v3           #caught:Ljava/lang/String;
    .end local v6           #currentconvertedDate:Ljava/util/Date;
    .end local v9           #diff:J
    .end local v15           #ex:Ljava/io/IOException;
    .end local v17           #fileModLine:Ljava/lang/String;
    .end local v27           #milis1:J
    .end local v29           #milis2:J
    .end local v31           #onlineModName:Ljava/lang/String;
    :catch_4b0
    move-exception v39

    goto/16 :goto_165

    .line 93
    :catch_4b3
    move-exception v39

    goto/16 :goto_165

    .line 98
    .restart local v6       #currentconvertedDate:Ljava/util/Date;
    :catch_4b6
    move-exception v39

    goto/16 :goto_170

    .line 99
    :catch_4b9
    move-exception v39

    goto/16 :goto_170
.end method
