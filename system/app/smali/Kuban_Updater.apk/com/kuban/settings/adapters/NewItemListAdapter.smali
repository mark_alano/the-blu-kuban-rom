.class public Lcom/kuban/settings/adapters/NewItemListAdapter;
.super Landroid/widget/BaseAdapter;
.source "NewItemListAdapter.java"


# static fields
.field private static inflater:Landroid/view/LayoutInflater;


# instance fields
.field private activity:Landroid/app/Activity;

.field cacheDirectory:Ljava/io/File;

.field private data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field file:Ljava/io/File;

.field public imageLoader:Lcom/kuban/settings/utils/ImageLoader;

.field reader:Ljava/io/LineNumberReader;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .registers 5
    .parameter "a"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, d:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    .line 34
    iput-object p1, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->activity:Landroid/app/Activity;

    .line 35
    iput-object p2, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->data:Ljava/util/ArrayList;

    .line 36
    iget-object v0, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->activity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sput-object v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 37
    new-instance v0, Lcom/kuban/settings/utils/ImageLoader;

    iget-object v1, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/kuban/settings/utils/ImageLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->imageLoader:Lcom/kuban/settings/utils/ImageLoader;

    .line 38
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/kuban/settings/adapters/NewItemListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 46
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 33
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 49
    move-object/from16 v25, p2

    .line 50
    .local v25, vi:Landroid/view/View;
    if-nez p2, :cond_f

    .line 51
    sget-object v26, Lcom/kuban/settings/adapters/NewItemListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v27, 0x7f03000f

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    .line 52
    :cond_f
    const v26, 0x7f0c001a

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 53
    .local v24, title:Landroid/widget/TextView;
    const v26, 0x7f0c002d

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 54
    .local v5, desc:Landroid/widget/TextView;
    const v26, 0x7f0c0024

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    .line 55
    .local v23, thumb_image:Landroid/widget/ImageView;
    const v26, 0x7f0c002e

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 56
    .local v7, dl_image:Landroid/widget/ImageView;
    const v26, 0x7f0c002f

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 57
    .local v14, install_image:Landroid/widget/ImageView;
    const v26, 0x7f0c0026

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 58
    .local v6, dl:Landroid/widget/TextView;
    const v26, 0x7f0c0027

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 59
    .local v15, installed:Landroid/widget/TextView;
    const v26, 0x7f0c0025

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 60
    .local v18, link:Landroid/widget/TextView;
    const v26, 0x7f0c0010

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 61
    .local v4, date:Landroid/widget/TextView;
    const v26, 0x7f0c0028

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 62
    .local v20, preview:Landroid/widget/TextView;
    const v26, 0x7f0c0029

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 63
    .local v21, preview2:Landroid/widget/TextView;
    const v26, 0x7f0c002a

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 64
    .local v22, preview3:Landroid/widget/TextView;
    const v26, 0x7f0c002b

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 65
    .local v13, folder:Landroid/widget/TextView;
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 66
    .local v17, item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->data:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17           #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v17, Ljava/util/HashMap;

    .line 67
    .restart local v17       #item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v27, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v28

    const-string v26, "Folder"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/NewItemListAdapter;->cacheDirectory:Ljava/io/File;

    .line 68
    const-string v26, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    const-string v27, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    const/16 v28, 0x2f

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v27

    add-int/lit8 v28, v27, 0x1

    const-string v27, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    move-object/from16 v0, v26

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 69
    .local v12, fileName:Ljava/lang/String;
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->cacheDirectory:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/NewItemListAdapter;->file:Ljava/io/File;

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->file:Ljava/io/File;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v26

    if-eqz v26, :cond_3a1

    .line 71
    const v26, 0x7f020018

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    const-string v8, "Yes"

    .line 73
    .local v8, downloaded:Ljava/lang/String;
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :goto_122
    const-string v26, ".zip"

    const-string v27, ""

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 82
    .local v19, onlineModName:Ljava/lang/String;
    :try_start_12e
    new-instance v26, Ljava/io/LineNumberReader;

    new-instance v27, Ljava/io/FileReader;

    const-string v28, "/data/mods.txt"

    invoke-direct/range {v27 .. v28}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v26 .. v27}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    .line 84
    const-string v3, "No"

    .line 85
    .local v3, caught:Ljava/lang/String;
    :cond_142
    :goto_142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_14b
    .catchall {:try_start_12e .. :try_end_14b} :catchall_429
    .catch Ljava/lang/Exception; {:try_start_12e .. :try_end_14b} :catch_40f

    move-result-object v11

    .local v11, fileModLine:Ljava/lang/String;
    if-nez v11, :cond_3b0

    .line 102
    :try_start_14e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v26, v0

    if-eqz v26, :cond_15f

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/LineNumberReader;->close()V
    :try_end_15f
    .catch Ljava/io/IOException; {:try_start_14e .. :try_end_15f} :catch_441

    .line 109
    .end local v3           #caught:Ljava/lang/String;
    .end local v11           #fileModLine:Ljava/lang/String;
    :cond_15f
    :goto_15f
    const-string v26, "Date"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_18e

    const-string v26, "Date"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_18e

    .line 110
    const-string v26, "Date"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :cond_18e
    const-string v26, "Name"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_1bf

    const-string v26, "Name"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_1bf

    .line 113
    const-string v26, "Name"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1bf
    const-string v26, "Desc"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_1ee

    const-string v26, "Desc"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_1ee

    .line 116
    const-string v26, "Desc"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    :cond_1ee
    const-string v26, "Desc"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_207

    .line 119
    const-string v26, " "

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_207
    const-string v26, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_238

    const-string v26, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_238

    .line 122
    const-string v26, "Url"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :cond_238
    const-string v26, "Preview"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_269

    const-string v26, "Preview"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_269

    .line 125
    const-string v26, "Preview"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    :cond_269
    const-string v26, "Preview"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_284

    .line 128
    const-string v26, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_284
    const-string v26, "Preview2"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_2b5

    const-string v26, "Preview2"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_2b5

    .line 131
    const-string v26, "Preview2"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :cond_2b5
    const-string v26, "Preview2"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_2d0

    .line 134
    const-string v26, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :cond_2d0
    const-string v26, "Preview3"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_301

    const-string v26, "Preview3"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_301

    .line 137
    const-string v26, "Preview3"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :cond_301
    const-string v26, "Preview3"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_31c

    .line 140
    const-string v26, "http://downloads.teamvenum.com/blu_kuban_updater/icons/none.png"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :cond_31c
    const-string v26, "Folder"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_34b

    const-string v26, "Folder"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_34b

    .line 143
    const-string v26, "Folder"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :cond_34b
    const-string v26, "ThumbnailUrl"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_384

    const-string v26, "ThumbnailUrl"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_384

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->imageLoader:Lcom/kuban/settings/utils/ImageLoader;

    move-object/from16 v27, v0

    const-string v26, "ThumbnailUrl"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/kuban/settings/utils/ImageLoader;->DisplayImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 148
    :cond_384
    const-string v26, "ThumbnailUrl"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_3a0

    .line 149
    const v26, 0x7f02001f

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 151
    :cond_3a0
    return-object v25

    .line 76
    .end local v8           #downloaded:Ljava/lang/String;
    .end local v19           #onlineModName:Ljava/lang/String;
    :cond_3a1
    const v26, 0x7f020041

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    const-string v8, "No"

    .line 78
    .restart local v8       #downloaded:Ljava/lang/String;
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_122

    .line 86
    .restart local v3       #caught:Ljava/lang/String;
    .restart local v11       #fileModLine:Ljava/lang/String;
    .restart local v19       #onlineModName:Ljava/lang/String;
    :cond_3b0
    const/16 v26, 0x0

    :try_start_3b2
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x2

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 87
    .local v10, fileMod:Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3f3

    .line 88
    const/16 v26, 0x3d

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v26

    add-int/lit8 v26, v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 89
    const-string v26, "1"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_142

    .line 90
    const v26, 0x7f02003d

    move/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    const-string v16, "Yes"

    .line 92
    .local v16, isInstalled:Ljava/lang/String;
    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const-string v3, "1"

    goto/16 :goto_142

    .line 95
    .end local v16           #isInstalled:Ljava/lang/String;
    :cond_3f3
    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_142

    const-string v26, "1"

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_142

    .line 96
    const v26, 0x7f020041

    move/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_40d
    .catchall {:try_start_3b2 .. :try_end_40d} :catchall_429
    .catch Ljava/lang/Exception; {:try_start_3b2 .. :try_end_40d} :catch_40f

    goto/16 :goto_142

    .line 99
    .end local v3           #caught:Ljava/lang/String;
    .end local v10           #fileMod:Ljava/lang/String;
    .end local v11           #fileModLine:Ljava/lang/String;
    :catch_40f
    move-exception v26

    .line 102
    :try_start_410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v26, v0

    if-eqz v26, :cond_15f

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/LineNumberReader;->close()V
    :try_end_421
    .catch Ljava/io/IOException; {:try_start_410 .. :try_end_421} :catch_423

    goto/16 :goto_15f

    .line 105
    :catch_423
    move-exception v9

    .line 106
    .local v9, ex:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_15f

    .line 100
    .end local v9           #ex:Ljava/io/IOException;
    :catchall_429
    move-exception v26

    .line 102
    :try_start_42a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v27, v0

    if-eqz v27, :cond_43b

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/adapters/NewItemListAdapter;->reader:Ljava/io/LineNumberReader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/LineNumberReader;->close()V
    :try_end_43b
    .catch Ljava/io/IOException; {:try_start_42a .. :try_end_43b} :catch_43c

    .line 108
    :cond_43b
    :goto_43b
    throw v26

    .line 105
    :catch_43c
    move-exception v9

    .line 106
    .restart local v9       #ex:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_43b

    .line 105
    .end local v9           #ex:Ljava/io/IOException;
    .restart local v3       #caught:Ljava/lang/String;
    .restart local v11       #fileModLine:Ljava/lang/String;
    :catch_441
    move-exception v9

    .line 106
    .restart local v9       #ex:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_15f
.end method
