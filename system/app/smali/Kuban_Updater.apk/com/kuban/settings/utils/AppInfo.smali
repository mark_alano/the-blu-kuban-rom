.class public Lcom/kuban/settings/utils/AppInfo;
.super Ljava/lang/Object;
.source "AppInfo.java"


# instance fields
.field private Name:Ljava/lang/String;

.field public filtered:Z

.field public icon:Landroid/graphics/drawable/Drawable;

.field public intent:Landroid/content/Intent;

.field private packageName:Ljava/lang/String;

.field public title:Ljava/lang/CharSequence;

.field private versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public setActivity(Landroid/content/ComponentName;I)V
    .registers 5
    .parameter "className"
    .parameter "launchFlags"

    .prologue
    .line 17
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->intent:Landroid/content/Intent;

    .line 18
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->intent:Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 19
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 20
    iget-object v0, p0, Lcom/kuban/settings/utils/AppInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 21
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .parameter "Name"

    .prologue
    .line 32
    iput-object p1, p0, Lcom/kuban/settings/utils/AppInfo;->Name:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .registers 2
    .parameter "packageName"

    .prologue
    .line 26
    iput-object p1, p0, Lcom/kuban/settings/utils/AppInfo;->packageName:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setVersionName(Ljava/lang/String;)V
    .registers 2
    .parameter "versionName"

    .prologue
    .line 38
    iput-object p1, p0, Lcom/kuban/settings/utils/AppInfo;->versionName:Ljava/lang/String;

    .line 39
    return-void
.end method
