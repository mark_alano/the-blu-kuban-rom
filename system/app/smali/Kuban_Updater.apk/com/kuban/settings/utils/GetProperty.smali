.class public Lcom/kuban/settings/utils/GetProperty;
.super Ljava/lang/Object;
.source "GetProperty.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSystemProperty(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "propName"

    .prologue
    .line 11
    const-string v0, "GetProp"

    .line 13
    .local v0, TAGPROP:Ljava/lang/String;
    const/4 v3, 0x0

    .line 15
    .local v3, input:Ljava/io/BufferedReader;
    :try_start_3
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "getprop "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v6

    .line 16
    .local v6, p:Ljava/lang/Process;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v6}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v8, 0x400

    invoke-direct {v4, v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_59
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_2a} :catch_38

    .line 17
    .end local v3           #input:Ljava/io/BufferedReader;
    .local v4, input:Ljava/io/BufferedReader;
    :try_start_2a
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 18
    .local v5, line:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_6e
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_31} :catch_71

    .line 25
    if-eqz v4, :cond_36

    .line 27
    :try_start_33
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_36} :catch_67

    :cond_36
    :goto_36
    move-object v3, v4

    .line 34
    .end local v4           #input:Ljava/io/BufferedReader;
    .end local v5           #line:Ljava/lang/String;
    .end local v6           #p:Ljava/lang/Process;
    .restart local v3       #input:Ljava/io/BufferedReader;
    :goto_37
    return-object v5

    .line 20
    :catch_38
    move-exception v2

    .line 21
    .local v2, ex:Ljava/io/IOException;
    :goto_39
    :try_start_39
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to read sysprop "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4b
    .catchall {:try_start_39 .. :try_end_4b} :catchall_59

    .line 25
    if-eqz v3, :cond_50

    .line 27
    :try_start_4d
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_50} :catch_52

    .line 22
    :cond_50
    :goto_50
    const/4 v5, 0x0

    goto :goto_37

    .line 29
    :catch_52
    move-exception v1

    .line 30
    .local v1, e:Ljava/io/IOException;
    const-string v7, "Exception while closing InputStream"

    invoke-static {v0, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_50

    .line 24
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #ex:Ljava/io/IOException;
    :catchall_59
    move-exception v7

    .line 25
    :goto_5a
    if-eqz v3, :cond_5f

    .line 27
    :try_start_5c
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5f
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_5f} :catch_60

    .line 33
    :cond_5f
    :goto_5f
    throw v7

    .line 29
    :catch_60
    move-exception v1

    .line 30
    .restart local v1       #e:Ljava/io/IOException;
    const-string v8, "Exception while closing InputStream"

    invoke-static {v0, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5f

    .line 29
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #input:Ljava/io/BufferedReader;
    .restart local v4       #input:Ljava/io/BufferedReader;
    .restart local v5       #line:Ljava/lang/String;
    .restart local v6       #p:Ljava/lang/Process;
    :catch_67
    move-exception v1

    .line 30
    .restart local v1       #e:Ljava/io/IOException;
    const-string v7, "Exception while closing InputStream"

    invoke-static {v0, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_36

    .line 24
    .end local v1           #e:Ljava/io/IOException;
    .end local v5           #line:Ljava/lang/String;
    :catchall_6e
    move-exception v7

    move-object v3, v4

    .end local v4           #input:Ljava/io/BufferedReader;
    .restart local v3       #input:Ljava/io/BufferedReader;
    goto :goto_5a

    .line 20
    .end local v3           #input:Ljava/io/BufferedReader;
    .restart local v4       #input:Ljava/io/BufferedReader;
    :catch_71
    move-exception v2

    move-object v3, v4

    .end local v4           #input:Ljava/io/BufferedReader;
    .restart local v3       #input:Ljava/io/BufferedReader;
    goto :goto_39
.end method
