.class public Lcom/kuban/settings/utils/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;,
        Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;,
        Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;
    }
.end annotation


# instance fields
.field executorService:Ljava/util/concurrent/ExecutorService;

.field fileCache:Lcom/kuban/settings/utils/FileCache;

.field private imageViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/widget/ImageView;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field memoryCache:Lcom/kuban/settings/utils/MemoryCache;

.field final stub_id:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/kuban/settings/utils/MemoryCache;

    invoke-direct {v0}, Lcom/kuban/settings/utils/MemoryCache;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->memoryCache:Lcom/kuban/settings/utils/MemoryCache;

    .line 29
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->imageViews:Ljava/util/Map;

    .line 36
    const v0, 0x7f02001f

    iput v0, p0, Lcom/kuban/settings/utils/ImageLoader;->stub_id:I

    .line 33
    new-instance v0, Lcom/kuban/settings/utils/FileCache;

    invoke-direct {v0, p1}, Lcom/kuban/settings/utils/FileCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->fileCache:Lcom/kuban/settings/utils/FileCache;

    .line 34
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/kuban/settings/utils/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/kuban/settings/utils/ImageLoader;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    .registers 12
    .parameter "f"

    .prologue
    const/16 v9, 0x48

    const/4 v6, 0x0

    .line 76
    :try_start_3
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 77
    .local v2, o:Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x0

    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 78
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v8, 0x0

    invoke-static {v7, v8, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 79
    const/16 v0, 0x48

    .line 80
    .local v0, REQUIRED_SIZE:I
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .local v5, width_tmp:I
    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 81
    .local v1, height_tmp:I
    const/4 v4, 0x1

    .line 83
    .local v4, scale:I
    :goto_1b
    div-int/lit8 v7, v5, 0x2

    if-lt v7, v9, :cond_23

    div-int/lit8 v7, v1, 0x2

    if-ge v7, v9, :cond_35

    .line 89
    :cond_23
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 90
    .local v3, o2:Landroid/graphics/BitmapFactory$Options;
    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 91
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v8, 0x0

    invoke-static {v7, v8, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 93
    .end local v0           #REQUIRED_SIZE:I
    .end local v1           #height_tmp:I
    .end local v2           #o:Landroid/graphics/BitmapFactory$Options;
    .end local v3           #o2:Landroid/graphics/BitmapFactory$Options;
    .end local v4           #scale:I
    .end local v5           #width_tmp:I
    :goto_34
    return-object v6

    .line 85
    .restart local v0       #REQUIRED_SIZE:I
    .restart local v1       #height_tmp:I
    .restart local v2       #o:Landroid/graphics/BitmapFactory$Options;
    .restart local v4       #scale:I
    .restart local v5       #width_tmp:I
    :cond_35
    div-int/lit8 v5, v5, 0x2

    .line 86
    div-int/lit8 v1, v1, 0x2
    :try_end_39
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_39} :catch_3c

    .line 87
    mul-int/lit8 v4, v4, 0x2

    .line 82
    goto :goto_1b

    .line 92
    .end local v0           #REQUIRED_SIZE:I
    .end local v1           #height_tmp:I
    .end local v2           #o:Landroid/graphics/BitmapFactory$Options;
    .end local v4           #scale:I
    .end local v5           #width_tmp:I
    :catch_3c
    move-exception v7

    goto :goto_34
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 11
    .parameter "url"

    .prologue
    .line 52
    iget-object v8, p0, Lcom/kuban/settings/utils/ImageLoader;->fileCache:Lcom/kuban/settings/utils/FileCache;

    invoke-virtual {v8, p1}, Lcom/kuban/settings/utils/FileCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 53
    .local v4, f:Ljava/io/File;
    invoke-direct {p0, v4}, Lcom/kuban/settings/utils/ImageLoader;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 54
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_d

    .line 71
    .end local v0           #b:Landroid/graphics/Bitmap;
    :goto_c
    return-object v0

    .line 57
    .restart local v0       #b:Landroid/graphics/Bitmap;
    :cond_d
    const/4 v1, 0x0

    .line 58
    .local v1, bitmap:Landroid/graphics/Bitmap;
    :try_start_e
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 59
    .local v5, imageUrl:Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 60
    .local v2, conn:Ljava/net/HttpURLConnection;
    const/16 v8, 0x7530

    invoke-virtual {v2, v8}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 61
    const/16 v8, 0x7530

    invoke-virtual {v2, v8}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 62
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 63
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 64
    .local v6, is:Ljava/io/InputStream;
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 65
    .local v7, os:Ljava/io/OutputStream;
    invoke-static {v6, v7}, Lcom/kuban/settings/utils/Utils;->CopyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 66
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 67
    invoke-direct {p0, v4}, Lcom/kuban/settings/utils/ImageLoader;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_39} :catch_3c

    move-result-object v1

    move-object v0, v1

    .line 68
    goto :goto_c

    .line 69
    .end local v2           #conn:Ljava/net/HttpURLConnection;
    .end local v5           #imageUrl:Ljava/net/URL;
    .end local v6           #is:Ljava/io/InputStream;
    .end local v7           #os:Ljava/io/OutputStream;
    :catch_3c
    move-exception v3

    .line 70
    .local v3, ex:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 71
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private queuePhoto(Ljava/lang/String;Landroid/widget/ImageView;)V
    .registers 6
    .parameter "url"
    .parameter "imageView"

    .prologue
    .line 48
    new-instance v0, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    invoke-direct {v0, p0, p1, p2}, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;-><init>(Lcom/kuban/settings/utils/ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 49
    .local v0, p:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;
    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;

    invoke-direct {v2, p0, v0}, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;-><init>(Lcom/kuban/settings/utils/ImageLoader;Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 50
    return-void
.end method


# virtual methods
.method public DisplayImage(Ljava/lang/String;Landroid/widget/ImageView;)V
    .registers 5
    .parameter "url"
    .parameter "imageView"

    .prologue
    .line 38
    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader;->imageViews:Ljava/util/Map;

    invoke-interface {v1, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader;->memoryCache:Lcom/kuban/settings/utils/MemoryCache;

    invoke-virtual {v1, p1}, Lcom/kuban/settings/utils/MemoryCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 40
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_11

    .line 41
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 46
    :goto_10
    return-void

    .line 43
    :cond_11
    invoke-direct {p0, p1, p2}, Lcom/kuban/settings/utils/ImageLoader;->queuePhoto(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 44
    const v1, 0x7f02001f

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_10
.end method

.method public clearCache()V
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->memoryCache:Lcom/kuban/settings/utils/MemoryCache;

    invoke-virtual {v0}, Lcom/kuban/settings/utils/MemoryCache;->clear()V

    .line 142
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader;->fileCache:Lcom/kuban/settings/utils/FileCache;

    invoke-virtual {v0}, Lcom/kuban/settings/utils/FileCache;->clear()V

    .line 143
    return-void
.end method

.method imageViewReused(Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)Z
    .registers 5
    .parameter "photoToLoad"

    .prologue
    .line 122
    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader;->imageViews:Ljava/util/Map;

    iget-object v2, p1, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123
    .local v0, tag:Ljava/lang/String;
    if-eqz v0, :cond_14

    iget-object v1, p1, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 124
    :cond_14
    const/4 v1, 0x1

    .line 125
    :goto_15
    return v1

    :cond_16
    const/4 v1, 0x0

    goto :goto_15
.end method
