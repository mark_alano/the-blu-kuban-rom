.class public Lcom/kuban/settings/utils/ItemCheck;
.super Landroid/app/Service;
.source "ItemCheck.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/utils/ItemCheck$getItems;
    }
.end annotation


# static fields
.field static final DAYS_DIFF:Ljava/lang/String; = "604800000"

.field static final ELEMENT_ITEM:Ljava/lang/String; = "Element"

.field static final KEY_ITEM:Ljava/lang/String; = "Tab"

.field public static final KEY_ITEM_DESC:Ljava/lang/String; = "Desc"

.field static final KEY_ITEM_DL:Ljava/lang/String; = "No"

.field static final KEY_ITEM_FOLDER:Ljava/lang/String; = "Folder"

.field public static final KEY_ITEM_INSTALL:Ljava/lang/String; = "No"

.field public static final KEY_ITEM_NAME:Ljava/lang/String; = "Name"

.field public static final KEY_ITEM_NEW:Ljava/lang/String; = "Date"

.field public static final KEY_ITEM_PREVIEW:Ljava/lang/String; = "Preview"

.field public static final KEY_ITEM_PREVIEW2:Ljava/lang/String; = "Preview2"

.field public static final KEY_ITEM_PREVIEW3:Ljava/lang/String; = "Preview3"

.field static final KEY_ITEM_THUMB:Ljava/lang/String; = "ThumbnailUrl"

.field public static final KEY_ITEM_URL:Ljava/lang/String; = "Url"

.field static final KEY_URL:Ljava/lang/String; = "Url"


# instance fields
.field ItemCheckInterval:Ljava/lang/String;

.field ItemCheckIntervalTime:I

.field ItemCheckOn:Z

.field Items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field TAG:Ljava/lang/String;

.field eventsData:Lcom/kuban/settings/utils/NewItemDatabaseHelper;

.field private handler:Landroid/os/Handler;

.field itemName:Ljava/lang/String;

.field private runnable:Ljava/lang/Runnable;

.field running:I

.field time:J


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->Items:Ljava/util/ArrayList;

    .line 56
    const-string v0, "Item Check Service: "

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->handler:Landroid/os/Handler;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/kuban/settings/utils/ItemCheck;->running:I

    .line 75
    new-instance v0, Lcom/kuban/settings/utils/ItemCheck$1;

    invoke-direct {v0, p0}, Lcom/kuban/settings/utils/ItemCheck$1;-><init>(Lcom/kuban/settings/utils/ItemCheck;)V

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->runnable:Ljava/lang/Runnable;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/kuban/settings/utils/ItemCheck;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/kuban/settings/utils/ItemCheck;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->runnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public checkTimer()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/kuban/settings/utils/ItemCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 87
    .local v0, prefs:Landroid/content/SharedPreferences;
    const-string v1, "newItemCheck"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckOn:Z

    .line 88
    iget-boolean v1, p0, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckOn:Z

    if-eqz v1, :cond_24

    .line 89
    new-instance v1, Lcom/kuban/settings/utils/ItemCheck$getItems;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/kuban/settings/utils/ItemCheck$getItems;-><init>(Lcom/kuban/settings/utils/ItemCheck;Lcom/kuban/settings/utils/ItemCheck$getItems;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/kuban/settings/utils/ItemCheck$getItems;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 90
    const/4 v1, 0x1

    iput v1, p0, Lcom/kuban/settings/utils/ItemCheck;->running:I

    .line 94
    :goto_23
    return-void

    .line 92
    :cond_24
    iput v3, p0, Lcom/kuban/settings/utils/ItemCheck;->running:I

    goto :goto_23
.end method

.method public notificationmultiple(Ljava/util/ArrayList;)V
    .registers 39
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v6, Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/kuban/settings/utils/NewItemDatabaseHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->eventsData:Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    .line 244
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->eventsData:Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    invoke-virtual {v6}, Lcom/kuban/settings/utils/NewItemDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 245
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v33, web:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v19, database:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_21
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_5c

    .line 251
    const-string v6, "events"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 252
    .local v18, cursor:Landroid/database/Cursor;
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 253
    :goto_36
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-eqz v6, :cond_7a

    .line 257
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 258
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    new-array v0, v6, [Ljava/lang/String;

    move-object/from16 v29, v0

    .line 259
    .local v29, simpleArray:[Ljava/lang/String;
    move-object/from16 v0, v33

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 260
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_52
    :goto_52
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_8a

    .line 292
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 293
    return-void

    .line 247
    .end local v18           #cursor:Landroid/database/Cursor;
    .end local v29           #simpleArray:[Ljava/lang/String;
    :cond_5c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/HashMap;

    .line 248
    .local v28, s:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "Name"

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->itemName:Ljava/lang/String;

    .line 249
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->itemName:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 254
    .end local v28           #s:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v18       #cursor:Landroid/database/Cursor;
    :cond_7a
    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_36

    .line 260
    .restart local v29       #simpleArray:[Ljava/lang/String;
    :cond_8a
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 261
    .local v34, webItem:Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_52

    .line 264
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v20

    .line 265
    .local v20, delete:Ljava/util/Calendar;
    new-instance v7, Ljava/lang/StringBuilder;

    const/4 v8, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x5

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, -0x5

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 266
    .local v13, Delete:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const/4 v8, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x5

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 267
    .local v26, now:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DELETE FROM events WHERE age <= "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 268
    .local v30, sql:Ljava/lang/String;
    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 269
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 270
    .local v32, values:Landroid/content/ContentValues;
    const-string v7, "item"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v7, "age"

    move-object/from16 v0, v32

    move-object/from16 v1, v26

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v7, "events"

    const/4 v8, 0x0

    move-object/from16 v0, v32

    invoke-virtual {v5, v7, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 273
    const/16 v23, 0x1

    .line 274
    .local v23, not_ID:I
    const-string v27, "notification"

    .line 275
    .local v27, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/ItemCheck;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/app/NotificationManager;

    .line 276
    .local v22, mNotificationManager:Landroid/app/NotificationManager;
    const v21, 0x7f02001d

    .line 277
    .local v21, icon:I
    const-string v31, "New Items are ready to download"

    .line 278
    .local v31, tickerText:Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    .line 279
    .local v35, when:J
    new-instance v24, Landroid/app/Notification;

    move-object/from16 v0, v24

    move/from16 v1, v21

    move-object/from16 v2, v31

    move-wide/from16 v3, v35

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 280
    .local v24, notification:Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/utils/ItemCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    .line 281
    .local v17, context:Landroid/content/Context;
    const-string v16, "Blu Kuban Updater"

    .line 282
    .local v16, contentTitle:Ljava/lang/CharSequence;
    const-string v15, "There are multiple new items"

    .line 283
    .local v15, contentText:Ljava/lang/CharSequence;
    new-instance v25, Landroid/content/Intent;

    const-class v7, Lcom/kuban/settings/NewestMods;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 284
    .local v25, notificationIntent:Landroid/content/Intent;
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-static {v0, v7, v1, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v14

    .line 285
    .local v14, contentIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2, v15, v14}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 286
    move-object/from16 v0, v24

    iget v7, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v24

    iput v7, v0, Landroid/app/Notification;->flags:I

    .line 287
    move-object/from16 v0, v24

    iget v7, v0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v24

    iput v7, v0, Landroid/app/Notification;->defaults:I

    .line 288
    move-object/from16 v0, v24

    iget v7, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x10

    move-object/from16 v0, v24

    iput v7, v0, Landroid/app/Notification;->flags:I

    .line 289
    const/4 v7, 0x1

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_52
.end method

.method public notificationsingle(Ljava/util/ArrayList;)V
    .registers 44
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Ljava/util/HashMap;

    .line 185
    .local v36, singleItemArray:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "Name"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 186
    .local v25, itemName:Ljava/lang/String;
    const-string v6, "Desc"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 187
    .local v22, description:Ljava/lang/String;
    const-string v6, "No"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 188
    .local v24, install:Ljava/lang/String;
    const-string v6, "Url"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 189
    .local v26, link:Ljava/lang/String;
    const-string v6, "Date"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 190
    .local v20, date:Ljava/lang/String;
    const-string v6, "Preview"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/lang/String;

    .line 191
    .local v33, preview:Ljava/lang/String;
    const-string v6, "Preview2"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 192
    .local v34, preview2:Ljava/lang/String;
    const-string v6, "Preview3"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    .line 193
    .local v35, preview3:Ljava/lang/String;
    new-instance v6, Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/kuban/settings/utils/NewItemDatabaseHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->eventsData:Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    .line 194
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/kuban/settings/utils/ItemCheck;->eventsData:Lcom/kuban/settings/utils/NewItemDatabaseHelper;

    invoke-virtual {v6}, Lcom/kuban/settings/utils/NewItemDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 195
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v19, database:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, "events"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 197
    .local v18, cursor:Landroid/database/Cursor;
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 198
    :goto_80
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-eqz v6, :cond_205

    .line 202
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 203
    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_204

    .line 205
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v21

    .line 206
    .local v21, delete:Ljava/util/Calendar;
    new-instance v6, Ljava/lang/StringBuilder;

    const/4 v7, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x5

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, -0x5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 207
    .local v13, Delete:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const/4 v7, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x5

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 208
    .local v31, now:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DELETE FROM events WHERE age <= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    .line 209
    .local v37, sql:Ljava/lang/String;
    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 210
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 211
    .local v39, values:Landroid/content/ContentValues;
    const-string v6, "item"

    move-object/from16 v0, v39

    move-object/from16 v1, v25

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v6, "age"

    move-object/from16 v0, v39

    move-object/from16 v1, v31

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v6, "events"

    const/4 v7, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v5, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 214
    const/16 v28, 0x1

    .line 215
    .local v28, not_ID:I
    const-string v32, "notification"

    .line 216
    .local v32, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/ItemCheck;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/app/NotificationManager;

    .line 217
    .local v27, mNotificationManager:Landroid/app/NotificationManager;
    const v23, 0x7f02001d

    .line 218
    .local v23, icon:I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " is ready to download"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    .line 219
    .local v38, tickerText:Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 220
    .local v40, when:J
    new-instance v29, Landroid/app/Notification;

    move-object/from16 v0, v29

    move/from16 v1, v23

    move-object/from16 v2, v38

    move-wide/from16 v3, v40

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 221
    .local v29, notification:Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/utils/ItemCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    .line 222
    .local v17, context:Landroid/content/Context;
    const-string v16, "Blu Kuban Updater"

    .line 223
    .local v16, contentTitle:Ljava/lang/CharSequence;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " Has been added or updated"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 224
    .local v15, contentText:Ljava/lang/CharSequence;
    new-instance v30, Landroid/content/Intent;

    const-class v6, Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    .local v30, notificationIntent:Landroid/content/Intent;
    const-string v6, "Name"

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string v6, "Desc"

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v6, "No"

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string v6, "Url"

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string v6, "Date"

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const-string v6, "Preview"

    move-object/from16 v0, v30

    move-object/from16 v1, v33

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string v6, "Preview2"

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const-string v6, "Preview3"

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-static {v0, v6, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v14

    .line 234
    .local v14, contentIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, v29

    move-object/from16 v1, v17

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2, v15, v14}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 235
    move-object/from16 v0, v29

    iget v6, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x1

    move-object/from16 v0, v29

    iput v6, v0, Landroid/app/Notification;->flags:I

    .line 236
    move-object/from16 v0, v29

    iget v6, v0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    move-object/from16 v0, v29

    iput v6, v0, Landroid/app/Notification;->defaults:I

    .line 237
    move-object/from16 v0, v29

    iget v6, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x10

    move-object/from16 v0, v29

    iput v6, v0, Landroid/app/Notification;->flags:I

    .line 238
    const/4 v6, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 240
    .end local v13           #Delete:Ljava/lang/String;
    .end local v14           #contentIntent:Landroid/app/PendingIntent;
    .end local v15           #contentText:Ljava/lang/CharSequence;
    .end local v16           #contentTitle:Ljava/lang/CharSequence;
    .end local v17           #context:Landroid/content/Context;
    .end local v21           #delete:Ljava/util/Calendar;
    .end local v23           #icon:I
    .end local v27           #mNotificationManager:Landroid/app/NotificationManager;
    .end local v28           #not_ID:I
    .end local v29           #notification:Landroid/app/Notification;
    .end local v30           #notificationIntent:Landroid/content/Intent;
    .end local v31           #now:Ljava/lang/String;
    .end local v32           #ns:Ljava/lang/String;
    .end local v37           #sql:Ljava/lang/String;
    .end local v38           #tickerText:Ljava/lang/CharSequence;
    .end local v39           #values:Landroid/content/ContentValues;
    .end local v40           #when:J
    :cond_204
    return-void

    .line 199
    :cond_205
    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_80
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .registers 1

    .prologue
    .line 67
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 7
    .parameter "intent"
    .parameter "startid"

    .prologue
    .line 70
    sget-wide v0, Lcom/kuban/settings/Home;->time:J

    iput-wide v0, p0, Lcom/kuban/settings/utils/ItemCheck;->time:J

    .line 71
    iget v0, p0, Lcom/kuban/settings/utils/ItemCheck;->running:I

    if-nez v0, :cond_11

    .line 72
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 74
    :cond_11
    return-void
.end method
