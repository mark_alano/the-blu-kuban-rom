.class public Lcom/kuban/settings/utils/NewItemDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "NewItemDatabaseHelper.java"


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "newitem.db"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final KEY_AGE:Ljava/lang/String; = "age"

.field public static final KEY_ID:Ljava/lang/String; = "id"

.field public static final KEY_NAME:Ljava/lang/String; = "item"

.field public static final TABLE:Ljava/lang/String; = "events"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 17
    const-string v0, "newitem.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 18
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    .prologue
    .line 21
    const-string v0, "CREATE TABLE events(id INTEGER PRIMARY KEY,item TEXT,age TEXT)"

    .line 24
    .local v0, CREATE_ITEM_TABLE:Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 28
    const-string v0, "DROP TABLE IF EXISTS events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, p1}, Lcom/kuban/settings/utils/NewItemDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 30
    return-void
.end method
