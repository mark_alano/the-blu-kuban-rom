.class Lcom/kuban/settings/utils/RomCheck$1;
.super Ljava/lang/Object;
.source "RomCheck.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/utils/RomCheck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/utils/RomCheck;


# direct methods
.method constructor <init>(Lcom/kuban/settings/utils/RomCheck;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 52
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/RomCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 53
    .local v0, prefs:Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    const-string v2, "romCheck_Interval"

    const-string v3, "54000000"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckInterval:Ljava/lang/String;

    .line 54
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget-object v2, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget-object v2, v2, Lcom/kuban/settings/utils/RomCheck;->RomCheckInterval:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckIntervalTime:I

    .line 55
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/RomCheck;->checkTimer()V

    .line 56
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    #getter for: Lcom/kuban/settings/utils/RomCheck;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/kuban/settings/utils/RomCheck;->access$0(Lcom/kuban/settings/utils/RomCheck;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    #getter for: Lcom/kuban/settings/utils/RomCheck;->runnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/kuban/settings/utils/RomCheck;->access$1(Lcom/kuban/settings/utils/RomCheck;)Ljava/lang/Runnable;

    move-result-object v2

    iget-object v3, p0, Lcom/kuban/settings/utils/RomCheck$1;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget v3, v3, Lcom/kuban/settings/utils/RomCheck;->RomCheckIntervalTime:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 57
    return-void
.end method
