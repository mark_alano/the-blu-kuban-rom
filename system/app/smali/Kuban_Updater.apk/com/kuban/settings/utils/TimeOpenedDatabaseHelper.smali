.class public Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TimeOpenedDatabaseHelper.java"


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "timeopened.db"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final TABLE:Ljava/lang/String; = "events"

.field public static final TIME:Ljava/lang/String; = "time"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 16
    const-string v0, "timeopened.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 17
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    .prologue
    .line 20
    const-string v0, "create table events( _id integer primary key autoincrement, time integer);"

    .line 22
    .local v0, sql:Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 4
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 27
    return-void
.end method
