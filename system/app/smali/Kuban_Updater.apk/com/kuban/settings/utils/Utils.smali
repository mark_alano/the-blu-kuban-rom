.class public Lcom/kuban/settings/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static CopyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .registers 7
    .parameter "is"
    .parameter "os"

    .prologue
    .line 9
    const/16 v0, 0x400

    .line 12
    .local v0, buffer_size:I
    const/16 v3, 0x400

    :try_start_4
    new-array v1, v3, [B

    .line 15
    .local v1, bytes:[B
    :goto_6
    const/4 v3, 0x0

    const/16 v4, 0x400

    invoke-virtual {p0, v1, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 16
    .local v2, count:I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_11

    .line 22
    .end local v1           #bytes:[B
    .end local v2           #count:I
    :goto_10
    return-void

    .line 18
    .restart local v1       #bytes:[B
    .restart local v2       #count:I
    :cond_11
    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_15} :catch_16

    goto :goto_6

    .line 21
    .end local v1           #bytes:[B
    .end local v2           #count:I
    :catch_16
    move-exception v3

    goto :goto_10
.end method
