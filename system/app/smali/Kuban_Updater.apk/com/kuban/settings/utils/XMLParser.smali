.class public Lcom/kuban/settings/utils/XMLParser;
.super Ljava/lang/Object;
.source "XMLParser.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .registers 10
    .parameter "xml"

    .prologue
    const/4 v5, 0x0

    .line 73
    const/4 v2, 0x0

    .line 74
    .local v2, doc:Lorg/w3c/dom/Document;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 76
    .local v1, dbf:Ljavax/xml/parsers/DocumentBuilderFactory;
    :try_start_6
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 77
    .local v0, db:Ljavax/xml/parsers/DocumentBuilder;
    new-instance v4, Lorg/xml/sax/InputSource;

    invoke-direct {v4}, Lorg/xml/sax/InputSource;-><init>()V

    .line 78
    .local v4, is:Lorg/xml/sax/InputSource;
    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Lorg/xml/sax/InputSource;->setCharacterStream(Ljava/io/Reader;)V

    .line 79
    invoke-virtual {v0, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;
    :try_end_1a
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_6 .. :try_end_1a} :catch_1d
    .catch Lorg/xml/sax/SAXException; {:try_start_6 .. :try_end_1a} :catch_28
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_1a} :catch_33

    move-result-object v2

    move-object v5, v2

    .line 90
    .end local v0           #db:Ljavax/xml/parsers/DocumentBuilder;
    .end local v4           #is:Lorg/xml/sax/InputSource;
    :goto_1c
    return-object v5

    .line 80
    :catch_1d
    move-exception v3

    .line 81
    .local v3, e:Ljavax/xml/parsers/ParserConfigurationException;
    const-string v6, "Error: "

    invoke-virtual {v3}, Ljavax/xml/parsers/ParserConfigurationException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c

    .line 83
    .end local v3           #e:Ljavax/xml/parsers/ParserConfigurationException;
    :catch_28
    move-exception v3

    .line 84
    .local v3, e:Lorg/xml/sax/SAXException;
    const-string v6, "Error: "

    invoke-virtual {v3}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c

    .line 86
    .end local v3           #e:Lorg/xml/sax/SAXException;
    :catch_33
    move-exception v3

    .line 87
    .local v3, e:Ljava/io/IOException;
    const-string v6, "Error: "

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c
.end method

.method public final getElementValue(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .registers 5
    .parameter "elem"

    .prologue
    .line 94
    if-eqz p1, :cond_e

    .line 95
    invoke-interface {p1}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 96
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .local v0, child:Lorg/w3c/dom/Node;
    :goto_c
    if-nez v0, :cond_11

    .line 103
    .end local v0           #child:Lorg/w3c/dom/Node;
    :cond_e
    const-string v1, ""

    :goto_10
    return-object v1

    .line 97
    .restart local v0       #child:Lorg/w3c/dom/Node;
    :cond_11
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1d

    .line 98
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_10

    .line 96
    :cond_1d
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_c
.end method

.method public getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "item"
    .parameter "str"

    .prologue
    .line 106
    invoke-interface {p1, p2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 107
    .local v0, n:Lorg/w3c/dom/NodeList;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/kuban/settings/utils/XMLParser;->getElementValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "url"

    .prologue
    .line 35
    const/4 v7, 0x0

    .line 37
    .local v7, xml:Ljava/lang/String;
    :try_start_1
    new-instance v3, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v3}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 38
    .local v3, httpParameters:Lorg/apache/http/params/HttpParams;
    const/16 v9, 0xbb8

    invoke-static {v3, v9}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 39
    const/16 v9, 0x1388

    invoke-static {v3, v9}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 40
    const/4 v9, 0x1

    invoke-static {v3, v9}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 41
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 42
    .local v1, httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v4, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 43
    .local v4, httpPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v1, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 44
    .local v5, httpResponse:Lorg/apache/http/HttpResponse;
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    .line 45
    .local v6, status:Lorg/apache/http/StatusLine;
    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    const/16 v10, 0xc8

    if-ne v9, v10, :cond_38

    .line 46
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 47
    .local v2, httpEntity:Lorg/apache/http/HttpEntity;
    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 70
    .end local v1           #httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v2           #httpEntity:Lorg/apache/http/HttpEntity;
    .end local v3           #httpParameters:Lorg/apache/http/params/HttpParams;
    .end local v4           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .end local v5           #httpResponse:Lorg/apache/http/HttpResponse;
    .end local v6           #status:Lorg/apache/http/StatusLine;
    .end local v7           #xml:Ljava/lang/String;
    .local v8, xml:Ljava/lang/String;
    :goto_37
    return-object v8

    .line 50
    .end local v8           #xml:Ljava/lang/String;
    .restart local v1       #httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #httpParameters:Lorg/apache/http/params/HttpParams;
    .restart local v4       #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .restart local v5       #httpResponse:Lorg/apache/http/HttpResponse;
    .restart local v6       #status:Lorg/apache/http/StatusLine;
    .restart local v7       #xml:Ljava/lang/String;
    :cond_38
    const-string v7, "0"
    :try_end_3a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_3a} :catch_3c
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_3a} :catch_44
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_3a} :catch_4c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_3a} :catch_54

    move-object v8, v7

    .line 51
    .end local v7           #xml:Ljava/lang/String;
    .restart local v8       #xml:Ljava/lang/String;
    goto :goto_37

    .line 53
    .end local v1           #httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v3           #httpParameters:Lorg/apache/http/params/HttpParams;
    .end local v4           #httpPost:Lorg/apache/http/client/methods/HttpPost;
    .end local v5           #httpResponse:Lorg/apache/http/HttpResponse;
    .end local v6           #status:Lorg/apache/http/StatusLine;
    .end local v8           #xml:Ljava/lang/String;
    .restart local v7       #xml:Ljava/lang/String;
    :catch_3c
    move-exception v0

    .line 54
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 55
    const-string v7, "0"

    move-object v8, v7

    .line 56
    .end local v7           #xml:Ljava/lang/String;
    .restart local v8       #xml:Ljava/lang/String;
    goto :goto_37

    .line 57
    .end local v0           #e:Ljava/io/UnsupportedEncodingException;
    .end local v8           #xml:Ljava/lang/String;
    .restart local v7       #xml:Ljava/lang/String;
    :catch_44
    move-exception v0

    .line 58
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 59
    const-string v7, "0"

    move-object v8, v7

    .line 60
    .end local v7           #xml:Ljava/lang/String;
    .restart local v8       #xml:Ljava/lang/String;
    goto :goto_37

    .line 61
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    .end local v8           #xml:Ljava/lang/String;
    .restart local v7       #xml:Ljava/lang/String;
    :catch_4c
    move-exception v0

    .line 62
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 63
    const-string v7, "0"

    move-object v8, v7

    .line 64
    .end local v7           #xml:Ljava/lang/String;
    .restart local v8       #xml:Ljava/lang/String;
    goto :goto_37

    .line 65
    .end local v0           #e:Ljava/io/IOException;
    .end local v8           #xml:Ljava/lang/String;
    .restart local v7       #xml:Ljava/lang/String;
    :catch_54
    move-exception v0

    .line 66
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    const-string v7, "0"

    move-object v8, v7

    .line 68
    .end local v7           #xml:Ljava/lang/String;
    .restart local v8       #xml:Ljava/lang/String;
    goto :goto_37
.end method
