.class public Lcom/anddoes/launcher/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/anddoes/launcher/a;

.field private static f:Lcom/anddoes/launcher/a;


# instance fields
.field a:Lcom/google/android/apps/analytics/i;

.field private b:Landroid/content/Context;

.field private volatile c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 182
    new-instance v0, Lcom/anddoes/launcher/b;

    invoke-direct {v0}, Lcom/anddoes/launcher/b;-><init>()V

    sput-object v0, Lcom/anddoes/launcher/a;->f:Lcom/anddoes/launcher/a;

    .line 13
    return-void
.end method

.method synthetic constructor <init>()V
    .registers 2

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/a;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/a;->c:Z

    .line 98
    if-nez p1, :cond_9

    .line 106
    :goto_8
    return-void

    .line 104
    :cond_9
    iput-object p1, p0, Lcom/anddoes/launcher/a;->b:Landroid/content/Context;

    .line 105
    invoke-static {}, Lcom/google/android/apps/analytics/i;->a()Lcom/google/android/apps/analytics/i;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/a;->a:Lcom/google/android/apps/analytics/i;

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;)Lcom/anddoes/launcher/a;
    .registers 2
    .parameter

    .prologue
    .line 83
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 84
    sget-object v0, Lcom/anddoes/launcher/a;->f:Lcom/anddoes/launcher/a;

    sput-object v0, Lcom/anddoes/launcher/a;->e:Lcom/anddoes/launcher/a;

    .line 87
    :cond_a
    sget-object v0, Lcom/anddoes/launcher/a;->e:Lcom/anddoes/launcher/a;

    if-nez v0, :cond_1a

    .line 88
    if-nez p0, :cond_13

    .line 89
    sget-object v0, Lcom/anddoes/launcher/a;->f:Lcom/anddoes/launcher/a;

    .line 94
    :goto_12
    return-object v0

    .line 91
    :cond_13
    new-instance v0, Lcom/anddoes/launcher/a;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/anddoes/launcher/a;->e:Lcom/anddoes/launcher/a;

    .line 94
    :cond_1a
    sget-object v0, Lcom/anddoes/launcher/a;->e:Lcom/anddoes/launcher/a;

    goto :goto_12
.end method


# virtual methods
.method public a(Lcom/anddoes/launcher/e;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 176
    iget-object v0, p0, Lcom/anddoes/launcher/a;->a:Lcom/google/android/apps/analytics/i;

    invoke-virtual {p1}, Lcom/anddoes/launcher/e;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Lcom/anddoes/launcher/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/android/apps/analytics/i;->a(ILjava/lang/String;Ljava/lang/String;)Z

    .line 177
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/anddoes/launcher/a;->b:Landroid/content/Context;

    if-eqz v0, :cond_12

    .line 128
    iget-object v0, p0, Lcom/anddoes/launcher/a;->a:Lcom/google/android/apps/analytics/i;

    iget-object v1, p0, Lcom/anddoes/launcher/a;->b:Landroid/content/Context;

    const v2, 0x7f070272

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/analytics/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_12
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->c:Z

    if-eqz v0, :cond_14

    .line 157
    new-instance v0, Lcom/anddoes/launcher/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/d;-><init>(Lcom/anddoes/launcher/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 171
    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 173
    :cond_14
    return-void
.end method

.method public a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/anddoes/launcher/a;->c:Z

    .line 110
    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->c:Z

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->d:Z

    if-nez v0, :cond_17

    .line 112
    iget-object v0, p0, Lcom/anddoes/launcher/a;->a:Lcom/google/android/apps/analytics/i;

    const-string v1, "UA-23723713-6"

    .line 113
    iget-object v2, p0, Lcom/anddoes/launcher/a;->b:Landroid/content/Context;

    .line 112
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/analytics/i;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/a;->d:Z

    .line 121
    :cond_16
    :goto_16
    return-void

    .line 115
    :cond_17
    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->c:Z

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->d:Z

    if-eqz v0, :cond_16

    .line 117
    invoke-static {}, Lcom/google/android/apps/analytics/i;->a()Lcom/google/android/apps/analytics/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/i;->b()Z

    .line 118
    invoke-static {}, Lcom/google/android/apps/analytics/i;->a()Lcom/google/android/apps/analytics/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/i;->d()V

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/anddoes/launcher/a;->d:Z

    goto :goto_16
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/anddoes/launcher/a;->c:Z

    if-eqz v0, :cond_f

    .line 136
    new-instance v0, Lcom/anddoes/launcher/c;

    invoke-direct {v0, p0, p1}, Lcom/anddoes/launcher/c;-><init>(Lcom/anddoes/launcher/a;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 148
    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 150
    :cond_f
    return-void
.end method
