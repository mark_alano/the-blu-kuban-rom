.class public abstract Lcom/anddoes/launcher/c/i;
.super Lcom/anddoes/launcher/c/g;
.source "SourceFile"


# instance fields
.field protected g:Z

.field protected h:Landroid/content/Context;

.field protected i:Ljava/lang/String;

.field protected j:Ljava/lang/String;

.field protected k:Landroid/graphics/drawable/Drawable;

.field protected l:Landroid/graphics/drawable/Drawable;

.field protected m:Ljava/lang/String;

.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/String;

.field protected p:Ljava/util/List;

.field protected q:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/anddoes/launcher/c/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->p:Ljava/util/List;

    .line 56
    iput v2, p0, Lcom/anddoes/launcher/c/i;->q:I

    .line 69
    iput-object p1, p0, Lcom/anddoes/launcher/c/i;->h:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/anddoes/launcher/c/i;->i:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/anddoes/launcher/c/i;->j:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->k:Landroid/graphics/drawable/Drawable;

    .line 73
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2e

    const-string v0, "default"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 74
    :cond_2e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/c/i;->g:Z

    .line 75
    const v0, 0x7f07010e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->j:Ljava/lang/String;

    .line 76
    const v0, 0x7f07010f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->o:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->k:Landroid/graphics/drawable/Drawable;

    .line 78
    const-string v0, "Android Does"

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->m:Ljava/lang/String;

    .line 83
    :goto_54
    return-void

    .line 80
    :cond_55
    iput-boolean v2, p0, Lcom/anddoes/launcher/c/i;->g:Z

    .line 81
    invoke-virtual {p0}, Lcom/anddoes/launcher/c/i;->a()V

    goto :goto_54
.end method

.method protected static a(Landroid/content/pm/PackageManager;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 292
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 293
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 307
    return-object v2

    .line 293
    :cond_10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 295
    :try_start_16
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 296
    invoke-virtual {p0, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v5

    .line 297
    const-string v1, "array"

    invoke-virtual {v5, p2, v1, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 298
    if-nez v1, :cond_2c

    .line 299
    const-string v1, "array"

    invoke-virtual {v5, p3, v1, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 301
    :cond_2c
    if-eqz v1, :cond_9

    .line 302
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_31} :catch_32

    goto :goto_9

    :catch_32
    move-exception v0

    goto :goto_9
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/anddoes/launcher/c/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_8

    .line 90
    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->j:Ljava/lang/String;

    .line 92
    :cond_8
    iget-object v1, p0, Lcom/anddoes/launcher/c/i;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 94
    :try_start_e
    iget-object v2, p0, Lcom/anddoes/launcher/c/i;->i:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 95
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 96
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->j:Ljava/lang/String;

    .line 98
    :cond_25
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->k:Landroid/graphics/drawable/Drawable;
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_2b} :catch_44

    .line 101
    :goto_2b
    invoke-virtual {p0, p2}, Lcom/anddoes/launcher/c/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_33

    .line 103
    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->m:Ljava/lang/String;

    .line 105
    :cond_33
    invoke-virtual {p0, p3}, Lcom/anddoes/launcher/c/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_3b

    .line 107
    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->n:Ljava/lang/String;

    .line 109
    :cond_3b
    invoke-virtual {p0, p4}, Lcom/anddoes/launcher/c/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_43

    .line 111
    iput-object v0, p0, Lcom/anddoes/launcher/c/i;->o:Ljava/lang/String;

    .line 113
    :cond_43
    return-void

    .line 99
    :catch_44
    move-exception v0

    goto :goto_2b
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 170
    const-string v0, "drawable"

    const-string v2, "xml"

    invoke-virtual {p0, v0, v2}, Lcom/anddoes/launcher/c/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v2, :cond_1f

    if-eqz v0, :cond_1f

    :try_start_f
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_d3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_f .. :try_end_16} :catch_9d
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_16} :catch_af
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_16} :catch_c1

    move-result v0

    :goto_17
    const/4 v2, 0x1

    if-ne v0, v2, :cond_62

    if-eqz v1, :cond_1f

    :try_start_1c
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1f} :catch_100

    .line 171
    :cond_1f
    :goto_1f
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 172
    :try_start_25
    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->b:Landroid/content/res/AssetManager;

    const-string v1, "drawable.xml"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_61

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    new-instance v2, Lcom/anddoes/launcher/c/j;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/anddoes/launcher/c/j;-><init>(B)V

    invoke-interface {v1, v2}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    new-instance v3, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v1, v0}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    invoke-virtual {v2}, Lcom/anddoes/launcher/c/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5b
    :goto_5b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_5e} :catch_fb

    move-result v0

    if-nez v0, :cond_da

    .line 174
    :cond_61
    :goto_61
    return-void

    .line 170
    :cond_62
    const/4 v2, 0x2

    if-ne v0, v2, :cond_97

    :try_start_65
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "item"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_97

    const/4 v0, 0x0

    const-string v2, "drawable"

    invoke-interface {v1, v0, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_97

    const-string v2, "drawable"

    invoke-virtual {p0, v0, v2}, Lcom/anddoes/launcher/c/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_97

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_97

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_97
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_9a
    .catchall {:try_start_65 .. :try_end_9a} :catchall_d3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_65 .. :try_end_9a} :catch_9d
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_9a} :catch_af
    .catch Ljava/lang/RuntimeException; {:try_start_65 .. :try_end_9a} :catch_c1

    move-result v0

    goto/16 :goto_17

    :catch_9d
    move-exception v0

    :try_start_9e
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing drawable."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a5
    .catchall {:try_start_9e .. :try_end_a5} :catchall_d3

    if-eqz v1, :cond_1f

    :try_start_a7
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_aa
    .catch Ljava/lang/Exception; {:try_start_a7 .. :try_end_aa} :catch_ac

    goto/16 :goto_1f

    :catch_ac
    move-exception v0

    goto/16 :goto_1f

    :catch_af
    move-exception v0

    :try_start_b0
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing drawable."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b7
    .catchall {:try_start_b0 .. :try_end_b7} :catchall_d3

    if-eqz v1, :cond_1f

    :try_start_b9
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_bc
    .catch Ljava/lang/Exception; {:try_start_b9 .. :try_end_bc} :catch_be

    goto/16 :goto_1f

    :catch_be
    move-exception v0

    goto/16 :goto_1f

    :catch_c1
    move-exception v0

    :try_start_c2
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing drawable."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c9
    .catchall {:try_start_c2 .. :try_end_c9} :catchall_d3

    if-eqz v1, :cond_1f

    :try_start_cb
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_ce
    .catch Ljava/lang/Exception; {:try_start_cb .. :try_end_ce} :catch_d0

    goto/16 :goto_1f

    :catch_d0
    move-exception v0

    goto/16 :goto_1f

    :catchall_d3
    move-exception v0

    if-eqz v1, :cond_d9

    :try_start_d6
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_d9
    .catch Ljava/lang/Exception; {:try_start_d6 .. :try_end_d9} :catch_fe

    :cond_d9
    :goto_d9
    throw v0

    .line 172
    :cond_da
    :try_start_da
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "drawable"

    invoke-virtual {p0, v0, v2}, Lcom/anddoes/launcher/c/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_f9
    .catch Ljava/lang/Exception; {:try_start_da .. :try_end_f9} :catch_fb

    goto/16 :goto_5b

    :catch_fb
    move-exception v0

    goto/16 :goto_61

    :catch_fe
    move-exception v1

    goto :goto_d9

    :catch_100
    move-exception v0

    goto/16 :goto_1f
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-virtual {p0, p2}, Lcom/anddoes/launcher/c/i;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 155
    if-nez v0, :cond_30

    .line 156
    invoke-virtual {p0, p3}, Lcom/anddoes/launcher/c/i;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 158
    :goto_b
    if-eqz v1, :cond_11

    .line 159
    array-length v2, v1

    const/4 v0, 0x0

    :goto_f
    if-lt v0, v2, :cond_12

    .line 166
    :cond_11
    return-void

    .line 159
    :cond_12
    aget-object v3, v1, v0

    .line 160
    const-string v4, "drawable"

    invoke-virtual {p0, v3, v4}, Lcom/anddoes/launcher/c/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 161
    if-eqz v3, :cond_2d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 162
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_30
    move-object v1, v0

    goto :goto_b
.end method

.method public abstract b()V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()I
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public final i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->k:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final n()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->l:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/anddoes/launcher/c/i;->p:Ljava/util/List;

    return-object v0
.end method

.method public final q()I
    .registers 2

    .prologue
    .line 149
    iget v0, p0, Lcom/anddoes/launcher/c/i;->q:I

    return v0
.end method
