.class public Lcom/anddoes/launcher/preference/GridPickerPreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Landroid/widget/NumberPicker;

.field private g:Landroid/widget/NumberPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    iput v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->a:I

    .line 16
    iput v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->b:I

    .line 18
    iput v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->d:I

    .line 19
    iput v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I

    .line 26
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "defaultValue"

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->c:Ljava/lang/String;

    .line 27
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->a()V

    .line 28
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "max"

    const/16 v2, 0x64

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->d:I

    .line 31
    :try_start_35
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "text"

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I
    :try_end_4c
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_4c} :catch_53

    .line 34
    :goto_4c
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->setDialogLayoutResource(I)V

    .line 35
    return-void

    :catch_53
    move-exception v0

    goto :goto_4c
.end method

.method private a()V
    .registers 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->c:Ljava/lang/String;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 70
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->a:I

    .line 71
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->b:I

    .line 72
    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x0

    .line 39
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 40
    const v0, 0x7f0d0027

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    .line 41
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->d:I

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 42
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 43
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    .line 45
    const v0, 0x7f0d0028

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    .line 46
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->d:I

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 47
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 48
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v3}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090003

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 51
    iget-object v3, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    iget v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->a:I

    if-eqz v0, :cond_67

    iget v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->a:I

    :goto_58
    invoke-virtual {v3, v0}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 52
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    iget v3, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->b:I

    if-eqz v3, :cond_6d

    iget v1, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->b:I

    :cond_63
    :goto_63
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 53
    return-void

    .line 51
    :cond_67
    if-eqz v2, :cond_6b

    const/4 v0, 0x6

    goto :goto_58

    :cond_6b
    move v0, v1

    goto :goto_58

    .line 52
    :cond_6d
    if-eqz v2, :cond_63

    const/16 v1, 0x8

    goto :goto_63
.end method

.method protected onDialogClosed(Z)V
    .registers 8
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 77
    if-eqz p1, :cond_72

    .line 78
    iget-object v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->f:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    move-result v0

    .line 79
    iget-object v1, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->g:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->shouldPersist()Z

    move-result v3

    if-eqz v3, :cond_72

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/GridPickerPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_72

    .line 84
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 85
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "_rows"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_columns"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 87
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 88
    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/GridPickerPreference;->persistString(Ljava/lang/String;)Z

    .line 91
    :cond_72
    return-void
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Landroid/preference/DialogPreference;->onSetInitialValue(ZLjava/lang/Object;)V

    .line 58
    if-eqz p1, :cond_52

    .line 59
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->shouldPersist()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 60
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->a:I

    .line 61
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_columns"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->e:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/GridPickerPreference;->b:I

    .line 66
    :cond_51
    :goto_51
    return-void

    .line 64
    :cond_52
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/GridPickerPreference;->a()V

    goto :goto_51
.end method
