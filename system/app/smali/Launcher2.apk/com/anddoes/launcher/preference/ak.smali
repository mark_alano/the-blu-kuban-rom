.class final Lcom/anddoes/launcher/preference/ak;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/anddoes/launcher/preference/PreferencesActivity;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 2130
    iput-object p1, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/ak;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2130
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/ak;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 2143
    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/b;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v4, "apex_data"

    invoke-static {v4}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sget-object v3, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_21

    sget-object v3, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    :cond_21
    iget-object v3, v0, Lcom/anddoes/launcher/preference/b;->a:Landroid/content/Context;

    instance-of v3, v3, Landroid/app/Activity;

    if-eqz v3, :cond_43

    iget-object v0, v0, Lcom/anddoes/launcher/preference/b;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->c()Lcom/android/launcher2/LauncherProvider;

    move-result-object v0

    if-eqz v0, :cond_43

    invoke-virtual {v0, v2}, Lcom/android/launcher2/LauncherProvider;->a(Ljava/io/File;)Z

    move-result v0

    :goto_3b
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/ak;->a:Z
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_3d} :catch_3f

    .line 2148
    :goto_3d
    const/4 v0, 0x0

    return-object v0

    .line 2145
    :catch_3f
    move-exception v0

    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/ak;->a:Z

    goto :goto_3d

    :cond_43
    move v0, v1

    goto :goto_3b
.end method


# virtual methods
.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/ak;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 1
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b()V

    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/ak;->a:Z

    if-eqz v0, :cond_42

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f070140

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "/Android/data/apexlauncher/apex_data"

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070007

    new-instance v2, Lcom/anddoes/launcher/preference/al;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/al;-><init>(Lcom/anddoes/launcher/preference/ak;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_41
    return-void

    :cond_42
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v1, 0x7f070151

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_41
.end method

.method protected final onPreExecute()V
    .registers 2

    .prologue
    .line 2136
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ak;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a()V

    .line 2137
    return-void
.end method
