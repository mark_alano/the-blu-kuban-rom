.class final Lcom/anddoes/launcher/preference/am;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/anddoes/launcher/preference/PreferencesActivity;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 1977
    iput-object p1, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1979
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/am;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1977
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/am;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1990
    :try_start_2
    iget-object v0, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/b;

    move-result-object v3

    sget-object v0, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_15

    sget-object v0, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_15
    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v3, Lcom/anddoes/launcher/preference/b;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "_preferences"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v6, "apex_settings"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_b3

    invoke-static {v0, v4}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    :goto_50
    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/b;->b:Z

    if-eqz v3, :cond_b1

    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    const-string v5, "DrawerGroups"

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_7d

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v6, "DrawerGroups"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_a9

    invoke-static {v3, v4}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_a9

    move v0, v1

    :cond_7d
    :goto_7d
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    const-string v5, "SwipeActions"

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_b1

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v6, "SwipeActions"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_ab

    invoke-static {v3, v4}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_ab

    :goto_a5
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/am;->a:Z
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_a7} :catch_ad

    .line 1995
    :goto_a7
    const/4 v0, 0x0

    return-object v0

    :cond_a9
    move v0, v2

    .line 1990
    goto :goto_7d

    :cond_ab
    move v1, v2

    goto :goto_a5

    .line 1992
    :catch_ad
    move-exception v0

    iput-boolean v2, p0, Lcom/anddoes/launcher/preference/am;->a:Z

    goto :goto_a7

    :cond_b1
    move v1, v0

    goto :goto_a5

    :cond_b3
    move v0, v1

    goto :goto_50
.end method


# virtual methods
.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/am;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 1
    iget-object v0, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b()V

    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/am;->a:Z

    if-eqz v0, :cond_42

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f070140

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "/Android/data/apexlauncher/apex_settings"

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070007

    new-instance v2, Lcom/anddoes/launcher/preference/an;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/an;-><init>(Lcom/anddoes/launcher/preference/am;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_41
    return-void

    :cond_42
    iget-object v0, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v1, 0x7f070151

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_41
.end method

.method protected final onPreExecute()V
    .registers 2

    .prologue
    .line 1983
    iget-object v0, p0, Lcom/anddoes/launcher/preference/am;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a()V

    .line 1984
    return-void
.end method
