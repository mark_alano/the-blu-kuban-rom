.class final Lcom/anddoes/launcher/preference/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/preference/PreferencesActivity;

.field private b:Landroid/widget/ArrayAdapter;

.field private c:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1876
    iput-object p1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1876
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/ao;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    return-void
.end method


# virtual methods
.method final a()Landroid/app/Dialog;
    .registers 4

    .prologue
    .line 1887
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ao;->c:Ljava/util/List;

    .line 1888
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f030003

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ao;->b:Landroid/widget/ArrayAdapter;

    .line 1890
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f070172

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1891
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->c:Ljava/util/List;

    const-string v1, "select_picture"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1892
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f070173

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1893
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->c:Ljava/util/List;

    const-string v1, "crop_picture"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1895
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1896
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v2, 0x7f0700c8

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1897
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->b:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1898
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 1900
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1901
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1902
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1903
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1904
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 1970
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const v7, 0x7f070151

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1913
    if-ltz p2, :cond_f

    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_10

    .line 1961
    :cond_f
    :goto_f
    return-void

    .line 1916
    :cond_10
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1917
    const-string v1, "select_picture"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 1919
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1920
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1922
    :try_start_2c
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 1923
    iget-object v2, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v3, 0x7f0700ca

    invoke-virtual {v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1922
    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 1923
    const/4 v2, 0x2

    .line 1922
    invoke-virtual {v1, v0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_3f} :catch_40

    goto :goto_f

    .line 1925
    :catch_40
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1926
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_f

    .line 1928
    :cond_4b
    const-string v1, "crop_picture"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1929
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1930
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 1931
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1932
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1933
    const v0, 0x7f0b0017

    .line 1934
    const-string v3, "NONE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e6

    .line 1935
    const v0, 0x7f0b000f

    .line 1941
    :cond_85
    :goto_85
    iget-object v2, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1943
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1944
    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1945
    const-string v3, "crop"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1946
    const-string v3, "outputX"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1947
    const-string v3, "outputY"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1948
    const-string v3, "aspectX"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1949
    const-string v1, "aspectY"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1950
    const-string v0, "scale"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1951
    const-string v0, "noFaceDetection"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1952
    const-string v0, "return-data"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1954
    :try_start_c5
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 1955
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v3, 0x7f070173

    invoke-virtual {v1, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1954
    invoke-static {v2, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 1955
    const/4 v2, 0x3

    .line 1954
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_d8
    .catch Ljava/lang/Exception; {:try_start_c5 .. :try_end_d8} :catch_da

    goto/16 :goto_f

    .line 1957
    :catch_da
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/preference/ao;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1958
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_f

    .line 1936
    :cond_e6
    const-string v3, "SMALL"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f2

    .line 1937
    const v0, 0x7f0b0013

    goto :goto_85

    .line 1938
    :cond_f2
    const-string v3, "LARGE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_85

    .line 1939
    const v0, 0x7f0b001b

    goto :goto_85
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 1966
    return-void
.end method

.method public final onShow(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 1909
    return-void
.end method
