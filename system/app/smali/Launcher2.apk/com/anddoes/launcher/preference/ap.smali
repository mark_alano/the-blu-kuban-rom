.class final Lcom/anddoes/launcher/preference/ap;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1606
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1607
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ap;->a:Landroid/view/LayoutInflater;

    .line 1608
    iput-boolean p3, p0, Lcom/anddoes/launcher/preference/ap;->b:Z

    .line 1609
    return-void
.end method

.method private static a(Landroid/preference/PreferenceActivity$Header;)I
    .registers 5
    .parameter

    .prologue
    .line 1572
    iget-wide v0, p0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0d0074

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    .line 1573
    const/4 v0, 0x0

    .line 1575
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x1

    goto :goto_a
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 1587
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .registers 3
    .parameter

    .prologue
    .line 1581
    invoke-virtual {p0, p1}, Lcom/anddoes/launcher/preference/ap;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 1582
    invoke-static {v0}, Lcom/anddoes/launcher/preference/ap;->a(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1614
    invoke-virtual {p0, p1}, Lcom/anddoes/launcher/preference/ap;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 1615
    invoke-static {v0}, Lcom/anddoes/launcher/preference/ap;->a(Landroid/preference/PreferenceActivity$Header;)I

    move-result v4

    .line 1616
    const/4 v2, 0x0

    .line 1618
    if-nez p2, :cond_91

    .line 1619
    new-instance v3, Lcom/anddoes/launcher/preference/aq;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, Lcom/anddoes/launcher/preference/aq;-><init>(B)V

    .line 1621
    packed-switch v4, :pswitch_data_126

    .line 1636
    :goto_16
    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v3

    .line 1643
    :goto_1a
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-boolean v3, p0, Lcom/anddoes/launcher/preference/ap;->b:Z

    if-eqz v3, :cond_99

    .line 1644
    const v3, 0x7f070154

    .line 1643
    :goto_25
    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1645
    iget-wide v5, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v7, 0x7f0d0076

    cmp-long v5, v5, v7

    if-nez v5, :cond_9d

    .line 1647
    :try_start_32
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f070155

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_42} :catch_122

    move-result-object v3

    .line 1658
    :goto_43
    packed-switch v4, :pswitch_data_12e

    .line 1681
    :goto_46
    return-object v2

    .line 1623
    :pswitch_47
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x0

    .line 1624
    const v6, 0x1010208

    .line 1623
    invoke-direct {v2, v1, v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v1, v2

    .line 1625
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/anddoes/launcher/preference/aq;->b:Landroid/widget/TextView;

    goto :goto_16

    .line 1629
    :pswitch_5a
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ap;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030023

    const/4 v5, 0x0

    invoke-virtual {v1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1630
    const v1, 0x7f0d0002

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v3, Lcom/anddoes/launcher/preference/aq;->a:Landroid/widget/ImageView;

    .line 1631
    const v1, 0x1020016

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/anddoes/launcher/preference/aq;->b:Landroid/widget/TextView;

    .line 1632
    const v1, 0x1020010

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/anddoes/launcher/preference/aq;->c:Landroid/widget/TextView;

    .line 1633
    const v1, 0x7f0d0048

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, v3, Lcom/anddoes/launcher/preference/aq;->d:Landroid/widget/FrameLayout;

    goto :goto_16

    .line 1639
    :cond_91
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/preference/aq;

    move-object v2, p2

    goto :goto_1a

    .line 1644
    :cond_99
    const v3, 0x7f070272

    goto :goto_25

    .line 1651
    :cond_9d
    iget-wide v5, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v7, 0x7f0d0077

    cmp-long v3, v5, v7

    if-nez v3, :cond_b2

    .line 1652
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f070166

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_43

    .line 1654
    :cond_b2
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_43

    .line 1660
    :pswitch_c3
    iget-object v0, v1, Lcom/anddoes/launcher/preference/aq;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_46

    .line 1664
    :pswitch_ca
    iget-object v4, v1, Lcom/anddoes/launcher/preference/aq;->a:Landroid/widget/ImageView;

    iget v5, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1665
    iget-object v4, v1, Lcom/anddoes/launcher/preference/aq;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1666
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ap;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1667
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_111

    .line 1668
    iget-object v4, v1, Lcom/anddoes/launcher/preference/aq;->c:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1669
    iget-object v4, v1, Lcom/anddoes/launcher/preference/aq;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1673
    :goto_f3
    iget-wide v3, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v5, 0x7f0d0075

    cmp-long v3, v3, v5

    if-eqz v3, :cond_109

    .line 1674
    iget-boolean v3, p0, Lcom/anddoes/launcher/preference/ap;->b:Z

    if-eqz v3, :cond_119

    iget-wide v3, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v5, 0x7f0d0076

    cmp-long v0, v3, v5

    if-nez v0, :cond_119

    .line 1675
    :cond_109
    iget-object v0, v1, Lcom/anddoes/launcher/preference/aq;->d:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_46

    .line 1671
    :cond_111
    iget-object v3, v1, Lcom/anddoes/launcher/preference/aq;->c:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_f3

    .line 1677
    :cond_119
    iget-object v0, v1, Lcom/anddoes/launcher/preference/aq;->d:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_46

    .line 1649
    :catch_122
    move-exception v5

    goto/16 :goto_43

    .line 1621
    nop

    :pswitch_data_126
    .packed-switch 0x0
        :pswitch_47
        :pswitch_5a
    .end packed-switch

    .line 1658
    :pswitch_data_12e
    .packed-switch 0x0
        :pswitch_c3
        :pswitch_ca
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 1597
    const/4 v0, 0x3

    return v0
.end method

.method public final hasStableIds()Z
    .registers 2

    .prologue
    .line 1602
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 1592
    invoke-virtual {p0, p1}, Lcom/anddoes/launcher/preference/ap;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
