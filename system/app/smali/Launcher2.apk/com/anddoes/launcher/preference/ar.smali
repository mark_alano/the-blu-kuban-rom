.class Lcom/anddoes/launcher/preference/ar;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field protected a:Lcom/anddoes/launcher/preference/PreferencesActivity;

.field protected b:Landroid/preference/PreferenceManager;

.field protected c:Lcom/anddoes/launcher/preference/h;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 1685
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2
    .parameter

    .prologue
    .line 1685
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/ar;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .registers 1

    .prologue
    .line 1736
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 1693
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1695
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ar;->b:Landroid/preference/PreferenceManager;

    .line 1696
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/anddoes/launcher/preference/PreferencesActivity;

    if-eqz v0, :cond_19

    .line 1697
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/preference/PreferencesActivity;

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 1699
    :cond_19
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    if-nez v0, :cond_24

    .line 1700
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1702
    :cond_24
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v0, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->onIsMultiPane()Z

    move-result v0

    if-eqz v0, :cond_4b

    const/4 v0, 0x0

    :goto_33
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1703
    new-instance v0, Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Lcom/anddoes/launcher/preference/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/ar;->c:Lcom/anddoes/launcher/preference/h;

    .line 1704
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ar;->c:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->bj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anddoes/launcher/v;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1705
    return-void

    .line 1702
    :cond_4b
    const/4 v0, 0x1

    goto :goto_33
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 1728
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 1729
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1730
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1732
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 1719
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 1720
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->a()V

    .line 1721
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/ar;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1722
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1723
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1710
    invoke-virtual {p0, p2}, Lcom/anddoes/launcher/preference/ar;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1711
    if-eqz v0, :cond_d

    .line 1712
    iget-object v1, p0, Lcom/anddoes/launcher/preference/ar;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/ar;->b:Landroid/preference/PreferenceManager;

    invoke-static {v1, v2, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Lcom/anddoes/launcher/preference/PreferencesActivity;Landroid/preference/PreferenceManager;Landroid/preference/Preference;)V

    .line 1714
    :cond_d
    return-void
.end method
