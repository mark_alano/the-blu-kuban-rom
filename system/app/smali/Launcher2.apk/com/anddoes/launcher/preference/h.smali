.class public final Lcom/anddoes/launcher/preference/h;
.super Lcom/anddoes/launcher/preference/g;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/g;-><init>(Landroid/content/Context;)V

    .line 88
    if-eqz p1, :cond_b

    .line 89
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/h;->b:Landroid/content/SharedPreferences;

    .line 91
    :cond_b
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .registers 4

    .prologue
    .line 251
    const-string v0, "drawer_hidden_apps"

    new-instance v1, Ljava/lang/StringBuilder;

    const-class v2, Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .registers 5

    .prologue
    .line 259
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070204

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701bd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 261
    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701be

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 259
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()I
    .registers 4

    .prologue
    .line 265
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070205

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 266
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701bd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 265
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final D()I
    .registers 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070206

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 271
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 270
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final E()Ljava/lang/String;
    .registers 5

    .prologue
    .line 275
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070207

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701bf

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 277
    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701c0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 276
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final F()I
    .registers 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070208

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 282
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701bf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 281
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final G()I
    .registers 4

    .prologue
    .line 286
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070209

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 286
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final H()Ljava/lang/String;
    .registers 4

    .prologue
    .line 291
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 291
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .registers 4

    .prologue
    .line 296
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 296
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final J()Ljava/lang/String;
    .registers 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 301
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .registers 4

    .prologue
    .line 306
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 307
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final L()I
    .registers 4

    .prologue
    .line 311
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 312
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 311
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final M()Ljava/lang/String;
    .registers 4

    .prologue
    .line 316
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07020f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 316
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final N()Z
    .registers 4

    .prologue
    .line 321
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070210

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 321
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final O()Z
    .registers 4

    .prologue
    .line 326
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070211

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 327
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 326
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final P()I
    .registers 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070212

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 332
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 331
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public final Q()Ljava/lang/String;
    .registers 4

    .prologue
    .line 336
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070213

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 337
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 336
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final R()Ljava/lang/String;
    .registers 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070215

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701c9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 341
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final S()Z
    .registers 4

    .prologue
    .line 346
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070216

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 346
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final T()Z
    .registers 4

    .prologue
    .line 351
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070217

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 351
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final U()Z
    .registers 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070218

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 356
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final V()Z
    .registers 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070219

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 361
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final W()Ljava/lang/String;
    .registers 4

    .prologue
    .line 366
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07021c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701ca

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 366
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .registers 4

    .prologue
    .line 371
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07021d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701cb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 371
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Y()Z
    .registers 4

    .prologue
    .line 376
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07021e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 377
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 376
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final Z()Ljava/lang/String;
    .registers 4

    .prologue
    .line 381
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07021f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 382
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 381
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()I
    .registers 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701a3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 102
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;I)V

    .line 108
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701fc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 461
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    .line 462
    return-void
.end method

.method public final aA()Ljava/lang/String;
    .registers 4

    .prologue
    .line 520
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 521
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701dc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 520
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aB()Ljava/lang/String;
    .registers 4

    .prologue
    .line 525
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07023e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701dd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aC()Ljava/lang/String;
    .registers 4

    .prologue
    .line 530
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07023f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701de

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 530
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aD()Ljava/lang/String;
    .registers 4

    .prologue
    .line 535
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070240

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 536
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701df

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 535
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aE()Ljava/lang/String;
    .registers 4

    .prologue
    .line 540
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070241

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 541
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701e0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 540
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aF()Ljava/lang/String;
    .registers 4

    .prologue
    .line 545
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070242

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 546
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701e1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 545
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aG()Ljava/lang/String;
    .registers 4

    .prologue
    .line 550
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070243

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 551
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 550
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH()Z
    .registers 4

    .prologue
    .line 563
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070244

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 564
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 563
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aI()Z
    .registers 4

    .prologue
    .line 568
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070245

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 569
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 568
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aJ()Z
    .registers 4

    .prologue
    .line 573
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070246

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 574
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 573
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aK()Ljava/lang/String;
    .registers 3

    .prologue
    .line 586
    const-string v0, "theme_iconpack_pkg"

    const-string v1, "default"

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aL()Ljava/lang/String;
    .registers 3

    .prologue
    .line 594
    const-string v0, "theme_skin_pkg"

    const-string v1, "default"

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aM()Ljava/lang/String;
    .registers 3

    .prologue
    .line 602
    const-string v0, "theme_font_pkg"

    const-string v1, "default"

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aN()Z
    .registers 4

    .prologue
    .line 610
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070249

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 611
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 610
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aO()Z
    .registers 4

    .prologue
    .line 615
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 616
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 615
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aP()Z
    .registers 4

    .prologue
    .line 620
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 621
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 620
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aQ()Z
    .registers 4

    .prologue
    .line 625
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 626
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 625
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aR()Z
    .registers 4

    .prologue
    .line 630
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 631
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 630
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aS()Z
    .registers 4

    .prologue
    .line 635
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 636
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 635
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aT()Z
    .registers 4

    .prologue
    .line 640
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07024f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 641
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 640
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aU()Z
    .registers 4

    .prologue
    .line 645
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070250

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 646
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 645
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aV()Z
    .registers 4

    .prologue
    .line 650
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070251

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 651
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 650
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aW()Z
    .registers 4

    .prologue
    .line 655
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070252

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 656
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 655
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aX()Z
    .registers 4

    .prologue
    .line 660
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070253

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 661
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 660
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aY()Z
    .registers 4

    .prologue
    .line 665
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070254

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 666
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 665
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aZ()Z
    .registers 4

    .prologue
    .line 670
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070256

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 671
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 670
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aa()Z
    .registers 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070220

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 386
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ab()Z
    .registers 4

    .prologue
    .line 391
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070221

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 392
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 391
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ac()Z
    .registers 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070222

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 397
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 396
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ad()I
    .registers 4

    .prologue
    .line 401
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070223

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 402
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701cd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 401
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final ae()I
    .registers 4

    .prologue
    .line 406
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070224

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 407
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 406
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final af()Ljava/lang/String;
    .registers 4

    .prologue
    .line 411
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070225

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ag()Ljava/lang/String;
    .registers 4

    .prologue
    .line 416
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070226

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 417
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 416
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah()Z
    .registers 4

    .prologue
    .line 421
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070227

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 421
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ai()Ljava/lang/String;
    .registers 4

    .prologue
    .line 426
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070228

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 427
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 426
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aj()Z
    .registers 4

    .prologue
    .line 431
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070229

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 432
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 431
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ak()Z
    .registers 4

    .prologue
    .line 436
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 437
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 436
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final al()I
    .registers 4

    .prologue
    .line 441
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 441
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public final am()Z
    .registers 4

    .prologue
    .line 446
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 447
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 446
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final an()Ljava/lang/String;
    .registers 4

    .prologue
    .line 451
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 451
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ao()Z
    .registers 4

    .prologue
    .line 456
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 456
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ap()Z
    .registers 4

    .prologue
    .line 465
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07022f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 466
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 465
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aq()Ljava/lang/String;
    .registers 4

    .prologue
    .line 470
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070231

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 471
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 470
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ar()Ljava/lang/String;
    .registers 4

    .prologue
    .line 475
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070232

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 475
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final as()Z
    .registers 4

    .prologue
    .line 480
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070233

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 481
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 480
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final at()Z
    .registers 4

    .prologue
    .line 485
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070234

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 486
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 485
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final au()Z
    .registers 4

    .prologue
    .line 490
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070235

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 491
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 490
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final av()Ljava/lang/String;
    .registers 4

    .prologue
    .line 495
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070237

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 496
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 495
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aw()Ljava/lang/String;
    .registers 4

    .prologue
    .line 500
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070238

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 501
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701da

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 500
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ax()Z
    .registers 4

    .prologue
    .line 505
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070239

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 506
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 505
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ay()Z
    .registers 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07023a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 511
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 510
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final az()Ljava/lang/String;
    .registers 4

    .prologue
    .line 515
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07023b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 516
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701db

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 515
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .registers 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 111
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .registers 4
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;I)V

    .line 117
    return-void
.end method

.method public final ba()Ljava/lang/String;
    .registers 3

    .prologue
    .line 675
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070257

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 676
    const-string v1, "com.android.contacts-com.android.contacts.activities.DialtactsActivity"

    .line 675
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bb()Z
    .registers 4

    .prologue
    .line 680
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 681
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 680
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bc()Ljava/lang/String;
    .registers 3

    .prologue
    .line 685
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070259

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 686
    const-string v1, "com.android.mms-com.android.mms.ui.ConversationList"

    .line 685
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bd()Z
    .registers 4

    .prologue
    .line 690
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07025a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 691
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 690
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final be()Ljava/lang/String;
    .registers 3

    .prologue
    .line 695
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07025b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 696
    const-string v1, "com.google.android.gm-com.google.android.gm.ConversationListActivityGmail"

    .line 695
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bf()Z
    .registers 4

    .prologue
    .line 700
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07025c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 701
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 700
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bg()Z
    .registers 4

    .prologue
    .line 705
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07025e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 706
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 705
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bh()Z
    .registers 4

    .prologue
    .line 710
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07025f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 711
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 710
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bi()Z
    .registers 4

    .prologue
    .line 715
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070260

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 716
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 715
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bj()Ljava/lang/String;
    .registers 4

    .prologue
    .line 720
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070262

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 721
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701e3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 720
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bk()Z
    .registers 4

    .prologue
    .line 725
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070263

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 726
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 725
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bl()Z
    .registers 4

    .prologue
    .line 730
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070264

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 731
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 730
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bm()V
    .registers 3

    .prologue
    .line 735
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070264

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    .line 736
    return-void
.end method

.method public final bn()Z
    .registers 4

    .prologue
    .line 748
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070265

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 749
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 748
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final bo()Ljava/lang/String;
    .registers 4

    .prologue
    .line 763
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f07026c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 764
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701e4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 763
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 5

    .prologue
    .line 120
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701ab

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 122
    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .registers 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701e9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701ab

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 126
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .registers 4

    .prologue
    .line 131
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 131
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .registers 5

    .prologue
    .line 136
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701eb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701ad

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 138
    iget-object v2, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v3, 0x7f0701ae

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .registers 4

    .prologue
    .line 142
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ec

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701ad

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 142
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final h()I
    .registers 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ed

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701ae

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 147
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .registers 4

    .prologue
    .line 152
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ee

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .registers 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .registers 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .registers 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 172
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .registers 4

    .prologue
    .line 177
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 177
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final n()I
    .registers 4

    .prologue
    .line 182
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 182
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public final o()Ljava/lang/String;
    .registers 4

    .prologue
    .line 187
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .registers 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 192
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final q()Ljava/lang/String;
    .registers 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .registers 4

    .prologue
    .line 202
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 202
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()Ljava/lang/String;
    .registers 4

    .prologue
    .line 207
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v2, 0x7f0701b6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()Z
    .registers 4

    .prologue
    .line 212
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701f9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 212
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .registers 4

    .prologue
    .line 217
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701fb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 217
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final v()Ljava/lang/String;
    .registers 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701fc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .registers 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701fd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 231
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Z
    .registers 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f0701ff

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 236
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .registers 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070200

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 241
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .registers 4

    .prologue
    .line 246
    iget-object v0, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    const v1, 0x7f070201

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 247
    iget-object v1, p0, Lcom/anddoes/launcher/preference/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 246
    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
