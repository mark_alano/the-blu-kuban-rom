.class final Lcom/anddoes/launcher/preference/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/preference/PreferencesActivity;


# direct methods
.method constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/j;)Lcom/anddoes/launcher/preference/PreferencesActivity;
    .registers 2
    .parameter

    .prologue
    .line 864
    iget-object v0, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    return-object v0
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 866
    invoke-static {}, Lcom/anddoes/launcher/v;->a()Z

    move-result v0

    if-nez v0, :cond_15

    .line 867
    iget-object v0, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 868
    const v1, 0x7f070152

    .line 867
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 902
    :goto_14
    return v4

    .line 873
    :cond_15
    :try_start_15
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 874
    const-string v2, "/Android/data/apexlauncher/apex_settings.bak"

    .line 873
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 875
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_42

    .line 876
    iget-object v0, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 877
    const v1, 0x7f070146

    .line 878
    const/4 v2, 0x0

    .line 876
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 878
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_33} :catch_34

    goto :goto_14

    .line 898
    :catch_34
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 899
    const v1, 0x7f070151

    .line 898
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 900
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_14

    .line 880
    :cond_42
    :try_start_42
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/j;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 881
    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 882
    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 883
    const v1, 0x7f070144

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 884
    const v1, 0x7f07000e

    .line 885
    new-instance v2, Lcom/anddoes/launcher/preference/k;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/k;-><init>(Lcom/anddoes/launcher/preference/j;)V

    .line 884
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 890
    const v1, 0x7f07000f

    .line 891
    new-instance v2, Lcom/anddoes/launcher/preference/l;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/l;-><init>(Lcom/anddoes/launcher/preference/j;)V

    .line 890
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 895
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_79} :catch_34

    goto :goto_14
.end method
