.class final Lcom/anddoes/launcher/preference/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/preference/PreferencesActivity;


# direct methods
.method constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/s;)Lcom/anddoes/launcher/preference/PreferencesActivity;
    .registers 2
    .parameter

    .prologue
    .line 983
    iget-object v0, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    return-object v0
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 985
    invoke-static {}, Lcom/anddoes/launcher/v;->a()Z

    move-result v0

    if-nez v0, :cond_15

    .line 986
    iget-object v0, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 987
    const v1, 0x7f070152

    .line 986
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 988
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1021
    :goto_14
    return v4

    .line 992
    :cond_15
    :try_start_15
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 993
    const-string v2, "/Android/data/apexlauncher/apex_data.bak"

    .line 992
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 994
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_42

    .line 995
    iget-object v0, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 996
    const v1, 0x7f070146

    .line 997
    const/4 v2, 0x0

    .line 995
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 997
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_33} :catch_34

    goto :goto_14

    .line 1017
    :catch_34
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    .line 1018
    const v1, 0x7f070151

    .line 1017
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1019
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_14

    .line 999
    :cond_42
    :try_start_42
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/s;->a:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1000
    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1001
    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1002
    const v1, 0x7f070150

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1003
    const v1, 0x7f07000e

    .line 1004
    new-instance v2, Lcom/anddoes/launcher/preference/t;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/t;-><init>(Lcom/anddoes/launcher/preference/s;)V

    .line 1003
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1009
    const v1, 0x7f07000f

    .line 1010
    new-instance v2, Lcom/anddoes/launcher/preference/u;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/u;-><init>(Lcom/anddoes/launcher/preference/s;)V

    .line 1009
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1014
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_79} :catch_34

    goto :goto_14
.end method
