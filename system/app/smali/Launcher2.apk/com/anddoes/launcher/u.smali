.class public final Lcom/anddoes/launcher/u;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/s;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/s;)V
    .registers 2
    .parameter

    .prologue
    .line 178
    iput-object p1, p0, Lcom/anddoes/launcher/u;->a:Lcom/anddoes/launcher/s;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 179
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/u;->a:Lcom/anddoes/launcher/s;

    iget-object v0, v0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 201
    iget-object v0, p0, Lcom/anddoes/launcher/u;->a:Lcom/anddoes/launcher/s;

    iget-object v0, v0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 205
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 182
    iget-object v0, p0, Lcom/anddoes/launcher/u;->a:Lcom/anddoes/launcher/s;

    iget-object v0, v0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/t;

    .line 184
    if-nez p2, :cond_30

    .line 185
    iget-object v1, p0, Lcom/anddoes/launcher/u;->a:Lcom/anddoes/launcher/s;

    iget-object v1, v1, Lcom/anddoes/launcher/s;->b:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_1f
    move-object v1, v2

    .line 188
    check-cast v1, Landroid/widget/TextView;

    .line 189
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 190
    iget-object v3, v0, Lcom/anddoes/launcher/t;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, v0, Lcom/anddoes/launcher/t;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 193
    return-object v2

    :cond_30
    move-object v2, p2

    goto :goto_1f
.end method
