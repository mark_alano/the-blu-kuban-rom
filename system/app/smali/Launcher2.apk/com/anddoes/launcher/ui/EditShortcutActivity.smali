.class public Lcom/anddoes/launcher/ui/EditShortcutActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/ImageButton;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/content/pm/PackageManager;

.field private h:Landroid/content/Intent;

.field private i:Landroid/content/Intent$ShortcutIconResource;

.field private j:Landroid/content/Intent$ShortcutIconResource;

.field private k:I

.field private l:Lcom/android/launcher2/LauncherApplication;

.field private m:Lcom/android/launcher2/di;

.field private n:Landroid/view/LayoutInflater;

.field private o:Ljava/util/List;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->o:Ljava/util/List;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->o:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 244
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 245
    const-string v1, "NONE"

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->getBackgroundType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 246
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 247
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 248
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 249
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 255
    :goto_35
    iput-object v4, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 256
    return-void

    .line 251
    :cond_38
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->getPreviewBackground()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 252
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 253
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_35
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/Intent$ShortcutIconResource;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Ljava/util/List;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->o:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic c(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->p:Z

    return v0
.end method

.method static synthetic d(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/di;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->m:Lcom/android/launcher2/di;

    return-object v0
.end method

.method static synthetic e(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;
    .registers 2
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    return-object v0
.end method

.method static synthetic f(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;
    .registers 2
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    return-object v0
.end method

.method static synthetic g(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/LauncherApplication;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    return-object v0
.end method

.method static synthetic h(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/widget/ImageButton;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic i(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic j(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a()V

    return-void
.end method

.method static synthetic k(Lcom/anddoes/launcher/ui/EditShortcutActivity;)I
    .registers 2
    .parameter

    .prologue
    .line 80
    iget v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->k:I

    return v0
.end method

.method static synthetic l(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->n:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, -0x2

    const/4 v2, 0x0

    .line 260
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 261
    const/4 v0, -0x1

    if-ne p2, v0, :cond_d

    .line 262
    packed-switch p1, :pswitch_data_20e

    .line 412
    :cond_d
    :goto_d
    return-void

    .line 264
    :pswitch_e
    const v0, 0x7f070286

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 265
    const v1, 0x7f070176

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 267
    const-string v3, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 269
    if-eqz v0, :cond_53

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 270
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 271
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 275
    const-string v0, "android.intent.extra.TITLE"

    .line 276
    const v2, 0x7f070292

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 275
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 277
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_d

    .line 278
    :cond_53
    if-eqz v1, :cond_69

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 279
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 280
    const-class v1, Lcom/anddoes/launcher/ui/ActPickerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0, v0, v4}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_d

    .line 298
    :cond_69
    invoke-virtual {p0, p3, v4}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_d

    .line 302
    :pswitch_6d
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_78

    .line 303
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 304
    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 306
    :cond_78
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 309
    :try_start_7c
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_82
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7c .. :try_end_82} :catch_ea

    move-result-object v0

    move-object v1, v0

    .line 312
    :goto_84
    if-eqz v1, :cond_d

    .line 314
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    if-nez v0, :cond_94

    .line 316
    iget-object v0, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 318
    :cond_94
    new-instance v3, Landroid/content/Intent$ShortcutIconResource;

    invoke-direct {v3}, Landroid/content/Intent$ShortcutIconResource;-><init>()V

    iput-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    .line 319
    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v4, v3, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    .line 321
    :try_start_a1
    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    iget-object v4, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v4, v4, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 322
    iget-object v4, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    invoke-virtual {v1}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;
    :try_end_b7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a1 .. :try_end_b7} :catch_ed
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_a1 .. :try_end_b7} :catch_f1

    .line 329
    :goto_b7
    iput-object p3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    .line 330
    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v3}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 332
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 333
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 334
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 335
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 336
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_d

    :catch_ea
    move-exception v0

    move-object v1, v2

    goto :goto_84

    .line 323
    :catch_ed
    move-exception v3

    .line 324
    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_b7

    .line 325
    :catch_f1
    move-exception v3

    .line 326
    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_b7

    .line 340
    :pswitch_f5
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_100

    .line 341
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 342
    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 344
    :cond_100
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 345
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 346
    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 348
    const-string v4, "android.intent.action.CALL_PRIVILEGED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_127

    .line 349
    const-string v4, "android.intent.action.CALL"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    :cond_127
    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    .line 354
    if-eqz v1, :cond_15f

    .line 355
    invoke-static {v1, p0}, Lcom/android/launcher2/jj;->a(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 356
    new-instance v1, Lcom/android/launcher2/ca;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 375
    :goto_138
    if-nez v1, :cond_142

    .line 376
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 378
    :cond_142
    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    .line 379
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 380
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 381
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 382
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 383
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_d

    .line 358
    :cond_15f
    const-string v1, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 359
    instance-of v4, v1, Landroid/content/Intent$ShortcutIconResource;

    if-eqz v4, :cond_20a

    .line 360
    check-cast v1, Landroid/content/Intent$ShortcutIconResource;

    iput-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    .line 363
    :try_start_16d
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    iget-object v4, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v4, v4, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_176
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16d .. :try_end_176} :catch_188

    move-result-object v1

    .line 367
    :goto_177
    if-eqz v1, :cond_20a

    .line 368
    iget-object v4, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v4, v4, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 369
    if-eqz v4, :cond_20a

    .line 370
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_138

    .line 365
    :catch_188
    move-exception v1

    move-object v1, v2

    goto :goto_177

    .line 386
    :pswitch_18b
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 387
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_d

    .line 388
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-static {v0, p0}, Lcom/android/launcher2/jj;->b(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 389
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 390
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 391
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_d

    .line 395
    :pswitch_1ba
    if-eqz p3, :cond_d

    .line 396
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/anddoes/launcher/v;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 397
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_d

    .line 398
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 399
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 400
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_d

    .line 405
    :pswitch_1e3
    const-string v0, "icon"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 406
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_d

    .line 408
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 409
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 410
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_d

    :cond_20a
    move-object v1, v2

    goto/16 :goto_138

    .line 262
    nop

    :pswitch_data_20e
    .packed-switch 0x1
        :pswitch_1ba
        :pswitch_e
        :pswitch_f5
        :pswitch_6d
        :pswitch_18b
        :pswitch_1e3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 421
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 422
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    iput-object v1, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    .line 423
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    iput-object v1, v0, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    .line 424
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->finish()V

    .line 466
    :cond_14
    :goto_14
    return-void

    .line 425
    :cond_15
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 426
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 427
    const-string v1, "android.intent.extra.shortcut.INTENT"

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 428
    const-string v1, "android.intent.extra.shortcut.NAME"

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-nez v1, :cond_4f

    .line 431
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    if-eqz v1, :cond_47

    .line 432
    const-string v1, "android.intent.extra.shortcut.ICON_RESOURCE"

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 437
    :cond_47
    :goto_47
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->setResult(ILandroid/content/Intent;)V

    .line 438
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->finish()V

    goto :goto_14

    .line 435
    :cond_4f
    const-string v1, "android.intent.extra.shortcut.ICON"

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_47

    .line 439
    :cond_57
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 440
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 441
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 442
    const v2, 0x7f070286

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    const v2, 0x7f070176

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 447
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 449
    const v2, 0x7f02007b

    .line 448
    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    const v2, 0x7f020037

    .line 450
    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 456
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 457
    const-string v2, "android.intent.extra.INTENT"

    new-instance v3, Landroid/content/Intent;

    .line 458
    const-string v4, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 457
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 459
    const-string v2, "android.intent.extra.TITLE"

    .line 460
    const v3, 0x7f070291

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 459
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 461
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 462
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_14

    .line 463
    :cond_c8
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 464
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->showDialog(I)V

    goto/16 :goto_14
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    .line 95
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f09

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->p:Z

    .line 97
    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    .line 98
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v0, 0x7f0d001d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    .line 100
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 102
    const v0, 0x7f0d0022

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    .line 103
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 104
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v0, 0x7f0d0021

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const v0, 0x7f0d001f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    .line 109
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    .line 110
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->n:Landroid/view/LayoutInflater;

    .line 111
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x105

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->k:I

    .line 112
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_e4

    const-string v2, "android.intent.action.EDIT"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e4

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    :goto_ab
    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->m:Lcom/android/launcher2/di;

    .line 113
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->m:Lcom/android/launcher2/di;

    if-eqz v0, :cond_e3

    instance-of v2, v0, Lcom/android/launcher2/jd;

    if-eqz v2, :cond_16a

    check-cast v0, Lcom/android/launcher2/jd;

    iget-object v2, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    iput-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    iget-object v3, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->l:Lcom/android/launcher2/LauncherApplication;

    iget-object v3, v3, Lcom/android/launcher2/LauncherApplication;->b:Lcom/android/launcher2/da;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    if-nez v2, :cond_e6

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 114
    :cond_e3
    :goto_e3
    return-void

    :cond_e4
    move-object v0, v1

    .line 112
    goto :goto_ab

    .line 113
    :cond_e6
    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_161

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_115

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.anddoes.launcher.ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_115

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_e3

    :cond_115
    :try_start_115
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_11b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_115 .. :try_end_11b} :catch_15a

    move-result-object v0

    move-object v2, v0

    :goto_11d
    if-eqz v2, :cond_e3

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12d

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    :cond_12d
    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent$ShortcutIconResource;

    invoke-direct {v0}, Landroid/content/Intent$ShortcutIconResource;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    iget-object v3, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v3, v0, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    :try_start_13f
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    iget-object v3, v3, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v3, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    invoke-virtual {v2}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;
    :try_end_155
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_13f .. :try_end_155} :catch_156
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_13f .. :try_end_155} :catch_15d

    goto :goto_e3

    :catch_156
    move-exception v0

    iput-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_e3

    :catch_15a
    move-exception v0

    move-object v2, v1

    goto :goto_11d

    :catch_15d
    move-exception v0

    iput-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_e3

    :cond_161
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    iget-object v0, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_e3

    :cond_16a
    instance-of v1, v0, Lcom/android/launcher2/cu;

    if-eqz v1, :cond_e3

    const v1, 0x7f07016a

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->setTitle(I)V

    const v1, 0x7f0d001b

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_185

    const v2, 0x7f07016c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_185
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    check-cast v0, Lcom/android/launcher2/cu;

    iget-object v1, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1b0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_19d
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e:Landroid/widget/EditText;

    iget-object v0, v0, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_e3

    :cond_1b0
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a()V

    goto :goto_19d
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 470
    packed-switch p1, :pswitch_data_1a

    .line 476
    const/4 v0, 0x0

    :goto_5
    return-object v0

    .line 472
    :pswitch_6
    new-instance v0, Lcom/anddoes/launcher/ui/y;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ui/y;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;B)V

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/y;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_5

    .line 474
    :pswitch_10
    new-instance v0, Lcom/anddoes/launcher/ui/x;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ui/x;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;B)V

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/x;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_5

    .line 470
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_6
        :pswitch_10
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 138
    if-eqz p1, :cond_65

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_65

    .line 139
    const-string v0, "mBitmap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    .line 140
    const-string v0, "mIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    .line 141
    const-string v0, "mIconResource"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent$ShortcutIconResource;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    .line 142
    const-string v0, "mOriginalIconResource"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent$ShortcutIconResource;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    .line 143
    const-string v0, "mIconResource"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->k:I

    .line 145
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_69

    .line 146
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 156
    :cond_44
    :goto_44
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    const-string v1, "btPickActivity_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    const-string v1, "btPickIcon_enabled"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 158
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    const-string v1, "btOk_enabled"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 160
    :cond_65
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 161
    return-void

    .line 147
    :cond_69
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    if-eqz v0, :cond_44

    .line 150
    :try_start_6d
    iget-object v0, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 152
    iget-object v2, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_8a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6d .. :try_end_8a} :catch_8b

    goto :goto_44

    :catch_8b
    move-exception v0

    goto :goto_44
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 126
    const-string v0, "mBitmap"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 127
    const-string v0, "mIntent"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    const-string v0, "mIconResource"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i:Landroid/content/Intent$ShortcutIconResource;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 129
    const-string v0, "mOriginalIconResource"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j:Landroid/content/Intent$ShortcutIconResource;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    const-string v0, "mIconSize"

    iget v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    const-string v0, "btOk_enabled"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 132
    const-string v0, "btPickIcon_enabled"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    const-string v0, "btPickActivity_text"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 134
    return-void
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 119
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 120
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/EditShortcut"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 121
    return-void
.end method
