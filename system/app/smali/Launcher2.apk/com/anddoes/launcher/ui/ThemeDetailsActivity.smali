.class public Lcom/anddoes/launcher/ui/ThemeDetailsActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/anddoes/launcher/preference/h;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/anddoes/launcher/c/i;

.field private f:Landroid/widget/CheckBox;

.field private g:Landroid/widget/CheckBox;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/widget/CheckBox;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/preference/h;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->a:Lcom/anddoes/launcher/preference/h;

    return-object v0
.end method

.method static synthetic c(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic f(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic g(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic h(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/c/i;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 283
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->showDialog(I)V

    .line 284
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 288
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->dismissDialog(I)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_4} :catch_5

    .line 292
    :goto_4
    return-void

    :catch_5
    move-exception v0

    goto :goto_4
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f070151

    const/4 v4, 0x0

    .line 223
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->j:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 224
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->finish()V

    .line 264
    :cond_f
    :goto_f
    return-void

    .line 225
    :cond_10
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->k:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 226
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    .line 227
    const-string v1, "User Action"

    .line 228
    const-string v2, "Manage Theme"

    const-string v3, "rate_theme"

    .line 226
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 229
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    .line 230
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_35

    const-string v1, "default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 231
    :cond_35
    invoke-static {p0}, Lcom/anddoes/launcher/a/e;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anddoes/launcher/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_62

    .line 232
    const-string v0, "com.anddoes.launcher.pro"

    .line 235
    :cond_41
    :goto_41
    :try_start_41
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    .line 236
    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 235
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 237
    const/high16 v0, 0x1000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 238
    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_58} :catch_59

    goto :goto_f

    .line 240
    :catch_59
    move-exception v0

    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_f

    .line 232
    :cond_62
    const-string v0, "com.anddoes.launcher"

    goto :goto_41

    .line 242
    :cond_65
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->l:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 243
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/anddoes/launcher/c/l;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_87

    .line 244
    const v0, 0x7f070279

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 245
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->finish()V

    goto :goto_f

    .line 248
    :cond_87
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    if-eqz v0, :cond_93

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 249
    :cond_93
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    if-eqz v0, :cond_9f

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 250
    :cond_9f
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_ab

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 251
    :cond_ab
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    if-eqz v0, :cond_b7

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 252
    :cond_b7
    const v0, 0x7f070115

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_f

    .line 255
    :cond_c3
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    .line 256
    const-string v1, "User Action"

    .line 257
    const-string v2, "Manage Theme"

    const-string v3, "apply_theme"

    .line 255
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 259
    :try_start_d0
    new-instance v0, Lcom/anddoes/launcher/ui/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ui/aq;-><init>(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;B)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/aq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_dc
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_dc} :catch_de

    goto/16 :goto_f

    .line 261
    :catch_de
    move-exception v0

    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_f
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter

    .prologue
    const/16 v8, 0x14

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->setContentView(I)V

    .line 71
    new-instance v0, Lcom/anddoes/launcher/preference/h;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->a:Lcom/anddoes/launcher/preference/h;

    .line 72
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 73
    if-nez v0, :cond_1e

    .line 74
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->finish()V

    .line 77
    :cond_1e
    const-string v1, "com.anddoes.launcher.THEME_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3f

    .line 79
    const v1, 0x7f070279

    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 80
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->finish()V

    .line 83
    :cond_3f
    const-string v1, "com.anddoes.launcher.THEME_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->c:Ljava/lang/String;

    .line 84
    const-string v1, "com.anddoes.launcher.THEME_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    .line 85
    const-string v0, "apex_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22a

    .line 86
    new-instance v0, Lcom/anddoes/launcher/c/b;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    .line 98
    :goto_62
    const v0, 0x1020006

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 99
    if-eqz v0, :cond_7c

    .line 100
    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v1}, Lcom/anddoes/launcher/c/i;->m()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_27c

    .line 102
    invoke-static {v1, p0}, Lcom/android/launcher2/jj;->b(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    :cond_7c
    :goto_7c
    const v0, 0x1020016

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    if-eqz v0, :cond_94

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_94

    .line 110
    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :cond_94
    const v0, 0x1020010

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    if-eqz v0, :cond_ae

    .line 114
    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v1}, Lcom/anddoes/launcher/c/i;->k()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_284

    .line 116
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_ae
    :goto_ae
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->b()V

    .line 123
    const v0, 0x7f0d004c

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ui/SimplePagedView;

    .line 124
    const v1, 0x7f0d004b

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/ui/CircleIndicator;

    .line 126
    const v2, 0x7f0d005d

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 127
    if-eqz v2, :cond_db

    .line 128
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v3}, Lcom/anddoes/launcher/c/i;->n()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 129
    if-eqz v3, :cond_289

    .line 130
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    :cond_db
    :goto_db
    const v2, 0x7f0d005e

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 137
    if-eqz v2, :cond_10a

    .line 138
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v3}, Lcom/anddoes/launcher/c/i;->o()Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_298

    .line 140
    const-string v4, "adw_theme"

    iget-object v5, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28e

    .line 141
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 142
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_10a
    :goto_10a
    const v2, 0x7f0d005f

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 153
    if-eqz v2, :cond_142

    .line 154
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v3}, Lcom/anddoes/launcher/c/i;->l()Ljava/lang/String;

    move-result-object v3

    .line 155
    invoke-static {v3}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2a0

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    const v5, 0x7f070111

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    :cond_142
    :goto_142
    iget-object v2, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v2}, Lcom/anddoes/launcher/c/i;->p()Ljava/util/List;

    move-result-object v2

    .line 163
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2a5

    .line 172
    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/SimplePagedView;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2c3

    .line 173
    iput-boolean v6, v0, Lcom/anddoes/launcher/ui/SimplePagedView;->aq:Z

    .line 174
    invoke-static {}, Lcom/anddoes/launcher/v;->d()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x4000

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/CircleIndicator;->setRadius(F)V

    .line 175
    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/SimplePagedView;->setPageIndicator(Lcom/android/launcher2/hz;)V

    .line 181
    :goto_169
    const v0, 0x7f0d0007

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    .line 182
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    if-eqz v0, :cond_18a

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->e()Z

    move-result v0

    if-nez v0, :cond_18a

    .line 183
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 184
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 186
    :cond_18a
    const v0, 0x7f0d0008

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    .line 187
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1ab

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->f()Z

    move-result v0

    if-nez v0, :cond_1ab

    .line 188
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 189
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 191
    :cond_1ab
    const v0, 0x7f0d0009

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    .line 192
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1cc

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->g()Z

    move-result v0

    if-nez v0, :cond_1cc

    .line 193
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 194
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 196
    :cond_1cc
    const v0, 0x7f0d000a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1ed

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->h()Z

    move-result v0

    if-nez v0, :cond_1ed

    .line 198
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 199
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 201
    :cond_1ed
    const v0, 0x7f0d0021

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->j:Landroid/widget/Button;

    .line 202
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->j:Landroid/widget/Button;

    if-eqz v0, :cond_201

    .line 203
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    :cond_201
    const v0, 0x7f0d0060

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->k:Landroid/widget/Button;

    .line 206
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->k:Landroid/widget/Button;

    if-eqz v0, :cond_215

    .line 207
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    :cond_215
    const v0, 0x7f0d0061

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->l:Landroid/widget/Button;

    .line 210
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->l:Landroid/widget/Button;

    if-eqz v0, :cond_229

    .line 211
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    :cond_229
    return-void

    .line 87
    :cond_22a
    const-string v0, "adw_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23f

    .line 88
    new-instance v0, Lcom/anddoes/launcher/c/a;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    goto/16 :goto_62

    .line 89
    :cond_23f
    const-string v0, "lp_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_254

    .line 90
    new-instance v0, Lcom/anddoes/launcher/c/f;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    goto/16 :goto_62

    .line 91
    :cond_254
    const-string v0, "go_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_269

    .line 92
    new-instance v0, Lcom/anddoes/launcher/c/c;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    goto/16 :goto_62

    .line 94
    :cond_269
    new-instance v0, Lcom/anddoes/launcher/c/b;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    .line 95
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->c:Ljava/lang/String;

    goto/16 :goto_62

    .line 104
    :cond_27c
    const v1, 0x7f02007b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7c

    .line 118
    :cond_284
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_ae

    .line 132
    :cond_289
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_db

    .line 144
    :cond_28e
    const/16 v4, 0xf

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 145
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_10a

    .line 148
    :cond_298
    const v3, 0x7f070110

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_10a

    .line 158
    :cond_2a0
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_142

    .line 163
    :cond_2a5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    .line 164
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 165
    invoke-virtual {v4, v8, v8, v8, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 166
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 167
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 169
    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/ui/SimplePagedView;->addView(Landroid/view/View;)V

    goto/16 :goto_14c

    .line 177
    :cond_2c3
    iput-boolean v6, v0, Lcom/anddoes/launcher/ui/SimplePagedView;->ao:Z

    .line 178
    invoke-virtual {v1, v7}, Lcom/anddoes/launcher/ui/CircleIndicator;->setVisibility(I)V

    goto/16 :goto_169
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 4
    .parameter

    .prologue
    .line 268
    packed-switch p1, :pswitch_data_2c

    .line 279
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 271
    :pswitch_8
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 272
    sget v1, Landroid/app/ProgressDialog;->STYLE_SPINNER:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 273
    const v1, 0x7f070015

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 274
    const v1, 0x7f070017

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 275
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 276
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_7

    .line 268
    nop

    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 217
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 218
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/Settings/ThemeDetails"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 219
    return-void
.end method
