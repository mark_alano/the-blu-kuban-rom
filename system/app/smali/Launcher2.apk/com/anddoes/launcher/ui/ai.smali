.class final Lcom/anddoes/launcher/ui/ai;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/ui/LicenseActivity;

.field private b:Lcom/anddoes/launcher/a/g;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/ui/LicenseActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Lcom/anddoes/launcher/ui/ai;->a:Lcom/anddoes/launcher/ui/LicenseActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/ui/LicenseActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/ai;-><init>(Lcom/anddoes/launcher/ui/LicenseActivity;)V

    return-void
.end method

.method private varargs a([Ljava/lang/String;)Ljava/lang/Void;
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f07019e

    .line 175
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ai;->b:Lcom/anddoes/launcher/a/g;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1}, Lcom/anddoes/launcher/a/g;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    const v1, 0x7f070197

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    .line 176
    :cond_13
    :goto_13
    const/4 v0, 0x0

    return-object v0

    .line 175
    :cond_15
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "type=2&key="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&androidid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/anddoes/launcher/a/g;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/anddoes/launcher/v;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http://apex1.anddoes.com/license/"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    if-nez v2, :cond_46

    const-string v2, "http://apex2.anddoes.com/license/"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_46
    iget-object v1, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    if-nez v1, :cond_59

    const v1, 0x7f07019d

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    :goto_4f
    iget v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    if-nez v1, :cond_13

    iget-object v0, v0, Lcom/anddoes/launcher/a/g;->d:Lcom/anddoes/launcher/a/e;

    invoke-virtual {v0}, Lcom/anddoes/launcher/a/e;->f()V

    goto :goto_13

    :cond_59
    :try_start_59
    iget-object v1, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    iget-object v1, v1, Lcom/anddoes/launcher/a/i;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_59 .. :try_end_60} :catch_99

    move-result v1

    sparse-switch v1, :sswitch_data_9c

    :goto_64
    iput v4, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :sswitch_67
    const v1, 0x7f070199

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :sswitch_6d
    const v1, 0x7f07019a

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :sswitch_73
    const v1, 0x7f07019c

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :sswitch_79
    iget-object v1, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    iget-object v1, v1, Lcom/anddoes/launcher/a/i;->b:Ljava/lang/String;

    if-eqz v1, :cond_96

    iget-object v1, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    iget-object v1, v1, Lcom/anddoes/launcher/a/i;->c:Ljava/lang/String;

    if-eqz v1, :cond_96

    iget-object v1, v0, Lcom/anddoes/launcher/a/g;->d:Lcom/anddoes/launcher/a/e;

    iget-object v2, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    iget-object v2, v2, Lcom/anddoes/launcher/a/i;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/anddoes/launcher/a/g;->c:Lcom/anddoes/launcher/a/i;

    iget-object v3, v3, Lcom/anddoes/launcher/a/i;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/a/e;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :cond_96
    iput v4, v0, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_4f

    :catch_99
    move-exception v1

    goto :goto_64

    nop

    :sswitch_data_9c
    .sparse-switch
        0xa -> :sswitch_67
        0xb -> :sswitch_6d
        0x14 -> :sswitch_73
        0xc8 -> :sswitch_79
    .end sparse-switch
.end method


# virtual methods
.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/ai;->a([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 11
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ai;->a:Lcom/anddoes/launcher/ui/LicenseActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/LicenseActivity;->b(Lcom/anddoes/launcher/ui/LicenseActivity;)V

    iget-object v4, p0, Lcom/anddoes/launcher/ui/ai;->b:Lcom/anddoes/launcher/a/g;

    iget-object v0, v4, Lcom/anddoes/launcher/a/g;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_5f

    iget-object v0, v4, Lcom/anddoes/launcher/a/g;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_5f

    iget v1, v4, Lcom/anddoes/launcher/a/g;->b:I

    if-nez v1, :cond_60

    move v1, v2

    :goto_1e
    invoke-static {v0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v5

    const-string v6, "User Action"

    const-string v7, "Unlock Pro Version"

    const-string v8, "license_key"

    if-eqz v1, :cond_62

    :goto_2a
    invoke-virtual {v5, v6, v7, v8, v2}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz v1, :cond_64

    const v2, 0x108009b

    :goto_37
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    if-eqz v1, :cond_68

    const/high16 v2, 0x7f07

    :goto_3f
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    if-eqz v1, :cond_6c

    const v2, 0x7f07019f

    :goto_48
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070007

    new-instance v5, Lcom/anddoes/launcher/a/h;

    invoke-direct {v5, v4, v1, v0}, Lcom/anddoes/launcher/a/h;-><init>(Lcom/anddoes/launcher/a/g;ZLandroid/app/Activity;)V

    invoke-virtual {v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_5f
    return-void

    :cond_60
    move v1, v3

    goto :goto_1e

    :cond_62
    move v2, v3

    goto :goto_2a

    :cond_64
    const v2, 0x1080027

    goto :goto_37

    :cond_68
    const v2, 0x7f070005

    goto :goto_3f

    :cond_6c
    iget v2, v4, Lcom/anddoes/launcher/a/g;->b:I

    goto :goto_48
.end method

.method protected final onPreExecute()V
    .registers 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ai;->a:Lcom/anddoes/launcher/ui/LicenseActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/LicenseActivity;->a(Lcom/anddoes/launcher/ui/LicenseActivity;)V

    .line 169
    new-instance v0, Lcom/anddoes/launcher/a/g;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/ai;->a:Lcom/anddoes/launcher/ui/LicenseActivity;

    invoke-direct {v0, v1}, Lcom/anddoes/launcher/a/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ai;->b:Lcom/anddoes/launcher/a/g;

    .line 170
    return-void
.end method
