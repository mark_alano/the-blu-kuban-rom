.class final Lcom/anddoes/launcher/ui/aq;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 294
    iput-object p1, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 294
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/aq;-><init>(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 5

    .prologue
    .line 304
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->a(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->a(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 305
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/preference/h;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->c(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theme_icon_type"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/preference/h;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theme_iconpack_pkg"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_36
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->e(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 309
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/preference/h;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theme_skin_pkg"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_5b
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_80

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->f(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_80

    .line 312
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/preference/h;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->d(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theme_font_pkg"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_80
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_c3

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->g(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 316
    :try_start_94
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    const-string v1, "wallpaper"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/WallpaperManager;

    .line 317
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 318
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 319
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 321
    iget-object v2, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/c/i;

    move-result-object v2

    iget-object v2, v2, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->h(Lcom/anddoes/launcher/ui/ThemeDetailsActivity;)Lcom/anddoes/launcher/c/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anddoes/launcher/c/i;->q()I

    move-result v3

    .line 320
    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 322
    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_c3
    .catch Ljava/io/IOException; {:try_start_94 .. :try_end_c3} :catch_c7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_94 .. :try_end_c3} :catch_c5

    .line 327
    :cond_c3
    :goto_c3
    const/4 v0, 0x0

    return-object v0

    :catch_c5
    move-exception v0

    goto :goto_c3

    .line 323
    :catch_c7
    move-exception v0

    goto :goto_c3
.end method


# virtual methods
.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/aq;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6
    .parameter

    .prologue
    .line 1
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->b()V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    instance-of v1, v0, Lcom/android/launcher2/LauncherApplication;

    if-eqz v1, :cond_14

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    :cond_14
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    const-class v3, Lcom/anddoes/launcher/Launcher;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->finish()V

    return-void
.end method

.method protected final onPreExecute()V
    .registers 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/anddoes/launcher/ui/aq;->a:Lcom/anddoes/launcher/ui/ThemeDetailsActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/ThemeDetailsActivity;->a()V

    .line 299
    return-void
.end method
