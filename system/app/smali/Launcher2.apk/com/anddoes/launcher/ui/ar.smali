.class final Lcom/anddoes/launcher/ui/ar;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field final synthetic b:Lcom/anddoes/launcher/ui/ThemeListActivity;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/ui/ThemeListActivity;)V
    .registers 8
    .parameter

    .prologue
    .line 117
    iput-object p1, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 118
    invoke-virtual {p1}, Lcom/anddoes/launcher/ui/ThemeListActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    .line 122
    invoke-static {v1}, Lcom/anddoes/launcher/c/b;->b(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 123
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 124
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6e

    .line 130
    invoke-static {p1}, Lcom/anddoes/launcher/ui/ThemeListActivity;->a(Lcom/anddoes/launcher/ui/ThemeListActivity;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 131
    invoke-static {v1}, Lcom/anddoes/launcher/c/a;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 132
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_31
    :goto_31
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_86

    .line 140
    invoke-static {v1}, Lcom/anddoes/launcher/c/c;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3f
    :goto_3f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a4

    .line 149
    invoke-static {v1}, Lcom/anddoes/launcher/c/f;->b(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 150
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4d
    :goto_4d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 158
    :cond_53
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 159
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    new-instance v1, Lcom/anddoes/launcher/c/k;

    invoke-direct {v1}, Lcom/anddoes/launcher/c/k;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 162
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    const/4 v1, 0x0

    new-instance v2, Lcom/anddoes/launcher/c/b;

    const-string v3, "default"

    invoke-direct {v2, p1, v3}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 163
    return-void

    .line 124
    :cond_6e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 125
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 127
    iget-object v4, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    new-instance v5, Lcom/anddoes/launcher/c/b;

    invoke-direct {v5, p1, v0}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 132
    :cond_86
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 133
    invoke-static {v2, v0}, Lcom/anddoes/launcher/ui/ar;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v4

    if-nez v4, :cond_31

    .line 134
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 136
    iget-object v4, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    new-instance v5, Lcom/anddoes/launcher/c/a;

    invoke-direct {v5, p1, v0}, Lcom/anddoes/launcher/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_31

    .line 141
    :cond_a4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 142
    invoke-static {v2, v0}, Lcom/anddoes/launcher/ui/ar;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v4

    if-nez v4, :cond_3f

    .line 143
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 145
    iget-object v4, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    new-instance v5, Lcom/anddoes/launcher/c/c;

    invoke-direct {v5, p1, v0}, Lcom/anddoes/launcher/c/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3f

    .line 150
    :cond_c3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 151
    invoke-static {v2, v0}, Lcom/anddoes/launcher/ui/ar;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v3

    if-nez v3, :cond_4d

    .line 152
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 154
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    new-instance v4, Lcom/anddoes/launcher/c/f;

    invoke-direct {v4, p1, v0}, Lcom/anddoes/launcher/c/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4d
.end method

.method private static a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 166
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 171
    const/4 v0, 0x0

    :goto_b
    return v0

    .line 166
    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 167
    invoke-static {v0, p1}, Lcom/anddoes/launcher/v;->a(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 168
    const/4 v0, 0x1

    goto :goto_b
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 181
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ar;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 182
    :cond_8
    const/4 v0, 0x0

    .line 184
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_9
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 189
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    const v3, 0x7f020008

    const v5, 0x7f020007

    const v4, 0x7f020006

    const v6, 0x7f020005

    .line 194
    if-ltz p1, :cond_15

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/ar;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_17

    .line 195
    :cond_15
    const/4 p2, 0x0

    .line 242
    :goto_16
    return-object p2

    .line 198
    :cond_17
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ar;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/c/i;

    .line 200
    if-nez p2, :cond_f9

    .line 201
    new-instance v2, Lcom/anddoes/launcher/ui/as;

    invoke-direct {v2, p0, v8}, Lcom/anddoes/launcher/ui/as;-><init>(Lcom/anddoes/launcher/ui/ar;B)V

    .line 202
    iget-object v1, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/ThemeListActivity;->b(Lcom/anddoes/launcher/ui/ThemeListActivity;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v7, 0x7f030033

    invoke-virtual {v1, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 203
    const v1, 0x7f0d0002

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->a:Landroid/widget/ImageView;

    .line 204
    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->b:Landroid/widget/TextView;

    .line 205
    const v1, 0x7f0d0062

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->c:Landroid/widget/ImageView;

    .line 206
    const v1, 0x7f0d0063

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->d:Landroid/widget/ImageView;

    .line 207
    const v1, 0x7f0d0065

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->e:Landroid/widget/ImageView;

    .line 208
    const v1, 0x7f0d0064

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/anddoes/launcher/ui/as;->f:Landroid/widget/ImageView;

    .line 209
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 215
    :goto_79
    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->m()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 216
    if-eqz v2, :cond_101

    .line 217
    iget-object v7, v1, Lcom/anddoes/launcher/ui/as;->a:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v2, v8}, Lcom/android/launcher2/jj;->b(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 221
    :goto_8a
    iget-object v2, v1, Lcom/anddoes/launcher/ui/as;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v2, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/ThemeListActivity;->a(Lcom/anddoes/launcher/ui/ThemeListActivity;)Z

    move-result v2

    if-eqz v2, :cond_10a

    .line 223
    iget-object v2, v1, Lcom/anddoes/launcher/ui/as;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->d()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    :goto_a4
    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v7}, Lcom/anddoes/launcher/ui/ThemeListActivity;->c(Lcom/anddoes/launcher/ui/ThemeListActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_114

    .line 228
    iget-object v7, v1, Lcom/anddoes/launcher/ui/as;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->e()Z

    move-result v2

    if-eqz v2, :cond_112

    move v2, v3

    :goto_bd
    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 232
    :goto_c0
    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v7}, Lcom/anddoes/launcher/ui/ThemeListActivity;->d(Lcom/anddoes/launcher/ui/ThemeListActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_125

    .line 233
    iget-object v7, v1, Lcom/anddoes/launcher/ui/as;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->f()Z

    move-result v2

    if-eqz v2, :cond_123

    move v2, v3

    :goto_d9
    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 237
    :goto_dc
    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/anddoes/launcher/ui/ar;->b:Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-static {v7}, Lcom/anddoes/launcher/ui/ThemeListActivity;->e(Lcom/anddoes/launcher/ui/ThemeListActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_136

    .line 238
    iget-object v1, v1, Lcom/anddoes/launcher/ui/as;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->g()Z

    move-result v0

    if-eqz v0, :cond_134

    :goto_f4
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_16

    .line 212
    :cond_f9
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/ui/as;

    goto/16 :goto_79

    .line 219
    :cond_101
    iget-object v2, v1, Lcom/anddoes/launcher/ui/as;->a:Landroid/widget/ImageView;

    const v7, 0x7f02007b

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8a

    .line 225
    :cond_10a
    iget-object v2, v1, Lcom/anddoes/launcher/ui/as;->c:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_a4

    :cond_112
    move v2, v4

    .line 228
    goto :goto_bd

    .line 230
    :cond_114
    iget-object v7, v1, Lcom/anddoes/launcher/ui/as;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->e()Z

    move-result v2

    if-eqz v2, :cond_121

    move v2, v5

    :goto_11d
    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_c0

    :cond_121
    move v2, v6

    goto :goto_11d

    :cond_123
    move v2, v4

    .line 233
    goto :goto_d9

    .line 235
    :cond_125
    iget-object v7, v1, Lcom/anddoes/launcher/ui/as;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->f()Z

    move-result v2

    if-eqz v2, :cond_132

    move v2, v5

    :goto_12e
    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_dc

    :cond_132
    move v2, v6

    goto :goto_12e

    :cond_134
    move v3, v4

    .line 238
    goto :goto_f4

    .line 240
    :cond_136
    iget-object v1, v1, Lcom/anddoes/launcher/ui/as;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->g()Z

    move-result v0

    if-eqz v0, :cond_143

    :goto_13e
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_16

    :cond_143
    move v5, v6

    goto :goto_13e
.end method
