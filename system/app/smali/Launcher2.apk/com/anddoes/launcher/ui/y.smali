.class final Lcom/anddoes/launcher/ui/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

.field private b:Landroid/widget/ArrayAdapter;

.field private c:Ljava/util/List;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 480
    iput-object p1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    const-string v0, "default_icon"

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->d:Ljava/lang/String;

    .line 488
    const-string v0, "select_picture"

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->e:Ljava/lang/String;

    .line 489
    const-string v0, "crop_picture"

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->f:Ljava/lang/String;

    .line 490
    const-string v0, "icon_pack"

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->g:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 480
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/y;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V

    return-void
.end method

.method private static a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 560
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 565
    const/4 v0, 0x0

    :goto_b
    return v0

    .line 560
    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 561
    invoke-static {v0, p1}, Lcom/anddoes/launcher/v;->a(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 562
    const/4 v0, 0x1

    goto :goto_b
.end method


# virtual methods
.method final a()Landroid/app/Dialog;
    .registers 8

    .prologue
    .line 493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    .line 494
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f030003

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    .line 495
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f070171

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 496
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f070172

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 498
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f070173

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 500
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_c2

    .line 503
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Ljava/util/List;)V

    .line 505
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/c/b;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 508
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_77
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_11b

    .line 513
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/anddoes/launcher/c/a;->a(Landroid/content/pm/PackageManager;Z)Ljava/util/List;

    move-result-object v0

    .line 514
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_91
    :goto_91
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_138

    .line 521
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/c/f;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 522
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a5
    :goto_a5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_15b

    .line 529
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/c/c;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 530
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b9
    :goto_b9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_17e

    .line 537
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 538
    :cond_c2
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/anddoes/launcher/ui/w;

    invoke-direct {v1}, Lcom/anddoes/launcher/ui/w;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 542
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f1

    .line 543
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f070174

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 544
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 547
    :cond_f1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 548
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v2, 0x7f07016f

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 549
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->b:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 550
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 552
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 553
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 554
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 555
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 556
    return-object v0

    .line 508
    :cond_11b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 509
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    iget-object v3, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/anddoes/launcher/ui/v;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const-string v6, "apex_theme"

    invoke-direct {v4, v5, v0, v6}, Lcom/anddoes/launcher/ui/v;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_77

    .line 514
    :cond_138
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 515
    invoke-static {v1, v0}, Lcom/anddoes/launcher/ui/y;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v3

    if-nez v3, :cond_91

    .line 516
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    iget-object v3, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/anddoes/launcher/ui/v;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const-string v6, "adw_theme"

    invoke-direct {v4, v5, v0, v6}, Lcom/anddoes/launcher/ui/v;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_91

    .line 522
    :cond_15b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 523
    invoke-static {v1, v0}, Lcom/anddoes/launcher/ui/y;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v3

    if-nez v3, :cond_a5

    .line 524
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 525
    iget-object v3, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/anddoes/launcher/ui/v;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const-string v6, "lp_theme"

    invoke-direct {v4, v5, v0, v6}, Lcom/anddoes/launcher/ui/v;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a5

    .line 530
    :cond_17e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 531
    invoke-static {v1, v0}, Lcom/anddoes/launcher/ui/y;->a(Ljava/util/List;Landroid/content/pm/ResolveInfo;)Z

    move-result v3

    if-nez v3, :cond_b9

    .line 532
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    iget-object v3, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/anddoes/launcher/ui/v;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const-string v6, "go_theme"

    invoke-direct {v4, v5, v0, v6}, Lcom/anddoes/launcher/ui/v;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b9
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 570
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const v6, 0x7f070151

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 582
    if-ltz p2, :cond_10

    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_11

    .line 643
    :cond_10
    :goto_10
    return-void

    .line 585
    :cond_11
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 586
    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a0

    .line 588
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/di;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_8a

    .line 589
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 590
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->e(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/content/Intent$ShortcutIconResource;)V

    .line 593
    :try_start_3e
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->b(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    iget-object v2, v2, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_4f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3e .. :try_end_4f} :catch_87

    move-result-object v0

    .line 597
    :goto_50
    if-eqz v0, :cond_10

    .line 598
    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->f(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    iget-object v2, v2, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 599
    if-eqz v1, :cond_10

    .line 600
    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->g(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/android/launcher2/LauncherApplication;->b:Lcom/android/launcher2/da;

    invoke-virtual {v2, v0, v1}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0, v2}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;Landroid/graphics/Bitmap;)V

    .line 602
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->h(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->i(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_10

    .line 595
    :catch_87
    move-exception v0

    move-object v0, v1

    goto :goto_50

    .line 606
    :cond_8a
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/di;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_10

    .line 607
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->d(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Lcom/android/launcher2/di;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->j(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V

    goto/16 :goto_10

    .line 609
    :cond_a0
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d5

    .line 611
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 612
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 614
    :try_start_b4
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    .line 615
    iget-object v2, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v3, 0x7f070170

    invoke-virtual {v2, v3}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 614
    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 615
    const/4 v2, 0x1

    .line 614
    invoke-virtual {v1, v0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_c7
    .catch Ljava/lang/Exception; {:try_start_b4 .. :try_end_c7} :catch_c9

    goto/16 :goto_10

    .line 617
    :catch_c9
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 618
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_10

    .line 620
    :cond_d5
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_128

    .line 624
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 625
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 626
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->k(Lcom/anddoes/launcher/ui/EditShortcutActivity;)I

    move-result v1

    .line 627
    const-string v2, "crop"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628
    const-string v2, "outputX"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 629
    const-string v2, "outputY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 630
    const-string v2, "aspectX"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 631
    const-string v2, "aspectY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 632
    const-string v1, "noFaceDetection"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 633
    const-string v1, "return-data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 635
    :try_start_114
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_11a
    .catch Ljava/lang/Exception; {:try_start_114 .. :try_end_11a} :catch_11c

    goto/16 :goto_10

    .line 637
    :catch_11c
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 638
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_10

    .line 640
    :cond_128
    iget-object v1, p0, Lcom/anddoes/launcher/ui/y;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 641
    iget-object v0, p0, Lcom/anddoes/launcher/ui/y;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->showDialog(I)V

    goto/16 :goto_10
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 575
    return-void
.end method

.method public final onShow(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 648
    return-void
.end method
