.class public final Lcom/anddoes/launcher/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/android/launcher2/Launcher;

.field private c:Landroid/os/Handler;

.field private d:Lcom/anddoes/launcher/ab;

.field private e:Lcom/anddoes/launcher/ae;

.field private f:Lcom/anddoes/launcher/ad;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Lcom/anddoes/launcher/ac;

.field private n:Ljava/util/Set;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_mail"

    aput-object v2, v0, v1

    sput-object v0, Lcom/anddoes/launcher/w;->a:[Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 4
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->n:Ljava/util/Set;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/w;->o:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/w;->p:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->k:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    .line 73
    const v0, 0x7f0c002e

    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aR:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/w;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/w;->o:Ljava/lang/String;

    .line 74
    const v0, 0x7f0c002f

    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aT:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/w;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/w;->p:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private a(Landroid/net/Uri;)I
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 258
    .line 260
    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 261
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    .line 262
    const-string v3, "type = ? AND new = ?"

    .line 263
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "1"

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p1

    .line 260
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_26} :catch_3f

    move-result-object v1

    .line 264
    if-eqz v1, :cond_40

    .line 266
    :try_start_29
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_3a
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_2c} :catch_31

    move-result v0

    .line 270
    :try_start_2d
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_30} :catch_44

    .line 277
    :goto_30
    return v0

    :catch_31
    move-exception v0

    .line 270
    :try_start_32
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_35} :catch_37

    move v0, v6

    goto :goto_30

    .line 271
    :catch_37
    move-exception v0

    move v0, v6

    goto :goto_30

    .line 268
    :catchall_3a
    move-exception v0

    .line 270
    :try_start_3b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3e} :catch_42

    .line 273
    :goto_3e
    :try_start_3e
    throw v0
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3f} :catch_3f

    :catch_3f
    move-exception v0

    :cond_40
    move v0, v6

    goto :goto_30

    :catch_42
    move-exception v1

    goto :goto_3e

    :catch_44
    move-exception v1

    goto :goto_30
.end method

.method static synthetic a(Lcom/anddoes/launcher/w;Landroid/net/Uri;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/w;->a(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 79
    const-string v2, ""

    .line 80
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->e()Lcom/android/launcher2/gb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/gb;->j()Lcom/android/launcher2/d;

    move-result-object v1

    iget-object v1, v1, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 82
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2a
    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_32

    move-object v0, p2

    .line 97
    :cond_31
    return-object v0

    .line 82
    :cond_32
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 83
    iget-object v1, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    if-eqz v1, :cond_2a

    .line 84
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4f

    move-object v0, p2

    .line 92
    :goto_47
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_31

    move-object v2, v0

    goto :goto_2a

    .line 88
    :cond_4f
    iget-object v0, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5d

    move-object v0, v1

    .line 89
    goto :goto_47

    :cond_5d
    move-object v0, v2

    goto :goto_47
.end method

.method static synthetic a(Lcom/anddoes/launcher/w;)V
    .registers 1
    .parameter

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->h()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/w;Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_27

    :cond_26
    return-void

    :cond_27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    invoke-direct {p0, p1, p2, v0}, Lcom/anddoes/launcher/w;->a(Ljava/lang/String;ILcom/android/launcher2/ja;)V

    goto :goto_20
.end method

.method static synthetic a(Lcom/anddoes/launcher/w;[Landroid/accounts/Account;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 180
    array-length v7, p1

    move v6, v0

    :goto_4
    if-lt v6, v7, :cond_27

    iget-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    new-instance v0, Lcom/anddoes/launcher/ad;

    iget-object v1, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ad;-><init>(Lcom/anddoes/launcher/w;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    iget-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_92

    invoke-direct {p0}, Lcom/anddoes/launcher/w;->j()V

    :cond_26
    return-void

    :cond_27
    aget-object v0, p1, v6

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_80

    :try_start_31
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "content://com.google.android.gm/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/labels"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "labelUri"

    aput-object v4, v2, v3

    const-string v3, "canonicalName = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "^i"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_66
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_66} :catch_90

    move-result-object v1

    if-eqz v1, :cond_80

    :try_start_69
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7d

    iget-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7d
    .catchall {:try_start_69 .. :try_end_7d} :catchall_8b
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_7d} :catch_84

    :cond_7d
    :try_start_7d
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_80
    .catch Ljava/lang/Exception; {:try_start_7d .. :try_end_80} :catch_a7

    :cond_80
    :goto_80
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    :catch_84
    move-exception v0

    :try_start_85
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_88
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_88} :catch_89

    goto :goto_80

    :catch_89
    move-exception v0

    goto :goto_80

    :catchall_8b
    move-exception v0

    :try_start_8c
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_8f
    .catch Ljava/lang/Exception; {:try_start_8c .. :try_end_8f} :catch_a5

    :goto_8f
    :try_start_8f
    throw v0
    :try_end_90
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_90} :catch_90

    :catch_90
    move-exception v0

    goto :goto_80

    :cond_92
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    invoke-virtual {v2, v0, v9, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_1d

    :catch_a5
    move-exception v1

    goto :goto_8f

    :catch_a7
    move-exception v0

    goto :goto_80
.end method

.method private a(Ljava/lang/String;ILcom/android/launcher2/ja;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 373
    invoke-virtual {p3}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v3

    .line 374
    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-lt v2, v3, :cond_9

    .line 400
    return-void

    .line 375
    :cond_9
    invoke-virtual {p3, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 376
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 377
    instance-of v4, v1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v4, :cond_46

    .line 378
    instance-of v4, v0, Lcom/android/launcher2/jd;

    if-eqz v4, :cond_46

    .line 379
    check-cast v0, Lcom/android/launcher2/jd;

    .line 380
    iget-object v4, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v4, :cond_42

    .line 381
    iget-object v0, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    move-object v0, v1

    .line 383
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 384
    invoke-virtual {v0, p2}, Lcom/android/launcher2/BubbleTextView;->setCounter(I)V

    .line 385
    iget-object v1, p0, Lcom/anddoes/launcher/w;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 386
    iget-object v1, p0, Lcom/anddoes/launcher/w;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    :cond_42
    :goto_42
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 390
    :cond_46
    instance-of v0, v1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_42

    .line 391
    check-cast v1, Lcom/android/launcher2/FolderIcon;

    .line 392
    iget-object v0, v1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    if-eqz v0, :cond_42

    iget-object v0, v1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    iget-object v0, v0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_42

    .line 393
    iget-object v0, v1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    iget-object v0, v0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    .line 394
    if-eqz v0, :cond_42

    .line 395
    invoke-direct {p0, p1, p2, v0}, Lcom/anddoes/launcher/w;->a(Ljava/lang/String;ILcom/android/launcher2/ja;)V

    goto :goto_42
.end method

.method private b(Landroid/net/Uri;)I
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 292
    .line 294
    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 295
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    .line 296
    const-string v3, "read = ?"

    .line 297
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "0"

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p1

    .line 294
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1e} :catch_37

    move-result-object v1

    .line 298
    if-eqz v1, :cond_38

    .line 300
    :try_start_21
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_32
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_24} :catch_29

    move-result v0

    .line 304
    :try_start_25
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_28} :catch_3c

    .line 311
    :goto_28
    return v0

    :catch_29
    move-exception v0

    .line 304
    :try_start_2a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2d} :catch_2f

    move v0, v6

    goto :goto_28

    .line 305
    :catch_2f
    move-exception v0

    move v0, v6

    goto :goto_28

    .line 302
    :catchall_32
    move-exception v0

    .line 304
    :try_start_33
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_36} :catch_3a

    .line 307
    :goto_36
    :try_start_36
    throw v0
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_37} :catch_37

    :catch_37
    move-exception v0

    :cond_38
    move v0, v6

    goto :goto_28

    :catch_3a
    move-exception v1

    goto :goto_36

    :catch_3c
    move-exception v1

    goto :goto_28
.end method

.method static synthetic b(Lcom/anddoes/launcher/w;Landroid/net/Uri;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/w;->b(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/anddoes/launcher/w;)V
    .registers 1
    .parameter

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->i()V

    return-void
.end method

.method private c(Landroid/net/Uri;)I
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 328
    .line 330
    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 331
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "numUnreadConversations"

    aput-object v3, v2, v1

    .line 332
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    .line 330
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_16} :catch_36

    move-result-object v1

    .line 333
    if-eqz v1, :cond_37

    .line 335
    :try_start_19
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 336
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_23
    .catchall {:try_start_19 .. :try_end_23} :catchall_31
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_23} :catch_28

    move-result v0

    .line 341
    :goto_24
    :try_start_24
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_27} :catch_3b

    .line 348
    :goto_27
    return v0

    :catch_28
    move-exception v0

    .line 341
    :try_start_29
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_2c} :catch_2e

    move v0, v6

    goto :goto_27

    .line 342
    :catch_2e
    move-exception v0

    move v0, v6

    goto :goto_27

    .line 339
    :catchall_31
    move-exception v0

    .line 341
    :try_start_32
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_35} :catch_39

    .line 344
    :goto_35
    :try_start_35
    throw v0
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_36} :catch_36

    :catch_36
    move-exception v0

    :cond_37
    move v0, v6

    goto :goto_27

    :catch_39
    move-exception v1

    goto :goto_35

    :catch_3b
    move-exception v1

    goto :goto_27

    :cond_3d
    move v0, v6

    goto :goto_24
.end method

.method static synthetic c(Lcom/anddoes/launcher/w;Landroid/net/Uri;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 327
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/w;->c(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/anddoes/launcher/w;)V
    .registers 1
    .parameter

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->j()V

    return-void
.end method

.method static synthetic d(Lcom/anddoes/launcher/w;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    if-eqz v0, :cond_12

    .line 221
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    .line 224
    :cond_12
    return-void
.end method

.method static synthetic e(Lcom/anddoes/launcher/w;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/anddoes/launcher/w;->n:Ljava/util/Set;

    return-object v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 227
    iget-object v0, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    if-eqz v0, :cond_12

    .line 228
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    .line 231
    :cond_12
    return-void
.end method

.method static synthetic f(Lcom/anddoes/launcher/w;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/anddoes/launcher/w;->o:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    if-eqz v0, :cond_12

    .line 235
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    .line 238
    :cond_12
    iget-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 239
    return-void
.end method

.method static synthetic g(Lcom/anddoes/launcher/w;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/anddoes/launcher/w;->p:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .registers 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/anddoes/launcher/w;->m:Lcom/anddoes/launcher/ac;

    if-eqz v0, :cond_b

    .line 243
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/anddoes/launcher/w;->m:Lcom/anddoes/launcher/ac;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 245
    :cond_b
    return-void
.end method

.method static synthetic h(Lcom/anddoes/launcher/w;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/anddoes/launcher/w;->l:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized h()V
    .registers 5

    .prologue
    .line 248
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    new-instance v1, Lcom/anddoes/launcher/y;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/y;-><init>(Lcom/anddoes/launcher/w;)V

    .line 253
    const-wide/16 v2, 0xc8

    .line 248
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 254
    monitor-exit p0

    return-void

    .line 248
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()V
    .registers 5

    .prologue
    .line 281
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    new-instance v1, Lcom/anddoes/launcher/z;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/z;-><init>(Lcom/anddoes/launcher/w;)V

    .line 287
    const-wide/16 v2, 0xc8

    .line 281
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 288
    monitor-exit p0

    return-void

    .line 281
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .registers 5

    .prologue
    .line 315
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    new-instance v1, Lcom/anddoes/launcher/aa;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/aa;-><init>(Lcom/anddoes/launcher/w;)V

    .line 323
    const-wide/16 v2, 0xc8

    .line 315
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 324
    monitor-exit p0

    return-void

    .line 315
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-nez v0, :cond_9

    .line 357
    :goto_8
    return-void

    .line 355
    :cond_9
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.anddoes.launcher.UPDATE_COUNTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_8
.end method

.method public final a(Lcom/android/launcher2/jd;)V
    .registers 4
    .parameter

    .prologue
    .line 424
    if-eqz p1, :cond_58

    iget-object v0, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v0, :cond_58

    .line 425
    iget-object v0, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 426
    iget-boolean v1, p0, Lcom/anddoes/launcher/w;->g:Z

    if-eqz v1, :cond_23

    .line 427
    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aR:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 428
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->h()V

    .line 430
    :cond_23
    iget-boolean v1, p0, Lcom/anddoes/launcher/w;->h:Z

    if-eqz v1, :cond_36

    .line 431
    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aT:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 432
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->i()V

    .line 434
    :cond_36
    iget-boolean v1, p0, Lcom/anddoes/launcher/w;->i:Z

    if-eqz v1, :cond_49

    .line 435
    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aV:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 436
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->j()V

    .line 438
    :cond_49
    iget-boolean v1, p0, Lcom/anddoes/launcher/w;->j:Z

    if-eqz v1, :cond_58

    iget-object v1, p0, Lcom/anddoes/launcher/w;->n:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 439
    invoke-virtual {p0}, Lcom/anddoes/launcher/w;->a()V

    .line 442
    :cond_58
    return-void
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/anddoes/launcher/w;->g:Z

    .line 102
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->g:Z

    if-eqz v0, :cond_22

    .line 103
    iget-object v0, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    if-nez v0, :cond_21

    .line 104
    new-instance v0, Lcom/anddoes/launcher/ab;

    iget-object v1, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ab;-><init>(Lcom/anddoes/launcher/w;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    .line 105
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 106
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/anddoes/launcher/w;->d:Lcom/anddoes/launcher/ab;

    .line 105
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 111
    :cond_21
    :goto_21
    return-void

    .line 109
    :cond_22
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->d()V

    goto :goto_21
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 403
    iget-object v0, p0, Lcom/anddoes/launcher/w;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 408
    iget-object v0, p0, Lcom/anddoes/launcher/w;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 409
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->g:Z

    if-eqz v0, :cond_18

    .line 410
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->h()V

    .line 412
    :cond_18
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->h:Z

    if-eqz v0, :cond_1f

    .line 413
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->i()V

    .line 415
    :cond_1f
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->i:Z

    if-eqz v0, :cond_26

    .line 416
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->j()V

    .line 418
    :cond_26
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->j:Z

    if-eqz v0, :cond_2d

    .line 419
    invoke-virtual {p0}, Lcom/anddoes/launcher/w;->a()V

    .line 421
    :cond_2d
    return-void

    .line 403
    :cond_2e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 404
    if-eqz v0, :cond_6

    .line 405
    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->b()V

    goto :goto_6
.end method

.method public final b(Z)V
    .registers 6
    .parameter

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/anddoes/launcher/w;->h:Z

    .line 115
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->h:Z

    if-eqz v0, :cond_22

    .line 116
    iget-object v0, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    if-nez v0, :cond_21

    .line 117
    new-instance v0, Lcom/anddoes/launcher/ae;

    iget-object v1, p0, Lcom/anddoes/launcher/w;->c:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ae;-><init>(Lcom/anddoes/launcher/w;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    .line 118
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 119
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_CONVERSATIONS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/anddoes/launcher/w;->e:Lcom/anddoes/launcher/ae;

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 124
    :cond_21
    :goto_21
    return-void

    .line 122
    :cond_22
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->e()V

    goto :goto_21
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->e()V

    .line 446
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->d()V

    .line 447
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->f()V

    .line 448
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->g()V

    .line 449
    return-void
.end method

.method public final c(Z)V
    .registers 7
    .parameter

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/anddoes/launcher/w;->i:Z

    .line 128
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->i:Z

    if-eqz v0, :cond_1e

    .line 129
    iget-object v0, p0, Lcom/anddoes/launcher/w;->f:Lcom/anddoes/launcher/ad;

    if-nez v0, :cond_1d

    .line 130
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    sget-object v2, Lcom/anddoes/launcher/w;->a:[Ljava/lang/String;

    new-instance v3, Lcom/anddoes/launcher/x;

    invoke-direct {v3, p0}, Lcom/anddoes/launcher/x;-><init>(Lcom/anddoes/launcher/w;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 135
    :cond_1d
    :goto_1d
    return-void

    .line 133
    :cond_1e
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->f()V

    goto :goto_1d
.end method

.method public final d(Z)V
    .registers 7
    .parameter

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/anddoes/launcher/w;->j:Z

    .line 139
    iget-boolean v0, p0, Lcom/anddoes/launcher/w;->j:Z

    if-eqz v0, :cond_28

    .line 140
    iget-object v0, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_27

    new-instance v0, Lcom/anddoes/launcher/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/ac;-><init>(Lcom/anddoes/launcher/w;B)V

    iput-object v0, p0, Lcom/anddoes/launcher/w;->m:Lcom/anddoes/launcher/ac;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.anddoes.launcher.COUNTER_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/w;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/anddoes/launcher/w;->m:Lcom/anddoes/launcher/ac;

    const-string v3, "com.anddoes.launcher.permission.UPDATE_COUNT"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 144
    :cond_27
    :goto_27
    return-void

    .line 142
    :cond_28
    invoke-direct {p0}, Lcom/anddoes/launcher/w;->g()V

    goto :goto_27
.end method
