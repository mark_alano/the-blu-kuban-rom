.class public Lcom/android/launcher2/AppsCustomizeTabHost;
.super Landroid/widget/TabHost;
.source "SourceFile"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lcom/android/launcher2/hn;


# instance fields
.field public a:Ljava/util/List;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Landroid/view/ViewGroup;

.field private d:Lcom/android/launcher2/AppsCustomizePagedView;

.field private e:Landroid/widget/FrameLayout;

.field private f:Landroid/widget/LinearLayout;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/android/launcher2/Launcher;

.field private k:Landroid/widget/HorizontalScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 535
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    move-object v0, p1

    .line 70
    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    .line 71
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->b:Landroid/view/LayoutInflater;

    .line 78
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->J:Z

    if-nez v0, :cond_34

    .line 79
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->K:Z

    if-nez v0, :cond_34

    .line 80
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->L:Z

    if-nez v0, :cond_34

    .line 81
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/anddoes/launcher/preference/f;->J:Z

    .line 83
    :cond_34
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    return-object v0
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizeTabHost;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 240
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->setContentType(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/android/launcher2/AppsCustomizeTabHost;)V
    .registers 3
    .parameter

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->c(Z)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->s(I)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->requestFocus()Z

    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->e:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/android/launcher2/AppsCustomizeTabHost;)V
    .registers 3
    .parameter

    .prologue
    .line 228
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->e(Z)V

    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 246
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 248
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->k:Landroid/widget/HorizontalScrollView;

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->k:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    .line 250
    :cond_2c
    return-void
.end method

.method private setVisibilityOfSiblingsWithLowerZOrder(I)V
    .registers 8
    .parameter

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 493
    if-nez v0, :cond_9

    .line 511
    :cond_8
    return-void

    .line 495
    :cond_9
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 496
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->isChildrenDrawingOrderEnabled()Z

    move-result v1

    if-nez v1, :cond_2a

    .line 497
    const/4 v1, 0x0

    :goto_14
    if-ge v1, v2, :cond_8

    .line 498
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 499
    if-eq v3, p0, :cond_8

    .line 500
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_27

    .line 503
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 497
    :cond_27
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 509
    :cond_2a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed; can\'t get z-order of views"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final a()V
    .registers 2

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    if-eqz v0, :cond_8

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->i:Z

    .line 399
    :goto_7
    return-void

    .line 397
    :cond_8
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->c_()V

    goto :goto_7
.end method

.method public final a(F)V
    .registers 2
    .parameter

    .prologue
    .line 467
    return-void
.end method

.method public final a(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 428
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/Launcher;ZZ)V

    .line 429
    iput-boolean v3, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    .line 430
    iput-boolean p3, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->h:Z

    .line 432
    if-eqz p3, :cond_21

    .line 434
    invoke-direct {p0, v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibilityOfSiblingsWithLowerZOrder(I)V

    .line 437
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->C()V

    .line 451
    :goto_15
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->i:Z

    if-eqz v0, :cond_20

    .line 452
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->c_()V

    .line 453
    iput-boolean v2, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->i:Z

    .line 455
    :cond_20
    return-void

    .line 440
    :cond_21
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->b(IZ)V

    goto :goto_15
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getVisibility()I

    move-result v0

    if-nez v0, :cond_23

    .line 515
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getCurrentPage()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->b(IZ)V

    .line 519
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->s(I)V

    .line 521
    :cond_23
    return-void
.end method

.method public final b(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 471
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/Launcher;ZZ)V

    .line 472
    iput-boolean v2, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    .line 473
    if-eqz p2, :cond_e

    .line 474
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setLayerType(ILandroid/graphics/Paint;)V

    .line 477
    :cond_e
    if-nez p3, :cond_24

    .line 479
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibilityOfSiblingsWithLowerZOrder(I)V

    .line 483
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->s(I)V

    .line 486
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->e(Z)V

    .line 489
    :cond_24
    return-void
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 459
    if-eqz p1, :cond_13

    .line 460
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->buildLayer()V

    invoke-static {}, Ljava/lang/System;->gc()V

    .line 462
    :cond_13
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 524
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 528
    return-void
.end method

.method final d()Z
    .registers 2

    .prologue
    .line 531
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    return v0
.end method

.method public getAnimationBuffer()Landroid/widget/FrameLayout;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->e:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getContent()Landroid/view/View;
    .registers 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getDescendantFocusability()I
    .registers 2

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    .line 386
    const/high16 v0, 0x6

    .line 388
    :goto_8
    return v0

    :cond_9
    invoke-super {p0}, Landroid/widget/TabHost;->getDescendantFocusability()I

    move-result v0

    goto :goto_8
.end method

.method protected onFinishInflate()V
    .registers 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-nez v0, :cond_fc

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->J:Z

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    new-instance v1, Lcom/anddoes/launcher/p;

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->o:Lcom/anddoes/launcher/preference/c;

    const-string v4, "apps_all"

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->mContext:Landroid/content/Context;

    const v6, 0x7f070293

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5, v7}, Lcom/anddoes/launcher/p;-><init>(Lcom/anddoes/launcher/preference/c;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->K:Z

    if-eqz v0, :cond_51

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    new-instance v1, Lcom/anddoes/launcher/p;

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->o:Lcom/anddoes/launcher/preference/c;

    const-string v4, "apps_downloaded"

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->mContext:Landroid/content/Context;

    const v6, 0x7f070046

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5, v7}, Lcom/anddoes/launcher/p;-><init>(Lcom/anddoes/launcher/preference/c;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_51
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->L:Z

    if-eqz v0, :cond_72

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    new-instance v1, Lcom/anddoes/launcher/p;

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->o:Lcom/anddoes/launcher/preference/c;

    const-string v4, "widgets_all"

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->mContext:Landroid/content/Context;

    const v6, 0x7f07027a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5, v2}, Lcom/anddoes/launcher/p;-><init>(Lcom/anddoes/launcher/preference/c;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_72
    :goto_72
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setup()V

    .line 117
    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 118
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const v3, 0x7f020081

    .line 119
    const-string v4, "tab_unselected_holo"

    .line 118
    invoke-virtual {v1, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 121
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v4

    .line 123
    const v0, 0x7f0d0010

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 122
    check-cast v0, Lcom/android/launcher2/AppsCustomizePagedView;

    .line 124
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->c:Landroid/view/ViewGroup;

    .line 126
    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    .line 127
    const v1, 0x7f0d0011

    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->e:Landroid/widget/FrameLayout;

    .line 128
    const v1, 0x7f0d000c

    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    .line 129
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->H()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v5, v5, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v5}, Lcom/anddoes/launcher/preference/h;->I()Ljava/lang/String;

    move-result-object v5

    const-string v6, "SMALL"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_108

    const v1, 0x7f0b0020

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_dc
    const-string v6, "SMALL"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_128

    const v5, 0x7f0b0023

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    :goto_eb
    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1, v3, v1, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 131
    if-eqz v4, :cond_f6

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    if-nez v1, :cond_148

    :cond_f6
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    invoke-direct {v0}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    throw v0

    .line 113
    :cond_fc
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->o:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/c;->d()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    goto/16 :goto_72

    .line 129
    :cond_108
    const-string v6, "MEDIUM"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_118

    const v1, 0x7f0b0021

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_dc

    :cond_118
    const-string v6, "LARGE"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f1

    const v1, 0x7f0b0022

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_dc

    :cond_128
    const-string v6, "MEDIUM"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_138

    const v5, 0x7f0b0024

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto :goto_eb

    :cond_138
    const-string v6, "LARGE"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1ee

    const v5, 0x7f0b0025

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto :goto_eb

    .line 135
    :cond_148
    new-instance v3, Lcom/android/launcher2/z;

    invoke-direct {v3, p0, v0}, Lcom/android/launcher2/z;-><init>(Lcom/android/launcher2/AppsCustomizeTabHost;Lcom/android/launcher2/AppsCustomizePagedView;)V

    .line 155
    const v0, 0x7f0d000e

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->k:Landroid/widget/HorizontalScrollView;

    .line 158
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 160
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_16b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_191

    .line 174
    invoke-virtual {p0, p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 177
    new-instance v0, Lcom/android/launcher2/ad;

    invoke-direct {v0}, Lcom/android/launcher2/ad;-><init>()V

    .line 178
    invoke-virtual {v4}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v1

    .line 179
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 180
    const v1, 0x7f0d000f

    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 181
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 185
    return-void

    .line 160
    :cond_191
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 161
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->b:Landroid/view/LayoutInflater;

    const v7, 0x7f030030

    invoke-virtual {v1, v7, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    iget-object v7, v0, Lcom/anddoes/launcher/p;->b:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v7, v0, Lcom/anddoes/launcher/p;->b:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const v8, 0x7f020084

    .line 165
    const-string v9, "tab_widget_indicator"

    .line 164
    invoke-virtual {v7, v8, v9}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 166
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v7

    .line 169
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v8

    .line 167
    invoke-virtual {v1, v5, v7, v5, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 170
    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v7, v1}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 171
    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->j:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v8, "drawer_tab_text_color"

    invoke-virtual {v7, v1, v8}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 172
    iget-object v0, v0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    goto/16 :goto_16b

    :cond_1ee
    move v3, v2

    goto/16 :goto_eb

    :cond_1f1
    move v1, v2

    goto/16 :goto_dc
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->h:Z

    if-eqz v0, :cond_a

    .line 208
    const/4 v0, 0x1

    .line 210
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1}, Landroid/widget/TabHost;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->e()V

    .line 262
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 263
    const v1, 0x7f0a000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 267
    new-instance v1, Lcom/android/launcher2/aa;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/launcher2/aa;-><init>(Lcom/android/launcher2/AppsCustomizeTabHost;Ljava/lang/String;I)V

    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    .line 347
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->g:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->h:Z

    if-eqz v0, :cond_d

    .line 217
    invoke-super {p0, p1}, Landroid/widget/TabHost;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 225
    :goto_c
    return v0

    .line 222
    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizeTabHost;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getBottom()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1e

    .line 223
    const/4 v0, 0x1

    goto :goto_c

    .line 225
    :cond_1e
    invoke-super {p0, p1}, Landroid/widget/TabHost;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_c
.end method

.method public setCurrentTabFromContent(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 350
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 351
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 352
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->e()V

    .line 353
    invoke-virtual {p0, p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 354
    return-void
.end method

.method public setup(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 87
    return-void
.end method
