.class public Lcom/android/launcher2/CellLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final ad:Landroid/graphics/PorterDuffXfermode;


# instance fields
.field private A:Landroid/graphics/drawable/Drawable;

.field private B:Landroid/graphics/drawable/Drawable;

.field private C:Landroid/graphics/drawable/Drawable;

.field private D:Landroid/graphics/Rect;

.field private E:Landroid/graphics/Rect;

.field private F:I

.field private G:Z

.field private final H:Landroid/graphics/Point;

.field private I:[Landroid/graphics/Rect;

.field private J:[F

.field private K:[Lcom/android/launcher2/dg;

.field private L:I

.field private final M:Landroid/graphics/Paint;

.field private N:Lcom/android/launcher2/BubbleTextView;

.field private O:Ljava/util/HashMap;

.field private P:Ljava/util/HashMap;

.field private Q:Z

.field private final R:[I

.field private S:Z

.field private T:Landroid/animation/TimeInterpolator;

.field private U:Lcom/android/launcher2/ja;

.field private V:Z

.field private W:F

.field private Z:Ljava/util/ArrayList;

.field a:[I

.field private aa:Landroid/graphics/Rect;

.field private ab:[I

.field private ac:Lcom/android/launcher2/by;

.field private final ae:Ljava/util/Stack;

.field private af:Z

.field private ag:I

.field private ah:I

.field private ai:Landroid/graphics/drawable/Drawable;

.field private aj:F

.field private final ak:Landroid/graphics/PointF;

.field private al:Lcom/android/launcher2/dg;

.field b:[[Z

.field c:[[Z

.field d:[I

.field public e:Z

.field private f:Lcom/android/launcher2/Launcher;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private final n:Landroid/graphics/Rect;

.field private final o:Lcom/android/launcher2/as;

.field private final p:[I

.field private final q:[I

.field private r:Z

.field private s:Landroid/view/View$OnTouchListener;

.field private t:Ljava/util/ArrayList;

.field private u:[I

.field private v:I

.field private w:F

.field private x:F

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 166
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 165
    sput-object v0, Lcom/android/launcher2/CellLayout;->ad:Landroid/graphics/PorterDuffXfermode;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 169
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 177
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->m:Z

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->n:Landroid/graphics/Rect;

    .line 80
    new-instance v0, Lcom/android/launcher2/as;

    invoke-direct {v0}, Lcom/android/launcher2/as;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->o:Lcom/android/launcher2/as;

    .line 84
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->p:[I

    .line 85
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->q:[I

    .line 86
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    .line 90
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->r:Z

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    .line 95
    new-array v0, v3, [I

    fill-array-data v0, :array_244

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    .line 97
    iput v1, p0, Lcom/android/launcher2/CellLayout;->v:I

    .line 99
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/launcher2/CellLayout;->x:F

    .line 111
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->G:Z

    .line 112
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->H:Landroid/graphics/Point;

    .line 116
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    .line 117
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->J:[F

    .line 119
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    array-length v0, v0

    new-array v0, v0, [Lcom/android/launcher2/dg;

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    .line 122
    iput v1, p0, Lcom/android/launcher2/CellLayout;->L:I

    .line 123
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->M:Landroid/graphics/Paint;

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    .line 132
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->Q:Z

    .line 135
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->R:[I

    .line 137
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->S:Z

    .line 142
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->V:Z

    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    .line 159
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->aa:Landroid/graphics/Rect;

    .line 160
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    .line 161
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->d:[I

    .line 1359
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    .line 3128
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->af:Z

    .line 3131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ai:Landroid/graphics/drawable/Drawable;

    .line 3132
    iput v8, p0, Lcom/android/launcher2/CellLayout;->aj:F

    .line 3133
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ak:Landroid/graphics/PointF;

    .line 3134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    .line 3135
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->e:Z

    .line 178
    new-instance v0, Lcom/android/launcher2/by;

    invoke-direct {v0, p1}, Lcom/android/launcher2/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ac:Lcom/android/launcher2/by;

    .line 182
    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setWillNotDraw(Z)V

    move-object v0, p1

    .line 183
    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    .line 185
    sget-object v0, Lcom/anddoes/launcher/at;->CellLayout:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 187
    const/16 v0, 0xa

    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->ag:I

    .line 188
    const/16 v0, 0xa

    invoke-virtual {v2, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->h:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->ah:I

    .line 192
    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->k:I

    .line 193
    const/4 v0, 0x3

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->l:I

    .line 194
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    .line 195
    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    .line 198
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    .line 199
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    .line 200
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/16 v3, -0x64

    aput v3, v0, v1

    .line 201
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/16 v3, -0x64

    aput v3, v0, v7

    .line 203
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 205
    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 207
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 215
    const v0, 0x7f0b0043

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 214
    iput v0, p0, Lcom/android/launcher2/CellLayout;->F:I

    .line 217
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 218
    const v3, 0x7f02001f

    .line 219
    const-string v4, "homescreen_normal_holo"

    .line 217
    invoke-virtual {v0, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->y:Landroid/graphics/drawable/Drawable;

    .line 220
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 221
    const v3, 0x7f020020

    .line 222
    const-string v4, "homescreen_strong_holo"

    .line 220
    invoke-virtual {v0, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->z:Landroid/graphics/drawable/Drawable;

    .line 224
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 225
    const v3, 0x7f020065

    .line 226
    const-string v4, "overscroll_glow_left"

    .line 224
    invoke-virtual {v0, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->B:Landroid/graphics/drawable/Drawable;

    .line 227
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 228
    const v3, 0x7f020066

    .line 229
    const-string v4, "overscroll_glow_right"

    .line 227
    invoke-virtual {v0, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->C:Landroid/graphics/drawable/Drawable;

    .line 231
    const v0, 0x3df5c28f

    .line 232
    const v3, 0x7f0b004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    .line 231
    iput v0, p0, Lcom/android/launcher2/CellLayout;->W:F

    .line 234
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 235
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 239
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x4020

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->T:Landroid/animation/TimeInterpolator;

    .line 241
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->t:Z

    if-nez v0, :cond_1c0

    .line 242
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 243
    const v3, 0x7f020010

    .line 244
    const-string v4, "gardening_crosshairs"

    .line 242
    invoke-virtual {v0, v3, v4}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->ai:Landroid/graphics/drawable/Drawable;

    .line 246
    new-instance v0, Lcom/android/launcher2/dg;

    const-wide/16 v3, 0x258

    const/high16 v5, 0x3f80

    invoke-direct {v0, v3, v4, v5}, Lcom/android/launcher2/dg;-><init>(JF)V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    .line 248
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    iget-object v0, v0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/android/launcher2/am;

    invoke-direct {v3, p0}, Lcom/android/launcher2/am;-><init>(Lcom/android/launcher2/CellLayout;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 254
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    iget-object v0, v0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->T:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 257
    :cond_1c0
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->R:[I

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->R:[I

    aput v6, v3, v7

    aput v6, v0, v1

    move v0, v1

    .line 258
    :goto_1c9
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    array-length v3, v3

    if-lt v0, v3, :cond_20f

    .line 266
    const v0, 0x7f0a0013

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 267
    const v3, 0x7f0a0014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    .line 270
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->J:[F

    invoke-static {v3, v8}, Ljava/util/Arrays;->fill([FF)V

    .line 272
    :goto_1e2
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    array-length v3, v3

    if-lt v1, v3, :cond_21b

    .line 312
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->D:Landroid/graphics/Rect;

    .line 313
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->E:Landroid/graphics/Rect;

    .line 315
    new-instance v0, Lcom/android/launcher2/ja;

    invoke-direct {v0, p1}, Lcom/android/launcher2/ja;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    .line 316
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    iget v1, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->k:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->l:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/ja;->a(IIII)V

    .line 317
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->addView(Landroid/view/View;)V

    .line 318
    return-void

    .line 259
    :cond_20f
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v4, v3, v0

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c9

    .line 274
    :cond_21b
    new-instance v3, Lcom/android/launcher2/dg;

    int-to-long v4, v0

    invoke-direct {v3, v4, v5, v2}, Lcom/android/launcher2/dg;-><init>(JF)V

    .line 275
    iget-object v4, v3, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->T:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 277
    iget-object v4, v3, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/android/launcher2/an;

    invoke-direct {v5, p0, v3, v1}, Lcom/android/launcher2/an;-><init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/dg;I)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 301
    iget-object v4, v3, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/android/launcher2/ao;

    invoke-direct {v5, p0, v3}, Lcom/android/launcher2/ao;-><init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/dg;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 309
    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    aput-object v3, v4, v1

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e2

    .line 95
    nop

    :array_244
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method

.method static a(Landroid/content/res/Resources;I)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 324
    const v0, 0x7f0b0096

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 325
    const v1, 0x7f0b0098

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 326
    const v2, 0x7f0b0099

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 325
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 328
    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v1, v2

    mul-int/2addr v0, p1

    add-int/2addr v0, v1

    return v0
.end method

.method private a(IIIIIILcom/android/launcher2/at;)Lcom/android/launcher2/at;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2241
    const/4 v1, 0x2

    new-array v8, v1, [I

    .line 2242
    const/4 v1, 0x2

    new-array v9, v1, [I

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    .line 2243
    invoke-direct/range {v1 .. v9}, Lcom/android/launcher2/CellLayout;->b(IIIIII[I[I)[I

    .line 2245
    const/4 v1, 0x0

    aget v1, v8, v1

    if-ltz v1, :cond_42

    const/4 v1, 0x1

    aget v1, v8, v1

    if-ltz v1, :cond_42

    .line 2246
    move-object/from16 v0, p7

    invoke-direct {p0, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;)V

    .line 2247
    const/4 v1, 0x0

    aget v1, v8, v1

    move-object/from16 v0, p7

    iput v1, v0, Lcom/android/launcher2/at;->c:I

    .line 2248
    const/4 v1, 0x1

    aget v1, v8, v1

    move-object/from16 v0, p7

    iput v1, v0, Lcom/android/launcher2/at;->d:I

    .line 2249
    const/4 v1, 0x0

    aget v1, v9, v1

    move-object/from16 v0, p7

    iput v1, v0, Lcom/android/launcher2/at;->e:I

    .line 2250
    const/4 v1, 0x1

    aget v1, v9, v1

    move-object/from16 v0, p7

    iput v1, v0, Lcom/android/launcher2/at;->f:I

    .line 2251
    const/4 v1, 0x1

    move-object/from16 v0, p7

    iput-boolean v1, v0, Lcom/android/launcher2/at;->b:Z

    .line 2255
    :goto_41
    return-object p7

    .line 2253
    :cond_42
    const/4 v1, 0x0

    move-object/from16 v0, p7

    iput-boolean v1, v0, Lcom/android/launcher2/at;->b:Z

    goto :goto_41
.end method

.method private a(IIIIII[ILandroid/view/View;ZZLcom/android/launcher2/at;)Lcom/android/launcher2/at;
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1958
    move-object/from16 v10, p8

    move-object/from16 v9, p7

    move/from16 v5, p6

    move/from16 v4, p5

    move/from16 v3, p2

    move/from16 v2, p1

    move-object/from16 v1, p0

    :goto_e
    move-object/from16 v0, p11

    invoke-direct {v1, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;)V

    .line 1961
    iget-object v8, v1, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v6, 0x0

    :goto_16
    iget v7, v1, Lcom/android/launcher2/CellLayout;->i:I

    if-lt v6, v7, :cond_45

    .line 1965
    const/4 v6, 0x2

    new-array v6, v6, [I

    .line 1966
    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    move-result-object v18

    .line 1968
    if-eqz p10, :cond_5a

    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v6, v6, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v6, v6, Lcom/anddoes/launcher/preference/f;->aY:Z

    if-eqz v6, :cond_5a

    .line 1970
    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v6}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v6

    if-eqz v6, :cond_5a

    .line 1971
    const/4 v6, 0x1

    .line 1979
    :goto_34
    if-nez v6, :cond_143

    .line 1982
    move/from16 v0, p3

    if-le v4, v0, :cond_133

    move/from16 v0, p4

    if-eq v0, v5, :cond_40

    if-eqz p9, :cond_133

    .line 1983
    :cond_40
    add-int/lit8 v4, v4, -0x1

    .line 1984
    const/16 p9, 0x0

    .line 1983
    goto :goto_e

    .line 1961
    :cond_45
    const/4 v7, 0x0

    :goto_46
    iget v11, v1, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v7, v11, :cond_4d

    add-int/lit8 v6, v6, 0x1

    goto :goto_16

    :cond_4d
    aget-object v11, v8, v6

    iget-object v12, v1, Lcom/android/launcher2/CellLayout;->b:[[Z

    aget-object v12, v12, v6

    aget-boolean v12, v12, v7

    aput-boolean v12, v11, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_46

    .line 1975
    :cond_5a
    const/4 v6, 0x0

    aget v7, v18, v6

    const/4 v6, 0x1

    aget v8, v18, v6

    if-ltz v7, :cond_64

    if-gez v8, :cond_66

    :cond_64
    const/4 v6, 0x0

    goto :goto_34

    :cond_66
    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->aa:Landroid/graphics/Rect;

    add-int v11, v7, v4

    add-int v12, v8, v5

    invoke-virtual {v6, v7, v8, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    if-eqz v10, :cond_86

    move-object/from16 v0, p11

    iget-object v6, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/ar;

    if-eqz v6, :cond_86

    iput v7, v6, Lcom/android/launcher2/ar;->a:I

    iput v8, v6, Lcom/android/launcher2/ar;->b:I

    :cond_86
    new-instance v11, Landroid/graphics/Rect;

    add-int v6, v7, v4

    add-int v12, v8, v5

    invoke-direct {v11, v7, v8, v6, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p11

    iget-object v6, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_a0
    :goto_a0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_b5

    iget-object v7, v1, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    iget-object v8, v1, Lcom/android/launcher2/CellLayout;->aa:Landroid/graphics/Rect;

    move-object v6, v1

    move-object/from16 v11, p11

    invoke-direct/range {v6 .. v11}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v6

    if-eqz v6, :cond_fd

    const/4 v6, 0x1

    goto :goto_34

    :cond_b5
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Landroid/view/View;

    if-eq v7, v10, :cond_a0

    move-object/from16 v0, p11

    iget-object v6, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v8, v6

    check-cast v8, Lcom/android/launcher2/ar;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v14, v8, Lcom/android/launcher2/ar;->a:I

    iget v15, v8, Lcom/android/launcher2/ar;->b:I

    iget v0, v8, Lcom/android/launcher2/ar;->a:I

    move/from16 v16, v0

    iget v0, v8, Lcom/android/launcher2/ar;->c:I

    move/from16 v17, v0

    add-int v16, v16, v17

    iget v0, v8, Lcom/android/launcher2/ar;->b:I

    move/from16 v17, v0

    iget v8, v8, Lcom/android/launcher2/ar;->d:I

    add-int v8, v8, v17

    move/from16 v0, v16

    invoke-virtual {v12, v14, v15, v0, v8}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {v11, v12}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v8

    if-eqz v8, :cond_a0

    iget-boolean v6, v6, Lcom/android/launcher2/CellLayout$LayoutParams;->i:Z

    if-nez v6, :cond_f7

    const/4 v6, 0x0

    goto/16 :goto_34

    :cond_f7
    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a0

    :cond_fd
    iget-object v12, v1, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    iget-object v13, v1, Lcom/android/launcher2/CellLayout;->aa:Landroid/graphics/Rect;

    const/4 v15, 0x0

    move-object v11, v1

    move-object v14, v9

    move-object/from16 v16, v10

    move-object/from16 v17, p11

    invoke-direct/range {v11 .. v17}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v6

    if-eqz v6, :cond_111

    const/4 v6, 0x1

    goto/16 :goto_34

    :cond_111
    iget-object v6, v1, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_117
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_120

    const/4 v6, 0x1

    goto/16 :goto_34

    :cond_120
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    iget-object v8, v1, Lcom/android/launcher2/CellLayout;->aa:Landroid/graphics/Rect;

    move-object/from16 v0, p11

    invoke-direct {v1, v6, v8, v9, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;Landroid/graphics/Rect;[ILcom/android/launcher2/at;)Z

    move-result v6

    if-nez v6, :cond_117

    const/4 v6, 0x0

    goto/16 :goto_34

    .line 1985
    :cond_133
    move/from16 v0, p4

    if-le v5, v0, :cond_13d

    .line 1986
    add-int/lit8 v5, v5, -0x1

    .line 1987
    const/16 p9, 0x1

    .line 1986
    goto/16 :goto_e

    .line 1989
    :cond_13d
    const/4 v1, 0x0

    move-object/from16 v0, p11

    iput-boolean v1, v0, Lcom/android/launcher2/at;->b:Z

    .line 1997
    :goto_142
    return-object p11

    .line 1991
    :cond_143
    const/4 v1, 0x1

    move-object/from16 v0, p11

    iput-boolean v1, v0, Lcom/android/launcher2/at;->b:Z

    .line 1992
    const/4 v1, 0x0

    aget v1, v18, v1

    move-object/from16 v0, p11

    iput v1, v0, Lcom/android/launcher2/at;->c:I

    .line 1993
    const/4 v1, 0x1

    aget v1, v18, v1

    move-object/from16 v0, p11

    iput v1, v0, Lcom/android/launcher2/at;->d:I

    .line 1994
    move-object/from16 v0, p11

    iput v4, v0, Lcom/android/launcher2/at;->e:I

    .line 1995
    move-object/from16 v0, p11

    iput v5, v0, Lcom/android/launcher2/at;->f:I

    goto :goto_142
.end method

.method private a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2311
    if-eqz p6, :cond_b

    .line 2312
    add-int v1, p1, p3

    add-int v2, p2, p4

    move-object/from16 v0, p6

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2314
    :cond_b
    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->clear()V

    .line 2315
    new-instance v3, Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-direct {v3, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2316
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2317
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v5

    .line 2318
    const/4 v1, 0x0

    move v2, v1

    :goto_24
    if-lt v2, v5, :cond_27

    .line 2330
    return-void

    .line 2319
    :cond_27
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2320
    move-object/from16 v0, p5

    if-eq v6, v0, :cond_5a

    .line 2321
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2322
    iget v7, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v8, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v9, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v10, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    add-int/2addr v9, v10

    iget v10, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v1, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    add-int/2addr v1, v10

    invoke-virtual {v4, v7, v8, v9, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 2323
    invoke-static {v3, v4}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 2324
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2325
    if-eqz p6, :cond_5a

    .line 2326
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 2318
    :cond_5a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_24
.end method

.method private a(IIII[[ZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2907
    if-ltz p1, :cond_4

    if-gez p2, :cond_5

    .line 2913
    :cond_4
    return-void

    :cond_5
    move v1, p1

    .line 2908
    :goto_6
    add-int v0, p1, p3

    if-ge v1, v0, :cond_4

    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-ge v1, v0, :cond_4

    move v0, p2

    .line 2909
    :goto_f
    add-int v2, p2, p4

    if-ge v0, v2, :cond_17

    iget v2, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v0, v2, :cond_1b

    .line 2908
    :cond_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2910
    :cond_1b
    aget-object v2, p5, v1

    aput-boolean p6, v2, v0

    .line 2909
    add-int/lit8 v0, v0, 0x1

    goto :goto_f
.end method

.method public static a(Landroid/graphics/Rect;Landroid/content/res/Resources;IIIII)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 948
    add-int/lit8 v8, p4, -0x1

    .line 949
    add-int/lit8 v9, p5, -0x1

    .line 960
    const v0, 0x7f0b003a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 961
    if-nez p6, :cond_71

    .line 962
    const v0, 0x7f0b007f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 963
    const v0, 0x7f0b0081

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 964
    const v0, 0x7f0b0083

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 965
    const v0, 0x7f0b0085

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 966
    const v0, 0x7f0b0070

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 967
    const v0, 0x7f0b0072

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 968
    const v0, 0x7f0b0074

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 969
    const v7, 0x7f0b0076

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    move v11, v0

    move v0, v5

    move v5, v3

    move v3, v1

    move v1, v6

    move v6, v4

    move v4, v2

    move v2, v11

    .line 982
    :goto_4d
    if-ltz v1, :cond_51

    if-gez v0, :cond_6d

    .line 983
    :cond_51
    sub-int v0, p2, v4

    sub-int/2addr v0, v3

    .line 984
    sub-int v1, p3, v2

    sub-int/2addr v1, v7

    .line 985
    mul-int v2, p4, v6

    sub-int/2addr v0, v2

    .line 986
    mul-int v2, p5, v5

    sub-int v2, v1, v2

    .line 987
    if-lez v8, :cond_b2

    div-int/2addr v0, v8

    :goto_61
    invoke-static {v10, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 988
    if-lez v9, :cond_b4

    div-int v0, v2, v9

    :goto_69
    invoke-static {v10, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 990
    :cond_6d
    invoke-virtual {p0, v6, v5, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 991
    return-void

    .line 972
    :cond_71
    const v0, 0x7f0b0080

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 973
    const v0, 0x7f0b0082

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 974
    const v0, 0x7f0b0084

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 975
    const v0, 0x7f0b0086

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 976
    const v0, 0x7f0b006f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 977
    const v0, 0x7f0b0071

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 978
    const v0, 0x7f0b0073

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 979
    const v7, 0x7f0b0075

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    move v11, v0

    move v0, v5

    move v5, v3

    move v3, v1

    move v1, v6

    move v6, v4

    move v4, v2

    move v2, v11

    goto :goto_4d

    .line 987
    :cond_b2
    const/4 v0, 0x0

    goto :goto_61

    .line 988
    :cond_b4
    const/4 v0, 0x0

    goto :goto_69
.end method

.method private a(Landroid/graphics/Rect;[[Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1777
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 1778
    return-void
.end method

.method private a(Landroid/view/View;[[Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 2877
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    if-eq v0, v1, :cond_b

    .line 2878
    :cond_a
    :goto_a
    return-void

    .line 2877
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    goto :goto_a
.end method

.method private a(Lcom/android/launcher2/BubbleTextView;)V
    .registers 8
    .parameter

    .prologue
    .line 356
    invoke-virtual {p1}, Lcom/android/launcher2/BubbleTextView;->getPressedOrFocusedBackgroundPadding()I

    move-result v0

    .line 357
    invoke-virtual {p1}, Lcom/android/launcher2/BubbleTextView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v1, v0

    .line 358
    invoke-virtual {p1}, Lcom/android/launcher2/BubbleTextView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v0

    .line 359
    invoke-virtual {p1}, Lcom/android/launcher2/BubbleTextView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v3, v0

    .line 360
    invoke-virtual {p1}, Lcom/android/launcher2/BubbleTextView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 357
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/android/launcher2/CellLayout;->invalidate(IIII)V

    .line 361
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/CellLayout;F)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3132
    iput p1, p0, Lcom/android/launcher2/CellLayout;->aj:F

    return-void
.end method

.method private a(Lcom/android/launcher2/at;)V
    .registers 11
    .parameter

    .prologue
    .line 2001
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v7

    .line 2002
    const/4 v0, 0x0

    move v6, v0

    :goto_8
    if-lt v6, v7, :cond_b

    .line 2013
    return-void

    .line 2003
    :cond_b
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v6}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 2004
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2006
    new-instance v0, Lcom/android/launcher2/ar;

    iget v2, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v3, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v4, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v5, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ar;-><init>(Lcom/android/launcher2/CellLayout;IIII)V

    .line 2011
    iget-object v1, p1, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2002
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_8
.end method

.method private a(Lcom/android/launcher2/at;Landroid/view/View;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2016
    move v0, v1

    :goto_3
    iget v2, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-lt v0, v2, :cond_1f

    .line 2022
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v8

    move v7, v1

    .line 2023
    :goto_e
    if-lt v7, v8, :cond_30

    .line 2036
    iget v1, p1, Lcom/android/launcher2/at;->c:I

    iget v2, p1, Lcom/android/launcher2/at;->d:I

    iget v3, p1, Lcom/android/launcher2/at;->e:I

    .line 2037
    iget v4, p1, Lcom/android/launcher2/at;->f:I

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object v0, p0

    .line 2036
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 2038
    return-void

    :cond_1f
    move v2, v1

    .line 2017
    :goto_20
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v2, v3, :cond_27

    .line 2016
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2018
    :cond_27
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    aget-object v3, v3, v0

    aput-boolean v1, v3, v2

    .line 2017
    add-int/lit8 v2, v2, 0x1

    goto :goto_20

    .line 2024
    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v7}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2025
    if-eq v1, p2, :cond_67

    .line 2026
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2027
    iget-object v2, p1, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/android/launcher2/ar;

    .line 2028
    if-eqz v4, :cond_67

    .line 2029
    iget v1, v4, Lcom/android/launcher2/ar;->a:I

    iput v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 2030
    iget v1, v4, Lcom/android/launcher2/ar;->b:I

    iput v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    .line 2031
    iget v1, v4, Lcom/android/launcher2/ar;->c:I

    iput v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 2032
    iget v1, v4, Lcom/android/launcher2/ar;->d:I

    iput v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 2033
    iget v1, v4, Lcom/android/launcher2/ar;->a:I

    iget v2, v4, Lcom/android/launcher2/ar;->b:I

    iget v3, v4, Lcom/android/launcher2/ar;->c:I

    iget v4, v4, Lcom/android/launcher2/ar;->d:I

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 2023
    :cond_67
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_e
.end method

.method private a(Lcom/android/launcher2/at;Landroid/view/View;Z)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2043
    iget-object v10, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    .line 2044
    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-lt v0, v1, :cond_22

    .line 2050
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v11

    .line 2051
    const/4 v0, 0x0

    move v9, v0

    :goto_f
    if-lt v9, v11, :cond_32

    .line 2061
    if-eqz p3, :cond_21

    .line 2062
    iget v1, p1, Lcom/android/launcher2/at;->c:I

    iget v2, p1, Lcom/android/launcher2/at;->d:I

    iget v3, p1, Lcom/android/launcher2/at;->e:I

    .line 2063
    iget v4, p1, Lcom/android/launcher2/at;->f:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, v10

    .line 2062
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 2065
    :cond_21
    return-void

    .line 2045
    :cond_22
    const/4 v1, 0x0

    :goto_23
    iget v2, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v1, v2, :cond_2a

    .line 2044
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2046
    :cond_2a
    aget-object v2, v10, v0

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    .line 2045
    add-int/lit8 v1, v1, 0x1

    goto :goto_23

    .line 2052
    :cond_32
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2053
    if-eq v1, p2, :cond_60

    .line 2054
    iget-object v0, p1, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/ar;

    .line 2055
    if-eqz v8, :cond_60

    .line 2056
    iget v2, v8, Lcom/android/launcher2/ar;->a:I

    iget v3, v8, Lcom/android/launcher2/ar;->b:I

    const/16 v4, 0x96

    const/4 v5, 0x0

    .line 2057
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    .line 2056
    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    .line 2058
    iget v1, v8, Lcom/android/launcher2/ar;->a:I

    iget v2, v8, Lcom/android/launcher2/ar;->b:I

    iget v3, v8, Lcom/android/launcher2/ar;->c:I

    iget v4, v8, Lcom/android/launcher2/ar;->d:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 2051
    :cond_60
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_f
.end method

.method private a(Ljava/util/Stack;)V
    .registers 4
    .parameter

    .prologue
    .line 1369
    :goto_0
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1372
    return-void

    .line 1370
    :cond_7
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;[ILcom/android/launcher2/at;)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1632
    iget-object v0, p4, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/android/launcher2/ar;

    .line 1633
    const/4 v10, 0x0

    .line 1634
    iget v1, v9, Lcom/android/launcher2/ar;->a:I

    iget v2, v9, Lcom/android/launcher2/ar;->b:I

    iget v3, v9, Lcom/android/launcher2/ar;->c:I

    iget v4, v9, Lcom/android/launcher2/ar;->d:I

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 1635
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    invoke-direct {p0, p2, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/graphics/Rect;[[Z)V

    .line 1637
    iget v1, v9, Lcom/android/launcher2/ar;->a:I

    iget v2, v9, Lcom/android/launcher2/ar;->b:I

    iget v3, v9, Lcom/android/launcher2/ar;->c:I

    iget v4, v9, Lcom/android/launcher2/ar;->d:I

    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/launcher2/CellLayout;->a:[I

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/android/launcher2/CellLayout;->a(IIII[I[[Z[[Z[I)[I

    .line 1639
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-ltz v0, :cond_5e

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-ltz v0, :cond_5e

    .line 1640
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, v9, Lcom/android/launcher2/ar;->a:I

    .line 1641
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v9, Lcom/android/launcher2/ar;->b:I

    .line 1642
    const/4 v0, 0x1

    move v7, v0

    .line 1645
    :goto_4e
    iget v1, v9, Lcom/android/launcher2/ar;->a:I

    iget v2, v9, Lcom/android/launcher2/ar;->b:I

    iget v3, v9, Lcom/android/launcher2/ar;->c:I

    iget v4, v9, Lcom/android/launcher2/ar;->d:I

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    .line 1646
    return v7

    :cond_5e
    move v7, v10

    goto :goto_4e
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lcom/android/launcher2/at;)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 1785
    aget v0, p3, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    aget v1, p3, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    if-le v0, v4, :cond_74

    .line 1788
    aget v8, p3, v4

    .line 1789
    aput v7, p3, v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1790
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1873
    :cond_21
    :goto_21
    return v4

    .line 1794
    :cond_22
    aput v8, p3, v4

    .line 1795
    aget v8, p3, v7

    .line 1796
    aput v7, p3, v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1797
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1802
    aput v8, p3, v7

    .line 1805
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1806
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    .line 1807
    aget v8, p3, v4

    .line 1808
    aput v7, p3, v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1809
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1814
    aput v8, p3, v4

    .line 1815
    aget v8, p3, v7

    .line 1816
    aput v7, p3, v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1817
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1822
    aput v8, p3, v7

    .line 1823
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1824
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    :goto_72
    move v4, v7

    .line 1873
    goto :goto_21

    :cond_74
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1829
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1835
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1836
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1837
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1842
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1843
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    .line 1849
    aget v0, p3, v4

    .line 1850
    aget v1, p3, v7

    aput v1, p3, v4

    .line 1851
    aput v0, p3, v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1852
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1858
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1859
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    .line 1860
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1865
    aget v0, p3, v7

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v7

    .line 1866
    aget v0, p3, v4

    mul-int/lit8 v0, v0, -0x1

    aput v0, p3, v4

    .line 1869
    aget v0, p3, v4

    .line 1870
    aget v1, p3, v7

    aput v1, p3, v4

    .line 1871
    aput v0, p3, v7

    goto :goto_72
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/Rect;[IZLandroid/view/View;Lcom/android/launcher2/at;)Z
    .registers 28
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1708
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_8

    const/4 v9, 0x1

    .line 1773
    :cond_7
    return v9

    .line 1710
    :cond_8
    const/16 v20, 0x0

    .line 1711
    const/4 v4, 0x0

    .line 1713
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_ea

    .line 1723
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 1726
    :cond_1b
    if-eqz p4, :cond_2f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[I[[ZLandroid/view/View;Lcom/android/launcher2/at;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1731
    :cond_2f
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_33
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_124

    .line 1736
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    filled-new-array {v2, v5}, [I

    move-result-object v2

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [[Z

    .line 1737
    iget v12, v4, Landroid/graphics/Rect;->top:I

    .line 1738
    iget v13, v4, Landroid/graphics/Rect;->left:I

    .line 1741
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_55
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_148

    .line 1746
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/CellLayout;->a(Landroid/graphics/Rect;[[Z)V

    .line 1748
    if-eqz p4, :cond_16c

    .line 1749
    iget v12, v4, Landroid/graphics/Rect;->left:I

    iget v13, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v14

    .line 1750
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/CellLayout;->a:[I

    move-object/from16 v19, v0

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    move-object/from16 v18, v10

    .line 1749
    invoke-direct/range {v11 .. v19}, Lcom/android/launcher2/CellLayout;->b(IIII[I[[Z[[Z[I)[I

    .line 1757
    :goto_89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    if-ltz v2, :cond_1ab

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v5, 0x1

    aget v2, v2, v5

    if-ltz v2, :cond_1ab

    .line 1758
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    iget v5, v4, Landroid/graphics/Rect;->left:I

    sub-int v5, v2, v5

    .line 1759
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v6, 0x1

    aget v2, v2, v6

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v4, v2, v4

    .line 1760
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_b5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_18f

    .line 1765
    const/4 v2, 0x1

    move v9, v2

    .line 1769
    :goto_bd
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_c1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1770
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ar;

    .line 1771
    iget v3, v2, Lcom/android/launcher2/ar;->a:I

    iget v4, v2, Lcom/android/launcher2/ar;->b:I

    iget v5, v2, Lcom/android/launcher2/ar;->c:I

    iget v6, v2, Lcom/android/launcher2/ar;->d:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    goto :goto_c1

    .line 1713
    :cond_ea
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1714
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ar;

    .line 1715
    if-nez v4, :cond_111

    .line 1716
    new-instance v4, Landroid/graphics/Rect;

    iget v5, v2, Lcom/android/launcher2/ar;->a:I

    iget v6, v2, Lcom/android/launcher2/ar;->b:I

    iget v7, v2, Lcom/android/launcher2/ar;->a:I

    iget v8, v2, Lcom/android/launcher2/ar;->c:I

    add-int/2addr v7, v8

    iget v8, v2, Lcom/android/launcher2/ar;->b:I

    iget v2, v2, Lcom/android/launcher2/ar;->d:I

    add-int/2addr v2, v8

    invoke-direct {v4, v5, v6, v7, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_f

    .line 1718
    :cond_111
    iget v5, v2, Lcom/android/launcher2/ar;->a:I

    iget v6, v2, Lcom/android/launcher2/ar;->b:I

    iget v7, v2, Lcom/android/launcher2/ar;->a:I

    iget v8, v2, Lcom/android/launcher2/ar;->c:I

    add-int/2addr v7, v8

    iget v8, v2, Lcom/android/launcher2/ar;->b:I

    iget v2, v2, Lcom/android/launcher2/ar;->d:I

    add-int/2addr v2, v8

    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/graphics/Rect;->union(IIII)V

    goto/16 :goto_f

    .line 1731
    :cond_124
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1732
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ar;

    .line 1733
    iget v6, v2, Lcom/android/launcher2/ar;->a:I

    iget v7, v2, Lcom/android/launcher2/ar;->b:I

    iget v8, v2, Lcom/android/launcher2/ar;->c:I

    iget v9, v2, Lcom/android/launcher2/ar;->d:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    const/4 v11, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    goto/16 :goto_33

    .line 1741
    :cond_148
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1742
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ar;

    .line 1743
    iget v5, v2, Lcom/android/launcher2/ar;->a:I

    sub-int v6, v5, v13

    iget v5, v2, Lcom/android/launcher2/ar;->b:I

    sub-int v7, v5, v12

    iget v8, v2, Lcom/android/launcher2/ar;->c:I

    iget v9, v2, Lcom/android/launcher2/ar;->d:I

    const/4 v11, 0x1

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    goto/16 :goto_55

    .line 1752
    :cond_16c
    iget v12, v4, Landroid/graphics/Rect;->left:I

    iget v13, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v14

    .line 1753
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/CellLayout;->a:[I

    move-object/from16 v19, v0

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    move-object/from16 v18, v10

    .line 1752
    invoke-direct/range {v11 .. v19}, Lcom/android/launcher2/CellLayout;->a(IIII[I[[Z[[Z[I)[I

    goto/16 :goto_89

    .line 1760
    :cond_18f
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1761
    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ar;

    .line 1762
    iget v7, v2, Lcom/android/launcher2/ar;->a:I

    add-int/2addr v7, v5

    iput v7, v2, Lcom/android/launcher2/ar;->a:I

    .line 1763
    iget v7, v2, Lcom/android/launcher2/ar;->b:I

    add-int/2addr v7, v4

    iput v7, v2, Lcom/android/launcher2/ar;->b:I

    goto/16 :goto_b5

    :cond_1ab
    move/from16 v9, v20

    goto/16 :goto_bd
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/Rect;[I[[ZLandroid/view/View;Lcom/android/launcher2/at;)Z
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1653
    const/4 v8, 0x0

    .line 1655
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v10

    .line 1656
    new-instance v11, Landroid/graphics/Rect;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1657
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 1659
    const/4 v2, 0x0

    .line 1660
    const/4 v1, 0x0

    .line 1661
    const/4 v3, 0x1

    aget v3, p3, v3

    if-gez v3, :cond_31

    .line 1662
    iget v1, v11, Landroid/graphics/Rect;->left:I

    iget v3, v11, Landroid/graphics/Rect;->top:I

    add-int/lit8 v3, v3, -0x1

    iget v4, v11, Landroid/graphics/Rect;->right:I

    iget v5, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v1, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1663
    const/4 v1, -0x1

    move v3, v1

    move v4, v2

    .line 1675
    :goto_2c
    const/4 v1, 0x0

    move v9, v1

    :goto_2e
    if-lt v9, v10, :cond_73

    .line 1703
    :goto_30
    return v8

    .line 1664
    :cond_31
    const/4 v3, 0x1

    aget v3, p3, v3

    if-lez v3, :cond_47

    .line 1665
    iget v1, v11, Landroid/graphics/Rect;->left:I

    iget v3, v11, Landroid/graphics/Rect;->top:I

    iget v4, v11, Landroid/graphics/Rect;->right:I

    iget v5, v11, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v11, v1, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1666
    const/4 v1, 0x1

    move v3, v1

    move v4, v2

    goto :goto_2c

    .line 1667
    :cond_47
    const/4 v3, 0x0

    aget v3, p3, v3

    if-gez v3, :cond_5d

    .line 1668
    iget v2, v11, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, -0x1

    iget v3, v11, Landroid/graphics/Rect;->top:I

    iget v4, v11, Landroid/graphics/Rect;->right:I

    iget v5, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1669
    const/4 v2, -0x1

    move v3, v1

    move v4, v2

    goto :goto_2c

    .line 1670
    :cond_5d
    const/4 v3, 0x0

    aget v3, p3, v3

    if-lez v3, :cond_11e

    .line 1671
    iget v2, v11, Landroid/graphics/Rect;->left:I

    iget v3, v11, Landroid/graphics/Rect;->top:I

    iget v4, v11, Landroid/graphics/Rect;->right:I

    add-int/lit8 v4, v4, 0x1

    iget v5, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v11, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1672
    const/4 v2, 0x1

    move v3, v1

    move v4, v2

    goto :goto_2c

    .line 1676
    :cond_73
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1, v9}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 1677
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11a

    move-object/from16 v0, p5

    if-eq v13, v0, :cond_11a

    .line 1678
    move-object/from16 v0, p6

    iget-object v1, v0, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/ar;

    .line 1680
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 1681
    iget v5, v1, Lcom/android/launcher2/ar;->a:I

    iget v6, v1, Lcom/android/launcher2/ar;->b:I

    iget v7, v1, Lcom/android/launcher2/ar;->a:I

    iget v14, v1, Lcom/android/launcher2/ar;->c:I

    add-int/2addr v7, v14

    iget v14, v1, Lcom/android/launcher2/ar;->b:I

    iget v15, v1, Lcom/android/launcher2/ar;->d:I

    add-int/2addr v14, v15

    invoke-virtual {v12, v5, v6, v7, v14}, Landroid/graphics/Rect;->set(IIII)V

    .line 1682
    invoke-static {v11, v12}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_11a

    .line 1683
    iget-boolean v2, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->i:Z

    if-nez v2, :cond_b5

    .line 1684
    const/4 v8, 0x0

    goto/16 :goto_30

    .line 1686
    :cond_b5
    const/4 v5, 0x0

    .line 1687
    iget v2, v1, Lcom/android/launcher2/ar;->a:I

    :goto_b8
    iget v6, v1, Lcom/android/launcher2/ar;->a:I

    iget v7, v1, Lcom/android/launcher2/ar;->c:I

    add-int/2addr v6, v7

    if-lt v2, v6, :cond_e0

    .line 1696
    if-eqz v5, :cond_11a

    .line 1697
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1698
    iget v2, v1, Lcom/android/launcher2/ar;->a:I

    iget v5, v1, Lcom/android/launcher2/ar;->b:I

    iget v6, v1, Lcom/android/launcher2/ar;->a:I

    iget v7, v1, Lcom/android/launcher2/ar;->c:I

    add-int/2addr v6, v7

    iget v7, v1, Lcom/android/launcher2/ar;->b:I

    iget v1, v1, Lcom/android/launcher2/ar;->d:I

    add-int/2addr v1, v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5, v6, v1}, Landroid/graphics/Rect;->union(IIII)V

    .line 1699
    const/4 v1, 0x1

    .line 1675
    :goto_da
    add-int/lit8 v2, v9, 0x1

    move v8, v1

    move v9, v2

    goto/16 :goto_2e

    .line 1688
    :cond_e0
    iget v6, v1, Lcom/android/launcher2/ar;->b:I

    :goto_e2
    iget v7, v1, Lcom/android/launcher2/ar;->b:I

    iget v14, v1, Lcom/android/launcher2/ar;->d:I

    add-int/2addr v7, v14

    if-lt v6, v7, :cond_ec

    .line 1687
    add-int/lit8 v2, v2, 0x1

    goto :goto_b8

    .line 1689
    :cond_ec
    sub-int v7, v2, v4

    if-ltz v7, :cond_118

    sub-int v7, v2, v4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/launcher2/CellLayout;->i:I

    if-ge v7, v14, :cond_118

    .line 1690
    sub-int v7, v6, v3

    if-ltz v7, :cond_118

    sub-int v7, v6, v3

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/launcher2/CellLayout;->j:I

    if-ge v7, v14, :cond_118

    const/4 v7, 0x1

    .line 1689
    :goto_105
    if-eqz v7, :cond_11c

    sub-int v7, v2, v4

    aget-object v7, p4, v7

    sub-int v14, v6, v3

    aget-boolean v7, v7, v14

    if-eqz v7, :cond_11c

    .line 1692
    const/4 v5, 0x1

    move v7, v5

    .line 1688
    :goto_113
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v7

    goto :goto_e2

    .line 1690
    :cond_118
    const/4 v7, 0x0

    goto :goto_105

    :cond_11a
    move v1, v8

    goto :goto_da

    :cond_11c
    move v7, v5

    goto :goto_113

    :cond_11e
    move v3, v1

    move v4, v2

    goto/16 :goto_2c
.end method

.method private a([IIIII[[Z)Z
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2619
    const/4 v1, 0x0

    move-object/from16 v0, p6

    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;[[Z)V

    .line 2621
    const/4 v5, 0x0

    .line 2623
    :goto_7
    const/4 v1, 0x0

    .line 2624
    if-ltz p4, :cond_13

    .line 2625
    const/4 v1, 0x0

    add-int/lit8 v2, p2, -0x1

    sub-int v2, p4, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2627
    :cond_13
    iget v2, p0, Lcom/android/launcher2/CellLayout;->i:I

    add-int/lit8 v3, p2, -0x1

    sub-int v3, v2, v3

    .line 2628
    if-ltz p4, :cond_99

    .line 2629
    add-int/lit8 v2, p2, -0x1

    add-int v4, p4, v2

    const/4 v2, 0x1

    if-ne p2, v2, :cond_5d

    const/4 v2, 0x1

    :goto_23
    add-int/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v8, v2

    .line 2631
    :goto_29
    const/4 v2, 0x0

    .line 2632
    if-ltz p5, :cond_35

    .line 2633
    const/4 v2, 0x0

    add-int/lit8 v3, p3, -0x1

    sub-int v3, p5, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2635
    :cond_35
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    add-int/lit8 v4, p3, -0x1

    sub-int v4, v3, v4

    .line 2636
    if-ltz p5, :cond_97

    .line 2637
    add-int/lit8 v3, p3, -0x1

    add-int v6, p5, v3

    const/4 v3, 0x1

    if-ne p3, v3, :cond_5f

    const/4 v3, 0x1

    :goto_45
    add-int/2addr v3, v6

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    :goto_4a
    move v7, v2

    move v2, v5

    .line 2640
    :goto_4c
    if-ge v7, v3, :cond_50

    if-eqz v2, :cond_61

    .line 2661
    :cond_50
    const/4 v1, -0x1

    if-ne p4, v1, :cond_58

    const/4 v1, -0x1

    move/from16 v0, p5

    if-eq v0, v1, :cond_90

    .line 2662
    :cond_58
    const/4 p4, -0x1

    .line 2667
    const/16 p5, -0x1

    move v5, v2

    .line 2622
    goto :goto_7

    .line 2629
    :cond_5d
    const/4 v2, 0x0

    goto :goto_23

    .line 2637
    :cond_5f
    const/4 v3, 0x0

    goto :goto_45

    :cond_61
    move v6, v1

    .line 2642
    :goto_62
    if-lt v6, v8, :cond_68

    .line 2640
    :goto_64
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_4c

    .line 2643
    :cond_68
    const/4 v4, 0x0

    move v5, v4

    :goto_6a
    if-lt v5, p2, :cond_76

    .line 2653
    if-eqz p1, :cond_74

    .line 2654
    const/4 v2, 0x0

    aput v6, p1, v2

    .line 2655
    const/4 v2, 0x1

    aput v7, p1, v2

    .line 2657
    :cond_74
    const/4 v2, 0x1

    .line 2658
    goto :goto_64

    .line 2644
    :cond_76
    const/4 v4, 0x0

    :goto_77
    if-lt v4, p3, :cond_7d

    .line 2643
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_6a

    .line 2645
    :cond_7d
    add-int v9, v6, v5

    aget-object v9, p6, v9

    add-int v10, v7, v4

    aget-boolean v9, v9, v10

    if-eqz v9, :cond_8d

    .line 2648
    add-int v4, v6, v5

    .line 2642
    add-int/lit8 v4, v4, 0x1

    move v6, v4

    goto :goto_62

    .line 2644
    :cond_8d
    add-int/lit8 v4, v4, 0x1

    goto :goto_77

    .line 2673
    :cond_90
    const/4 v1, 0x0

    move-object/from16 v0, p6

    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;[[Z)V

    .line 2674
    return v2

    :cond_97
    move v3, v4

    goto :goto_4a

    :cond_99
    move v8, v3

    goto :goto_29
.end method

.method static a([III[[Z)Z
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2838
    move v4, v1

    :goto_3
    if-lt v4, p2, :cond_7

    move v2, v1

    .line 2856
    :goto_6
    return v2

    :cond_7
    move v6, v1

    .line 2839
    :goto_8
    if-lt v6, p1, :cond_d

    .line 2838
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2840
    :cond_d
    aget-object v0, p3, v6

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_24

    move v0, v1

    :goto_14
    move v5, v6

    .line 2841
    :goto_15
    add-int/lit8 v3, v6, 0x1

    add-int/lit8 v3, v3, -0x1

    if-ge v5, v3, :cond_1d

    if-lt v6, p1, :cond_26

    .line 2848
    :cond_1d
    if-eqz v0, :cond_43

    .line 2849
    aput v6, p0, v1

    .line 2850
    aput v4, p0, v2

    goto :goto_6

    :cond_24
    move v0, v2

    .line 2840
    goto :goto_14

    :cond_26
    move v3, v4

    .line 2842
    :goto_27
    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_2f

    if-lt v4, p2, :cond_33

    .line 2841
    :cond_2f
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_15

    .line 2843
    :cond_33
    if-eqz v0, :cond_41

    aget-object v0, p3, v5

    aget-boolean v0, v0, v3

    if-nez v0, :cond_41

    move v0, v2

    .line 2844
    :goto_3c
    if-eqz v0, :cond_1d

    .line 2842
    add-int/lit8 v3, v3, 0x1

    goto :goto_27

    :cond_41
    move v0, v1

    .line 2843
    goto :goto_3c

    .line 2839
    :cond_43
    add-int/lit8 v6, v6, 0x1

    goto :goto_8
.end method

.method private a(IIIIIIZ[I[I[[Z)[I
    .registers 35
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/CellLayout;->i:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/CellLayout;->j:I

    mul-int/2addr v3, v4

    if-lt v2, v3, :cond_7e

    .line 1395
    :cond_16
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p10

    invoke-direct {v0, v2, v1}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;[[Z)V

    .line 1400
    move/from16 v0, p1

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/CellLayout;->g:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v3, v4

    add-int/lit8 v4, p5, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v12, v2

    .line 1401
    move/from16 v0, p2

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/CellLayout;->h:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v3, v4

    add-int/lit8 v4, p6, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v13, v2

    .line 1404
    if-eqz p8, :cond_8e

    .line 1405
    :goto_4a
    const-wide v8, 0x7fefffffffffffffL

    .line 1406
    new-instance v14, Landroid/graphics/Rect;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v14, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1407
    new-instance v15, Ljava/util/Stack;

    invoke-direct {v15}, Ljava/util/Stack;-><init>()V

    .line 1409
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/CellLayout;->i:I

    move/from16 v16, v0

    .line 1410
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/CellLayout;->j:I

    move/from16 v17, v0

    .line 1412
    if-lez p3, :cond_7d

    if-lez p4, :cond_7d

    if-lez p5, :cond_7d

    if-lez p6, :cond_7d

    .line 1413
    move/from16 v0, p5

    move/from16 v1, p3

    if-lt v0, v1, :cond_7d

    move/from16 v0, p6

    move/from16 v1, p4

    if-ge v0, v1, :cond_94

    .line 1511
    :cond_7d
    :goto_7d
    return-object p8

    .line 1393
    :cond_7e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    .line 1404
    :cond_8e
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 p8, v0

    goto :goto_4a

    .line 1417
    :cond_94
    const/4 v2, 0x0

    move v11, v2

    :goto_96
    add-int/lit8 v2, p4, -0x1

    sub-int v2, v17, v2

    if-lt v11, v2, :cond_bb

    .line 1503
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p10

    invoke-direct {v0, v2, v1}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;[[Z)V

    .line 1506
    const-wide v2, 0x7fefffffffffffffL

    cmpl-double v2, v8, v2

    if-nez v2, :cond_b5

    .line 1507
    const/4 v2, 0x0

    const/4 v3, -0x1

    aput v3, p8, v2

    .line 1508
    const/4 v2, 0x1

    const/4 v3, -0x1

    aput v3, p8, v2

    .line 1510
    :cond_b5
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/launcher2/CellLayout;->a(Ljava/util/Stack;)V

    goto :goto_7d

    .line 1419
    :cond_bb
    const/4 v2, 0x0

    move v10, v2

    :goto_bd
    add-int/lit8 v2, p3, -0x1

    sub-int v2, v16, v2

    if-lt v10, v2, :cond_c7

    .line 1417
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_96

    .line 1420
    :cond_c7
    const/4 v3, -0x1

    .line 1421
    const/4 v2, -0x1

    .line 1422
    if-eqz p7, :cond_225

    .line 1424
    const/4 v2, 0x0

    move v3, v2

    :goto_cd
    move/from16 v0, p3

    if-lt v3, v0, :cond_159

    .line 1437
    const/4 v3, 0x1

    .line 1438
    move/from16 v0, p3

    move/from16 v1, p5

    if-lt v0, v1, :cond_170

    const/4 v6, 0x1

    .line 1439
    :goto_d9
    move/from16 v0, p4

    move/from16 v1, p6

    if-lt v0, v1, :cond_173

    const/4 v2, 0x1

    :goto_e0
    move v5, v2

    move v7, v3

    move/from16 v4, p4

    move/from16 v3, p3

    .line 1440
    :goto_e6
    if-eqz v6, :cond_176

    if-eqz v5, :cond_176

    move v5, v4

    move v4, v3

    .line 1466
    :goto_ec
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/CellLayout;->p:[I

    .line 1471
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11, v7}, Lcom/android/launcher2/CellLayout;->a(II[I)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 1477
    add-int v3, v10, v4

    add-int v6, v11, v5

    invoke-virtual {v2, v10, v11, v3, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1478
    const/4 v6, 0x0

    .line 1479
    invoke-virtual {v15}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_10b
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1f5

    move v3, v6

    .line 1485
    :goto_112
    invoke-virtual {v15, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1486
    const/4 v6, 0x0

    aget v6, v7, v6

    sub-int/2addr v6, v12

    int-to-double v0, v6

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4000

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v18

    .line 1487
    const/4 v6, 0x1

    aget v6, v7, v6

    sub-int/2addr v6, v13

    int-to-double v6, v6

    const-wide/high16 v20, 0x4000

    move-wide/from16 v0, v20

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    .line 1486
    add-double v6, v6, v18

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 1489
    cmpg-double v18, v6, v8

    if-gtz v18, :cond_13b

    if-eqz v3, :cond_141

    .line 1490
    :cond_13b
    invoke-virtual {v2, v14}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_204

    .line 1492
    :cond_141
    const/4 v3, 0x0

    aput v10, p8, v3

    .line 1493
    const/4 v3, 0x1

    aput v11, p8, v3

    .line 1494
    if-eqz p9, :cond_14f

    .line 1495
    const/4 v3, 0x0

    aput v4, p9, v3

    .line 1496
    const/4 v3, 0x1

    aput v5, p9, v3

    .line 1498
    :cond_14f
    invoke-virtual {v14, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    move-wide v2, v6

    .line 1419
    :goto_153
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v8, v2

    goto/16 :goto_bd

    .line 1425
    :cond_159
    const/4 v2, 0x0

    .line 1427
    :goto_15a
    move/from16 v0, p4

    if-lt v2, v0, :cond_163

    .line 1424
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_cd

    .line 1426
    :cond_163
    add-int v4, v10, v3

    aget-object v4, p10, v4

    add-int v5, v11, v2

    aget-boolean v4, v4, v5

    if-nez v4, :cond_204

    .line 1427
    add-int/lit8 v2, v2, 0x1

    goto :goto_15a

    .line 1438
    :cond_170
    const/4 v6, 0x0

    goto/16 :goto_d9

    .line 1439
    :cond_173
    const/4 v2, 0x0

    goto/16 :goto_e0

    .line 1441
    :cond_176
    if-eqz v7, :cond_1c1

    if-nez v6, :cond_1c1

    .line 1442
    const/4 v2, 0x0

    move/from16 v22, v2

    move v2, v6

    move/from16 v6, v22

    :goto_180
    if-lt v6, v4, :cond_1a9

    .line 1448
    if-nez v2, :cond_21c

    .line 1449
    add-int/lit8 v3, v3, 0x1

    move v6, v4

    move v4, v2

    move/from16 v22, v3

    move v3, v5

    move/from16 v5, v22

    .line 1462
    :goto_18d
    move/from16 v0, p5

    if-lt v5, v0, :cond_1ef

    const/4 v2, 0x1

    :goto_192
    or-int/2addr v4, v2

    .line 1463
    move/from16 v0, p6

    if-lt v6, v0, :cond_1f1

    const/4 v2, 0x1

    :goto_198
    or-int/2addr v3, v2

    .line 1464
    if-eqz v7, :cond_1f3

    const/4 v2, 0x0

    :goto_19c
    move v7, v2

    move/from16 v22, v4

    move v4, v6

    move/from16 v6, v22

    move/from16 v23, v5

    move v5, v3

    move/from16 v3, v23

    goto/16 :goto_e6

    .line 1443
    :cond_1a9
    add-int v18, v10, v3

    add-int/lit8 v19, v16, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_1bd

    add-int v18, v10, v3

    aget-object v18, p10, v18

    add-int v19, v11, v6

    aget-boolean v18, v18, v19

    if-eqz v18, :cond_1be

    .line 1445
    :cond_1bd
    const/4 v2, 0x1

    .line 1442
    :cond_1be
    add-int/lit8 v6, v6, 0x1

    goto :goto_180

    .line 1451
    :cond_1c1
    if-nez v5, :cond_210

    .line 1452
    const/4 v2, 0x0

    move/from16 v22, v2

    move v2, v5

    move/from16 v5, v22

    :goto_1c9
    if-lt v5, v3, :cond_1d7

    .line 1458
    if-nez v2, :cond_207

    .line 1459
    add-int/lit8 v4, v4, 0x1

    move v5, v3

    move v3, v2

    move/from16 v22, v6

    move v6, v4

    move/from16 v4, v22

    goto :goto_18d

    .line 1453
    :cond_1d7
    add-int v18, v11, v4

    add-int/lit8 v19, v17, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_1eb

    add-int v18, v10, v5

    aget-object v18, p10, v18

    add-int v19, v11, v4

    aget-boolean v18, v18, v19

    if-eqz v18, :cond_1ec

    .line 1455
    :cond_1eb
    const/4 v2, 0x1

    .line 1452
    :cond_1ec
    add-int/lit8 v5, v5, 0x1

    goto :goto_1c9

    .line 1462
    :cond_1ef
    const/4 v2, 0x0

    goto :goto_192

    .line 1463
    :cond_1f1
    const/4 v2, 0x0

    goto :goto_198

    .line 1464
    :cond_1f3
    const/4 v2, 0x1

    goto :goto_19c

    .line 1479
    :cond_1f5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 1480
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_10b

    .line 1481
    const/4 v3, 0x1

    .line 1482
    goto/16 :goto_112

    :cond_204
    move-wide v2, v8

    goto/16 :goto_153

    :cond_207
    move v5, v3

    move v3, v2

    move/from16 v22, v6

    move v6, v4

    move/from16 v4, v22

    goto/16 :goto_18d

    :cond_210
    move/from16 v22, v5

    move v5, v3

    move/from16 v3, v22

    move/from16 v23, v6

    move v6, v4

    move/from16 v4, v23

    goto/16 :goto_18d

    :cond_21c
    move v6, v4

    move v4, v2

    move/from16 v22, v3

    move v3, v5

    move/from16 v5, v22

    goto/16 :goto_18d

    :cond_225
    move v4, v2

    move v5, v3

    goto/16 :goto_ec
.end method

.method private a(IIIIZ[I)[I
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1355
    .line 1356
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    .line 1355
    invoke-direct/range {v0 .. v10}, Lcom/android/launcher2/CellLayout;->a(IIIIIIZ[I[I[[Z)[I

    move-result-object v0

    return-object v0
.end method

.method private a(IIII[I[[Z[[Z[I)[I
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1538
    if-eqz p8, :cond_23

    .line 1539
    :goto_2
    const v3, 0x7f7fffff

    .line 1540
    const/high16 v2, -0x8000

    .line 1542
    iget v7, p0, Lcom/android/launcher2/CellLayout;->i:I

    .line 1543
    iget v8, p0, Lcom/android/launcher2/CellLayout;->j:I

    .line 1545
    const/4 v1, 0x0

    move v6, v1

    :goto_d
    add-int/lit8 v1, p4, -0x1

    sub-int v1, v8, v1

    if-lt v6, v1, :cond_29

    .line 1580
    const v1, 0x7f7fffff

    cmpl-float v1, v3, v1

    if-nez v1, :cond_22

    .line 1581
    const/4 v1, 0x0

    const/4 v2, -0x1

    aput v2, p8, v1

    .line 1582
    const/4 v1, 0x1

    const/4 v2, -0x1

    aput v2, p8, v1

    .line 1584
    :cond_22
    return-object p8

    .line 1538
    :cond_23
    const/4 v1, 0x2

    new-array v0, v1, [I

    move-object/from16 p8, v0

    goto :goto_2

    .line 1547
    :cond_29
    const/4 v1, 0x0

    move v5, v1

    move v1, v2

    :goto_2c
    add-int/lit8 v2, p3, -0x1

    sub-int v2, v7, v2

    if-lt v5, v2, :cond_37

    .line 1545
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v1

    goto :goto_d

    .line 1549
    :cond_37
    const/4 v2, 0x0

    move v4, v2

    :goto_39
    if-lt v4, p3, :cond_81

    .line 1558
    sub-int v2, v5, p1

    sub-int v4, v5, p1

    mul-int/2addr v2, v4

    sub-int v4, v6, p2

    sub-int v9, v6, p2

    mul-int/2addr v4, v9

    add-int/2addr v2, v4

    int-to-double v9, v2

    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v9

    double-to-float v4, v9

    .line 1559
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->q:[I

    .line 1560
    sub-int v9, v5, p1

    int-to-float v9, v9

    sub-int v10, v6, p2

    int-to-float v10, v10

    invoke-static {v9, v10, v2}, Lcom/android/launcher2/CellLayout;->b(FF[I)V

    .line 1563
    const/4 v9, 0x0

    aget v9, p5, v9

    const/4 v10, 0x0

    aget v10, v2, v10

    mul-int/2addr v9, v10

    .line 1564
    const/4 v10, 0x1

    aget v10, p5, v10

    const/4 v11, 0x1

    aget v2, v2, v11

    mul-int/2addr v2, v10

    .line 1563
    add-int/2addr v2, v9

    .line 1565
    invoke-static {v4, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v9

    if-ltz v9, :cond_74

    .line 1569
    invoke-static {v4, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v9

    if-nez v9, :cond_9f

    .line 1570
    if-le v2, v1, :cond_9f

    .line 1573
    :cond_74
    const/4 v1, 0x0

    aput v5, p8, v1

    .line 1574
    const/4 v1, 0x1

    aput v6, p8, v1

    move v1, v2

    move v2, v4

    .line 1547
    :goto_7c
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_2c

    .line 1550
    :cond_81
    const/4 v2, 0x0

    .line 1552
    :goto_82
    move/from16 v0, p4

    if-lt v2, v0, :cond_8a

    .line 1549
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_39

    .line 1551
    :cond_8a
    add-int v9, v5, v4

    aget-object v9, p6, v9

    add-int v10, v6, v2

    aget-boolean v9, v9, v10

    if-eqz v9, :cond_9c

    if-eqz p7, :cond_9f

    aget-object v9, p7, v4

    aget-boolean v9, v9, v2

    if-nez v9, :cond_9f

    .line 1552
    :cond_9c
    add-int/lit8 v2, v2, 0x1

    goto :goto_82

    :cond_9f
    move v2, v3

    goto :goto_7c
.end method

.method public static a(Landroid/content/res/Resources;II)[I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2775
    const v0, 0x7f0b0096

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2776
    const v1, 0x7f0b0097

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2777
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2780
    int-to-float v1, p1

    int-to-float v2, v0

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    .line 2781
    int-to-float v2, p2

    int-to-float v0, v0

    div-float v0, v2, v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 2783
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v1, 0x1

    aput v0, v2, v1

    return-object v2
.end method

.method static synthetic a(Lcom/android/launcher2/CellLayout;)[I
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->q:[I

    return-object v0
.end method

.method static synthetic b(Lcom/android/launcher2/CellLayout;)F
    .registers 2
    .parameter

    .prologue
    .line 156
    iget v0, p0, Lcom/android/launcher2/CellLayout;->W:F

    return v0
.end method

.method static b(Landroid/content/res/Resources;I)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 335
    const v0, 0x7f0b0097

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 336
    const v1, 0x7f0b0098

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 337
    const v2, 0x7f0b0099

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 336
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 339
    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v1, v2

    mul-int/2addr v0, p1

    add-int/2addr v0, v1

    return v0
.end method

.method private static b(FF[I)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const-wide/high16 v5, 0x3fe0

    const/4 v4, 0x0

    .line 1935
    div-float v0, p1, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    .line 1937
    aput v4, p2, v4

    .line 1938
    aput v4, p2, v7

    .line 1939
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v2, v2, v5

    if-lez v2, :cond_22

    .line 1940
    invoke-static {p0}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-int v2, v2

    aput v2, p2, v4

    .line 1942
    :cond_22
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpl-double v0, v0, v5

    if-lez v0, :cond_35

    .line 1943
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    aput v0, p2, v7

    .line 1945
    :cond_35
    return-void
.end method

.method private b(IIIILandroid/graphics/Rect;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 903
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 904
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v1

    .line 905
    iget v2, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p1

    add-int/2addr v0, v2

    .line 906
    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    .line 907
    iget v2, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v2, p3

    add-int/lit8 v3, p3, -0x1

    iget v4, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v2, v0

    .line 908
    iget v3, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v3, p4

    add-int/lit8 v4, p4, -0x1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    add-int/2addr v3, v1

    .line 907
    invoke-virtual {p5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 909
    return-void
.end method

.method static b(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2726
    if-eqz p0, :cond_e

    .line 2727
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2728
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->l:Z

    .line 2729
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 2731
    :cond_e
    return-void
.end method

.method private b(Landroid/view/View;[[Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 2900
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    if-eq v0, v1, :cond_b

    .line 2903
    :cond_a
    :goto_a
    return-void

    .line 2901
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2902
    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIII[[ZZ)V

    goto :goto_a
.end method

.method private b(Lcom/android/launcher2/at;Landroid/view/View;)V
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    .line 2069
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v10

    .line 2070
    const/4 v0, 0x0

    move v9, v0

    :goto_a
    if-lt v9, v10, :cond_d

    .line 2081
    return-void

    .line 2071
    :cond_d
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2072
    if-eq v2, p2, :cond_72

    .line 2073
    iget-object v0, p1, Lcom/android/launcher2/at;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/ar;

    .line 2074
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2075
    if-eqz v1, :cond_72

    .line 2076
    new-instance v0, Lcom/android/launcher2/au;

    iget v3, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v4, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 2077
    iget v5, v1, Lcom/android/launcher2/ar;->a:I

    iget v6, v1, Lcom/android/launcher2/ar;->b:I

    iget v7, v1, Lcom/android/launcher2/ar;->c:I

    iget v8, v1, Lcom/android/launcher2/ar;->d:I

    move-object v1, p0

    .line 2076
    invoke-direct/range {v0 .. v8}, Lcom/android/launcher2/au;-><init>(Lcom/android/launcher2/CellLayout;Landroid/view/View;IIIIII)V

    .line 2078
    iget-object v1, v0, Lcom/android/launcher2/au;->i:Lcom/android/launcher2/CellLayout;

    iget-object v1, v1, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_76

    iget-object v1, v0, Lcom/android/launcher2/au;->i:Lcom/android/launcher2/CellLayout;

    iget-object v1, v1, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/au;

    iget-object v2, v1, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    if-eqz v2, :cond_5a

    iget-object v1, v1, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    :cond_5a
    iget-object v1, v0, Lcom/android/launcher2/au;->i:Lcom/android/launcher2/CellLayout;

    iget-object v1, v1, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v0, Lcom/android/launcher2/au;->b:F

    cmpl-float v1, v1, v11

    if-nez v1, :cond_76

    iget v1, v0, Lcom/android/launcher2/au;->c:F

    cmpl-float v1, v1, v11

    if-nez v1, :cond_76

    invoke-virtual {v0}, Lcom/android/launcher2/au;->a()V

    .line 2070
    :cond_72
    :goto_72
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_a

    .line 2078
    :cond_76
    iget v1, v0, Lcom/android/launcher2/au;->b:F

    cmpl-float v1, v1, v11

    if-nez v1, :cond_82

    iget v1, v0, Lcom/android/launcher2/au;->c:F

    cmpl-float v1, v1, v11

    if-eqz v1, :cond_72

    :cond_82
    new-array v1, v12, [F

    fill-array-data v1, :array_c2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, v0, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    invoke-virtual {v1, v12}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x404e

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v2, Lcom/android/launcher2/av;

    invoke-direct {v2, v0}, Lcom/android/launcher2/av;-><init>(Lcom/android/launcher2/au;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Lcom/android/launcher2/aw;

    invoke-direct {v2, v0}, Lcom/android/launcher2/aw;-><init>(Lcom/android/launcher2/au;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v2, v0, Lcom/android/launcher2/au;->i:Lcom/android/launcher2/CellLayout;

    iget-object v2, v2, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_72

    :array_c2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private b(IIIIII[I[I)[I
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2537
    const/4 v7, 0x1

    .line 2538
    iget-object v10, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 2537
    invoke-direct/range {v0 .. v10}, Lcom/android/launcher2/CellLayout;->a(IIIIIIZ[I[I[[Z)[I

    move-result-object v0

    return-object v0
.end method

.method private b(IIII[I[[Z[[Z[I)[I
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1591
    if-eqz p8, :cond_22

    .line 1592
    :goto_2
    const/4 v1, 0x0

    const/4 v2, -0x1

    aput v2, p8, v1

    .line 1593
    const/4 v1, 0x1

    const/4 v2, -0x1

    aput v2, p8, v1

    .line 1594
    const v3, 0x7f7fffff

    .line 1597
    const/4 v1, 0x0

    aget v1, p5, v1

    if-eqz v1, :cond_17

    const/4 v1, 0x1

    aget v1, p5, v1

    if-nez v1, :cond_21

    .line 1598
    :cond_17
    const/4 v1, 0x0

    aget v1, p5, v1

    if-nez v1, :cond_28

    const/4 v1, 0x1

    aget v1, p5, v1

    if-nez v1, :cond_28

    .line 1627
    :cond_21
    return-object p8

    .line 1591
    :cond_22
    const/4 v1, 0x2

    new-array v0, v1, [I

    move-object/from16 p8, v0

    goto :goto_2

    .line 1603
    :cond_28
    const/4 v1, 0x0

    aget v1, p5, v1

    add-int v2, p1, v1

    .line 1604
    const/4 v1, 0x1

    aget v1, p5, v1

    add-int/2addr v1, p2

    move v5, v1

    move v6, v2

    move v2, v3

    .line 1605
    :goto_34
    if-ltz v6, :cond_21

    add-int v1, v6, p3

    iget v3, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-gt v1, v3, :cond_21

    if-ltz v5, :cond_21

    add-int v1, v5, p4

    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-gt v1, v3, :cond_21

    .line 1607
    const/4 v3, 0x0

    .line 1608
    const/4 v1, 0x0

    move v4, v1

    :goto_47
    if-lt v4, p3, :cond_75

    .line 1615
    if-nez v3, :cond_96

    .line 1617
    sub-int v1, v6, p1

    sub-int v3, v6, p1

    mul-int/2addr v1, v3

    sub-int v3, v5, p2

    sub-int v4, v5, p2

    mul-int/2addr v3, v4

    add-int/2addr v1, v3

    int-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v1, v3

    .line 1618
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-gez v3, :cond_96

    .line 1620
    const/4 v2, 0x0

    aput v6, p8, v2

    .line 1621
    const/4 v2, 0x1

    aput v5, p8, v2

    .line 1624
    :goto_68
    const/4 v2, 0x0

    aget v2, p5, v2

    add-int v3, v6, v2

    .line 1625
    const/4 v2, 0x1

    aget v2, p5, v2

    add-int/2addr v2, v5

    move v5, v2

    move v6, v3

    move v2, v1

    goto :goto_34

    .line 1609
    :cond_75
    const/4 v1, 0x0

    move v9, v1

    move v1, v3

    move v3, v9

    :goto_79
    if-lt v3, p4, :cond_80

    .line 1608
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    goto :goto_47

    .line 1610
    :cond_80
    add-int v7, v6, v4

    aget-object v7, p6, v7

    add-int v8, v5, v3

    aget-boolean v7, v7, v8

    if-eqz v7, :cond_93

    if-eqz p7, :cond_92

    aget-object v7, p7, v4

    aget-boolean v7, v7, v3

    if-eqz v7, :cond_93

    .line 1611
    :cond_92
    const/4 v1, 0x1

    .line 1609
    :cond_93
    add-int/lit8 v3, v3, 0x1

    goto :goto_79

    :cond_96
    move v1, v2

    goto :goto_68
.end method

.method private c(II[I)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 859
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 860
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v1

    .line 862
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v0, v3

    aput v0, p3, v2

    .line 863
    const/4 v0, 0x1

    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    aput v1, p3, v0

    .line 864
    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/CellLayout;)[F
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->J:[F

    return-object v0
.end method

.method static synthetic d(Lcom/android/launcher2/CellLayout;)[Landroid/graphics/Rect;
    .registers 2
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic e(Lcom/android/launcher2/CellLayout;)Ljava/util/HashMap;
    .registers 2
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    return-object v0
.end method

.method private e(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 3216
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    add-int/lit8 v0, v0, -0x1

    .line 3217
    iget v2, p0, Lcom/android/launcher2/CellLayout;->j:I

    add-int/lit8 v2, v2, -0x1

    .line 3219
    iget v3, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    sub-int v3, p1, v3

    iget v4, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    sub-int/2addr v3, v4

    .line 3220
    iget v4, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    sub-int v4, p2, v4

    iget v5, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    sub-int/2addr v4, v5

    .line 3222
    iget v5, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v6, p0, Lcom/android/launcher2/CellLayout;->ag:I

    mul-int/2addr v5, v6

    sub-int v5, v3, v5

    .line 3223
    iget v6, p0, Lcom/android/launcher2/CellLayout;->j:I

    iget v7, p0, Lcom/android/launcher2/CellLayout;->ah:I

    mul-int/2addr v6, v7

    sub-int v6, v4, v6

    .line 3226
    iget v7, p0, Lcom/android/launcher2/CellLayout;->i:I

    div-int v7, v3, v7

    iget v8, p0, Lcom/android/launcher2/CellLayout;->ag:I

    if-le v7, v8, :cond_59

    .line 3227
    if-lez v0, :cond_57

    div-int v0, v5, v0

    :goto_31
    iput v0, p0, Lcom/android/launcher2/CellLayout;->k:I

    .line 3228
    iget v0, p0, Lcom/android/launcher2/CellLayout;->ag:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    .line 3233
    :goto_37
    iget v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    div-int v0, v4, v0

    iget v3, p0, Lcom/android/launcher2/CellLayout;->ah:I

    if-le v0, v3, :cond_62

    .line 3234
    if-lez v2, :cond_43

    div-int v1, v6, v2

    :cond_43
    iput v1, p0, Lcom/android/launcher2/CellLayout;->l:I

    .line 3235
    iget v0, p0, Lcom/android/launcher2/CellLayout;->ah:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->h:I

    .line 3240
    :goto_49
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    iget v1, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->k:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->l:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/ja;->a(IIII)V

    .line 3274
    return-void

    :cond_57
    move v0, v1

    .line 3227
    goto :goto_31

    .line 3230
    :cond_59
    iput v1, p0, Lcom/android/launcher2/CellLayout;->k:I

    .line 3231
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    div-int v0, v3, v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    goto :goto_37

    .line 3237
    :cond_62
    iput v1, p0, Lcom/android/launcher2/CellLayout;->l:I

    .line 3238
    iget v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    div-int v0, v4, v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->h:I

    goto :goto_49
.end method

.method private l()V
    .registers 3

    .prologue
    .line 2202
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 2205
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->P:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2206
    return-void

    .line 2202
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/au;

    .line 2203
    invoke-virtual {v0}, Lcom/android/launcher2/au;->a()V

    goto :goto_a
.end method

.method private m()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 2209
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-lt v0, v2, :cond_19

    .line 2214
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v3

    move v2, v1

    .line 2215
    :goto_d
    if-lt v2, v3, :cond_30

    .line 2228
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Workspace;->b(Lcom/android/launcher2/CellLayout;)V

    .line 2229
    return-void

    :cond_19
    move v2, v1

    .line 2210
    :goto_1a
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v2, v3, :cond_21

    .line 2209
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2211
    :cond_21
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    aput-boolean v4, v3, v2

    .line 2210
    add-int/lit8 v2, v2, 0x1

    goto :goto_1a

    .line 2216
    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2217
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2218
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    .line 2221
    if-eqz v1, :cond_58

    .line 2222
    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    iput v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iput v4, v1, Lcom/android/launcher2/di;->l:I

    .line 2223
    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    iput v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iput v4, v1, Lcom/android/launcher2/di;->m:I

    .line 2224
    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iput v4, v1, Lcom/android/launcher2/di;->n:I

    .line 2225
    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    iput v0, v1, Lcom/android/launcher2/di;->o:I

    .line 2215
    :cond_58
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_d
.end method

.method private n()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 2860
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-lt v0, v2, :cond_7

    .line 2865
    return-void

    :cond_7
    move v2, v1

    .line 2861
    :goto_8
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-lt v2, v3, :cond_f

    .line 2860
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2862
    :cond_f
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    aget-object v3, v3, v0

    aput-boolean v1, v3, v2

    .line 2861
    add-int/lit8 v2, v2, 0x1

    goto :goto_8
.end method


# virtual methods
.method public final a(FF[I)F
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x4000

    .line 912
    aget v0, p3, v3

    aget v1, p3, v6

    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->q:[I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/CellLayout;->a(II[I)V

    .line 913
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->q:[I

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 914
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->q:[I

    aget v2, v2, v6

    int-to-float v2, v2

    sub-float v2, p2, v2

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 913
    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 915
    return v0
.end method

.method public final a(I)I
    .registers 5
    .parameter

    .prologue
    .line 3288
    iget v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    .line 3289
    add-int/lit8 v1, p1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v1, v2

    .line 3288
    add-int/2addr v0, v1

    return v0
.end method

.method public final a()V
    .registers 4

    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/ja;->setLayerType(ILandroid/graphics/Paint;)V

    .line 344
    return-void
.end method

.method final a(FZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 364
    if-eqz p2, :cond_20

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->B:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_20

    .line 365
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->B:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    .line 370
    :cond_c
    :goto_c
    const/high16 v0, 0x437f

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->v:I

    .line 371
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/launcher2/CellLayout;->v:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 372
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 373
    return-void

    .line 366
    :cond_20
    if-nez p2, :cond_c

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->C:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_c

    .line 367
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->C:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    goto :goto_c
.end method

.method public final a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 347
    iput p1, p0, Lcom/android/launcher2/CellLayout;->i:I

    .line 348
    iput p2, p0, Lcom/android/launcher2/CellLayout;->j:I

    .line 349
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->j:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    .line 350
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->j:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    .line 351
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->ae:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 352
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->requestLayout()V

    .line 353
    return-void
.end method

.method public final a(IIIILandroid/graphics/Rect;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2743
    iget v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    .line 2744
    iget v1, p0, Lcom/android/launcher2/CellLayout;->h:I

    .line 2745
    iget v2, p0, Lcom/android/launcher2/CellLayout;->k:I

    .line 2746
    iget v3, p0, Lcom/android/launcher2/CellLayout;->l:I

    .line 2748
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v4

    .line 2749
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v5

    .line 2751
    mul-int v6, p3, v0

    add-int/lit8 v7, p3, -0x1

    mul-int/2addr v7, v2

    add-int/2addr v6, v7

    .line 2752
    mul-int v7, p4, v1

    add-int/lit8 v8, p4, -0x1

    mul-int/2addr v8, v3

    add-int/2addr v7, v8

    .line 2754
    add-int/2addr v0, v2

    mul-int/2addr v0, p1

    add-int/2addr v0, v4

    .line 2755
    add-int/2addr v1, v3

    mul-int/2addr v1, p2

    add-int/2addr v1, v5

    .line 2757
    add-int v2, v0, v6

    add-int v3, v1, v7

    invoke-virtual {p5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 2758
    return-void
.end method

.method final a(IIII[I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 887
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 888
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v1

    .line 889
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v0, v3

    .line 890
    iget v3, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v3, p3

    add-int/lit8 v4, p3, -0x1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 889
    aput v0, p5, v2

    .line 891
    const/4 v0, 0x1

    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    .line 892
    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v2, p4

    add-int/lit8 v3, p4, -0x1

    iget v4, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 891
    aput v1, p5, v0

    .line 893
    return-void
.end method

.method final a(II[I)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 875
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(IIII[I)V

    .line 876
    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .registers 2
    .parameter

    .prologue
    .line 605
    invoke-virtual {p0, p1}, Lcom/android/launcher2/CellLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 606
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 2259
    invoke-virtual {p0, p1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 2260
    return-void
.end method

.method final a(Landroid/view/View;Landroid/graphics/Bitmap;IIIIIIZLandroid/graphics/Point;Landroid/graphics/Rect;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1228
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 1229
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 1231
    if-eqz p1, :cond_26

    if-nez p10, :cond_26

    .line 1232
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->H:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, p3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, p4

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 1237
    :goto_21
    if-nez p2, :cond_2c

    if-nez p1, :cond_2c

    .line 1292
    :cond_25
    :goto_25
    return-void

    .line 1234
    :cond_26
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->H:Landroid/graphics/Point;

    invoke-virtual {v3, p3, p4}, Landroid/graphics/Point;->set(II)V

    goto :goto_21

    .line 1241
    :cond_2c
    if-ne p5, v1, :cond_30

    if-eq p6, v2, :cond_25

    .line 1242
    :cond_30
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v2, 0x0

    aput p5, v1, v2

    .line 1243
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v2, 0x1

    aput p6, v1, v2

    .line 1245
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->q:[I

    .line 1246
    invoke-direct {p0, p5, p6, v1}, Lcom/android/launcher2/CellLayout;->c(II[I)V

    .line 1248
    const/4 v2, 0x0

    aget v2, v1, v2

    .line 1249
    const/4 v3, 0x1

    aget v3, v1, v3

    .line 1251
    if-eqz p1, :cond_b5

    if-nez p10, :cond_b5

    .line 1254
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1255
    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v2

    .line 1256
    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v3

    .line 1261
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    .line 1263
    iget v1, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v1, p7

    add-int/lit8 v3, p7, -0x1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v3, v5

    add-int/2addr v1, v3

    .line 1264
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1263
    sub-int/2addr v1, v3

    .line 1264
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    .line 1280
    :goto_72
    iget v3, p0, Lcom/android/launcher2/CellLayout;->L:I

    .line 1281
    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    aget-object v4, v4, v3

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/android/launcher2/dg;->a(I)V

    .line 1282
    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    array-length v4, v4

    rem-int/2addr v3, v4

    iput v3, p0, Lcom/android/launcher2/CellLayout;->L:I

    .line 1283
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    iget v4, p0, Lcom/android/launcher2/CellLayout;->L:I

    aget-object v6, v3, v4

    .line 1284
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v6, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1285
    if-eqz p9, :cond_a1

    move-object v1, p0

    move v2, p5

    move v3, p6

    move v4, p7

    move v5, p8

    .line 1286
    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/graphics/Rect;)V

    .line 1289
    :cond_a1
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    iget v2, p0, Lcom/android/launcher2/CellLayout;->L:I

    aget-object v1, v1, v2

    iput-object p2, v1, Lcom/android/launcher2/dg;->b:Ljava/lang/Object;

    .line 1290
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    iget v2, p0, Lcom/android/launcher2/CellLayout;->L:I

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/launcher2/dg;->a(I)V

    goto/16 :goto_25

    .line 1266
    :cond_b5
    if-eqz p10, :cond_d5

    if-eqz p11, :cond_d5

    .line 1269
    move-object/from16 v0, p10

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v4, p7

    add-int/lit8 v5, p7, -0x1

    iget v6, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 1270
    invoke-virtual/range {p11 .. p11}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    add-int/2addr v1, v2

    .line 1271
    move-object/from16 v0, p10

    iget v2, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    goto :goto_72

    .line 1274
    :cond_d5
    iget v1, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v1, p7

    add-int/lit8 v4, p7, -0x1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 1275
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 1274
    sub-int/2addr v1, v4

    .line 1275
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 1276
    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v2, p8

    add-int/lit8 v4, p8, -0x1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 1277
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1276
    sub-int/2addr v2, v4

    .line 1277
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    goto/16 :goto_72
.end method

.method public final a(Lcom/android/launcher2/co;)V
    .registers 3
    .parameter

    .prologue
    .line 577
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    return-void
.end method

.method public final a(Lcom/android/launcher2/di;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 2805
    instance-of v0, p1, Lcom/android/launcher2/fz;

    if-eqz v0, :cond_21

    move-object v0, p1

    .line 2806
    check-cast v0, Lcom/android/launcher2/fz;

    iget v1, v0, Lcom/android/launcher2/fz;->c:I

    move-object v0, p1

    .line 2807
    check-cast v0, Lcom/android/launcher2/fz;

    iget v0, v0, Lcom/android/launcher2/fz;->d:I

    .line 2816
    :goto_f
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/content/res/Resources;II)[I

    move-result-object v0

    .line 2817
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p1, Lcom/android/launcher2/di;->n:I

    .line 2818
    aget v0, v0, v3

    iput v0, p1, Lcom/android/launcher2/di;->o:I

    .line 2819
    :goto_20
    return-void

    .line 2808
    :cond_21
    instance-of v0, p1, Lcom/android/launcher2/iw;

    if-eqz v0, :cond_30

    move-object v0, p1

    .line 2809
    check-cast v0, Lcom/android/launcher2/iw;

    iget v1, v0, Lcom/android/launcher2/iw;->b:I

    move-object v0, p1

    .line 2810
    check-cast v0, Lcom/android/launcher2/iw;

    iget v0, v0, Lcom/android/launcher2/iw;->c:I

    goto :goto_f

    .line 2813
    :cond_30
    iput v3, p1, Lcom/android/launcher2/di;->o:I

    iput v3, p1, Lcom/android/launcher2/di;->n:I

    goto :goto_20
.end method

.method final a(IIIILandroid/view/View;[I)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2334
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    move-result-object v0

    .line 2335
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v6, 0x0

    .line 2336
    iget-object v7, p0, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    move-object v0, p0

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 2335
    invoke-direct/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V

    .line 2337
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x0

    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x1

    goto :goto_23
.end method

.method final a(IIIILandroid/view/View;[IZ)Z
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2359
    const/4 v2, 0x2

    new-array v7, v2, [I

    move-object v2, p0

    move v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    .line 2360
    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher2/CellLayout;->a(IIII[I)V

    .line 2363
    const/4 v2, 0x0

    aget v3, v7, v2

    const/4 v2, 0x1

    aget v4, v7, v2

    .line 2364
    const/4 v11, 0x1

    const/4 v12, 0x1

    new-instance v13, Lcom/android/launcher2/at;

    const/4 v2, 0x0

    invoke-direct {v13, p0, v2}, Lcom/android/launcher2/at;-><init>(Lcom/android/launcher2/CellLayout;B)V

    move-object v2, p0

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    .line 2363
    invoke-direct/range {v2 .. v13}, Lcom/android/launcher2/CellLayout;->a(IIIIII[ILandroid/view/View;ZZLcom/android/launcher2/at;)Lcom/android/launcher2/at;

    move-result-object v2

    .line 2366
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/android/launcher2/CellLayout;->setUseTempCoords(Z)V

    .line 2367
    if-eqz v2, :cond_58

    iget-boolean v3, v2, Lcom/android/launcher2/at;->b:Z

    if-eqz v3, :cond_58

    .line 2371
    move-object/from16 v0, p5

    invoke-direct {p0, v2, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;Landroid/view/View;)V

    .line 2372
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/android/launcher2/CellLayout;->setItemPlacementDirty(Z)V

    .line 2373
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-direct {p0, v2, v0, v1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;Landroid/view/View;Z)V

    .line 2375
    if-eqz p7, :cond_5b

    .line 2376
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->m()V

    .line 2377
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->l()V

    .line 2378
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/launcher2/CellLayout;->setItemPlacementDirty(Z)V

    .line 2383
    :goto_53
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v3}, Lcom/android/launcher2/ja;->requestLayout()V

    .line 2385
    :cond_58
    iget-boolean v2, v2, Lcom/android/launcher2/at;->b:Z

    return v2

    .line 2381
    :cond_5b
    move-object/from16 v0, p5

    invoke-direct {p0, v2, v0}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/at;Landroid/view/View;)V

    goto :goto_53
.end method

.method public final a(Landroid/view/View;IIIIZZ)Z
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1114
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v6

    .line 1115
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    .line 1116
    if-nez p6, :cond_a3

    .line 1117
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->c:[[Z

    move-object v4, v0

    .line 1120
    :goto_b
    invoke-virtual {v6, p1}, Lcom/android/launcher2/ja;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a1

    .line 1121
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 1122
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 1125
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1126
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    .line 1127
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    :cond_36
    iget v3, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 1131
    iget v5, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 1132
    if-eqz p7, :cond_4a

    .line 1133
    iget v1, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    aget-object v1, v4, v1

    iget v7, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    const/4 v8, 0x0

    aput-boolean v8, v1, v7

    .line 1134
    aget-object v1, v4, p2

    const/4 v4, 0x1

    aput-boolean v4, v1, p3

    .line 1136
    :cond_4a
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 1137
    if-eqz p6, :cond_6e

    .line 1138
    iput p2, v0, Lcom/android/launcher2/di;->l:I

    iput p2, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    .line 1139
    iput p3, v0, Lcom/android/launcher2/di;->m:I

    iput p3, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 1144
    :goto_57
    invoke-virtual {v6, v2}, Lcom/android/launcher2/ja;->setupLp(Lcom/android/launcher2/CellLayout$LayoutParams;)V

    .line 1145
    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 1146
    iget v4, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 1147
    iget v6, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 1149
    iput v3, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 1150
    iput v5, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 1153
    if-ne v3, v4, :cond_73

    if-ne v5, v6, :cond_73

    .line 1154
    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 1155
    const/4 v0, 0x1

    .line 1193
    :goto_6d
    return v0

    .line 1141
    :cond_6e
    iput p2, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 1142
    iput p3, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    goto :goto_57

    .line 1158
    :cond_73
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_a6

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v8

    .line 1159
    int-to-long v0, p4

    invoke-virtual {v8, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1160
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->O:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162
    new-instance v0, Lcom/android/launcher2/ap;

    move-object v1, p0

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher2/ap;-><init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/CellLayout$LayoutParams;IIIILandroid/view/View;)V

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1171
    new-instance v0, Lcom/android/launcher2/aq;

    invoke-direct {v0, p0, v2, p1}, Lcom/android/launcher2/aq;-><init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/CellLayout$LayoutParams;Landroid/view/View;)V

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1189
    int-to-long v0, p5

    invoke-virtual {v8, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1190
    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 1191
    const/4 v0, 0x1

    goto :goto_6d

    .line 1193
    :cond_a1
    const/4 v0, 0x0

    goto :goto_6d

    :cond_a3
    move-object v4, v0

    goto/16 :goto_b

    .line 1158
    :array_a6
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(Landroid/view/View;IILcom/android/launcher2/CellLayout$LayoutParams;Z)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 638
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_f

    move-object v0, p1

    .line 642
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 645
    iget-boolean v2, p0, Lcom/android/launcher2/CellLayout;->V:Z

    if-eqz v2, :cond_4b

    .line 646
    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setTextColor(I)V

    .line 654
    :cond_f
    :goto_f
    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    if-ltz v0, :cond_51

    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->i:I

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_51

    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    if-ltz v0, :cond_51

    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->j:I

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_51

    .line 657
    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    if-gez v0, :cond_2f

    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iput v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 658
    :cond_2f
    iget v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    if-gez v0, :cond_37

    iget v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    iput v0, p4, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 660
    :cond_37
    invoke-virtual {p1, p3}, Landroid/view/View;->setId(I)V

    .line 662
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1, p2, p4}, Lcom/android/launcher2/ja;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 664
    if-eqz p5, :cond_44

    invoke-virtual {p0, p1}, Lcom/android/launcher2/CellLayout;->c(Landroid/view/View;)V

    .line 665
    :cond_44
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->a()V

    .line 666
    const/4 v0, 0x1

    .line 668
    :goto_4a
    return v0

    .line 648
    :cond_4b
    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->a:I

    invoke-virtual {v0, v2}, Lcom/android/launcher2/BubbleTextView;->setTextColor(I)V

    goto :goto_f

    :cond_51
    move v0, v1

    .line 668
    goto :goto_4a
.end method

.method public final a([I)Z
    .registers 5
    .parameter

    .prologue
    .line 2832
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->j:I

    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    invoke-static {p1, v0, v1, v2}, Lcom/android/launcher2/CellLayout;->a([III[[Z)Z

    move-result v0

    return v0
.end method

.method final a([III)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 2577
    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a([IIIII[[Z)Z

    move-result v0

    return v0
.end method

.method final a(IIIIIILandroid/view/View;[I[II)[I
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2391
    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p8

    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    move-result-object v13

    .line 2393
    if-nez p9, :cond_16

    .line 2394
    const/4 v1, 0x2

    new-array v0, v1, [I

    move-object/from16 p9, v0

    .line 2400
    :cond_16
    const/4 v1, 0x1

    move/from16 v0, p10

    if-eq v0, v1, :cond_25

    const/4 v1, 0x2

    move/from16 v0, p10

    if-eq v0, v1, :cond_25

    const/4 v1, 0x3

    move/from16 v0, p10

    if-ne v0, v1, :cond_ff

    .line 2401
    :cond_25
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, -0x64

    if-eq v1, v2, :cond_ff

    .line 2402
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2403
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2405
    const/4 v1, 0x1

    move/from16 v0, p10

    if-eq v0, v1, :cond_4c

    const/4 v1, 0x2

    move/from16 v0, p10

    if-ne v0, v1, :cond_5a

    .line 2406
    :cond_4c
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v2, 0x0

    const/16 v3, -0x64

    aput v3, v1, v2

    .line 2407
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v2, 0x1

    const/16 v3, -0x64

    aput v3, v1, v2

    .line 2416
    :cond_5a
    :goto_5a
    iget-object v8, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v10, 0x1

    const/4 v11, 0x0

    new-instance v12, Lcom/android/launcher2/at;

    const/4 v1, 0x0

    invoke-direct {v12, p0, v1}, Lcom/android/launcher2/at;-><init>(Lcom/android/launcher2/CellLayout;B)V

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v9, p7

    .line 2415
    invoke-direct/range {v1 .. v12}, Lcom/android/launcher2/CellLayout;->a(IIIIII[ILandroid/view/View;ZZLcom/android/launcher2/at;)Lcom/android/launcher2/at;

    move-result-object v9

    .line 2420
    new-instance v8, Lcom/android/launcher2/at;

    const/4 v1, 0x0

    invoke-direct {v8, p0, v1}, Lcom/android/launcher2/at;-><init>(Lcom/android/launcher2/CellLayout;B)V

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 2419
    invoke-direct/range {v1 .. v8}, Lcom/android/launcher2/CellLayout;->a(IIIIIILcom/android/launcher2/at;)Lcom/android/launcher2/at;

    move-result-object v1

    .line 2422
    const/4 v2, 0x0

    .line 2423
    iget-boolean v3, v9, Lcom/android/launcher2/at;->b:Z

    if-eqz v3, :cond_1af

    invoke-virtual {v9}, Lcom/android/launcher2/at;->a()I

    move-result v3

    invoke-virtual {v1}, Lcom/android/launcher2/at;->a()I

    move-result v4

    if-lt v3, v4, :cond_1af

    move-object v1, v9

    .line 2429
    :cond_9e
    :goto_9e
    const/4 v3, 0x1

    .line 2431
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/launcher2/CellLayout;->setUseTempCoords(Z)V

    .line 2434
    if-eqz v1, :cond_1c1

    .line 2435
    const/4 v2, 0x0

    iget v4, v1, Lcom/android/launcher2/at;->c:I

    aput v4, v13, v2

    .line 2436
    const/4 v2, 0x1

    iget v4, v1, Lcom/android/launcher2/at;->d:I

    aput v4, v13, v2

    .line 2437
    const/4 v2, 0x0

    iget v4, v1, Lcom/android/launcher2/at;->e:I

    aput v4, p9, v2

    .line 2438
    const/4 v2, 0x1

    iget v4, v1, Lcom/android/launcher2/at;->f:I

    aput v4, p9, v2

    .line 2443
    if-eqz p10, :cond_c5

    const/4 v2, 0x1

    move/from16 v0, p10

    if-eq v0, v2, :cond_c5

    const/4 v2, 0x2

    move/from16 v0, p10

    if-ne v0, v2, :cond_1d1

    .line 2445
    :cond_c5
    move-object/from16 v0, p7

    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;Landroid/view/View;)V

    .line 2447
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/launcher2/CellLayout;->setItemPlacementDirty(Z)V

    .line 2448
    const/4 v2, 0x1

    move/from16 v0, p10

    if-ne v0, v2, :cond_1b6

    const/4 v2, 0x1

    :goto_d4
    move-object/from16 v0, p7

    invoke-direct {p0, v1, v0, v2}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/at;Landroid/view/View;Z)V

    .line 2451
    const/4 v2, 0x1

    move/from16 v0, p10

    if-eq v0, v2, :cond_e3

    const/4 v2, 0x2

    move/from16 v0, p10

    if-ne v0, v2, :cond_1b9

    .line 2452
    :cond_e3
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->m()V

    .line 2453
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->l()V

    .line 2454
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setItemPlacementDirty(Z)V

    move v1, v3

    .line 2465
    :goto_ee
    const/4 v2, 0x1

    move/from16 v0, p10

    if-eq v0, v2, :cond_f5

    if-nez v1, :cond_f9

    .line 2466
    :cond_f5
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setUseTempCoords(Z)V

    .line 2469
    :cond_f9
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->requestLayout()V

    .line 2470
    return-object v13

    .line 2410
    :cond_ff
    iget-object v14, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v1, 0x2

    new-array v6, v1, [I

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p5

    move/from16 v5, p6

    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    aget v8, v6, v1

    const/4 v1, 0x1

    aget v9, v6, v1

    move-object v7, p0

    move/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v7 .. v12}, Lcom/android/launcher2/CellLayout;->b(IIIILandroid/graphics/Rect;)V

    invoke-virtual {v12}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v12}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    aget v2, v6, v1

    const/4 v1, 0x1

    aget v3, v6, v1

    iget-object v8, p0, Lcom/android/launcher2/CellLayout;->Z:Ljava/util/ArrayList;

    move-object v1, p0

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v8}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v3, v7, Landroid/graphics/Rect;->left:I

    iget v4, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v6

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/launcher2/CellLayout;->b(IIIILandroid/graphics/Rect;)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    sub-int v1, v1, p1

    div-int v1, v1, p5

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, v2, p2

    div-int v2, v2, p6

    iget v3, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-eq v8, v3, :cond_17b

    iget v3, p0, Lcom/android/launcher2/CellLayout;->i:I

    move/from16 v0, p5

    if-ne v0, v3, :cond_17c

    :cond_17b
    const/4 v1, 0x0

    :cond_17c
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-eq v9, v3, :cond_186

    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    move/from16 v0, p6

    if-ne v0, v3, :cond_187

    :cond_186
    const/4 v2, 0x0

    :cond_187
    if-nez v1, :cond_1a9

    if-nez v2, :cond_1a9

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v14, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v14, v1

    .line 2411
    :goto_193
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2412
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->d:[I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->ab:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    goto/16 :goto_5a

    .line 2410
    :cond_1a9
    int-to-float v1, v1

    int-to-float v2, v2

    invoke-static {v1, v2, v14}, Lcom/android/launcher2/CellLayout;->b(FF[I)V

    goto :goto_193

    .line 2425
    :cond_1af
    iget-boolean v3, v1, Lcom/android/launcher2/at;->b:Z

    if-nez v3, :cond_9e

    move-object v1, v2

    goto/16 :goto_9e

    .line 2448
    :cond_1b6
    const/4 v2, 0x0

    goto/16 :goto_d4

    .line 2457
    :cond_1b9
    move-object/from16 v0, p7

    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/at;Landroid/view/View;)V

    move v1, v3

    goto/16 :goto_ee

    .line 2461
    :cond_1c1
    const/4 v1, 0x0

    .line 2462
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, -0x1

    aput v6, p9, v5

    aput v6, p9, v4

    aput v6, v13, v3

    aput v6, v13, v2

    goto/16 :goto_ee

    :cond_1d1
    move v1, v3

    goto/16 :goto_ee
.end method

.method final a(IIIIII[I[I)[I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1335
    invoke-direct/range {p0 .. p8}, Lcom/android/launcher2/CellLayout;->b(IIIIII[I[I)[I

    move-result-object v0

    return-object v0
.end method

.method protected final b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 404
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->m:Z

    if-eqz v0, :cond_26

    .line 405
    invoke-virtual {p0, v2}, Lcom/android/launcher2/CellLayout;->setOverscrollTransformsDirty(Z)V

    .line 406
    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 407
    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 410
    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/CellLayout;->a(FZ)V

    .line 411
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->setPivotX(F)V

    .line 412
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->setPivotY(F)V

    .line 414
    :cond_26
    return-void
.end method

.method public final b(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 588
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 589
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 590
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 591
    return-void
.end method

.method public final b(Lcom/android/launcher2/co;)V
    .registers 3
    .parameter

    .prologue
    .line 581
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 582
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 584
    :cond_d
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 585
    return-void
.end method

.method final b([III)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 2609
    .line 2610
    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, p2

    move v5, p3

    .line 2609
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a([IIIII[[Z)Z

    move-result v0

    return v0
.end method

.method final b(IIII[I)[I
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2556
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIIIZ[I)[I

    move-result-object v0

    return-object v0
.end method

.method final b(II[I)[I
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 1315
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v5, v3

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIIIZ[I)[I

    move-result-object v0

    return-object v0
.end method

.method public final c(II)Landroid/view/View;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1109
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v3}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_8
    if-lt v2, v4, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    invoke-virtual {v3, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    if-gt v5, p1, :cond_2e

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v6, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    add-int/2addr v5, v6

    if-ge p1, v5, :cond_2e

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    if-gt v5, p2, :cond_2e

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    add-int/2addr v0, v5

    if-ge p2, v0, :cond_2e

    move-object v0, v1

    goto :goto_b

    :cond_2e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 594
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 595
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 596
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 597
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2873
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;[[Z)V

    .line 2874
    return-void
.end method

.method public cancelLongPress()V
    .registers 4

    .prologue
    .line 610
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 613
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getChildCount()I

    move-result v1

    .line 614
    const/4 v0, 0x0

    :goto_8
    if-lt v0, v1, :cond_b

    .line 618
    return-void

    .line 615
    :cond_b
    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 616
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 614
    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter

    .prologue
    .line 2940
    instance-of v0, p1, Lcom/android/launcher2/CellLayout$LayoutParams;

    return v0
.end method

.method public final d()V
    .registers 6

    .prologue
    .line 1295
    iget v0, p0, Lcom/android/launcher2/CellLayout;->L:I

    .line 1296
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    aget-object v0, v1, v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/launcher2/dg;->a(I)V

    .line 1297
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v3, 0x1

    const/4 v4, -0x1

    aput v4, v2, v3

    aput v4, v0, v1

    .line 1298
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2897
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;[[Z)V

    .line 2898
    return-void
.end method

.method public final d(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2926
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    if-ge p1, v0, :cond_f

    iget v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    if-ge p2, v0, :cond_f

    .line 2927
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    aget-object v0, v0, p1

    aget-boolean v0, v0, p2

    return v0

    .line 2929
    :cond_f
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position exceeds the bound of this CellLayout"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    .line 566
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 567
    iget v0, p0, Lcom/android/launcher2/CellLayout;->v:I

    if-lez v0, :cond_24

    .line 568
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->E:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 569
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 570
    sget-object v1, Lcom/android/launcher2/CellLayout;->ad:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 571
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 572
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 574
    :cond_24
    return-void
.end method

.method final e()V
    .registers 11

    .prologue
    const/4 v5, 0x0

    .line 2341
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->Q:Z

    if-nez v0, :cond_6

    .line 2355
    :goto_5
    return-void

    .line 2342
    :cond_6
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v9

    move v8, v5

    .line 2343
    :goto_d
    if-lt v8, v9, :cond_16

    .line 2353
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->l()V

    .line 2354
    invoke-virtual {p0, v5}, Lcom/android/launcher2/CellLayout;->setItemPlacementDirty(Z)V

    goto :goto_5

    .line 2344
    :cond_16
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v8}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2345
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2346
    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    if-ne v2, v3, :cond_2e

    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    if-eq v2, v3, :cond_42

    .line 2347
    :cond_2e
    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iput v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 2348
    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iput v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    .line 2349
    iget v2, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    const/16 v4, 0x96

    move-object v0, p0

    move v6, v5

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    .line 2343
    :cond_42
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_d
.end method

.method final f()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 2683
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->ac:Lcom/android/launcher2/by;

    invoke-virtual {v0}, Lcom/android/launcher2/by;->b()V

    .line 2684
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->S:Z

    if-nez v0, :cond_13

    .line 2686
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    if-eqz v0, :cond_13

    .line 2687
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/dg;->a(I)V

    .line 2690
    :cond_13
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout;->S:Z

    .line 2691
    return-void
.end method

.method final g()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 2697
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->ac:Lcom/android/launcher2/by;

    invoke-virtual {v0}, Lcom/android/launcher2/by;->c()V

    .line 2701
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->S:Z

    if-eqz v0, :cond_16

    .line 2702
    iput-boolean v4, p0, Lcom/android/launcher2/CellLayout;->S:Z

    .line 2705
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    if-eqz v0, :cond_16

    .line 2706
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->al:Lcom/android/launcher2/dg;

    invoke-virtual {v0, v5}, Lcom/android/launcher2/dg;->a(I)V

    .line 2711
    :cond_16
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->R:[I

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->R:[I

    const/4 v2, 0x1

    const/4 v3, -0x1

    aput v3, v1, v2

    aput v3, v0, v4

    .line 2712
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    iget v1, p0, Lcom/android/launcher2/CellLayout;->L:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Lcom/android/launcher2/dg;->a(I)V

    .line 2713
    iget v0, p0, Lcom/android/launcher2/CellLayout;->L:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/CellLayout;->L:I

    .line 2714
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->e()V

    .line 2715
    invoke-virtual {p0, v4}, Lcom/android/launcher2/CellLayout;->setIsDragOverlapping(Z)V

    .line 2716
    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter

    .prologue
    .line 2935
    new-instance v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/launcher2/CellLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter

    .prologue
    .line 2945
    new-instance v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/android/launcher2/CellLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getBackgroundAlpha()F
    .registers 2

    .prologue
    .line 1073
    iget v0, p0, Lcom/android/launcher2/CellLayout;->w:F

    return v0
.end method

.method public getBackgroundAlphaMultiplier()F
    .registers 2

    .prologue
    .line 1084
    iget v0, p0, Lcom/android/launcher2/CellLayout;->x:F

    return v0
.end method

.method public getCellHeight()I
    .registers 2

    .prologue
    .line 923
    iget v0, p0, Lcom/android/launcher2/CellLayout;->h:I

    return v0
.end method

.method public getCellWidth()I
    .registers 2

    .prologue
    .line 919
    iget v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    return v0
.end method

.method getCountX()I
    .registers 2

    .prologue
    .line 625
    iget v0, p0, Lcom/android/launcher2/CellLayout;->i:I

    return v0
.end method

.method getCountY()I
    .registers 2

    .prologue
    .line 629
    iget v0, p0, Lcom/android/launcher2/CellLayout;->j:I

    return v0
.end method

.method public getDesiredHeight()I
    .registers 4

    .prologue
    .line 2921
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout;->j:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2922
    iget v1, p0, Lcom/android/launcher2/CellLayout;->j:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v1, v2

    .line 2921
    add-int/2addr v0, v1

    return v0
.end method

.method public getDesiredWidth()I
    .registers 4

    .prologue
    .line 2916
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2917
    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v1, v2

    .line 2916
    add-int/2addr v0, v1

    return v0
.end method

.method getHeightGap()I
    .registers 2

    .prologue
    .line 931
    iget v0, p0, Lcom/android/launcher2/CellLayout;->l:I

    return v0
.end method

.method getIsDragOverlapping()Z
    .registers 2

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->G:Z

    return v0
.end method

.method public getShortcutsAndWidgets()Lcom/android/launcher2/ja;
    .registers 2

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 1103
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    .line 1105
    :goto_d
    return-object v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public getTag()Lcom/android/launcher2/as;
    .registers 2

    .prologue
    .line 815
    invoke-super {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/as;

    return-object v0
.end method

.method public bridge synthetic getTag()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getTag()Lcom/android/launcher2/as;

    move-result-object v0

    return-object v0
.end method

.method getWidthGap()I
    .registers 2

    .prologue
    .line 927
    iget v0, p0, Lcom/android/launcher2/CellLayout;->k:I

    return v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 3124
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->r:Z

    return v0
.end method

.method final i()Lcom/android/launcher2/as;
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 3199
    iget v3, p0, Lcom/android/launcher2/CellLayout;->i:I

    .line 3200
    iget v4, p0, Lcom/android/launcher2/CellLayout;->j:I

    move v2, v0

    .line 3202
    :goto_6
    if-lt v2, v4, :cond_a

    .line 3212
    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    move v1, v0

    .line 3203
    :goto_b
    if-lt v1, v3, :cond_11

    .line 3202
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    .line 3204
    :cond_11
    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->b:[[Z

    aget-object v5, v5, v1

    aget-boolean v5, v5, v2

    if-nez v5, :cond_23

    .line 3205
    new-instance v0, Lcom/android/launcher2/as;

    invoke-direct {v0}, Lcom/android/launcher2/as;-><init>()V

    .line 3206
    iput v1, v0, Lcom/android/launcher2/as;->b:I

    .line 3207
    iput v2, v0, Lcom/android/launcher2/as;->c:I

    goto :goto_9

    .line 3203
    :cond_23
    add-int/lit8 v1, v1, 0x1

    goto :goto_b
.end method

.method public final j()V
    .registers 5

    .prologue
    .line 3277
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->af:Z

    if-nez v0, :cond_33

    .line 3278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout;->af:Z

    .line 3279
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 3280
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    .line 3281
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    .line 3282
    invoke-virtual {v1}, Lcom/android/launcher2/DragLayer;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 3283
    invoke-virtual {v1}, Lcom/android/launcher2/DragLayer;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingTop()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingBottom()I

    move-result v0

    sub-int v0, v1, v0

    .line 3282
    invoke-direct {p0, v2, v0}, Lcom/android/launcher2/CellLayout;->e(II)V

    .line 3285
    :cond_33
    return-void
.end method

.method public final k()V
    .registers 4

    .prologue
    .line 3293
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3295
    instance-of v1, v0, Lcom/android/launcher2/Hotseat;

    if-eqz v1, :cond_1a

    move-object v1, v0

    .line 3296
    check-cast v1, Lcom/android/launcher2/Hotseat;

    .line 3297
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->o:Lcom/android/launcher2/as;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Hotseat;->i(I)I

    move-result v0

    iput v0, v2, Lcom/android/launcher2/as;->f:I

    .line 3301
    :goto_19
    return-void

    .line 3299
    :cond_1a
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->o:Lcom/android/launcher2/as;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, v1, Lcom/android/launcher2/as;->f:I

    goto :goto_19
.end method

.method protected onAttachedToWindow()V
    .registers 1

    .prologue
    .line 725
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 726
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->k()V

    .line 727
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 15
    .parameter

    .prologue
    .line 423
    iget v0, p0, Lcom/android/launcher2/CellLayout;->w:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_21

    .line 426
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->G:Z

    if-eqz v0, :cond_e0

    .line 428
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->z:Landroid/graphics/drawable/Drawable;

    .line 433
    :goto_d
    iget v1, p0, Lcom/android/launcher2/CellLayout;->w:F

    iget v2, p0, Lcom/android/launcher2/CellLayout;->x:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x437f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 434
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->D:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 435
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 438
    :cond_21
    iget v0, p0, Lcom/android/launcher2/CellLayout;->aj:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_47

    .line 439
    iget v4, p0, Lcom/android/launcher2/CellLayout;->i:I

    .line 440
    iget v5, p0, Lcom/android/launcher2/CellLayout;->j:I

    .line 442
    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->ai:Landroid/graphics/drawable/Drawable;

    .line 447
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 448
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    .line 450
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/CellLayout;->k:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    div-int/lit8 v1, v7, 0x2

    sub-int/2addr v0, v1

    .line 451
    const/4 v1, 0x0

    move v2, v0

    move v3, v1

    :goto_45
    if-le v3, v4, :cond_e4

    .line 470
    :cond_47
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->M:Landroid/graphics/Paint;

    .line 471
    const/4 v0, 0x0

    move v1, v0

    :goto_4b
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    array-length v0, v0

    if-lt v1, v0, :cond_14a

    .line 483
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_81

    .line 484
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getPressedOrFocusedBackgroundPadding()I

    move-result v0

    .line 485
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v1}, Lcom/android/launcher2/BubbleTextView;->getPressedOrFocusedBackground()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 486
    if-eqz v1, :cond_81

    .line 488
    iget-object v2, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v2}, Lcom/android/launcher2/BubbleTextView;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v0

    int-to-float v2, v2

    .line 489
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v3}, Lcom/android/launcher2/BubbleTextView;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v0, v3, v0

    int-to-float v0, v0

    .line 490
    const/4 v3, 0x0

    .line 487
    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 511
    :cond_81
    sget v2, Lcom/android/launcher2/co;->j:I

    .line 514
    const/4 v0, 0x0

    move v1, v0

    :goto_85
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_16f

    .line 547
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-ltz v0, :cond_df

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-ltz v0, :cond_df

    .line 548
    sget-object v0, Lcom/android/launcher2/FolderIcon;->c:Landroid/graphics/drawable/Drawable;

    .line 549
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 550
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 552
    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->u:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->a:[I

    invoke-direct {p0, v4, v5, v6}, Lcom/android/launcher2/CellLayout;->c(II[I)V

    .line 553
    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    iget v5, p0, Lcom/android/launcher2/CellLayout;->g:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 554
    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v5

    .line 556
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 557
    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v2, v5

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 558
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 559
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 560
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 562
    :cond_df
    return-void

    .line 430
    :cond_e0
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->y:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_d

    .line 452
    :cond_e4
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/CellLayout;->l:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    div-int/lit8 v1, v8, 0x2

    sub-int v1, v0, v1

    .line 453
    const/4 v0, 0x0

    :goto_f2
    if-le v0, v5, :cond_100

    .line 466
    iget v0, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 451
    add-int/lit8 v1, v3, 0x1

    move v2, v0

    move v3, v1

    goto/16 :goto_45

    .line 454
    :cond_100
    iget-object v9, p0, Lcom/android/launcher2/CellLayout;->ak:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/android/launcher2/CellLayout;->H:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int v10, v2, v10

    int-to-float v10, v10

    iget-object v11, p0, Lcom/android/launcher2/CellLayout;->H:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->y:I

    sub-int v11, v1, v11

    int-to-float v11, v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/PointF;->set(FF)V

    .line 455
    iget-object v9, p0, Lcom/android/launcher2/CellLayout;->ak:Landroid/graphics/PointF;

    invoke-virtual {v9}, Landroid/graphics/PointF;->length()F

    move-result v9

    .line 457
    const v10, 0x3ecccccd

    .line 458
    const v11, 0x3b03126f

    const/high16 v12, 0x4416

    sub-float v9, v12, v9

    mul-float/2addr v9, v11

    .line 457
    invoke-static {v10, v9}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 459
    const/4 v10, 0x0

    cmpl-float v10, v9, v10

    if-lez v10, :cond_141

    .line 460
    add-int v10, v2, v7

    add-int v11, v1, v8

    invoke-virtual {v6, v2, v1, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 461
    const/high16 v10, 0x437f

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/android/launcher2/CellLayout;->aj:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 462
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 464
    :cond_141
    iget v9, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v10, p0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v9, v10

    add-int/2addr v1, v9

    .line 453
    add-int/lit8 v0, v0, 0x1

    goto :goto_f2

    .line 472
    :cond_14a
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->J:[F

    aget v3, v0, v1

    .line 473
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_16a

    .line 474
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->I:[Landroid/graphics/Rect;

    aget-object v4, v0, v1

    .line 475
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->K:[Lcom/android/launcher2/dg;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/android/launcher2/dg;->b:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 476
    const/high16 v5, 0x3f00

    add-float/2addr v3, v5

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 477
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 471
    :cond_16a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4b

    .line 515
    :cond_16f
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/co;

    .line 518
    sget-object v3, Lcom/android/launcher2/co;->h:Landroid/graphics/drawable/Drawable;

    .line 519
    invoke-virtual {v0}, Lcom/android/launcher2/co;->d()F

    move-result v4

    float-to-int v4, v4

    .line 521
    iget v5, v0, Lcom/android/launcher2/co;->a:I

    iget v6, v0, Lcom/android/launcher2/co;->b:I

    iget-object v7, p0, Lcom/android/launcher2/CellLayout;->a:[I

    invoke-direct {p0, v5, v6, v7}, Lcom/android/launcher2/CellLayout;->c(II[I)V

    .line 523
    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    iget v6, p0, Lcom/android/launcher2/CellLayout;->g:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    .line 524
    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    div-int/lit8 v7, v2, 0x2

    add-int/2addr v6, v7

    .line 526
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 527
    div-int/lit8 v7, v4, 0x2

    sub-int/2addr v5, v7

    int-to-float v5, v5

    div-int/lit8 v7, v4, 0x2

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 528
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6, v4, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 529
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 530
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 533
    sget-object v3, Lcom/android/launcher2/co;->i:Landroid/graphics/drawable/Drawable;

    .line 534
    invoke-virtual {v0}, Lcom/android/launcher2/co;->e()F

    move-result v4

    float-to-int v4, v4

    .line 536
    iget v5, v0, Lcom/android/launcher2/co;->a:I

    iget v0, v0, Lcom/android/launcher2/co;->b:I

    iget-object v6, p0, Lcom/android/launcher2/CellLayout;->a:[I

    invoke-direct {p0, v5, v0, v6}, Lcom/android/launcher2/CellLayout;->c(II[I)V

    .line 538
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v5, 0x0

    aget v0, v0, v5

    iget v5, p0, Lcom/android/launcher2/CellLayout;->g:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 539
    iget-object v5, p0, Lcom/android/launcher2/CellLayout;->a:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    div-int/lit8 v6, v2, 0x2

    add-int/2addr v5, v6

    .line 540
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 541
    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v0, v6

    int-to-float v0, v0

    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 542
    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v4, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 543
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 544
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 514
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_85
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter

    .prologue
    .line 787
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 789
    if-nez v0, :cond_1a

    .line 790
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->o:Lcom/android/launcher2/as;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    const/4 v2, -0x1

    iput v2, v1, Lcom/android/launcher2/as;->b:I

    const/4 v2, -0x1

    iput v2, v1, Lcom/android/launcher2/as;->c:I

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/launcher2/as;->d:I

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/launcher2/as;->e:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->setTag(Ljava/lang/Object;)V

    .line 793
    :cond_1a
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->s:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_28

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->s:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 794
    const/4 v0, 0x1

    .line 801
    :goto_27
    return v0

    .line 797
    :cond_28
    if-nez v0, :cond_b5

    .line 798
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v2, v1

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->o:Lcom/android/launcher2/as;

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->n:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getScrollX()I

    move-result v3

    add-int v5, v0, v3

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getScrollY()I

    move-result v0

    add-int v6, v2, v0

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4e
    if-gez v3, :cond_b8

    move v0, v2

    :goto_51
    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout;->r:Z

    if-nez v0, :cond_b2

    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->p:[I

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v2

    const/4 v3, 0x0

    sub-int v1, v5, v1

    iget v5, p0, Lcom/android/launcher2/CellLayout;->g:I

    iget v7, p0, Lcom/android/launcher2/CellLayout;->k:I

    add-int/2addr v5, v7

    div-int/2addr v1, v5

    aput v1, v0, v3

    const/4 v1, 0x1

    sub-int v2, v6, v2

    iget v3, p0, Lcom/android/launcher2/CellLayout;->h:I

    iget v5, p0, Lcom/android/launcher2/CellLayout;->l:I

    add-int/2addr v3, v5

    div-int/2addr v2, v3

    aput v2, v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->j:I

    const/4 v3, 0x0

    aget v3, v0, v3

    if-gez v3, :cond_82

    const/4 v3, 0x0

    const/4 v5, 0x0

    aput v5, v0, v3

    :cond_82
    const/4 v3, 0x0

    aget v3, v0, v3

    if-lt v3, v1, :cond_8c

    const/4 v3, 0x0

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v3

    :cond_8c
    const/4 v1, 0x1

    aget v1, v0, v1

    if-gez v1, :cond_95

    const/4 v1, 0x1

    const/4 v3, 0x0

    aput v3, v0, v1

    :cond_95
    const/4 v1, 0x1

    aget v1, v0, v1

    if-lt v1, v2, :cond_9f

    const/4 v1, 0x1

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    :cond_9f
    const/4 v1, 0x0

    iput-object v1, v4, Lcom/android/launcher2/as;->a:Landroid/view/View;

    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, v4, Lcom/android/launcher2/as;->b:I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v4, Lcom/android/launcher2/as;->c:I

    const/4 v0, 0x1

    iput v0, v4, Lcom/android/launcher2/as;->d:I

    const/4 v0, 0x1

    iput v0, v4, Lcom/android/launcher2/as;->e:I

    :cond_b2
    invoke-virtual {p0, v4}, Lcom/android/launcher2/CellLayout;->setTag(Ljava/lang/Object;)V

    .line 801
    :cond_b5
    const/4 v0, 0x0

    goto/16 :goto_27

    .line 798
    :cond_b8
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_d0

    invoke-virtual {v7}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v8

    if-eqz v8, :cond_13b

    :cond_d0
    iget-boolean v8, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    if-eqz v8, :cond_13b

    invoke-virtual {v7, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v7}, Landroid/view/View;->getScaleX()F

    move-result v8

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v11

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v12

    invoke-direct {v1, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v10

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Rect;->offset(II)V

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f80

    sub-float/2addr v10, v8

    mul-float/2addr v9, v10

    const/high16 v10, 0x4000

    div-float/2addr v9, v10

    float-to-int v9, v9

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x3f80

    sub-float v8, v11, v8

    mul-float/2addr v8, v10

    const/high16 v10, 0x4000

    div-float/2addr v8, v10

    float-to-int v8, v8

    invoke-virtual {v1, v9, v8}, Landroid/graphics/Rect;->inset(II)V

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_134

    iput-object v7, v4, Lcom/android/launcher2/as;->a:Landroid/view/View;

    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iput v1, v4, Lcom/android/launcher2/as;->b:I

    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iput v1, v4, Lcom/android/launcher2/as;->c:I

    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iput v1, v4, Lcom/android/launcher2/as;->d:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    iput v0, v4, Lcom/android/launcher2/as;->e:I

    const/4 v0, 0x1

    goto/16 :goto_51

    :cond_134
    move-object v0, v1

    :goto_135
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move-object v1, v0

    goto/16 :goto_4e

    :cond_13b
    move-object v0, v1

    goto :goto_135
.end method

.method protected onLayout(ZIIII)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1046
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getChildCount()I

    move-result v1

    .line 1047
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_8

    .line 1052
    return-void

    .line 1048
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1049
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v4

    .line 1050
    sub-int v5, p4, p2

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v6, p5, p3

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    .line 1049
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 1047
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/high16 v10, 0x4000

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 995
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 996
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 999
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1001
    iput-boolean v9, p0, Lcom/android/launcher2/CellLayout;->af:Z

    .line 1002
    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->e(II)V

    .line 1021
    const/high16 v4, -0x8000

    if-ne v3, v4, :cond_4a

    .line 1024
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->g:I

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1025
    iget v1, p0, Lcom/android/launcher2/CellLayout;->i:I

    add-int/lit8 v1, v1, -0x1

    iget v3, p0, Lcom/android/launcher2/CellLayout;->k:I

    mul-int/2addr v1, v3

    .line 1024
    add-int/2addr v1, v0

    .line 1026
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    iget v4, p0, Lcom/android/launcher2/CellLayout;->h:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1027
    iget v3, p0, Lcom/android/launcher2/CellLayout;->j:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/android/launcher2/CellLayout;->l:I

    mul-int/2addr v3, v4

    .line 1026
    add-int/2addr v0, v3

    .line 1028
    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->setMeasuredDimension(II)V

    .line 1031
    :cond_4a
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getChildCount()I

    move-result v4

    move v3, v2

    .line 1032
    :goto_4f
    if-lt v3, v4, :cond_82

    .line 1040
    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/CellLayout;->setMeasuredDimension(II)V

    .line 1041
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->e:Z

    if-eqz v0, :cond_81

    iput-boolean v2, p0, Lcom/android/launcher2/CellLayout;->e:Z

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v3

    if-eqz v3, :cond_81

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a0001

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getCellHeight()I

    move-result v1

    if-lt v1, v0, :cond_81

    :goto_7b
    invoke-virtual {v3}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v0

    if-lt v2, v0, :cond_aa

    .line 1042
    :cond_81
    return-void

    .line 1033
    :cond_82
    invoke-virtual {p0, v3}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1034
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v6

    sub-int v6, v1, v6

    .line 1035
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingRight()I

    move-result v7

    .line 1034
    sub-int/2addr v6, v7

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 1036
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v7

    sub-int v7, v0, v7

    .line 1037
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getPaddingBottom()I

    move-result v8

    .line 1036
    sub-int/2addr v7, v8

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 1038
    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    .line 1032
    add-int/lit8 v3, v3, 0x1

    goto :goto_4f

    .line 1041
    :cond_aa
    invoke-virtual {v3, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/android/launcher2/BubbleTextView;

    if-eqz v1, :cond_c6

    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/jd;

    iget-object v1, v1, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->invalidate()V

    :cond_c2
    :goto_c2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7b

    :cond_c6
    instance-of v1, v0, Lcom/android/launcher2/FolderIcon;

    if-eqz v1, :cond_c2

    check-cast v0, Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/FolderIcon;->setTextVisible(Z)V

    goto :goto_c2
.end method

.method protected onSizeChanged(IIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1056
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1057
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->D:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1058
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->E:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/launcher2/CellLayout;->F:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->F:I

    .line 1059
    iget v3, p0, Lcom/android/launcher2/CellLayout;->F:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    iget v4, p0, Lcom/android/launcher2/CellLayout;->F:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    .line 1058
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1060
    return-void
.end method

.method public removeAllViews()V
    .registers 2

    .prologue
    .line 673
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->n()V

    .line 674
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->removeAllViews()V

    .line 675
    return-void
.end method

.method public removeAllViewsInLayout()V
    .registers 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v0

    if-lez v0, :cond_10

    .line 680
    invoke-direct {p0}, Lcom/android/launcher2/CellLayout;->n()V

    .line 681
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->removeAllViewsInLayout()V

    .line 683
    :cond_10
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 691
    invoke-virtual {p0, p1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 692
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->removeView(Landroid/view/View;)V

    .line 693
    return-void
.end method

.method public removeViewAt(I)V
    .registers 3
    .parameter

    .prologue
    .line 697
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 698
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->removeViewAt(I)V

    .line 699
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 703
    invoke-virtual {p0, p1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 704
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->removeViewInLayout(Landroid/view/View;)V

    .line 705
    return-void
.end method

.method public removeViews(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 709
    move v0, p1

    :goto_1
    add-int v1, p1, p2

    if-lt v0, v1, :cond_b

    .line 712
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/ja;->removeViews(II)V

    .line 713
    return-void

    .line 710
    :cond_b
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 709
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public removeViewsInLayout(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 717
    move v0, p1

    :goto_1
    add-int v1, p1, p2

    if-lt v0, v1, :cond_b

    .line 720
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/ja;->removeViewsInLayout(II)V

    .line 721
    return-void

    .line 718
    :cond_b
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 717
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setBackgroundAlpha(F)V
    .registers 3
    .parameter

    .prologue
    .line 1088
    iget v0, p0, Lcom/android/launcher2/CellLayout;->w:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 1089
    iput p1, p0, Lcom/android/launcher2/CellLayout;->w:F

    .line 1090
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 1092
    :cond_b
    return-void
.end method

.method public setBackgroundAlphaMultiplier(F)V
    .registers 3
    .parameter

    .prologue
    .line 1077
    iget v0, p0, Lcom/android/launcher2/CellLayout;->x:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 1078
    iput p1, p0, Lcom/android/launcher2/CellLayout;->x:F

    .line 1079
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 1081
    :cond_b
    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->setChildrenDrawingCacheEnabled(Z)V

    .line 1065
    return-void
.end method

.method protected setChildrenDrawnWithCacheEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 1070
    return-void
.end method

.method setIsDragOverlapping(Z)V
    .registers 3
    .parameter

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout;->G:Z

    if-eq v0, p1, :cond_9

    .line 390
    iput-boolean p1, p0, Lcom/android/launcher2/CellLayout;->G:Z

    .line 391
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 393
    :cond_9
    return-void
.end method

.method public setIsHotseat(Z)V
    .registers 2
    .parameter

    .prologue
    .line 633
    iput-boolean p1, p0, Lcom/android/launcher2/CellLayout;->V:Z

    .line 634
    return-void
.end method

.method setItemPlacementDirty(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2474
    iput-boolean p1, p0, Lcom/android/launcher2/CellLayout;->Q:Z

    .line 2475
    return-void
.end method

.method public setMargins(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3138
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 3139
    if-eqz p1, :cond_d6

    .line 3140
    const-string v1, "NONE"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 3141
    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    .line 3149
    :cond_1b
    :goto_1b
    const-string v1, "NONE"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_86

    .line 3150
    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    .line 3195
    :cond_2d
    :goto_2d
    iget v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    iget v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    iget v2, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iget v3, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/launcher2/CellLayout;->setPadding(IIII)V

    .line 3196
    return-void

    .line 3142
    :cond_39
    const-string v1, "SMALL"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 3143
    const/high16 v1, 0x7f0b

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    goto :goto_1b

    .line 3144
    :cond_52
    const-string v1, "MEDIUM"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6c

    .line 3145
    const v1, 0x7f0b0001

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    goto :goto_1b

    .line 3146
    :cond_6c
    const-string v1, "LARGE"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 3147
    const v1, 0x7f0b0002

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    goto :goto_1b

    .line 3151
    :cond_86
    const-string v0, "SMALL"

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 3152
    const v0, 0x7f0b0003

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    goto :goto_2d

    .line 3153
    :cond_a0
    const-string v0, "MEDIUM"

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 3154
    const v0, 0x7f0b0004

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    goto/16 :goto_2d

    .line 3155
    :cond_bb
    const-string v0, "LARGE"

    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 3156
    const v0, 0x7f0b0005

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    goto/16 :goto_2d

    .line 3160
    :cond_d6
    const-string v1, "NONE"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->V:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_128

    .line 3169
    :cond_e4
    :goto_e4
    iget-object v1, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-boolean v1, v1, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v1, :cond_16b

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v1

    if-nez v1, :cond_16b

    .line 3170
    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    .line 3175
    :goto_f4
    const v1, 0x7f0b0018

    .line 3176
    const v0, 0x7f0b0019

    .line 3177
    const-string v3, "NONE"

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_170

    .line 3178
    const v1, 0x7f0b0010

    .line 3179
    const v0, 0x7f0b0011

    .line 3187
    :cond_10e
    :goto_10e
    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-boolean v3, v3, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v3, :cond_19b

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v3

    if-nez v3, :cond_19b

    .line 3188
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    .line 3189
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    goto/16 :goto_2d

    .line 3162
    :cond_128
    const-string v1, "SMALL"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->V:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13e

    .line 3163
    const v0, 0x7f0b000c

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_e4

    .line 3164
    :cond_13e
    const-string v1, "MEDIUM"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->V:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_154

    .line 3165
    const v0, 0x7f0b000d

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_e4

    .line 3166
    :cond_154
    const-string v1, "LARGE"

    iget-object v3, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->V:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e4

    .line 3167
    const v0, 0x7f0b000e

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto/16 :goto_e4

    .line 3172
    :cond_16b
    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingRight:I

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingLeft:I

    goto :goto_f4

    .line 3180
    :cond_170
    const-string v3, "SMALL"

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_185

    .line 3181
    const v1, 0x7f0b0014

    .line 3182
    const v0, 0x7f0b0015

    goto :goto_10e

    .line 3183
    :cond_185
    const-string v3, "LARGE"

    iget-object v4, p0, Lcom/android/launcher2/CellLayout;->f:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10e

    .line 3184
    const v1, 0x7f0b001c

    .line 3185
    const v0, 0x7f0b001d

    goto/16 :goto_10e

    .line 3191
    :cond_19b
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/CellLayout;->mPaddingTop:I

    .line 3192
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/CellLayout;->mPaddingBottom:I

    goto/16 :goto_2d
.end method

.method public setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V
    .registers 2
    .parameter

    .prologue
    .line 621
    iput-object p1, p0, Lcom/android/launcher2/CellLayout;->s:Landroid/view/View$OnTouchListener;

    .line 622
    return-void
.end method

.method protected setOverscrollTransformsDirty(Z)V
    .registers 2
    .parameter

    .prologue
    .line 400
    iput-boolean p1, p0, Lcom/android/launcher2/CellLayout;->m:Z

    .line 401
    return-void
.end method

.method setPressedOrFocusedIcon(Lcom/android/launcher2/BubbleTextView;)V
    .registers 3
    .parameter

    .prologue
    .line 378
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    .line 379
    iput-object p1, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    .line 380
    if-eqz v0, :cond_9

    .line 381
    invoke-direct {p0, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/BubbleTextView;)V

    .line 383
    :cond_9
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_12

    .line 384
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->N:Lcom/android/launcher2/BubbleTextView;

    invoke-direct {p0, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/BubbleTextView;)V

    .line 386
    :cond_12
    return-void
.end method

.method public setShortcutAndWidgetAlpha(F)V
    .registers 5
    .parameter

    .prologue
    .line 1095
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getChildCount()I

    move-result v1

    .line 1096
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_8

    .line 1099
    return-void

    .line 1097
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1096
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public setUseTempCoords(Z)V
    .registers 5
    .parameter

    .prologue
    .line 2232
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v2

    .line 2233
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-lt v1, v2, :cond_b

    .line 2237
    return-void

    .line 2234
    :cond_b
    iget-object v0, p0, Lcom/android/launcher2/CellLayout;->U:Lcom/android/launcher2/ja;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2235
    iput-boolean p1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    .line 2233
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    .prologue
    .line 601
    const/4 v0, 0x0

    return v0
.end method
