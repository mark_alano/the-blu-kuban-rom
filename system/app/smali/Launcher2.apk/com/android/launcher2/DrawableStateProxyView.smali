.class public Lcom/android/launcher2/DrawableStateProxyView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/DrawableStateProxyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/DrawableStateProxyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    sget-object v0, Lcom/anddoes/launcher/at;->DrawableStateProxyView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 46
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/DrawableStateProxyView;->b:I

    .line 47
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    invoke-virtual {p0, v2}, Lcom/android/launcher2/DrawableStateProxyView;->setFocusable(Z)V

    .line 50
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 56
    iget-object v0, p0, Lcom/android/launcher2/DrawableStateProxyView;->a:Landroid/view/View;

    if-nez v0, :cond_15

    .line 57
    invoke-virtual {p0}, Lcom/android/launcher2/DrawableStateProxyView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 58
    iget v1, p0, Lcom/android/launcher2/DrawableStateProxyView;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/DrawableStateProxyView;->a:Landroid/view/View;

    .line 60
    :cond_15
    iget-object v0, p0, Lcom/android/launcher2/DrawableStateProxyView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/launcher2/DrawableStateProxyView;->isPressed()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 61
    iget-object v0, p0, Lcom/android/launcher2/DrawableStateProxyView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/launcher2/DrawableStateProxyView;->isHovered()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setHovered(Z)V

    .line 62
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method
