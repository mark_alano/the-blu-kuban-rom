.class public Lcom/android/launcher2/FolderIcon;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/cv;


# static fields
.field public static c:Landroid/graphics/drawable/Drawable;

.field private static g:Z


# instance fields
.field public a:Lcom/android/launcher2/Folder;

.field b:Lcom/android/launcher2/cu;

.field d:Lcom/android/launcher2/co;

.field e:Z

.field private f:Lcom/android/launcher2/Launcher;

.field private h:Landroid/widget/ImageView;

.field private i:Lcom/android/launcher2/BubbleTextView;

.field private j:I

.field private k:F

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:F

.field private r:Lcom/android/launcher2/ct;

.field private s:Lcom/android/launcher2/ct;

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 59
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/FolderIcon;->g:Z

    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher2/FolderIcon;->c:Landroid/graphics/drawable/Drawable;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->n:I

    .line 99
    iput-boolean v5, p0, Lcom/android/launcher2/FolderIcon;->e:Z

    .line 100
    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    .line 101
    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    .line 733
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->t:I

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 104
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->n:I

    .line 99
    iput-boolean v5, p0, Lcom/android/launcher2/FolderIcon;->e:Z

    .line 100
    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    .line 101
    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    .line 733
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->t:I

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/FolderIcon;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static a(Lcom/android/launcher2/Launcher;Landroid/view/ViewGroup;Lcom/android/launcher2/cu;)Lcom/android/launcher2/FolderIcon;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 127
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030012

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 136
    const v1, 0x7f0d0026

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/BubbleTextView;

    iput-object v1, v0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    .line 137
    iget-object v1, v0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    iget-object v2, p2, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, v0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 139
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, v0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    const-string v3, "homescreen_icon_text_color"

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 141
    const v1, 0x7f0d0025

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    .line 143
    const-string v1, "NONE"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9b

    .line 144
    iget-object v1, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    :cond_4d
    :goto_4d
    const-string v1, "GRID"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e9

    .line 159
    iput v5, v0, Lcom/android/launcher2/FolderIcon;->t:I

    .line 163
    :cond_5b
    :goto_5b
    invoke-virtual {v0, p2}, Lcom/android/launcher2/FolderIcon;->setTag(Ljava/lang/Object;)V

    .line 164
    invoke-virtual {v0, p0}, Lcom/android/launcher2/FolderIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iput-object p2, v0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    .line 166
    iput-object p0, v0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    .line 167
    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->b()V

    .line 168
    const v1, 0x7f0702c3

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 169
    iget-object v3, p2, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    .line 168
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 170
    invoke-static {p0}, Lcom/android/launcher2/Folder;->a(Landroid/content/Context;)Lcom/android/launcher2/Folder;

    move-result-object v1

    .line 171
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->g()Lcom/android/launcher2/bk;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Folder;->setDragController(Lcom/android/launcher2/bk;)V

    .line 172
    invoke-virtual {v1, v0}, Lcom/android/launcher2/Folder;->setFolderIcon(Lcom/android/launcher2/FolderIcon;)V

    .line 173
    invoke-virtual {v1, p2}, Lcom/android/launcher2/Folder;->a(Lcom/android/launcher2/cu;)V

    .line 174
    iput-object v1, v0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    .line 176
    new-instance v1, Lcom/android/launcher2/co;

    invoke-direct {v1, p0, v0}, Lcom/android/launcher2/co;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/FolderIcon;)V

    iput-object v1, v0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    .line 177
    invoke-virtual {p2, v0}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/cv;)V

    .line 179
    return-object v0

    .line 145
    :cond_9b
    const-string v1, "SQUARE"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b0

    .line 146
    iget-object v1, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    const v2, 0x7f020074

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4d

    .line 147
    :cond_b0
    const-string v1, "IOS"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c5

    .line 148
    iget-object v1, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    const v2, 0x7f02006f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4d

    .line 149
    :cond_c5
    const-string v1, "CUSTOM"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_de

    .line 150
    invoke-static {p0}, Lcom/anddoes/launcher/v;->d(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_4d

    .line 152
    iget-object v2, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto/16 :goto_4d

    .line 155
    :cond_de
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, v0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    const-string v3, "folder_icon_inner_holo"

    invoke-virtual {v1, v2, v4, v3}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    goto/16 :goto_4d

    .line 160
    :cond_e9
    const-string v1, "IOS"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 161
    const/16 v1, 0x9

    iput v1, v0, Lcom/android/launcher2/FolderIcon;->t:I

    goto/16 :goto_5b
.end method

.method private a(ILcom/android/launcher2/ct;)Lcom/android/launcher2/ct;
    .registers 10
    .parameter
    .parameter

    .prologue
    const v4, 0x3e9eb852

    const/4 v0, 0x0

    const/4 v5, -0x1

    const/high16 v6, 0x3f80

    const/high16 v1, 0x4000

    .line 859
    const-string v2, "FAN"

    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7d

    .line 860
    const v4, 0x3f333333

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->l:I

    int-to-float v2, v2

    const v3, 0x3f333333

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/android/launcher2/FolderIcon;->m:I

    int-to-float v3, v3

    iget v6, p0, Lcom/android/launcher2/FolderIcon;->q:F

    div-float/2addr v6, v1

    sub-float/2addr v3, v6

    int-to-float v6, v2

    sub-float/2addr v3, v6

    div-float/2addr v3, v1

    if-nez p1, :cond_5b

    const v4, 0x3f4ccccd

    iget v0, p0, Lcom/android/launcher2/FolderIcon;->l:I

    int-to-float v0, v0

    const v2, 0x3f4ccccd

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->m:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/launcher2/FolderIcon;->q:F

    div-float/2addr v3, v1

    sub-float/2addr v2, v3

    int-to-float v3, v0

    sub-float/2addr v2, v3

    div-float/2addr v2, v1

    const/high16 v3, 0x4040

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/FolderIcon;->m:I

    int-to-float v3, v3

    iget v6, p0, Lcom/android/launcher2/FolderIcon;->q:F

    div-float/2addr v6, v1

    sub-float/2addr v3, v6

    int-to-float v0, v0

    sub-float v0, v3, v0

    div-float v3, v0, v1

    :goto_52
    if-nez p2, :cond_73

    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    .line 866
    :goto_5a
    return-object v0

    .line 860
    :cond_5b
    const/4 v6, 0x1

    if-ne p1, v6, :cond_61

    const/high16 v2, -0x3f40

    goto :goto_52

    :cond_61
    const/4 v6, 0x2

    if-ne p1, v6, :cond_143

    iget v0, p0, Lcom/android/launcher2/FolderIcon;->m:I

    int-to-float v0, v0

    iget v6, p0, Lcom/android/launcher2/FolderIcon;->q:F

    div-float v1, v6, v1

    sub-float/2addr v0, v1

    int-to-float v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x4040

    add-float v2, v0, v1

    goto :goto_52

    :cond_73
    iput v2, p2, Lcom/android/launcher2/ct;->a:F

    iput v3, p2, Lcom/android/launcher2/ct;->b:F

    iput v4, p2, Lcom/android/launcher2/ct;->c:F

    iput v5, p2, Lcom/android/launcher2/ct;->d:I

    :goto_7b
    move-object v0, p2

    goto :goto_5a

    .line 861
    :cond_7d
    const-string v2, "GRID"

    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_bd

    .line 862
    div-int/lit8 v2, p1, 0x2

    rem-int/lit8 v4, p1, 0x2

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v3

    if-eqz v3, :cond_b0

    :goto_95
    iget v1, p0, Lcom/android/launcher2/FolderIcon;->m:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float v3, v0, v1

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->m:I

    mul-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float v2, v0, v1

    if-nez p2, :cond_b2

    new-instance v0, Lcom/android/launcher2/ct;

    const/high16 v4, 0x3f00

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    goto :goto_5a

    :cond_b0
    move v0, v1

    goto :goto_95

    :cond_b2
    iput v2, p2, Lcom/android/launcher2/ct;->a:F

    iput v3, p2, Lcom/android/launcher2/ct;->b:F

    const/high16 v0, 0x3f00

    iput v0, p2, Lcom/android/launcher2/ct;->c:F

    iput v5, p2, Lcom/android/launcher2/ct;->d:I

    goto :goto_7b

    .line 863
    :cond_bd
    const-string v2, "IOS"

    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_fa

    .line 864
    div-int/lit8 v2, p1, 0x3

    rem-int/lit8 v6, p1, 0x3

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v3

    if-eqz v3, :cond_ef

    :goto_d5
    iget v1, p0, Lcom/android/launcher2/FolderIcon;->m:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    add-float v3, v0, v1

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->m:I

    mul-int/2addr v1, v6

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    add-float v2, v0, v1

    if-nez p2, :cond_f1

    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    goto/16 :goto_5a

    :cond_ef
    move v0, v1

    goto :goto_d5

    :cond_f1
    iput v2, p2, Lcom/android/launcher2/ct;->a:F

    iput v3, p2, Lcom/android/launcher2/ct;->b:F

    iput v4, p2, Lcom/android/launcher2/ct;->c:F

    iput v5, p2, Lcom/android/launcher2/ct;->d:I

    goto :goto_7b

    .line 866
    :cond_fa
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->t:I

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->t:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3eb33333

    sub-float v2, v6, v0

    mul-float/2addr v1, v2

    sub-float v1, v6, v1

    sub-float v2, v6, v0

    iget v3, p0, Lcom/android/launcher2/FolderIcon;->q:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/FolderIcon;->l:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v4, v6, v1

    iget v5, p0, Lcom/android/launcher2/FolderIcon;->l:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/android/launcher2/FolderIcon;->m:I

    int-to-float v5, v5

    add-float/2addr v3, v2

    add-float/2addr v3, v4

    sub-float v3, v5, v3

    add-float/2addr v2, v4

    iget v4, p0, Lcom/android/launcher2/FolderIcon;->k:F

    mul-float/2addr v4, v1

    const/high16 v1, 0x42a0

    sub-float v0, v6, v0

    mul-float/2addr v0, v1

    float-to-int v5, v0

    if-nez p2, :cond_139

    new-instance v0, Lcom/android/launcher2/ct;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ct;-><init>(Lcom/android/launcher2/FolderIcon;FFFI)V

    goto/16 :goto_5a

    :cond_139
    iput v2, p2, Lcom/android/launcher2/ct;->a:F

    iput v3, p2, Lcom/android/launcher2/ct;->b:F

    iput v4, p2, Lcom/android/launcher2/ct;->c:F

    iput v5, p2, Lcom/android/launcher2/ct;->d:I

    goto/16 :goto_7b

    :cond_143
    move v2, v0

    goto/16 :goto_52
.end method

.method private a(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 491
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->j:I

    if-ne v0, p1, :cond_8

    iget v0, p0, Lcom/android/launcher2/FolderIcon;->n:I

    if-eq v0, p2, :cond_4c

    .line 492
    :cond_8
    iput p1, p0, Lcom/android/launcher2/FolderIcon;->j:I

    .line 493
    iput p2, p0, Lcom/android/launcher2/FolderIcon;->n:I

    .line 495
    sget v0, Lcom/android/launcher2/co;->j:I

    .line 496
    sget v1, Lcom/android/launcher2/co;->k:I

    .line 498
    mul-int/lit8 v2, v1, 0x2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->m:I

    .line 500
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->m:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    const v2, 0x3fe66666

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 502
    iget v2, p0, Lcom/android/launcher2/FolderIcon;->j:I

    int-to-float v2, v2

    const v3, 0x3f9eb852

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 503
    const/high16 v3, 0x3f80

    int-to-float v0, v0

    mul-float/2addr v0, v3

    int-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->k:F

    .line 505
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->j:I

    int-to-float v0, v0

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->k:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->l:I

    .line 506
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->l:I

    int-to-float v0, v0

    const v2, 0x3e75c28f

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->q:F

    .line 508
    iget v0, p0, Lcom/android/launcher2/FolderIcon;->n:I

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->m:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/launcher2/FolderIcon;->o:I

    .line 509
    iput v1, p0, Lcom/android/launcher2/FolderIcon;->p:I

    .line 511
    :cond_4c
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Lcom/android/launcher2/ct;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 573
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 574
    iget v0, p2, Lcom/android/launcher2/ct;->a:F

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->o:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p2, Lcom/android/launcher2/ct;->b:F

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->p:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 575
    iget v0, p2, Lcom/android/launcher2/ct;->c:F

    iget v1, p2, Lcom/android/launcher2/ct;->c:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 576
    iget-object v0, p2, Lcom/android/launcher2/ct;->e:Landroid/graphics/drawable/Drawable;

    .line 578
    if-eqz v0, :cond_42

    .line 579
    iget v1, p0, Lcom/android/launcher2/FolderIcon;->j:I

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->j:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 580
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 581
    iget v1, p2, Lcom/android/launcher2/ct;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_39

    .line 582
    iget v1, p2, Lcom/android/launcher2/ct;->d:I

    invoke-static {v1, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 584
    :cond_39
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 585
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 586
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 588
    :cond_42
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 589
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter

    .prologue
    .line 514
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/FolderIcon;->a(II)V

    .line 515
    return-void
.end method

.method private a(Lcom/android/launcher2/jd;Lcom/android/launcher2/bu;Landroid/graphics/Rect;FILjava/lang/Runnable;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 402
    const/4 v1, -0x1

    iput v1, p1, Lcom/android/launcher2/jd;->l:I

    .line 403
    const/4 v1, -0x1

    iput v1, p1, Lcom/android/launcher2/jd;->m:I

    .line 408
    if-eqz p2, :cond_11f

    .line 409
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    .line 410
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 411
    move-object/from16 v0, p2

    invoke-virtual {v1, v0, v3}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 413
    if-nez p3, :cond_123

    .line 414
    new-instance p3, Landroid/graphics/Rect;

    invoke-direct/range {p3 .. p3}, Landroid/graphics/Rect;-><init>()V

    .line 415
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v5

    .line 416
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    .line 417
    if-eqz v5, :cond_39

    instance-of v2, v4, Lcom/android/launcher2/CellLayout;

    if-eqz v2, :cond_39

    move-object v2, v4

    .line 419
    check-cast v2, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5, v2}, Lcom/android/launcher2/Workspace;->setFinalTransitionTransform(Lcom/android/launcher2/CellLayout;)V

    .line 421
    :cond_39
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getScaleX()F

    move-result v2

    .line 422
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getScaleY()F

    move-result v6

    .line 423
    const/high16 v7, 0x3f80

    invoke-virtual {p0, v7}, Lcom/android/launcher2/FolderIcon;->setScaleX(F)V

    .line 424
    const/high16 v7, 0x3f80

    invoke-virtual {p0, v7}, Lcom/android/launcher2/FolderIcon;->setScaleY(F)V

    .line 425
    move-object/from16 v0, p3

    invoke-virtual {v1, p0, v0}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result p4

    .line 427
    invoke-virtual {p0, v2}, Lcom/android/launcher2/FolderIcon;->setScaleX(F)V

    .line 428
    invoke-virtual {p0, v6}, Lcom/android/launcher2/FolderIcon;->setScaleY(F)V

    .line 429
    if-eqz v5, :cond_62

    instance-of v2, v4, Lcom/android/launcher2/CellLayout;

    if-eqz v2, :cond_62

    .line 430
    check-cast v4, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5, v4}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;)V

    :cond_62
    move-object/from16 v4, p3

    .line 434
    :goto_64
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 435
    iget v5, p0, Lcom/android/launcher2/FolderIcon;->t:I

    move/from16 v0, p5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget-object v6, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    invoke-direct {p0, v5, v6}, Lcom/android/launcher2/FolderIcon;->a(ILcom/android/launcher2/ct;)Lcom/android/launcher2/ct;

    move-result-object v5

    iput-object v5, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget-object v5, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v6, v5, Lcom/android/launcher2/ct;->a:F

    iget v7, p0, Lcom/android/launcher2/FolderIcon;->o:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Lcom/android/launcher2/ct;->a:F

    iget-object v5, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v6, v5, Lcom/android/launcher2/ct;->b:F

    iget v7, p0, Lcom/android/launcher2/FolderIcon;->p:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Lcom/android/launcher2/ct;->b:F

    iget-object v5, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v5, v5, Lcom/android/launcher2/ct;->a:F

    iget-object v6, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v6, v6, Lcom/android/launcher2/ct;->c:F

    iget v7, p0, Lcom/android/launcher2/FolderIcon;->j:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    const/high16 v7, 0x4000

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v6, v6, Lcom/android/launcher2/ct;->b:F

    iget-object v7, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v7, v7, Lcom/android/launcher2/ct;->c:F

    iget v8, p0, Lcom/android/launcher2/FolderIcon;->j:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    const/high16 v8, 0x4000

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    const/4 v7, 0x0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    aput v5, v2, v7

    const/4 v5, 0x1

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    aput v6, v2, v5

    iget-object v5, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iget v6, v5, Lcom/android/launcher2/ct;->c:F

    .line 436
    const/4 v5, 0x0

    const/4 v7, 0x0

    aget v7, v2, v7

    int-to-float v7, v7

    mul-float v7, v7, p4

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    aput v7, v2, v5

    .line 437
    const/4 v5, 0x1

    const/4 v7, 0x1

    aget v7, v2, v7

    int-to-float v7, v7

    mul-float v7, v7, p4

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    aput v7, v2, v5

    .line 439
    const/4 v5, 0x0

    aget v5, v2, v5

    invoke-virtual/range {p2 .. p2}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v5, v7

    .line 440
    const/4 v7, 0x1

    aget v2, v2, v7

    invoke-virtual/range {p2 .. p2}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v2, v7

    .line 439
    invoke-virtual {v4, v5, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 442
    iget v2, p0, Lcom/android/launcher2/FolderIcon;->t:I

    move/from16 v0, p5

    if-ge v0, v2, :cond_11d

    const/high16 v5, 0x3f00

    .line 444
    :goto_f6
    mul-float v6, v6, p4

    .line 446
    const/16 v8, 0x190

    .line 447
    new-instance v9, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x4000

    invoke-direct {v9, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x4000

    invoke-direct {v10, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 448
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v2, p2

    move v7, v6

    move-object/from16 v11, p6

    .line 445
    invoke-virtual/range {v1 .. v13}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 449
    new-instance v1, Lcom/android/launcher2/cl;

    invoke-direct {v1, p0, p1}, Lcom/android/launcher2/cl;-><init>(Lcom/android/launcher2/FolderIcon;Lcom/android/launcher2/jd;)V

    .line 453
    const-wide/16 v2, 0x190

    .line 449
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/launcher2/FolderIcon;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 457
    :goto_11c
    return-void

    .line 442
    :cond_11d
    const/4 v5, 0x0

    goto :goto_f6

    .line 455
    :cond_11f
    invoke-virtual {p0, p1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/di;)V

    goto :goto_11c

    :cond_123
    move-object/from16 v4, p3

    goto/16 :goto_64
.end method

.method static synthetic b(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/BubbleTextView;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/ct;
    .registers 2
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    return-object v0
.end method

.method static synthetic c()Z
    .registers 1

    .prologue
    .line 59
    sget-boolean v0, Lcom/android/launcher2/FolderIcon;->g:Z

    return v0
.end method

.method static synthetic d()V
    .registers 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/FolderIcon;->g:Z

    return-void
.end method

.method private d(Lcom/android/launcher2/di;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 340
    iget v1, p1, Lcom/android/launcher2/di;->i:I

    .line 341
    if-eqz v1, :cond_1a

    .line 342
    if-eq v1, v0, :cond_1a

    .line 343
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2a

    .line 344
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->ai:Z

    if-eqz v1, :cond_2a

    .line 345
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 346
    :cond_1a
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-static {}, Lcom/android/launcher2/Folder;->j()Z

    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    if-eq p1, v1, :cond_2a

    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-boolean v1, v1, Lcom/android/launcher2/cu;->a:Z

    if-nez v1, :cond_2a

    :goto_29
    return v0

    :cond_2a
    const/4 v0, 0x0

    .line 341
    goto :goto_29
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    invoke-virtual {v0}, Lcom/android/launcher2/co;->c()V

    .line 393
    return-void
.end method

.method public final a(Lcom/android/launcher2/bz;)V
    .registers 9
    .parameter

    .prologue
    .line 461
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_3e

    .line 462
    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/android/launcher2/bz;->k:Z

    .line 463
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ai:Z

    if-eqz v0, :cond_33

    .line 464
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 465
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/cu;

    .line 466
    iget-object v1, v0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_34

    .line 469
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/cu;)V

    .line 470
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 484
    :cond_33
    :goto_33
    return-void

    .line 466
    :cond_34
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    .line 467
    invoke-virtual {p0, v1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/di;)V

    goto :goto_23

    .line 476
    :cond_3e
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/h;

    if-eqz v0, :cond_65

    .line 478
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/h;

    invoke-virtual {v0}, Lcom/android/launcher2/h;->a()Lcom/android/launcher2/jd;

    move-result-object v1

    .line 482
    :goto_4c
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->i()V

    .line 483
    iget-object v2, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/4 v3, 0x0

    const/high16 v4, 0x3f80

    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p1, Lcom/android/launcher2/bz;->i:Ljava/lang/Runnable;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/bu;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    goto :goto_33

    .line 480
    :cond_65
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/jd;

    move-object v1, v0

    goto :goto_4c
.end method

.method public final a(Lcom/android/launcher2/di;)V
    .registers 2
    .parameter

    .prologue
    .line 692
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->invalidate()V

    .line 693
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->requestLayout()V

    .line 694
    return-void
.end method

.method public final a(Lcom/android/launcher2/jd;Landroid/view/View;Lcom/android/launcher2/jd;Lcom/android/launcher2/bu;Landroid/graphics/Rect;FLjava/lang/Runnable;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 379
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v7, v0, v5

    .line 380
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/FolderIcon;->a(II)V

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move-object v6, p7

    .line 383
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/bu;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    .line 387
    invoke-direct {p0, v7}, Lcom/android/launcher2/FolderIcon;->a(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/FolderIcon;->a(ILcom/android/launcher2/ct;)Lcom/android/launcher2/ct;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->m:I

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/launcher2/FolderIcon;->m:I

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    iput-object v7, v3, Lcom/android/launcher2/ct;->e:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_66

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    new-instance v4, Lcom/android/launcher2/cm;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/android/launcher2/cm;-><init>(Lcom/android/launcher2/FolderIcon;FLcom/android/launcher2/ct;F)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/android/launcher2/cn;

    invoke-direct {v0, p0}, Lcom/android/launcher2/cn;-><init>(Lcom/android/launcher2/FolderIcon;)V

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v0, 0x15e

    invoke-virtual {v3, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 388
    invoke-virtual {p0, p1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/di;)V

    .line 389
    return-void

    .line 387
    nop

    :array_66
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter

    .prologue
    .line 702
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 703
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0702c3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 704
    aput-object p1, v1, v2

    .line 703
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 705
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 350
    check-cast p1, Lcom/android/launcher2/di;

    .line 351
    invoke-direct {p0, p1}, Lcom/android/launcher2/FolderIcon;->d(Lcom/android/launcher2/di;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 736
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 737
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6b

    .line 738
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 739
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_63

    .line 740
    const v0, 0x7f0b0032

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 739
    :goto_24
    invoke-virtual {v2, v0}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 742
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v2}, Lcom/android/launcher2/BubbleTextView;->getPaddingLeft()I

    move-result v2

    .line 743
    const v3, 0x7f0b0045

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 744
    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v3}, Lcom/android/launcher2/BubbleTextView;->getPaddingRight()I

    move-result v3

    .line 745
    iget-object v4, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v4}, Lcom/android/launcher2/BubbleTextView;->getPaddingBottom()I

    move-result v4

    .line 742
    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/android/launcher2/BubbleTextView;->setPadding(IIII)V

    .line 746
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    .line 747
    new-instance v1, Lcom/android/launcher2/ca;

    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v2, v2, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 746
    invoke-virtual {v0, v6, v1, v6, v6}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 748
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 749
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 750
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v5}, Lcom/android/launcher2/BubbleTextView;->setVisibility(I)V

    .line 766
    :goto_62
    return-void

    .line 741
    :cond_63
    const v0, 0x7f0b0031

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_24

    .line 752
    :cond_6b
    const-string v0, "NONE"

    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 753
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 757
    :goto_7f
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v5}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 758
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v2}, Lcom/android/launcher2/BubbleTextView;->getPaddingLeft()I

    move-result v2

    .line 759
    const v3, 0x7f0b002e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 760
    iget-object v4, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v4}, Lcom/android/launcher2/BubbleTextView;->getPaddingRight()I

    move-result v4

    .line 761
    iget-object v5, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v5}, Lcom/android/launcher2/BubbleTextView;->getPaddingBottom()I

    move-result v5

    .line 758
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/launcher2/BubbleTextView;->setPadding(IIII)V

    .line 762
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v6, v6, v6, v6}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 763
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 764
    const v2, 0x7f0b0028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto :goto_62

    .line 755
    :cond_b9
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_7f
.end method

.method public final b(Lcom/android/launcher2/di;)V
    .registers 2
    .parameter

    .prologue
    .line 697
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->invalidate()V

    .line 698
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->requestLayout()V

    .line 699
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 360
    check-cast p1, Lcom/android/launcher2/di;

    invoke-direct {p0, p1}, Lcom/android/launcher2/FolderIcon;->d(Lcom/android/launcher2/di;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 370
    :goto_9
    return-void

    .line 361
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_15

    .line 362
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 364
    :cond_15
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 365
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    .line 366
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    iget v3, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    invoke-virtual {v2, v3, v0}, Lcom/android/launcher2/co;->a(II)V

    .line 367
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/co;->a(Lcom/android/launcher2/CellLayout;)V

    .line 368
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    invoke-virtual {v0}, Lcom/android/launcher2/co;->b()V

    .line 369
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->d:Lcom/android/launcher2/co;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/co;)V

    goto :goto_9
.end method

.method public final c(Lcom/android/launcher2/di;)V
    .registers 9
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/di;)V

    .line 356
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-wide v2, v1, Lcom/android/launcher2/cu;->h:J

    const/4 v4, 0x0

    iget v5, p1, Lcom/android/launcher2/di;->l:I

    iget v6, p1, Lcom/android/launcher2/di;->m:I

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    .line 357
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 593
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 595
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    if-nez v0, :cond_a

    .line 626
    :cond_9
    :goto_9
    return-void

    .line 596
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/android/launcher2/FolderIcon;->e:Z

    if-eqz v0, :cond_9

    .line 598
    :cond_16
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Folder;->b(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 603
    iget-boolean v0, p0, Lcom/android/launcher2/FolderIcon;->e:Z

    if-eqz v0, :cond_61

    .line 604
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    iget-object v0, v0, Lcom/android/launcher2/ct;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/android/launcher2/FolderIcon;->a(Landroid/graphics/drawable/Drawable;)V

    .line 611
    :goto_27
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/FolderIcon;->t:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 612
    iget-boolean v1, p0, Lcom/android/launcher2/FolderIcon;->e:Z

    if-nez v1, :cond_71

    .line 613
    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v1, v1, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-nez v1, :cond_9

    .line 614
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3e
    if-ltz v1, :cond_9

    .line 615
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 616
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v4

    .line 618
    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    invoke-direct {p0, v1, v3}, Lcom/android/launcher2/FolderIcon;->a(ILcom/android/launcher2/ct;)Lcom/android/launcher2/ct;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    .line 619
    iget-object v3, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    iput-object v0, v3, Lcom/android/launcher2/ct;->e:Landroid/graphics/drawable/Drawable;

    .line 620
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->r:Lcom/android/launcher2/ct;

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/FolderIcon;->a(Landroid/graphics/Canvas;Lcom/android/launcher2/ct;)V

    .line 614
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3e

    .line 606
    :cond_61
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 607
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v4

    .line 608
    invoke-direct {p0, v0}, Lcom/android/launcher2/FolderIcon;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_27

    .line 624
    :cond_71
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->s:Lcom/android/launcher2/ct;

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/FolderIcon;->a(Landroid/graphics/Canvas;Lcom/android/launcher2/ct;)V

    goto :goto_9
.end method

.method public getBackgroundType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 769
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->f:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderName()Lcom/android/launcher2/BubbleTextView;
    .registers 2

    .prologue
    .line 777
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    return-object v0
.end method

.method public getPreviewBackground()Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getTextVisible()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 679
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v2, v2, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_17

    .line 680
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v2}, Lcom/android/launcher2/BubbleTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_15

    .line 682
    :cond_14
    :goto_14
    return v0

    :cond_15
    move v0, v1

    .line 680
    goto :goto_14

    .line 682
    :cond_17
    iget-object v2, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v2}, Lcom/android/launcher2/BubbleTextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_14

    move v0, v1

    goto :goto_14
.end method

.method public final k()V
    .registers 1

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->invalidate()V

    .line 688
    invoke-virtual {p0}, Lcom/android/launcher2/FolderIcon;->requestLayout()V

    .line 689
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 2

    .prologue
    .line 184
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/FolderIcon;->g:Z

    .line 185
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setTextVisible(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 663
    if-eqz p1, :cond_18

    .line 664
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setVisibility(I)V

    .line 665
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_17

    .line 666
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    iget-object v1, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v1, v1, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    :cond_17
    :goto_17
    return-void

    .line 669
    :cond_18
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2b

    .line 670
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17

    .line 673
    :cond_2b
    iget-object v0, p0, Lcom/android/launcher2/FolderIcon;->i:Lcom/android/launcher2/BubbleTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setVisibility(I)V

    goto :goto_17
.end method
