.class public Lcom/android/launcher2/HolographicLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private final a:Lcom/android/launcher2/cy;

.field private b:Landroid/widget/ImageView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/HolographicLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/HolographicLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    sget-object v0, Lcom/anddoes/launcher/at;->HolographicLinearLayout:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/HolographicLinearLayout;->c:I

    .line 50
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 52
    invoke-virtual {p0, v2}, Lcom/android/launcher2/HolographicLinearLayout;->setWillNotDraw(Z)V

    .line 53
    new-instance v0, Lcom/android/launcher2/cy;

    invoke-direct {v0, p1}, Lcom/android/launcher2/cy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->a:Lcom/android/launcher2/cy;

    .line 54
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->a:Lcom/android/launcher2/cy;

    iget-object v1, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/cy;->b(Landroid/widget/ImageView;)V

    .line 71
    invoke-virtual {p0}, Lcom/android/launcher2/HolographicLinearLayout;->invalidate()V

    .line 72
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 3

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 60
    iget-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_1a

    .line 61
    iget-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 62
    instance-of v1, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v1, :cond_1a

    .line 63
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 64
    invoke-virtual {p0}, Lcom/android/launcher2/HolographicLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 67
    :cond_1a
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 80
    iget-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    if-nez v0, :cond_11

    .line 81
    iget v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->c:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HolographicLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    .line 83
    :cond_11
    iget-object v0, p0, Lcom/android/launcher2/HolographicLinearLayout;->a:Lcom/android/launcher2/cy;

    iget-object v1, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/cy;->a(Landroid/widget/ImageView;)V

    .line 84
    return-void
.end method

.method public setImageView(Landroid/widget/ImageView;)V
    .registers 2
    .parameter

    .prologue
    .line 88
    if-eqz p1, :cond_7

    .line 89
    iput-object p1, p0, Lcom/android/launcher2/HolographicLinearLayout;->b:Landroid/widget/ImageView;

    .line 90
    invoke-virtual {p0}, Lcom/android/launcher2/HolographicLinearLayout;->a()V

    .line 92
    :cond_7
    return-void
.end method
