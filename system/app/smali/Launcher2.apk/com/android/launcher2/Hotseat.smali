.class public Lcom/android/launcher2/Hotseat;
.super Lcom/android/launcher2/PagedView;
.source "SourceFile"


# instance fields
.field public a:Landroid/view/View;

.field private b:Lcom/android/launcher2/Launcher;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Landroid/view/ViewGroup$LayoutParams;

.field private g:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/Hotseat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/Hotseat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 57
    const/4 v3, 0x2

    if-ne v0, v3, :cond_27

    move v0, v1

    .line 56
    :goto_13
    iput-boolean v0, p0, Lcom/android/launcher2/Hotseat;->e:Z

    .line 58
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->e:Z

    if-eqz v0, :cond_29

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_29

    :goto_1f
    iput-boolean v1, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    .line 59
    const v0, 0x7f0d0039

    iput v0, p0, Lcom/android/launcher2/Hotseat;->aw:I

    .line 60
    return-void

    :cond_27
    move v0, v2

    .line 57
    goto :goto_13

    :cond_29
    move v1, v2

    .line 58
    goto :goto_1f
.end method

.method public static c()Z
    .registers 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method final a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    sub-int/2addr v0, p2

    add-int/lit8 p1, v0, -0x1

    :cond_f
    return p1
.end method

.method final a(Lcom/android/launcher2/CellLayout;II)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Hotseat;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 232
    if-ltz v0, :cond_16

    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_16

    .line 233
    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aget v0, v1, v0

    invoke-virtual {p0, p2, p3}, Lcom/android/launcher2/Hotseat;->a(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final a(IZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 325
    return-void
.end method

.method protected final a(Landroid/view/MotionEvent;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f80

    const/4 v3, 0x0

    const v4, 0x3f060a92

    .line 276
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 278
    iget v2, p0, Lcom/android/launcher2/Hotseat;->y:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 279
    iget v0, p0, Lcom/android/launcher2/Hotseat;->A:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 281
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_29

    .line 282
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_2f

    .line 317
    :cond_28
    :goto_28
    return-void

    .line 286
    :cond_29
    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_28

    .line 291
    :cond_2f
    div-float v0, v1, v2

    .line 292
    iget-boolean v3, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v3, :cond_37

    .line 293
    div-float v0, v2, v1

    .line 295
    :cond_37
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 297
    const v1, 0x3f860a92

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_28

    .line 299
    cmpl-float v1, v0, v4

    if-lez v1, :cond_58

    .line 307
    sub-float/2addr v0, v4

    .line 309
    div-float/2addr v0, v4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 310
    const/high16 v1, 0x4080

    .line 311
    mul-float/2addr v0, v1

    add-float/2addr v0, v5

    .line 310
    invoke-super {p0, p1, v0, v6}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;FZ)V

    goto :goto_28

    .line 315
    :cond_58
    invoke-super {p0, p1, v5, v6}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;FZ)V

    goto :goto_28
.end method

.method final a(Landroid/view/View;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 265
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v2

    move v1, v0

    .line 266
    :goto_6
    if-lt v1, v2, :cond_9

    .line 271
    :goto_8
    return v0

    .line 267
    :cond_9
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ne v3, p1, :cond_11

    .line 268
    const/4 v0, 0x1

    goto :goto_8

    .line 266
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method final d()V
    .registers 4

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v2

    .line 217
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-lt v1, v2, :cond_9

    .line 221
    return-void

    .line 218
    :cond_9
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 219
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->removeAllViewsInLayout()V

    .line 217
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6
.end method

.method public final e()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 339
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ac:Z

    iput-boolean v0, p0, Lcom/android/launcher2/Hotseat;->aj:Z

    .line 340
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->aj:Z

    if-eqz v0, :cond_4c

    .line 341
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    if-eqz v0, :cond_4c

    .line 342
    const-string v0, "AUTO"

    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 343
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_2c

    .line 344
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    const-string v1, "BOTTOM"

    iput-object v1, v0, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    .line 346
    :cond_2c
    const-string v0, "TOP"

    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 347
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 350
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 351
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 352
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 353
    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    :cond_4c
    :goto_4c
    return-void

    .line 354
    :cond_4d
    const-string v0, "BOTTOM"

    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 355
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 358
    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 359
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 360
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 361
    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4c

    .line 363
    :cond_6e
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->f:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4c
.end method

.method protected final e_()V
    .registers 2

    .prologue
    .line 372
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->e_()V

    .line 374
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->d(Z)V

    .line 375
    return-void
.end method

.method public final f_()V
    .registers 1

    .prologue
    .line 321
    return-void
.end method

.method final g(I)I
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_5

    const/4 p1, 0x0

    :cond_5
    return p1
.end method

.method protected final g()V
    .registers 2

    .prologue
    .line 378
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->g()V

    .line 379
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->e(Z)V

    .line 380
    return-void
.end method

.method getLayout()Lcom/android/launcher2/CellLayout;
    .registers 3

    .prologue
    .line 138
    iget v0, p0, Lcom/android/launcher2/Hotseat;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_f

    iget v0, p0, Lcom/android/launcher2/Hotseat;->v:I

    .line 139
    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    return-object v0

    .line 138
    :cond_f
    iget v0, p0, Lcom/android/launcher2/Hotseat;->u:I

    goto :goto_8
.end method

.method final h(I)I
    .registers 4
    .parameter

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    sub-int/2addr v0, v1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method final i(I)I
    .registers 3
    .parameter

    .prologue
    .line 224
    if-ltz p1, :cond_d

    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 225
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aget v0, v0, p1

    .line 227
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method final j(I)Lcom/android/launcher2/CellLayout;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 239
    move v0, v1

    .line 241
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v2

    if-lt v0, v2, :cond_c

    .line 249
    :goto_8
    if-nez v1, :cond_1f

    .line 250
    const/4 v0, 0x0

    .line 252
    :goto_b
    return-object v0

    .line 242
    :cond_c
    iget-object v2, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aget v2, v2, v0

    if-lt p1, v2, :cond_1c

    iget-object v2, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aget v2, v2, v0

    add-int/lit8 v2, v2, 0x64

    if-ge p1, v2, :cond_1c

    .line 243
    const/4 v1, 0x1

    .line 244
    goto :goto_8

    .line 246
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 252
    :cond_1f
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    goto :goto_b
.end method

.method final k(I)Lcom/android/launcher2/CellLayout;
    .registers 4
    .parameter

    .prologue
    .line 256
    iget v0, p0, Lcom/android/launcher2/Hotseat;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_1c

    iget v0, p0, Lcom/android/launcher2/Hotseat;->v:I

    move v1, v0

    .line 257
    :goto_9
    if-nez p1, :cond_20

    const/4 v0, -0x1

    :goto_c
    add-int/2addr v0, v1

    .line 258
    if-ltz v0, :cond_22

    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_22

    .line 259
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 261
    :goto_1b
    return-object v0

    .line 256
    :cond_1c
    iget v0, p0, Lcom/android/launcher2/Hotseat;->u:I

    move v1, v0

    goto :goto_9

    .line 257
    :cond_20
    const/4 v0, 0x1

    goto :goto_c

    .line 261
    :cond_22
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method protected onMeasure(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/PagedView;->onMeasure(II)V

    .line 331
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->e:Z

    if-eqz v0, :cond_13

    .line 332
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Hotseat;->av:I

    .line 334
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget v0, v0, Lcom/android/launcher2/Workspace;->ah:I

    iput v0, p0, Lcom/android/launcher2/Hotseat;->ah:I

    .line 335
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget v0, v0, Lcom/android/launcher2/Workspace;->ai:I

    iput v0, p0, Lcom/android/launcher2/Hotseat;->ai:I

    .line 336
    return-void
.end method

.method public setup(Lcom/android/launcher2/Launcher;)V
    .registers 11
    .parameter

    .prologue
    const/16 v4, 0x3e8

    const/4 v5, 0x2

    const/16 v1, 0x64

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    iput-object p1, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    .line 64
    new-instance v0, Lcom/android/launcher2/cz;

    invoke-direct {v0}, Lcom/android/launcher2/cz;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 65
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->Z:Z

    iput-boolean v0, p0, Lcom/android/launcher2/Hotseat;->aq:Z

    .line 66
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aa:Z

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->setElasticScrolling(Z)V

    .line 67
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->ab:I

    iput v0, p0, Lcom/android/launcher2/Hotseat;->at:I

    .line 68
    iput-boolean v2, p0, Lcom/android/launcher2/Hotseat;->ab:Z

    .line 69
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->q()V

    .line 71
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    .line 72
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_94

    .line 73
    iput v3, p0, Lcom/android/launcher2/Hotseat;->c:I

    .line 74
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->U:I

    iput v0, p0, Lcom/android/launcher2/Hotseat;->d:I

    .line 79
    :goto_43
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->removeAllViews()V

    .line 81
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->T:I

    packed-switch v0, :pswitch_data_184

    move v5, v2

    :goto_50
    move v1, v2

    .line 118
    :goto_51
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->T:I

    if-lt v1, v0, :cond_162

    .line 125
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget v1, p0, Lcom/android/launcher2/Hotseat;->aw:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    if-eqz v0, :cond_7a

    .line 127
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->f:Landroid/view/ViewGroup$LayoutParams;

    .line 128
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    const-string v4, "scroll_indicator_holo"

    invoke-virtual {v0, v1, v2, v4}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 130
    :cond_7a
    invoke-virtual {p0}, Lcom/android/launcher2/Hotseat;->e()V

    .line 131
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->T:I

    if-le v0, v3, :cond_180

    :goto_85
    iput-boolean v3, p0, Lcom/android/launcher2/Hotseat;->ao:Z

    .line 132
    iput v5, p0, Lcom/android/launcher2/Hotseat;->al:I

    .line 133
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Hotseat;->setCurrentPage(I)V

    .line 134
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 135
    return-void

    .line 76
    :cond_94
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->U:I

    iput v0, p0, Lcom/android/launcher2/Hotseat;->c:I

    .line 77
    iput v3, p0, Lcom/android/launcher2/Hotseat;->d:I

    goto :goto_43

    .line 83
    :pswitch_9f
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    .line 84
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aput v2, v0, v2

    move v5, v2

    .line 86
    goto :goto_50

    .line 88
    :pswitch_a9
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    .line 89
    iget-object v4, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_c6

    move v0, v1

    :goto_b4
    aput v0, v4, v2

    .line 90
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v4, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v4, :cond_bd

    move v1, v2

    :cond_bd
    aput v1, v0, v3

    .line 91
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_c8

    move v0, v3

    :goto_c4
    move v5, v0

    .line 92
    goto :goto_50

    :cond_c6
    move v0, v2

    .line 89
    goto :goto_b4

    :cond_c8
    move v0, v2

    .line 91
    goto :goto_c4

    .line 94
    :pswitch_ca
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    .line 95
    iget-object v7, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_e7

    move v0, v1

    :goto_d6
    aput v0, v7, v2

    .line 96
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aput v2, v0, v3

    .line 97
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v7, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v7, :cond_e9

    :goto_e2
    aput v4, v0, v5

    move v5, v3

    .line 99
    goto/16 :goto_50

    :cond_e7
    move v0, v4

    .line 95
    goto :goto_d6

    :cond_e9
    move v4, v1

    .line 97
    goto :goto_e2

    .line 101
    :pswitch_eb
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    .line 102
    iget-object v7, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_11d

    const/16 v0, 0xc8

    :goto_f8
    aput v0, v7, v2

    .line 103
    iget-object v7, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_11f

    move v0, v1

    :goto_101
    aput v0, v7, v3

    .line 104
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v7, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v7, :cond_10a

    move v1, v2

    :cond_10a
    aput v1, v0, v5

    .line 105
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    const/4 v1, 0x3

    iget-boolean v7, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v7, :cond_121

    :goto_113
    aput v4, v0, v1

    .line 106
    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_124

    move v0, v5

    :goto_11a
    move v5, v0

    .line 107
    goto/16 :goto_50

    :cond_11d
    move v0, v4

    .line 102
    goto :goto_f8

    :cond_11f
    move v0, v2

    .line 103
    goto :goto_101

    .line 105
    :cond_121
    const/16 v4, 0xc8

    goto :goto_113

    :cond_124
    move v0, v3

    .line 106
    goto :goto_11a

    .line 109
    :pswitch_126
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    .line 110
    iget-object v7, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_158

    const/16 v0, 0xc8

    :goto_133
    aput v0, v7, v2

    .line 111
    iget-object v7, p0, Lcom/android/launcher2/Hotseat;->g:[I

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_15b

    move v0, v1

    :goto_13c
    aput v0, v7, v3

    .line 112
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    aput v2, v0, v5

    .line 113
    iget-object v0, p0, Lcom/android/launcher2/Hotseat;->g:[I

    const/4 v7, 0x3

    iget-boolean v8, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v8, :cond_15d

    :goto_149
    aput v4, v0, v7

    .line 114
    iget-object v1, p0, Lcom/android/launcher2/Hotseat;->g:[I

    const/4 v4, 0x4

    iget-boolean v0, p0, Lcom/android/launcher2/Hotseat;->ap:Z

    if-eqz v0, :cond_15f

    const/16 v0, 0x44c

    :goto_154
    aput v0, v1, v4

    goto/16 :goto_50

    .line 110
    :cond_158
    const/16 v0, 0x44c

    goto :goto_133

    :cond_15b
    move v0, v4

    .line 111
    goto :goto_13c

    :cond_15d
    move v4, v1

    .line 113
    goto :goto_149

    .line 114
    :cond_15f
    const/16 v0, 0xc8

    goto :goto_154

    .line 119
    :cond_162
    const v0, 0x7f030017

    invoke-virtual {v6, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 120
    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setMargins(Z)V

    .line 121
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setIsHotseat(Z)V

    .line 122
    iget v4, p0, Lcom/android/launcher2/Hotseat;->c:I

    iget v7, p0, Lcom/android/launcher2/Hotseat;->d:I

    invoke-virtual {v0, v4, v7}, Lcom/android/launcher2/CellLayout;->a(II)V

    .line 123
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Hotseat;->addView(Landroid/view/View;)V

    .line 118
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_51

    :cond_180
    move v3, v2

    .line 131
    goto/16 :goto_85

    .line 81
    nop

    :pswitch_data_184
    .packed-switch 0x1
        :pswitch_9f
        :pswitch_a9
        :pswitch_ca
        :pswitch_eb
        :pswitch_126
    .end packed-switch
.end method
