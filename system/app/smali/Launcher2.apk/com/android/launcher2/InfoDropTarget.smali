.class public Lcom/android/launcher2/InfoDropTarget;
.super Lcom/android/launcher2/ak;
.source "SourceFile"


# instance fields
.field private f:Landroid/content/res/ColorStateList;

.field private g:Landroid/graphics/drawable/TransitionDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/InfoDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/ak;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/android/launcher2/ak;->a()V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/InfoDropTarget;->d:Z

    .line 110
    return-void
.end method

.method public final a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 93
    const/4 v0, 0x1

    .line 96
    instance-of v1, p1, Lcom/android/launcher2/AppsCustomizePagedView;

    if-nez v1, :cond_22

    move v1, v2

    .line 100
    :goto_7
    iput-boolean v1, p0, Lcom/android/launcher2/InfoDropTarget;->d:Z

    .line 101
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->g:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 102
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/InfoDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 103
    invoke-virtual {p0}, Lcom/android/launcher2/InfoDropTarget;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1f

    :goto_1b
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 104
    return-void

    .line 103
    :cond_1f
    const/16 v2, 0x8

    goto :goto_1b

    :cond_22
    move v1, v0

    goto :goto_7
.end method

.method public final b(Lcom/android/launcher2/bz;)V
    .registers 4
    .parameter

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/android/launcher2/ak;->b(Lcom/android/launcher2/bz;)V

    .line 115
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->g:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/android/launcher2/InfoDropTarget;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 116
    iget v0, p0, Lcom/android/launcher2/InfoDropTarget;->e:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/InfoDropTarget;->setTextColor(I)V

    .line 117
    return-void
.end method

.method public final d(Lcom/android/launcher2/bz;)V
    .registers 3
    .parameter

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/android/launcher2/ak;->d(Lcom/android/launcher2/bz;)V

    .line 122
    iget-boolean v0, p1, Lcom/android/launcher2/bz;->e:Z

    if-nez v0, :cond_11

    .line 123
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->g:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 124
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/InfoDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 126
    :cond_11
    return-void
.end method

.method public final e(Lcom/android/launcher2/bz;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 74
    const/4 v0, 0x0

    .line 75
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/android/launcher2/h;

    if-eqz v1, :cond_18

    .line 76
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/h;

    iget-object v0, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    .line 82
    :cond_e
    :goto_e
    if-eqz v0, :cond_15

    .line 83
    iget-object v1, p0, Lcom/android/launcher2/InfoDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/ComponentName;)V

    .line 87
    :cond_15
    iput-boolean v2, p1, Lcom/android/launcher2/bz;->k:Z

    .line 88
    return v2

    .line 77
    :cond_18
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_29

    .line 78
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/jd;

    iget-object v0, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_e

    .line 79
    :cond_29
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/android/launcher2/iu;

    if-eqz v1, :cond_e

    .line 80
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/iu;

    iget-object v0, v0, Lcom/android/launcher2/iu;->a:Landroid/content/ComponentName;

    goto :goto_e
.end method

.method protected onFinishInflate()V
    .registers 3

    .prologue
    .line 46
    invoke-super {p0}, Lcom/android/launcher2/ak;->onFinishInflate()V

    .line 48
    invoke-virtual {p0}, Lcom/android/launcher2/InfoDropTarget;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->f:Landroid/content/res/ColorStateList;

    .line 51
    invoke-virtual {p0}, Lcom/android/launcher2/InfoDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/InfoDropTarget;->e:I

    .line 53
    invoke-virtual {p0}, Lcom/android/launcher2/InfoDropTarget;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->g:Landroid/graphics/drawable/TransitionDrawable;

    .line 54
    iget-object v0, p0, Lcom/android/launcher2/InfoDropTarget;->g:Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/android/launcher2/InfoDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 58
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3c

    .line 59
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_3c

    .line 60
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/launcher2/InfoDropTarget;->setText(Ljava/lang/CharSequence;)V

    .line 63
    :cond_3c
    return-void
.end method
