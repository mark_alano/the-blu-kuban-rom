.class public Lcom/android/launcher2/LauncherProvider;
.super Landroid/content/ContentProvider;
.source "SourceFile"


# static fields
.field static final a:Landroid/net/Uri;


# instance fields
.field private b:Lcom/android/launcher2/hi;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 92
    const-string v0, "content://com.anddoes.launcher.settings/appWidgetReset"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 91
    sput-object v0, Lcom/android/launcher2/LauncherProvider;->a:Landroid/net/Uri;

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-static {p0, p1, p2}, Lcom/android/launcher2/LauncherProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method static a(Ljava/lang/String;[I)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1236
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1237
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-gez v0, :cond_f

    .line 1243
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1238
    :cond_f
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1239
    if-lez v0, :cond_25

    .line 1240
    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    :cond_25
    add-int/lit8 v0, v0, -0x1

    goto :goto_8
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-static {p1, p2}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lcom/android/launcher2/hj;

    invoke-direct {v1, v0, v2, v2}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, v1, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    iget-object v2, v1, Lcom/android/launcher2/hj;->b:Ljava/lang/String;

    iget-object v1, v1, Lcom/android/launcher2/hj;->c:[Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 201
    const-string v0, "notify"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_10

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 203
    :cond_10
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 205
    :cond_1c
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 130
    const-string v0, "_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: attempting to add item without specifying an id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_10
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .registers 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/io/File;)Z
    .registers 3
    .parameter

    .prologue
    .line 1280
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-static {v0, p1}, Lcom/android/launcher2/hi;->a(Lcom/android/launcher2/hi;Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b()V
    .registers 4

    .prologue
    .line 212
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 214
    const-string v1, "DB_CREATED_BUT_DEFAULT_WORKSPACE_NOT_LOADED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 216
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 217
    const-string v1, "DB_CREATED_BUT_DEFAULT_WORKSPACE_NOT_LOADED"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 218
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v2}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/launcher2/hi;->a(Lcom/android/launcher2/hi;Landroid/database/sqlite/SQLiteDatabase;)I

    .line 219
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_30

    .line 221
    :cond_2e
    monitor-exit p0

    return-void

    .line 212
    :catchall_30
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/io/File;)Z
    .registers 3
    .parameter

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-static {v0, p1}, Lcom/android/launcher2/hi;->b(Lcom/android/launcher2/hi;Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 158
    new-instance v2, Lcom/android/launcher2/hj;

    invoke-direct {v2, p1}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;)V

    .line 160
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v1}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 161
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 163
    :try_start_f
    array-length v4, p2

    move v1, v0

    .line 164
    :goto_11
    if-lt v1, v4, :cond_1e

    .line 169
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_35

    .line 171
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 174
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/net/Uri;)V

    .line 175
    array-length v0, p2

    :goto_1d
    return v0

    .line 165
    :cond_1e
    :try_start_1e
    iget-object v5, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    iget-object v5, v2, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    aget-object v6, p2, v1

    invoke-static {v3, v5, v6}, Lcom/android/launcher2/LauncherProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_27
    .catchall {:try_start_1e .. :try_end_27} :catchall_35

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gez v5, :cond_32

    .line 171
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1d

    .line 164
    :cond_32
    add-int/lit8 v1, v1, 0x1

    goto :goto_11

    .line 170
    :catchall_35
    move-exception v0

    .line 171
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 172
    throw v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 180
    new-instance v0, Lcom/android/launcher2/hj;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v1}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 183
    iget-object v2, v0, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/launcher2/hj;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/launcher2/hj;->c:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 184
    if-lez v0, :cond_1a

    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/net/Uri;)V

    .line 186
    :cond_1a
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 105
    new-instance v0, Lcom/android/launcher2/hj;

    invoke-direct {v0, p1, v1, v1}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 106
    iget-object v1, v0, Lcom/android/launcher2/hj;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vnd.android.cursor.dir/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_1f
    return-object v0

    :cond_20
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vnd.android.cursor.item/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1f
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 144
    new-instance v0, Lcom/android/launcher2/hj;

    invoke-direct {v0, p1}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;)V

    .line 146
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v1}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 147
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    iget-object v0, v0, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lcom/android/launcher2/LauncherProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 148
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1b

    const/4 v0, 0x0

    .line 153
    :goto_1a
    return-object v0

    .line 150
    :cond_1b
    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 151
    invoke-direct {p0, v0}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/net/Uri;)V

    goto :goto_1a
.end method

.method public onCreate()Z
    .registers 3

    .prologue
    .line 98
    new-instance v0, Lcom/android/launcher2/hi;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/launcher2/hi;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    .line 99
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/LauncherApplication;->a(Lcom/android/launcher2/LauncherProvider;)V

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 117
    new-instance v2, Lcom/android/launcher2/hj;

    invoke-direct {v2, p1, p3, p4}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 118
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 119
    iget-object v1, v2, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v1}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 122
    iget-object v3, v2, Lcom/android/launcher2/hj;->b:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/launcher2/hj;->c:[Ljava/lang/String;

    move-object v2, p2

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 123
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 125
    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    new-instance v0, Lcom/android/launcher2/hj;

    invoke-direct {v0, p1, p3, p4}, Lcom/android/launcher2/hj;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 193
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider;->b:Lcom/android/launcher2/hi;

    invoke-virtual {v1}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 194
    iget-object v2, v0, Lcom/android/launcher2/hj;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/launcher2/hj;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/launcher2/hj;->c:[Ljava/lang/String;

    invoke-virtual {v1, v2, p2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 195
    if-lez v0, :cond_1a

    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/net/Uri;)V

    .line 197
    :cond_1a
    return v0
.end method
