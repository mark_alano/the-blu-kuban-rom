.class public Lcom/android/launcher2/PagedViewWidget;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field static d:Lcom/android/launcher2/PagedViewWidget;

.field private static f:Z


# instance fields
.field a:Lcom/android/launcher2/iq;

.field b:Lcom/android/launcher2/ir;

.field c:Z

.field e:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/android/launcher2/Launcher;

.field private final i:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/PagedViewWidget;->f:Z

    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher2/PagedViewWidget;->d:Lcom/android/launcher2/PagedViewWidget;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    .line 45
    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->b:Lcom/android/launcher2/ir;

    .line 46
    iput-boolean v2, p0, Lcom/android/launcher2/PagedViewWidget;->c:Z

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    .line 62
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    const v1, 0x7f07027d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->g:Ljava/lang/String;

    .line 66
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedViewWidget;->setWillNotDraw(Z)V

    .line 67
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedViewWidget;->setClipToPadding(Z)V

    .line 68
    return-void
.end method

.method static b()V
    .registers 1

    .prologue
    .line 248
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher2/PagedViewWidget;->d:Lcom/android/launcher2/PagedViewWidget;

    .line 249
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 239
    :cond_9
    iget-boolean v0, p0, Lcom/android/launcher2/PagedViewWidget;->c:Z

    if-eqz v0, :cond_19

    .line 240
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->b:Lcom/android/launcher2/ir;

    if-eqz v0, :cond_16

    .line 241
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->b:Lcom/android/launcher2/ir;

    invoke-interface {v0}, Lcom/android/launcher2/ir;->f()V

    .line 243
    :cond_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedViewWidget;->c:Z

    .line 245
    :cond_19
    return-void
.end method

.method public static setDeletePreviewsWhenDetachedFromWindow(Z)V
    .registers 1
    .parameter

    .prologue
    .line 83
    return-void
.end method


# virtual methods
.method final a()V
    .registers 6

    .prologue
    .line 191
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewWidgetImageView;

    .line 192
    if-eqz v0, :cond_28

    .line 193
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 194
    const v2, 0x7f0b0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 195
    const v3, 0x7f0b0063

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 196
    const v4, 0x7f0b0062

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v4, 0x0

    .line 194
    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/android/launcher2/PagedViewWidgetImageView;->setPadding(IIII)V

    .line 198
    :cond_28
    return-void
.end method

.method public final a(Landroid/appwidget/AppWidgetProviderInfo;[I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 103
    iput-boolean v3, p0, Lcom/android/launcher2/PagedViewWidget;->e:Z

    .line 104
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 105
    iget-object v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_89

    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->S:Z

    if-eqz v0, :cond_89

    move v1, v2

    .line 113
    :goto_1f
    const v0, 0x7f0d0015

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    if-eqz v1, :cond_2f

    .line 115
    iget-object v4, p1, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :cond_2f
    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v4, v0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 118
    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v5, "drawer_icon_text_color"

    invoke-virtual {v4, v0, v5}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 120
    const v0, 0x7f0d0016

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    if-eqz v0, :cond_88

    .line 122
    aget v4, p2, v2

    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 123
    aget v5, p2, v3

    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 124
    if-eqz v1, :cond_78

    .line 125
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->g:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    :cond_78
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 128
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v2, "drawer_widget_size_color"

    invoke-virtual {v1, v0, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 130
    :cond_88
    return-void

    :cond_89
    move v1, v3

    goto :goto_1f
.end method

.method public final a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 133
    iput-boolean v2, p0, Lcom/android/launcher2/PagedViewWidget;->e:Z

    .line 134
    invoke-virtual {p2, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 135
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 136
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_75

    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->S:Z

    if-eqz v0, :cond_75

    move v1, v2

    .line 141
    :goto_21
    const v0, 0x7f0d0015

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 142
    if-eqz v1, :cond_2f

    .line 143
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :cond_2f
    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v4, v0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 146
    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v5, "drawer_icon_text_color"

    invoke-virtual {v4, v0, v5}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 148
    const v0, 0x7f0d0016

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 149
    if-eqz v0, :cond_74

    .line 150
    if-eqz v1, :cond_64

    .line 151
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->g:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v3

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    :cond_64
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 154
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->h:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v2, "drawer_widget_size_color"

    invoke-virtual {v1, v0, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 156
    :cond_74
    return-void

    :cond_75
    move v1, v3

    goto :goto_21
.end method

.method final a(Lcom/android/launcher2/ca;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 168
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewWidgetImageView;

    .line 169
    if-eqz p1, :cond_44

    .line 170
    iput-boolean v3, v0, Lcom/android/launcher2/PagedViewWidgetImageView;->a:Z

    .line 171
    invoke-virtual {v0, p1}, Lcom/android/launcher2/PagedViewWidgetImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 172
    iget-boolean v1, p0, Lcom/android/launcher2/PagedViewWidget;->e:Z

    if-eqz v1, :cond_3c

    .line 174
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidget;->getPreviewSize()[I

    move-result-object v1

    .line 175
    aget v2, v1, v3

    if-gtz v2, :cond_1f

    .line 176
    aput p2, v1, v3

    .line 178
    :cond_1f
    aget v1, v1, v3

    invoke-virtual {p1}, Lcom/android/launcher2/ca;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 179
    iget-object v2, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    .line 180
    iget-object v2, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 181
    iget-object v3, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    .line 182
    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    .line 179
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/PagedViewWidgetImageView;->setPadding(IIII)V

    .line 184
    :cond_3c
    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedViewWidgetImageView;->setAlpha(F)V

    .line 185
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/PagedViewWidgetImageView;->a:Z

    .line 187
    :cond_44
    return-void
.end method

.method public getPreviewSize()[I
    .registers 6

    .prologue
    .line 159
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 160
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 161
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    aput v3, v1, v2

    .line 162
    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v3

    aput v0, v1, v2

    .line 163
    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .registers 4

    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 89
    sget-boolean v0, Lcom/android/launcher2/PagedViewWidget;->f:Z

    if-eqz v0, :cond_2b

    .line 90
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 91
    if-eqz v0, :cond_2b

    .line 92
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/ca;

    .line 93
    if-eqz v1, :cond_27

    invoke-virtual {v1}, Lcom/android/launcher2/ca;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_27

    .line 94
    invoke-virtual {v1}, Lcom/android/launcher2/ca;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 96
    :cond_27
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    :cond_2b
    return-void
.end method

.method protected onFinishInflate()V
    .registers 4

    .prologue
    .line 72
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 74
    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 75
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 76
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 77
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 78
    iget-object v1, p0, Lcom/android/launcher2/PagedViewWidget;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 79
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 253
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 255
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_2c

    .line 264
    :cond_a
    :goto_a
    :pswitch_a
    const/4 v0, 0x1

    return v0

    .line 257
    :pswitch_c
    invoke-direct {p0}, Lcom/android/launcher2/PagedViewWidget;->c()V

    goto :goto_a

    .line 260
    :pswitch_10
    sget-object v0, Lcom/android/launcher2/PagedViewWidget;->d:Lcom/android/launcher2/PagedViewWidget;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    if-nez v0, :cond_1f

    new-instance v0, Lcom/android/launcher2/iq;

    invoke-direct {v0, p0}, Lcom/android/launcher2/iq;-><init>(Lcom/android/launcher2/PagedViewWidget;)V

    iput-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    :cond_1f
    iget-object v0, p0, Lcom/android/launcher2/PagedViewWidget;->a:Lcom/android/launcher2/iq;

    const-wide/16 v1, 0x78

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedViewWidget;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_a

    .line 263
    :pswitch_27
    invoke-direct {p0}, Lcom/android/launcher2/PagedViewWidget;->c()V

    goto :goto_a

    .line 255
    nop

    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_10
        :pswitch_c
        :pswitch_a
        :pswitch_27
    .end packed-switch
.end method

.method setShortPressListener(Lcom/android/launcher2/ir;)V
    .registers 2
    .parameter

    .prologue
    .line 201
    iput-object p1, p0, Lcom/android/launcher2/PagedViewWidget;->b:Lcom/android/launcher2/ir;

    .line 202
    return-void
.end method
