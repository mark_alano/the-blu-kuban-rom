.class Lcom/android/launcher2/PagedViewWidgetImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedViewWidgetImageView;->a:Z

    .line 33
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 45
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 46
    invoke-static {p0}, Lcom/anddoes/launcher/r;->a(Landroid/widget/ImageView;)Landroid/graphics/Rect;

    move-result-object v0

    .line 47
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    .line 48
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    .line 49
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    .line 50
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getScrollY()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getBottom()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewWidgetImageView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v4, v0

    .line 47
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 52
    :cond_55
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 55
    return-void
.end method

.method public requestLayout()V
    .registers 2

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/android/launcher2/PagedViewWidgetImageView;->a:Z

    if-eqz v0, :cond_7

    .line 37
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 39
    :cond_7
    return-void
.end method
