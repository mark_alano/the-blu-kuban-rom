.class public Lcom/android/launcher2/Workspace;
.super Lcom/android/launcher2/je;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Lcom/android/launcher2/bl;
.implements Lcom/android/launcher2/bs;
.implements Lcom/android/launcher2/bt;
.implements Lcom/android/launcher2/bx;
.implements Lcom/android/launcher2/hn;


# static fields
.field private static cv:J

.field private static cw:J


# instance fields
.field private aG:Landroid/animation/ValueAnimator;

.field private aH:Landroid/animation/ValueAnimator;

.field private aI:Landroid/graphics/drawable/Drawable;

.field private aJ:F

.field private aK:F

.field private aL:F

.field private final aM:Landroid/app/WallpaperManager;

.field private aN:Landroid/os/IBinder;

.field private aO:Lcom/android/launcher2/as;

.field private aP:[I

.field private aQ:I

.field private aR:I

.field private aS:Lcom/android/launcher2/CellLayout;

.field private aT:Lcom/android/launcher2/CellLayout;

.field private aU:Lcom/android/launcher2/CellLayout;

.field private aV:Lcom/android/launcher2/Launcher;

.field private aW:Lcom/android/launcher2/da;

.field private aX:Lcom/android/launcher2/bk;

.field private aY:[I

.field private aZ:[I

.field b:Z

.field private bA:Lcom/android/launcher2/co;

.field private bB:Lcom/android/launcher2/FolderIcon;

.field private bC:Z

.field private bD:Z

.field private bE:Lcom/android/launcher2/by;

.field private bF:F

.field private bG:F

.field private bH:F

.field private bI:I

.field private bJ:I

.field private bK:I

.field private bL:Landroid/util/SparseArray;

.field private final bM:Ljava/util/ArrayList;

.field private bN:I

.field private bO:F

.field private bP:F

.field private bQ:F

.field private bR:F

.field private bS:F

.field private bT:F

.field private bU:F

.field private bV:[F

.field private bW:[F

.field private bX:[F

.field private bY:[F

.field private bZ:[F

.field private ba:[F

.field private bb:[F

.field private bc:[F

.field private bd:[F

.field private be:Landroid/graphics/Matrix;

.field private bf:Lcom/android/launcher2/jg;

.field private bg:F

.field private bh:Lcom/android/launcher2/kg;

.field private bi:Z

.field private bj:Z

.field private final bk:Lcom/android/launcher2/cx;

.field private bl:Landroid/graphics/Bitmap;

.field private final bm:Landroid/graphics/Rect;

.field private final bn:[I

.field private bo:F

.field private bp:Z

.field private bq:Z

.field private br:Ljava/lang/Runnable;

.field private bs:Ljava/lang/Runnable;

.field private bt:Landroid/graphics/Point;

.field private bu:Z

.field private bv:I

.field private bw:I

.field private bx:I

.field private final by:Lcom/android/launcher2/c;

.field private final bz:Lcom/android/launcher2/c;

.field c:Z

.field private cA:J

.field private cB:I

.field private final cC:[[I

.field private final cD:[F

.field private cE:F

.field private cF:Landroid/graphics/Paint;

.field private cG:Z

.field private ca:[F

.field private cb:[F

.field private cc:[F

.field private cd:[F

.field private ce:[F

.field private cf:[F

.field private cg:[F

.field private ch:F

.field private final ci:Lcom/android/launcher2/kj;

.field private cj:Lcom/anddoes/launcher/preference/h;

.field private ck:Z

.field private cl:Z

.field private cm:Z

.field private cn:J

.field private co:F

.field private cp:F

.field private cq:F

.field private cr:F

.field private cs:F

.field private ct:F

.field private cu:J

.field private cx:Z

.field private cy:Z

.field private cz:Z

.field d:Z

.field e:Z

.field f:I

.field g:I

.field h:Lcom/android/launcher2/kh;

.field i:Z

.field public j:[F

.field protected k:Z

.field protected l:D

.field private m:Landroid/animation/ObjectAnimator;

.field private n:Landroid/animation/ObjectAnimator;

.field private o:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const-wide/16 v0, 0x190

    .line 4244
    sput-wide v0, Lcom/android/launcher2/Workspace;->cv:J

    .line 4245
    sput-wide v0, Lcom/android/launcher2/Workspace;->cw:J

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 275
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/Workspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 286
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/je;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->o:F

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->b:Z

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->aK:F

    .line 111
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/launcher2/Workspace;->aL:F

    .line 127
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->aQ:I

    .line 129
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->aR:I

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    .line 151
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aY:[I

    .line 152
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aZ:[I

    .line 153
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->ba:[F

    .line 154
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bb:[F

    .line 155
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bc:[F

    .line 156
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bd:[F

    .line 157
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->be:Landroid/graphics/Matrix;

    .line 169
    sget-object v0, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->c:Z

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->d:Z

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->e:Z

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bj:Z

    .line 179
    new-instance v0, Lcom/android/launcher2/cx;

    invoke-direct {v0}, Lcom/android/launcher2/cx;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bk:Lcom/android/launcher2/cx;

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 181
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bm:Landroid/graphics/Rect;

    .line 182
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bn:[I

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bo:F

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->i:Z

    .line 200
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    .line 209
    new-instance v0, Lcom/android/launcher2/c;

    invoke-direct {v0}, Lcom/android/launcher2/c;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->by:Lcom/android/launcher2/c;

    .line 210
    new-instance v0, Lcom/android/launcher2/c;

    invoke-direct {v0}, Lcom/android/launcher2/c;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bz:Lcom/android/launcher2/c;

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bA:Lcom/android/launcher2/co;

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bC:Z

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bD:Z

    .line 237
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    .line 238
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->bJ:I

    .line 239
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->bK:I

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bM:Ljava/util/ArrayList;

    .line 1791
    new-instance v0, Lcom/android/launcher2/kj;

    invoke-direct {v0}, Lcom/android/launcher2/kj;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->ci:Lcom/android/launcher2/kj;

    .line 4230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->ck:Z

    .line 4231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->k:Z

    .line 4232
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/Workspace;->l:D

    .line 4233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cl:Z

    .line 4235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cm:Z

    .line 4236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/Workspace;->cn:J

    .line 4243
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 4246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cx:Z

    .line 4247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    .line 4253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    .line 4256
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/Workspace;->cB:I

    .line 4258
    const/16 v0, 0x9

    new-array v0, v0, [[I

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x2

    aput v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_2ce

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_2d6

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 4259
    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_2de

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_2e8

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_2f2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_2fc

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_306

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->cC:[[I

    .line 4260
    const/16 v0, 0x9

    new-array v0, v0, [F

    fill-array-data v0, :array_310

    .line 4261
    iput-object v0, p0, Lcom/android/launcher2/Workspace;->cD:[F

    .line 4262
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/launcher2/Workspace;->cE:F

    move-object v0, p1

    .line 287
    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    .line 288
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->cj:Lcom/anddoes/launcher/preference/h;

    .line 289
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cG:Z

    .line 290
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->h:Ljava/lang/String;

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2bf

    const/4 v0, 0x0

    :goto_167
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->aq:Z

    .line 291
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->i:Z

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setElasticScrolling(Z)V

    .line 292
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->j:Z

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->as:Z

    .line 293
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    .line 295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->ab:Z

    .line 297
    new-instance v0, Lcom/android/launcher2/by;

    invoke-direct {v0, p1}, Lcom/android/launcher2/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bE:Lcom/android/launcher2/by;

    .line 299
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->q()V

    .line 301
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 302
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bq:Z

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->ac:Z

    .line 304
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    .line 306
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v3, v0, Lcom/anddoes/launcher/preference/f;->d:I

    .line 307
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v4, v0, Lcom/anddoes/launcher/preference/f;->c:I

    .line 309
    const/4 v0, 0x4

    .line 310
    const/4 v1, 0x4

    .line 311
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->J()V

    .line 314
    sget-object v5, Lcom/anddoes/launcher/at;->Workspace:[I

    const/4 v6, 0x0

    .line 313
    invoke-virtual {p1, p2, v5, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 316
    iget-boolean v6, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v6, :cond_1ff

    .line 322
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v6, 0x10102eb

    aput v6, v0, v1

    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 323
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 324
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    .line 325
    const v0, 0x7f0b005c

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 326
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    int-to-float v0, v0

    .line 327
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 326
    mul-float v8, v0, v1

    .line 329
    const/4 v0, 0x1

    .line 330
    :goto_1e5
    add-int/lit8 v1, v0, 0x1

    invoke-static {v2, v1}, Lcom/android/launcher2/CellLayout;->a(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v1, v8

    if-lez v1, :cond_2c2

    .line 334
    const/4 v1, 0x1

    .line 335
    :goto_1f1
    add-int/lit8 v9, v1, 0x1

    invoke-static {v2, v9}, Lcom/android/launcher2/CellLayout;->b(Landroid/content/res/Resources;I)I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v9, v6

    .line 336
    sub-float v10, v8, v7

    cmpg-float v9, v9, v10

    if-lez v9, :cond_2c6

    .line 342
    :cond_1ff
    const v6, 0x7f0a0005

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x42c8

    div-float/2addr v6, v7

    .line 341
    iput v6, p0, Lcom/android/launcher2/Workspace;->bg:F

    .line 344
    const v6, 0x7f0b0044

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 343
    iput v6, p0, Lcom/android/launcher2/Workspace;->bw:I

    .line 345
    const v6, 0x7f0a001b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/launcher2/Workspace;->bx:I

    .line 351
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 353
    invoke-virtual {p0, p0}, Lcom/android/launcher2/Workspace;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 355
    invoke-static {v3, v4, v0, v1}, Lcom/android/launcher2/gb;->a(IIII)V

    .line 356
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setHapticFeedbackEnabled(Z)V

    .line 358
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    .line 359
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/Workspace;->al:I

    iput v1, p0, Lcom/android/launcher2/Workspace;->u:I

    iget v1, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-static {v1}, Lcom/android/launcher2/Launcher;->a(I)V

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->a()Lcom/android/launcher2/da;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aW:Lcom/android/launcher2/da;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setWillNotDraw(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setChildrenDrawnWithCacheEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-nez v0, :cond_2ca

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-nez v0, :cond_2ca

    const/4 v0, 0x0

    :goto_25b
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020003

    :try_start_264
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;
    :try_end_26a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_264 .. :try_end_26a} :catch_2cc

    :goto_26a
    new-instance v1, Lcom/android/launcher2/kh;

    invoke-direct {v1, p0}, Lcom/android/launcher2/kh;-><init>(Lcom/android/launcher2/Workspace;)V

    iput-object v1, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Lcom/android/launcher2/Workspace;->d(II)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bv:I

    const v1, 0x3f0ccccd

    const v2, 0x7f0b004f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/Workspace;->bF:F

    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/launcher2/Workspace;->p:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 362
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setMotionEventSplittingEnabled(Z)V

    .line 368
    return-void

    .line 290
    :cond_2bf
    const/4 v0, 0x1

    goto/16 :goto_167

    .line 331
    :cond_2c2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1e5

    .line 337
    :cond_2c6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1f1

    .line 359
    :cond_2ca
    const/4 v0, 0x1

    goto :goto_25b

    :catch_2cc
    move-exception v1

    goto :goto_26a

    .line 4258
    :array_2ce
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_2d6
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    .line 4259
    :array_2de
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_2e8
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_2f2
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_2fc
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_306
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    .line 4260
    :array_310
    .array-data 0x4
        0x66t 0x66t 0x66t 0x3ft
        0xd7t 0xa3t 0xf0t 0x3et
        0xd7t 0xa3t 0xf0t 0x3et
        0xd7t 0xa3t 0xf0t 0x3et
        0x52t 0xb8t 0x9et 0x3et
        0x52t 0xb8t 0x9et 0x3et
        0x52t 0xb8t 0x9et 0x3et
        0x52t 0xb8t 0x9et 0x3et
        0x52t 0xb8t 0x9et 0x3et
    .end array-data
.end method

.method private N()V
    .registers 5

    .prologue
    .line 1329
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_38

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_38

    .line 1330
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1331
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1332
    :cond_1c
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x3f80

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    .line 1333
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1334
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1336
    :cond_38
    return-void
.end method

.method private O()V
    .registers 5

    .prologue
    .line 1339
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_3e

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_3e

    .line 1340
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->m:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1341
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1342
    :cond_1c
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    .line 1343
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x177

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1344
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1345
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->n:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1347
    :cond_3e
    return-void
.end method

.method private P()Z
    .registers 3

    .prologue
    .line 1675
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v1, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    if-eq v0, v1, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v1, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method private Q()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1710
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v3, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    if-eq v2, v3, :cond_27

    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v2, :cond_27

    move v2, v1

    .line 1711
    :goto_d
    if-nez v2, :cond_1a

    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->c:Z

    if-nez v2, :cond_1a

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->v()Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 1713
    :cond_1a
    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->e:Z

    if-eq v0, v2, :cond_26

    .line 1714
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->e:Z

    .line 1715
    :goto_20
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getPageCount()I

    move-result v0

    if-lt v1, v0, :cond_29

    .line 1719
    :cond_26
    return-void

    :cond_27
    move v2, v0

    .line 1710
    goto :goto_d

    .line 1716
    :cond_29
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->e:Z

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setChildrenLayersEnabled(Z)V

    .line 1715
    add-int/lit8 v1, v1, 0x1

    goto :goto_20
.end method

.method private R()V
    .registers 2

    .prologue
    .line 2838
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bA:Lcom/android/launcher2/co;

    if-eqz v0, :cond_9

    .line 2839
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bA:Lcom/android/launcher2/co;

    invoke-virtual {v0}, Lcom/android/launcher2/co;->c()V

    .line 2841
    :cond_9
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->by:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 2842
    return-void
.end method

.method private S()V
    .registers 2

    .prologue
    .line 2845
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_c

    .line 2846
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->a()V

    .line 2847
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    .line 2849
    :cond_c
    return-void
.end method

.method private T()Z
    .registers 3

    .prologue
    .line 4399
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 4400
    const-string v0, "DO_NOTHING"

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 4401
    const-string v0, "DO_NOTHING"

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->au:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    .line 4399
    goto :goto_25
.end method

.method private U()Z
    .registers 3

    .prologue
    .line 4405
    const/4 v0, 0x0

    .line 4406
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v1, :cond_15

    .line 4407
    const-string v0, "MULTIPLE_SCREEN"

    .line 4408
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 4410
    :cond_15
    return v0
.end method

.method private a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 2146
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 2147
    const v1, 0x1060012

    const-string v2, "outline_color"

    .line 2146
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v1

    .line 2150
    :try_start_10
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_15
    .catch Ljava/lang/OutOfMemoryError; {:try_start_10 .. :try_end_15} :catch_66

    move-result-object v0

    .line 2155
    :goto_16
    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2157
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2158
    add-int/lit8 v3, p3, -0x2

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 2159
    add-int/lit8 v4, p4, -0x2

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 2158
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2160
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v3

    float-to-int v4, v4

    .line 2161
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v3, v5

    float-to-int v3, v3

    .line 2162
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v6, v6, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2165
    sub-int v4, p3, v4

    div-int/lit8 v4, v4, 0x2

    sub-int v3, p4, v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v5, v4, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 2167
    invoke-virtual {p2, p1, v2, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2168
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bk:Lcom/android/launcher2/cx;

    .line 2169
    invoke-virtual {v2, v0, p2, v1, v1}, Lcom/android/launcher2/cx;->b(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    .line 2170
    invoke-virtual {p2, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2172
    return-object v0

    .line 2152
    :catch_66
    move-exception v0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2153
    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_16
.end method

.method private static a(Lcom/android/launcher2/CellLayout;IIII)Landroid/graphics/Rect;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 398
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 399
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/graphics/Rect;)V

    .line 400
    return-object v5
.end method

.method private a(FZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1375
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_6

    .line 1400
    :cond_5
    :goto_5
    return-void

    .line 1376
    :cond_6
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aG:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_11

    .line 1377
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aG:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1378
    iput-object v1, p0, Lcom/android/launcher2/Workspace;->aG:Landroid/animation/ValueAnimator;

    .line 1380
    :cond_11
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1c

    .line 1381
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1382
    iput-object v1, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    .line 1384
    :cond_1c
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getBackgroundAlpha()F

    move-result v0

    .line 1385
    cmpl-float v1, p1, v0

    if-eqz v1, :cond_5

    .line 1386
    if-eqz p2, :cond_58

    .line 1387
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v0, 0x1

    aput p1, v1, v0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    .line 1388
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/jv;

    invoke-direct {v1, p0}, Lcom/android/launcher2/jv;-><init>(Lcom/android/launcher2/Workspace;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1393
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc0

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1394
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x15e

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1395
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aH:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_5

    .line 1397
    :cond_58
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->setBackgroundAlpha(F)V

    goto :goto_5
.end method

.method private a(Landroid/view/View;Landroid/graphics/Canvas;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x3f80

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2050
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->bm:Landroid/graphics/Rect;

    .line 2051
    invoke-virtual {p1, v3}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2055
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    .line 2056
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_31

    .line 2057
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    .line 2058
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v3, v2, v2, v1, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2059
    invoke-virtual {p2, v5, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2060
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2087
    :cond_2d
    :goto_2d
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 2088
    return-void

    .line 2062
    :cond_31
    instance-of v0, p1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_68

    move-object v0, p1

    .line 2065
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->getTextVisible()Z

    move-result v0

    if-eqz v0, :cond_9d

    move-object v0, p1

    .line 2066
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/FolderIcon;->setTextVisible(Z)V

    move v0, v1

    .line 2078
    :goto_45
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v2

    neg-int v2, v2

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    neg-int v4, v4

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    invoke-virtual {p2, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2079
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p2, v3, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 2080
    invoke-virtual {p1, p2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2083
    if-eqz v0, :cond_2d

    .line 2084
    check-cast p1, Lcom/android/launcher2/FolderIcon;

    invoke-virtual {p1, v1}, Lcom/android/launcher2/FolderIcon;->setTextVisible(Z)V

    goto :goto_2d

    .line 2069
    :cond_68
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_82

    move-object v0, p1

    .line 2070
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 2071
    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getExtendedPaddingTop()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    .line 2072
    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    add-int/2addr v0, v4

    .line 2071
    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    move v0, v2

    goto :goto_45

    .line 2073
    :cond_82
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_9d

    move-object v0, p1

    .line 2074
    check-cast v0, Landroid/widget/TextView;

    .line 2075
    invoke-virtual {v0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v5

    sub-int/2addr v4, v5

    .line 2076
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    add-int/2addr v0, v4

    .line 2075
    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    :cond_9d
    move v0, v2

    goto :goto_45
.end method

.method private static a(Landroid/view/View;[F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2903
    aget v0, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v2

    .line 2904
    aget v0, p1, v3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v3

    .line 2905
    return-void
.end method

.method private a(Landroid/view/View;[FLandroid/graphics/Matrix;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2885
    if-nez p3, :cond_f

    .line 2886
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->be:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 2887
    iget-object p3, p0, Lcom/android/launcher2/Workspace;->be:Landroid/graphics/Matrix;

    .line 2889
    :cond_f
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v0

    .line 2890
    iget v1, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v2, -0x3e7

    if-eq v1, v2, :cond_1f

    .line 2891
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    .line 2893
    :cond_1f
    aget v1, p2, v3

    int-to-float v0, v0

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v3

    .line 2894
    aget v0, p2, v4

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v4

    .line 2895
    invoke-virtual {p3, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 2896
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Workspace;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 238
    iput p1, p0, Lcom/android/launcher2/Workspace;->bJ:I

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/co;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 211
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->bA:Lcom/android/launcher2/co;

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Workspace;Ljava/lang/Runnable;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 198
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->br:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Workspace;[I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    return-void
.end method

.method private a(IFFZ)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 634
    iget-boolean v1, p0, Lcom/android/launcher2/Workspace;->aq:Z

    if-eqz v1, :cond_10

    .line 635
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 636
    if-gez p1, :cond_d

    .line 637
    add-int/lit8 p1, v1, -0x1

    .line 639
    :cond_d
    if-lt p1, v1, :cond_10

    move p1, v0

    .line 643
    :cond_10
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 644
    if-eqz v1, :cond_41

    .line 645
    const/4 v2, 0x0

    cmpl-float v2, p3, v2

    if-ltz v2, :cond_41

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p3, v2

    if-gtz v2, :cond_41

    if-eqz p4, :cond_2f

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-ltz v2, :cond_40

    .line 646
    :cond_2f
    if-nez p4, :cond_41

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int v1, v2, v1

    int-to-float v1, v1

    cmpl-float v1, p2, v1

    if-lez v1, :cond_41

    .line 647
    :cond_40
    const/4 v0, 0x1

    .line 654
    :cond_41
    return v0
.end method

.method static synthetic a(Lcom/android/launcher2/Workspace;)Z
    .registers 2
    .parameter

    .prologue
    .line 4247
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    return v0
.end method

.method private a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;[IFZ)Z
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2352
    iget v0, p0, Lcom/android/launcher2/Workspace;->bF:F

    cmpl-float v0, p4, v0

    if-lez v0, :cond_9

    .line 2376
    :cond_8
    :goto_8
    return v2

    .line 2353
    :cond_9
    aget v0, p3, v2

    aget v3, p3, v1

    invoke-virtual {p2, v0, v3}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v3

    .line 2355
    if-eqz v3, :cond_29

    .line 2356
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2357
    iget-boolean v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v4, :cond_29

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    if-ne v4, v5, :cond_8

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    if-ne v4, v0, :cond_8

    .line 2363
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v0, :cond_57

    .line 2364
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    if-ne v3, v0, :cond_53

    move v0, v1

    .line 2367
    :goto_34
    if-eqz v3, :cond_8

    if-nez v0, :cond_8

    if-eqz p5, :cond_3e

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bC:Z

    if-eqz v0, :cond_8

    .line 2371
    :cond_3e
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Lcom/android/launcher2/jd;

    .line 2373
    iget v0, p1, Lcom/android/launcher2/di;->i:I

    if-eqz v0, :cond_55

    .line 2374
    iget v0, p1, Lcom/android/launcher2/di;->i:I

    if-eq v0, v1, :cond_55

    move v0, v2

    .line 2376
    :goto_4d
    if-eqz v3, :cond_8

    if-eqz v0, :cond_8

    move v2, v1

    goto :goto_8

    :cond_53
    move v0, v2

    .line 2364
    goto :goto_34

    :cond_55
    move v0, v1

    .line 2374
    goto :goto_4d

    :cond_57
    move v0, v2

    goto :goto_34
.end method

.method private a(Ljava/lang/Object;Lcom/android/launcher2/CellLayout;[IF)Z
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2381
    iget v0, p0, Lcom/android/launcher2/Workspace;->bF:F

    cmpl-float v0, p4, v0

    if-lez v0, :cond_a

    move v0, v2

    .line 2397
    :goto_9
    return v0

    .line 2382
    :cond_a
    aget v0, p3, v2

    aget v1, p3, v3

    invoke-virtual {p2, v0, v1}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v1

    .line 2384
    if-eqz v1, :cond_2c

    .line 2385
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2386
    iget-boolean v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v4, :cond_2c

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    iget v5, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    if-ne v4, v5, :cond_2a

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    if-eq v4, v0, :cond_2c

    :cond_2a
    move v0, v2

    .line 2387
    goto :goto_9

    .line 2391
    :cond_2c
    instance-of v0, v1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_3b

    move-object v0, v1

    .line 2392
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 2393
    invoke-virtual {v0, p1}, Lcom/android/launcher2/FolderIcon;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    move v0, v3

    .line 2394
    goto :goto_9

    :cond_3b
    move v0, v2

    .line 2397
    goto :goto_9
.end method

.method private a(IIIILcom/android/launcher2/bu;[F)[F
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3034
    if-nez p6, :cond_5

    .line 3035
    const/4 v0, 0x2

    new-array p6, v0, [F

    .line 3037
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, p1

    .line 3043
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, p2

    .line 3049
    sub-int/2addr v0, p3

    .line 3050
    sub-int/2addr v1, p4

    .line 3053
    const/4 v2, 0x0

    invoke-virtual {p5}, Lcom/android/launcher2/bu;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    aput v0, p6, v2

    .line 3054
    const/4 v0, 0x1

    invoke-virtual {p5}, Lcom/android/launcher2/bu;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    aput v1, p6, v0

    .line 3056
    return-object p6
.end method

.method static synthetic a(IIIILcom/android/launcher2/CellLayout;[I)[I
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3679
    invoke-static/range {p0 .. p5}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/view/View;Landroid/graphics/Canvas;)Landroid/graphics/Bitmap;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 2097
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_2b

    move-object v0, p1

    .line 2098
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    .line 2099
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 2100
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2099
    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2111
    :goto_20
    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2112
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 2113
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2115
    return-object v0

    .line 2104
    :cond_2b
    :try_start_2b
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2103
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_3c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2b .. :try_end_3c} :catch_3e

    move-result-object v0

    goto :goto_20

    .line 2106
    :catch_3e
    move-exception v0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2107
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_20
.end method

.method private b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;
    .registers 5
    .parameter

    .prologue
    .line 3924
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;

    move-result-object v0

    .line 3925
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 3930
    const/4 v0, 0x0

    :goto_f
    return-object v0

    .line 3925
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3926
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/launcher2/ja;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_8

    goto :goto_f
.end method

.method static synthetic b(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/co;
    .registers 2
    .parameter

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bA:Lcom/android/launcher2/co;

    return-object v0
.end method

.method static synthetic b(Lcom/android/launcher2/Workspace;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 239
    iput p1, p0, Lcom/android/launcher2/Workspace;->bK:I

    return-void
.end method

.method private static b(IIIILcom/android/launcher2/CellLayout;[I)[I
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3681
    move-object v0, p4

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/view/View;Landroid/graphics/Canvas;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 2123
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 2124
    const v1, 0x1060012

    const-string v2, "outline_color"

    .line 2123
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v1

    .line 2128
    :try_start_e
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2127
    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_1f} :catch_30

    move-result-object v0

    .line 2133
    :goto_20
    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2134
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 2135
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bk:Lcom/android/launcher2/cx;

    invoke-virtual {v2, v0, p2, v1, v1}, Lcom/android/launcher2/cx;->c(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    .line 2136
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2137
    return-object v0

    .line 2130
    :catch_30
    move-exception v0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2131
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_20
.end method

.method private c(FF)Lcom/android/launcher2/CellLayout;
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 2986
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v5

    .line 2987
    const/4 v3, 0x0

    .line 2988
    const v2, 0x7f7fffff

    .line 2990
    const/4 v0, 0x0

    move v4, v0

    :goto_a
    if-lt v4, v5, :cond_e

    move-object v0, v3

    .line 3024
    :cond_d
    return-object v0

    .line 2991
    :cond_e
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2993
    const/4 v1, 0x2

    new-array v6, v1, [F

    const/4 v1, 0x0

    aput p1, v6, v1

    const/4 v1, 0x1

    aput p2, v6, v1

    .line 2996
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget-object v7, p0, Lcom/android/launcher2/Workspace;->be:Landroid/graphics/Matrix;

    invoke-virtual {v1, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 2997
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->be:Landroid/graphics/Matrix;

    invoke-direct {p0, v0, v6, v1}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    .line 2999
    const/4 v1, 0x0

    aget v1, v6, v1

    const/4 v7, 0x0

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_53

    const/4 v1, 0x0

    aget v1, v6, v1

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getWidth()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v1, v1, v7

    if-gtz v1, :cond_53

    .line 3000
    const/4 v1, 0x1

    aget v1, v6, v1

    const/4 v7, 0x0

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_53

    const/4 v1, 0x1

    aget v1, v6, v1

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v1, v1, v7

    if-lez v1, :cond_d

    .line 3004
    :cond_53
    iget-object v7, p0, Lcom/android/launcher2/Workspace;->bc:[F

    .line 3007
    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    aput v8, v7, v1

    .line 3008
    const/4 v1, 0x1

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    aput v8, v7, v1

    .line 3009
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v1

    iget v8, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v9, -0x3e7

    if-eq v8, v9, :cond_80

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->x:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalX()I

    move-result v1

    :cond_80
    const/4 v8, 0x0

    aget v9, v7, v8

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int/2addr v1, v10

    int-to-float v1, v1

    sub-float v1, v9, v1

    aput v1, v7, v8

    const/4 v1, 0x1

    aget v8, v7, v1

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollY()I

    move-result v9

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    sub-float/2addr v8, v9

    aput v8, v7, v1

    .line 3011
    const/4 v1, 0x0

    aput p1, v6, v1

    .line 3012
    const/4 v1, 0x1

    aput p2, v6, v1

    .line 3016
    const/4 v1, 0x0

    aget v1, v6, v1

    const/4 v6, 0x0

    aget v6, v7, v6

    sub-float/2addr v1, v6

    const/4 v6, 0x1

    aget v6, v7, v6

    const/4 v8, 0x1

    aget v7, v7, v8

    sub-float/2addr v6, v7

    mul-float/2addr v1, v1

    mul-float/2addr v6, v6

    add-float/2addr v1, v6

    .line 3018
    cmpg-float v6, v1, v2

    if-gez v6, :cond_c2

    move v11, v1

    move-object v1, v0

    move v0, v11

    .line 2990
    :goto_bb
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto/16 :goto_a

    :cond_c2
    move v0, v2

    move-object v1, v3

    goto :goto_bb
.end method

.method static synthetic c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private static d(II)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1019
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    .line 1026
    const v1, 0x3e9d89d7

    mul-float/2addr v0, v1

    const v1, 0x3f80fc10

    add-float/2addr v0, v1

    return v0
.end method

.method static synthetic d(Lcom/android/launcher2/Workspace;)[F
    .registers 2
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->ba:[F

    return-object v0
.end method

.method static synthetic e(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/CellLayout;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    return-object v0
.end method

.method private e(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 1679
    if-le p1, p2, :cond_26

    .line 1685
    :goto_3
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 1687
    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1688
    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v1, v0

    .line 1690
    :goto_13
    if-le v1, v2, :cond_16

    .line 1695
    return-void

    .line 1691
    :cond_16
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 1692
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 1693
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    .line 1690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_13

    :cond_26
    move v4, p2

    move p2, p1

    move p1, v4

    goto :goto_3
.end method

.method private f(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2808
    iget v0, p0, Lcom/android/launcher2/Workspace;->aQ:I

    if-ne p1, v0, :cond_8

    iget v0, p0, Lcom/android/launcher2/Workspace;->aR:I

    if-eq p2, v0, :cond_10

    .line 2809
    :cond_8
    iput p1, p0, Lcom/android/launcher2/Workspace;->aQ:I

    .line 2810
    iput p2, p0, Lcom/android/launcher2/Workspace;->aR:I

    .line 2811
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setDragMode(I)V

    .line 2813
    :cond_10
    return-void
.end method

.method private f(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 2853
    if-eqz p1, :cond_8

    .line 2854
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bz:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 2856
    :cond_8
    iput v1, p0, Lcom/android/launcher2/Workspace;->bJ:I

    .line 2857
    iput v1, p0, Lcom/android/launcher2/Workspace;->bK:I

    .line 2858
    return-void
.end method

.method private static f(Lcom/android/launcher2/bz;)Z
    .registers 2
    .parameter

    .prologue
    .line 3060
    iget-object v0, p0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/fz;

    if-nez v0, :cond_e

    .line 3061
    iget-object v0, p0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/iw;

    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    .line 3060
    goto :goto_d
.end method

.method static synthetic f(Lcom/android/launcher2/Workspace;)[I
    .registers 2
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    return-object v0
.end method

.method static synthetic g(Lcom/android/launcher2/Workspace;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter

    .prologue
    .line 180
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private g(Landroid/view/View;)Landroid/graphics/RectF;
    .registers 14
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 4604
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    .line 4605
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWidth()I

    move-result v5

    .line 4606
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getHeight()I

    move-result v2

    .line 4607
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v6

    .line 4609
    add-int/lit8 v7, v0, -0x1

    .line 4612
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/android/launcher2/Workspace;->cE:F

    mul-float/2addr v0, v3

    float-to-int v8, v0

    .line 4613
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/android/launcher2/Workspace;->cE:F

    mul-float/2addr v0, v3

    float-to-int v9, v0

    .line 4618
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cC:[[I

    aget-object v0, v0, v7

    array-length v0, v0

    div-int v10, v2, v0

    move v0, v1

    move v2, v1

    .line 4620
    :goto_2e
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->cC:[[I

    aget-object v3, v3, v7

    array-length v3, v3

    if-lt v0, v3, :cond_3b

    .line 4638
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    :goto_3a
    return-object v0

    .line 4621
    :cond_3b
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->cC:[[I

    aget-object v3, v3, v7

    aget v3, v3, v0

    div-int v11, v5, v3

    move v3, v1

    .line 4623
    :goto_44
    iget-object v4, p0, Lcom/android/launcher2/Workspace;->cC:[[I

    aget-object v4, v4, v7

    aget v4, v4, v0

    if-lt v3, v4, :cond_4f

    .line 4620
    :cond_4c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    .line 4624
    :cond_4f
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gt v2, v4, :cond_4c

    .line 4625
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4627
    if-ne p1, v4, :cond_86

    .line 4628
    new-instance v1, Landroid/graphics/RectF;

    mul-int v2, v11, v3

    add-int/2addr v2, v6

    .line 4629
    sub-int v4, v11, v8

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    mul-int v4, v10, v0

    .line 4630
    sub-int v5, v10, v9

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    .line 4631
    mul-int/2addr v3, v11

    add-int/2addr v3, v6

    sub-int v5, v11, v8

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 4632
    add-int/2addr v3, v8

    int-to-float v3, v3

    mul-int/2addr v0, v10

    .line 4633
    sub-int v5, v10, v9

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    add-int/2addr v0, v9

    int-to-float v0, v0

    .line 4628
    invoke-direct {v1, v2, v4, v3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v0, v1

    goto :goto_3a

    .line 4635
    :cond_86
    add-int/lit8 v4, v2, 0x1

    .line 4623
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_44
.end method

.method private g(II)V
    .registers 13
    .parameter
    .parameter

    .prologue
    .line 4728
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/android/launcher2/CellLayout;

    .line 4729
    invoke-virtual {v7}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v9

    .line 4730
    const/4 v0, 0x0

    move v8, v0

    :goto_11
    if-lt v8, v9, :cond_14

    .line 4738
    return-void

    .line 4731
    :cond_14
    invoke-virtual {v7}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4732
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 4733
    check-cast v1, Lcom/android/launcher2/di;

    .line 4734
    if-eqz v1, :cond_30

    .line 4735
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-wide v2, v1, Lcom/android/launcher2/di;->j:J

    iget v5, v1, Lcom/android/launcher2/di;->l:I

    iget v6, v1, Lcom/android/launcher2/di;->m:I

    move v4, p2

    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    .line 4730
    :cond_30
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_11
.end method

.method private static g(Lcom/android/launcher2/bz;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4659
    invoke-static {p0}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/bz;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 4660
    iget-object v0, p0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/di;

    .line 4661
    iget v3, v0, Lcom/android/launcher2/di;->n:I

    if-ne v3, v1, :cond_16

    iget v0, v0, Lcom/android/launcher2/di;->o:I

    if-ne v0, v1, :cond_16

    move v0, v1

    .line 4663
    :goto_15
    return v0

    :cond_16
    move v0, v2

    .line 4661
    goto :goto_15

    :cond_18
    move v0, v2

    .line 4663
    goto :goto_15
.end method

.method private getScrollRange()I
    .registers 3

    .prologue
    .line 1045
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->p(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->p(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method static synthetic h(Lcom/android/launcher2/Workspace;)Landroid/app/WallpaperManager;
    .registers 2
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    return-object v0
.end method

.method static synthetic i(Lcom/android/launcher2/Workspace;)[F
    .registers 2
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bY:[F

    return-object v0
.end method

.method private j(I)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 1199
    iget v0, p0, Lcom/android/launcher2/Workspace;->W:F

    .line 1200
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->p(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->q(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1201
    iput v4, p0, Lcom/android/launcher2/Workspace;->W:F

    .line 1202
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->p(I)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->q(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 1203
    iput v0, p0, Lcom/android/launcher2/Workspace;->W:F

    .line 1204
    if-lez v1, :cond_24

    .line 1205
    mul-float v0, v4, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/Workspace;->aL:F

    .line 1209
    :goto_23
    return-void

    .line 1207
    :cond_24
    iput v4, p0, Lcom/android/launcher2/Workspace;->aL:F

    goto :goto_23
.end method

.method static synthetic j(Lcom/android/launcher2/Workspace;)[F
    .registers 2
    .parameter

    .prologue
    .line 263
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->ce:[F

    return-object v0
.end method

.method static synthetic k(Lcom/android/launcher2/Workspace;)V
    .registers 1
    .parameter

    .prologue
    .line 1709
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    return-void
.end method

.method static synthetic l(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/bk;
    .registers 2
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    return-object v0
.end method

.method private setChildrenBackgroundAlphaMultipliers(F)V
    .registers 4
    .parameter

    .prologue
    .line 1486
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_9

    .line 1490
    return-void

    .line 1487
    :cond_9
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 1488
    invoke-virtual {v0, p1}, Lcom/android/launcher2/CellLayout;->setBackgroundAlphaMultiplier(F)V

    .line 1486
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method


# virtual methods
.method protected final B()Z
    .registers 3

    .prologue
    .line 926
    invoke-super {p0}, Lcom/android/launcher2/je;->B()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v1, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final E()V
    .registers 2

    .prologue
    .line 1350
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->af:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/android/launcher2/Workspace;->C:I

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_9
    if-nez v0, :cond_10

    .line 1351
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    .line 1353
    :cond_10
    return-void

    .line 1350
    :cond_11
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final F()Z
    .registers 3

    .prologue
    .line 1638
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_11

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->b:Z

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final G()V
    .registers 2

    .prologue
    .line 1820
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    .line 1821
    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->a()V

    .line 1822
    return-void
.end method

.method public final H()V
    .registers 5

    .prologue
    .line 3801
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 3802
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_d

    .line 3807
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3808
    return-void

    .line 3803
    :cond_d
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bM:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    .line 3804
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->h(I)V

    .line 3802
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method final I()V
    .registers 7

    .prologue
    .line 4008
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;

    move-result-object v0

    .line 4007
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 4009
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    .line 4018
    return-void

    .line 4007
    :cond_f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/ja;

    .line 4010
    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    .line 4011
    const/4 v0, 0x0

    move v2, v0

    :goto_1c
    if-ge v2, v4, :cond_8

    .line 4012
    invoke-virtual {v1, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4013
    instance-of v5, v0, Lcom/android/launcher2/bx;

    if-eqz v5, :cond_2d

    .line 4014
    iget-object v5, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    check-cast v0, Lcom/android/launcher2/bx;

    invoke-virtual {v5, v0}, Lcom/android/launcher2/bk;->b(Lcom/android/launcher2/bx;)V

    .line 4011
    :cond_2d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c
.end method

.method public final J()V
    .registers 3

    .prologue
    .line 4311
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cj:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->b()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->al:I

    .line 4312
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cj:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->a()I

    move-result v0

    .line 4313
    iget v1, p0, Lcom/android/launcher2/Workspace;->al:I

    if-le v1, v0, :cond_15

    .line 4314
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/launcher2/Workspace;->al:I

    .line 4316
    :cond_15
    iget v1, p0, Lcom/android/launcher2/Workspace;->al:I

    if-nez v1, :cond_1f

    .line 4318
    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->al:I

    .line 4320
    :cond_1f
    iget v0, p0, Lcom/android/launcher2/Workspace;->al:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Workspace;->al:I

    .line 4321
    return-void
.end method

.method final K()Lcom/android/launcher2/as;
    .registers 4

    .prologue
    .line 4324
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    .line 4325
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4326
    if-eqz v0, :cond_19

    .line 4327
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->i()Lcom/android/launcher2/as;

    move-result-object v0

    .line 4328
    if-eqz v0, :cond_18

    .line 4329
    iput v1, v0, Lcom/android/launcher2/as;->f:I

    .line 4330
    const-wide/16 v1, -0x64

    iput-wide v1, v0, Lcom/android/launcher2/as;->g:J

    .line 4334
    :cond_18
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final L()V
    .registers 7

    .prologue
    const/high16 v1, 0x3f80

    const/4 v3, 0x0

    const/high16 v2, 0x3f00

    .line 4431
    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v0, :cond_22

    move v0, v1

    .line 4432
    :goto_c
    iget-boolean v5, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v5, :cond_24

    .line 4431
    :goto_10
    invoke-virtual {v4, v0, v1}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    .line 4433
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    iget-boolean v4, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v4, :cond_1e

    move v3, v2

    :cond_1e
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    .line 4434
    return-void

    :cond_22
    move v0, v2

    .line 4431
    goto :goto_c

    :cond_24
    move v1, v3

    .line 4432
    goto :goto_10
.end method

.method final M()Z
    .registers 3

    .prologue
    .line 4667
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    iget v1, p0, Lcom/android/launcher2/Workspace;->al:I

    if-eq v0, v1, :cond_e

    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    iget v1, p0, Lcom/android/launcher2/Workspace;->al:I

    if-eq v0, v1, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method final a(Lcom/android/launcher2/kg;Z)Landroid/animation/Animator;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1843
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/kg;ZI)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/android/launcher2/kg;ZI)Landroid/animation/Animator;
    .registers 23
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1847
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_a

    .line 1848
    const/4 v3, 0x0

    .line 2002
    :goto_9
    return-object v3

    .line 1852
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->j:[F

    if-nez v3, :cond_62

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->j:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->bV:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->bW:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->bX:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->bY:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->bZ:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->ca:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->cb:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->cc:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->cd:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->ce:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->cf:[F

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->cg:[F

    .line 1854
    :cond_62
    if-eqz p2, :cond_119

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    move-object v3, v2

    .line 1857
    :goto_6a
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getNextPage()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 1859
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    .line 1860
    sget-object v2, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    if-ne v6, v2, :cond_11d

    const/4 v2, 0x1

    move v4, v2

    .line 1861
    :goto_7d
    sget-object v2, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    if-ne v6, v2, :cond_121

    const/4 v2, 0x1

    move v5, v2

    .line 1862
    :goto_83
    sget-object v2, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    if-ne v6, v2, :cond_125

    const/4 v2, 0x1

    move v6, v2

    .line 1863
    :goto_89
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    .line 1864
    sget-object v2, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_129

    const/4 v2, 0x1

    move v7, v2

    .line 1865
    :goto_97
    sget-object v2, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_12d

    const/4 v2, 0x1

    move v8, v2

    .line 1866
    :goto_9f
    sget-object v2, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_131

    const/4 v2, 0x1

    move v15, v2

    .line 1867
    :goto_a7
    const/high16 v9, 0x3f80

    .line 1868
    if-eqz v8, :cond_135

    const/high16 v2, 0x3f80

    .line 1869
    :goto_ad
    const/4 v10, 0x1

    .line 1873
    sget-object v11, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    move-object/from16 v0, p1

    if-eq v0, v11, :cond_145

    .line 1874
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/launcher2/Workspace;->bg:F

    if-eqz v15, :cond_138

    const v9, 0x3dcccccd

    :goto_bd
    sub-float/2addr v11, v9

    .line 1875
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/launcher2/Workspace;->bw:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/launcher2/Workspace;->setPageSpacing(I)V

    .line 1876
    if-eqz v4, :cond_13a

    if-eqz v15, :cond_13a

    .line 1877
    const/4 v9, 0x0

    .line 1878
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/Workspace;->setLayoutScale(F)V

    .line 1879
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->Q()V

    move v13, v2

    move v14, v11

    move v2, v9

    .line 1889
    :goto_d7
    if-eqz v2, :cond_156

    .line 1890
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f0a0004

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move v9, v2

    .line 1892
    :goto_e5
    const/4 v2, 0x0

    move v10, v2

    :goto_e7
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-lt v10, v2, :cond_163

    .line 1940
    if-eqz p2, :cond_100

    .line 1941
    const/4 v2, 0x0

    move v4, v2

    :goto_f1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-lt v4, v2, :cond_23d

    .line 1988
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->i()V

    .line 1989
    move/from16 v0, p3

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1992
    :cond_100
    if-eqz v8, :cond_370

    .line 1996
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1997
    const v4, 0x7f0a0003

    .line 1996
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    .line 1997
    const/high16 v4, 0x42c8

    div-float/2addr v2, v4

    const/4 v4, 0x0

    .line 1996
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/android/launcher2/Workspace;->a(FZ)V

    goto/16 :goto_9

    .line 1854
    :cond_119
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_6a

    .line 1860
    :cond_11d
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_7d

    .line 1861
    :cond_121
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_83

    .line 1862
    :cond_125
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_89

    .line 1864
    :cond_129
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_97

    .line 1865
    :cond_12d
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_9f

    .line 1866
    :cond_131
    const/4 v2, 0x0

    move v15, v2

    goto/16 :goto_a7

    .line 1868
    :cond_135
    const/4 v2, 0x0

    goto/16 :goto_ad

    .line 1874
    :cond_138
    const/4 v9, 0x0

    goto :goto_bd

    .line 1881
    :cond_13a
    const/high16 v2, 0x3f80

    .line 1882
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/Workspace;->setLayoutScale(F)V

    move v13, v2

    move v14, v11

    move v2, v10

    goto :goto_d7

    .line 1885
    :cond_145
    const/4 v11, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/Workspace;->setPageSpacing(I)V

    .line 1886
    const/high16 v11, 0x3f80

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/Workspace;->setLayoutScale(F)V

    move v13, v2

    move v14, v9

    move v2, v10

    goto :goto_d7

    .line 1891
    :cond_156
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f0a000b

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move v9, v2

    goto :goto_e5

    .line 1893
    :cond_163
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 1894
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/launcher2/Workspace;->bq:Z

    if-eqz v11, :cond_179

    if-nez v8, :cond_179

    .line 1895
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/launcher2/Workspace;->u:I

    if-ne v10, v11, :cond_222

    :cond_179
    const/high16 v11, 0x3f80

    .line 1896
    :goto_17b
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/launcher2/ja;->getAlpha()F

    move-result v12

    .line 1900
    if-eqz v6, :cond_187

    if-nez v7, :cond_18b

    .line 1901
    :cond_187
    if-eqz v4, :cond_379

    if-eqz v15, :cond_379

    .line 1905
    :cond_18b
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/launcher2/Workspace;->u:I

    if-eq v10, v11, :cond_195

    if-eqz p2, :cond_195

    if-eqz v5, :cond_225

    .line 1906
    :cond_195
    const/high16 v11, 0x3f80

    .line 1907
    if-eqz v4, :cond_379

    if-eqz v15, :cond_379

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    move/from16 v16, v0

    const/16 v17, 0x64

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_379

    .line 1908
    const/4 v11, 0x0

    move/from16 v18, v12

    move v12, v11

    move/from16 v11, v18

    .line 1916
    :goto_1bb
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->bZ:[F

    move-object/from16 v16, v0

    aput v11, v16, v10

    .line 1917
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aput v12, v11, v10

    .line 1918
    if-eqz p2, :cond_228

    .line 1919
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->j:[F

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getTranslationX()F

    move-result v12

    aput v12, v11, v10

    .line 1920
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->bV:[F

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getTranslationY()F

    move-result v12

    aput v12, v11, v10

    .line 1921
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->bW:[F

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getScaleX()F

    move-result v12

    aput v12, v11, v10

    .line 1922
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->bX:[F

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getScaleY()F

    move-result v12

    aput v12, v11, v10

    .line 1923
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->bY:[F

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getBackgroundAlpha()F

    move-result v2

    aput v2, v11, v10

    .line 1925
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ca:[F

    const/4 v11, 0x0

    aput v11, v2, v10

    .line 1926
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->cb:[F

    const/4 v11, 0x0

    aput v11, v2, v10

    .line 1927
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->cc:[F

    aput v14, v2, v10

    .line 1928
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->cd:[F

    aput v14, v2, v10

    .line 1929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ce:[F

    aput v13, v2, v10

    .line 1892
    :goto_21d
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto/16 :goto_e7

    .line 1895
    :cond_222
    const/4 v11, 0x0

    goto/16 :goto_17b

    .line 1911
    :cond_225
    const/4 v11, 0x0

    .line 1912
    const/4 v12, 0x0

    goto :goto_1bb

    .line 1931
    :cond_228
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 1932
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Lcom/android/launcher2/CellLayout;->setTranslationY(F)V

    .line 1933
    invoke-virtual {v2, v14}, Lcom/android/launcher2/CellLayout;->setScaleX(F)V

    .line 1934
    invoke-virtual {v2, v14}, Lcom/android/launcher2/CellLayout;->setScaleY(F)V

    .line 1935
    invoke-virtual {v2, v13}, Lcom/android/launcher2/CellLayout;->setBackgroundAlpha(F)V

    .line 1936
    invoke-virtual {v2, v12}, Lcom/android/launcher2/CellLayout;->setShortcutAndWidgetAlpha(F)V

    goto :goto_21d

    .line 1943
    :cond_23d
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 1944
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/ja;->getAlpha()F

    move-result v5

    .line 1945
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getWidth()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x4000

    div-float/2addr v6, v7

    invoke-virtual {v2, v6}, Lcom/android/launcher2/CellLayout;->setPivotX(F)V

    .line 1946
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x4000

    div-float/2addr v6, v7

    invoke-virtual {v2, v6}, Lcom/android/launcher2/CellLayout;->setPivotY(F)V

    .line 1947
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->bZ:[F

    aget v6, v6, v4

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_2bd

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aget v6, v6, v4

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_2bd

    .line 1948
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->ca:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 1949
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->cb:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setTranslationY(F)V

    .line 1950
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->cc:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setScaleX(F)V

    .line 1951
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->cd:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setScaleY(F)V

    .line 1952
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->ce:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setBackgroundAlpha(F)V

    .line 1953
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setShortcutAndWidgetAlpha(F)V

    .line 1954
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->cg:[F

    aget v5, v5, v4

    invoke-virtual {v2, v5}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 1941
    :cond_2b8
    :goto_2b8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_f1

    .line 1956
    :cond_2bd
    new-instance v6, Lcom/android/launcher2/ho;

    invoke-direct {v6, v2}, Lcom/android/launcher2/ho;-><init>(Landroid/view/View;)V

    .line 1957
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->ca:[F

    aget v7, v7, v4

    invoke-virtual {v6, v7}, Lcom/android/launcher2/ho;->a(F)Lcom/android/launcher2/ho;

    move-result-object v7

    .line 1958
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/Workspace;->cb:[F

    aget v10, v10, v4

    invoke-virtual {v7, v10}, Lcom/android/launcher2/ho;->b(F)Lcom/android/launcher2/ho;

    move-result-object v7

    .line 1959
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/Workspace;->cc:[F

    aget v10, v10, v4

    invoke-virtual {v7, v10}, Lcom/android/launcher2/ho;->c(F)Lcom/android/launcher2/ho;

    move-result-object v7

    .line 1960
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/Workspace;->cd:[F

    aget v10, v10, v4

    invoke-virtual {v7, v10}, Lcom/android/launcher2/ho;->d(F)Lcom/android/launcher2/ho;

    move-result-object v7

    .line 1961
    int-to-long v10, v9

    invoke-virtual {v7, v10, v11}, Lcom/android/launcher2/ho;->setDuration(J)Landroid/animation/Animator;

    move-result-object v7

    .line 1962
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/Workspace;->ci:Lcom/android/launcher2/kj;

    invoke-virtual {v7, v10}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1963
    invoke-virtual {v3, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1965
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->bZ:[F

    aget v6, v6, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aget v7, v7, v4

    cmpl-float v6, v6, v7

    if-nez v6, :cond_313

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aget v6, v6, v4

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_335

    .line 1967
    :cond_313
    new-instance v5, Lcom/android/launcher2/ho;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/launcher2/ho;-><init>(Landroid/view/View;)V

    .line 1968
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->cf:[F

    aget v6, v6, v4

    invoke-virtual {v5, v6}, Lcom/android/launcher2/ho;->e(F)Lcom/android/launcher2/ho;

    move-result-object v6

    .line 1969
    int-to-long v10, v9

    invoke-virtual {v6, v10, v11}, Lcom/android/launcher2/ho;->setDuration(J)Landroid/animation/Animator;

    move-result-object v6

    .line 1970
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->ci:Lcom/android/launcher2/kj;

    invoke-virtual {v6, v7}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1971
    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1973
    :cond_335
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->bY:[F

    aget v5, v5, v4

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_34b

    .line 1974
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->ce:[F

    aget v5, v5, v4

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_2b8

    .line 1975
    :cond_34b
    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_380

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v5

    int-to-long v6, v9

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 1976
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->ci:Lcom/android/launcher2/kj;

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1977
    new-instance v6, Lcom/android/launcher2/jx;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v2, v4}, Lcom/android/launcher2/jx;-><init>(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/CellLayout;I)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1984
    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2b8

    .line 2000
    :cond_370
    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/android/launcher2/Workspace;->a(FZ)V

    goto/16 :goto_9

    :cond_379
    move/from16 v18, v12

    move v12, v11

    move/from16 v11, v18

    goto/16 :goto_1bb

    .line 1975
    :array_380
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(Ljava/lang/Object;)Lcom/android/launcher2/Folder;
    .registers 8
    .parameter

    .prologue
    .line 3975
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;

    move-result-object v0

    .line 3974
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 3976
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 3988
    const/4 v0, 0x0

    :cond_f
    return-object v0

    .line 3974
    :cond_10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/ja;

    .line 3977
    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    .line 3978
    const/4 v0, 0x0

    move v2, v0

    :goto_1d
    if-ge v2, v4, :cond_8

    .line 3979
    invoke-virtual {v1, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3980
    instance-of v5, v0, Lcom/android/launcher2/Folder;

    if-eqz v5, :cond_37

    .line 3981
    check-cast v0, Lcom/android/launcher2/Folder;

    .line 3982
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getInfo()Lcom/android/launcher2/cu;

    move-result-object v5

    if-ne v5, p1, :cond_37

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getInfo()Lcom/android/launcher2/cu;

    move-result-object v5

    iget-boolean v5, v5, Lcom/android/launcher2/cu;->a:Z

    if-nez v5, :cond_f

    .line 3978
    :cond_37
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1d
.end method

.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 424
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->d:Z

    .line 425
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 426
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->d(Z)V

    .line 429
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/InstallShortcutReceiver;->a(Landroid/content/Context;)V

    .line 430
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/UninstallShortcutReceiver;->a(Landroid/content/Context;)V

    .line 431
    return-void
.end method

.method public final a(F)V
    .registers 2
    .parameter

    .prologue
    .line 2017
    iput p1, p0, Lcom/android/launcher2/Workspace;->ch:F

    .line 2018
    return-void
.end method

.method public final a(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 4692
    .line 4694
    if-le p1, p2, :cond_4e

    move v0, p1

    move v1, p2

    .line 4699
    :goto_4
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4700
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 4701
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->detachViewFromParent(Landroid/view/View;)V

    .line 4702
    invoke-virtual {p0, v2, p2, v3}, Lcom/android/launcher2/Workspace;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4703
    :goto_12
    if-le v1, v0, :cond_34

    .line 4707
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4708
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    .line 4709
    const/4 v0, 0x0

    :goto_1e
    if-lt v0, v2, :cond_3a

    .line 4712
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->removeAllViews()V

    .line 4713
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_27
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_44

    .line 4716
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->requestLayout()V

    .line 4717
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 4718
    return-void

    .line 4704
    :cond_34
    invoke-direct {p0, v1, v1}, Lcom/android/launcher2/Workspace;->g(II)V

    .line 4703
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 4710
    :cond_3a
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4709
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 4713
    :cond_44
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 4714
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->addView(Landroid/view/View;)V

    goto :goto_27

    :cond_4e
    move v0, p2

    move v1, p1

    goto :goto_4
.end method

.method protected final a(ILjava/lang/Runnable;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1188
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    if-eqz v0, :cond_9

    .line 1189
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1191
    :cond_9
    iput-object p2, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    .line 1192
    const/16 v0, 0x3b6

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/Workspace;->c(II)V

    .line 1193
    return-void
.end method

.method public final a(IZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4199
    return-void
.end method

.method protected final a(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x3f80

    const/4 v3, 0x0

    const v5, 0x3f060a92

    .line 879
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 922
    :cond_d
    :goto_d
    return-void

    .line 880
    :cond_e
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->k()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 881
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 882
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 883
    iget v2, p0, Lcom/android/launcher2/Workspace;->bG:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 884
    iget v0, p0, Lcom/android/launcher2/Workspace;->bH:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 886
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->ap:Z

    if-eqz v0, :cond_6f

    .line 887
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_d

    .line 896
    :cond_35
    div-float v0, v1, v2

    .line 897
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->ap:Z

    if-eqz v3, :cond_3d

    .line 898
    div-float v0, v2, v1

    .line 900
    :cond_3d
    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->atan(D)D

    move-result-wide v3

    double-to-float v0, v3

    .line 902
    iget v3, p0, Lcom/android/launcher2/Workspace;->G:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_51

    iget v2, p0, Lcom/android/launcher2/Workspace;->G:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_54

    .line 903
    :cond_51
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->x()V

    .line 906
    :cond_54
    const v1, 0x3f860a92

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_d

    .line 909
    cmpl-float v1, v0, v5

    if-lez v1, :cond_76

    .line 914
    sub-float/2addr v0, v5

    .line 916
    div-float/2addr v0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 917
    const/high16 v1, 0x4080

    mul-float/2addr v0, v1

    add-float/2addr v0, v6

    invoke-super {p0, p1, v0, v7}, Lcom/android/launcher2/je;->a(Landroid/view/MotionEvent;FZ)V

    goto :goto_d

    .line 891
    :cond_6f
    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_35

    goto :goto_d

    .line 920
    :cond_76
    invoke-super {p0, p1, v6, v7}, Lcom/android/launcher2/je;->a(Landroid/view/MotionEvent;FZ)V

    goto :goto_d
.end method

.method public final a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 1803
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 1806
    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/Workspace;->c(Landroid/view/View;Landroid/graphics/Canvas;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 1807
    return-void
.end method

.method final a(Landroid/view/View;JIIIII)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 529
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 530
    return-void
.end method

.method final a(Landroid/view/View;JIIIIIZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 546
    const-wide/16 v0, -0x64

    cmp-long v0, p2, v0

    if-nez v0, :cond_37

    .line 547
    if-ltz p4, :cond_e

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt p4, v0, :cond_37

    .line 548
    :cond_e
    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The screen must be >= 0 and < "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 549
    const-string v2, " (was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "); skipping child"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 548
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    :cond_36
    :goto_36
    return-void

    .line 555
    :cond_37
    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_f6

    .line 556
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/android/launcher2/Hotseat;->j(I)Lcom/android/launcher2/CellLayout;

    move-result-object v1

    .line 557
    if-eqz v1, :cond_36

    .line 560
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 563
    instance-of v0, p1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_58

    move-object v0, p1

    .line 564
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/FolderIcon;->setTextVisible(Z)V

    .line 567
    :cond_58
    rem-int/lit8 v0, p4, 0x64

    .line 568
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Hotseat;->g(I)I

    move-result p5

    .line 569
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Hotseat;->h(I)I

    move-result p6

    move-object v0, v1

    .line 592
    :goto_6f
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 594
    if-eqz v1, :cond_79

    instance-of v2, v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    if-nez v2, :cond_149

    .line 595
    :cond_79
    new-instance v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-direct {v4, p5, p6, p7, p8}, Lcom/android/launcher2/CellLayout$LayoutParams;-><init>(IIII)V

    .line 604
    :goto_7e
    if-gez p7, :cond_85

    if-gez p8, :cond_85

    .line 605
    const/4 v1, 0x0

    iput-boolean v1, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 609
    :cond_85
    invoke-static {p2, p3, p4, p5, p6}, Lcom/android/launcher2/gb;->a(JIII)I

    move-result v3

    .line 610
    instance-of v1, p1, Lcom/android/launcher2/Folder;

    if-eqz v1, :cond_156

    const/4 v5, 0x0

    .line 611
    :goto_8e
    if-eqz p9, :cond_159

    const/4 v2, 0x0

    :goto_91
    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IILcom/android/launcher2/CellLayout$LayoutParams;Z)Z

    move-result v0

    if-nez v0, :cond_c0

    .line 615
    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to add to item at ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") to CellLayout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_c0
    instance-of v0, p1, Lcom/android/launcher2/Folder;

    if-nez v0, :cond_cd

    .line 619
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 620
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->E:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 622
    :cond_cd
    instance-of v0, p1, Lcom/android/launcher2/bx;

    if-eqz v0, :cond_d9

    .line 623
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    move-object v0, p1

    check-cast v0, Lcom/android/launcher2/bx;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 625
    :cond_d9
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    if-eqz v0, :cond_36

    .line 626
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jd;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/w;->a(Lcom/android/launcher2/jd;)V

    goto/16 :goto_36

    .line 572
    :cond_f6
    instance-of v0, p1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_11d

    move-object v0, p1

    .line 573
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 574
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object v1, p1

    check-cast v1, Lcom/android/launcher2/FolderIcon;

    iget-object v1, v1, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-wide v3, v1, Lcom/android/launcher2/cu;->j:J

    .line 573
    invoke-static {v2, v3, v4}, Lcom/anddoes/launcher/v;->a(Lcom/android/launcher2/Launcher;J)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderIcon;->setTextVisible(Z)V

    .line 588
    :cond_10d
    :goto_10d
    invoke-virtual {p0, p4}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 589
    new-instance v1, Lcom/android/launcher2/dd;

    invoke-direct {v1}, Lcom/android/launcher2/dd;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto/16 :goto_6f

    .line 575
    :cond_11d
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_10d

    .line 576
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 577
    if-eqz v0, :cond_10d

    .line 578
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-static {v1, p2, p3}, Lcom/anddoes/launcher/v;->a(Lcom/android/launcher2/Launcher;J)Z

    move-result v1

    if-eqz v1, :cond_140

    .line 579
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_10d

    move-object v1, p1

    .line 580
    check-cast v1, Lcom/android/launcher2/BubbleTextView;

    check-cast v0, Lcom/android/launcher2/jd;

    iget-object v0, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_10d

    :cond_140
    move-object v0, p1

    .line 583
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_10d

    .line 597
    :cond_149
    check-cast v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 598
    iput p5, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    .line 599
    iput p6, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 600
    iput p7, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 601
    iput p8, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    move-object v4, v1

    goto/16 :goto_7e

    .line 610
    :cond_156
    const/4 v5, 0x1

    goto/16 :goto_8e

    .line 611
    :cond_159
    const/4 v2, -0x1

    goto/16 :goto_91
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/bt;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/high16 v9, 0x4000

    const/4 v10, 0x0

    .line 2204
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2207
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;Landroid/graphics/Canvas;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2209
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 2210
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 2212
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    iget-object v6, p0, Lcom/android/launcher2/Workspace;->bn:[I

    invoke-virtual {v2, p1, v6}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 2214
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bn:[I

    aget v2, v2, v10

    int-to-float v2, v2

    int-to-float v6, v5

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    div-float/2addr v6, v9

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2216
    iget-object v6, p0, Lcom/android/launcher2/Workspace;->bn:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    int-to-float v6, v6

    int-to-float v7, v3

    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v8

    int-to-float v3, v3

    mul-float/2addr v3, v8

    sub-float v3, v7, v3

    div-float/2addr v3, v9

    sub-float v3, v6, v3

    .line 2217
    const/high16 v6, 0x3f80

    .line 2216
    sub-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2221
    instance-of v6, p1, Lcom/android/launcher2/BubbleTextView;

    if-nez v6, :cond_5c

    instance-of v6, p1, Lcom/android/launcher2/PagedViewIcon;

    if-eqz v6, :cond_a5

    .line 2222
    :cond_5c
    const v0, 0x7f0b004f

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2223
    const v6, 0x7f0b0045

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2224
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    .line 2225
    sub-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    .line 2226
    add-int v8, v5, v0

    .line 2227
    add-int v9, v6, v0

    .line 2228
    add-int/2addr v3, v6

    .line 2231
    new-instance v0, Landroid/graphics/Point;

    const/4 v7, -0x1

    .line 2232
    add-int/lit8 v4, v4, -0x1

    .line 2231
    invoke-direct {v0, v7, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 2233
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v5, v6, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v0

    .line 2240
    :goto_84
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_8e

    move-object v0, p1

    .line 2241
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 2242
    invoke-virtual {v0}, Lcom/android/launcher2/BubbleTextView;->a()V

    .line 2245
    :cond_8e
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    .line 2246
    sget v4, Lcom/android/launcher2/bk;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v8

    move-object v4, p2

    .line 2245
    invoke-virtual/range {v0 .. v8}, Lcom/android/launcher2/bk;->a(Landroid/graphics/Bitmap;IILcom/android/launcher2/bt;Ljava/lang/Object;Landroid/graphics/Point;Landroid/graphics/Rect;F)V

    .line 2247
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2250
    invoke-virtual {p0, v10}, Lcom/android/launcher2/Workspace;->d(Z)V

    .line 2251
    return-void

    .line 2234
    :cond_a5
    instance-of v5, p1, Lcom/android/launcher2/FolderIcon;

    if-eqz v5, :cond_bb

    .line 2235
    const v5, 0x7f0b006a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2236
    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-direct {v7, v10, v10, v5, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v0

    goto :goto_84

    :cond_bb
    move-object v7, v0

    move-object v6, v0

    goto :goto_84
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 3702
    if-eqz p4, :cond_4d

    .line 3703
    if-eq p1, p0, :cond_34

    .line 3704
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    if-eqz v0, :cond_34

    .line 3705
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3706
    if-eqz v0, :cond_21

    .line 3707
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v1, v1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 3709
    :cond_21
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    instance-of v0, v0, Lcom/android/launcher2/bx;

    if-eqz v0, :cond_34

    .line 3710
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    check-cast v0, Lcom/android/launcher2/bx;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/bk;->b(Lcom/android/launcher2/bx;)V

    .line 3723
    :cond_34
    :goto_34
    iget-boolean v0, p2, Lcom/android/launcher2/bz;->j:Z

    if-eqz v0, :cond_45

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    if-eqz v0, :cond_45

    .line 3724
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3726
    :cond_45
    iput-object v3, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 3727
    iput-object v3, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    .line 3730
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3731
    return-void

    .line 3714
    :cond_4d
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v0, :cond_34

    .line 3716
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 3717
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    .line 3721
    :goto_62
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;)V

    goto :goto_34

    .line 3719
    :cond_6a
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v0, v0, Lcom/android/launcher2/as;->f:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    goto :goto_62
.end method

.method public final a(Lcom/android/launcher2/CellLayout;)V
    .registers 3
    .parameter

    .prologue
    .line 3642
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-eqz v0, :cond_3b

    .line 3643
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bQ:F

    .line 3644
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getScaleY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bR:F

    .line 3645
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getTranslationX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bT:F

    .line 3646
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getTranslationY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bU:F

    .line 3647
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getRotationY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bS:F

    .line 3648
    iget v0, p0, Lcom/android/launcher2/Workspace;->bQ:F

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setScaleX(F)V

    .line 3649
    iget v0, p0, Lcom/android/launcher2/Workspace;->bR:F

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setScaleY(F)V

    .line 3650
    iget v0, p0, Lcom/android/launcher2/Workspace;->bT:F

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 3651
    iget v0, p0, Lcom/android/launcher2/Workspace;->bU:F

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setTranslationY(F)V

    .line 3652
    iget v0, p0, Lcom/android/launcher2/Workspace;->bS:F

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 3654
    :cond_3b
    return-void
.end method

.method public final a(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2007
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    .line 2008
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->C()V

    .line 2009
    return-void
.end method

.method final a(Lcom/android/launcher2/as;)V
    .registers 5
    .parameter

    .prologue
    .line 2176
    iget-object v1, p1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    .line 2179
    invoke-virtual {v1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2201
    :goto_8
    return-void

    .line 2183
    :cond_9
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    .line 2184
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2185
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2186
    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;)V

    .line 2188
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 2189
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 2191
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ao:Z

    if-eqz v0, :cond_36

    .line 2192
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 2193
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2, v0, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/di;Landroid/view/View;)V

    .line 2196
    :cond_36
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 2199
    invoke-direct {p0, v1, v0}, Lcom/android/launcher2/Workspace;->c(Landroid/view/View;Landroid/graphics/Canvas;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 2200
    invoke-virtual {p0, v1, p0}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;Lcom/android/launcher2/bt;)V

    goto :goto_8
.end method

.method public final a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->d:Z

    .line 415
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 416
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->x()V

    .line 417
    const/high16 v0, 0x3f80

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->setChildrenBackgroundAlphaMultipliers(F)V

    .line 419
    invoke-static {}, Lcom/android/launcher2/InstallShortcutReceiver;->a()V

    .line 420
    invoke-static {}, Lcom/android/launcher2/UninstallShortcutReceiver;->a()V

    .line 421
    return-void
.end method

.method public final a(Lcom/android/launcher2/bz;)V
    .registers 30
    .parameter

    .prologue
    .line 2485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->F()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2696
    :cond_a
    :goto_a
    return-void

    .line 2488
    :cond_b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/launcher2/bz;->a:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/bz;->b:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/bz;->c:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/bz;->d:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    .line 2489
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->ba:[F

    move-object/from16 v2, p0

    .line 2488
    invoke-direct/range {v2 .. v8}, Lcom/android/launcher2/Workspace;->a(IIIILcom/android/launcher2/bu;[F)[F

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    .line 2491
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    .line 2492
    const/4 v2, 0x0

    .line 2495
    if-eqz v6, :cond_1d0

    .line 2496
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3, v6}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1c6

    .line 2497
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    invoke-static {v2, v3}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[F)V

    .line 2498
    const/4 v2, 0x1

    move/from16 v21, v2

    .line 2504
    :goto_50
    const/16 v26, -0x1

    .line 2505
    const/16 v24, 0x0

    .line 2506
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    move-object/from16 v0, p0

    if-eq v2, v0, :cond_332

    .line 2507
    const/4 v2, 0x2

    new-array v7, v2, [I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v3, v3

    aput v3, v7, v2

    const/4 v2, 0x1

    .line 2508
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    float-to-int v3, v3

    aput v3, v7, v2

    .line 2509
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    new-instance v20, Lcom/android/launcher2/kb;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/launcher2/kb;-><init>(Lcom/android/launcher2/Workspace;)V

    move-object/from16 v17, v2

    check-cast v17, Lcom/android/launcher2/di;

    move-object/from16 v0, v17

    iget v4, v0, Lcom/android/launcher2/di;->n:I

    move-object/from16 v0, v17

    iget v5, v0, Lcom/android/launcher2/di;->o:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v3, :cond_a0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v4, v3, Lcom/android/launcher2/as;->d:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v5, v3, Lcom/android/launcher2/as;->e:I

    :cond_a0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3, v6}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1d4

    const/16 v3, -0x65

    :goto_ac
    int-to-long v0, v3

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v8, v6}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_1d8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    invoke-virtual {v3, v6, v8, v9}, Lcom/android/launcher2/Hotseat;->a(Lcom/android/launcher2/CellLayout;II)I

    move-result v3

    move/from16 v19, v3

    :goto_db
    move-object/from16 v0, v17

    instance-of v3, v0, Lcom/android/launcher2/iu;

    if-eqz v3, :cond_1f1

    move-object/from16 v18, v2

    check-cast v18, Lcom/android/launcher2/iu;

    const/4 v10, 0x1

    move-object/from16 v0, v18

    iget v2, v0, Lcom/android/launcher2/iu;->i:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_679

    const/4 v2, 0x0

    aget v2, v7, v2

    const/4 v3, 0x1

    aget v3, v7, v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    invoke-static/range {v2 .. v7}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    invoke-virtual {v6, v2, v3, v4}, Lcom/android/launcher2/CellLayout;->a(FF[I)F

    move-result v8

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v5, Lcom/android/launcher2/di;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v9, 0x1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;[IFZ)Z

    move-result v2

    if-nez v2, :cond_13a

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/di;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6, v3, v8}, Lcom/android/launcher2/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher2/CellLayout;[IF)Z

    move-result v2

    if-eqz v2, :cond_679

    :cond_13a
    const/4 v2, 0x0

    move v3, v2

    :goto_13c
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/di;

    if-eqz v3, :cond_188

    iget v9, v2, Lcom/android/launcher2/di;->n:I

    iget v10, v2, Lcom/android/launcher2/di;->o:I

    iget v3, v2, Lcom/android/launcher2/di;->p:I

    if-lez v3, :cond_154

    iget v3, v2, Lcom/android/launcher2/di;->q:I

    if-lez v3, :cond_154

    iget v9, v2, Lcom/android/launcher2/di;->p:I

    iget v10, v2, Lcom/android/launcher2/di;->q:I

    :cond_154
    const/4 v3, 0x2

    new-array v15, v3, [I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v7, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    float-to-int v8, v3

    move-object/from16 v0, v17

    iget v11, v0, Lcom/android/launcher2/di;->n:I

    move-object/from16 v0, v17

    iget v12, v0, Lcom/android/launcher2/di;->o:I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/16 v16, 0x2

    invoke-virtual/range {v6 .. v16}, Lcom/android/launcher2/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x0

    aget v3, v15, v3

    iput v3, v2, Lcom/android/launcher2/di;->n:I

    const/4 v3, 0x1

    aget v3, v15, v3

    iput v3, v2, Lcom/android/launcher2/di;->o:I

    :cond_188
    new-instance v7, Lcom/android/launcher2/kc;

    move-object/from16 v8, p0

    move-object/from16 v9, v18

    move-object v10, v2

    move-wide/from16 v11, v21

    move/from16 v13, v19

    invoke-direct/range {v7 .. v13}, Lcom/android/launcher2/kc;-><init>(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/iu;Lcom/android/launcher2/di;JI)V

    move-object/from16 v0, v18

    iget v2, v0, Lcom/android/launcher2/iu;->i:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1ef

    move-object/from16 v2, v18

    check-cast v2, Lcom/android/launcher2/iw;

    iget-object v14, v2, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    :goto_1a3
    const/4 v13, 0x0

    move-object/from16 v0, v18

    iget v2, v0, Lcom/android/launcher2/iu;->i:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1b6

    check-cast v18, Lcom/android/launcher2/iw;

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, v2, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v2, :cond_1b6

    const/4 v13, 0x1

    :cond_1b6
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/4 v15, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, v17

    move-object v10, v6

    move-object v12, v7

    invoke-virtual/range {v8 .. v15}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/bu;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    goto/16 :goto_a

    .line 2500
    :cond_1c6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v3, v4}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    :cond_1d0
    move/from16 v21, v2

    goto/16 :goto_50

    .line 2509
    :cond_1d4
    const/16 v3, -0x64

    goto/16 :goto_ac

    :cond_1d8
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/launcher2/Workspace;->u:I

    if-eq v3, v8, :cond_1eb

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v9, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    if-eq v8, v9, :cond_1eb

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Workspace;->r(I)V

    :cond_1eb
    move/from16 v19, v3

    goto/16 :goto_db

    :cond_1ef
    const/4 v14, 0x0

    goto :goto_1a3

    :cond_1f1
    move-object/from16 v0, v17

    iget v2, v0, Lcom/android/launcher2/di;->i:I

    packed-switch v2, :pswitch_data_67c

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown item type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    iget v4, v0, Lcom/android/launcher2/di;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_211
    move-object/from16 v0, v17

    iget-wide v2, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v8, -0x1

    cmp-long v2, v2, v8

    if-nez v2, :cond_675

    move-object/from16 v0, v17

    instance-of v2, v0, Lcom/android/launcher2/h;

    if-eqz v2, :cond_675

    new-instance v3, Lcom/android/launcher2/jd;

    check-cast v17, Lcom/android/launcher2/h;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Lcom/android/launcher2/jd;-><init>(Lcom/android/launcher2/h;)V

    :goto_22a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object v2, v3

    check-cast v2, Lcom/android/launcher2/jd;

    invoke-virtual {v8, v6, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/ViewGroup;Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v2

    move-object/from16 v17, v3

    move-object/from16 v18, v2

    :goto_239
    const/4 v2, 0x0

    aget v2, v7, v2

    const/4 v3, 0x1

    aget v3, v7, v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    invoke-static/range {v2 .. v7}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    invoke-virtual {v6, v2, v3, v4}, Lcom/android/launcher2/CellLayout;->a(FF[I)F

    move-result v8

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/launcher2/bz;->i:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v9, 0x1

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/android/launcher2/bz;->i:Ljava/lang/Runnable;

    move-object/from16 v2, p0

    move-object/from16 v3, v18

    move-wide/from16 v4, v21

    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JLcom/android/launcher2/CellLayout;[IFZLcom/android/launcher2/bu;Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v10, 0x1

    move-object/from16 v5, p0

    move-object/from16 v9, p1

    invoke-virtual/range {v5 .. v10}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;[IFLcom/android/launcher2/bz;Z)Z

    move-result v2

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v7, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v8, v2

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v15, 0x0

    const/16 v16, 0x2

    invoke-virtual/range {v6 .. v16}, Lcom/android/launcher2/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x0

    aget v12, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x1

    aget v13, v2, v3

    move-object/from16 v0, v17

    iget v14, v0, Lcom/android/launcher2/di;->n:I

    move-object/from16 v0, v17

    iget v15, v0, Lcom/android/launcher2/di;->o:I

    const/16 v16, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, v18

    move-wide/from16 v9, v21

    move/from16 v11, v19

    invoke-virtual/range {v7 .. v16}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    invoke-static/range {v18 .. v18}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;)V

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-virtual {v6}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/android/launcher2/ja;->a(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget v12, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v13, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    move-object/from16 v8, v17

    move-wide/from16 v9, v21

    move/from16 v11, v19

    invoke-static/range {v7 .. v13}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->setFinalTransitionTransform(Lcom/android/launcher2/CellLayout;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v2, v3, v0, v1}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/view/View;Ljava/lang/Runnable;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;)V

    goto/16 :goto_a

    :pswitch_31e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object/from16 v2, v17

    check-cast v2, Lcom/android/launcher2/cu;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->aW:Lcom/android/launcher2/da;

    invoke-static {v3, v6, v2}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/Launcher;Landroid/view/ViewGroup;Lcom/android/launcher2/cu;)Lcom/android/launcher2/FolderIcon;

    move-result-object v2

    move-object/from16 v18, v2

    goto/16 :goto_239

    .line 2510
    :cond_332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v2, :cond_a

    .line 2511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v8, v2, Lcom/android/launcher2/as;->a:Landroid/view/View;

    .line 2513
    const/16 v25, 0x0

    .line 2514
    if-eqz v6, :cond_66f

    .line 2516
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v2

    if-eq v2, v6, :cond_5a8

    const/4 v2, 0x1

    move/from16 v22, v2

    .line 2517
    :goto_34d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2, v6}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v27

    .line 2518
    if-eqz v27, :cond_5ad

    .line 2519
    const/16 v2, -0x65

    .line 2518
    :goto_359
    int-to-long v9, v2

    .line 2521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-gez v2, :cond_5b1

    .line 2523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v2, v2, Lcom/android/launcher2/as;->f:I

    move/from16 v23, v2

    .line 2532
    :goto_36b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v2, :cond_5db

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v4, v2, Lcom/android/launcher2/as;->d:I

    .line 2533
    :goto_377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v2, :cond_5de

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v5, v2, Lcom/android/launcher2/as;->e:I

    .line 2537
    :goto_383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v2, v2

    .line 2538
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v7, 0x1

    aget v3, v3, v7

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2537
    invoke-static/range {v2 .. v7}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    .line 2540
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v7, 0x1

    aget v3, v3, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2539
    invoke-virtual {v6, v2, v3, v7}, Lcom/android/launcher2/CellLayout;->a(FF[I)F

    move-result v13

    .line 2544
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/Workspace;->bj:Z

    if-nez v2, :cond_3cf

    .line 2545
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/16 v16, 0x0

    move-object/from16 v7, p0

    move-object v11, v6

    .line 2544
    invoke-virtual/range {v7 .. v16}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JLcom/android/launcher2/CellLayout;[IFZLcom/android/launcher2/bu;Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2549
    :cond_3cf
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v16, v0

    .line 2550
    const/16 v19, 0x0

    move-object/from16 v14, p0

    move-object v15, v6

    move/from16 v17, v13

    move-object/from16 v18, p1

    .line 2549
    invoke-virtual/range {v14 .. v19}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;[IFLcom/android/launcher2/bz;Z)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2556
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/di;

    .line 2557
    iget v14, v2, Lcom/android/launcher2/di;->n:I

    .line 2558
    iget v15, v2, Lcom/android/launcher2/di;->o:I

    .line 2559
    iget v3, v2, Lcom/android/launcher2/di;->p:I

    if-lez v3, :cond_3fa

    iget v3, v2, Lcom/android/launcher2/di;->q:I

    if-lez v3, :cond_3fa

    .line 2560
    iget v14, v2, Lcom/android/launcher2/di;->p:I

    .line 2561
    iget v15, v2, Lcom/android/launcher2/di;->q:I

    .line 2564
    :cond_3fa
    const/4 v3, 0x2

    new-array v0, v3, [I

    move-object/from16 v20, v0

    .line 2565
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aY:Z

    if-eqz v3, :cond_5e1

    .line 2566
    if-nez v21, :cond_5e1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v3

    if-eqz v3, :cond_5e1

    .line 2567
    iget-wide v11, v2, Lcom/android/launcher2/di;->j:J

    const-wide/16 v16, -0x64

    cmp-long v3, v11, v16

    if-nez v3, :cond_5e1

    .line 2568
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v3

    iget v7, v2, Lcom/android/launcher2/di;->k:I

    if-ne v3, v7, :cond_5e1

    .line 2569
    iget v3, v2, Lcom/android/launcher2/di;->l:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x0

    aget v7, v7, v11

    if-ne v3, v7, :cond_5e1

    iget v3, v2, Lcom/android/launcher2/di;->m:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x1

    aget v7, v7, v11

    if-ne v3, v7, :cond_5e1

    .line 2570
    const/4 v3, 0x0

    aput v4, v20, v3

    .line 2571
    const/4 v3, 0x1

    aput v5, v20, v3

    .line 2577
    :goto_443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-ltz v3, :cond_60a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    if-ltz v3, :cond_60a

    const/4 v3, 0x1

    move v4, v3

    .line 2578
    :goto_457
    instance-of v3, v8, Landroid/appwidget/AppWidgetHostView;

    if-eqz v3, :cond_66b

    .line 2579
    if-eqz v4, :cond_66b

    const/4 v3, 0x0

    aget v3, v20, v3

    iget v5, v2, Lcom/android/launcher2/di;->n:I

    if-ne v3, v5, :cond_46b

    const/4 v3, 0x1

    aget v3, v20, v3

    iget v5, v2, Lcom/android/launcher2/di;->o:I

    if-eq v3, v5, :cond_66b

    .line 2580
    :cond_46b
    const/4 v5, 0x1

    .line 2581
    const/4 v3, 0x0

    aget v3, v20, v3

    iput v3, v2, Lcom/android/launcher2/di;->n:I

    .line 2582
    const/4 v3, 0x1

    aget v3, v20, v3

    iput v3, v2, Lcom/android/launcher2/di;->o:I

    move-object v3, v8

    .line 2583
    check-cast v3, Landroid/appwidget/AppWidgetHostView;

    .line 2584
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v7

    if-eqz v7, :cond_48c

    .line 2585
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    .line 2586
    const/4 v11, 0x0

    aget v11, v20, v11

    const/4 v12, 0x1

    aget v12, v20, v12

    .line 2585
    invoke-static {v3, v7, v11, v12}, Lcom/anddoes/launcher/r;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V

    .line 2590
    :cond_48c
    :goto_48c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/Workspace;->u:I

    move/from16 v0, v23

    if-eq v3, v0, :cond_667

    if-nez v27, :cond_667

    .line 2592
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->r(I)V

    move/from16 v18, v23

    .line 2595
    :goto_49f
    if-eqz v4, :cond_60e

    .line 2596
    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/di;

    .line 2597
    if-eqz v22, :cond_4cd

    .line 2599
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    .line 2600
    if-eqz v4, :cond_4b4

    .line 2601
    invoke-virtual {v4, v8}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 2603
    :cond_4b4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v7, 0x0

    aget v12, v4, v7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v7, 0x1

    aget v13, v4, v7

    .line 2604
    iget v14, v3, Lcom/android/launcher2/di;->n:I

    iget v15, v3, Lcom/android/launcher2/di;->o:I

    move-object/from16 v7, p0

    move/from16 v11, v23

    .line 2603
    invoke-virtual/range {v7 .. v15}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIII)V

    .line 2608
    :cond_4cd
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2609
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x0

    aget v7, v7, v11

    iput v7, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    iput v7, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    .line 2610
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x1

    aget v7, v7, v11

    iput v7, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    iput v7, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 2611
    iget v7, v2, Lcom/android/launcher2/di;->n:I

    iput v7, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 2612
    iget v2, v2, Lcom/android/launcher2/di;->o:I

    iput v2, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 2613
    const/4 v2, 0x1

    iput-boolean v2, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 2614
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v2, v2, Lcom/android/launcher2/as;->f:I

    .line 2615
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x0

    aget v7, v7, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v12, 0x1

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v12, v12, Lcom/android/launcher2/as;->d:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v12, v12, Lcom/android/launcher2/as;->e:I

    .line 2614
    invoke-static {v9, v10, v2, v7, v11}, Lcom/android/launcher2/gb;->a(JIII)I

    move-result v2

    invoke-virtual {v8, v2}, Landroid/view/View;->setId(I)V

    .line 2617
    const-wide/16 v11, -0x65

    cmp-long v2, v9, v11

    if-eqz v2, :cond_663

    .line 2618
    instance-of v2, v8, Lcom/android/launcher2/fy;

    if-eqz v2, :cond_663

    .line 2619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->ao:Z

    if-nez v2, :cond_663

    move-object v2, v8

    .line 2623
    check-cast v2, Lcom/android/launcher2/fy;

    .line 2624
    invoke-virtual {v2}, Lcom/android/launcher2/fy;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v7

    .line 2625
    if-eqz v7, :cond_663

    .line 2626
    iget v7, v7, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    if-eqz v7, :cond_663

    .line 2627
    new-instance v7, Lcom/android/launcher2/jy;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v3, v2, v6}, Lcom/android/launcher2/jy;-><init>(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/di;Lcom/android/launcher2/fy;Lcom/android/launcher2/CellLayout;)V

    .line 2633
    new-instance v2, Lcom/android/launcher2/jz;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v7}, Lcom/android/launcher2/jz;-><init>(Lcom/android/launcher2/Workspace;Ljava/lang/Runnable;)V

    .line 2646
    :goto_54a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget v0, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    move/from16 v16, v0

    .line 2647
    iget v0, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    move/from16 v17, v0

    move-object v12, v3

    move-wide v13, v9

    move/from16 v15, v23

    .line 2646
    invoke-static/range {v11 .. v17}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    move-object/from16 v25, v2

    move/from16 v2, v18

    .line 2660
    :goto_561
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 2663
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout;

    .line 2667
    new-instance v6, Lcom/android/launcher2/ka;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v6, v0, v1}, Lcom/android/launcher2/ka;-><init>(Lcom/android/launcher2/Workspace;Ljava/lang/Runnable;)V

    .line 2677
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Workspace;->c:Z

    .line 2678
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->b()Z

    move-result v3

    if-eqz v3, :cond_658

    .line 2679
    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/di;

    .line 2680
    iget v7, v3, Lcom/android/launcher2/di;->i:I

    const/4 v9, 0x4

    if-ne v7, v9, :cond_63e

    .line 2681
    if-eqz v5, :cond_63b

    const/4 v7, 0x2

    .line 2683
    :goto_599
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    .line 2684
    const/4 v9, 0x0

    move-object/from16 v2, p0

    .line 2683
    invoke-virtual/range {v2 .. v9}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/bu;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    .line 2694
    :goto_5a3
    invoke-static {v8}, Lcom/android/launcher2/CellLayout;->b(Landroid/view/View;)V

    goto/16 :goto_a

    .line 2516
    :cond_5a8
    const/4 v2, 0x0

    move/from16 v22, v2

    goto/16 :goto_34d

    .line 2520
    :cond_5ad
    const/16 v2, -0x64

    goto/16 :goto_359

    .line 2525
    :cond_5b1
    if-eqz v27, :cond_5d1

    .line 2526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v2, v6, v3, v4}, Lcom/android/launcher2/Hotseat;->a(Lcom/android/launcher2/CellLayout;II)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_36b

    .line 2528
    :cond_5d1
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_36b

    .line 2532
    :cond_5db
    const/4 v4, 0x1

    goto/16 :goto_377

    .line 2533
    :cond_5de
    const/4 v5, 0x1

    goto/16 :goto_383

    .line 2573
    :cond_5e1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v7, 0x0

    aget v3, v3, v7

    float-to-int v12, v3

    .line 2574
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v7, 0x1

    aget v3, v3, v7

    float-to-int v13, v3

    .line 2575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v19, v0

    const/16 v21, 0x1

    move-object v11, v6

    move/from16 v16, v4

    move/from16 v17, v5

    move-object/from16 v18, v8

    .line 2573
    invoke-virtual/range {v11 .. v21}, Lcom/android/launcher2/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    goto/16 :goto_443

    .line 2577
    :cond_60a
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_457

    .line 2650
    :cond_60e
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 2651
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v4, 0x0

    iget v6, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    aput v6, v3, v4

    .line 2652
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v4, 0x1

    iget v2, v2, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    aput v2, v3, v4

    .line 2653
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 2654
    instance-of v3, v2, Lcom/android/launcher2/CellLayout;

    if-eqz v3, :cond_637

    .line 2655
    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 2656
    invoke-virtual {v2, v8}, Lcom/android/launcher2/CellLayout;->c(Landroid/view/View;)V

    :cond_637
    move/from16 v2, v18

    goto/16 :goto_561

    .line 2682
    :cond_63b
    const/4 v7, 0x0

    goto/16 :goto_599

    .line 2686
    :cond_63e
    if-gez v2, :cond_655

    const/4 v5, -0x1

    .line 2687
    :goto_641
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    move-object v4, v8

    move-object/from16 v7, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V

    goto/16 :goto_5a3

    .line 2686
    :cond_655
    const/16 v5, 0x12c

    goto :goto_641

    .line 2691
    :cond_658
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/android/launcher2/bz;->k:Z

    .line 2692
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5a3

    :cond_663
    move-object/from16 v2, v25

    goto/16 :goto_54a

    :cond_667
    move/from16 v18, v26

    goto/16 :goto_49f

    :cond_66b
    move/from16 v5, v24

    goto/16 :goto_48c

    :cond_66f
    move/from16 v5, v24

    move/from16 v2, v26

    goto/16 :goto_561

    :cond_675
    move-object/from16 v3, v17

    goto/16 :goto_22a

    :cond_679
    move v3, v10

    goto/16 :goto_13c

    .line 2509
    :pswitch_data_67c
    .packed-switch 0x0
        :pswitch_211
        :pswitch_211
        :pswitch_31e
    .end packed-switch
.end method

.method public final a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3767
    return-void
.end method

.method public final a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/bu;Ljava/lang/Runnable;ILandroid/view/View;Z)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3572
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 3573
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v5}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 3575
    const/4 v2, 0x2

    new-array v7, v2, [I

    .line 3576
    const/4 v2, 0x2

    new-array v10, v2, [F

    .line 3577
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/android/launcher2/iv;

    if-eqz v2, :cond_177

    const/4 v2, 0x0

    .line 3578
    :goto_1d
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/di;->n:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/di;->o:I

    const/4 v8, 0x0

    aget v8, v3, v8

    const/4 v9, 0x1

    aget v3, v3, v9

    move-object/from16 v0, p2

    invoke-static {v0, v8, v3, v4, v6}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;IIII)Landroid/graphics/Rect;

    move-result-object v4

    const/4 v3, 0x0

    iget v6, v4, Landroid/graphics/Rect;->left:I

    aput v6, v7, v3

    const/4 v3, 0x1

    iget v6, v4, Landroid/graphics/Rect;->top:I

    aput v6, v7, v3

    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setFinalTransitionTransform(Lcom/android/launcher2/CellLayout;)V

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v7}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;[I)F

    move-result v6

    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;)V

    if-eqz v2, :cond_17a

    const/high16 v2, 0x3f80

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual/range {p3 .. p3}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v2, v3

    const/high16 v2, 0x3f80

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v2, v8

    invoke-virtual/range {p3 .. p3}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v2, v8

    :goto_72
    const/4 v8, 0x0

    aget v9, v7, v8

    int-to-float v9, v9

    invoke-virtual/range {p3 .. p3}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v6

    sub-float/2addr v11, v12

    const/high16 v12, 0x4000

    div-float/2addr v11, v12

    sub-float/2addr v9, v11

    float-to-int v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    aget v9, v7, v8

    int-to-float v9, v9

    invoke-virtual/range {p3 .. p3}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float v4, v11, v4

    const/high16 v11, 0x4000

    div-float/2addr v4, v11

    sub-float v4, v9, v4

    float-to-int v4, v4

    aput v4, v7, v8

    const/4 v4, 0x0

    mul-float/2addr v3, v6

    aput v3, v10, v4

    const/4 v3, 0x1

    mul-float/2addr v2, v6

    aput v2, v10, v3

    .line 3581
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 3582
    const v3, 0x7f0a0016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    add-int/lit16 v13, v2, -0xc8

    .line 3585
    move-object/from16 v0, p6

    instance-of v2, v0, Landroid/appwidget/AppWidgetHostView;

    if-eqz v2, :cond_d3

    if-eqz p7, :cond_d3

    .line 3586
    const-string v2, "Launcher.Workspace"

    const-string v3, "6557954 Animate widget drop, final view is appWidgetHostView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3587
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    .line 3589
    :cond_d3
    const/4 v2, 0x2

    move/from16 v0, p5

    if-eq v0, v2, :cond_da

    if-eqz p7, :cond_180

    :cond_da
    if-eqz p6, :cond_180

    .line 3590
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/launcher2/di;->n:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/di;->o:I

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v6}, Lcom/android/launcher2/Workspace;->a(IIZ)[I

    move-result-object v2

    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const/4 v4, 0x0

    aget v4, v2, v4

    if-gtz v4, :cond_102

    const/4 v4, 0x0

    const/4 v6, 0x1

    aput v6, v2, v4

    :cond_102
    const/4 v4, 0x1

    aget v4, v2, v4

    if-gtz v4, :cond_10b

    const/4 v4, 0x1

    const/4 v6, 0x1

    aput v6, v2, v4

    :cond_10b
    const/4 v4, 0x0

    aget v4, v2, v4

    const/high16 v6, 0x4000

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v6, 0x1

    aget v6, v2, v6

    const/high16 v8, 0x4000

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/4 v8, 0x0

    aget v8, v2, v8

    const/4 v9, 0x1

    aget v9, v2, v9

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v6}, Landroid/view/View;->measure(II)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v11, 0x0

    aget v11, v2, v11

    const/4 v12, 0x1

    aget v2, v2, v12

    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v6, v11, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v9}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3591
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Lcom/android/launcher2/bu;->setCrossFadeBitmap(Landroid/graphics/Bitmap;)V

    .line 3592
    int-to-float v2, v13

    const v3, 0x3f4ccccd

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/android/launcher2/bu;->a(I)V

    .line 3597
    :cond_15e
    :goto_15e
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    .line 3598
    const/4 v3, 0x4

    move/from16 v0, p5

    if-ne v0, v3, :cond_19a

    .line 3599
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v7, v1, v13}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;[ILjava/lang/Runnable;I)V

    .line 3624
    :goto_176
    return-void

    .line 3577
    :cond_177
    const/4 v2, 0x1

    goto/16 :goto_1d

    .line 3578
    :cond_17a
    const/high16 v3, 0x3f80

    const/high16 v2, 0x3f80

    goto/16 :goto_72

    .line 3593
    :cond_180
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/launcher2/di;->i:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_15e

    if-eqz p7, :cond_15e

    .line 3594
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    aget v4, v10, v4

    const/4 v6, 0x1

    aget v6, v10, v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    aput v4, v10, v3

    aput v4, v10, v2

    goto :goto_15e

    .line 3603
    :cond_19a
    const/4 v3, 0x1

    move/from16 v0, p5

    if-ne v0, v3, :cond_1c2

    .line 3604
    const/4 v12, 0x2

    .line 3609
    :goto_1a0
    new-instance v11, Lcom/android/launcher2/js;

    move-object/from16 v0, p6

    move-object/from16 v1, p4

    invoke-direct {v11, p0, v0, v1}, Lcom/android/launcher2/js;-><init>(Lcom/android/launcher2/Workspace;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 3620
    iget v4, v5, Landroid/graphics/Rect;->left:I

    iget v5, v5, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    aget v6, v7, v3

    .line 3621
    const/4 v3, 0x1

    aget v7, v7, v3

    const/high16 v8, 0x3f80

    const/4 v3, 0x0

    aget v9, v10, v3

    const/4 v3, 0x1

    aget v10, v10, v3

    move-object/from16 v3, p3

    move-object v14, p0

    .line 3620
    invoke-virtual/range {v2 .. v14}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;IIIIFFFLjava/lang/Runnable;IILandroid/view/View;)V

    goto :goto_176

    .line 3606
    :cond_1c2
    const/4 v12, 0x0

    goto :goto_1a0
.end method

.method public final a(Lcom/android/launcher2/iu;Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1810
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 1812
    iget v1, p1, Lcom/android/launcher2/iu;->n:I

    iget v2, p1, Lcom/android/launcher2/iu;->o:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->a(IIZ)[I

    move-result-object v1

    .line 1815
    aget v2, v1, v3

    .line 1816
    const/4 v3, 0x1

    aget v1, v1, v3

    .line 1815
    invoke-direct {p0, p2, v0, v2, v1}, Lcom/android/launcher2/Workspace;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 1817
    return-void
.end method

.method final a(Lcom/android/launcher2/jd;Lcom/android/launcher2/CellLayout;JIZII)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2255
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2, p2, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/ViewGroup;Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v3

    .line 2257
    const/4 v2, 0x2

    new-array v12, v2, [I

    .line 2258
    move/from16 v0, p7

    move/from16 v1, p8

    invoke-virtual {p2, v12, v0, v1}, Lcom/android/launcher2/CellLayout;->b([III)Z

    .line 2259
    const/4 v2, 0x0

    aget v7, v12, v2

    const/4 v2, 0x1

    aget v8, v12, v2

    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object v2, p0

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move/from16 v11, p6

    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 2260
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    const/4 v3, 0x0

    aget v7, v12, v3

    .line 2261
    const/4 v3, 0x1

    aget v8, v12, v3

    move-object v3, p1

    move-wide/from16 v4, p3

    move/from16 v6, p5

    .line 2260
    invoke-static/range {v2 .. v8}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    .line 2262
    return-void
.end method

.method final a(Ljava/util/ArrayList;)V
    .registers 7
    .parameter

    .prologue
    .line 4023
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 4024
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 4025
    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-lt v1, v3, :cond_24

    .line 4029
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;

    move-result-object v0

    .line 4030
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_37

    .line 4113
    new-instance v0, Lcom/android/launcher2/ju;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/ju;-><init>(Lcom/android/launcher2/Workspace;Ljava/util/HashSet;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 4147
    return-void

    .line 4026
    :cond_24
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    iget-object v0, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4025
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 4030
    :cond_37
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4031
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v3

    .line 4034
    new-instance v4, Lcom/android/launcher2/jt;

    invoke-direct {v4, p0, v3, v2, v0}, Lcom/android/launcher2/jt;-><init>(Lcom/android/launcher2/Workspace;Landroid/view/ViewGroup;Ljava/util/HashSet;Lcom/android/launcher2/CellLayout;)V

    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->post(Ljava/lang/Runnable;)Z

    goto :goto_15
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 4351
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->x:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 4353
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cD:[F

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    iput v1, p0, Lcom/android/launcher2/Workspace;->cE:F

    .line 4354
    iput-boolean v3, p0, Lcom/android/launcher2/Workspace;->cz:Z

    .line 4355
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 4357
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/launcher2/Workspace;->cA:J

    .line 4359
    if-eqz p1, :cond_41

    .line 4360
    iput v3, p0, Lcom/android/launcher2/Workspace;->cB:I

    move v1, v0

    .line 4361
    :goto_21
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_2b

    .line 4373
    :goto_27
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 4374
    return-void

    .line 4362
    :cond_2b
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->c(Landroid/view/View;)V

    .line 4363
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    const/high16 v2, 0x3f80

    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setShortcutAndWidgetAlpha(F)V

    .line 4361
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_21

    .line 4366
    :cond_41
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/launcher2/Workspace;->cB:I

    goto :goto_27
.end method

.method public final a([I)V
    .registers 3
    .parameter

    .prologue
    .line 4209
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 4210
    return-void
.end method

.method protected final a(FF)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 661
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v2, -0x3e7

    if-ne v0, v2, :cond_17

    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    .line 665
    :goto_9
    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v2, :cond_1a

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/android/launcher2/Workspace;->a(IFFZ)Z

    move-result v0

    if-eqz v0, :cond_1a

    move v0, v1

    :goto_16
    return v0

    .line 661
    :cond_17
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    goto :goto_9

    .line 665
    :cond_1a
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public final a(III)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3842
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->F()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 3891
    :cond_b
    :goto_b
    return v2

    .line 3846
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_8a

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_8a

    .line 3847
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 3848
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/launcher2/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    .line 3849
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_8a

    move v0, v1

    .line 3855
    :goto_2f
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v3

    if-nez v3, :cond_b

    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v3, :cond_b

    .line 3856
    iput-boolean v2, p0, Lcom/android/launcher2/Workspace;->cx:Z

    .line 3857
    if-eqz v0, :cond_65

    .line 3858
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/launcher2/Hotseat;->k(I)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3859
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3860
    if-eqz v0, :cond_b

    .line 3862
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eqz v3, :cond_5a

    .line 3863
    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3, v2}, Lcom/android/launcher2/CellLayout;->setIsDragOverlapping(Z)V

    .line 3864
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->g()V

    .line 3866
    :cond_5a
    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    .line 3867
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setIsDragOverlapping(Z)V

    .line 3868
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->cx:Z

    move v2, v1

    .line 3869
    goto :goto_b

    .line 3872
    :cond_65
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bj:Z

    .line 3874
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getNextPage()I

    move-result v3

    .line 3875
    if-nez p3, :cond_88

    const/4 v0, -0x1

    .line 3874
    :goto_6e
    add-int/2addr v0, v3

    .line 3878
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3880
    if-ltz v0, :cond_b

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_b

    .line 3881
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3882
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3886
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    move v2, v1

    .line 3887
    goto :goto_b

    :cond_88
    move v0, v1

    .line 3875
    goto :goto_6e

    :cond_8a
    move v0, v2

    goto :goto_2f
.end method

.method final a(Landroid/view/View;JLcom/android/launcher2/CellLayout;[IFZLcom/android/launcher2/bu;Ljava/lang/Runnable;)Z
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2403
    iget v1, p0, Lcom/android/launcher2/Workspace;->bF:F

    cmpl-float v1, p6, v1

    if-lez v1, :cond_8

    const/4 v1, 0x0

    .line 2455
    :goto_7
    return v1

    .line 2404
    :cond_8
    const/4 v1, 0x0

    aget v1, p5, v1

    const/4 v2, 0x1

    aget v2, p5, v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v10

    .line 2406
    const/4 v1, 0x0

    .line 2407
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v2, :cond_38

    .line 2408
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v1, v1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v1

    .line 2409
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v2, v2, Lcom/android/launcher2/as;->b:I

    const/4 v3, 0x0

    aget v3, p5, v3

    if-ne v2, v3, :cond_42

    .line 2410
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v2, v2, Lcom/android/launcher2/as;->c:I

    const/4 v3, 0x1

    aget v3, p5, v3

    if-ne v2, v3, :cond_42

    move-object/from16 v0, p4

    if-ne v1, v0, :cond_42

    const/4 v1, 0x1

    .line 2413
    :cond_38
    :goto_38
    if-eqz v10, :cond_40

    if-nez v1, :cond_40

    iget-boolean v1, p0, Lcom/android/launcher2/Workspace;->bC:Z

    if-nez v1, :cond_44

    :cond_40
    const/4 v1, 0x0

    goto :goto_7

    .line 2410
    :cond_42
    const/4 v1, 0x0

    goto :goto_38

    .line 2414
    :cond_44
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bC:Z

    .line 2415
    if-nez p5, :cond_c1

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget v5, v1, Lcom/android/launcher2/as;->f:I

    .line 2419
    :goto_4d
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/android/launcher2/jd;

    .line 2420
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/launcher2/jd;

    .line 2422
    if-eqz v1, :cond_f0

    if-eqz v2, :cond_f0

    .line 2423
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/android/launcher2/jd;

    .line 2424
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/android/launcher2/jd;

    .line 2426
    if-nez p7, :cond_7e

    .line 2427
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v1, v1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v1

    .line 2428
    if-eqz v1, :cond_7e

    .line 2429
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v2, v2, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 2433
    :cond_7e
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 2434
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v12

    .line 2435
    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 2438
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    const/4 v2, 0x0

    aget v6, p5, v2

    const/4 v2, 0x1

    aget v7, p5, v2

    move-object/from16 v2, p4

    move-wide v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/CellLayout;JIII)Lcom/android/launcher2/FolderIcon;

    move-result-object v1

    .line 2439
    const/4 v2, -0x1

    iput v2, v9, Lcom/android/launcher2/jd;->l:I

    .line 2440
    const/4 v2, -0x1

    iput v2, v9, Lcom/android/launcher2/jd;->m:I

    .line 2441
    const/4 v2, -0x1

    iput v2, v8, Lcom/android/launcher2/jd;->l:I

    .line 2442
    const/4 v2, -0x1

    iput v2, v8, Lcom/android/launcher2/jd;->m:I

    .line 2445
    if-eqz p8, :cond_e7

    const/4 v2, 0x1

    .line 2446
    :goto_b0
    if-eqz v2, :cond_e9

    move-object v2, v9

    move-object v3, v10

    move-object v4, v8

    move-object/from16 v5, p8

    move-object v6, v11

    move v7, v12

    move-object/from16 v8, p9

    .line 2447
    invoke-virtual/range {v1 .. v8}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/jd;Landroid/view/View;Lcom/android/launcher2/jd;Lcom/android/launcher2/bu;Landroid/graphics/Rect;FLjava/lang/Runnable;)V

    .line 2453
    :goto_be
    const/4 v1, 0x1

    goto/16 :goto_7

    .line 2415
    :cond_c1
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    .line 2416
    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_df

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v1

    .line 2417
    const/4 v2, 0x0

    aget v2, p5, v2

    const/4 v3, 0x1

    aget v3, p5, v3

    .line 2416
    move-object/from16 v0, p4

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/launcher2/Hotseat;->a(Lcom/android/launcher2/CellLayout;II)I

    move-result v5

    goto/16 :goto_4d

    .line 2417
    :cond_df
    move-object/from16 v0, p4

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v5

    goto/16 :goto_4d

    .line 2445
    :cond_e7
    const/4 v2, 0x0

    goto :goto_b0

    .line 2450
    :cond_e9
    invoke-virtual {v1, v9}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/di;)V

    .line 2451
    invoke-virtual {v1, v8}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/di;)V

    goto :goto_be

    .line 2455
    :cond_f0
    const/4 v1, 0x0

    goto/16 :goto_7
.end method

.method final a(Lcom/android/launcher2/CellLayout;[IFLcom/android/launcher2/bz;Z)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2460
    iget v0, p0, Lcom/android/launcher2/Workspace;->bF:F

    cmpl-float v0, p3, v0

    if-lez v0, :cond_a

    move v0, v1

    .line 2481
    :goto_9
    return v0

    .line 2462
    :cond_a
    aget v0, p2, v1

    aget v3, p2, v2

    invoke-virtual {p1, v0, v3}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v0

    .line 2463
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->bD:Z

    if-nez v3, :cond_18

    move v0, v1

    goto :goto_9

    .line 2464
    :cond_18
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bD:Z

    .line 2466
    instance-of v3, v0, Lcom/android/launcher2/FolderIcon;

    if-eqz v3, :cond_40

    .line 2467
    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 2468
    iget-object v3, p4, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/FolderIcon;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 2469
    invoke-virtual {v0, p4}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/bz;)V

    .line 2472
    if-nez p5, :cond_3e

    .line 2473
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->b(Landroid/view/View;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2474
    if-eqz v0, :cond_3e

    .line 2475
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v1, v1, Lcom/android/launcher2/as;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    :cond_3e
    move v0, v2

    .line 2478
    goto :goto_9

    :cond_40
    move v0, v1

    .line 2481
    goto :goto_9
.end method

.method public final a(IIZ)[I
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const v5, 0x7fffffff

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 374
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 375
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_4b

    .line 376
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 377
    invoke-static {v0, v3, v3, p1, p2}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;IIII)Landroid/graphics/Rect;

    move-result-object v0

    .line 378
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    aput v2, v1, v3

    .line 379
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    aput v0, v1, v4

    .line 380
    if-eqz p3, :cond_3e

    .line 381
    aget v0, v1, v3

    int-to-float v0, v0

    iget v2, p0, Lcom/android/launcher2/Workspace;->bg:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v1, v3

    .line 382
    aget v0, v1, v4

    int-to-float v0, v0

    iget v2, p0, Lcom/android/launcher2/Workspace;->bg:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v1, v4

    .line 384
    :cond_3e
    aget v0, v1, v3

    aget v2, v1, v4

    mul-int/2addr v0, v2

    if-nez v0, :cond_49

    .line 386
    aput v5, v1, v3

    .line 387
    aput v5, v1, v4

    :cond_49
    move-object v0, v1

    .line 393
    :goto_4a
    return-object v0

    .line 391
    :cond_4b
    aput v5, v1, v3

    .line 392
    aput v5, v1, v4

    move-object v0, v1

    .line 393
    goto :goto_4a
.end method

.method public final a_()V
    .registers 2

    .prologue
    .line 3812
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cx:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 3813
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->a_()V

    .line 3823
    :cond_15
    :goto_15
    return-void

    .line 3816
    :cond_16
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_23

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_23

    .line 3817
    invoke-super {p0}, Lcom/android/launcher2/je;->a_()V

    .line 3819
    :cond_23
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 3820
    if-eqz v0, :cond_15

    .line 3821
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->h()V

    goto :goto_15
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-nez v0, :cond_11

    .line 1665
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 1666
    if-eqz v0, :cond_12

    .line 1667
    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/Folder;->addFocusables(Ljava/util/ArrayList;I)V

    .line 1672
    :cond_11
    :goto_11
    return-void

    .line 1669
    :cond_12
    invoke-super {p0, p1, p2, p3}, Lcom/android/launcher2/je;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_11
.end method

.method public final b(Ljava/lang/Object;)Landroid/view/View;
    .registers 8
    .parameter

    .prologue
    .line 3993
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;

    move-result-object v0

    .line 3992
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 3994
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 4003
    const/4 v0, 0x0

    :goto_f
    return-object v0

    .line 3992
    :cond_10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    .line 3995
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    .line 3996
    const/4 v1, 0x0

    move v2, v1

    :goto_1c
    if-ge v2, v4, :cond_8

    .line 3997
    invoke-virtual {v0, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3998
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-ne v5, p1, :cond_2a

    move-object v0, v1

    .line 3999
    goto :goto_f

    .line 3996
    :cond_2a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1c
.end method

.method protected final b(F)V
    .registers 2
    .parameter

    .prologue
    .line 1589
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->c(F)V

    .line 1590
    return-void
.end method

.method final b(Lcom/android/launcher2/CellLayout;)V
    .registers 15
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3734
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v12

    .line 3737
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v4

    .line 3738
    const/16 v0, -0x64

    .line 3740
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 3741
    const/4 v1, 0x1

    .line 3742
    const/16 v0, -0x65

    move v9, v0

    move v10, v1

    :goto_1c
    move v11, v2

    .line 3745
    :goto_1d
    if-lt v11, v12, :cond_20

    .line 3757
    return-void

    .line 3746
    :cond_20
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3747
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    .line 3749
    if-eqz v1, :cond_4e

    .line 3750
    if-eqz v10, :cond_40

    .line 3751
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    iget v2, v1, Lcom/android/launcher2/di;->l:I

    iget v3, v1, Lcom/android/launcher2/di;->m:I

    invoke-virtual {v0, p1, v2, v3}, Lcom/android/launcher2/Hotseat;->a(Lcom/android/launcher2/CellLayout;II)I

    move-result v4

    .line 3753
    :cond_40
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    int-to-long v2, v9

    iget v5, v1, Lcom/android/launcher2/di;->l:I

    .line 3754
    iget v6, v1, Lcom/android/launcher2/di;->m:I

    iget v7, v1, Lcom/android/launcher2/di;->n:I

    iget v8, v1, Lcom/android/launcher2/di;->o:I

    .line 3753
    invoke-static/range {v0 .. v8}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIII)V

    .line 3745
    :cond_4e
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_1d

    :cond_52
    move v9, v0

    move v10, v2

    goto :goto_1c
.end method

.method public final b(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2022
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    .line 2023
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iput-boolean v0, v1, Lcom/android/launcher2/kh;->g:Z

    .line 2024
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 2029
    iget-boolean v1, p0, Lcom/android/launcher2/Workspace;->bq:Z

    if-nez v1, :cond_15

    move v1, v0

    .line 2030
    :goto_f
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_16

    .line 2035
    :cond_15
    return-void

    .line 2031
    :cond_16
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2032
    const/high16 v2, 0x3f80

    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setShortcutAndWidgetAlpha(F)V

    .line 2030
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f
.end method

.method public final b(Lcom/android/launcher2/bz;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2734
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->F()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2751
    :cond_9
    :goto_9
    return-void

    .line 2737
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bE:Lcom/android/launcher2/by;

    invoke-virtual {v0}, Lcom/android/launcher2/by;->b()V

    .line 2738
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bC:Z

    .line 2739
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bD:Z

    .line 2741
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    .line 2742
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getCurrentDropLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2743
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 2744
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    .line 2748
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2749
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->N()V

    goto :goto_9
.end method

.method final b(Ljava/util/ArrayList;)V
    .registers 16
    .parameter

    .prologue
    .line 4150
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;

    move-result-object v0

    .line 4151
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    .line 4180
    return-void

    .line 4151
    :cond_f
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/android/launcher2/ja;

    .line 4152
    invoke-virtual {v4}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v8

    .line 4153
    const/4 v0, 0x0

    move v6, v0

    :goto_1c
    if-ge v6, v8, :cond_8

    .line 4154
    invoke-virtual {v4, v6}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4155
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 4156
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_4c

    .line 4157
    check-cast v0, Lcom/android/launcher2/jd;

    .line 4161
    iget-object v1, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 4162
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    .line 4163
    iget v2, v0, Lcom/android/launcher2/jd;->i:I

    if-nez v2, :cond_4c

    .line 4164
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4c

    if-eqz v9, :cond_4c

    .line 4165
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 4166
    const/4 v1, 0x0

    move v5, v1

    :goto_4a
    if-lt v5, v10, :cond_50

    .line 4153
    :cond_4c
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1c

    .line 4167
    :cond_50
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/h;

    .line 4168
    iget-object v2, v1, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v2, v9}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7b

    move-object v2, v3

    .line 4169
    check-cast v2, Lcom/android/launcher2/BubbleTextView;

    .line 4170
    iget-object v11, p0, Lcom/android/launcher2/Workspace;->aW:Lcom/android/launcher2/da;

    invoke-virtual {v0, v11}, Lcom/android/launcher2/jd;->b(Lcom/android/launcher2/da;)V

    .line 4171
    iget-object v1, v1, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 4172
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aW:Lcom/android/launcher2/da;

    .line 4173
    iget-object v11, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-wide v12, v0, Lcom/android/launcher2/jd;->j:J

    invoke-static {v11, v12, v13}, Lcom/anddoes/launcher/v;->a(Lcom/android/launcher2/Launcher;J)Z

    move-result v11

    .line 4172
    invoke-virtual {v2, v0, v1, v11}, Lcom/android/launcher2/BubbleTextView;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/da;Z)V

    .line 4166
    :cond_7b
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4a
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2013
    return-void
.end method

.method protected final b(FF)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 672
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v2, -0x3e7

    if-ne v0, v2, :cond_17

    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    .line 676
    :goto_9
    iget-boolean v2, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v2, :cond_1a

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/android/launcher2/Workspace;->a(IFFZ)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_16
    return v0

    .line 672
    :cond_17
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    goto :goto_9

    :cond_1a
    move v0, v1

    .line 676
    goto :goto_16
.end method

.method protected final c(I)V
    .registers 12
    .parameter

    .prologue
    const v9, 0x3dcccccd

    const/4 v4, 0x0

    const/high16 v5, 0x3f80

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1494
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->c(I)V

    .line 1496
    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    if-ltz v0, :cond_50

    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    iget v3, p0, Lcom/android/launcher2/Workspace;->w:I

    if-gt v0, v3, :cond_50

    move v0, v1

    :goto_16
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->bq:Z

    if-eqz v3, :cond_2d

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v6, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    if-ne v3, v6, :cond_2d

    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v3, :cond_2d

    if-nez v0, :cond_2d

    move v6, v1

    :goto_27
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v6, v0, :cond_52

    .line 1497
    :cond_2d
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_1a4

    .line 1503
    const-string v0, "CARD_STACK"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 1504
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->l(I)V

    move v2, v1

    .line 1534
    :cond_41
    :goto_41
    if-eqz v2, :cond_4f

    .line 1535
    :goto_43
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_16a

    .line 1551
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->f(I)V

    .line 1552
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 1557
    :cond_4f
    :goto_4f
    return-void

    :cond_50
    move v0, v2

    .line 1496
    goto :goto_16

    :cond_52
    invoke-virtual {p0, v6}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_7b

    invoke-virtual {p0, p1, v0, v6}, Lcom/android/launcher2/Workspace;->a(ILandroid/view/View;I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    sub-float v7, v5, v7

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/android/launcher2/ja;->setAlpha(F)V

    iget-boolean v7, p0, Lcom/android/launcher2/Workspace;->d:Z

    if-nez v7, :cond_8e

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v7, v3, v9

    if-gez v7, :cond_7f

    move v3, v4

    :goto_78
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setBackgroundAlphaMultiplier(F)V

    :cond_7b
    :goto_7b
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_27

    :cond_7f
    const v7, 0x3ecccccd

    cmpl-float v7, v3, v7

    if-lez v7, :cond_88

    move v3, v5

    goto :goto_78

    :cond_88
    sub-float/2addr v3, v9

    const v7, 0x3e99999a

    div-float/2addr v3, v7

    goto :goto_78

    :cond_8e
    invoke-virtual {v0, v5}, Lcom/android/launcher2/CellLayout;->setBackgroundAlphaMultiplier(F)V

    goto :goto_7b

    .line 1505
    :cond_92
    const-string v0, "TABLET"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 1506
    new-instance v0, Lcom/android/launcher2/ig;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ig;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ig;->a(I)V

    move v2, v1

    goto :goto_41

    .line 1507
    :cond_a6
    const-string v0, "CUBE_IN"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ba

    .line 1508
    new-instance v0, Lcom/android/launcher2/hw;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hw;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/hw;->a(I)V

    move v2, v1

    goto :goto_41

    .line 1509
    :cond_ba
    const-string v0, "CUBE"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 1510
    new-instance v0, Lcom/android/launcher2/hx;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hx;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/hx;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1512
    :cond_cf
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1513
    const-string v0, "ACCORDION"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ec

    .line 1514
    new-instance v0, Lcom/android/launcher2/hu;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hu;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/hu;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1515
    :cond_ec
    const-string v0, "CROSS"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_101

    .line 1516
    new-instance v0, Lcom/android/launcher2/hv;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hv;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/hv;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1517
    :cond_101
    const-string v0, "FLIP"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_116

    .line 1518
    new-instance v0, Lcom/android/launcher2/hy;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hy;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/hy;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1519
    :cond_116
    const-string v0, "OVERLAP"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12b

    .line 1520
    new-instance v0, Lcom/android/launcher2/ia;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ia;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ia;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1521
    :cond_12b
    const-string v0, "ROTATE"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_140

    .line 1522
    new-instance v0, Lcom/android/launcher2/ic;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ic;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ic;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1523
    :cond_140
    const-string v0, "SCALE"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_155

    .line 1524
    new-instance v0, Lcom/android/launcher2/ie;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ie;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ie;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1525
    :cond_155
    const-string v0, "WHEEL"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1526
    new-instance v0, Lcom/android/launcher2/ii;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ii;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ii;->a(I)V

    move v2, v1

    goto/16 :goto_41

    .line 1536
    :cond_16a
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->a(I)Landroid/view/View;

    move-result-object v2

    .line 1538
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->ac:Z

    if-eqz v0, :cond_193

    .line 1539
    invoke-virtual {p0, p1, v2, v1}, Lcom/android/launcher2/Workspace;->a(ILandroid/view/View;I)F

    move-result v0

    .line 1540
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->aq:Z

    if-eqz v3, :cond_18a

    .line 1541
    iget v3, p0, Lcom/android/launcher2/Workspace;->V:I

    if-gez v3, :cond_197

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_197

    .line 1542
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->m(I)F

    move-result v0

    .line 1547
    :cond_18a
    :goto_18a
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v5, v0

    .line 1548
    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1535
    :cond_193
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_43

    .line 1543
    :cond_197
    iget v3, p0, Lcom/android/launcher2/Workspace;->V:I

    iget v4, p0, Lcom/android/launcher2/Workspace;->w:I

    if-le v3, v4, :cond_18a

    if-nez v1, :cond_18a

    .line 1544
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->n(I)F

    move-result v0

    goto :goto_18a

    .line 1555
    :cond_1a4
    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    if-ltz v0, :cond_1ae

    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    iget v3, p0, Lcom/android/launcher2/Workspace;->w:I

    if-le v0, v3, :cond_20b

    :cond_1ae
    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    if-gez v0, :cond_200

    move v3, v1

    :goto_1b3
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {p0, p1, v0, v3}, Lcom/android/launcher2/Workspace;->a(ILandroid/view/View;I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    if-nez v3, :cond_1c4

    move v1, v2

    :cond_1c4
    invoke-virtual {v0, v5, v1}, Lcom/android/launcher2/CellLayout;->a(FZ)V

    const/high16 v1, -0x3e40

    mul-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->setFadeForOverScroll(F)V

    iget-boolean v1, p0, Lcom/android/launcher2/Workspace;->bp:Z

    if-nez v1, :cond_4f

    iput-boolean v2, p0, Lcom/android/launcher2/Workspace;->bp:Z

    iget v1, p0, Lcom/android/launcher2/Workspace;->q:F

    iget v4, p0, Lcom/android/launcher2/Workspace;->bx:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setCameraDistance(F)V

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getMeasuredWidth()I

    move-result v1

    int-to-float v4, v1

    if-nez v3, :cond_208

    const/high16 v1, 0x3f40

    :goto_1ec
    mul-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setPivotX(F)V

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f00

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setPivotY(F)V

    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setOverscrollTransformsDirty(Z)V

    goto/16 :goto_4f

    :cond_200
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    goto :goto_1b3

    :cond_208
    const/high16 v1, 0x3e80

    goto :goto_1ec

    :cond_20b
    iget v0, p0, Lcom/android/launcher2/Workspace;->bo:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_214

    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->setFadeForOverScroll(F)V

    :cond_214
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bp:Z

    if-eqz v0, :cond_4f

    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bp:Z

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->b()V

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->b()V

    goto/16 :goto_4f
.end method

.method protected final c(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1183
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/je;->c(II)V

    .line 1184
    invoke-direct {p0, p1}, Lcom/android/launcher2/Workspace;->j(I)V

    .line 1185
    return-void
.end method

.method public final c(Lcom/android/launcher2/bz;)V
    .registers 16
    .parameter

    .prologue
    .line 3068
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->F()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3199
    :cond_8
    :goto_8
    return-void

    .line 3072
    :cond_9
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bj:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v1, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    if-eq v0, v1, :cond_8

    .line 3074
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 3075
    const/4 v7, 0x0

    .line 3076
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/di;

    .line 3079
    iget v0, v8, Lcom/android/launcher2/di;->n:I

    if-ltz v0, :cond_2a

    iget v0, v8, Lcom/android/launcher2/di;->o:I

    if-gez v0, :cond_32

    :cond_2a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Improper spans found"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3080
    :cond_32
    iget v1, p1, Lcom/android/launcher2/bz;->a:I

    iget v2, p1, Lcom/android/launcher2/bz;->b:I

    iget v3, p1, Lcom/android/launcher2/bz;->c:I

    iget v4, p1, Lcom/android/launcher2/bz;->d:I

    .line 3081
    iget-object v5, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    iget-object v6, p0, Lcom/android/launcher2/Workspace;->ba:[F

    move-object v0, p0

    .line 3080
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/Workspace;->a(IIIILcom/android/launcher2/bu;[F)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->ba:[F

    .line 3083
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-nez v0, :cond_1dd

    const/4 v0, 0x0

    move-object v12, v0

    .line 3085
    :goto_4b
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-eqz v0, :cond_203

    .line 3086
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_31d

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_31d

    .line 3087
    iget-object v0, p1, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    if-eq v0, p0, :cond_1e4

    invoke-static {p1}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/bz;)Z

    move-result v0

    if-eqz v0, :cond_1e4

    const/4 v0, 0x1

    :goto_6a
    if-eqz v0, :cond_82

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aZ:Z

    if-eqz v0, :cond_31d

    .line 3088
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_31d

    invoke-static {p1}, Lcom/android/launcher2/Workspace;->g(Lcom/android/launcher2/bz;)Z

    move-result v0

    if-eqz v0, :cond_31d

    .line 3089
    :cond_82
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/launcher2/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    .line 3090
    iget v0, p1, Lcom/android/launcher2/bz;->a:I

    iget v1, p1, Lcom/android/launcher2/bz;->b:I

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_31d

    .line 3091
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v7

    move-object v0, v7

    .line 3094
    :goto_a0
    if-nez v0, :cond_ae

    .line 3095
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    iget v0, p1, Lcom/android/launcher2/bz;->a:I

    int-to-float v0, v0

    iget v1, p1, Lcom/android/launcher2/bz;->b:I

    int-to-float v1, v1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Workspace;->c(FF)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3097
    :cond_ae
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eq v0, v1, :cond_d0

    .line 3099
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3100
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3102
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v2, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    if-ne v1, v2, :cond_1e7

    const/4 v1, 0x1

    .line 3103
    :goto_bf
    if-eqz v1, :cond_d0

    .line 3104
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1ea

    .line 3105
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bf:Lcom/android/launcher2/jg;

    iget-object v0, v0, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 3130
    :cond_d0
    :goto_d0
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_8

    .line 3131
    const/4 v0, 0x0

    .line 3133
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_25c

    .line 3134
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    invoke-static {v0, v1}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[F)V

    .line 3135
    const/4 v0, 0x1

    move v9, v0

    .line 3140
    :goto_ec
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lcom/android/launcher2/di;

    .line 3142
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-int v0, v0

    .line 3143
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-int v1, v1

    iget v2, v8, Lcom/android/launcher2/di;->n:I

    iget v3, v8, Lcom/android/launcher2/di;->o:I

    .line 3144
    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v5, p0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 3142
    invoke-static/range {v0 .. v5}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 3145
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Workspace;->f(II)V

    .line 3147
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    .line 3148
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 3147
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/CellLayout;->a(FF[I)F

    move-result v4

    .line 3150
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 3151
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 3150
    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v7

    .line 3153
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;[IFZ)Z

    move-result v0

    iget v1, p0, Lcom/android/launcher2/Workspace;->bI:I

    if-nez v1, :cond_267

    if-eqz v0, :cond_267

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->by:Lcom/android/launcher2/c;

    invoke-virtual {v1}, Lcom/android/launcher2/c;->b()Z

    move-result v1

    if-nez v1, :cond_267

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->by:Lcom/android/launcher2/c;

    new-instance v1, Lcom/android/launcher2/kd;

    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-direct {v1, p0, v2, v4, v3}, Lcom/android/launcher2/kd;-><init>(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/CellLayout;II)V

    invoke-virtual {v0, v1}, Lcom/android/launcher2/c;->a(Lcom/android/launcher2/hq;)V

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->by:Lcom/android/launcher2/c;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/c;->a(J)V

    .line 3156
    :cond_16a
    :goto_16a
    iget v1, v8, Lcom/android/launcher2/di;->n:I

    .line 3157
    iget v0, v8, Lcom/android/launcher2/di;->o:I

    .line 3158
    iget v2, v8, Lcom/android/launcher2/di;->p:I

    if-lez v2, :cond_316

    iget v2, v8, Lcom/android/launcher2/di;->q:I

    if-lez v2, :cond_316

    .line 3159
    iget v1, v8, Lcom/android/launcher2/di;->p:I

    .line 3160
    iget v0, v8, Lcom/android/launcher2/di;->q:I

    move v10, v0

    move v11, v1

    .line 3163
    :goto_17c
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    .line 3164
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v2, v2

    iget v3, v8, Lcom/android/launcher2/di;->n:I

    .line 3165
    iget v4, v8, Lcom/android/launcher2/di;->o:I

    iget-object v6, p0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object v5, v12

    .line 3163
    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/view/View;[I)Z

    move-result v13

    .line 3167
    if-nez v13, :cond_29e

    .line 3168
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bl:Landroid/graphics/Bitmap;

    .line 3169
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v3, v1

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v4, 0x1

    aget v1, v1, v4

    float-to-int v4, v1

    .line 3170
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v5, 0x0

    aget v5, v1, v5

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v6, 0x1

    aget v6, v1, v6

    iget v7, v8, Lcom/android/launcher2/di;->n:I

    iget v8, v8, Lcom/android/launcher2/di;->o:I

    const/4 v9, 0x0

    .line 3171
    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->getDragVisualizeOffset()Landroid/graphics/Point;

    move-result-object v10

    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v11

    move-object v1, v12

    .line 3168
    invoke-virtual/range {v0 .. v11}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;Landroid/graphics/Bitmap;IIIIIIZLandroid/graphics/Point;Landroid/graphics/Rect;)V

    .line 3192
    :cond_1c6
    :goto_1c6
    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1d2

    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1d2

    .line 3193
    if-nez v13, :cond_8

    .line 3194
    :cond_1d2
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_8

    .line 3195
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->e()V

    goto/16 :goto_8

    .line 3083
    :cond_1dd
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    iget-object v0, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    move-object v12, v0

    goto/16 :goto_4b

    .line 3087
    :cond_1e4
    const/4 v0, 0x0

    goto/16 :goto_6a

    .line 3102
    :cond_1e7
    const/4 v1, 0x0

    goto/16 :goto_bf

    .line 3107
    :cond_1ea
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->bf:Lcom/android/launcher2/jg;

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v0, v2, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    iget-object v4, v2, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    if-nez v3, :cond_200

    const-wide/16 v0, 0x3b6

    :goto_1f9
    invoke-virtual {v4, v0, v1}, Lcom/android/launcher2/c;->a(J)V

    iput-object v3, v2, Lcom/android/launcher2/jg;->e:Lcom/android/launcher2/CellLayout;

    goto/16 :goto_d0

    :cond_200
    const-wide/16 v0, 0x1f4

    goto :goto_1f9

    .line 3113
    :cond_203
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_31a

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_31a

    invoke-static {p1}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/bz;)Z

    move-result v0

    if-eqz v0, :cond_22d

    .line 3114
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aZ:Z

    if-eqz v0, :cond_31a

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v0

    if-eqz v0, :cond_31a

    invoke-static {p1}, Lcom/android/launcher2/Workspace;->g(Lcom/android/launcher2/bz;)Z

    move-result v0

    if-eqz v0, :cond_31a

    .line 3115
    :cond_22d
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/launcher2/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    .line 3116
    iget v0, p1, Lcom/android/launcher2/bz;->a:I

    iget v1, p1, Lcom/android/launcher2/bz;->b:I

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_31a

    .line 3117
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3120
    :goto_24a
    if-nez v0, :cond_250

    .line 3121
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getCurrentDropLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3123
    :cond_250
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eq v0, v1, :cond_d0

    .line 3124
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3125
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    goto/16 :goto_d0

    .line 3137
    :cond_25c
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    move v9, v0

    goto/16 :goto_ec

    .line 3153
    :cond_267
    invoke-direct {p0, v6, v2, v3, v4}, Lcom/android/launcher2/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher2/CellLayout;[IF)Z

    move-result v1

    if-eqz v1, :cond_286

    iget v3, p0, Lcom/android/launcher2/Workspace;->bI:I

    if-nez v3, :cond_286

    move-object v0, v7

    check-cast v0, Lcom/android/launcher2/FolderIcon;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bB:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0, v6}, Lcom/android/launcher2/FolderIcon;->b(Ljava/lang/Object;)V

    if-eqz v2, :cond_280

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->d()V

    :cond_280
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setDragMode(I)V

    goto/16 :goto_16a

    :cond_286
    iget v2, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_291

    if-nez v1, :cond_291

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->setDragMode(I)V

    :cond_291
    iget v1, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_16a

    if-nez v0, :cond_16a

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setDragMode(I)V

    goto/16 :goto_16a

    .line 3172
    :cond_29e
    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    if-eqz v0, :cond_2a7

    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1c6

    .line 3173
    :cond_2a7
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bz:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->b()Z

    move-result v0

    if-nez v0, :cond_1c6

    iget v0, p0, Lcom/android/launcher2/Workspace;->bJ:I

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-ne v0, v1, :cond_2c1

    .line 3174
    iget v0, p0, Lcom/android/launcher2/Workspace;->bK:I

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    if-eq v0, v1, :cond_1c6

    .line 3176
    :cond_2c1
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aY:Z

    if-eqz v0, :cond_2f7

    .line 3177
    if-nez v9, :cond_2f7

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_2f7

    .line 3178
    iget-wide v0, v8, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x64

    cmp-long v0, v0, v2

    if-nez v0, :cond_2f7

    .line 3179
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, v8, Lcom/android/launcher2/di;->k:I

    if-ne v0, v1, :cond_2f7

    .line 3180
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, v8, Lcom/android/launcher2/di;->l:I

    if-ne v0, v1, :cond_2f7

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iget v1, v8, Lcom/android/launcher2/di;->m:I

    if-eq v0, v1, :cond_1c6

    .line 3185
    :cond_2f7
    new-instance v0, Lcom/android/launcher2/kf;

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->ba:[F

    .line 3186
    iget v5, v8, Lcom/android/launcher2/di;->n:I

    iget v6, v8, Lcom/android/launcher2/di;->o:I

    iget-object v7, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    move-object v1, p0

    move v3, v11

    move v4, v10

    move-object v8, v12

    .line 3185
    invoke-direct/range {v0 .. v8}, Lcom/android/launcher2/kf;-><init>(Lcom/android/launcher2/Workspace;[FIIIILcom/android/launcher2/bu;Landroid/view/View;)V

    .line 3187
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->bz:Lcom/android/launcher2/c;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/c;->a(Lcom/android/launcher2/hq;)V

    .line 3188
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bz:Lcom/android/launcher2/c;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/c;->a(J)V

    goto/16 :goto_1c6

    :cond_316
    move v10, v0

    move v11, v1

    goto/16 :goto_17c

    :cond_31a
    move-object v0, v7

    goto/16 :goto_24a

    :cond_31d
    move-object v0, v7

    goto/16 :goto_a0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 3775
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->F()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public computeScroll()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/high16 v1, 0x3f80

    .line 1324
    invoke-super {p0}, Lcom/android/launcher2/je;->computeScroll()V

    .line 1325
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cl:Z

    if-eqz v0, :cond_63

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v3, p0, Lcom/android/launcher2/Workspace;->mRight:I

    iget v4, p0, Lcom/android/launcher2/Workspace;->mLeft:I

    sub-int/2addr v3, v4

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_5a

    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->U()Z

    move-result v0

    if-eqz v0, :cond_5f

    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    div-float v5, v1, v0

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v0, :cond_5b

    move v0, v1

    :goto_3c
    invoke-virtual {v4, v5, v0}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    iget v5, p0, Lcom/android/launcher2/Workspace;->mScrollX:I

    int-to-float v5, v5

    int-to-float v3, v3

    div-float v3, v5, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget-boolean v5, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v5, :cond_5d

    :goto_57
    invoke-virtual {v0, v4, v3, v1}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    .line 1326
    :cond_5a
    :goto_5a
    return-void

    :cond_5b
    move v0, v2

    .line 1325
    goto :goto_3c

    :cond_5d
    move v1, v2

    goto :goto_57

    :cond_5f
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->L()V

    goto :goto_5a

    :cond_63
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->U()Z

    move-result v0

    if-eqz v0, :cond_102

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    div-float v5, v1, v0

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cy:Z

    if-eqz v0, :cond_f0

    move v0, v1

    :goto_7b
    invoke-virtual {v4, v5, v0}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget v0, p0, Lcom/android/launcher2/Workspace;->W:F

    iput v1, p0, Lcom/android/launcher2/Workspace;->W:F

    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->getScrollRange()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v5

    iget v6, p0, Lcom/android/launcher2/Workspace;->w:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/android/launcher2/Workspace;->aL:F

    mul-float/2addr v5, v6

    iput v0, p0, Lcom/android/launcher2/Workspace;->W:F

    int-to-float v0, v4

    div-float v0, v5, v0

    iget-boolean v4, p0, Lcom/android/launcher2/Workspace;->aq:Z

    if-eqz v4, :cond_c6

    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->h:Ljava/lang/String;

    const-string v5, "BOUNCE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c6

    invoke-virtual {p0, v7}, Lcom/android/launcher2/Workspace;->a(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->d(Landroid/view/View;)I

    move-result v4

    iget v5, p0, Lcom/android/launcher2/Workspace;->H:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/android/launcher2/Workspace;->mScrollX:I

    if-gez v5, :cond_f2

    const/high16 v0, -0x4080

    iget v5, p0, Lcom/android/launcher2/Workspace;->mScrollX:I

    int-to-float v5, v5

    mul-float/2addr v0, v5

    int-to-float v4, v4

    div-float/2addr v0, v4

    :cond_c6
    :goto_c6
    iget-boolean v4, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v4, :cond_e4

    iget-boolean v4, p0, Lcom/android/launcher2/Workspace;->bu:Z

    if-eqz v4, :cond_e4

    iget v4, p0, Lcom/android/launcher2/Workspace;->bv:I

    iget v5, p0, Lcom/android/launcher2/Workspace;->f:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v5, v4

    mul-float/2addr v0, v5

    iget v5, p0, Lcom/android/launcher2/Workspace;->f:I

    sub-int v4, v5, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget v4, p0, Lcom/android/launcher2/Workspace;->f:I

    int-to-float v4, v4

    div-float/2addr v0, v4

    :cond_e4
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, v3, Lcom/android/launcher2/kh;->a:F

    goto/16 :goto_5a

    :cond_f0
    move v0, v2

    goto :goto_7b

    :cond_f2
    iget v5, p0, Lcom/android/launcher2/Workspace;->mScrollX:I

    iget v6, p0, Lcom/android/launcher2/Workspace;->w:I

    if-le v5, v6, :cond_c6

    iget v0, p0, Lcom/android/launcher2/Workspace;->w:I

    add-int/2addr v0, v4

    iget v5, p0, Lcom/android/launcher2/Workspace;->mScrollX:I

    sub-int/2addr v0, v5

    int-to-float v0, v0

    int-to-float v4, v4

    div-float/2addr v0, v4

    goto :goto_c6

    :cond_102
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->L()V

    goto/16 :goto_5a
.end method

.method public final d(Lcom/android/launcher2/bz;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2754
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bE:Lcom/android/launcher2/by;

    invoke-virtual {v0}, Lcom/android/launcher2/by;->c()V

    .line 2758
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bj:Z

    if-eqz v0, :cond_30

    .line 2759
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    .line 2764
    :goto_f
    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    if-ne v0, v2, :cond_35

    .line 2765
    iput-boolean v2, p0, Lcom/android/launcher2/Workspace;->bC:Z

    .line 2771
    :cond_15
    :goto_15
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bj:Z

    .line 2772
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 2773
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    .line 2775
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bf:Lcom/android/launcher2/jg;

    iget-object v0, v0, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 2777
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->af:Z

    if-nez v0, :cond_2f

    .line 2778
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->O()V

    .line 2780
    :cond_2f
    return-void

    .line 2761
    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    goto :goto_f

    .line 2766
    :cond_35
    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_15

    .line 2767
    iput-boolean v2, p0, Lcom/android/launcher2/Workspace;->bD:Z

    goto :goto_15
.end method

.method public final d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 4301
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    if-nez v0, :cond_7

    .line 4302
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->d(Z)V

    .line 4303
    :cond_7
    return-void
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 3761
    const/4 v0, 0x1

    return v0
.end method

.method public final d_()V
    .registers 2

    .prologue
    .line 3827
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cx:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 3828
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->d_()V

    .line 3838
    :cond_15
    :goto_15
    return-void

    .line 3831
    :cond_16
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_23

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_23

    .line 3832
    invoke-super {p0}, Lcom/android/launcher2/je;->d_()V

    .line 3834
    :cond_23
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 3835
    if-eqz v0, :cond_15

    .line 3836
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->h()V

    goto :goto_15
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 4476
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->cz:Z

    if-eqz v3, :cond_4c

    .line 4478
    iget-wide v3, p0, Lcom/android/launcher2/Workspace;->cA:J

    cmp-long v3, v3, v1

    if-nez v3, :cond_28

    .line 4479
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/launcher2/Workspace;->cA:J

    .line 4484
    :goto_13
    const-wide/16 v3, 0x190

    cmp-long v1, v1, v3

    if-ltz v1, :cond_3a

    .line 4486
    iget v1, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_30

    .line 4487
    const/4 v1, 0x3

    iput v1, p0, Lcom/android/launcher2/Workspace;->cB:I

    .line 4496
    :cond_21
    :goto_21
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 4497
    :goto_25
    if-lt v0, v1, :cond_3e

    .line 4531
    :goto_27
    return-void

    .line 4482
    :cond_28
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/launcher2/Workspace;->cA:J

    sub-long/2addr v1, v3

    goto :goto_13

    .line 4488
    :cond_30
    iget v1, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_21

    .line 4489
    const/4 v1, 0x4

    iput v1, p0, Lcom/android/launcher2/Workspace;->cB:I

    .line 4490
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    .line 4491
    :cond_3a
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->postInvalidate()V

    goto :goto_21

    .line 4498
    :cond_3e
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getDrawingTime()J

    move-result-wide v3

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/android/launcher2/Workspace;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 4497
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 4501
    :cond_4c
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_27
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter

    .prologue
    .line 3789
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->bL:Landroid/util/SparseArray;

    .line 3790
    return-void
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 705
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->k()Z

    move-result v0

    if-nez v0, :cond_e

    .line 707
    :cond_c
    const/4 v0, 0x0

    .line 709
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/je;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_d
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4536
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    if-eqz v0, :cond_100

    .line 4537
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/launcher2/Workspace;->cA:J

    sub-long v5, v0, v2

    .line 4538
    new-instance v7, Landroid/graphics/Rect;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v7, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4539
    invoke-direct {p0, p2}, Lcom/android/launcher2/Workspace;->g(Landroid/view/View;)Landroid/graphics/RectF;

    move-result-object v8

    .line 4540
    const/4 v4, 0x0

    .line 4541
    const/4 v3, 0x0

    .line 4542
    const/4 v2, 0x0

    .line 4543
    const/4 v1, 0x0

    .line 4544
    const/high16 v0, 0x437f

    .line 4546
    iget v9, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_b0

    .line 4547
    long-to-float v0, v5

    const/4 v1, 0x0

    const/high16 v2, 0x42c8

    .line 4548
    invoke-static {v0, v1, v2}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v0

    .line 4549
    long-to-float v1, v5

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    .line 4550
    invoke-static {v1, v2, v3}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v4

    .line 4551
    long-to-float v1, v5

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    iget v3, v8, Landroid/graphics/RectF;->top:F

    .line 4552
    invoke-static {v1, v2, v3}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v3

    .line 4553
    long-to-float v1, v5

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    iget v9, v8, Landroid/graphics/RectF;->right:F

    .line 4554
    invoke-static {v1, v2, v9}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v2

    .line 4555
    long-to-float v1, v5

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    int-to-float v5, v5

    iget v6, v8, Landroid/graphics/RectF;->bottom:F

    .line 4556
    invoke-static {v1, v5, v6}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v1

    .line 4575
    :cond_62
    :goto_62
    sub-float/2addr v2, v4

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v2, v5

    .line 4576
    sub-float/2addr v1, v3

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v1, v5

    .line 4577
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4587
    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4588
    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 4589
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 4590
    new-instance v0, Landroid/graphics/RectF;

    iget v1, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x5

    int-to-float v1, v1

    iget v2, v7, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, -0x5

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x5

    int-to-float v3, v3

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, 0x5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x41a0

    const/high16 v2, 0x41a0

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 4591
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->cF:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 4592
    invoke-virtual {p2, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 4594
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 4600
    :goto_ae
    const/4 v0, 0x1

    return v0

    .line 4557
    :cond_b0
    iget v9, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_ef

    .line 4558
    long-to-float v0, v5

    const/high16 v1, 0x42c8

    const/4 v2, 0x0

    .line 4559
    invoke-static {v0, v1, v2}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v0

    .line 4560
    long-to-float v1, v5

    iget v2, v8, Landroid/graphics/RectF;->left:F

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    .line 4561
    invoke-static {v1, v2, v3}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v4

    .line 4562
    long-to-float v1, v5

    iget v2, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    .line 4563
    invoke-static {v1, v2, v3}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v3

    .line 4564
    long-to-float v1, v5

    iget v2, v8, Landroid/graphics/RectF;->right:F

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v9

    int-to-float v9, v9

    .line 4565
    invoke-static {v1, v2, v9}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v2

    .line 4566
    long-to-float v1, v5

    iget v5, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v6

    int-to-float v6, v6

    .line 4567
    invoke-static {v1, v5, v6}, Lcom/anddoes/launcher/v;->a(FFF)F

    move-result v1

    goto/16 :goto_62

    .line 4568
    :cond_ef
    iget v5, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_62

    .line 4569
    iget v4, v8, Landroid/graphics/RectF;->left:F

    .line 4570
    iget v3, v8, Landroid/graphics/RectF;->top:F

    .line 4571
    iget v2, v8, Landroid/graphics/RectF;->right:F

    .line 4572
    iget v1, v8, Landroid/graphics/RectF;->bottom:F

    .line 4573
    const/high16 v0, 0x42c8

    goto/16 :goto_62

    .line 4597
    :cond_100
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/launcher2/je;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_ae
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 3772
    return-void
.end method

.method protected final e(Landroid/view/View;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 490
    move-object v0, p1

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 491
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->e(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 492
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_1e

    .line 493
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getBackgroundAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    .line 491
    goto :goto_1f
.end method

.method public final e(Lcom/android/launcher2/bz;)Z
    .registers 21
    .parameter

    .prologue
    .line 2273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->aU:Lcom/android/launcher2/CellLayout;

    move-object/from16 v18, v0

    .line 2274
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    move-object/from16 v0, p0

    if-eq v2, v0, :cond_18d

    .line 2276
    if-nez v18, :cond_12

    .line 2277
    const/4 v2, 0x0

    .line 2347
    :goto_11
    return v2

    .line 2279
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/Workspace;->bi:Z

    if-eqz v2, :cond_22

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/Workspace;->ch:F

    const/high16 v3, 0x3f00

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2f

    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->bh:Lcom/android/launcher2/kg;

    sget-object v3, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    if-eq v2, v3, :cond_2f

    const/4 v2, 0x1

    :goto_2b
    if-nez v2, :cond_31

    const/4 v2, 0x0

    goto :goto_11

    :cond_2f
    const/4 v2, 0x0

    goto :goto_2b

    .line 2281
    :cond_31
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/launcher2/bz;->a:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/bz;->b:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/bz;->c:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/bz;->d:I

    .line 2282
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Workspace;->ba:[F

    move-object/from16 v2, p0

    .line 2281
    invoke-direct/range {v2 .. v8}, Lcom/android/launcher2/Workspace;->a(IIIILcom/android/launcher2/bu;[F)[F

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    .line 2285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_e4

    .line 2286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    invoke-static {v2, v3}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[F)V

    .line 2291
    :goto_6e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    if-eqz v2, :cond_f2

    .line 2294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    .line 2295
    iget v2, v3, Lcom/android/launcher2/as;->d:I

    .line 2296
    iget v12, v3, Lcom/android/launcher2/as;->e:I

    move/from16 v17, v2

    .line 2305
    :goto_7e
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v2, v2, Lcom/android/launcher2/iw;

    if-eqz v2, :cond_190

    .line 2306
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/iw;

    iget v4, v2, Lcom/android/launcher2/iw;->p:I

    .line 2307
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/iw;

    iget v5, v2, Lcom/android/launcher2/iw;->q:I

    .line 2310
    :goto_96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v2, v2

    .line 2311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    float-to-int v3, v3

    .line 2312
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Workspace;->aP:[I

    move-object/from16 v6, v18

    .line 2310
    invoke-static/range {v2 .. v7}, Lcom/android/launcher2/Workspace;->b(IIIILcom/android/launcher2/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    .line 2314
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2313
    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v6}, Lcom/android/launcher2/CellLayout;->a(FF[I)F

    move-result v10

    .line 2315
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v7, Lcom/android/launcher2/di;

    .line 2316
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v11, 0x1

    move-object/from16 v6, p0

    move-object/from16 v8, v18

    .line 2315
    invoke-direct/range {v6 .. v11}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;[IFZ)Z

    move-result v2

    if-eqz v2, :cond_ff

    .line 2317
    const/4 v2, 0x1

    goto/16 :goto_11

    .line 2288
    :cond_e4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    goto/16 :goto_6e

    .line 2298
    :cond_f2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/di;

    .line 2299
    iget v3, v2, Lcom/android/launcher2/di;->n:I

    .line 2300
    iget v12, v2, Lcom/android/launcher2/di;->o:I

    move/from16 v17, v3

    goto :goto_7e

    .line 2319
    :cond_ff
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v2, Lcom/android/launcher2/di;

    .line 2320
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2319
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v2, v1, v3, v10}, Lcom/android/launcher2/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher2/CellLayout;[IF)Z

    move-result v2

    if-eqz v2, :cond_116

    .line 2321
    const/4 v2, 0x1

    goto/16 :goto_11

    .line 2324
    :cond_116
    const/4 v2, 0x2

    new-array v15, v2, [I

    .line 2325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v7, v2

    .line 2326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->ba:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v8, v2

    .line 2327
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/16 v16, 0x3

    move-object/from16 v6, v18

    move v9, v4

    move v10, v5

    move/from16 v11, v17

    .line 2325
    invoke-virtual/range {v6 .. v16}, Lcom/android/launcher2/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    .line 2328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-ltz v2, :cond_18b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    if-ltz v2, :cond_18b

    const/4 v2, 0x1

    .line 2331
    :goto_151
    if-nez v2, :cond_18d

    .line 2334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v2

    .line 2335
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aP:[I

    if-eqz v3, :cond_181

    if-eqz v2, :cond_181

    .line 2336
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v3

    .line 2337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Workspace;->aP:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher2/Hotseat;->a(II)I

    invoke-static {}, Lcom/android/launcher2/Hotseat;->c()Z

    .line 2339
    :cond_181
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3, v2}, Lcom/android/launcher2/Launcher;->a(Z)V

    .line 2344
    const/4 v2, 0x0

    goto/16 :goto_11

    .line 2328
    :cond_18b
    const/4 v2, 0x0

    goto :goto_151

    .line 2347
    :cond_18d
    const/4 v2, 0x1

    goto/16 :goto_11

    :cond_190
    move v5, v12

    move/from16 v4, v17

    goto/16 :goto_96
.end method

.method protected final e_()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 930
    invoke-super {p0}, Lcom/android/launcher2/je;->e_()V

    .line 932
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_36

    .line 933
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 947
    :goto_e
    const-string v2, "TABLET"

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 948
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->ck:Z

    .line 949
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->N()V

    .line 950
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v2

    if-nez v2, :cond_50

    :goto_25
    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->bu:Z

    .line 955
    :cond_27
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bq:Z

    if-nez v0, :cond_32

    move v2, v1

    .line 956
    :goto_2c
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v2, v0, :cond_52

    .line 962
    :cond_32
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->d(Z)V

    .line 963
    return-void

    .line 935
    :cond_36
    iget v2, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v3, -0x3e7

    if-eq v2, v3, :cond_44

    .line 937
    iget v2, p0, Lcom/android/launcher2/Workspace;->u:I

    iget v3, p0, Lcom/android/launcher2/Workspace;->v:I

    invoke-direct {p0, v2, v3}, Lcom/android/launcher2/Workspace;->e(II)V

    goto :goto_e

    .line 941
    :cond_44
    iget v2, p0, Lcom/android/launcher2/Workspace;->u:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/launcher2/Workspace;->u:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/android/launcher2/Workspace;->e(II)V

    goto :goto_e

    :cond_50
    move v0, v1

    .line 950
    goto :goto_25

    .line 957
    :cond_52
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    const/high16 v3, 0x3f80

    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setShortcutAndWidgetAlpha(F)V

    .line 956
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2c
.end method

.method protected final f(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 4438
    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    if-ltz v0, :cond_d

    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    iget v1, p0, Lcom/android/launcher2/Workspace;->w:I

    if-le v0, v1, :cond_6f

    .line 4439
    :cond_d
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->aq:Z

    if-nez v0, :cond_63

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->as:Z

    if-eqz v0, :cond_63

    .line 4440
    iget v0, p0, Lcom/android/launcher2/Workspace;->V:I

    if-gez v0, :cond_64

    move v1, v2

    .line 4443
    :goto_1a
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4444
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher2/Workspace;->a(ILandroid/view/View;I)F

    move-result v4

    .line 4445
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    if-nez v1, :cond_2b

    move v2, v3

    :cond_2b
    invoke-virtual {v0, v5, v2}, Lcom/android/launcher2/CellLayout;->a(FZ)V

    .line 4446
    const/high16 v2, -0x3e40

    mul-float/2addr v2, v4

    .line 4447
    iget v5, p0, Lcom/android/launcher2/Workspace;->q:F

    sget v6, Lcom/android/launcher2/Workspace;->ay:F

    mul-float/2addr v5, v6

    invoke-virtual {v0, v5}, Lcom/android/launcher2/CellLayout;->setCameraDistance(F)V

    .line 4448
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    if-nez v1, :cond_6c

    const/high16 v1, 0x3f40

    :goto_42
    mul-float/2addr v1, v5

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setPivotX(F)V

    .line 4449
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v5, 0x3f00

    mul-float/2addr v1, v5

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setPivotY(F)V

    .line 4450
    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 4451
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setOverscrollTransformsDirty(Z)V

    .line 4452
    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setAlpha(F)V

    .line 4453
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setFadeForOverScroll(F)V

    .line 4466
    :cond_63
    :goto_63
    return-void

    .line 4440
    :cond_64
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_1a

    .line 4448
    :cond_6c
    const/high16 v1, 0x3e80

    goto :goto_42

    .line 4456
    :cond_6f
    iget v0, p0, Lcom/android/launcher2/Workspace;->bo:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_78

    .line 4457
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Workspace;->setFadeForOverScroll(F)V

    .line 4460
    :cond_78
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-nez v0, :cond_63

    .line 4461
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->b()V

    .line 4462
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4463
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->b()V

    goto :goto_63
.end method

.method public final f_()V
    .registers 1

    .prologue
    .line 4195
    return-void
.end method

.method protected final g()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 966
    invoke-super {p0}, Lcom/android/launcher2/je;->g()V

    .line 968
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 969
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 974
    :cond_e
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->b()Z

    move-result v0

    if-eqz v0, :cond_69

    .line 975
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 978
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->f()V

    .line 992
    :cond_21
    :goto_21
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/Workspace;->aK:F

    .line 994
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->br:Ljava/lang/Runnable;

    if-eqz v0, :cond_2f

    .line 995
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->br:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 996
    iput-object v5, p0, Lcom/android/launcher2/Workspace;->br:Ljava/lang/Runnable;

    .line 999
    :cond_2f
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    if-eqz v0, :cond_3a

    .line 1000
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1001
    iput-object v5, p0, Lcom/android/launcher2/Workspace;->bs:Ljava/lang/Runnable;

    .line 1004
    :cond_3a
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    if-eqz v0, :cond_4b

    .line 1005
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 1006
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iput-object v5, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    .line 1008
    :cond_4b
    return-void

    .line 971
    :cond_4c
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_51
    if-ge v1, v3, :cond_e

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->isHardwareAccelerated()Z

    move-result v4

    if-nez v4, :cond_65

    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    :cond_65
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_51

    .line 983
    :cond_69
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->ck:Z

    if-eqz v0, :cond_70

    .line 984
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->O()V

    .line 988
    :cond_70
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->b()Z

    move-result v0

    if-nez v0, :cond_21

    .line 989
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->e(Z)V

    goto :goto_21
.end method

.method public final g(I)V
    .registers 4
    .parameter

    .prologue
    .line 2712
    if-ltz p1, :cond_17

    .line 2713
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2714
    iget v1, p0, Lcom/android/launcher2/Workspace;->bN:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->setScrollX(I)V

    .line 2715
    iget v1, p0, Lcom/android/launcher2/Workspace;->bP:F

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 2716
    iget v1, p0, Lcom/android/launcher2/Workspace;->bO:F

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 2718
    :cond_17
    return-void
.end method

.method public getAllShortcutAndWidgetContainers()Ljava/util/ArrayList;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 3958
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3959
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    move v2, v1

    .line 3960
    :goto_b
    if-lt v2, v4, :cond_22

    .line 3963
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 3964
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v2

    .line 3965
    :goto_1f
    if-lt v1, v2, :cond_33

    .line 3970
    :cond_21
    return-object v3

    .line 3961
    :cond_22
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3960
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 3966
    :cond_33
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3967
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    .line 3966
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3965
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f
.end method

.method public getBackgroundAlpha()F
    .registers 2

    .prologue
    .line 1410
    iget v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    return v0
.end method

.method public getChildrenOutlineAlpha()F
    .registers 2

    .prologue
    .line 1364
    iget v0, p0, Lcom/android/launcher2/Workspace;->o:F

    return v0
.end method

.method public getContent()Landroid/view/View;
    .registers 1

    .prologue
    .line 2039
    return-object p0
.end method

.method public getCurrentDropLayout()Lcom/android/launcher2/CellLayout;
    .registers 2

    .prologue
    .line 3661
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getNextPage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    return-object v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .registers 5

    .prologue
    .line 4203
    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_2f

    iget v0, p0, Lcom/android/launcher2/Workspace;->v:I

    .line 4204
    :goto_8
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702bb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 4205
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 4204
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 4203
    :cond_2f
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    goto :goto_8
.end method

.method public getDescendantFocusability()I
    .registers 2

    .prologue
    .line 1656
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1657
    const/high16 v0, 0x6

    .line 1659
    :goto_8
    return v0

    :cond_9
    invoke-super {p0}, Lcom/android/launcher2/je;->getDescendantFocusability()I

    move-result v0

    goto :goto_8
.end method

.method public getDragInfo()Lcom/android/launcher2/as;
    .registers 2

    .prologue
    .line 3671
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aO:Lcom/android/launcher2/as;

    return-object v0
.end method

.method public final getDropTargetDelegate$2b911dda()Lcom/android/launcher2/bx;
    .registers 2

    .prologue
    .line 2861
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHitRect(Landroid/graphics/Rect;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3309
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 3310
    return-void
.end method

.method getOpenFolder()Lcom/android/launcher2/Folder;
    .registers 6

    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    .line 501
    invoke-virtual {v2}, Lcom/android/launcher2/DragLayer;->getChildCount()I

    move-result v3

    .line 502
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-lt v1, v3, :cond_10

    .line 510
    const/4 v0, 0x0

    :cond_f
    return-object v0

    .line 503
    :cond_10
    invoke-virtual {v2, v1}, Lcom/android/launcher2/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 504
    instance-of v4, v0, Lcom/android/launcher2/Folder;

    if-eqz v4, :cond_22

    .line 505
    check-cast v0, Lcom/android/launcher2/Folder;

    .line 506
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getInfo()Lcom/android/launcher2/cu;

    move-result-object v4

    iget-boolean v4, v4, Lcom/android/launcher2/cu;->a:Z

    if-nez v4, :cond_f

    .line 502
    :cond_22
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method protected getScrollMode()I
    .registers 2

    .prologue
    .line 469
    const/4 v0, 0x1

    return v0
.end method

.method getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 3937
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3938
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    move v2, v1

    .line 3939
    :goto_b
    if-lt v2, v4, :cond_22

    .line 3942
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 3943
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getChildCount()I

    move-result v2

    .line 3944
    :goto_1f
    if-lt v1, v2, :cond_2f

    .line 3948
    :cond_21
    return-object v3

    .line 3940
    :cond_22
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3939
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 3945
    :cond_2f
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3944
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f
.end method

.method public final h(I)V
    .registers 4
    .parameter

    .prologue
    .line 3793
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bL:Landroid/util/SparseArray;

    if-eqz v0, :cond_18

    .line 3794
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->bM:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3795
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3796
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->bL:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->a(Landroid/util/SparseArray;)V

    .line 3798
    :cond_18
    return-void
.end method

.method public final h()Z
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 3896
    .line 3897
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bj:Z

    if-eqz v0, :cond_27

    .line 3898
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 3900
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cx:Z

    if-eqz v0, :cond_22

    .line 3901
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3902
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->cx:Z

    .line 3906
    :goto_18
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3907
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V

    .line 3909
    const/4 v0, 0x1

    .line 3910
    iput-boolean v1, p0, Lcom/android/launcher2/Workspace;->bj:Z

    .line 3912
    :goto_21
    return v0

    .line 3904
    :cond_22
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getCurrentDropLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    goto :goto_18

    :cond_27
    move v0, v1

    goto :goto_21
.end method

.method public final i()V
    .registers 4

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 405
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    .line 406
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-lt v1, v2, :cond_f

    .line 411
    :cond_e
    return-void

    .line 407
    :cond_f
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 408
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->buildLayer()V

    .line 406
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method public final i(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 4671
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4672
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    move v2, v3

    .line 4674
    :goto_10
    if-lt v2, v4, :cond_3e

    .line 4681
    add-int/lit8 v0, p1, 0x1

    :goto_14
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_55

    .line 4682
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->removeView(Landroid/view/View;)V

    .line 4683
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/Workspace;->u:I

    if-gt v0, v1, :cond_30

    .line 4684
    iput v3, p0, Lcom/android/launcher2/Workspace;->u:I

    .line 4685
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4687
    :cond_30
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    .line 4688
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/preference/h;->a(I)V

    .line 4689
    return-void

    .line 4675
    :cond_3e
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4676
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 4678
    check-cast v1, Lcom/android/launcher2/di;

    .line 4679
    iget-object v5, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v5, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/di;)V

    .line 4674
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_10

    .line 4681
    :cond_55
    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Workspace;->g(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_14
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    return v0
.end method

.method public final k()Z
    .registers 3

    .prologue
    .line 696
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/android/launcher2/Workspace;->ch:F

    const/high16 v1, 0x3f00

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method protected final l()V
    .registers 11

    .prologue
    const/4 v4, 0x0

    .line 855
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v6

    move v5, v4

    .line 856
    :goto_6
    if-lt v5, v6, :cond_9

    .line 875
    return-void

    .line 857
    :cond_9
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 858
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v7

    .line 859
    invoke-virtual {v7}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v8

    move v3, v4

    .line 860
    :goto_18
    if-lt v3, v8, :cond_1e

    .line 856
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    .line 861
    :cond_1e
    invoke-virtual {v7, v3}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 863
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/launcher2/fz;

    if-eqz v2, :cond_49

    .line 864
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/fz;

    .line 865
    iget-object v2, v1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    check-cast v2, Lcom/android/launcher2/fy;

    .line 866
    if-eqz v2, :cond_49

    invoke-virtual {v2}, Lcom/android/launcher2/fy;->a()Z

    move-result v9

    if-eqz v9, :cond_49

    .line 867
    iget-object v9, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v9, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fz;)V

    .line 869
    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 870
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/fz;)V

    .line 860
    :cond_49
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_18
.end method

.method public final m()V
    .registers 2

    .prologue
    .line 1049
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setWallpaperDimension(Z)V

    .line 1050
    return-void
.end method

.method public final o(I)V
    .registers 3
    .parameter

    .prologue
    .line 4338
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_11

    .line 4339
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->r(I)V

    .line 4347
    :goto_9
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 4348
    return-void

    .line 4342
    :cond_11
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    goto :goto_9
.end method

.method protected onAttachedToWindow()V
    .registers 3

    .prologue
    .line 1593
    invoke-super {p0}, Lcom/android/launcher2/je;->onAttachedToWindow()V

    .line 1594
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aN:Landroid/os/IBinder;

    .line 1595
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->computeScroll()V

    .line 1596
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aN:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bk;->a(Landroid/os/IBinder;)V

    .line 1597
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 474
    instance-of v0, p2, Lcom/android/launcher2/CellLayout;

    if-nez v0, :cond_d

    .line 475
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A Workspace can only have CellLayout children."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_d
    check-cast p2, Lcom/android/launcher2/CellLayout;

    .line 478
    invoke-virtual {p2, p0}, Lcom/android/launcher2/CellLayout;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 479
    invoke-virtual {p2, v2}, Lcom/android/launcher2/CellLayout;->setClickable(Z)V

    .line 480
    invoke-virtual {p2}, Lcom/android/launcher2/CellLayout;->a()V

    .line 481
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 482
    const v1, 0x7f0702b9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 481
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/launcher2/CellLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 483
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 487
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 1600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->aN:Landroid/os/IBinder;

    .line 1601
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 15
    .parameter

    .prologue
    const/high16 v12, 0x3f80

    const/high16 v3, 0x3f00

    const v11, 0x3727c5ac

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1613
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cl:Z

    if-nez v0, :cond_47

    .line 1614
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_47

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->i:Z

    if-eqz v0, :cond_88

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget v3, v0, Lcom/android/launcher2/kh;->a:F

    iput v3, v0, Lcom/android/launcher2/kh;->c:F

    iget v3, v0, Lcom/android/launcher2/kh;->b:F

    iput v3, v0, Lcom/android/launcher2/kh;->d:F

    iput-boolean v2, p0, Lcom/android/launcher2/Workspace;->i:Z

    move v0, v1

    move v1, v2

    :goto_27
    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aN:Landroid/os/IBinder;

    if-eqz v0, :cond_42

    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->U()Z

    move-result v0

    if-eqz v0, :cond_150

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    iget-object v3, p0, Lcom/android/launcher2/Workspace;->aN:Landroid/os/IBinder;

    iget-object v4, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget v4, v4, Lcom/android/launcher2/kh;->c:F

    iget-object v5, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget v5, v5, Lcom/android/launcher2/kh;->d:F

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    :cond_42
    :goto_42
    if-eqz v1, :cond_47

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 1618
    :cond_47
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7c

    iget v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7c

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->b:Z

    if-eqz v0, :cond_7c

    .line 1619
    iget v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    const/high16 v1, 0x437f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1620
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1621
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 1622
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getMeasuredHeight()I

    move-result v4

    .line 1621
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1623
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aI:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1626
    :cond_7c
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->onDraw(Landroid/graphics/Canvas;)V

    .line 1629
    new-instance v0, Lcom/android/launcher2/jw;

    invoke-direct {v0, p0}, Lcom/android/launcher2/jw;-><init>(Lcom/android/launcher2/Workspace;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 1635
    return-void

    .line 1614
    :cond_88
    iget-object v4, p0, Lcom/android/launcher2/Workspace;->h:Lcom/android/launcher2/kh;

    iget v0, v4, Lcom/android/launcher2/kh;->c:F

    iget v5, v4, Lcom/android/launcher2/kh;->a:F

    invoke-static {v0, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_a3

    iget v0, v4, Lcom/android/launcher2/kh;->d:F

    iget v5, v4, Lcom/android/launcher2/kh;->b:F

    invoke-static {v0, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_a3

    iput-boolean v2, v4, Lcom/android/launcher2/kh;->f:Z

    move v1, v2

    :goto_a1
    move v0, v1

    goto :goto_27

    :cond_a3
    iget-object v0, v4, Lcom/android/launcher2/kh;->j:Lcom/android/launcher2/Workspace;

    iget-object v0, v0, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v5, v4, Lcom/android/launcher2/kh;->j:Lcom/android/launcher2/Workspace;

    iget-object v5, v5, Lcom/android/launcher2/Workspace;->bt:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    if-le v0, v5, :cond_122

    move v0, v1

    :goto_b2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, v4, Lcom/android/launcher2/kh;->e:J

    sub-long/2addr v5, v7

    const-wide/16 v7, 0x21

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    const-wide/16 v7, 0x1

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    iget v7, v4, Lcom/android/launcher2/kh;->a:F

    iget v8, v4, Lcom/android/launcher2/kh;->c:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget-boolean v8, v4, Lcom/android/launcher2/kh;->f:Z

    if-nez v8, :cond_de

    float-to-double v7, v7

    const-wide v9, 0x3fb1eb851eb851ecL

    cmpl-double v7, v7, v9

    if-lez v7, :cond_de

    iput-boolean v1, v4, Lcom/android/launcher2/kh;->f:Z

    :cond_de
    iget-boolean v7, v4, Lcom/android/launcher2/kh;->g:Z

    if-eqz v7, :cond_124

    iget v3, v4, Lcom/android/launcher2/kh;->h:F

    :cond_e4
    :goto_e4
    move v0, v3

    :goto_e5
    iget v3, v4, Lcom/android/launcher2/kh;->i:F

    const/high16 v7, 0x4204

    div-float v7, v0, v7

    const/high16 v0, 0x4204

    div-float/2addr v3, v0

    iget v0, v4, Lcom/android/launcher2/kh;->a:F

    iget v8, v4, Lcom/android/launcher2/kh;->c:F

    sub-float v8, v0, v8

    iget v0, v4, Lcom/android/launcher2/kh;->b:F

    iget v9, v4, Lcom/android/launcher2/kh;->d:F

    sub-float v9, v0, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v11

    if-gez v0, :cond_135

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v11

    if-gez v0, :cond_135

    move v0, v1

    :goto_10b
    iget-object v10, v4, Lcom/android/launcher2/kh;->j:Lcom/android/launcher2/Workspace;

    iget-boolean v10, v10, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v10, :cond_113

    if-eqz v0, :cond_137

    :cond_113
    iget v0, v4, Lcom/android/launcher2/kh;->a:F

    iput v0, v4, Lcom/android/launcher2/kh;->c:F

    iget v0, v4, Lcom/android/launcher2/kh;->b:F

    iput v0, v4, Lcom/android/launcher2/kh;->d:F

    :goto_11b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/android/launcher2/kh;->e:J

    goto :goto_a1

    :cond_122
    move v0, v2

    goto :goto_b2

    :cond_124
    iget-boolean v7, v4, Lcom/android/launcher2/kh;->f:Z

    if-eqz v7, :cond_12f

    if-eqz v0, :cond_12c

    move v0, v3

    goto :goto_e5

    :cond_12c
    const/high16 v0, 0x3f40

    goto :goto_e5

    :cond_12f
    if-eqz v0, :cond_e4

    const v3, 0x3e8a3d71

    goto :goto_e4

    :cond_135
    move v0, v2

    goto :goto_10b

    :cond_137
    long-to-float v0, v5

    mul-float/2addr v0, v3

    invoke-static {v12, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    long-to-float v3, v5

    mul-float/2addr v3, v7

    invoke-static {v12, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget v5, v4, Lcom/android/launcher2/kh;->c:F

    mul-float/2addr v3, v8

    add-float/2addr v3, v5

    iput v3, v4, Lcom/android/launcher2/kh;->c:F

    iget v3, v4, Lcom/android/launcher2/kh;->d:F

    mul-float/2addr v0, v9

    add-float/2addr v0, v3

    iput v0, v4, Lcom/android/launcher2/kh;->d:F

    goto :goto_11b

    :cond_150
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->L()V

    goto/16 :goto_42
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v12, 0x6

    const-wide/16 v10, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 714
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    .line 715
    iget v0, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_50

    .line 716
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_25

    .line 717
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    :goto_1f
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-lt v5, v2, :cond_27

    :cond_25
    move v5, v4

    .line 851
    :cond_26
    :goto_26
    return v5

    .line 717
    :cond_27
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/launcher2/Workspace;->g(Landroid/view/View;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v0

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollY()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v1

    invoke-virtual {v2, v3, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_4d

    iget v2, p0, Lcom/android/launcher2/Workspace;->u:I

    if-eq v2, v5, :cond_48

    invoke-virtual {p0, v5}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    :cond_48
    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->G()V

    :cond_4d
    add-int/lit8 v5, v5, 0x1

    goto :goto_1f

    .line 721
    :cond_50
    if-nez v1, :cond_b4

    .line 722
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 723
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->cq:F

    .line 724
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->cr:F

    .line 731
    :cond_64
    :goto_64
    if-eq v1, v4, :cond_68

    .line 732
    if-ne v1, v12, :cond_155

    .line 733
    :cond_68
    iget-wide v2, p0, Lcom/android/launcher2/Workspace;->cu:J

    sget-wide v6, Lcom/android/launcher2/Workspace;->cw:J

    add-long/2addr v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v2, v6

    if-lez v0, :cond_155

    .line 734
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->F:Z

    .line 735
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 736
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget v3, p0, Lcom/android/launcher2/Workspace;->cq:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    .line 737
    iget v3, p0, Lcom/android/launcher2/Workspace;->cr:F

    sub-float v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    .line 738
    if-ne v1, v4, :cond_de

    .line 739
    iget v6, p0, Lcom/android/launcher2/Workspace;->G:I

    mul-int/lit8 v6, v6, 0x3

    if-lt v3, v6, :cond_155

    if-le v3, v2, :cond_155

    .line 740
    iget v1, p0, Lcom/android/launcher2/Workspace;->cr:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_d0

    .line 741
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ar:Ljava/lang/String;

    const-string v2, "SWIPE_UP"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    :goto_ad
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->k:Z

    .line 746
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cu:J

    move v5, v4

    .line 747
    goto/16 :goto_26

    .line 725
    :cond_b4
    const/4 v0, 0x5

    if-ne v1, v0, :cond_64

    .line 726
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v8, :cond_64

    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->T()Z

    move-result v0

    if-eqz v0, :cond_64

    .line 727
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->cs:F

    .line 728
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->ct:F

    goto :goto_64

    .line 743
    :cond_d0
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->as:Ljava/lang/String;

    const-string v2, "SWIPE_DOWN"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_ad

    .line 749
    :cond_de
    if-ne v1, v12, :cond_155

    .line 750
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ne v6, v8, :cond_155

    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->T()Z

    move-result v6

    if-eqz v6, :cond_155

    .line 751
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 752
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    iget v8, p0, Lcom/android/launcher2/Workspace;->cs:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    float-to-int v7, v7

    .line 753
    iget v8, p0, Lcom/android/launcher2/Workspace;->ct:F

    sub-float v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    float-to-int v8, v8

    .line 754
    iget v9, p0, Lcom/android/launcher2/Workspace;->G:I

    mul-int/lit8 v9, v9, 0x3

    if-lt v3, v9, :cond_155

    if-le v3, v2, :cond_155

    .line 755
    iget v2, p0, Lcom/android/launcher2/Workspace;->G:I

    mul-int/lit8 v2, v2, 0x3

    if-lt v8, v2, :cond_155

    .line 756
    if-le v8, v7, :cond_155

    .line 757
    iget v2, p0, Lcom/android/launcher2/Workspace;->cr:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_135

    iget v2, p0, Lcom/android/launcher2/Workspace;->ct:F

    cmpg-float v2, v6, v2

    if-gez v2, :cond_135

    .line 758
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->at:Ljava/lang/String;

    const-string v2, "TWO_FINGER_SWIPE_UP"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 760
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->k:Z

    move v5, v4

    .line 761
    goto/16 :goto_26

    .line 762
    :cond_135
    iget v2, p0, Lcom/android/launcher2/Workspace;->cr:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_155

    iget v0, p0, Lcom/android/launcher2/Workspace;->ct:F

    cmpl-float v0, v6, v0

    if-lez v0, :cond_155

    .line 763
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->au:Ljava/lang/String;

    const-string v2, "TWO_FINGER_SWIPE_DOWN"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 765
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->k:Z

    move v5, v4

    .line 766
    goto/16 :goto_26

    .line 773
    :cond_155
    const-string v0, "DO_NOTHING"

    iget-object v2, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->aq:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d1

    .line 784
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->k:Z

    if-eqz v0, :cond_1ae

    .line 785
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v4, :cond_1a4

    .line 786
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 787
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 786
    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->a(FFFF)D

    move-result-wide v0

    .line 788
    iget-wide v2, p0, Lcom/android/launcher2/Workspace;->l:D

    sub-double v0, v2, v0

    iget v2, p0, Lcom/android/launcher2/Workspace;->G:I

    mul-int/lit8 v2, v2, 0x8

    int-to-double v2, v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_26

    .line 789
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->k:Z

    .line 790
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 791
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->F:Z

    .line 792
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aq:Ljava/lang/String;

    const-string v2, "PINCH_IN"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v4

    .line 793
    goto/16 :goto_26

    .line 795
    :cond_1a4
    if-eq v1, v12, :cond_1aa

    .line 796
    if-eq v1, v4, :cond_1aa

    .line 797
    if-nez v1, :cond_26

    .line 798
    :cond_1aa
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->k:Z

    goto/16 :goto_26

    .line 801
    :cond_1ae
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v4, :cond_1d1

    .line 802
    const/4 v0, 0x5

    if-ne v1, v0, :cond_1d1

    .line 803
    iput-boolean v4, p0, Lcom/android/launcher2/Workspace;->k:Z

    .line 804
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 805
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 804
    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->a(FFFF)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/Workspace;->l:D

    goto/16 :goto_26

    .line 812
    :cond_1d1
    if-nez v1, :cond_210

    .line 813
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->av:Ljava/lang/String;

    const-string v2, "DO_NOTHING"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_210

    .line 814
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 815
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 816
    iget-boolean v6, p0, Lcom/android/launcher2/Workspace;->cm:Z

    if-eqz v6, :cond_1fe

    iget-wide v6, p0, Lcom/android/launcher2/Workspace;->cn:J

    sget-wide v8, Lcom/android/launcher2/Workspace;->cv:J

    add-long/2addr v6, v8

    cmp-long v6, v6, v2

    if-ltz v6, :cond_1fe

    .line 817
    iget-boolean v6, v0, Lcom/android/launcher2/LauncherApplication;->e:Z

    if-eqz v6, :cond_219

    .line 818
    :cond_1fe
    iput-boolean v5, v0, Lcom/android/launcher2/LauncherApplication;->e:Z

    .line 819
    iput-boolean v4, p0, Lcom/android/launcher2/Workspace;->cm:Z

    .line 820
    iput-wide v2, p0, Lcom/android/launcher2/Workspace;->cn:J

    .line 821
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->co:F

    .line 822
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->cp:F

    .line 837
    :cond_210
    sparse-switch v1, :sswitch_data_2b2

    .line 851
    :cond_213
    :goto_213
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    goto/16 :goto_26

    .line 823
    :cond_219
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cm:Z

    if-eqz v0, :cond_210

    .line 824
    iget-wide v6, p0, Lcom/android/launcher2/Workspace;->cn:J

    sget-wide v8, Lcom/android/launcher2/Workspace;->cv:J

    add-long/2addr v6, v8

    cmp-long v0, v6, v2

    if-lez v0, :cond_210

    .line 825
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 826
    iget v3, p0, Lcom/android/launcher2/Workspace;->co:F

    iget v6, p0, Lcom/android/launcher2/Workspace;->cp:F

    .line 825
    invoke-static {v0, v2, v3, v6}, Lcom/android/launcher2/Workspace;->a(FFFF)D

    move-result-wide v2

    .line 827
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const/high16 v0, 0x41c8

    iget v6, p0, Lcom/android/launcher2/Workspace;->q:F

    mul-float/2addr v0, v6

    float-to-double v6, v0

    cmpg-double v0, v2, v6

    if-gez v0, :cond_210

    .line 828
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->cm:Z

    .line 829
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cn:J

    .line 830
    iput-boolean v5, p0, Lcom/android/launcher2/Workspace;->F:Z

    .line 831
    iput-wide v10, p0, Lcom/android/launcher2/Workspace;->cu:J

    .line 832
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->av:Ljava/lang/String;

    const-string v2, "DOUBLE_TAP"

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_26

    .line 839
    :sswitch_25b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bG:F

    .line 840
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bH:F

    goto :goto_213

    .line 844
    :sswitch_268
    iget v0, p0, Lcom/android/launcher2/Workspace;->C:I

    if-nez v0, :cond_213

    .line 845
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 846
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->h()Z

    move-result v0

    if-nez v0, :cond_213

    .line 847
    iget-object v6, p0, Lcom/android/launcher2/Workspace;->aY:[I

    invoke-virtual {p0, v6}, Lcom/android/launcher2/Workspace;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    aget v1, v6, v5

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    aput v1, v6, v5

    aget v1, v6, v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    add-int/2addr v0, v1

    aput v0, v6, v4

    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aM:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_2af

    const-string v2, "android.wallpaper.tap"

    :goto_2a5
    aget v3, v6, v5

    aget v4, v6, v4

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/WallpaperManager;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V

    goto/16 :goto_213

    :cond_2af
    const-string v2, "android.wallpaper.secondaryTap"

    goto :goto_2a5

    .line 837
    :sswitch_data_2b2
    .sparse-switch
        0x0 -> :sswitch_25b
        0x1 -> :sswitch_268
        0x6 -> :sswitch_268
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1605
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->t:Z

    if-eqz v0, :cond_13

    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    if-ltz v0, :cond_13

    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_13

    .line 1606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->i:Z

    .line 1608
    :cond_13
    invoke-super/range {p0 .. p5}, Lcom/android/launcher2/je;->onLayout(ZIIII)V

    .line 1609
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1643
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-nez v0, :cond_18

    .line 1644
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 1645
    if-eqz v0, :cond_13

    .line 1646
    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/Folder;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 1651
    :goto_12
    return v0

    .line 1648
    :cond_13
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/je;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_12

    .line 1651
    :cond_18
    const/4 v0, 0x0

    goto :goto_12
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 3
    .parameter

    .prologue
    .line 3780
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 3781
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(I)V

    .line 3782
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 686
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->P()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->k()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 4392
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_e

    .line 4393
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 4395
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 4378
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->onViewAdded(Landroid/view/View;)V

    .line 4379
    instance-of v0, p1, Lcom/android/launcher2/CellLayout;

    if-nez v0, :cond_f

    .line 4380
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 4381
    const-string v1, "A Workspace can only have CellLayout children."

    .line 4380
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4383
    :cond_f
    check-cast p1, Lcom/android/launcher2/CellLayout;

    .line 4384
    invoke-virtual {p1, p0}, Lcom/android/launcher2/CellLayout;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4385
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->E:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 4386
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setClickable(Z)V

    .line 4387
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->a()V

    .line 4388
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter

    .prologue
    .line 700
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Launcher;->b(I)V

    .line 701
    return-void
.end method

.method protected final r(I)V
    .registers 2
    .parameter

    .prologue
    .line 1177
    invoke-super {p0, p1}, Lcom/android/launcher2/je;->r(I)V

    .line 1178
    invoke-direct {p0, p1}, Lcom/android/launcher2/Workspace;->j(I)V

    .line 1179
    return-void
.end method

.method protected final s()V
    .registers 2

    .prologue
    .line 1171
    invoke-super {p0}, Lcom/android/launcher2/je;->s()V

    .line 1172
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->j(I)V

    .line 1173
    return-void
.end method

.method public scrollTo(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4470
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/je;->scrollTo(II)V

    .line 4471
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Workspace;->cm:Z

    .line 4472
    return-void
.end method

.method public setBackgroundAlpha(F)V
    .registers 3
    .parameter

    .prologue
    .line 1403
    iget v0, p0, Lcom/android/launcher2/Workspace;->aJ:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_b

    .line 1404
    iput p1, p0, Lcom/android/launcher2/Workspace;->aJ:F

    .line 1405
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 1407
    :cond_b
    return-void
.end method

.method public setChildrenOutlineAlpha(F)V
    .registers 4
    .parameter

    .prologue
    .line 1356
    iput p1, p0, Lcom/android/launcher2/Workspace;->o:F

    .line 1357
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_b

    .line 1361
    return-void

    .line 1358
    :cond_b
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 1359
    invoke-virtual {v0, p1}, Lcom/android/launcher2/CellLayout;->setBackgroundAlpha(F)V

    .line 1357
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4
.end method

.method setCurrentDragOverlappingLayout(Lcom/android/launcher2/CellLayout;)V
    .registers 4
    .parameter

    .prologue
    .line 2797
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_a

    .line 2798
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setIsDragOverlapping(Z)V

    .line 2800
    :cond_a
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    .line 2801
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_16

    .line 2802
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aT:Lcom/android/launcher2/CellLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setIsDragOverlapping(Z)V

    .line 2804
    :cond_16
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 2805
    return-void
.end method

.method setCurrentDropLayout(Lcom/android/launcher2/CellLayout;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 2783
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_f

    .line 2784
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->e()V

    .line 2785
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->g()V

    .line 2787
    :cond_f
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    .line 2788
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_1a

    .line 2789
    iget-object v0, p0, Lcom/android/launcher2/Workspace;->aS:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->f()V

    .line 2791
    :cond_1a
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->f(Z)V

    .line 2792
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->R()V

    .line 2793
    invoke-direct {p0, v1, v1}, Lcom/android/launcher2/Workspace;->f(II)V

    .line 2794
    return-void
.end method

.method setDragMode(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 2816
    iget v0, p0, Lcom/android/launcher2/Workspace;->bI:I

    if-eq p1, v0, :cond_13

    .line 2817
    if-nez p1, :cond_14

    .line 2818
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->S()V

    .line 2821
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Workspace;->f(Z)V

    .line 2822
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->R()V

    .line 2833
    :cond_11
    :goto_11
    iput p1, p0, Lcom/android/launcher2/Workspace;->bI:I

    .line 2835
    :cond_13
    return-void

    .line 2823
    :cond_14
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1e

    .line 2824
    invoke-direct {p0, v1}, Lcom/android/launcher2/Workspace;->f(Z)V

    .line 2825
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->R()V

    goto :goto_11

    .line 2826
    :cond_1e
    if-ne p1, v1, :cond_27

    .line 2827
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->S()V

    .line 2828
    invoke-direct {p0, v1}, Lcom/android/launcher2/Workspace;->f(Z)V

    goto :goto_11

    .line 2829
    :cond_27
    const/4 v0, 0x3

    if-ne p1, v0, :cond_11

    .line 2830
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->S()V

    .line 2831
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->R()V

    goto :goto_11
.end method

.method setFadeForOverScroll(F)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/high16 v1, 0x3f00

    .line 4213
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->B()Z

    move-result v0

    if-nez v0, :cond_b

    .line 4226
    :goto_a
    return-void

    .line 4215
    :cond_b
    iput p1, p0, Lcom/android/launcher2/Workspace;->bo:F

    .line 4216
    sub-float v0, v4, p1

    mul-float/2addr v0, v1

    add-float v2, v1, v0

    .line 4217
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 4218
    const v1, 0x7f0d0037

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 4219
    const v3, 0x7f0d0038

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4220
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollingIndicator()Landroid/view/View;

    move-result-object v3

    .line 4222
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->C()V

    .line 4223
    if-eqz v1, :cond_36

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 4224
    :cond_36
    if-eqz v0, :cond_3b

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 4225
    :cond_3b
    sub-float v0, v4, p1

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_a
.end method

.method public setFinalScrollForPageChange(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 2699
    if-ltz p1, :cond_2d

    .line 2700
    invoke-virtual {p0}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->bN:I

    .line 2701
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2702
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getTranslationX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bP:F

    .line 2703
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getRotationY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bO:F

    .line 2704
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->p(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->q(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 2705
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Workspace;->setScrollX(I)V

    .line 2706
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 2707
    invoke-virtual {v0, v3}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 2709
    :cond_2d
    return-void
.end method

.method public setFinalTransitionTransform(Lcom/android/launcher2/CellLayout;)V
    .registers 4
    .parameter

    .prologue
    .line 3627
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->bi:Z

    if-eqz v0, :cond_49

    .line 3628
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 3629
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getScaleX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bQ:F

    .line 3630
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getScaleY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bR:F

    .line 3631
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getTranslationX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bT:F

    .line 3632
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getTranslationY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bU:F

    .line 3633
    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getRotationY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/Workspace;->bS:F

    .line 3634
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cc:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/android/launcher2/CellLayout;->setScaleX(F)V

    .line 3635
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cd:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/android/launcher2/CellLayout;->setScaleY(F)V

    .line 3636
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->ca:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/android/launcher2/CellLayout;->setTranslationX(F)V

    .line 3637
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cb:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/android/launcher2/CellLayout;->setTranslationY(F)V

    .line 3638
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->cg:[F

    aget v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/android/launcher2/CellLayout;->setRotationY(F)V

    .line 3640
    :cond_49
    return-void
.end method

.method public setWallpaperDimension(Z)V
    .registers 7
    .parameter

    .prologue
    .line 1053
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1054
    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 1055
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1056
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1060
    iget-boolean v3, p0, Lcom/android/launcher2/Workspace;->cG:Z

    if-eqz v3, :cond_3c

    .line 1061
    int-to-float v0, v1

    invoke-static {v1, v2}, Lcom/android/launcher2/Workspace;->d(II)F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->f:I

    .line 1062
    iput v1, p0, Lcom/android/launcher2/Workspace;->g:I

    .line 1074
    :goto_31
    new-instance v0, Lcom/android/launcher2/jr;

    const-string v1, "setWallpaperDimension"

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/jr;-><init>(Lcom/android/launcher2/Workspace;Ljava/lang/String;)V

    .line 1078
    invoke-virtual {v0}, Lcom/android/launcher2/jr;->start()V

    .line 1079
    return-void

    .line 1064
    :cond_3c
    if-nez p1, :cond_5b

    .line 1065
    const-string v3, "SINGLE_SCREEN"

    iget-object v4, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5b

    .line 1066
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v3, v4, :cond_5b

    .line 1067
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/android/launcher2/Workspace;->f:I

    .line 1068
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/android/launcher2/Workspace;->g:I

    goto :goto_31

    .line 1070
    :cond_5b
    int-to-float v0, v2

    const/high16 v2, 0x4000

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Workspace;->f:I

    .line 1071
    iput v1, p0, Lcom/android/launcher2/Workspace;->g:I

    goto :goto_31
.end method

.method setup(Lcom/android/launcher2/bk;)V
    .registers 4
    .parameter

    .prologue
    .line 3686
    new-instance v0, Lcom/android/launcher2/jg;

    iget-object v1, p0, Lcom/android/launcher2/Workspace;->aV:Lcom/android/launcher2/Launcher;

    invoke-direct {v0, v1}, Lcom/android/launcher2/jg;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Workspace;->bf:Lcom/android/launcher2/jg;

    .line 3687
    iput-object p1, p0, Lcom/android/launcher2/Workspace;->aX:Lcom/android/launcher2/bk;

    .line 3691
    invoke-direct {p0}, Lcom/android/launcher2/Workspace;->Q()V

    .line 3692
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cl:Z

    if-nez v0, :cond_16

    .line 3693
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Workspace;->setWallpaperDimension(Z)V

    .line 3695
    :cond_16
    return-void
.end method

.method protected final t()V
    .registers 2

    .prologue
    .line 1012
    invoke-super {p0}, Lcom/android/launcher2/je;->t()V

    .line 1013
    iget v0, p0, Lcom/android/launcher2/Workspace;->u:I

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(I)V

    .line 1014
    return-void
.end method

.method public final z()Z
    .registers 3

    .prologue
    .line 4296
    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->F:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->k:Z

    if-nez v0, :cond_13

    iget-boolean v0, p0, Lcom/android/launcher2/Workspace;->cz:Z

    if-nez v0, :cond_13

    iget v0, p0, Lcom/android/launcher2/Workspace;->cB:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method
