.class public final Lcom/android/launcher2/a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/android/launcher2/b;

.field private d:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 9
    .parameter

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    .line 83
    iput-object p1, p0, Lcom/android/launcher2/a;->d:Lcom/android/launcher2/Launcher;

    .line 84
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/launcher2/a;->a:Landroid/view/LayoutInflater;

    .line 87
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 89
    iget-object v6, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/launcher2/b;

    const v3, 0x7f07002c

    .line 90
    const v4, 0x7f020024

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/b;-><init>(Lcom/android/launcher2/a;Landroid/content/res/Resources;III)V

    .line 89
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v6, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/launcher2/b;

    const v3, 0x7f070287

    .line 93
    const v4, 0x7f02003b

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/b;-><init>(Lcom/android/launcher2/a;Landroid/content/res/Resources;III)V

    .line 92
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v6, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/launcher2/b;

    const v3, 0x7f070288

    .line 96
    const v4, 0x7f020034

    const/4 v5, 0x2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/b;-><init>(Lcom/android/launcher2/a;Landroid/content/res/Resources;III)V

    .line 95
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v0, Lcom/android/launcher2/b;

    const v3, 0x7f070289

    .line 102
    const v4, 0x7f02003e

    const/4 v5, 0x4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/b;-><init>(Lcom/android/launcher2/a;Landroid/content/res/Resources;III)V

    .line 101
    iput-object v0, p0, Lcom/android/launcher2/a;->c:Lcom/android/launcher2/b;

    .line 103
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/launcher2/a;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_1a

    .line 71
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/launcher2/a;->c:Lcom/android/launcher2/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 72
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/launcher2/a;->c:Lcom/android/launcher2/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_19
    :goto_19
    return-void

    .line 75
    :cond_1a
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/launcher2/a;->c:Lcom/android/launcher2/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 76
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/launcher2/a;->c:Lcom/android/launcher2/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_19
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/launcher2/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 129
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 106
    invoke-virtual {p0, p1}, Lcom/android/launcher2/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/b;

    .line 108
    if-nez p2, :cond_24

    .line 109
    iget-object v1, p0, Lcom/android/launcher2/a;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_13
    move-object v1, v2

    .line 112
    check-cast v1, Landroid/widget/TextView;

    .line 113
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 114
    iget-object v3, v0, Lcom/android/launcher2/b;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, v0, Lcom/android/launcher2/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 117
    return-object v2

    :cond_24
    move-object v2, p2

    goto :goto_13
.end method
