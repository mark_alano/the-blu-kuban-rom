.class final Lcom/android/launcher2/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/CellLayout;

.field private final synthetic b:Lcom/android/launcher2/CellLayout$LayoutParams;

.field private final synthetic c:I

.field private final synthetic d:I

.field private final synthetic e:I

.field private final synthetic f:I

.field private final synthetic g:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/CellLayout$LayoutParams;IIIILandroid/view/View;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ap;->a:Lcom/android/launcher2/CellLayout;

    iput-object p2, p0, Lcom/android/launcher2/ap;->b:Lcom/android/launcher2/CellLayout$LayoutParams;

    iput p3, p0, Lcom/android/launcher2/ap;->c:I

    iput p4, p0, Lcom/android/launcher2/ap;->d:I

    iput p5, p0, Lcom/android/launcher2/ap;->e:I

    iput p6, p0, Lcom/android/launcher2/ap;->f:I

    iput-object p7, p0, Lcom/android/launcher2/ap;->g:Landroid/view/View;

    .line 1162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 1165
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1166
    iget-object v1, p0, Lcom/android/launcher2/ap;->b:Lcom/android/launcher2/CellLayout$LayoutParams;

    sub-float v2, v4, v0

    iget v3, p0, Lcom/android/launcher2/ap;->c:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/ap;->d:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 1167
    iget-object v1, p0, Lcom/android/launcher2/ap;->b:Lcom/android/launcher2/CellLayout$LayoutParams;

    sub-float v2, v4, v0

    iget v3, p0, Lcom/android/launcher2/ap;->e:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/ap;->f:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 1168
    iget-object v0, p0, Lcom/android/launcher2/ap;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1169
    return-void
.end method
