.class final Lcom/android/launcher2/aq;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/launcher2/CellLayout;

.field private final synthetic c:Lcom/android/launcher2/CellLayout$LayoutParams;

.field private final synthetic d:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/CellLayout$LayoutParams;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/aq;->b:Lcom/android/launcher2/CellLayout;

    iput-object p2, p0, Lcom/android/launcher2/aq;->c:Lcom/android/launcher2/CellLayout$LayoutParams;

    iput-object p3, p0, Lcom/android/launcher2/aq;->d:Landroid/view/View;

    .line 1171
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 1172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/aq;->a:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter

    .prologue
    .line 1186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/aq;->a:Z

    .line 1187
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 1177
    iget-boolean v0, p0, Lcom/android/launcher2/aq;->a:Z

    if-nez v0, :cond_e

    .line 1178
    iget-object v0, p0, Lcom/android/launcher2/aq;->c:Lcom/android/launcher2/CellLayout$LayoutParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 1179
    iget-object v0, p0, Lcom/android/launcher2/aq;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1181
    :cond_e
    iget-object v0, p0, Lcom/android/launcher2/aq;->b:Lcom/android/launcher2/CellLayout;

    invoke-static {v0}, Lcom/android/launcher2/CellLayout;->e(Lcom/android/launcher2/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/aq;->c:Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1182
    iget-object v0, p0, Lcom/android/launcher2/aq;->b:Lcom/android/launcher2/CellLayout;

    invoke-static {v0}, Lcom/android/launcher2/CellLayout;->e(Lcom/android/launcher2/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/aq;->c:Lcom/android/launcher2/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184
    :cond_27
    return-void
.end method
