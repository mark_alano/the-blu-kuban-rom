.class final Lcom/android/launcher2/au;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/view/View;

.field b:F

.field c:F

.field d:F

.field e:F

.field f:F

.field g:F

.field h:Landroid/animation/Animator;

.field final synthetic i:Lcom/android/launcher2/CellLayout;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/CellLayout;Landroid/view/View;IIIIII)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2097
    iput-object p1, p0, Lcom/android/launcher2/au;->i:Lcom/android/launcher2/CellLayout;

    .line 2096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2098
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v5

    move-object v0, p1

    move v1, p3

    move v2, p4

    move/from16 v3, p7

    move/from16 v4, p8

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(IIII[I)V

    .line 2099
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 2100
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x1

    aget v7, v0, v1

    .line 2101
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v5

    move-object v0, p1

    move v1, p5

    move/from16 v2, p6

    move/from16 v3, p7

    move/from16 v4, p8

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(IIII[I)V

    .line 2102
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2103
    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/CellLayout;)[I

    move-result-object v1

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 2104
    sub-int/2addr v0, v6

    .line 2105
    sub-int/2addr v1, v7

    .line 2106
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/launcher2/au;->b:F

    .line 2107
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/launcher2/au;->c:F

    .line 2108
    if-ne v0, v1, :cond_4a

    if-eqz v0, :cond_59

    .line 2110
    :cond_4a
    if-nez v1, :cond_91

    .line 2111
    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    neg-float v0, v0

    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/CellLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/au;->b:F

    .line 2122
    :cond_59
    :goto_59
    invoke-virtual {p2}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/au;->d:F

    .line 2123
    invoke-virtual {p2}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/au;->e:F

    .line 2124
    const/high16 v0, 0x3f80

    const/high16 v1, 0x4080

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/au;->f:F

    .line 2125
    invoke-virtual {p2}, Landroid/view/View;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/au;->g:F

    .line 2127
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    invoke-virtual {p2, v0}, Landroid/view/View;->setPivotY(F)V

    .line 2128
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    invoke-virtual {p2, v0}, Landroid/view/View;->setPivotX(F)V

    .line 2129
    iput-object p2, p0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    .line 2130
    return-void

    .line 2112
    :cond_91
    if-nez v0, :cond_a1

    .line 2113
    int-to-float v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    neg-float v0, v0

    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/CellLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/au;->c:F

    goto :goto_59

    .line 2115
    :cond_a1
    int-to-float v2, v1

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    .line 2116
    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    neg-float v0, v0

    float-to-double v4, v0

    .line 2117
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/CellLayout;)F

    move-result v0

    float-to-double v8, v0

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v0, v4

    int-to-float v0, v0

    .line 2116
    iput v0, p0, Lcom/android/launcher2/au;->b:F

    .line 2118
    int-to-float v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    neg-float v0, v0

    float-to-double v0, v0

    .line 2119
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {p1}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/CellLayout;)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    int-to-float v0, v0

    .line 2118
    iput v0, p0, Lcom/android/launcher2/au;->c:F

    goto/16 :goto_59
.end method


# virtual methods
.method final a()V
    .registers 10

    .prologue
    const/high16 v5, 0x3f80

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2183
    iget-object v0, p0, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    if-eqz v0, :cond_e

    .line 2184
    iget-object v0, p0, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 2187
    :cond_e
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2188
    iput-object v0, p0, Lcom/android/launcher2/au;->h:Landroid/animation/Animator;

    .line 2189
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/animation/Animator;

    .line 2190
    iget-object v2, p0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    const-string v3, "scaleX"

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    .line 2191
    iget-object v2, p0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    const-string v3, "scaleY"

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    .line 2192
    iget-object v3, p0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    const-string v4, "translationX"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 2193
    iget-object v3, p0, Lcom/android/launcher2/au;->a:Landroid/view/View;

    const-string v4, "translationY"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2189
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2195
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2196
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc0

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2197
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2198
    return-void
.end method
