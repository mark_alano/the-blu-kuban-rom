.class final Lcom/android/launcher2/bb;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field final synthetic a:Lcom/android/launcher2/az;


# direct methods
.method private constructor <init>(Lcom/android/launcher2/az;)V
    .registers 2
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/launcher2/az;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/android/launcher2/bb;-><init>(Lcom/android/launcher2/az;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    iget-object v1, v0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 42
    :try_start_5
    iget-object v0, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    iget-object v0, v0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_11

    .line 43
    monitor-exit v1

    .line 48
    :goto_10
    return-void

    .line 45
    :cond_11
    iget-object v0, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    iget-object v0, v0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 41
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_5 .. :try_end_1c} :catchall_2e

    .line 47
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 48
    iget-object v0, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    iget-object v1, v0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 49
    :try_start_24
    iget-object v0, p0, Lcom/android/launcher2/bb;->a:Lcom/android/launcher2/az;

    invoke-virtual {v0}, Lcom/android/launcher2/az;->b()V

    .line 48
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_24 .. :try_end_2a} :catchall_2b

    goto :goto_10

    :catchall_2b
    move-exception v0

    monitor-exit v1

    throw v0

    .line 41
    :catchall_2e
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final queueIdle()Z
    .registers 2

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bb;->handleMessage(Landroid/os/Message;)V

    .line 55
    const/4 v0, 0x0

    return v0
.end method
