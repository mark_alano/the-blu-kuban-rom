.class final Lcom/android/launcher2/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/DeleteDropTarget;

.field private final synthetic b:Lcom/android/launcher2/DragLayer;

.field private final synthetic c:Landroid/animation/TimeInterpolator;

.field private final synthetic d:F

.field private final synthetic e:F

.field private final synthetic f:F

.field private final synthetic g:F

.field private final synthetic h:F

.field private final synthetic i:F


# direct methods
.method constructor <init>(Lcom/android/launcher2/DeleteDropTarget;Lcom/android/launcher2/DragLayer;Landroid/animation/TimeInterpolator;FFFFFF)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/bg;->a:Lcom/android/launcher2/DeleteDropTarget;

    iput-object p2, p0, Lcom/android/launcher2/bg;->b:Lcom/android/launcher2/DragLayer;

    iput-object p3, p0, Lcom/android/launcher2/bg;->c:Landroid/animation/TimeInterpolator;

    iput p4, p0, Lcom/android/launcher2/bg;->d:F

    iput p5, p0, Lcom/android/launcher2/bg;->e:F

    iput p6, p0, Lcom/android/launcher2/bg;->f:F

    iput p7, p0, Lcom/android/launcher2/bg;->g:F

    iput p8, p0, Lcom/android/launcher2/bg;->h:F

    iput p9, p0, Lcom/android/launcher2/bg;->i:F

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 14
    .parameter

    .prologue
    const/high16 v11, 0x3f00

    const/high16 v10, 0x4000

    const/high16 v9, 0x3f80

    .line 337
    iget-object v0, p0, Lcom/android/launcher2/bg;->b:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->getAnimatedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/bu;

    .line 338
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 339
    iget-object v2, p0, Lcom/android/launcher2/bg;->c:Landroid/animation/TimeInterpolator;

    invoke-interface {v2, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    .line 340
    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getInitialScale()F

    move-result v3

    .line 341
    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getScaleX()F

    move-result v4

    .line 343
    sub-float v5, v9, v4

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v10

    .line 344
    sub-float v4, v9, v4

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v4, v6

    div-float/2addr v4, v10

    .line 345
    sub-float v6, v9, v1

    sub-float v7, v9, v1

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/android/launcher2/bg;->d:F

    sub-float/2addr v7, v5

    mul-float/2addr v6, v7

    sub-float v7, v9, v1

    mul-float/2addr v7, v10

    mul-float/2addr v7, v1

    iget v8, p0, Lcom/android/launcher2/bg;->e:F

    sub-float/2addr v8, v5

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    .line 346
    mul-float v7, v1, v1

    iget v8, p0, Lcom/android/launcher2/bg;->f:F

    mul-float/2addr v7, v8

    .line 345
    add-float/2addr v6, v7

    .line 347
    sub-float v7, v9, v1

    sub-float v8, v9, v1

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/android/launcher2/bg;->g:F

    sub-float v4, v8, v4

    mul-float/2addr v4, v7

    sub-float v7, v9, v1

    mul-float/2addr v7, v10

    mul-float/2addr v7, v1

    iget v8, p0, Lcom/android/launcher2/bg;->h:F

    sub-float v5, v8, v5

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    .line 348
    mul-float/2addr v1, v1

    iget v5, p0, Lcom/android/launcher2/bg;->i:F

    mul-float/2addr v1, v5

    .line 347
    add-float/2addr v1, v4

    .line 350
    invoke-virtual {v0, v6}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 351
    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    .line 352
    sub-float v1, v9, v2

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setScaleX(F)V

    .line 353
    sub-float v1, v9, v2

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setScaleY(F)V

    .line 354
    sub-float v1, v9, v2

    mul-float/2addr v1, v11

    add-float/2addr v1, v11

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setAlpha(F)V

    .line 355
    return-void
.end method
