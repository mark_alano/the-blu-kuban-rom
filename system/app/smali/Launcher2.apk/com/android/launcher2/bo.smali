.class final Lcom/android/launcher2/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/DragLayer;

.field private final synthetic b:Lcom/android/launcher2/bu;

.field private final synthetic c:Landroid/view/animation/Interpolator;

.field private final synthetic d:Landroid/view/animation/Interpolator;

.field private final synthetic e:F

.field private final synthetic f:F

.field private final synthetic g:F

.field private final synthetic h:F

.field private final synthetic i:F

.field private final synthetic j:F

.field private final synthetic k:F

.field private final synthetic l:Landroid/graphics/Rect;

.field private final synthetic m:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/android/launcher2/DragLayer;Lcom/android/launcher2/bu;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;FFFFFLandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    .line 1
    iput-object p1, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    iput-object p2, p0, Lcom/android/launcher2/bo;->b:Lcom/android/launcher2/bu;

    iput-object p3, p0, Lcom/android/launcher2/bo;->c:Landroid/view/animation/Interpolator;

    iput-object p4, p0, Lcom/android/launcher2/bo;->d:Landroid/view/animation/Interpolator;

    iput v0, p0, Lcom/android/launcher2/bo;->e:F

    iput p5, p0, Lcom/android/launcher2/bo;->f:F

    iput v0, p0, Lcom/android/launcher2/bo;->g:F

    iput p6, p0, Lcom/android/launcher2/bo;->h:F

    iput p7, p0, Lcom/android/launcher2/bo;->i:F

    iput p8, p0, Lcom/android/launcher2/bo;->j:F

    iput p9, p0, Lcom/android/launcher2/bo;->k:F

    iput-object p10, p0, Lcom/android/launcher2/bo;->l:Landroid/graphics/Rect;

    iput-object p11, p0, Lcom/android/launcher2/bo;->m:Landroid/graphics/Rect;

    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 14
    .parameter

    .prologue
    const/high16 v11, 0x4000

    const/high16 v10, 0x3f80

    .line 569
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 570
    iget-object v0, p0, Lcom/android/launcher2/bo;->b:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v3

    .line 571
    iget-object v0, p0, Lcom/android/launcher2/bo;->b:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v4

    .line 573
    iget-object v0, p0, Lcom/android/launcher2/bo;->c:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_d7

    move v0, v1

    .line 575
    :goto_1f
    iget-object v2, p0, Lcom/android/launcher2/bo;->d:Landroid/view/animation/Interpolator;

    if-nez v2, :cond_df

    move v2, v1

    .line 578
    :goto_24
    iget v5, p0, Lcom/android/launcher2/bo;->e:F

    iget v6, p0, Lcom/android/launcher2/bo;->f:F

    mul-float/2addr v5, v6

    .line 579
    iget v6, p0, Lcom/android/launcher2/bo;->g:F

    iget v7, p0, Lcom/android/launcher2/bo;->f:F

    mul-float/2addr v6, v7

    .line 580
    iget v7, p0, Lcom/android/launcher2/bo;->h:F

    mul-float/2addr v7, v1

    sub-float v8, v10, v1

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    .line 581
    iget v8, p0, Lcom/android/launcher2/bo;->i:F

    mul-float/2addr v8, v1

    sub-float v1, v10, v1

    mul-float/2addr v1, v6

    add-float/2addr v1, v8

    .line 582
    iget v8, p0, Lcom/android/launcher2/bo;->j:F

    mul-float/2addr v8, v0

    iget v9, p0, Lcom/android/launcher2/bo;->k:F

    sub-float v0, v10, v0

    mul-float/2addr v0, v9

    add-float/2addr v8, v0

    .line 584
    iget-object v0, p0, Lcom/android/launcher2/bo;->l:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float/2addr v5, v10

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v11

    add-float/2addr v0, v3

    .line 585
    iget-object v3, p0, Lcom/android/launcher2/bo;->l:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float v5, v6, v10

    int-to-float v4, v4

    mul-float/2addr v4, v5

    div-float/2addr v4, v11

    add-float/2addr v3, v4

    .line 587
    iget-object v4, p0, Lcom/android/launcher2/bo;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v4, v0

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 588
    iget-object v4, p0, Lcom/android/launcher2/bo;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 590
    iget-object v3, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v3}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->getScrollX()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->b(Lcom/android/launcher2/DragLayer;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e7

    .line 591
    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->c(Lcom/android/launcher2/DragLayer;)I

    move-result v0

    iget-object v4, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v4}, Lcom/android/launcher2/DragLayer;->b(Lcom/android/launcher2/DragLayer;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getScrollX()I

    move-result v4

    sub-int/2addr v0, v4

    .line 590
    :goto_9b
    add-int/2addr v0, v3

    .line 592
    iget-object v3, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v3}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    .line 594
    iget-object v3, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v3}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v3

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 595
    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v0

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    .line 596
    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/launcher2/bu;->setScaleX(F)V

    .line 597
    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setScaleY(F)V

    .line 598
    iget-object v0, p0, Lcom/android/launcher2/bo;->a:Lcom/android/launcher2/DragLayer;

    invoke-static {v0}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/android/launcher2/bu;->setAlpha(F)V

    .line 599
    return-void

    .line 574
    :cond_d7
    iget-object v0, p0, Lcom/android/launcher2/bo;->c:Landroid/view/animation/Interpolator;

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto/16 :goto_1f

    .line 576
    :cond_df
    iget-object v2, p0, Lcom/android/launcher2/bo;->d:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    goto/16 :goto_24

    .line 591
    :cond_e7
    const/4 v0, 0x0

    goto :goto_9b
.end method
