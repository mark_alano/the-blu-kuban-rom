.class public final Lcom/android/launcher2/cb;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 620
    invoke-static {p0, p1}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    .line 621
    invoke-static {v0, p2, p3}, Lcom/android/launcher2/cb;->a(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 625
    invoke-static {p0, p1}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    .line 626
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v0, v1, p3}, Lcom/android/launcher2/cb;->a(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;II)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 607
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 614
    :cond_4
    add-int/2addr p1, p2

    .line 609
    if-ltz p1, :cond_9

    if-lt p1, v1, :cond_b

    .line 616
    :cond_9
    const/4 v0, 0x0

    :cond_a
    :goto_a
    return-object v0

    .line 610
    :cond_b
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 611
    instance-of v2, v0, Lcom/android/launcher2/BubbleTextView;

    if-nez v2, :cond_a

    instance-of v2, v0, Lcom/android/launcher2/FolderIcon;

    if-eqz v2, :cond_4

    goto :goto_a
.end method

.method private static a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 123
    check-cast p0, Lcom/android/launcher2/PagedView;

    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 124
    instance-of v1, v0, Lcom/android/launcher2/ik;

    if-eqz v1, :cond_13

    .line 126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 128
    :cond_13
    return-object v0
.end method

.method static a(Landroid/view/View;)Landroid/widget/TabHost;
    .registers 3
    .parameter

    .prologue
    .line 77
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 78
    :goto_4
    if-eqz v0, :cond_a

    instance-of v1, v0, Landroid/widget/TabHost;

    if-eqz v1, :cond_d

    .line 81
    :cond_a
    check-cast v0, Landroid/widget/TabHost;

    return-object v0

    .line 79
    :cond_d
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_4
.end method

.method private static a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v1

    .line 582
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 583
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 584
    const/4 v0, 0x0

    :goto_e
    if-lt v0, v2, :cond_19

    .line 587
    new-instance v0, Lcom/android/launcher2/cc;

    invoke-direct {v0, v1}, Lcom/android/launcher2/cc;-><init>(I)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 597
    return-object v3

    .line 585
    :cond_19
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    add-int/lit8 v0, v0, 0x1

    goto :goto_e
.end method

.method static a(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 285
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/im;

    if-eqz v0, :cond_79

    .line 286
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 287
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/ScrollView;

    if-eqz v1, :cond_71

    .line 288
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v2, v1

    :goto_21
    move-object v1, v2

    .line 292
    check-cast v1, Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getCellCountX()I

    move-result v3

    move-object v1, v2

    .line 293
    check-cast v1, Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getCellCountY()I

    move-result v1

    move-object v4, v2

    move v2, v3

    move-object v3, v0

    .line 302
    :goto_32
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 304
    instance-of v5, v0, Landroid/widget/ScrollView;

    if-eqz v5, :cond_90

    .line 305
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedView;

    .line 309
    :goto_42
    invoke-static {v0}, Lcom/android/launcher2/cb;->a(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v5

    .line 310
    invoke-virtual {v5}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v6

    .line 311
    invoke-virtual {v3, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    .line 312
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    .line 313
    invoke-virtual {v0, v4}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/launcher2/PagedView;->b(I)I

    move-result v9

    .line 314
    invoke-virtual {v0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v10

    .line 316
    rem-int v11, v7, v2

    .line 317
    div-int v12, v7, v2

    .line 319
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 320
    const/4 v5, 0x1

    if-eq v4, v5, :cond_97

    const/4 v4, 0x1

    move v5, v4

    .line 321
    :goto_6b
    const/4 v4, 0x0

    .line 326
    sparse-switch p1, :sswitch_data_19c

    move v0, v4

    .line 441
    :goto_70
    return v0

    .line 290
    :cond_71
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v2, v1

    goto :goto_21

    .line 295
    :cond_79
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 296
    check-cast v1, Lcom/android/launcher2/in;

    invoke-virtual {v1}, Lcom/android/launcher2/in;->getCellCountX()I

    move-result v2

    move-object v1, v0

    .line 297
    check-cast v1, Lcom/android/launcher2/in;

    invoke-virtual {v1}, Lcom/android/launcher2/in;->getCellCountY()I

    move-result v1

    move-object v3, v0

    move-object v4, v0

    goto :goto_32

    .line 307
    :cond_90
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedView;

    goto :goto_42

    .line 320
    :cond_97
    const/4 v4, 0x0

    move v5, v4

    goto :goto_6b

    .line 328
    :sswitch_9a
    if-eqz v5, :cond_a7

    .line 330
    if-lez v7, :cond_a9

    .line 331
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 343
    :cond_a7
    :goto_a7
    const/4 v0, 0x1

    .line 344
    goto :goto_70

    .line 333
    :cond_a9
    if-lez v9, :cond_a7

    .line 334
    add-int/lit8 v1, v9, -0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/cb;->a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_a7

    .line 336
    add-int/lit8 v2, v9, -0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 337
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_a7

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_a7

    .line 346
    :sswitch_c8
    if-eqz v5, :cond_d7

    .line 348
    add-int/lit8 v1, v8, -0x1

    if-ge v7, v1, :cond_d9

    .line 349
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 361
    :cond_d7
    :goto_d7
    const/4 v0, 0x1

    .line 362
    goto :goto_70

    .line 351
    :cond_d9
    add-int/lit8 v1, v10, -0x1

    if-ge v9, v1, :cond_d7

    .line 352
    add-int/lit8 v1, v9, 0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/cb;->a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 353
    if-eqz v1, :cond_d7

    .line 354
    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 355
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_d7

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_d7

    .line 364
    :sswitch_f5
    if-eqz v5, :cond_104

    .line 366
    if-lez v12, :cond_107

    .line 367
    add-int/lit8 v0, v12, -0x1

    mul-int/2addr v0, v2

    add-int/2addr v0, v11

    .line 368
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 373
    :cond_104
    :goto_104
    const/4 v0, 0x1

    .line 374
    goto/16 :goto_70

    .line 370
    :cond_107
    invoke-virtual {v6}, Landroid/widget/TabWidget;->requestFocus()Z

    goto :goto_104

    .line 376
    :sswitch_10b
    if-eqz v5, :cond_122

    .line 378
    add-int/lit8 v0, v1, -0x1

    if-ge v12, v0, :cond_122

    .line 379
    add-int/lit8 v0, v8, -0x1

    add-int/lit8 v1, v12, 0x1

    mul-int/2addr v1, v2

    add-int/2addr v1, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 380
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 383
    :cond_122
    const/4 v0, 0x1

    .line 384
    goto/16 :goto_70

    .line 387
    :sswitch_125
    if-eqz v5, :cond_12c

    .line 389
    check-cast v0, Landroid/view/View$OnClickListener;

    .line 390
    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 392
    :cond_12c
    const/4 v0, 0x1

    .line 393
    goto/16 :goto_70

    .line 395
    :sswitch_12f
    if-eqz v5, :cond_14a

    .line 398
    if-lez v9, :cond_14d

    .line 399
    add-int/lit8 v1, v9, -0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/cb;->a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 400
    if-eqz v1, :cond_14a

    .line 401
    add-int/lit8 v2, v9, -0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 402
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_14a

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 409
    :cond_14a
    :goto_14a
    const/4 v0, 0x1

    .line 410
    goto/16 :goto_70

    .line 406
    :cond_14d
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_14a

    .line 412
    :sswitch_156
    if-eqz v5, :cond_173

    .line 415
    add-int/lit8 v1, v10, -0x1

    if-ge v9, v1, :cond_176

    .line 416
    add-int/lit8 v1, v9, 0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/cb;->a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 417
    if-eqz v1, :cond_173

    .line 418
    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 419
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 420
    if-eqz v0, :cond_173

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 426
    :cond_173
    :goto_173
    const/4 v0, 0x1

    .line 427
    goto/16 :goto_70

    .line 423
    :cond_176
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_173

    .line 429
    :sswitch_180
    if-eqz v5, :cond_18a

    .line 431
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 433
    :cond_18a
    const/4 v0, 0x1

    .line 434
    goto/16 :goto_70

    .line 436
    :sswitch_18d
    if-eqz v5, :cond_198

    .line 438
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 440
    :cond_198
    const/4 v0, 0x1

    goto/16 :goto_70

    .line 326
    nop

    :sswitch_data_19c
    .sparse-switch
        0x13 -> :sswitch_f5
        0x14 -> :sswitch_10b
        0x15 -> :sswitch_9a
        0x16 -> :sswitch_c8
        0x17 -> :sswitch_125
        0x42 -> :sswitch_125
        0x5c -> :sswitch_12f
        0x5d -> :sswitch_156
        0x7a -> :sswitch_180
        0x7b -> :sswitch_18d
    .end sparse-switch
.end method

.method static a(Lcom/android/launcher2/AccessibleTabView;ILandroid/view/KeyEvent;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 451
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_9

    .line 495
    :goto_8
    return v2

    .line 453
    :cond_9
    invoke-virtual {p0}, Lcom/android/launcher2/AccessibleTabView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/FocusOnlyTabWidget;

    .line 454
    invoke-static {v0}, Lcom/android/launcher2/cb;->a(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v4

    .line 455
    invoke-virtual {v4}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v5

    .line 456
    invoke-virtual {v0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getTabCount()I

    move-result v6

    .line 457
    invoke-virtual {v0, p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->a(Landroid/view/View;)I

    move-result v7

    .line 459
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    .line 460
    if-eq v3, v1, :cond_2c

    move v3, v1

    .line 462
    :goto_26
    packed-switch p1, :pswitch_data_6a

    move v0, v2

    :goto_2a
    move v2, v0

    .line 495
    goto :goto_8

    :cond_2c
    move v3, v2

    .line 460
    goto :goto_26

    .line 464
    :pswitch_2e
    if-eqz v3, :cond_3b

    .line 466
    if-lez v7, :cond_3b

    .line 467
    add-int/lit8 v2, v7, -0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_3b
    move v0, v1

    .line 471
    goto :goto_2a

    .line 473
    :pswitch_3d
    if-eqz v3, :cond_4c

    .line 475
    add-int/lit8 v2, v6, -0x1

    if-ge v7, v2, :cond_4e

    .line 476
    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_4c
    :goto_4c
    move v0, v1

    .line 484
    goto :goto_2a

    .line 478
    :cond_4e
    invoke-virtual {p0}, Lcom/android/launcher2/AccessibleTabView;->getNextFocusRightId()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4c

    .line 479
    invoke-virtual {p0}, Lcom/android/launcher2/AccessibleTabView;->getNextFocusRightId()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_4c

    :pswitch_61
    move v0, v1

    .line 488
    goto :goto_2a

    .line 490
    :pswitch_63
    if-eqz v3, :cond_68

    .line 492
    invoke-virtual {v5}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_68
    move v0, v1

    .line 494
    goto :goto_2a

    .line 462
    :pswitch_data_6a
    .packed-switch 0x13
        :pswitch_61
        :pswitch_63
        :pswitch_2e
        :pswitch_3d
    .end packed-switch
.end method

.method static b(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 636
    invoke-static/range {p0 .. p1}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v9

    .line 637
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 638
    invoke-virtual {p0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v2

    .line 639
    iget v10, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 640
    add-int v3, v10, p3

    .line 641
    if-ltz v3, :cond_8d

    if-ge v3, v2, :cond_8d

    .line 642
    const v3, 0x7f7fffff

    .line 643
    const/4 v5, -0x1

    .line 644
    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 645
    if-gez p3, :cond_30

    const/4 v2, -0x1

    move v4, v2

    :goto_24
    move v7, v3

    .line 646
    :goto_25
    if-ne v6, v4, :cond_36

    .line 665
    if-ltz v5, :cond_8d

    .line 666
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 669
    :goto_2f
    return-object v1

    .line 645
    :cond_30
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v4, v2

    goto :goto_24

    .line 647
    :cond_36
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 648
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 649
    if-gez p3, :cond_80

    iget v8, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    if-ge v8, v10, :cond_7e

    const/4 v8, 0x1

    .line 650
    :goto_49
    if-eqz v8, :cond_8f

    .line 651
    instance-of v8, v2, Lcom/android/launcher2/BubbleTextView;

    if-nez v8, :cond_53

    instance-of v2, v2, Lcom/android/launcher2/FolderIcon;

    if-eqz v2, :cond_8f

    .line 652
    :cond_53
    iget v2, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v8, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    sub-int/2addr v2, v8

    int-to-double v11, v2

    const-wide/high16 v13, 0x4000

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    .line 653
    iget v2, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v3, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    const-wide/high16 v13, 0x4000

    invoke-static {v2, v3, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 652
    add-double/2addr v2, v11

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 654
    cmpg-float v3, v2, v7

    if-gez v3, :cond_8f

    move v3, v2

    move v2, v6

    .line 659
    :goto_77
    if-gt v6, v4, :cond_88

    .line 660
    add-int/lit8 v6, v6, 0x1

    move v5, v2

    move v7, v3

    goto :goto_25

    .line 649
    :cond_7e
    const/4 v8, 0x0

    goto :goto_49

    :cond_80
    iget v8, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    if-le v8, v10, :cond_86

    const/4 v8, 0x1

    goto :goto_49

    :cond_86
    const/4 v8, 0x0

    goto :goto_49

    .line 662
    :cond_88
    add-int/lit8 v6, v6, -0x1

    move v5, v2

    move v7, v3

    goto :goto_25

    .line 669
    :cond_8d
    const/4 v1, 0x0

    goto :goto_2f

    :cond_8f
    move v2, v5

    move v3, v7

    goto :goto_77
.end method

.method private static b(Landroid/view/ViewGroup;I)Lcom/android/launcher2/ja;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 570
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 571
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    return-object v0
.end method

.method static b(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 505
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 506
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 507
    const v4, 0x7f0d0036

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Workspace;

    .line 508
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v5

    .line 509
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 510
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v7

    .line 516
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 517
    if-eq v4, v2, :cond_2f

    move v4, v2

    .line 519
    :goto_2a
    packed-switch p1, :pswitch_data_7c

    move v0, v3

    .line 559
    :goto_2e
    return v0

    :cond_2f
    move v4, v3

    .line 517
    goto :goto_2a

    .line 521
    :pswitch_31
    if-eqz v4, :cond_3e

    .line 523
    if-lez v5, :cond_40

    .line 524
    add-int/lit8 v1, v5, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_3e
    :goto_3e
    move v0, v2

    .line 530
    goto :goto_2e

    .line 526
    :cond_40
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_3e

    .line 532
    :pswitch_46
    if-eqz v4, :cond_55

    .line 534
    add-int/lit8 v3, v6, -0x1

    if-ge v5, v3, :cond_57

    .line 535
    add-int/lit8 v1, v5, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_55
    :goto_55
    move v0, v2

    .line 541
    goto :goto_2e

    .line 537
    :cond_57
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_55

    .line 543
    :pswitch_5d
    if-eqz v4, :cond_73

    .line 545
    invoke-virtual {v1, v7}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 546
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v3

    .line 547
    const/4 v4, -0x1

    invoke-static {v0, v3, v4, v2}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_75

    .line 549
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_73
    :goto_73
    move v0, v2

    .line 555
    goto :goto_2e

    .line 551
    :cond_75
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->requestFocus()Z

    goto :goto_73

    :pswitch_79
    move v0, v2

    .line 558
    goto :goto_2e

    .line 519
    nop

    :pswitch_data_7c
    .packed-switch 0x13
        :pswitch_5d
        :pswitch_79
        :pswitch_31
        :pswitch_46
    .end packed-switch
.end method

.method static c(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v5, 0x1

    .line 676
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    .line 677
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    .line 678
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Workspace;

    .line 679
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 680
    const v4, 0x7f0d003b

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 681
    const v7, 0x7f0d003a

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 682
    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v8

    .line 683
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v9

    .line 685
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    .line 686
    if-eq v7, v5, :cond_41

    move v7, v5

    .line 688
    :goto_3c
    sparse-switch p1, :sswitch_data_12a

    :cond_3f
    move v0, v6

    .line 821
    :goto_40
    return v0

    :cond_41
    move v7, v6

    .line 686
    goto :goto_3c

    .line 690
    :sswitch_43
    if-eqz v7, :cond_4e

    .line 692
    invoke-static {v1, v0, p0, v10}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 693
    if-eqz v0, :cond_50

    .line 694
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_4e
    :goto_4e
    move v0, v5

    .line 710
    goto :goto_40

    .line 696
    :cond_50
    if-lez v8, :cond_4e

    .line 697
    add-int/lit8 v0, v8, -0x1

    invoke-static {v2, v0}, Lcom/android/launcher2/cb;->b(Landroid/view/ViewGroup;I)Lcom/android/launcher2/ja;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v3

    .line 698
    invoke-static {v1, v0, v3, v10}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 700
    if-eqz v0, :cond_66

    .line 701
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_4e

    .line 704
    :cond_66
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_4e

    .line 712
    :sswitch_6c
    if-eqz v7, :cond_77

    .line 714
    invoke-static {v1, v0, p0, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 715
    if-eqz v0, :cond_79

    .line 716
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_77
    :goto_77
    move v0, v5

    .line 731
    goto :goto_40

    .line 718
    :cond_79
    add-int/lit8 v0, v9, -0x1

    if-ge v8, v0, :cond_77

    .line 719
    add-int/lit8 v0, v8, 0x1

    invoke-static {v2, v0}, Lcom/android/launcher2/cb;->b(Landroid/view/ViewGroup;I)Lcom/android/launcher2/ja;

    move-result-object v0

    .line 720
    invoke-static {v1, v0, v10, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 721
    if-eqz v0, :cond_8d

    .line 722
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_77

    .line 725
    :cond_8d
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_77

    .line 733
    :sswitch_93
    if-eqz v7, :cond_3f

    .line 735
    invoke-static {v1, v0, p0, v10}, Lcom/android/launcher2/cb;->b(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 736
    if-eqz v0, :cond_a0

    .line 737
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_9e
    :goto_9e
    move v0, v5

    .line 820
    goto :goto_40

    .line 740
    :cond_a0
    invoke-virtual {v4}, Landroid/view/ViewGroup;->requestFocus()Z

    move v0, v6

    .line 743
    goto :goto_40

    .line 745
    :sswitch_a5
    if-eqz v7, :cond_3f

    .line 747
    invoke-static {v1, v0, p0, v5}, Lcom/android/launcher2/cb;->b(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 748
    if-eqz v0, :cond_b1

    .line 749
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_9e

    .line 751
    :cond_b1
    if-eqz v3, :cond_3f

    .line 752
    invoke-virtual {v3}, Landroid/view/ViewGroup;->requestFocus()Z

    move v0, v6

    .line 755
    goto :goto_40

    .line 757
    :sswitch_b8
    if-eqz v7, :cond_cb

    .line 760
    if-lez v8, :cond_d4

    .line 761
    add-int/lit8 v0, v8, -0x1

    invoke-static {v2, v0}, Lcom/android/launcher2/cb;->b(Landroid/view/ViewGroup;I)Lcom/android/launcher2/ja;

    move-result-object v0

    .line 762
    invoke-static {v1, v0, v10, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 763
    if-eqz v0, :cond_ce

    .line 764
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_cb
    :goto_cb
    move v0, v5

    .line 777
    goto/16 :goto_40

    .line 767
    :cond_ce
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_cb

    .line 770
    :cond_d4
    invoke-static {v1, v0, v10, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 771
    if-eqz v0, :cond_cb

    .line 772
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_cb

    .line 779
    :sswitch_de
    if-eqz v7, :cond_f3

    .line 782
    add-int/lit8 v3, v9, -0x1

    if-ge v8, v3, :cond_fc

    .line 783
    add-int/lit8 v0, v8, 0x1

    invoke-static {v2, v0}, Lcom/android/launcher2/cb;->b(Landroid/view/ViewGroup;I)Lcom/android/launcher2/ja;

    move-result-object v0

    .line 784
    invoke-static {v1, v0, v10, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_f6

    .line 786
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_f3
    :goto_f3
    move v0, v5

    .line 800
    goto/16 :goto_40

    .line 789
    :cond_f6
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Workspace;->r(I)V

    goto :goto_f3

    .line 793
    :cond_fc
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v2

    .line 792
    invoke-static {v1, v0, v2, v10}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 794
    if-eqz v0, :cond_f3

    .line 795
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_f3

    .line 802
    :sswitch_10a
    if-eqz v7, :cond_115

    .line 804
    invoke-static {v1, v0, v10, v5}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 805
    if-eqz v0, :cond_115

    .line 806
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_115
    move v0, v5

    .line 810
    goto/16 :goto_40

    .line 812
    :sswitch_118
    if-eqz v7, :cond_9e

    .line 815
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v2

    .line 814
    invoke-static {v1, v0, v2, v10}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 816
    if-eqz v0, :cond_9e

    .line 817
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_9e

    .line 688
    nop

    :sswitch_data_12a
    .sparse-switch
        0x13 -> :sswitch_93
        0x14 -> :sswitch_a5
        0x15 -> :sswitch_43
        0x16 -> :sswitch_6c
        0x5c -> :sswitch_b8
        0x5d -> :sswitch_de
        0x7a -> :sswitch_10a
        0x7b -> :sswitch_118
    .end sparse-switch
.end method
