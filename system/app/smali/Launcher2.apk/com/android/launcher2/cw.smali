.class final Lcom/android/launcher2/cw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x1

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Folder;

    iget-object v5, v2, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eq v2, v3, :cond_27

    move v2, v3

    :goto_22
    sparse-switch p2, :sswitch_data_84

    move v0, v4

    :goto_26
    return v0

    :cond_27
    move v2, v4

    goto :goto_22

    :sswitch_29
    if-eqz v2, :cond_34

    invoke-static {v1, v0, p1, v6}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_34

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_34
    move v0, v3

    goto :goto_26

    :sswitch_36
    if-eqz v2, :cond_41

    invoke-static {v1, v0, p1, v3}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_43

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_41
    :goto_41
    move v0, v3

    goto :goto_26

    :cond_43
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_41

    :sswitch_47
    if-eqz v2, :cond_52

    invoke-static {v1, v0, p1, v6}, Lcom/android/launcher2/cb;->b(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_52

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_52
    move v0, v3

    goto :goto_26

    :sswitch_54
    if-eqz v2, :cond_5f

    invoke-static {v1, v0, p1, v3}, Lcom/android/launcher2/cb;->b(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_61

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_5f
    :goto_5f
    move v0, v3

    goto :goto_26

    :cond_61
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_5f

    :sswitch_65
    if-eqz v2, :cond_70

    invoke-static {v1, v0, v6, v3}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_70

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_70
    move v0, v3

    goto :goto_26

    :sswitch_72
    if-eqz v2, :cond_81

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v2

    invoke-static {v1, v0, v2, v6}, Lcom/android/launcher2/cb;->a(Lcom/android/launcher2/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_81

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_81
    move v0, v3

    goto :goto_26

    nop

    :sswitch_data_84
    .sparse-switch
        0x13 -> :sswitch_47
        0x14 -> :sswitch_54
        0x15 -> :sswitch_29
        0x16 -> :sswitch_36
        0x7a -> :sswitch_65
        0x7b -> :sswitch_72
    .end sparse-switch
.end method
