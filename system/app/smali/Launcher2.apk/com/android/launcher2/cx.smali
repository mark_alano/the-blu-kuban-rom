.class public final Lcom/android/launcher2/cx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I

.field private static final g:Landroid/graphics/BlurMaskFilter;

.field private static final h:Landroid/graphics/BlurMaskFilter;

.field private static final i:Landroid/graphics/BlurMaskFilter;

.field private static final j:Landroid/graphics/BlurMaskFilter;

.field private static final k:Landroid/graphics/BlurMaskFilter;

.field private static final l:Landroid/graphics/BlurMaskFilter;

.field private static final m:Landroid/graphics/BlurMaskFilter;

.field private static final o:Landroid/graphics/MaskFilter;


# instance fields
.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private n:[I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/high16 v2, 0x4140

    const/high16 v6, 0x40c0

    const/high16 v5, 0x4000

    const/high16 v4, 0x3f80

    .line 50
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v0

    .line 52
    mul-float v1, v0, v4

    float-to-int v1, v1

    sput v1, Lcom/android/launcher2/cx;->b:I

    .line 53
    mul-float v1, v0, v2

    float-to-int v1, v1

    sput v1, Lcom/android/launcher2/cx;->a:I

    .line 55
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float/2addr v2, v0

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->g:Landroid/graphics/BlurMaskFilter;

    .line 56
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v6

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->h:Landroid/graphics/BlurMaskFilter;

    .line 57
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v5

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->i:Landroid/graphics/BlurMaskFilter;

    .line 58
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v4

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->j:Landroid/graphics/BlurMaskFilter;

    .line 59
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v6

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->l:Landroid/graphics/BlurMaskFilter;

    .line 60
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    const/high16 v2, 0x4080

    mul-float/2addr v2, v0

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->k:Landroid/graphics/BlurMaskFilter;

    .line 61
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float/2addr v0, v5

    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v0, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    sput-object v1, Lcom/android/launcher2/cx;->m:Landroid/graphics/BlurMaskFilter;

    .line 218
    const/4 v0, 0x0

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/cx;->o:Landroid/graphics/MaskFilter;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cx;->f:Landroid/graphics/Paint;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/cx;->n:[I

    .line 67
    iget-object v0, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 68
    iget-object v0, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 69
    iget-object v0, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 70
    iget-object v0, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 71
    iget-object v0, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 72
    iget-object v0, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 73
    iget-object v0, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 74
    const/16 v0, 0xb4

    const/16 v1, 0xff

    invoke-static {v0, v1}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/android/launcher2/cx;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 76
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;III)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    iget-object v5, p0, Lcom/android/launcher2/cx;->f:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IILandroid/graphics/Paint;I)V

    .line 107
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IILandroid/graphics/Paint;I)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 113
    if-nez p5, :cond_6

    .line 114
    iget-object v0, p0, Lcom/android/launcher2/cx;->f:Landroid/graphics/Paint;

    move-object/from16 p5, v0

    .line 116
    :cond_6
    iget-object v1, p0, Lcom/android/launcher2/cx;->n:[I

    move-object/from16 v0, p5

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 120
    packed-switch p6, :pswitch_data_114

    .line 131
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Invalid blur thickness"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :pswitch_19
    sget-object v1, Lcom/android/launcher2/cx;->g:Landroid/graphics/BlurMaskFilter;

    .line 133
    :goto_1b
    iget-object v2, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 134
    const/4 v1, 0x2

    new-array v8, v1, [I

    .line 135
    iget-object v1, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v8}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 136
    const/4 v1, 0x2

    move/from16 v0, p6

    if-ne v0, v1, :cond_5d

    .line 137
    iget-object v1, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/launcher2/cx;->i:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 142
    :goto_35
    const/4 v1, 0x2

    new-array v10, v1, [I

    .line 143
    iget-object v1, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v10}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 146
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 147
    const/high16 v1, -0x100

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 149
    packed-switch p6, :pswitch_data_11e

    .line 160
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Invalid blur thickness"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :pswitch_57
    sget-object v1, Lcom/android/launcher2/cx;->h:Landroid/graphics/BlurMaskFilter;

    goto :goto_1b

    .line 128
    :pswitch_5a
    sget-object v1, Lcom/android/launcher2/cx;->i:Landroid/graphics/BlurMaskFilter;

    goto :goto_1b

    .line 139
    :cond_5d
    iget-object v1, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/launcher2/cx;->j:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_35

    .line 151
    :pswitch_65
    sget-object v1, Lcom/android/launcher2/cx;->l:Landroid/graphics/BlurMaskFilter;

    .line 162
    :goto_67
    iget-object v2, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 163
    const/4 v1, 0x2

    new-array v12, v1, [I

    .line 164
    iget-object v1, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v12}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 167
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 168
    const/4 v1, 0x0

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v1, v1

    .line 169
    const/4 v2, 0x1

    aget v2, v12, v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    .line 168
    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v4, v1

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v5, v1

    .line 171
    iget-object v6, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    move-object/from16 v1, p2

    .line 170
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 172
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v4, v1

    const/4 v1, 0x1

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v5, v1

    .line 173
    iget-object v6, p0, Lcom/android/launcher2/cx;->e:Landroid/graphics/Paint;

    move-object/from16 v1, p2

    .line 172
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 176
    move-object/from16 v0, p2

    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 177
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 178
    iget-object v1, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    const/4 v1, 0x0

    aget v1, v12, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v12, v2

    int-to-float v2, v2

    .line 180
    iget-object v3, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    .line 179
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/4 v1, 0x0

    aget v1, v8, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v8, v2

    int-to-float v2, v2

    .line 182
    iget-object v3, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    .line 181
    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    iget-object v1, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 186
    const/4 v1, 0x0

    aget v1, v10, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v10, v2

    int-to-float v2, v2

    .line 187
    iget-object v3, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    .line 186
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 190
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 191
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 192
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 193
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 194
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 195
    return-void

    .line 154
    :pswitch_10c
    sget-object v1, Lcom/android/launcher2/cx;->k:Landroid/graphics/BlurMaskFilter;

    goto/16 :goto_67

    .line 157
    :pswitch_110
    sget-object v1, Lcom/android/launcher2/cx;->m:Landroid/graphics/BlurMaskFilter;

    goto/16 :goto_67

    .line 120
    :pswitch_data_114
    .packed-switch 0x0
        :pswitch_57
        :pswitch_5a
        :pswitch_19
    .end packed-switch

    .line 149
    :pswitch_data_11e
    .packed-switch 0x0
        :pswitch_10c
        :pswitch_110
        :pswitch_65
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    sget-object v1, Lcom/android/launcher2/cx;->h:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 226
    iget-object v0, p0, Lcom/android/launcher2/cx;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/launcher2/cx;->n:[I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/launcher2/cx;->o:Landroid/graphics/MaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 230
    iget-object v1, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    const/16 v2, 0x96

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 231
    iget-object v1, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 233
    iget-object v1, p0, Lcom/android/launcher2/cx;->n:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/launcher2/cx;->n:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/launcher2/cx;->c:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 234
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 235
    return-void
.end method

.method final a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 199
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;III)V

    .line 200
    return-void
.end method

.method final b(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 209
    const/4 v5, 0x0

    .line 210
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 209
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IILandroid/graphics/Paint;I)V

    .line 211
    return-void
.end method

.method final c(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 215
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;III)V

    .line 216
    return-void
.end method
