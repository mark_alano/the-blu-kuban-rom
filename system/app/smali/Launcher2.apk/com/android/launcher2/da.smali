.class public final Lcom/android/launcher2/da;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/graphics/Bitmap;

.field protected b:Lcom/anddoes/launcher/c/l;

.field private final c:Lcom/android/launcher2/LauncherApplication;

.field private final d:Landroid/content/pm/PackageManager;

.field private final e:Ljava/util/HashMap;

.field private f:I


# direct methods
.method public constructor <init>(Lcom/android/launcher2/LauncherApplication;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    .line 59
    iput-object p1, p0, Lcom/android/launcher2/da;->c:Lcom/android/launcher2/LauncherApplication;

    .line 60
    invoke-virtual {p1}, Lcom/android/launcher2/LauncherApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    .line 61
    invoke-static {p1}, Lcom/anddoes/launcher/v;->e(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/da;->f:I

    .line 64
    invoke-virtual {p0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/android/launcher2/da;->a:Landroid/graphics/Bitmap;

    .line 65
    return-void
.end method

.method private b(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Lcom/android/launcher2/db;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/high16 v3, 0x3f00

    const/4 v2, 0x0

    .line 196
    iget-object v0, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/db;

    .line 197
    if-nez v0, :cond_53

    .line 198
    new-instance v5, Lcom/android/launcher2/db;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lcom/android/launcher2/db;-><init>(B)V

    .line 200
    iget-object v0, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-static {p2}, Lcom/android/launcher2/gb;->a(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v0

    .line 203
    if-eqz p3, :cond_54

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 204
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    .line 211
    :cond_32
    :goto_32
    iget-object v0, v5, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    if-nez v0, :cond_3c

    .line 212
    iget-object v0, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v0, v5, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    .line 215
    :cond_3c
    invoke-direct {p0, p2}, Lcom/android/launcher2/da;->b(Landroid/content/pm/ResolveInfo;)Lcom/android/launcher2/dc;

    move-result-object v6

    .line 216
    iget-object v0, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    if-eqz v0, :cond_48

    iget-boolean v0, v6, Lcom/android/launcher2/dc;->a:Z

    if-eqz v0, :cond_68

    .line 217
    :cond_48
    iget-object v0, v6, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher2/da;->c:Lcom/android/launcher2/LauncherApplication;

    invoke-static {v0, v1}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v5, Lcom/android/launcher2/db;->a:Landroid/graphics/Bitmap;

    move-object v0, v5

    .line 225
    :cond_53
    :goto_53
    return-object v0

    .line 206
    :cond_54
    iget-object v1, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {p2, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    .line 207
    if-eqz p3, :cond_32

    .line 208
    iget-object v1, v5, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_32

    .line 219
    :cond_68
    iget-object v0, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    iget-object v1, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    if-eqz v1, :cond_bf

    iget-object v0, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    iget-object v1, v0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_bd

    invoke-static {v1}, Lcom/anddoes/launcher/c/g;->a(I)I

    move-result v1

    iget-object v0, v0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    .line 220
    :goto_85
    iget-object v0, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    iget-object v7, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    if-eqz v7, :cond_a2

    iget-object v0, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    iget-object v7, v0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_a2

    invoke-static {v7}, Lcom/anddoes/launcher/c/g;->a(I)I

    move-result v2

    iget-object v0, v0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    .line 221
    :cond_a2
    iget-object v6, v6, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/android/launcher2/da;->c:Lcom/android/launcher2/LauncherApplication;

    .line 222
    iget-object v0, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    iget-object v8, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    if-eqz v8, :cond_ca

    iget-object v0, v0, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    iget v8, v0, Lcom/anddoes/launcher/c/g;->f:F

    cmpg-float v8, v8, v3

    if-gez v8, :cond_c1

    move v0, v3

    .line 221
    :goto_b5
    invoke-static {v6, v7, v1, v2, v0}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v5, Lcom/android/launcher2/db;->a:Landroid/graphics/Bitmap;

    move-object v0, v5

    goto :goto_53

    :cond_bd
    move-object v1, v2

    .line 219
    goto :goto_85

    :cond_bf
    move-object v1, v2

    goto :goto_85

    .line 222
    :cond_c1
    iget v3, v0, Lcom/anddoes/launcher/c/g;->f:F

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_ca

    iget v0, v0, Lcom/anddoes/launcher/c/g;->f:F

    goto :goto_b5

    :cond_ca
    move v0, v4

    goto :goto_b5
.end method

.method private b(Landroid/content/pm/ResolveInfo;)Lcom/android/launcher2/dc;
    .registers 5
    .parameter

    .prologue
    .line 242
    new-instance v0, Lcom/android/launcher2/dc;

    invoke-direct {v0, p0}, Lcom/android/launcher2/dc;-><init>(Lcom/android/launcher2/da;)V

    .line 245
    :try_start_5
    iget-object v1, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    if-eqz v1, :cond_19

    .line 246
    iget-object v1, p0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    iget v2, p0, Lcom/android/launcher2/da;->f:I

    invoke-virtual {v1, p1, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/content/pm/ResolveInfo;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 247
    if-eqz v1, :cond_19

    .line 248
    iput-object v1, v0, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    .line 249
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/dc;->a:Z

    .line 266
    :goto_18
    return-object v0

    .line 253
    :cond_19
    iget-object v1, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    .line 254
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 253
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_22
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_22} :catch_34

    move-result-object v1

    .line 258
    :goto_23
    if-eqz v1, :cond_37

    .line 259
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v2

    .line 260
    if-eqz v2, :cond_37

    .line 261
    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_18

    .line 256
    :catch_34
    move-exception v1

    const/4 v1, 0x0

    goto :goto_23

    .line 265
    :cond_37
    invoke-virtual {p0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_18
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 180
    iget-object v1, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 181
    if-eqz p2, :cond_7

    if-nez p1, :cond_a

    .line 182
    :cond_7
    monitor-exit v1

    const/4 v0, 0x0

    .line 186
    :goto_9
    return-object v0

    .line 185
    :cond_a
    :try_start_a
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/da;->b(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Lcom/android/launcher2/db;

    move-result-object v0

    .line 186
    iget-object v0, v0, Lcom/android/launcher2/db;->a:Landroid/graphics/Bitmap;

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_a .. :try_end_11} :catchall_12

    goto :goto_9

    .line 180
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/content/Intent;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter

    .prologue
    .line 165
    iget-object v1, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 166
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 169
    if-eqz v0, :cond_12

    if-nez v2, :cond_16

    .line 170
    :cond_12
    iget-object v0, p0, Lcom/android/launcher2/da;->a:Landroid/graphics/Bitmap;

    monitor-exit v1

    .line 174
    :goto_15
    return-object v0

    .line 173
    :cond_16
    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v3}, Lcom/android/launcher2/da;->b(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Lcom/android/launcher2/db;

    move-result-object v0

    .line 174
    iget-object v0, v0, Lcom/android/launcher2/db;->a:Landroid/graphics/Bitmap;

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1f

    goto :goto_15

    .line 165
    :catchall_1f
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 68
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 69
    const/high16 v1, 0x10d

    .line 68
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter

    .prologue
    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    .line 108
    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 107
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_7} :catch_15

    move-result-object v0

    .line 112
    :goto_8
    if-eqz v0, :cond_18

    .line 113
    invoke-virtual {p1}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v1

    .line 114
    if-eqz v1, :cond_18

    .line 115
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 118
    :goto_14
    return-object v0

    .line 110
    :catch_15
    move-exception v0

    const/4 v0, 0x0

    goto :goto_8

    .line 118
    :cond_18
    invoke-virtual {p0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_14
.end method

.method public final a(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/launcher2/da;->b(Landroid/content/pm/ResolveInfo;)Lcom/android/launcher2/dc;

    move-result-object v0

    .line 100
    iget-object v0, v0, Lcom/android/launcher2/dc;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 75
    :try_start_0
    iget v0, p0, Lcom/android/launcher2/da;->f:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_5} :catch_9

    move-result-object v0

    .line 80
    :goto_6
    if-eqz v0, :cond_c

    :goto_8
    return-object v0

    .line 77
    :catch_9
    move-exception v0

    const/4 v0, 0x0

    goto :goto_6

    .line 80
    :cond_c
    invoke-virtual {p0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_8
.end method

.method public final a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher2/da;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_5} :catch_f

    move-result-object v0

    .line 90
    :goto_6
    if-eqz v0, :cond_12

    .line 91
    if-eqz p2, :cond_12

    .line 92
    invoke-virtual {p0, v0, p2}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 95
    :goto_e
    return-object v0

    .line 88
    :catch_f
    move-exception v0

    const/4 v0, 0x0

    goto :goto_6

    .line 95
    :cond_12
    invoke-virtual {p0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_e
.end method

.method public final a(Landroid/content/ComponentName;)V
    .registers 4
    .parameter

    .prologue
    .line 137
    iget-object v1, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 138
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_a

    return-void

    :catchall_a
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/android/launcher2/h;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    iget-object v1, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 157
    :try_start_3
    iget-object v0, p1, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-direct {p0, v0, p2, p3}, Lcom/android/launcher2/da;->b(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Lcom/android/launcher2/db;

    move-result-object v0

    .line 159
    iget-object v2, v0, Lcom/android/launcher2/db;->b:Ljava/lang/String;

    iput-object v2, p1, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    .line 160
    iget-object v0, v0, Lcom/android/launcher2/db;->a:Landroid/graphics/Bitmap;

    iput-object v0, p1, Lcom/android/launcher2/h;->d:Landroid/graphics/Bitmap;

    .line 156
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_13

    return-void

    :catchall_13
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 146
    iget-object v1, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 147
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/da;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 146
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_a

    return-void

    :catchall_a
    move-exception v0

    monitor-exit v1

    throw v0
.end method
