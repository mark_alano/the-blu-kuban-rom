.class final Lcom/android/launcher2/dw;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/launcher2/Launcher;

.field private final synthetic c:Landroid/view/View;

.field private final synthetic d:Z

.field private final synthetic e:Lcom/android/launcher2/AppsCustomizeTabHost;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLcom/android/launcher2/AppsCustomizeTabHost;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/dw;->c:Landroid/view/View;

    iput-boolean p3, p0, Lcom/android/launcher2/dw;->d:Z

    iput-object p4, p0, Lcom/android/launcher2/dw;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 3218
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 3219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/dw;->a:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter

    .prologue
    .line 3239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/dw;->a:Z

    .line 3240
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3229
    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dw;->c:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/launcher2/dw;->d:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3230
    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dw;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-boolean v2, p0, Lcom/android/launcher2/dw;->d:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3232
    iget-boolean v0, p0, Lcom/android/launcher2/dw;->a:Z

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_26

    .line 3233
    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3235
    :cond_26
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 3223
    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/dw;->c:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/launcher2/dw;->d:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3224
    iget-object v0, p0, Lcom/android/launcher2/dw;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/dw;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-boolean v1, p0, Lcom/android/launcher2/dw;->d:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3225
    return-void
.end method
