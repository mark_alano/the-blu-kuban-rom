.class final Lcom/android/launcher2/dx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/dx;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/dx;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/android/launcher2/dx;->c:Landroid/view/View;

    .line 3326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 4
    .parameter

    .prologue
    .line 3329
    const/high16 v1, 0x3f80

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v1, v0

    .line 3330
    iget-object v1, p0, Lcom/android/launcher2/dx;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dx;->b:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;F)V

    .line 3331
    iget-object v1, p0, Lcom/android/launcher2/dx;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dx;->c:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;F)V

    .line 3332
    return-void
.end method
