.class public final Lcom/android/launcher2/e;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field final A:F

.field final B:F

.field private C:Landroid/view/View;

.field private D:Lcom/android/launcher2/DragLayer;

.field private E:Lcom/android/launcher2/Workspace;

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:Lcom/android/launcher2/Launcher;

.field a:Lcom/android/launcher2/CellLayout;

.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/ImageView;

.field d:Landroid/widget/ImageView;

.field e:Landroid/widget/ImageView;

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field x:[I

.field final y:I

.field final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/DragLayer;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x2

    .line 81
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    iput v4, p0, Lcom/android/launcher2/e;->v:I

    .line 62
    iput v4, p0, Lcom/android/launcher2/e;->w:I

    .line 64
    new-array v0, v8, [I

    iput-object v0, p0, Lcom/android/launcher2/e;->x:[I

    .line 66
    const/16 v0, 0x96

    iput v0, p0, Lcom/android/launcher2/e;->y:I

    .line 67
    const/16 v0, 0x18

    iput v0, p0, Lcom/android/launcher2/e;->z:I

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/e;->A:F

    .line 69
    const v0, 0x3f28f5c3

    iput v0, p0, Lcom/android/launcher2/e;->B:F

    move-object v0, p1

    .line 82
    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    .line 83
    iput-object p3, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    .line 84
    iput-object p2, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    .line 85
    iput v4, p0, Lcom/android/launcher2/e;->n:I

    .line 86
    iput-object p4, p0, Lcom/android/launcher2/e;->D:Lcom/android/launcher2/DragLayer;

    .line 87
    const v0, 0x7f0d0036

    invoke-virtual {p4, v0}, Lcom/android/launcher2/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Workspace;

    iput-object v0, p0, Lcom/android/launcher2/e;->E:Lcom/android/launcher2/Workspace;

    .line 89
    const/4 v0, 0x0

    .line 90
    instance-of v1, p2, Lcom/android/launcher2/fy;

    if-eqz v1, :cond_4a

    .line 91
    check-cast p2, Lcom/android/launcher2/fy;

    invoke-virtual {p2}, Lcom/android/launcher2/fy;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_4a

    .line 93
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    iput v1, p0, Lcom/android/launcher2/e;->n:I

    .line 97
    :cond_4a
    if-nez v0, :cond_54

    .line 98
    invoke-static {p1}, Lcom/anddoes/launcher/au;->b(Landroid/content/Context;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 99
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    iput v1, p0, Lcom/android/launcher2/e;->n:I

    .line 103
    :cond_54
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aX:Z

    if-nez v1, :cond_5e

    if-nez v0, :cond_142

    .line 104
    :cond_5e
    const/4 v1, 0x3

    iput v1, p0, Lcom/android/launcher2/e;->n:I

    .line 105
    iput v6, p0, Lcom/android/launcher2/e;->J:I

    .line 106
    iput v6, p0, Lcom/android/launcher2/e;->K:I

    .line 113
    :goto_65
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 114
    const v2, 0x7f02008b

    .line 115
    const-string v3, "widget_resize_frame_holo"

    .line 113
    invoke-virtual {v1, p0, v2, v3}, Lcom/anddoes/launcher/c/l;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 116
    invoke-virtual {p0, v4, v4, v4, v4}, Lcom/android/launcher2/e;->setPadding(IIII)V

    .line 119
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    .line 120
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    .line 121
    const v3, 0x7f02008d

    .line 122
    const-string v4, "widget_resize_handle_left"

    .line 120
    invoke-virtual {v1, v2, v3, v4}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 123
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 124
    const/16 v2, 0x13

    .line 123
    invoke-direct {v1, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 125
    iget-object v2, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/e;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    .line 128
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    .line 129
    const v3, 0x7f02008e

    .line 130
    const-string v4, "widget_resize_handle_right"

    .line 128
    invoke-virtual {v1, v2, v3, v4}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 131
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 132
    const/16 v2, 0x15

    .line 131
    invoke-direct {v1, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 133
    iget-object v2, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/e;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    .line 136
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    .line 137
    const v3, 0x7f02008f

    .line 138
    const-string v4, "widget_resize_handle_top"

    .line 136
    invoke-virtual {v1, v2, v3, v4}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 139
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 140
    const/16 v2, 0x31

    .line 139
    invoke-direct {v1, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 141
    iget-object v2, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/e;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    .line 144
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v2, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    .line 145
    const v3, 0x7f02008c

    .line 146
    const-string v4, "widget_resize_handle_bottom"

    .line 144
    invoke-virtual {v1, v2, v3, v4}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 147
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 148
    const/16 v2, 0x51

    .line 147
    invoke-direct {v1, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 149
    iget-object v2, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/e;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    const/4 v1, 0x0

    .line 151
    invoke-static {p1, v0, v1}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 153
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/android/launcher2/e;->F:I

    .line 154
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/android/launcher2/e;->H:I

    .line 155
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/android/launcher2/e;->G:I

    .line 156
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Lcom/android/launcher2/e;->I:I

    .line 158
    iget v0, p0, Lcom/android/launcher2/e;->n:I

    if-ne v0, v6, :cond_152

    .line 159
    iget-object v0, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    :cond_11d
    :goto_11d
    iget-object v0, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 167
    const/high16 v1, 0x41c0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/android/launcher2/e;->L:I

    .line 168
    iget v0, p0, Lcom/android/launcher2/e;->L:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/launcher2/e;->u:I

    .line 173
    iget-object v0, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    iget-object v1, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->d(Landroid/view/View;)V

    .line 174
    return-void

    .line 108
    :cond_142
    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v1

    .line 109
    aget v2, v1, v4

    iput v2, p0, Lcom/android/launcher2/e;->J:I

    .line 110
    aget v1, v1, v6

    iput v1, p0, Lcom/android/launcher2/e;->K:I

    goto/16 :goto_65

    .line 161
    :cond_152
    iget v0, p0, Lcom/android/launcher2/e;->n:I

    if-ne v0, v8, :cond_11d

    .line 162
    iget-object v0, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_11d
.end method


# virtual methods
.method public final a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/android/launcher2/e;->f:Z

    if-eqz v0, :cond_7c

    iget v0, p0, Lcom/android/launcher2/e;->l:I

    neg-int v0, v0

    iget-object v1, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->q:I

    iget v0, p0, Lcom/android/launcher2/e;->j:I

    iget v1, p0, Lcom/android/launcher2/e;->u:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/e;->q:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->q:I

    :cond_25
    :goto_25
    iget-boolean v0, p0, Lcom/android/launcher2/e;->h:Z

    if-eqz v0, :cond_ad

    iget v0, p0, Lcom/android/launcher2/e;->m:I

    neg-int v0, v0

    iget-object v1, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->r:I

    iget v0, p0, Lcom/android/launcher2/e;->k:I

    iget v1, p0, Lcom/android/launcher2/e;->u:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/e;->r:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->r:I

    :cond_4a
    :goto_4a
    invoke-virtual {p0}, Lcom/android/launcher2/e;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    iget-boolean v1, p0, Lcom/android/launcher2/e;->f:Z

    if-eqz v1, :cond_de

    iget v1, p0, Lcom/android/launcher2/e;->l:I

    iget v2, p0, Lcom/android/launcher2/e;->q:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    iget v1, p0, Lcom/android/launcher2/e;->j:I

    iget v2, p0, Lcom/android/launcher2/e;->q:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    :cond_62
    :goto_62
    iget-boolean v1, p0, Lcom/android/launcher2/e;->h:Z

    if-eqz v1, :cond_eb

    iget v1, p0, Lcom/android/launcher2/e;->m:I

    iget v2, p0, Lcom/android/launcher2/e;->r:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    iget v1, p0, Lcom/android/launcher2/e;->k:I

    iget v2, p0, Lcom/android/launcher2/e;->r:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    :cond_74
    :goto_74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/e;->a(Z)V

    invoke-virtual {p0}, Lcom/android/launcher2/e;->requestLayout()V

    .line 226
    return-void

    .line 225
    :cond_7c
    iget-boolean v0, p0, Lcom/android/launcher2/e;->g:Z

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/android/launcher2/e;->D:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/e;->l:I

    iget v2, p0, Lcom/android/launcher2/e;->j:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->q:I

    iget v0, p0, Lcom/android/launcher2/e;->j:I

    neg-int v0, v0

    iget v1, p0, Lcom/android/launcher2/e;->u:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/e;->q:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->q:I

    goto/16 :goto_25

    :cond_ad
    iget-boolean v0, p0, Lcom/android/launcher2/e;->i:Z

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/android/launcher2/e;->D:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/e;->m:I

    iget v2, p0, Lcom/android/launcher2/e;->k:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->r:I

    iget v0, p0, Lcom/android/launcher2/e;->k:I

    neg-int v0, v0

    iget v1, p0, Lcom/android/launcher2/e;->u:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/e;->r:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/e;->r:I

    goto/16 :goto_4a

    :cond_de
    iget-boolean v1, p0, Lcom/android/launcher2/e;->g:Z

    if-eqz v1, :cond_62

    iget v1, p0, Lcom/android/launcher2/e;->j:I

    iget v2, p0, Lcom/android/launcher2/e;->q:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    goto/16 :goto_62

    :cond_eb
    iget-boolean v1, p0, Lcom/android/launcher2/e;->i:Z

    if-eqz v1, :cond_74

    iget v1, p0, Lcom/android/launcher2/e;->k:I

    iget v2, p0, Lcom/android/launcher2/e;->r:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    goto/16 :goto_74
.end method

.method final a(Z)V
    .registers 16
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCellWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getWidthGap()I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    iget-object v1, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getCellHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getHeightGap()I

    move-result v2

    add-int/2addr v1, v2

    .line 260
    iget v2, p0, Lcom/android/launcher2/e;->q:I

    iget v3, p0, Lcom/android/launcher2/e;->s:I

    add-int/2addr v2, v3

    .line 261
    iget v3, p0, Lcom/android/launcher2/e;->r:I

    iget v4, p0, Lcom/android/launcher2/e;->t:I

    add-int/2addr v3, v4

    .line 263
    const/high16 v4, 0x3f80

    int-to-float v2, v2

    mul-float/2addr v2, v4

    int-to-float v0, v0

    div-float v0, v2, v0

    iget v2, p0, Lcom/android/launcher2/e;->o:I

    int-to-float v2, v2

    sub-float v2, v0, v2

    .line 264
    const/high16 v0, 0x3f80

    int-to-float v3, v3

    mul-float/2addr v0, v3

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/e;->p:I

    int-to-float v1, v1

    sub-float v3, v0, v1

    .line 266
    const/4 v0, 0x0

    .line 267
    const/4 v7, 0x0

    .line 268
    const/4 v4, 0x0

    .line 269
    const/4 v6, 0x0

    .line 271
    iget-object v1, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v9

    .line 272
    iget-object v1, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v10

    .line 274
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v5, 0x3f28f5c3

    cmpl-float v1, v1, v5

    if-lez v1, :cond_1a5

    .line 275
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v1, v0

    .line 277
    :goto_5b
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3f28f5c3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6a

    .line 278
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 281
    :cond_6a
    if-nez p1, :cond_71

    if-nez v1, :cond_71

    if-nez v7, :cond_71

    .line 355
    :cond_70
    :goto_70
    return-void

    .line 283
    :cond_71
    iget-object v0, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 285
    iget v11, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 286
    iget v12, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 287
    iget-boolean v0, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v0, :cond_150

    iget v0, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 288
    :goto_84
    iget-boolean v2, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v2, :cond_154

    iget v2, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    .line 290
    :goto_8a
    const/4 v3, 0x0

    .line 291
    const/4 v5, 0x0

    .line 295
    iget-boolean v13, p0, Lcom/android/launcher2/e;->f:Z

    if-eqz v13, :cond_158

    .line 296
    neg-int v3, v0

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 297
    iget v4, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v9, p0, Lcom/android/launcher2/e;->J:I

    sub-int/2addr v4, v9

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 298
    mul-int/lit8 v1, v1, -0x1

    .line 299
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 300
    iget v4, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v9, p0, Lcom/android/launcher2/e;->J:I

    sub-int/2addr v4, v9

    neg-int v4, v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 301
    neg-int v1, v4

    move v9, v1

    move v1, v3

    move v3, v4

    .line 309
    :goto_b2
    iget-boolean v4, p0, Lcom/android/launcher2/e;->h:Z

    if-eqz v4, :cond_173

    .line 310
    neg-int v4, v2

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 311
    iget v5, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    iget v6, p0, Lcom/android/launcher2/e;->K:I

    sub-int/2addr v5, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 312
    mul-int/lit8 v4, v7, -0x1

    .line 313
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 314
    iget v6, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    iget v7, p0, Lcom/android/launcher2/e;->K:I

    sub-int/2addr v6, v7

    neg-int v6, v6

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 315
    neg-int v4, v6

    move v10, v4

    move v4, v5

    move v5, v6

    .line 322
    :goto_d8
    iget-object v6, p0, Lcom/android/launcher2/e;->x:[I

    const/4 v7, 0x0

    const/4 v13, 0x0

    aput v13, v6, v7

    .line 323
    iget-object v6, p0, Lcom/android/launcher2/e;->x:[I

    const/4 v7, 0x1

    const/4 v13, 0x0

    aput v13, v6, v7

    .line 325
    iget-boolean v6, p0, Lcom/android/launcher2/e;->f:Z

    if-nez v6, :cond_ec

    iget-boolean v6, p0, Lcom/android/launcher2/e;->g:Z

    if-eqz v6, :cond_197

    .line 326
    :cond_ec
    add-int/2addr v3, v11

    .line 327
    add-int/2addr v1, v0

    .line 328
    iget-object v6, p0, Lcom/android/launcher2/e;->x:[I

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/android/launcher2/e;->f:Z

    if-eqz v0, :cond_18e

    const/4 v0, -0x1

    :goto_f6
    aput v0, v6, v7

    .line 331
    :goto_f8
    iget-boolean v0, p0, Lcom/android/launcher2/e;->h:Z

    if-nez v0, :cond_100

    iget-boolean v0, p0, Lcom/android/launcher2/e;->i:Z

    if-eqz v0, :cond_194

    .line 332
    :cond_100
    add-int/2addr v5, v12

    .line 333
    add-int/2addr v2, v4

    .line 334
    iget-object v4, p0, Lcom/android/launcher2/e;->x:[I

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/android/launcher2/e;->h:Z

    if-eqz v0, :cond_191

    const/4 v0, -0x1

    :goto_10a
    aput v0, v4, v6

    move v4, v5

    .line 337
    :goto_10d
    if-nez p1, :cond_113

    if-nez v10, :cond_113

    if-eqz v9, :cond_70

    .line 339
    :cond_113
    iget-object v0, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    iget-object v5, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    .line 340
    iget-object v6, p0, Lcom/android/launcher2/e;->x:[I

    move v7, p1

    .line 339
    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(IIIILandroid/view/View;[IZ)Z

    move-result v0

    if-eqz v0, :cond_149

    .line 341
    iput v1, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 342
    iput v2, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    .line 343
    iput v3, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 344
    iput v4, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 345
    iget v0, p0, Lcom/android/launcher2/e;->p:I

    add-int/2addr v0, v10

    iput v0, p0, Lcom/android/launcher2/e;->p:I

    .line 346
    iget v0, p0, Lcom/android/launcher2/e;->o:I

    add-int/2addr v0, v9

    iput v0, p0, Lcom/android/launcher2/e;->o:I

    .line 347
    if-nez p1, :cond_149

    iget-object v0, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    instance-of v0, v0, Landroid/appwidget/AppWidgetHostView;

    if-eqz v0, :cond_149

    .line 348
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-eqz v0, :cond_149

    .line 350
    iget-object v0, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    iget-object v1, p0, Lcom/android/launcher2/e;->M:Lcom/android/launcher2/Launcher;

    .line 349
    invoke-static {v0, v1, v3, v4}, Lcom/anddoes/launcher/r;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V

    .line 354
    :cond_149
    iget-object v0, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_70

    .line 287
    :cond_150
    iget v0, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    goto/16 :goto_84

    .line 288
    :cond_154
    iget v2, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    goto/16 :goto_8a

    .line 303
    :cond_158
    iget-boolean v13, p0, Lcom/android/launcher2/e;->g:Z

    if-eqz v13, :cond_1a0

    .line 304
    add-int v3, v0, v11

    sub-int v3, v9, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 305
    iget v3, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    iget v9, p0, Lcom/android/launcher2/e;->J:I

    sub-int/2addr v3, v9

    neg-int v3, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v9, v1

    move v3, v1

    move v1, v4

    .line 306
    goto/16 :goto_b2

    .line 316
    :cond_173
    iget-boolean v4, p0, Lcom/android/launcher2/e;->i:Z

    if-eqz v4, :cond_19b

    .line 317
    add-int v4, v2, v12

    sub-int v4, v10, v4

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 318
    iget v5, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    iget v7, p0, Lcom/android/launcher2/e;->K:I

    sub-int/2addr v5, v7

    neg-int v5, v5

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v10, v4

    move v5, v4

    move v4, v6

    .line 319
    goto/16 :goto_d8

    .line 328
    :cond_18e
    const/4 v0, 0x1

    goto/16 :goto_f6

    .line 334
    :cond_191
    const/4 v0, 0x1

    goto/16 :goto_10a

    :cond_194
    move v4, v12

    goto/16 :goto_10d

    :cond_197
    move v1, v0

    move v3, v11

    goto/16 :goto_f8

    :cond_19b
    move v10, v5

    move v4, v6

    move v5, v7

    goto/16 :goto_d8

    :cond_1a0
    move v9, v3

    move v3, v1

    move v1, v4

    goto/16 :goto_b2

    :cond_1a5
    move v1, v0

    goto/16 :goto_5b
.end method

.method public final b(Z)V
    .registers 15
    .parameter

    .prologue
    const/4 v12, 0x3

    const/high16 v11, 0x3f80

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 384
    invoke-virtual {p0}, Lcom/android/launcher2/e;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    .line 385
    iget-object v1, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher2/e;->E:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getScrollX()I

    move-result v2

    sub-int/2addr v1, v2

    .line 386
    iget-object v2, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/launcher2/e;->E:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    .line 388
    iget-object v3, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 389
    iget-object v4, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 388
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 390
    iget-object v4, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 391
    iget-object v5, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v6}, Lcom/android/launcher2/CellLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v6}, Lcom/android/launcher2/CellLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    .line 390
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 393
    iget v5, p0, Lcom/android/launcher2/e;->L:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    iget v5, p0, Lcom/android/launcher2/e;->F:I

    sub-int/2addr v3, v5

    .line 394
    iget v5, p0, Lcom/android/launcher2/e;->G:I

    .line 393
    sub-int/2addr v3, v5

    .line 395
    iget v5, p0, Lcom/android/launcher2/e;->L:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    iget v5, p0, Lcom/android/launcher2/e;->H:I

    sub-int/2addr v4, v5

    .line 396
    iget v5, p0, Lcom/android/launcher2/e;->I:I

    .line 395
    sub-int/2addr v4, v5

    .line 398
    iget-object v5, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/android/launcher2/e;->L:I

    sub-int/2addr v5, v6

    add-int/2addr v1, v5

    iget v5, p0, Lcom/android/launcher2/e;->F:I

    add-int/2addr v1, v5

    .line 399
    iget-object v5, p0, Lcom/android/launcher2/e;->C:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    iget v6, p0, Lcom/android/launcher2/e;->L:I

    sub-int/2addr v5, v6

    add-int/2addr v2, v5

    iget v5, p0, Lcom/android/launcher2/e;->H:I

    add-int/2addr v2, v5

    .line 402
    if-gez v2, :cond_dd

    .line 403
    neg-int v5, v2

    iput v5, p0, Lcom/android/launcher2/e;->v:I

    .line 407
    :goto_a5
    add-int v5, v2, v4

    iget-object v6, p0, Lcom/android/launcher2/e;->D:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v6}, Lcom/android/launcher2/DragLayer;->getHeight()I

    move-result v6

    if-le v5, v6, :cond_e0

    .line 408
    add-int v5, v2, v4

    iget-object v6, p0, Lcom/android/launcher2/e;->D:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v6}, Lcom/android/launcher2/DragLayer;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    neg-int v5, v5

    iput v5, p0, Lcom/android/launcher2/e;->w:I

    .line 413
    :goto_bb
    if-nez p1, :cond_e3

    .line 414
    iput v3, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    .line 415
    iput v4, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    .line 416
    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    .line 417
    iput v2, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    .line 418
    iget-object v0, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 419
    iget-object v0, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 420
    iget-object v0, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 421
    iget-object v0, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 422
    invoke-virtual {p0}, Lcom/android/launcher2/e;->requestLayout()V

    .line 450
    :goto_dc
    return-void

    .line 405
    :cond_dd
    iput v8, p0, Lcom/android/launcher2/e;->v:I

    goto :goto_a5

    .line 410
    :cond_e0
    iput v8, p0, Lcom/android/launcher2/e;->w:I

    goto :goto_bb

    .line 424
    :cond_e3
    const-string v5, "width"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    aput v7, v6, v8

    aput v3, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 425
    const-string v5, "height"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    aput v7, v6, v8

    aput v4, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 426
    const-string v5, "x"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    aput v7, v6, v8

    aput v1, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 427
    const-string v5, "y"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    aput v7, v6, v8

    aput v2, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 428
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v5, v8

    aput-object v4, v5, v9

    aput-object v1, v5, v10

    aput-object v2, v5, v12

    invoke-static {v0, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 429
    iget-object v1, p0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    const-string v2, "alpha"

    new-array v3, v9, [F

    aput v11, v3, v8

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 430
    iget-object v2, p0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    const-string v3, "alpha"

    new-array v4, v9, [F

    aput v11, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 431
    iget-object v3, p0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    const-string v4, "alpha"

    new-array v5, v9, [F

    aput v11, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 432
    iget-object v4, p0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    const-string v5, "alpha"

    new-array v6, v9, [F

    aput v11, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 433
    new-instance v5, Lcom/android/launcher2/g;

    invoke-direct {v5, p0}, Lcom/android/launcher2/g;-><init>(Lcom/android/launcher2/e;)V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 438
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 439
    iget v6, p0, Lcom/android/launcher2/e;->n:I

    if-ne v6, v10, :cond_180

    .line 440
    new-array v1, v12, [Landroid/animation/Animator;

    aput-object v0, v1, v8

    aput-object v3, v1, v9

    aput-object v4, v1, v10

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 447
    :goto_176
    const-wide/16 v0, 0x96

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 448
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_dc

    .line 441
    :cond_180
    iget v6, p0, Lcom/android/launcher2/e;->n:I

    if-ne v6, v9, :cond_190

    .line 442
    new-array v3, v12, [Landroid/animation/Animator;

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    aput-object v2, v3, v10

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_176

    .line 444
    :cond_190
    const/4 v6, 0x5

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object v0, v6, v8

    aput-object v1, v6, v9

    aput-object v2, v6, v10

    aput-object v3, v6, v12

    const/4 v0, 0x4

    aput-object v4, v6, v0

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_176
.end method
