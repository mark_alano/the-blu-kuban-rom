.class final Lcom/android/launcher2/eg;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/eg;->a:Lcom/android/launcher2/Launcher;

    .line 1596
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter

    .prologue
    .line 1599
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_20

    .line 1600
    const/4 v0, 0x0

    .line 1601
    iget-object v1, p0, Lcom/android/launcher2/eg;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v1}, Lcom/android/launcher2/Launcher;->e(Lcom/android/launcher2/Launcher;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_15
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_21

    .line 1613
    iget-object v0, p0, Lcom/android/launcher2/eg;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->f(Lcom/android/launcher2/Launcher;)V

    .line 1615
    :cond_20
    return-void

    .line 1601
    :cond_21
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1602
    iget-object v1, p0, Lcom/android/launcher2/eg;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v1}, Lcom/android/launcher2/Launcher;->e(Lcom/android/launcher2/Launcher;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/appwidget/AppWidgetProviderInfo;

    iget v1, v1, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1603
    mul-int/lit16 v1, v2, 0xfa

    .line 1604
    instance-of v4, v0, Landroid/widget/Advanceable;

    if-eqz v4, :cond_48

    .line 1605
    new-instance v4, Lcom/android/launcher2/eh;

    invoke-direct {v4, p0, v0}, Lcom/android/launcher2/eh;-><init>(Lcom/android/launcher2/eg;Landroid/view/View;)V

    .line 1609
    int-to-long v0, v1

    .line 1605
    invoke-virtual {p0, v4, v0, v1}, Lcom/android/launcher2/eg;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1611
    :cond_48
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_15
.end method
