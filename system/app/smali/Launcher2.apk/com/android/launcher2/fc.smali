.class final Lcom/android/launcher2/fc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Lcom/anddoes/launcher/ui/am;

.field private final synthetic c:Lcom/android/launcher2/di;

.field private final synthetic d:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Lcom/anddoes/launcher/ui/am;Lcom/android/launcher2/di;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/fc;->b:Lcom/anddoes/launcher/ui/am;

    iput-object p3, p0, Lcom/android/launcher2/fc;->c:Lcom/android/launcher2/di;

    iput-object p4, p0, Lcom/android/launcher2/fc;->d:Landroid/view/View;

    .line 5983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 5986
    iget-object v0, p0, Lcom/android/launcher2/fc;->b:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 5987
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    const-class v2, Lcom/anddoes/launcher/ui/MultiPickerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5988
    sget-object v1, Lcom/anddoes/launcher/ui/MultiPickerActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070024

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5989
    sget-object v1, Lcom/anddoes/launcher/ui/MultiPickerActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/launcher2/fc;->c:Lcom/android/launcher2/di;

    iget-wide v2, v2, Lcom/android/launcher2/di;->h:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5990
    iget-object v1, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v2, p0, Lcom/android/launcher2/fc;->c:Lcom/android/launcher2/di;

    iput-object v2, v1, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    .line 5991
    iget-object v1, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v2, p0, Lcom/android/launcher2/fc;->d:Landroid/view/View;

    iput-object v2, v1, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    .line 5992
    iget-object v1, p0, Lcom/android/launcher2/fc;->a:Lcom/android/launcher2/Launcher;

    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    .line 5993
    return-void
.end method
