.class final Lcom/android/launcher2/ff;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Landroid/content/ComponentName;

.field private final synthetic d:Lcom/anddoes/launcher/ui/am;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Landroid/content/ComponentName;Lcom/anddoes/launcher/ui/am;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ff;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/ff;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/android/launcher2/ff;->c:Landroid/content/ComponentName;

    iput-object p4, p0, Lcom/android/launcher2/ff;->d:Lcom/anddoes/launcher/ui/am;

    .line 6026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f070023

    .line 6029
    iget-object v0, p0, Lcom/android/launcher2/ff;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_5f

    .line 6030
    iget-object v0, p0, Lcom/android/launcher2/ff;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/ff;->c:Landroid/content/ComponentName;

    invoke-static {v0, v1}, Lcom/anddoes/launcher/v;->a(Landroid/content/pm/PackageManager;Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 6031
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 6032
    iget-object v0, p0, Lcom/android/launcher2/ff;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6034
    :cond_23
    iget-object v1, p0, Lcom/android/launcher2/ff;->c:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6035
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6036
    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 6037
    const-string v3, "android.intent.extra.TITLE"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6038
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6039
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6040
    const/high16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 6041
    iget-object v0, p0, Lcom/android/launcher2/ff;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/launcher2/ff;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "share"

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 6043
    :cond_5f
    iget-object v0, p0, Lcom/android/launcher2/ff;->d:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 6044
    return-void
.end method
