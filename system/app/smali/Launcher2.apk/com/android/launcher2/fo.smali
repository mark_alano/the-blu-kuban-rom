.class final Lcom/android/launcher2/fo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private b:Lcom/android/launcher2/a;


# direct methods
.method private constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 6249
    iput-object p1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/launcher2/Launcher;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6249
    invoke-direct {p0, p1}, Lcom/android/launcher2/fo;-><init>(Lcom/android/launcher2/Launcher;)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/fo;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 6249
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 6281
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->dismissDialog(I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6} :catch_7

    .line 6286
    :goto_6
    return-void

    :catch_7
    move-exception v0

    goto :goto_6
.end method


# virtual methods
.method final a()Landroid/app/Dialog;
    .registers 4

    .prologue
    .line 6256
    new-instance v0, Lcom/android/launcher2/a;

    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-direct {v0, v1}, Lcom/android/launcher2/a;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/fo;->b:Lcom/android/launcher2/a;

    .line 6258
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 6259
    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/4 v2, 0x2

    .line 6258
    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 6260
    iget-object v1, p0, Lcom/android/launcher2/fo;->b:Lcom/android/launcher2/a;

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6261
    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const v2, 0x7f070285

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 6262
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 6263
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 6264
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 6265
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 6266
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 6270
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/Launcher;Z)V

    .line 6271
    invoke-direct {p0}, Lcom/android/launcher2/fo;->b()V

    .line 6272
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 6292
    invoke-direct {p0}, Lcom/android/launcher2/fo;->b()V

    .line 6293
    iget-object v0, p0, Lcom/android/launcher2/fo;->b:Lcom/android/launcher2/a;

    invoke-virtual {v0, p2}, Lcom/android/launcher2/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/b;

    .line 6294
    iget v0, v0, Lcom/android/launcher2/b;->c:I

    packed-switch v0, :pswitch_data_c8

    .line 6349
    :goto_10
    :pswitch_10
    return-void

    .line 6296
    :pswitch_11
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6297
    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const v2, 0x7f07002c

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 6298
    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v1}, Lcom/android/launcher2/Launcher;->l(Lcom/android/launcher2/Launcher;)Lcom/anddoes/launcher/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anddoes/launcher/s;->a()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 6299
    new-instance v2, Lcom/android/launcher2/fp;

    invoke-direct {v2, p0, v1}, Lcom/android/launcher2/fp;-><init>(Lcom/android/launcher2/fo;Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6306
    new-instance v1, Lcom/android/launcher2/fq;

    invoke-direct {v1, p0}, Lcom/android/launcher2/fq;-><init>(Lcom/android/launcher2/fo;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 6311
    iget-object v1, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/app/Dialog;)V

    .line 6312
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->m(Lcom/android/launcher2/Launcher;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_10

    .line 6317
    :pswitch_51
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->n(Lcom/android/launcher2/Launcher;)V

    goto :goto_10

    .line 6321
    :pswitch_57
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->o(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/fx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/fx;->allocateAppWidgetId()I

    move-result v0

    .line 6322
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.appwidget.action.APPWIDGET_PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6323
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6327
    iget-object v2, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v2}, Lcom/anddoes/launcher/au;->b(Landroid/content/Context;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6329
    const-string v2, "customInfo"

    .line 6328
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 6330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6331
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 6332
    const-string v3, "custom_widget"

    const-string v4, "search_widget"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6333
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6335
    const-string v2, "customExtras"

    .line 6334
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 6336
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-nez v0, :cond_b1

    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->bb:Z

    if-nez v0, :cond_b1

    .line 6337
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const-string v2, "android.permission.BIND_APPWIDGET"

    invoke-static {v0, v2}, Lcom/anddoes/launcher/v;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 6338
    :cond_b1
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const-class v2, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6341
    :cond_b8
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    goto/16 :goto_10

    .line 6345
    :pswitch_c1
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->p(Lcom/android/launcher2/Launcher;)V

    goto/16 :goto_10

    .line 6294
    :pswitch_data_c8
    .packed-switch 0x0
        :pswitch_11
        :pswitch_51
        :pswitch_57
        :pswitch_10
        :pswitch_c1
    .end packed-switch
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 6275
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/Launcher;Z)V

    .line 6276
    invoke-direct {p0}, Lcom/android/launcher2/fo;->b()V

    .line 6277
    return-void
.end method

.method public final onShow(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 6352
    iget-object v0, p0, Lcom/android/launcher2/fo;->b:Lcom/android/launcher2/a;

    invoke-virtual {v0}, Lcom/android/launcher2/a;->a()V

    .line 6353
    iget-object v0, p0, Lcom/android/launcher2/fo;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/Launcher;Z)V

    .line 6354
    return-void
.end method
