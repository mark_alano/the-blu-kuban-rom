.class final Lcom/android/launcher2/fs;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field b:Ljava/util/List;

.field final synthetic c:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Launcher;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 6122
    iput-object p1, p0, Lcom/android/launcher2/fs;->c:Lcom/android/launcher2/Launcher;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 6119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    .line 6120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    .line 6123
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 6124
    if-eqz p2, :cond_1ce

    .line 6125
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aE:Z

    if-eqz v1, :cond_54

    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v1, :cond_54

    .line 6126
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->h(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/fv;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v1, v2, :cond_54

    .line 6127
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->K()Lcom/android/launcher2/as;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/as;)V

    .line 6128
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->i(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/as;

    move-result-object v1

    if-eqz v1, :cond_54

    .line 6129
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6130
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f07029f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6133
    :cond_54
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aF:Z

    if-eqz v1, :cond_84

    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v1, :cond_84

    .line 6134
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v1, :cond_84

    invoke-static {p1}, Lcom/android/launcher2/Launcher;->h(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/fv;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v1, v2, :cond_84

    .line 6135
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6136
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6138
    :cond_84
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aG:Z

    if-eqz v1, :cond_a0

    .line 6139
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6140
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f070108

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6149
    :cond_a0
    :goto_a0
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aJ:Z

    if-eqz v1, :cond_bc

    .line 6150
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6151
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6154
    :cond_bc
    if-eqz p2, :cond_db

    .line 6155
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aK:Z

    if-eqz v1, :cond_db

    .line 6156
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6157
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f070064

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6160
    :cond_db
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aL:Z

    if-eqz v1, :cond_fe

    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v1

    if-eqz v1, :cond_fe

    .line 6161
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6162
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0700b7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6165
    :cond_fe
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aH:Z

    if-eqz v1, :cond_11a

    .line 6166
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6167
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6170
    :cond_11a
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aI:Z

    if-eqz v1, :cond_136

    .line 6171
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6172
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6175
    :cond_136
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aM:Z

    if-eqz v1, :cond_153

    .line 6176
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6177
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f070040

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6180
    :cond_153
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aN:Z

    if-eqz v1, :cond_170

    .line 6181
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6182
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6185
    :cond_170
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aO:Z

    if-eqz v1, :cond_193

    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v1, :cond_193

    .line 6186
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6187
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f070041

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6190
    :cond_193
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    if-eqz v1, :cond_1b0

    .line 6191
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6192
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f070042

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6195
    :cond_1b0
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aP:Z

    if-eqz v1, :cond_1cd

    .line 6196
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6197
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f0702a5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6199
    :cond_1cd
    return-void

    .line 6143
    :cond_1ce
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->j(Lcom/android/launcher2/Launcher;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_a0

    .line 6144
    iget-object v1, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6145
    iget-object v1, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    const v2, 0x7f07001d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a0
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 6202
    iget-object v0, p0, Lcom/android/launcher2/fs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 6207
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 6212
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6218
    if-nez p2, :cond_1f

    .line 6220
    iget-object v0, p0, Lcom/android/launcher2/fs;->c:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 6221
    const v1, 0x7f030020

    const/4 v2, 0x0

    .line 6220
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 6225
    :goto_13
    iget-object v0, p0, Lcom/android/launcher2/fs;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6226
    return-object p2

    .line 6223
    :cond_1f
    check-cast p2, Landroid/widget/TextView;

    goto :goto_13
.end method
