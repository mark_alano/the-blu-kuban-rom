.class final enum Lcom/android/launcher2/fv;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/android/launcher2/fv;

.field public static final enum b:Lcom/android/launcher2/fv;

.field public static final enum c:Lcom/android/launcher2/fv;

.field public static final enum d:Lcom/android/launcher2/fv;

.field public static final enum e:Lcom/android/launcher2/fv;

.field public static final enum f:Lcom/android/launcher2/fv;

.field private static final synthetic g:[Lcom/android/launcher2/fv;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->a:Lcom/android/launcher2/fv;

    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "WORKSPACE"

    invoke-direct {v0, v1, v4}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "APPS_CUSTOMIZE"

    invoke-direct {v0, v1, v5}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "APPS_CUSTOMIZE_SPRING_LOADED"

    invoke-direct {v0, v1, v6}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "PREVIEW"

    invoke-direct {v0, v1, v7}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    new-instance v0, Lcom/android/launcher2/fv;

    const-string v1, "MANAGE_SCREENS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->a:Lcom/android/launcher2/fv;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/launcher2/fv;->g:[Lcom/android/launcher2/fv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 257
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/launcher2/fv;
    .registers 2
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/android/launcher2/fv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/fv;

    return-object v0
.end method

.method public static values()[Lcom/android/launcher2/fv;
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/android/launcher2/fv;->g:[Lcom/android/launcher2/fv;

    array-length v1, v0

    new-array v2, v1, [Lcom/android/launcher2/fv;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
