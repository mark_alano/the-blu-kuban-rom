.class final Lcom/android/launcher2/fz;
.super Lcom/android/launcher2/di;
.source "SourceFile"


# instance fields
.field a:I

.field b:Landroid/content/ComponentName;

.field c:I

.field d:I

.field e:Z

.field f:Landroid/appwidget/AppWidgetHostView;


# direct methods
.method constructor <init>(ILandroid/content/ComponentName;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0}, Lcom/android/launcher2/di;-><init>()V

    .line 40
    iput v1, p0, Lcom/android/launcher2/fz;->a:I

    .line 45
    iput v1, p0, Lcom/android/launcher2/fz;->c:I

    .line 46
    iput v1, p0, Lcom/android/launcher2/fz;->d:I

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    .line 57
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/fz;->i:I

    .line 58
    iput p1, p0, Lcom/android/launcher2/fz;->a:I

    .line 59
    iput-object p2, p0, Lcom/android/launcher2/fz;->b:Landroid/content/ComponentName;

    .line 63
    iput v1, p0, Lcom/android/launcher2/fz;->n:I

    .line 64
    iput v1, p0, Lcom/android/launcher2/fz;->o:I

    .line 65
    return-void
.end method


# virtual methods
.method final a(Landroid/content/ContentValues;)V
    .registers 4
    .parameter

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;)V

    .line 70
    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/android/launcher2/fz;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 71
    return-void
.end method

.method final a(Lcom/android/launcher2/Launcher;)V
    .registers 5
    .parameter

    .prologue
    .line 87
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 88
    iget-object v0, p0, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    iget v1, p0, Lcom/android/launcher2/fz;->n:I

    iget v2, p0, Lcom/android/launcher2/fz;->o:I

    invoke-static {v0, p1, v1, v2}, Lcom/anddoes/launcher/r;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V

    .line 90
    :cond_f
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/fz;->e:Z

    .line 91
    return-void
.end method

.method final b_()V
    .registers 2

    .prologue
    .line 100
    invoke-super {p0}, Lcom/android/launcher2/di;->b_()V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    .line 102
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppWidget(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/launcher2/fz;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
