.class final Lcom/android/launcher2/gd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic a:Landroid/content/ContentResolver;

.field private final synthetic b:Z

.field private final synthetic c:Landroid/content/ContentValues;

.field private final synthetic d:Lcom/android/launcher2/di;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;ZLandroid/content/ContentValues;Lcom/android/launcher2/di;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/gd;->a:Landroid/content/ContentResolver;

    iput-boolean p2, p0, Lcom/android/launcher2/gd;->b:Z

    iput-object p3, p0, Lcom/android/launcher2/gd;->c:Landroid/content/ContentValues;

    iput-object p4, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    .line 497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    .line 499
    iget-object v1, p0, Lcom/android/launcher2/gd;->a:Landroid/content/ContentResolver;

    iget-boolean v0, p0, Lcom/android/launcher2/gd;->b:Z

    if-eqz v0, :cond_4c

    sget-object v0, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    .line 500
    :goto_8
    iget-object v2, p0, Lcom/android/launcher2/gd;->c:Landroid/content/ContentValues;

    .line 499
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 502
    sget-object v1, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 503
    :try_start_10
    sget-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v2, v2, Lcom/android/launcher2/di;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 505
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: ItemInfo id ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v3, v3, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") passed to addItemToDatabase already exists."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 506
    iget-object v3, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    invoke-virtual {v3}, Lcom/android/launcher2/di;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 505
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_49
    .catchall {:try_start_10 .. :try_end_49} :catchall_49

    .line 502
    :catchall_49
    move-exception v0

    monitor-exit v1

    throw v0

    .line 500
    :cond_4c
    sget-object v0, Lcom/android/launcher2/hm;->b:Landroid/net/Uri;

    goto :goto_8

    .line 508
    :cond_4f
    :try_start_4f
    sget-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v2, v2, Lcom/android/launcher2/di;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->i:I

    sparse-switch v0, :sswitch_data_9e

    .line 502
    :cond_65
    :goto_65
    monitor-exit v1

    return-void

    .line 511
    :sswitch_67
    sget-object v2, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v3, v0, Lcom/android/launcher2/di;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/cu;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    :sswitch_78
    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v2, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v4, -0x64

    cmp-long v0, v2, v4

    if-eqz v0, :cond_8c

    .line 517
    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    iget-wide v2, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v4, -0x65

    cmp-long v0, v2, v4

    if-nez v0, :cond_65

    .line 518
    :cond_8c
    sget-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_65

    .line 522
    :sswitch_94
    sget-object v2, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/launcher2/gd;->d:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/fz;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9d
    .catchall {:try_start_4f .. :try_end_9d} :catchall_49

    goto :goto_65

    .line 509
    :sswitch_data_9e
    .sparse-switch
        0x0 -> :sswitch_78
        0x1 -> :sswitch_78
        0x2 -> :sswitch_67
        0x4 -> :sswitch_94
        0x3e9 -> :sswitch_78
    .end sparse-switch
.end method
