.class final Lcom/android/launcher2/gf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic a:Landroid/content/ContentResolver;

.field private final synthetic b:Lcom/android/launcher2/cu;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Lcom/android/launcher2/cu;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/gf;->a:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    .line 601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 603
    iget-object v0, p0, Lcom/android/launcher2/gf;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    iget-wide v1, v1, Lcom/android/launcher2/cu;->h:J

    invoke-static {v1, v2}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 605
    sget-object v1, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 606
    :try_start_11
    sget-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    sget-object v0, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    sget-object v0, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    sget-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 605
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_11 .. :try_end_3a} :catchall_67

    .line 612
    iget-object v0, p0, Lcom/android/launcher2/gf;->a:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/launcher2/hm;->b:Landroid/net/Uri;

    .line 613
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "container="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    iget-wide v3, v3, Lcom/android/launcher2/cu;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 612
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 615
    sget-object v1, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 616
    :try_start_57
    iget-object v0, p0, Lcom/android/launcher2/gf;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6a

    .line 615
    monitor-exit v1
    :try_end_66
    .catchall {:try_start_57 .. :try_end_66} :catchall_81

    return-void

    .line 605
    :catchall_67
    move-exception v0

    monitor-exit v1

    throw v0

    .line 616
    :cond_6a
    :try_start_6a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 617
    sget-object v3, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v4, v0, Lcom/android/launcher2/di;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    sget-object v3, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_80
    .catchall {:try_start_6a .. :try_end_80} :catchall_81

    goto :goto_5f

    .line 615
    :catchall_81
    move-exception v0

    monitor-exit v1

    throw v0
.end method
