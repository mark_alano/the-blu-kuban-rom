.class final Lcom/android/launcher2/gn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic a:Landroid/content/ContentResolver;

.field private final synthetic b:Landroid/net/Uri;

.field private final synthetic c:Landroid/content/ContentValues;

.field private final synthetic d:J

.field private final synthetic e:Lcom/android/launcher2/di;

.field private final synthetic f:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;JLcom/android/launcher2/di;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/gn;->a:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/launcher2/gn;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/launcher2/gn;->c:Landroid/content/ContentValues;

    iput-wide p4, p0, Lcom/android/launcher2/gn;->d:J

    iput-object p6, p0, Lcom/android/launcher2/gn;->e:Lcom/android/launcher2/di;

    iput-object p7, p0, Lcom/android/launcher2/gn;->f:Ljava/lang/String;

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 267
    iget-object v0, p0, Lcom/android/launcher2/gn;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/launcher2/gn;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/launcher2/gn;->c:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 270
    sget-object v2, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 271
    :try_start_d
    sget-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v3, p0, Lcom/android/launcher2/gn;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 272
    iget-object v1, p0, Lcom/android/launcher2/gn;->e:Lcom/android/launcher2/di;

    if-eq v1, v0, :cond_69

    .line 275
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "item: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/launcher2/gn;->e:Lcom/android/launcher2/di;

    if-eqz v1, :cond_63

    iget-object v1, p0, Lcom/android/launcher2/gn;->e:Lcom/android/launcher2/di;

    invoke-virtual {v1}, Lcom/android/launcher2/di;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 276
    const-string v3, "modelItem: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_66

    invoke-virtual {v0}, Lcom/android/launcher2/di;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_40
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 277
    const-string v1, "Error: ItemInfo passed to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/gn;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " doesn\'t match original"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_60
    .catchall {:try_start_d .. :try_end_60} :catchall_60

    .line 270
    :catchall_60
    move-exception v0

    monitor-exit v2

    throw v0

    .line 275
    :cond_63
    :try_start_63
    const-string v1, "null"

    goto :goto_30

    .line 276
    :cond_66
    const-string v0, "null"

    goto :goto_40

    .line 285
    :cond_69
    iget-wide v3, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x64

    cmp-long v1, v3, v5

    if-eqz v1, :cond_79

    .line 286
    iget-wide v3, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x65

    cmp-long v1, v3, v5

    if-nez v1, :cond_88

    .line 287
    :cond_79
    sget-object v1, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_86

    .line 288
    sget-object v1, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    :cond_86
    :goto_86
    monitor-exit v2

    return-void

    .line 292
    :cond_88
    sget-object v1, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_8d
    .catchall {:try_start_63 .. :try_end_8d} :catchall_60

    goto :goto_86
.end method
