.class final Lcom/android/launcher2/gp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Z

.field b:Z

.field final synthetic c:Lcom/android/launcher2/gb;

.field private d:Landroid/content/Context;

.field private e:Z

.field private f:Z

.field private g:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/android/launcher2/gb;Landroid/content/Context;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 841
    iput-object p1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 842
    iput-object p2, p0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    .line 843
    iput-boolean p3, p0, Lcom/android/launcher2/gp;->e:Z

    .line 844
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/gp;->g:Ljava/util/HashMap;

    .line 845
    return-void
.end method

.method private a(ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1414
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1415
    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_27

    .line 1424
    if-gez p1, :cond_f

    .line 1425
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1431
    :cond_f
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1432
    new-instance v0, Lcom/android/launcher2/gt;

    invoke-direct {v0, p0}, Lcom/android/launcher2/gt;-><init>(Lcom/android/launcher2/gp;)V

    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1438
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_33

    .line 1458
    return-void

    .line 1416
    :cond_27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 1417
    if-nez v0, :cond_4

    .line 1418
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 1438
    :cond_33
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 1439
    iget-wide v3, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x64

    cmp-long v3, v3, v5

    if-nez v3, :cond_56

    .line 1440
    iget v3, v0, Lcom/android/launcher2/di;->k:I

    if-ne v3, p1, :cond_52

    .line 1441
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1442
    iget-wide v3, v0, Lcom/android/launcher2/di;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 1444
    :cond_52
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 1446
    :cond_56
    iget-wide v3, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x65

    cmp-long v3, v3, v5

    if-nez v3, :cond_6b

    .line 1447
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1448
    iget-wide v3, v0, Lcom/android/launcher2/di;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 1450
    :cond_6b
    iget-wide v3, v0, Lcom/android/launcher2/di;->j:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_84

    .line 1451
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1452
    iget-wide v3, v0, Lcom/android/launcher2/di;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 1454
    :cond_84
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_20
.end method

.method private static a(ILjava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1490
    if-gez p0, :cond_5

    .line 1491
    invoke-virtual {p3, p2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1494
    :cond_5
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_14

    .line 1505
    return-void

    .line 1494
    :cond_14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 1495
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 1496
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/cu;

    .line 1497
    if-eqz v0, :cond_d

    if-eqz v1, :cond_d

    .line 1498
    iget-wide v5, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v7, -0x64

    cmp-long v5, v5, v7

    if-nez v5, :cond_4a

    .line 1499
    iget v0, v0, Lcom/android/launcher2/di;->k:I

    if-ne v0, p0, :cond_4a

    .line 1500
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 1502
    :cond_4a
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d
.end method

.method private a(Lcom/android/launcher2/go;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 1533
    if-eqz p5, :cond_23

    const/4 v0, 0x1

    move v6, v0

    .line 1536
    :goto_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v7

    .line 1537
    :goto_a
    if-lt v4, v8, :cond_25

    .line 1557
    invoke-virtual {p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1558
    new-instance v0, Lcom/android/launcher2/gw;

    invoke-direct {v0, p0, p1, p4}, Lcom/android/launcher2/gw;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;Ljava/util/HashMap;)V

    .line 1566
    if-eqz v6, :cond_43

    .line 1567
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1574
    :cond_1c
    :goto_1c
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1575
    :goto_20
    if-lt v7, v1, :cond_49

    .line 1591
    return-void

    :cond_23
    move v6, v7

    .line 1533
    goto :goto_5

    .line 1539
    :cond_25
    add-int/lit8 v0, v4, 0x6

    if-gt v0, v8, :cond_3a

    const/4 v5, 0x6

    .line 1540
    :goto_2a
    new-instance v0, Lcom/android/launcher2/gv;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/gv;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;Ljava/util/ArrayList;II)V

    .line 1549
    if-eqz v6, :cond_3d

    .line 1550
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1537
    :goto_37
    add-int/lit8 v4, v4, 0x6

    goto :goto_a

    .line 1539
    :cond_3a
    sub-int v5, v8, v4

    goto :goto_2a

    .line 1552
    :cond_3d
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    goto :goto_37

    .line 1569
    :cond_43
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    goto :goto_1c

    .line 1576
    :cond_49
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/fz;

    .line 1577
    new-instance v2, Lcom/android/launcher2/gx;

    invoke-direct {v2, p0, p1, v0}, Lcom/android/launcher2/gx;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;Lcom/android/launcher2/fz;)V

    .line 1585
    if-eqz v6, :cond_5c

    .line 1586
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1575
    :goto_59
    add-int/lit8 v7, v7, 0x1

    goto :goto_20

    .line 1588
    :cond_5c
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0, v2}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    goto :goto_59
.end method

.method private a(Ljava/util/ArrayList;)V
    .registers 3
    .parameter

    .prologue
    .line 1511
    new-instance v0, Lcom/android/launcher2/gu;

    invoke-direct {v0, p0}, Lcom/android/launcher2/gu;-><init>(Lcom/android/launcher2/gp;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1525
    return-void
.end method

.method private a([[[Lcom/android/launcher2/di;Lcom/android/launcher2/di;)Z
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1064
    iget v4, p2, Lcom/android/launcher2/di;->k:I

    .line 1065
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    .line 1066
    iget-wide v5, p2, Lcom/android/launcher2/di;->j:J

    const-wide/16 v7, -0x65

    cmp-long v3, v5, v7

    if-nez v3, :cond_a6

    .line 1068
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    iget v3, p2, Lcom/android/launcher2/di;->k:I

    invoke-interface {v0}, Lcom/android/launcher2/go;->m()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1111
    :cond_36
    :goto_36
    return v1

    .line 1074
    :cond_37
    iget v0, p2, Lcom/android/launcher2/di;->k:I

    const/16 v3, 0x44c

    if-lt v0, v3, :cond_93

    move v0, v1

    .line 1075
    :goto_3e
    iget v3, p2, Lcom/android/launcher2/di;->k:I

    rem-int/lit8 v3, v3, 0x64

    .line 1076
    aget-object v4, p1, v9

    aget-object v4, v4, v0

    aget-object v4, v4, v3

    if-eqz v4, :cond_9e

    .line 1077
    const-string v2, "Launcher.Model"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error loading shortcut into hotseat "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1078
    const-string v5, " into position ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Lcom/android/launcher2/di;->k:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Lcom/android/launcher2/di;->l:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Lcom/android/launcher2/di;->m:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1079
    const-string v5, ")  occupied by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v9

    aget-object v0, v5, v0

    aget-object v0, v0, v3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1077
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_36

    .line 1074
    :cond_93
    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_99

    move v0, v2

    goto :goto_3e

    :cond_99
    add-int/lit16 v0, v0, 0xc8

    div-int/lit8 v0, v0, 0x64

    goto :goto_3e

    .line 1082
    :cond_9e
    aget-object v1, p1, v9

    aget-object v0, v1, v0

    aput-object p2, v0, v3

    move v1, v2

    .line 1083
    goto :goto_36

    .line 1085
    :cond_a6
    iget-wide v5, p2, Lcom/android/launcher2/di;->j:J

    const-wide/16 v7, -0x64

    cmp-long v3, v5, v7

    if-eqz v3, :cond_b0

    move v1, v2

    .line 1087
    goto :goto_36

    .line 1091
    :cond_b0
    if-eqz v0, :cond_c1

    invoke-interface {v0}, Lcom/android/launcher2/go;->M()Z

    move-result v0

    if-nez v0, :cond_c1

    .line 1092
    iget v0, p2, Lcom/android/launcher2/di;->l:I

    :goto_ba
    iget v3, p2, Lcom/android/launcher2/di;->l:I

    iget v5, p2, Lcom/android/launcher2/di;->n:I

    add-int/2addr v3, v5

    if-lt v0, v3, :cond_cd

    .line 1105
    :cond_c1
    iget v0, p2, Lcom/android/launcher2/di;->l:I

    :goto_c3
    iget v1, p2, Lcom/android/launcher2/di;->l:I

    iget v3, p2, Lcom/android/launcher2/di;->n:I

    add-int/2addr v1, v3

    if-lt v0, v1, :cond_134

    move v1, v2

    .line 1111
    goto/16 :goto_36

    .line 1093
    :cond_cd
    iget v3, p2, Lcom/android/launcher2/di;->m:I

    :goto_cf
    iget v5, p2, Lcom/android/launcher2/di;->m:I

    iget v6, p2, Lcom/android/launcher2/di;->o:I

    add-int/2addr v5, v6

    if-lt v3, v5, :cond_d9

    .line 1092
    add-int/lit8 v0, v0, 0x1

    goto :goto_ba

    .line 1094
    :cond_d9
    aget-object v5, p1, v4

    aget-object v5, v5, v0

    aget-object v5, v5, v3

    if-eqz v5, :cond_131

    .line 1095
    const-string v2, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error loading shortcut "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1096
    const-string v6, " into cell ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/android/launcher2/di;->k:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1097
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1098
    const-string v6, ") occupied by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1099
    aget-object v4, p1, v4

    aget-object v0, v4, v0

    aget-object v0, v0, v3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1095
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_36

    .line 1093
    :cond_131
    add-int/lit8 v3, v3, 0x1

    goto :goto_cf

    .line 1106
    :cond_134
    iget v1, p2, Lcom/android/launcher2/di;->m:I

    :goto_136
    iget v3, p2, Lcom/android/launcher2/di;->m:I

    iget v5, p2, Lcom/android/launcher2/di;->o:I

    add-int/2addr v3, v5

    if-lt v1, v3, :cond_140

    .line 1105
    add-int/lit8 v0, v0, 0x1

    goto :goto_c3

    .line 1107
    :cond_140
    aget-object v3, p1, v4

    aget-object v3, v3, v0

    aput-object p2, v3, v1

    .line 1106
    add-int/lit8 v1, v1, 0x1

    goto :goto_136
.end method

.method private b(I)V
    .registers 19
    .parameter

    .prologue
    .line 1597
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 1602
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v2}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/go;

    .line 1603
    if-nez v3, :cond_1c

    .line 1605
    const-string v2, "Launcher.Model"

    const-string v3, "LoaderTask running with no launcher"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    :goto_1b
    return-void

    .line 1609
    :cond_1c
    if-ltz p1, :cond_d4

    const/4 v2, 0x1

    move v8, v2

    .line 1610
    :goto_20
    if-eqz v8, :cond_d8

    move/from16 v2, p1

    .line 1615
    :goto_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v4}, Lcom/android/launcher2/gb;->k(Lcom/android/launcher2/gb;)V

    .line 1616
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1618
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1619
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 1620
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 1621
    sget-object v4, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v4

    .line 1622
    :try_start_42
    sget-object v5, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1623
    sget-object v5, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1624
    sget-object v5, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    invoke-virtual {v12, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1625
    sget-object v5, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    invoke-virtual {v13, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1621
    monitor-exit v4
    :try_end_57
    .catchall {:try_start_42 .. :try_end_57} :catchall_de

    .line 1628
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1629
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1631
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1633
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1634
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1635
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 1638
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7, v4, v14}, Lcom/android/launcher2/gp;->a(ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1640
    invoke-static {v2, v11, v5, v15}, Lcom/android/launcher2/gp;->b(ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1642
    move-object/from16 v0, v16

    invoke-static {v2, v13, v12, v6, v0}, Lcom/android/launcher2/gp;->a(ILjava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 1644
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/launcher2/gp;->a(Ljava/util/ArrayList;)V

    .line 1645
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/launcher2/gp;->a(Ljava/util/ArrayList;)V

    .line 1648
    new-instance v2, Lcom/android/launcher2/gy;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/launcher2/gy;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;)V

    .line 1656
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v7, v2}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    .line 1660
    const/4 v7, 0x0

    move-object/from16 v2, p0

    .line 1659
    invoke-direct/range {v2 .. v7}, Lcom/android/launcher2/gp;->a(Lcom/android/launcher2/go;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 1661
    if-eqz v8, :cond_b2

    .line 1662
    new-instance v2, Lcom/android/launcher2/gz;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v2, v0, v3, v1}, Lcom/android/launcher2/gz;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;I)V

    .line 1670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v4, v2}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    .line 1675
    :cond_b2
    sget-object v2, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1677
    if-eqz v8, :cond_e1

    sget-object v7, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    :goto_bb
    move-object/from16 v2, p0

    move-object v4, v14

    move-object v5, v15

    move-object/from16 v6, v16

    .line 1676
    invoke-direct/range {v2 .. v7}, Lcom/android/launcher2/gp;->a(Lcom/android/launcher2/go;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 1680
    new-instance v2, Lcom/android/launcher2/ha;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v9, v10}, Lcom/android/launcher2/ha;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;J)V

    .line 1696
    if-eqz v8, :cond_e3

    .line 1697
    sget-object v3, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1b

    .line 1609
    :cond_d4
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_20

    .line 1611
    :cond_d8
    invoke-interface {v3}, Lcom/android/launcher2/go;->s()I

    move-result v2

    goto/16 :goto_24

    .line 1621
    :catchall_de
    move-exception v2

    monitor-exit v4

    throw v2

    .line 1677
    :cond_e1
    const/4 v7, 0x0

    goto :goto_bb

    .line 1699
    :cond_e3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v3, v2}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V

    goto/16 :goto_1b
.end method

.method private static b(ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1467
    if-gez p0, :cond_5

    .line 1468
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1471
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1480
    return-void

    .line 1471
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/fz;

    .line 1472
    if-eqz v0, :cond_9

    .line 1473
    iget-wide v2, v0, Lcom/android/launcher2/fz;->j:J

    const-wide/16 v4, -0x64

    cmp-long v2, v2, v4

    if-nez v2, :cond_28

    .line 1474
    iget v2, v0, Lcom/android/launcher2/fz;->k:I

    if-ne v2, p0, :cond_28

    .line 1475
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1477
    :cond_28
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9
.end method

.method private e()V
    .registers 35

    .prologue
    .line 856
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/launcher2/gp;->a:Z

    .line 863
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v2}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;)Z

    move-result v2

    if-nez v2, :cond_3cf

    .line 864
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    invoke-static {v12}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v20

    invoke-virtual/range {v19 .. v19}, Landroid/content/pm/PackageManager;->isSafeMode()Z

    move-result v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v3}, Lcom/android/launcher2/gb;->j(Lcom/android/launcher2/gb;)Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherApplication;->c()Lcom/android/launcher2/LauncherProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherProvider;->b()V

    sget-object v22, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v22

    :try_start_35
    sget-object v3, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    sget-object v3, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    sget-object v3, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    sget-object v3, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    sget-object v3, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    sget-object v3, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const/16 v3, 0xa

    const/16 v4, 0xd

    const/16 v5, 0xd

    filled-new-array {v3, v4, v5}, [I

    move-result-object v3

    const-class v4, Lcom/android/launcher2/di;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, [[[Lcom/android/launcher2/di;

    move-object/from16 v18, v0
    :try_end_72
    .catchall {:try_start_35 .. :try_end_72} :catchall_18a

    :try_start_72
    const-string v3, "_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    const-string v3, "intent"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    const-string v3, "title"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v3, "iconType"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    const-string v3, "icon"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    const-string v3, "iconPackage"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    const-string v3, "iconResource"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    const-string v3, "container"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    const-string v3, "itemType"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    const-string v3, "appWidgetId"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    const-string v3, "screen"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    const-string v3, "cellX"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v30

    const-string v3, "cellY"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v31

    const-string v3, "spanX"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v32

    const-string v3, "spanY"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v33

    :cond_cc
    :goto_cc
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/launcher2/gp;->f:Z

    if-nez v3, :cond_d8

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_d5
    .catchall {:try_start_72 .. :try_end_d5} :catchall_185

    move-result v3

    if-nez v3, :cond_fb

    :cond_d8
    :try_start_d8
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_f1

    sget-object v3, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_eb
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_39b

    :cond_f1
    monitor-exit v22
    :try_end_f2
    .catchall {:try_start_d8 .. :try_end_f2} :catchall_18a

    .line 865
    monitor-enter p0

    .line 866
    :try_start_f3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/gp;->f:Z

    if-eqz v2, :cond_3c7

    .line 867
    monitor-exit p0
    :try_end_fa
    .catchall {:try_start_f3 .. :try_end_fa} :catchall_3d7

    .line 875
    :goto_fa
    return-void

    .line 864
    :cond_fb
    :try_start_fb
    move/from16 v0, v27

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_3de

    goto :goto_cc

    :sswitch_105
    move/from16 v0, v25

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_10a
    .catchall {:try_start_fb .. :try_end_10a} :catchall_185
    .catch Ljava/lang/Exception; {:try_start_fb .. :try_end_10a} :catch_17b

    move-result-object v4

    const/4 v5, 0x0

    :try_start_10c
    invoke-static {v4, v5}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_10f
    .catchall {:try_start_10c .. :try_end_10f} :catchall_185
    .catch Ljava/net/URISyntaxException; {:try_start_10c .. :try_end_10f} :catch_3da
    .catch Ljava/lang/Exception; {:try_start_10c .. :try_end_10f} :catch_17b

    move-result-object v5

    if-nez v3, :cond_18d

    :try_start_112
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/gp;->g:Ljava/util/HashMap;

    move-object/from16 v4, v19

    move-object v6, v12

    invoke-virtual/range {v3 .. v10}, Lcom/android/launcher2/gb;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/android/launcher2/jd;

    move-result-object v3

    :cond_121
    :goto_121
    if-eqz v3, :cond_1cb

    iput-object v5, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/launcher2/jd;->h:J

    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v5, v4

    iput-wide v5, v3, Lcom/android/launcher2/jd;->j:J

    move/from16 v0, v29

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v3, Lcom/android/launcher2/jd;->k:I

    move/from16 v0, v30

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v3, Lcom/android/launcher2/jd;->l:I

    move/from16 v0, v31

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v3, Lcom/android/launcher2/jd;->m:I

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v3}, Lcom/android/launcher2/gp;->a([[[Lcom/android/launcher2/di;Lcom/android/launcher2/di;)Z

    move-result v5

    if-eqz v5, :cond_cc

    packed-switch v4, :pswitch_data_3f4

    sget-object v5, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    int-to-long v10, v4

    invoke-static {v5, v10, v11}, Lcom/android/launcher2/gb;->a(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/di;)V

    :goto_165
    sget-object v4, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v5, v3, Lcom/android/launcher2/jd;->h:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    sget-object v5, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v5, v3, v7, v8}, Lcom/android/launcher2/gb;->a(Ljava/util/HashMap;Lcom/android/launcher2/jd;Landroid/database/Cursor;I)Z
    :try_end_179
    .catchall {:try_start_112 .. :try_end_179} :catchall_185
    .catch Ljava/lang/Exception; {:try_start_112 .. :try_end_179} :catch_17b

    goto/16 :goto_cc

    :catch_17b
    move-exception v3

    :try_start_17c
    const-string v4, "Launcher.Model"

    const-string v5, "Desktop items loading interrupted:"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_183
    .catchall {:try_start_17c .. :try_end_183} :catchall_185

    goto/16 :goto_cc

    :catchall_185
    move-exception v2

    :try_start_186
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_18a
    .catchall {:try_start_186 .. :try_end_18a} :catchall_18a

    :catchall_18a
    move-exception v2

    monitor-exit v22

    throw v2

    :cond_18d
    :try_start_18d
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    move-object v11, v7

    move/from16 v16, v8

    move/from16 v17, v9

    invoke-static/range {v10 .. v17}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/android/launcher2/jd;

    move-result-object v3

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_121

    invoke-virtual {v5}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v4

    if-eqz v4, :cond_121

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_121

    invoke-virtual {v5}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v4

    const-string v6, "android.intent.category.LAUNCHER"

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_121

    const/high16 v4, 0x1020

    invoke-virtual {v5, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_121

    :pswitch_1c5
    sget-object v4, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_165

    :cond_1cb
    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string v5, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v10, "Error loading shortcut "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ", removing it"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v3, v4}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_cc

    :sswitch_1f6
    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    sget-object v5, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    invoke-static {v5, v3, v4}, Lcom/android/launcher2/gb;->a(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;

    move-result-object v5

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    iput-wide v3, v5, Lcom/android/launcher2/cu;->h:J

    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v10, v3

    iput-wide v10, v5, Lcom/android/launcher2/cu;->j:J

    move/from16 v0, v29

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v5, Lcom/android/launcher2/cu;->k:I

    move/from16 v0, v30

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v5, Lcom/android/launcher2/cu;->l:I

    move/from16 v0, v31

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v5, Lcom/android/launcher2/cu;->m:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v7, v8, v12}, Lcom/android/launcher2/gb;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v5, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v5}, Lcom/android/launcher2/gp;->a([[[Lcom/android/launcher2/di;Lcom/android/launcher2/di;)Z

    move-result v4

    if-eqz v4, :cond_cc

    packed-switch v3, :pswitch_data_3fc

    :goto_242
    sget-object v3, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v10, v5, Lcom/android/launcher2/cu;->h:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    iget-wide v10, v5, Lcom/android/launcher2/cu;->h:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_cc

    :pswitch_25a
    sget-object v3, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_242

    :sswitch_260
    move/from16 v0, v28

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v6

    if-nez v21, :cond_2b0

    if-eqz v6, :cond_282

    iget-object v10, v6, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v10, :cond_282

    iget-object v10, v6, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_2b0

    :cond_282
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v10, "Deleting widget that isn\'t installed anymore: id="

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " appWidgetId="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "Launcher.Model"

    invoke-static {v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/launcher2/Launcher;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_cc

    :cond_2b0
    new-instance v10, Lcom/android/launcher2/fz;

    iget-object v11, v6, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-direct {v10, v3, v11}, Lcom/android/launcher2/fz;-><init>(ILandroid/content/ComponentName;)V

    iput-wide v4, v10, Lcom/android/launcher2/fz;->h:J

    move/from16 v0, v29

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v10, Lcom/android/launcher2/fz;->k:I

    move/from16 v0, v30

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v10, Lcom/android/launcher2/fz;->l:I

    move/from16 v0, v31

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v10, Lcom/android/launcher2/fz;->m:I

    move/from16 v0, v32

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v10, Lcom/android/launcher2/fz;->n:I

    move/from16 v0, v33

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v10, Lcom/android/launcher2/fz;->o:I

    invoke-static {v12, v6}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v3

    const/4 v4, 0x0

    aget v4, v3, v4

    iput v4, v10, Lcom/android/launcher2/fz;->p:I

    const/4 v4, 0x1

    aget v3, v3, v4

    iput v3, v10, Lcom/android/launcher2/fz;->q:I

    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, -0x64

    if-eq v3, v4, :cond_306

    const/16 v4, -0x65

    if-eq v3, v4, :cond_306

    const-string v3, "Launcher.Model"

    const-string v4, "Widget found where container != CONTAINER_DESKTOP nor CONTAINER_HOTSEAT - ignoring!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_cc

    :cond_306
    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, v10, Lcom/android/launcher2/fz;->j:J

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v10}, Lcom/android/launcher2/gp;->a([[[Lcom/android/launcher2/di;Lcom/android/launcher2/di;)Z

    move-result v3

    if-eqz v3, :cond_cc

    sget-object v3, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v4, v10, Lcom/android/launcher2/fz;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_cc

    :sswitch_32b
    invoke-static {v12}, Lcom/anddoes/launcher/au;->a(Landroid/content/Context;)Lcom/anddoes/launcher/au;

    move-result-object v3

    move/from16 v0, v24

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/anddoes/launcher/i;->h:J

    move/from16 v0, v29

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/anddoes/launcher/i;->k:I

    move/from16 v0, v30

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/anddoes/launcher/i;->l:I

    move/from16 v0, v31

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/anddoes/launcher/i;->m:I

    move/from16 v0, v32

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/anddoes/launcher/i;->n:I

    move/from16 v0, v33

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/anddoes/launcher/i;->o:I

    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, -0x64

    if-eq v4, v5, :cond_376

    const/16 v5, -0x65

    if-eq v4, v5, :cond_376

    const-string v3, "Launcher.Model"

    const-string v4, "Widget found where container != CONTAINER_DESKTOP nor CONTAINER_HOTSEAT - ignoring!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_cc

    :cond_376
    move/from16 v0, v26

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, v3, Lcom/anddoes/launcher/i;->j:J

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v3}, Lcom/android/launcher2/gp;->a([[[Lcom/android/launcher2/di;Lcom/android/launcher2/di;)Z

    move-result v4

    if-eqz v4, :cond_cc

    sget-object v4, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-wide v5, v3, Lcom/anddoes/launcher/i;->h:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_399
    .catchall {:try_start_18d .. :try_end_399} :catchall_185
    .catch Ljava/lang/Exception; {:try_start_18d .. :try_end_399} :catch_17b

    goto/16 :goto_cc

    :cond_39b
    :try_start_39b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_3a4
    .catchall {:try_start_39b .. :try_end_3a4} :catchall_18a

    move-result-wide v5

    :try_start_3a5
    invoke-static {v5, v6}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v2, v7, v8}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3ae
    .catchall {:try_start_3a5 .. :try_end_3ae} :catchall_18a
    .catch Landroid/os/RemoteException; {:try_start_3a5 .. :try_end_3ae} :catch_3b0

    goto/16 :goto_eb

    :catch_3b0
    move-exception v2

    :try_start_3b1
    const-string v2, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Could not remove id = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c5
    .catchall {:try_start_3b1 .. :try_end_3c5} :catchall_18a

    goto/16 :goto_eb

    .line 869
    :cond_3c7
    :try_start_3c7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v2}, Lcom/android/launcher2/gb;->b(Lcom/android/launcher2/gb;)V

    .line 865
    monitor-exit p0
    :try_end_3cf
    .catchall {:try_start_3c7 .. :try_end_3cf} :catchall_3d7

    .line 874
    :cond_3cf
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher2/gp;->b(I)V

    goto/16 :goto_fa

    .line 865
    :catchall_3d7
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_3da
    move-exception v3

    goto/16 :goto_cc

    .line 864
    nop

    :sswitch_data_3de
    .sparse-switch
        0x0 -> :sswitch_105
        0x1 -> :sswitch_105
        0x2 -> :sswitch_1f6
        0x4 -> :sswitch_260
        0x3e9 -> :sswitch_32b
    .end sparse-switch

    :pswitch_data_3f4
    .packed-switch -0x65
        :pswitch_1c5
        :pswitch_1c5
    .end packed-switch

    :pswitch_data_3fc
    .packed-switch -0x65
        :pswitch_25a
        :pswitch_25a
    .end packed-switch
.end method

.method private f()V
    .registers 15

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1707
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->d(Lcom/android/launcher2/gb;)Z

    move-result v0

    if-nez v0, :cond_10d

    .line 1708
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    if-nez v0, :cond_26

    const-string v0, "Launcher.Model"

    const-string v1, "LoaderTask running with no launcher (loadAllAppsByBatch)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1709
    :cond_1f
    monitor-enter p0

    .line 1710
    :try_start_20
    iget-boolean v0, p0, Lcom/android/launcher2/gp;->f:Z

    if-eqz v0, :cond_102

    .line 1711
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_20 .. :try_end_25} :catchall_10a

    .line 1718
    :goto_25
    return-void

    .line 1708
    :cond_26
    new-instance v8, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v8, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const v3, 0x7fffffff

    const/4 v2, -0x1

    move v1, v2

    move v6, v5

    move v2, v3

    move-object v3, v4

    :goto_40
    if-ge v6, v2, :cond_1f

    iget-boolean v4, p0, Lcom/android/launcher2/gp;->f:Z

    if-nez v4, :cond_1f

    if-nez v6, :cond_d4

    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;

    move-result-object v1

    iget-object v2, v1, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, v1, Lcom/android/launcher2/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, v1, Lcom/android/launcher2/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v1, v1, Lcom/android/launcher2/d;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v9, v8, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1f

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->n(Lcom/android/launcher2/gb;)I

    move-result v1

    if-nez v1, :cond_cc

    move v1, v2

    :goto_77
    :try_start_77
    new-instance v4, Lcom/android/launcher2/hg;

    iget-object v7, p0, Lcom/android/launcher2/gp;->g:Ljava/util/HashMap;

    invoke-direct {v4, v9, v7}, Lcom/android/launcher2/hg;-><init>(Landroid/content/pm/PackageManager;Ljava/util/HashMap;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_81
    .catch Ljava/lang/Exception; {:try_start_77 .. :try_end_81} :catch_d3

    move-object v4, v3

    move v3, v2

    move v2, v1

    :goto_84
    move v7, v5

    :goto_85
    if-ge v6, v3, :cond_89

    if-lt v7, v2, :cond_d8

    :cond_89
    if-gt v6, v2, :cond_fa

    const/4 v1, 0x1

    :goto_8c
    invoke-virtual {p0, v0}, Lcom/android/launcher2/gp;->a(Lcom/android/launcher2/go;)Lcom/android/launcher2/go;

    move-result-object v7

    iget-object v10, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v10}, Lcom/android/launcher2/gb;->m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;

    move-result-object v10

    iget-object v10, v10, Lcom/android/launcher2/d;->b:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v11}, Lcom/android/launcher2/gb;->m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, v11, Lcom/android/launcher2/d;->b:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v11}, Lcom/android/launcher2/gb;->c(Lcom/android/launcher2/gb;)Lcom/android/launcher2/az;

    move-result-object v11

    new-instance v12, Lcom/android/launcher2/gs;

    invoke-direct {v12, p0, v7, v1, v10}, Lcom/android/launcher2/gs;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;ZLjava/util/ArrayList;)V

    invoke-virtual {v11, v12}, Lcom/android/launcher2/az;->a(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->p(Lcom/android/launcher2/gb;)I

    move-result v1

    if-lez v1, :cond_fd

    if-ge v6, v3, :cond_fd

    :try_start_bd
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->p(Lcom/android/launcher2/gb;)I

    move-result v1

    int-to-long v10, v1

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_c7
    .catch Ljava/lang/InterruptedException; {:try_start_bd .. :try_end_c7} :catch_fc

    move v1, v2

    move v2, v3

    move-object v3, v4

    goto/16 :goto_40

    :cond_cc
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->n(Lcom/android/launcher2/gb;)I

    move-result v1

    goto :goto_77

    :catch_d3
    move-exception v4

    :cond_d4
    move-object v4, v3

    move v3, v2

    move v2, v1

    goto :goto_84

    :cond_d8
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;

    move-result-object v10

    new-instance v11, Lcom/android/launcher2/h;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v12, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v12}, Lcom/android/launcher2/gb;->o(Lcom/android/launcher2/gb;)Lcom/android/launcher2/da;

    move-result-object v12

    iget-object v13, p0, Lcom/android/launcher2/gp;->g:Ljava/util/HashMap;

    invoke-direct {v11, v9, v1, v12, v13}, Lcom/android/launcher2/h;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/android/launcher2/da;Ljava/util/HashMap;)V

    invoke-virtual {v10, v11}, Lcom/android/launcher2/d;->a(Lcom/android/launcher2/h;)V

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_85

    :cond_fa
    move v1, v5

    goto :goto_8c

    :catch_fc
    move-exception v1

    :cond_fd
    move v1, v2

    move v2, v3

    move-object v3, v4

    goto/16 :goto_40

    .line 1713
    :cond_102
    :try_start_102
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->l(Lcom/android/launcher2/gb;)V

    .line 1709
    monitor-exit p0
    :try_end_108
    .catchall {:try_start_102 .. :try_end_108} :catchall_10a

    goto/16 :goto_25

    :catchall_10a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1716
    :cond_10d
    invoke-direct {p0}, Lcom/android/launcher2/gp;->g()V

    goto/16 :goto_25
.end method

.method private g()V
    .registers 5

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    .line 1722
    if-nez v0, :cond_16

    .line 1724
    const-string v0, "Launcher.Model"

    const-string v1, "LoaderTask running with no launcher (onlyBindAllApps)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    :goto_15
    return-void

    .line 1731
    :cond_16
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;

    move-result-object v1

    iget-object v1, v1, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1732
    new-instance v2, Lcom/android/launcher2/gr;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/launcher2/gr;-><init>(Lcom/android/launcher2/gp;Lcom/android/launcher2/go;Ljava/util/ArrayList;)V

    .line 1745
    invoke-static {}, Lcom/android/launcher2/gb;->l()Landroid/os/HandlerThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v1

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    if-ne v1, v3, :cond_44

    const/4 v1, 0x0

    .line 1746
    :goto_38
    invoke-interface {v0}, Lcom/android/launcher2/go;->l()Z

    move-result v0

    if-eqz v0, :cond_46

    if-eqz v1, :cond_46

    .line 1747
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_15

    .line 1745
    :cond_44
    const/4 v1, 0x1

    goto :goto_38

    .line 1749
    :cond_46
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->c(Lcom/android/launcher2/gb;)Lcom/android/launcher2/az;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/az;->a(Ljava/lang/Runnable;)V

    goto :goto_15
.end method


# virtual methods
.method final a(Lcom/android/launcher2/go;)Lcom/android/launcher2/go;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1040
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1041
    :try_start_8
    iget-boolean v0, p0, Lcom/android/launcher2/gp;->f:Z

    if-eqz v0, :cond_f

    .line 1042
    monitor-exit v2

    move-object v0, v1

    .line 1058
    :goto_e
    return-object v0

    .line 1045
    :cond_f
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-nez v0, :cond_1a

    .line 1046
    monitor-exit v2

    move-object v0, v1

    goto :goto_e

    .line 1049
    :cond_1a
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    .line 1050
    if-eq v0, p1, :cond_2b

    .line 1051
    monitor-exit v2

    move-object v0, v1

    goto :goto_e

    .line 1053
    :cond_2b
    if-nez v0, :cond_37

    .line 1054
    const-string v0, "Launcher.Model"

    const-string v3, "no mCallbacks"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    monitor-exit v2
    :try_end_35
    .catchall {:try_start_8 .. :try_end_35} :catchall_39

    move-object v0, v1

    goto :goto_e

    .line 1058
    :cond_37
    monitor-exit v2

    goto :goto_e

    .line 1040
    :catchall_39
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 912
    if-gez p1, :cond_a

    .line 914
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should not call runBindSynchronousPage() without valid page index"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 917
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->d(Lcom/android/launcher2/gb;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 920
    :cond_1a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expecting AllApps and Workspace to be loaded"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 922
    :cond_22
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 923
    :try_start_29
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->f(Lcom/android/launcher2/gb;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 926
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Error! Background loading is already running"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_39
    .catchall {:try_start_29 .. :try_end_39} :catchall_39

    .line 922
    :catchall_39
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3c
    :try_start_3c
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_39

    .line 937
    iget-object v0, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->c(Lcom/android/launcher2/gb;)Lcom/android/launcher2/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/az;->a()V

    .line 941
    invoke-direct {p0, p1}, Lcom/android/launcher2/gp;->b(I)V

    .line 944
    invoke-direct {p0}, Lcom/android/launcher2/gp;->g()V

    .line 945
    return-void
.end method

.method final a()Z
    .registers 2

    .prologue
    .line 848
    iget-boolean v0, p0, Lcom/android/launcher2/gp;->e:Z

    return v0
.end method

.method final b()Z
    .registers 2

    .prologue
    .line 852
    iget-boolean v0, p0, Lcom/android/launcher2/gp;->a:Z

    return v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 1026
    monitor-enter p0

    .line 1027
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/android/launcher2/gp;->f:Z

    .line 1028
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 1026
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_9

    return-void

    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .registers 5

    .prologue
    .line 1872
    sget-object v1, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1873
    :try_start_3
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mLoaderTask.mContext="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1874
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mLoaderTask.mIsLaunching="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/gp;->e:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mLoaderTask.mStopped="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/gp;->f:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1876
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mLoaderTask.mLoadAndBindStepFinished="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/gp;->b:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mItems size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1872
    monitor-exit v1
    :try_end_76
    .catchall {:try_start_3 .. :try_end_76} :catchall_77

    return-void

    :catchall_77
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final run()V
    .registers 9

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 948
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 949
    :try_start_b
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    const/4 v6, 0x1

    invoke-static {v1, v6}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Z)V

    .line 948
    monitor-exit v5
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_b6

    .line 954
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/go;

    .line 955
    if-eqz v1, :cond_bc

    invoke-interface {v1}, Lcom/android/launcher2/go;->l()Z

    move-result v1

    if-eqz v1, :cond_b9

    move v1, v2

    .line 960
    :goto_27
    iget-object v3, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v3}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 963
    :try_start_2e
    iget-boolean v5, p0, Lcom/android/launcher2/gp;->e:Z

    if-eqz v5, :cond_bf

    :goto_32
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 960
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_2e .. :try_end_36} :catchall_c2

    .line 966
    if-eqz v1, :cond_c5

    .line 968
    invoke-direct {p0}, Lcom/android/launcher2/gp;->e()V

    .line 974
    :goto_3b
    iget-boolean v2, p0, Lcom/android/launcher2/gp;->f:Z

    if-nez v2, :cond_7e

    .line 975
    iget-object v2, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v2}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 981
    :try_start_46
    iget-boolean v3, p0, Lcom/android/launcher2/gp;->e:Z

    if-eqz v3, :cond_4f

    .line 983
    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 980
    :cond_4f
    monitor-exit v2
    :try_end_50
    .catchall {:try_start_46 .. :try_end_50} :catchall_ca

    .line 986
    monitor-enter p0

    :try_start_51
    iget-object v2, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v2}, Lcom/android/launcher2/gb;->c(Lcom/android/launcher2/gb;)Lcom/android/launcher2/az;

    move-result-object v2

    new-instance v3, Lcom/android/launcher2/gq;

    invoke-direct {v3, p0}, Lcom/android/launcher2/gq;-><init>(Lcom/android/launcher2/gp;)V

    new-instance v4, Lcom/android/launcher2/ba;

    invoke-direct {v4, v2, v3}, Lcom/android/launcher2/ba;-><init>(Lcom/android/launcher2/az;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v4}, Lcom/android/launcher2/az;->a(Ljava/lang/Runnable;)V

    :goto_64
    iget-boolean v2, p0, Lcom/android/launcher2/gp;->f:Z

    if-nez v2, :cond_6c

    iget-boolean v2, p0, Lcom/android/launcher2/gp;->b:Z

    if-eqz v2, :cond_cd

    :cond_6c
    monitor-exit p0
    :try_end_6d
    .catchall {:try_start_51 .. :try_end_6d} :catchall_d3

    .line 989
    if-eqz v1, :cond_d6

    .line 991
    invoke-direct {p0}, Lcom/android/launcher2/gp;->f()V

    .line 998
    :goto_72
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 999
    const/4 v1, 0x0

    :try_start_7a
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 998
    monitor-exit v2
    :try_end_7e
    .catchall {:try_start_7a .. :try_end_7e} :catchall_da

    .line 1005
    :cond_7e
    sget-object v3, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v3

    .line 1006
    :try_start_81
    sget-object v1, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_dd

    .line 1009
    sget-object v1, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1005
    monitor-exit v3
    :try_end_97
    .catchall {:try_start_81 .. :try_end_97} :catchall_f5

    .line 1014
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    .line 1016
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->e(Lcom/android/launcher2/gb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1018
    :try_start_a1
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->h(Lcom/android/launcher2/gb;)Lcom/android/launcher2/gp;

    move-result-object v1

    if-ne v1, p0, :cond_ae

    .line 1019
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    invoke-static {v1}, Lcom/android/launcher2/gb;->i(Lcom/android/launcher2/gb;)V

    .line 1021
    :cond_ae
    iget-object v1, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/gb;Z)V

    .line 1016
    monitor-exit v2
    :try_end_b5
    .catchall {:try_start_a1 .. :try_end_b5} :catchall_f8

    return-void

    .line 948
    :catchall_b6
    move-exception v1

    monitor-exit v5

    throw v1

    :cond_b9
    move v1, v3

    .line 955
    goto/16 :goto_27

    :cond_bc
    move v1, v3

    goto/16 :goto_27

    :cond_bf
    move v2, v4

    .line 964
    goto/16 :goto_32

    .line 960
    :catchall_c2
    move-exception v1

    monitor-exit v3

    throw v1

    .line 971
    :cond_c5
    invoke-direct {p0}, Lcom/android/launcher2/gp;->f()V

    goto/16 :goto_3b

    .line 980
    :catchall_ca
    move-exception v1

    monitor-exit v2

    throw v1

    .line 986
    :cond_cd
    :try_start_cd
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_d0
    .catchall {:try_start_cd .. :try_end_d0} :catchall_d3
    .catch Ljava/lang/InterruptedException; {:try_start_cd .. :try_end_d0} :catch_d1

    goto :goto_64

    :catch_d1
    move-exception v2

    goto :goto_64

    :catchall_d3
    move-exception v1

    monitor-exit p0

    throw v1

    .line 994
    :cond_d6
    invoke-direct {p0}, Lcom/android/launcher2/gp;->e()V

    goto :goto_72

    .line 998
    :catchall_da
    move-exception v1

    monitor-exit v2

    throw v1

    .line 1006
    :cond_dd
    :try_start_dd
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1007
    iget-object v5, p0, Lcom/android/launcher2/gp;->c:Lcom/android/launcher2/gb;

    iget-object v6, p0, Lcom/android/launcher2/gp;->d:Landroid/content/Context;

    move-object v0, v2

    check-cast v0, Lcom/android/launcher2/jd;

    move-object v1, v0

    sget-object v7, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-virtual {v5, v6, v1, v2}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/jd;[B)V
    :try_end_f4
    .catchall {:try_start_dd .. :try_end_f4} :catchall_f5

    goto :goto_8b

    .line 1005
    :catchall_f5
    move-exception v1

    monitor-exit v3

    throw v1

    .line 1016
    :catchall_f8
    move-exception v1

    monitor-exit v2

    throw v1
.end method
