.class final Lcom/android/launcher2/hi;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/appwidget/AppWidgetHost;

.field private c:J


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    .line 238
    const-string v0, "launcher.db"

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 235
    iput-wide v3, p0, Lcom/android/launcher2/hi;->c:J

    .line 239
    iput-object p1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    .line 240
    new-instance v0, Landroid/appwidget/AppWidgetHost;

    const/16 v1, 0x400

    invoke-direct {v0, p1, v1}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/launcher2/hi;->b:Landroid/appwidget/AppWidgetHost;

    .line 244
    iget-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_27

    .line 245
    invoke-virtual {p0}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/hi;->c(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    .line 247
    :cond_27
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I
    .registers 24
    .parameter
    .parameter

    .prologue
    .line 374
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 375
    const-string v2, "intent"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 376
    const-string v2, "title"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 377
    const-string v2, "iconType"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 378
    const-string v2, "icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 379
    const-string v2, "iconPackage"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 380
    const-string v2, "iconResource"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 381
    const-string v2, "container"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 382
    const-string v2, "itemType"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 383
    const-string v2, "screen"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 384
    const-string v2, "cellX"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 385
    const-string v2, "cellY"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 386
    const-string v2, "uri"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 387
    const-string v2, "displayMode"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 389
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v0, v2, [Landroid/content/ContentValues;

    move-object/from16 v18, v0

    .line 390
    const/4 v2, 0x0

    .line 391
    :goto_79
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_90

    .line 411
    invoke-virtual/range {p0 .. p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 412
    const/4 v2, 0x0

    .line 414
    :try_start_83
    move-object/from16 v0, v18

    array-length v4, v0

    .line 415
    const/4 v3, 0x0

    :goto_87
    if-lt v3, v4, :cond_1a7

    .line 422
    invoke-virtual/range {p0 .. p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8c
    .catchall {:try_start_83 .. :try_end_8c} :catchall_1c3

    .line 424
    invoke-virtual/range {p0 .. p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 427
    :goto_8f
    return v2

    .line 392
    :cond_90
    new-instance v19, Landroid/content/ContentValues;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v3

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 393
    const-string v3, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 394
    const-string v3, "intent"

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v3, "title"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v3, "iconType"

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 397
    const-string v3, "icon"

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 398
    const-string v3, "iconPackage"

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string v3, "iconResource"

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v3, "container"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 401
    const-string v3, "itemType"

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 402
    const-string v3, "appWidgetId"

    const/16 v20, -0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403
    const-string v3, "screen"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404
    const-string v3, "cellX"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    const-string v3, "cellY"

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    const-string v3, "uri"

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v3, "displayMode"

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408
    add-int/lit8 v3, v2, 0x1

    aput-object v19, v18, v2

    move v2, v3

    goto/16 :goto_79

    .line 416
    :cond_1a7
    :try_start_1a7
    const-string v5, "favorites"

    aget-object v6, v18, v3

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1b0
    .catchall {:try_start_1a7 .. :try_end_1b0} :catchall_1c3

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gez v5, :cond_1bd

    .line 424
    invoke-virtual/range {p0 .. p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 417
    const/4 v2, 0x0

    goto/16 :goto_8f

    .line 419
    :cond_1bd
    add-int/lit8 v2, v2, 0x1

    .line 415
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_87

    .line 423
    :catchall_1c3
    move-exception v2

    .line 424
    invoke-virtual/range {p0 .. p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 425
    throw v2
.end method

.method static synthetic a(Lcom/android/launcher2/hi;Landroid/database/sqlite/SQLiteDatabase;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 809
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->e(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;)J
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v0, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1177
    iget-object v2, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1179
    const/16 v2, 0x8

    invoke-virtual {p3, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 1180
    const/16 v2, 0x9

    invoke-virtual {p3, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 1183
    const/4 v2, 0x0

    .line 1185
    const/16 v3, 0xa

    :try_start_19
    invoke-virtual {p3, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1186
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_21
    .catch Ljava/net/URISyntaxException; {:try_start_19 .. :try_end_21} :catch_2e

    move-result-object v7

    .line 1192
    if-eqz v5, :cond_26

    if-nez v6, :cond_44

    .line 1193
    :cond_26
    const-string v2, "Launcher.LauncherProvider"

    const-string v3, "Shortcut is missing title or icon resource ID"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    :cond_2d
    :goto_2d
    return-wide v0

    .line 1188
    :catch_2e
    move-exception v3

    const-string v3, "Launcher.LauncherProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Shortcut has malformed uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2d

    .line 1197
    :cond_44
    invoke-virtual {p0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v2

    .line 1198
    const/high16 v8, 0x1000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1199
    const-string v8, "intent"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    const-string v7, "title"

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    const-string v6, "itemType"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1202
    const-string v6, "spanX"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1203
    const-string v6, "spanY"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1204
    const-string v6, "iconType"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1205
    const-string v6, "iconPackage"

    iget-object v7, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    const-string v6, "iconResource"

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    const-string v4, "_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1209
    const-string v4, "favorites"

    invoke-static {p1, v4, p2}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2d

    move-wide v0, v2

    .line 1212
    goto :goto_2d
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v8, -0x1

    .line 945
    .line 946
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 947
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 949
    const-string v0, "com.android.mms.ui.ConversationList"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 951
    const-string v4, "com.android.mms.ui.ConversationComposer"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 952
    cmp-long v2, v0, v8

    if-eqz v2, :cond_24

    .line 991
    :cond_23
    :goto_23
    return-wide v0

    :cond_24
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v7

    move-object v5, p4

    move-object v6, p5

    .line 958
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 959
    cmp-long v2, v0, v8

    if-nez v2, :cond_4c

    const-string v2, "com.google.android."

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 960
    const-string v0, "com.google.android."

    const-string v1, "com.android."

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v7

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 964
    :cond_4c
    cmp-long v2, v0, v8

    if-nez v2, :cond_65

    const-string v2, "com.android.camera.Camera"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_65

    .line 965
    const-string v3, "com.google.android.gallery3d"

    const-string v4, "com.android.camera.CameraLauncher"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 969
    :cond_65
    cmp-long v2, v0, v8

    if-nez v2, :cond_7e

    const-string v2, "com.android.contacts.activities.DialtactsActivity"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7e

    .line 970
    const-string v3, "com.android.htccontacts"

    const-string v4, "com.android.htccontacts.DialerTabActivity"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 972
    :cond_7e
    cmp-long v2, v0, v8

    if-nez v2, :cond_97

    const-string v2, "com.android.contacts.activities.PeopleActivity"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_97

    .line 973
    const-string v3, "com.android.htccontacts"

    const-string v4, "com.android.htccontacts.BrowseLayerCarouselActivity"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 977
    :cond_97
    cmp-long v2, v0, v8

    if-nez v2, :cond_b0

    const-string v2, "com.android.camera.Camera"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b0

    .line 978
    const-string v3, "com.sec.android.app.camera"

    const-string v4, "com.sec.android.app.camera.Camera"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 980
    :cond_b0
    cmp-long v2, v0, v8

    if-nez v2, :cond_c9

    const-string v2, "com.android.gallery3d.app.Gallery"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c9

    .line 981
    const-string v3, "com.sec.android.gallery3d"

    const-string v4, "com.sec.android.gallery3d.app.Gallery"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 985
    :cond_c9
    cmp-long v2, v0, v8

    if-nez v2, :cond_e2

    const-string v2, "com.android.contacts.activities.DialtactsActivity"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e2

    .line 986
    const-string v3, "com.android.contacts"

    const-string v4, "com.android.contacts.activities.TwelveKeyDialer"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    .line 988
    :cond_e2
    cmp-long v2, v0, v8

    if-nez v2, :cond_23

    const-string v2, "com.android.gallery3d.app.Gallery"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 989
    const-string v3, "com.miui.gallery"

    const-string v4, "com.miui.gallery.app.Gallery"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v0

    goto/16 :goto_23
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, -0x1

    .line 997
    .line 1002
    :try_start_2
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p3, p4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_b} :catch_6a

    move-result-object v1

    move-object v4, v0

    move-object v5, v1

    .line 1010
    :goto_e
    :try_start_e
    invoke-virtual {p0}, Lcom/android/launcher2/hi;->a()J
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e .. :try_end_11} :catch_85

    move-result-wide v0

    .line 1011
    :try_start_12
    invoke-virtual {p6, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1012
    const/high16 v4, 0x1020

    invoke-virtual {p6, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1014
    const-string v4, "intent"

    const/4 v6, 0x0

    invoke-virtual {p6, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    const-string v4, "title"

    invoke-virtual {v5, p5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    const-string v4, "itemType"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1017
    const-string v4, "spanX"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1018
    const-string v4, "spanY"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1019
    const-string v4, "_id"

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1020
    const-string v4, "favorites"

    invoke-static {p1, v4, p2}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_61
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_61} :catch_88

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_69

    move-wide v0, v2

    .line 1027
    :cond_69
    :goto_69
    return-wide v0

    .line 1005
    :catch_6a
    move-exception v0

    .line 1006
    const/4 v0, 0x1

    :try_start_6c
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    .line 1005
    invoke-virtual {p5, v0}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1007
    new-instance v0, Landroid/content/ComponentName;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    invoke-direct {v0, v1, p4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_81
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6c .. :try_end_81} :catch_85

    move-result-object v1

    move-object v4, v0

    move-object v5, v1

    goto :goto_e

    .line 1023
    :catch_85
    move-exception v0

    move-wide v0, v2

    goto :goto_69

    :catch_88
    move-exception v2

    goto :goto_69
.end method

.method private a(Ljava/lang/String;)Landroid/content/ComponentName;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1056
    iget-object v0, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 1057
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v3

    .line 1058
    if-nez v3, :cond_f

    move-object v0, v1

    .line 1066
    :cond_e
    :goto_e
    return-object v0

    .line 1059
    :cond_f
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 1060
    const/4 v0, 0x0

    move v2, v0

    :goto_15
    if-lt v2, v4, :cond_19

    move-object v0, v1

    .line 1066
    goto :goto_e

    .line 1061
    :cond_19
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 1062
    if-eqz v0, :cond_2d

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1060
    :cond_2d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_15
.end method

.method private static final a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 788
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v2, :cond_a

    .line 789
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 793
    :cond_a
    if-eq v0, v2, :cond_14

    .line 794
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v1, "No start tag found"

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797
    :cond_14
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_41

    .line 798
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected start tag: found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 799
    const-string v2, ", expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 798
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 801
    :cond_41
    return-void
.end method

.method private a(Landroid/content/res/XmlResourceParser;Landroid/util/AttributeSet;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1084
    const/4 v0, 0x1

    invoke-virtual {p5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1085
    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1087
    if-eqz v1, :cond_e

    if-nez v2, :cond_10

    .line 1088
    :cond_e
    const/4 v0, 0x0

    .line 1137
    :goto_f
    return v0

    .line 1091
    :cond_10
    const/4 v0, 0x1

    .line 1092
    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    const/4 v4, 0x0

    :try_start_17
    invoke-virtual {p6, v3, v4}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_46

    .line 1106
    :goto_1a
    if-eqz v0, :cond_9d

    .line 1107
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 1108
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 1111
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1112
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v0

    .line 1113
    :cond_31
    :goto_31
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_61

    .line 1114
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v2

    if-gt v2, v0, :cond_61

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    .line 1134
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/ComponentName;IILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_f

    .line 1096
    :catch_46
    move-exception v3

    .line 1097
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    .line 1096
    invoke-virtual {p6, v3}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1098
    new-instance v3, Landroid/content/ComponentName;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    invoke-direct {v3, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const/4 v1, 0x0

    :try_start_5a
    invoke-virtual {p6, v3, v1}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_5d} :catch_5e

    goto :goto_1a

    .line 1102
    :catch_5e
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1a

    .line 1115
    :cond_61
    const/4 v2, 0x2

    if-ne v1, v2, :cond_31

    .line 1116
    iget-object v1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    sget-object v2, Lcom/anddoes/launcher/at;->Extra:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1120
    const-string v2, "extra"

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    .line 1121
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1122
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1123
    if-eqz v2, :cond_8d

    if-eqz v7, :cond_8d

    .line 1124
    invoke-virtual {v6, v2, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_31

    .line 1126
    :cond_8d
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Widget extras must have a key and value"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1129
    :cond_95
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Widgets can contain only extras"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1137
    :cond_9d
    const/4 v0, 0x0

    goto/16 :goto_f
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 313
    .line 315
    const-string v0, "content://settings/old_favorites?notify=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 317
    iget-object v0, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 321
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_12
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_15} :catch_34

    move-result-object v2

    move-object v3, v2

    .line 327
    :goto_17
    if-eqz v3, :cond_3e

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3e

    .line 329
    :try_start_1f
    invoke-static {p1, v3}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I
    :try_end_22
    .catchall {:try_start_1f .. :try_end_22} :catchall_39

    move-result v2

    if-lez v2, :cond_37

    const/4 v2, 0x1

    .line 331
    :goto_26
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 334
    if-eqz v2, :cond_2e

    .line 335
    invoke-virtual {v0, v1, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 339
    :cond_2e
    :goto_2e
    if-eqz v2, :cond_33

    .line 342
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 345
    :cond_33
    return v2

    :catch_34
    move-exception v2

    move-object v3, v7

    goto :goto_17

    :cond_37
    move v2, v6

    .line 329
    goto :goto_26

    .line 330
    :catchall_39
    move-exception v0

    .line 331
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 332
    throw v0

    :cond_3e
    move v2, v6

    goto :goto_2e
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/ComponentName;IILandroid/os/Bundle;)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1142
    const/4 v1, 0x0

    .line 1143
    iget-object v0, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 1146
    :try_start_7
    iget-object v0, p0, Lcom/android/launcher2/hi;->b:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result v3

    .line 1148
    const-string v0, "itemType"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1149
    const-string v0, "spanX"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1150
    const-string v0, "spanY"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1151
    const-string v0, "appWidgetId"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1152
    const-string v0, "_id"

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1153
    const-string v0, "favorites"

    invoke-static {p1, v0, p2}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_44
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_44} :catch_68

    .line 1155
    const/4 v0, 0x1

    .line 1159
    :try_start_45
    invoke-virtual {v2, v3, p3}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;)V

    .line 1161
    if-eqz p6, :cond_67

    invoke-virtual {p6}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_67

    .line 1162
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.launcher.action.APPWIDGET_DEFAULT_WORKSPACE_CONFIGURE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1163
    invoke-virtual {v1, p3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1164
    invoke-virtual {v1, p6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1165
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1166
    iget-object v2, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_67
    .catch Ljava/lang/RuntimeException; {:try_start_45 .. :try_end_67} :catch_74

    .line 1172
    :cond_67
    :goto_67
    return v0

    .line 1168
    :catch_68
    move-exception v0

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    .line 1169
    :goto_6c
    const-string v2, "Launcher.LauncherProvider"

    const-string v3, "Problem allocating appWidgetId"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_67

    .line 1168
    :catch_74
    move-exception v1

    goto :goto_6c
.end method

.method static synthetic a(Lcom/android/launcher2/hi;Ljava/io/File;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 349
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->getDatabaseName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->close()V

    invoke-static {v1, p1}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    :cond_1e
    return v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)Z
    .registers 14
    .parameter

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 533
    const-string v0, "itemType"

    .line 534
    new-array v1, v9, [I

    aput v9, v1, v8

    .line 533
    invoke-static {v0, v1}, Lcom/android/launcher2/LauncherProvider;->a(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v3

    .line 537
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 541
    :try_start_10
    const-string v1, "favorites"

    .line 542
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "intent"

    aput-object v4, v2, v0

    .line 543
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    .line 541
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_27
    .catchall {:try_start_10 .. :try_end_27} :catchall_113
    .catch Landroid/database/SQLException; {:try_start_10 .. :try_end_27} :catch_116

    move-result-object v1

    .line 544
    if-nez v1, :cond_34

    .line 603
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 604
    if-eqz v1, :cond_32

    .line 605
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_32
    move v0, v8

    .line 609
    :goto_33
    return v0

    .line 548
    :cond_34
    :try_start_34
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 549
    const-string v0, "intent"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 551
    :cond_40
    :goto_40
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_53

    .line 598
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_49
    .catchall {:try_start_34 .. :try_end_49} :catchall_109
    .catch Landroid/database/SQLException; {:try_start_34 .. :try_end_49} :catch_ec

    .line 603
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 604
    if-eqz v1, :cond_51

    .line 605
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_51
    move v0, v9

    .line 609
    goto :goto_33

    .line 552
    :cond_53
    :try_start_53
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 553
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_109
    .catch Landroid/database/SQLException; {:try_start_53 .. :try_end_5a} :catch_ec

    move-result-object v0

    .line 554
    if-eqz v0, :cond_40

    .line 556
    const/4 v6, 0x0

    :try_start_5e
    invoke-static {v0, v6}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 557
    const-string v6, "Home"

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 559
    if-eqz v6, :cond_40

    .line 560
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 561
    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8d

    .line 562
    const-string v10, "com.android.contacts.action.QUICK_CONTACT"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 563
    :cond_8d
    const-string v0, "content://contacts/people/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9d

    .line 564
    const-string v0, "content://com.android.contacts/contacts/lookup/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 567
    :cond_9d
    new-instance v0, Landroid/content/Intent;

    const-string v7, "com.android.contacts.action.QUICK_CONTACT"

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 573
    const v7, 0x10008000

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 576
    const-string v7, "com.android.launcher.intent.extra.shortcut.INGORE_LAUNCH_ANIMATION"

    const/4 v10, 0x1

    .line 575
    invoke-virtual {v0, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 577
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 580
    iget-object v7, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 582
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 583
    const-string v7, "intent"

    .line 584
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    .line 583
    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 587
    const-string v4, "favorites"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v6, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_e0
    .catchall {:try_start_5e .. :try_end_e0} :catchall_109
    .catch Ljava/lang/RuntimeException; {:try_start_5e .. :try_end_e0} :catch_e2
    .catch Ljava/net/URISyntaxException; {:try_start_5e .. :try_end_e0} :catch_ff
    .catch Landroid/database/SQLException; {:try_start_5e .. :try_end_e0} :catch_ec

    goto/16 :goto_40

    .line 590
    :catch_e2
    move-exception v0

    .line 591
    :try_start_e3
    const-string v4, "Launcher.LauncherProvider"

    const-string v5, "Problem upgrading shortcut"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ea
    .catchall {:try_start_e3 .. :try_end_ea} :catchall_109
    .catch Landroid/database/SQLException; {:try_start_e3 .. :try_end_ea} :catch_ec

    goto/16 :goto_40

    .line 599
    :catch_ec
    move-exception v0

    .line 600
    :goto_ed
    :try_start_ed
    const-string v2, "Launcher.LauncherProvider"

    const-string v3, "Problem while upgrading contacts"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f4
    .catchall {:try_start_ed .. :try_end_f4} :catchall_109

    .line 603
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 604
    if-eqz v1, :cond_fc

    .line 605
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_fc
    move v0, v8

    .line 601
    goto/16 :goto_33

    .line 592
    :catch_ff
    move-exception v0

    .line 593
    :try_start_100
    const-string v4, "Launcher.LauncherProvider"

    const-string v5, "Problem upgrading shortcut"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_107
    .catchall {:try_start_100 .. :try_end_107} :catchall_109
    .catch Landroid/database/SQLException; {:try_start_100 .. :try_end_107} :catch_ec

    goto/16 :goto_40

    .line 602
    :catchall_109
    move-exception v0

    .line 603
    :goto_10a
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 604
    if-eqz v1, :cond_112

    .line 605
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 607
    :cond_112
    throw v0

    .line 602
    :catchall_113
    move-exception v0

    move-object v1, v10

    goto :goto_10a

    .line 599
    :catch_116
    move-exception v0

    move-object v1, v10

    goto :goto_ed
.end method

.method static synthetic b(Lcom/android/launcher2/hi;Ljava/io/File;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 360
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->getDatabaseName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->close()V

    invoke-static {p1, v1}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_27
    return v0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)J
    .registers 6
    .parameter

    .prologue
    const-wide/16 v2, -0x1

    .line 682
    const-string v0, "SELECT MAX(_id) FROM favorites"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 687
    if-eqz v4, :cond_28

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 688
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 690
    :goto_16
    if-eqz v4, :cond_1b

    .line 691
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 694
    :cond_1b
    cmp-long v2, v0, v2

    if-nez v2, :cond_27

    .line 695
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: could not query max id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_27
    return-wide v0

    :cond_28
    move-wide v0, v2

    goto :goto_16
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 707
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_de

    .line 713
    const-string v1, "itemType"

    invoke-static {v1, v0}, Lcom/android/launcher2/LauncherProvider;->a(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v3

    .line 717
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 720
    :try_start_10
    const-string v1, "favorites"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "itemType"

    aput-object v4, v2, v0

    .line 721
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    .line 720
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_27
    .catchall {:try_start_10 .. :try_end_27} :catchall_ce
    .catch Landroid/database/SQLException; {:try_start_10 .. :try_end_27} :catch_db

    move-result-object v1

    .line 725
    :try_start_28
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 726
    :cond_2d
    :goto_2d
    if-eqz v1, :cond_35

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_41

    .line 774
    :cond_35
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_38
    .catchall {:try_start_28 .. :try_end_38} :catchall_d9
    .catch Landroid/database/SQLException; {:try_start_28 .. :try_end_38} :catch_bc

    .line 778
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 779
    if-eqz v1, :cond_40

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 783
    :cond_40
    :goto_40
    return-void

    .line 727
    :cond_41
    const/4 v0, 0x0

    :try_start_42
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 728
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_4a
    .catchall {:try_start_42 .. :try_end_4a} :catchall_d9
    .catch Landroid/database/SQLException; {:try_start_42 .. :try_end_4a} :catch_bc

    move-result v0

    .line 732
    :try_start_4b
    iget-object v5, p0, Lcom/android/launcher2/hi;->b:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v5}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result v5

    .line 738
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 739
    const-string v6, "itemType"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 740
    const-string v6, "appWidgetId"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 743
    const/16 v5, 0x3e9

    if-ne v0, v5, :cond_9d

    .line 744
    const-string v5, "spanX"

    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v5, "spanY"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 751
    :goto_7f
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 752
    const-string v4, "favorites"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 754
    const/16 v3, 0x3e8

    if-eq v0, v3, :cond_2d

    .line 759
    const/16 v3, 0x3ea

    if-eq v0, v3, :cond_2d

    goto :goto_2d

    .line 747
    :cond_9d
    const-string v5, "spanX"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 748
    const-string v5, "spanY"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_b1
    .catchall {:try_start_4b .. :try_end_b1} :catchall_d9
    .catch Ljava/lang/RuntimeException; {:try_start_4b .. :try_end_b1} :catch_b2
    .catch Landroid/database/SQLException; {:try_start_4b .. :try_end_b1} :catch_bc

    goto :goto_7f

    .line 769
    :catch_b2
    move-exception v0

    .line 770
    :try_start_b3
    const-string v3, "Launcher.LauncherProvider"

    const-string v4, "Problem allocating appWidgetId"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ba
    .catchall {:try_start_b3 .. :try_end_ba} :catchall_d9
    .catch Landroid/database/SQLException; {:try_start_b3 .. :try_end_ba} :catch_bc

    goto/16 :goto_2d

    .line 775
    :catch_bc
    move-exception v0

    .line 776
    :goto_bd
    :try_start_bd
    const-string v2, "Launcher.LauncherProvider"

    const-string v3, "Problem while allocating appWidgetIds for existing widgets"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c4
    .catchall {:try_start_bd .. :try_end_c4} :catchall_d9

    .line 778
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 779
    if-eqz v1, :cond_40

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_40

    .line 777
    :catchall_ce
    move-exception v0

    move-object v1, v8

    .line 778
    :goto_d0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 779
    if-eqz v1, :cond_d8

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 782
    :cond_d8
    throw v0

    .line 777
    :catchall_d9
    move-exception v0

    goto :goto_d0

    .line 775
    :catch_db
    move-exception v0

    move-object v1, v8

    goto :goto_bd

    .line 707
    :array_de
    .array-data 0x4
        0xe8t 0x3t 0x0t 0x0t
        0xeat 0x3t 0x0t 0x0t
        0xe9t 0x3t 0x0t 0x0t
    .end array-data
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;)I
    .registers 24
    .parameter

    .prologue
    .line 810
    new-instance v8, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v4, 0x0

    invoke-direct {v8, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 811
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 812
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 814
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 817
    const/16 v16, 0x0

    .line 819
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v19

    .line 820
    invoke-static/range {v19 .. v19}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v20

    .line 821
    const-string v3, "favorites"

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/android/launcher2/hi;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 823
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v21

    .line 826
    :cond_3a
    :goto_3a
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_49

    .line 827
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v4

    move/from16 v0, v21

    if-le v4, v0, :cond_2a6

    :cond_49
    const/4 v4, 0x1

    if-ne v3, v4, :cond_4f

    move/from16 v3, v16

    .line 940
    :goto_4e
    return v3

    .line 829
    :cond_4f
    const/4 v4, 0x2

    if-ne v3, v4, :cond_3a

    .line 830
    const/4 v3, 0x0

    .line 834
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 836
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    sget-object v9, Lcom/anddoes/launcher/at;->Favorite:[I

    move-object/from16 v0, v20

    invoke-virtual {v6, v0, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 838
    const-wide/16 v9, -0x64

    .line 839
    const/4 v11, 0x2

    invoke-virtual {v6, v11}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v11

    if-eqz v11, :cond_79

    .line 840
    const/4 v9, 0x2

    invoke-virtual {v6, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    .line 843
    :cond_79
    const/4 v11, 0x3

    invoke-virtual {v6, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 844
    const/4 v12, 0x4

    invoke-virtual {v6, v12}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 845
    const/4 v13, 0x5

    invoke-virtual {v6, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 855
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 856
    const-string v14, "container"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 857
    const-string v9, "screen"

    invoke-virtual {v5, v9, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    const-string v9, "cellX"

    invoke-virtual {v5, v9, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const-string v9, "cellY"

    invoke-virtual {v5, v9, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    const-string v9, "favorite"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    .line 862
    invoke-direct/range {v3 .. v8}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v3

    .line 863
    const-wide/16 v9, 0x0

    cmp-long v3, v3, v9

    if-ltz v3, :cond_cf

    const/4 v3, 0x1

    .line 929
    :cond_ba
    :goto_ba
    if-eqz v3, :cond_be

    add-int/lit8 v16, v16, 0x1

    .line 930
    :cond_be
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_c1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1c .. :try_end_c1} :catch_c3
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_c1} :catch_260
    .catch Ljava/lang/RuntimeException; {:try_start_1c .. :try_end_c1} :catch_291

    goto/16 :goto_3a

    .line 932
    :catch_c3
    move-exception v3

    move-object v4, v3

    move/from16 v3, v16

    .line 933
    const-string v5, "Launcher.LauncherProvider"

    const-string v6, "Got exception parsing favorites."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4e

    .line 863
    :cond_cf
    const/4 v3, 0x0

    goto :goto_ba

    .line 864
    :cond_d1
    :try_start_d1
    const-string v9, "search"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_104

    .line 865
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    const-string v4, "search"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/SearchManager;

    invoke-virtual {v3}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v3

    if-nez v3, :cond_f9

    const/4 v12, 0x0

    :goto_ec
    const/4 v13, 0x4

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v5

    invoke-direct/range {v9 .. v15}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/ComponentName;IILandroid/os/Bundle;)Z

    move-result v3

    goto :goto_ba

    :cond_f9
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/launcher2/hi;->a(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v12

    goto :goto_ec

    .line 866
    :cond_104
    const-string v9, "clock"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_122

    .line 867
    new-instance v12, Landroid/content/ComponentName;

    const-string v3, "com.android.alarmclock"

    const-string v4, "com.android.alarmclock.AnalogAppWidgetProvider"

    invoke-direct {v12, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v13, 0x2

    const/4 v14, 0x2

    const/4 v15, 0x0

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v5

    invoke-direct/range {v9 .. v15}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/ComponentName;IILandroid/os/Bundle;)Z

    move-result v3

    goto :goto_ba

    .line 868
    :cond_122
    const-string v9, "appwidget"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13a

    move-object/from16 v9, p0

    move-object/from16 v10, v19

    move-object/from16 v11, v20

    move-object/from16 v12, p1

    move-object v13, v5

    move-object v14, v6

    move-object v15, v7

    .line 869
    invoke-direct/range {v9 .. v15}, Lcom/android/launcher2/hi;->a(Landroid/content/res/XmlResourceParser;Landroid/util/AttributeSet;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;)Z

    move-result v3

    goto :goto_ba

    .line 870
    :cond_13a
    const-string v9, "shortcut"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_156

    .line 871
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;)J

    move-result-wide v3

    .line 872
    const-wide/16 v9, 0x0

    cmp-long v3, v3, v9

    if-ltz v3, :cond_153

    const/4 v3, 0x1

    goto/16 :goto_ba

    :cond_153
    const/4 v3, 0x0

    goto/16 :goto_ba

    .line 873
    :cond_156
    const-string v9, "folder"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_ba

    .line 875
    const/16 v3, 0x9

    const/4 v4, -0x1

    invoke-virtual {v6, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 876
    const/4 v4, -0x1

    if-eq v3, v4, :cond_200

    .line 877
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 881
    :goto_174
    const-string v4, "title"

    invoke-virtual {v5, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    const-string v3, "itemType"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "spanX"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "spanY"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v3

    const-string v9, "_id"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "favorites"

    move-object/from16 v0, p1

    invoke-static {v0, v9, v5}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-gtz v9, :cond_211

    const-wide/16 v3, -0x1

    move-wide/from16 v17, v3

    .line 883
    :goto_1b6
    const-wide/16 v3, 0x0

    cmp-long v3, v17, v3

    if-ltz v3, :cond_214

    const/4 v3, 0x1

    .line 885
    :goto_1bd
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 887
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v15

    .line 888
    :cond_1c6
    :goto_1c6
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_216

    .line 889
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v10

    if-gt v10, v15, :cond_216

    .line 920
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x2

    if-ge v9, v10, :cond_ba

    const-wide/16 v9, 0x0

    cmp-long v9, v17, v9

    if-ltz v9, :cond_ba

    .line 922
    move-object/from16 v0, p1

    move-wide/from16 v1, v17

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 923
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1fd

    .line 924
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 926
    :cond_1fd
    const/4 v3, 0x0

    goto/16 :goto_ba

    .line 879
    :cond_200
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070275

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_174

    :cond_211
    move-wide/from16 v17, v3

    .line 882
    goto :goto_1b6

    .line 883
    :cond_214
    const/4 v3, 0x0

    goto :goto_1bd

    .line 890
    :cond_216
    const/4 v10, 0x2

    if-ne v9, v10, :cond_1c6

    .line 891
    invoke-interface/range {v19 .. v19}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v9

    .line 895
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    .line 896
    sget-object v11, Lcom/anddoes/launcher/at;->Favorite:[I

    .line 895
    move-object/from16 v0, v20

    invoke-virtual {v10, v0, v11}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v12

    .line 897
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 898
    const-string v10, "container"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 900
    const-string v10, "favorite"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_26d

    const-wide/16 v10, 0x0

    cmp-long v10, v17, v10

    if-ltz v10, :cond_26d

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v5

    move-object v13, v7

    move-object v14, v8

    .line 902
    invoke-direct/range {v9 .. v14}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Landroid/content/pm/PackageManager;Landroid/content/Intent;)J

    move-result-wide v9

    .line 903
    const-wide/16 v13, 0x0

    cmp-long v11, v9, v13

    if-ltz v11, :cond_25b

    .line 904
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 915
    :cond_25b
    :goto_25b
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_25e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_d1 .. :try_end_25e} :catch_c3
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_25e} :catch_260
    .catch Ljava/lang/RuntimeException; {:try_start_d1 .. :try_end_25e} :catch_291

    goto/16 :goto_1c6

    .line 934
    :catch_260
    move-exception v3

    move-object v4, v3

    move/from16 v3, v16

    .line 935
    const-string v5, "Launcher.LauncherProvider"

    const-string v6, "Got exception parsing favorites."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4e

    .line 906
    :cond_26d
    :try_start_26d
    const-string v10, "shortcut"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_29e

    const-wide/16 v9, 0x0

    cmp-long v9, v17, v9

    if-ltz v9, :cond_29e

    .line 907
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v12}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/res/TypedArray;)J

    move-result-wide v9

    .line 908
    const-wide/16 v13, 0x0

    cmp-long v11, v9, v13

    if-ltz v11, :cond_25b

    .line 909
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_290
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_26d .. :try_end_290} :catch_c3
    .catch Ljava/io/IOException; {:try_start_26d .. :try_end_290} :catch_260
    .catch Ljava/lang/RuntimeException; {:try_start_26d .. :try_end_290} :catch_291

    goto :goto_25b

    .line 936
    :catch_291
    move-exception v3

    move-object v4, v3

    move/from16 v3, v16

    .line 937
    const-string v5, "Launcher.LauncherProvider"

    const-string v6, "Got exception parsing favorites."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4e

    .line 912
    :cond_29e
    :try_start_29e
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Folders can contain only shortcuts"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2a6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_29e .. :try_end_2a6} :catch_c3
    .catch Ljava/io/IOException; {:try_start_29e .. :try_end_2a6} :catch_260
    .catch Ljava/lang/RuntimeException; {:try_start_29e .. :try_end_2a6} :catch_291

    :cond_2a6
    move/from16 v3, v16

    goto/16 :goto_4e
.end method


# virtual methods
.method public final a()J
    .registers 5

    .prologue
    .line 674
    iget-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_10

    .line 675
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: max id was not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_10
    iget-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    .line 678
    iget-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    return-wide v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 264
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    .line 266
    const-string v0, "CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,isShortcut INTEGER,iconType INTEGER,iconPackage TEXT,iconResource TEXT,icon BLOB,uri TEXT,displayMode INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/android/launcher2/hi;->b:Landroid/appwidget/AppWidgetHost;

    if-eqz v0, :cond_1f

    .line 289
    iget-object v0, p0, Lcom/android/launcher2/hi;->b:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHost;->deleteHost()V

    .line 290
    iget-object v0, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/launcher2/LauncherProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 293
    :cond_1f
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-nez v0, :cond_b6

    .line 295
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v1, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DB_CREATED_BUT_DEFAULT_WORKSPACE_NOT_LOADED"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 296
    const-string v0, "ALWAYS"

    iget-object v1, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 297
    const v2, 0x7f0701b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 296
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b6

    .line 298
    iget-object v0, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/anddoes/launcher/au;->a(Landroid/content/Context;)Lcom/anddoes/launcher/au;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "itemType"

    const/16 v3, 0x3e9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "container"

    const/16 v3, -0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "screen"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cellX"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cellY"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "spanX"

    iget v3, v0, Lcom/anddoes/launcher/i;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "spanY"

    iget v0, v0, Lcom/anddoes/launcher/i;->o:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "_id"

    invoke-virtual {p0}, Lcom/android/launcher2/hi;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "favorites"

    invoke-static {p1, v0, v1}, Lcom/android/launcher2/LauncherProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 301
    :cond_b6
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v6, 0x3

    .line 434
    .line 435
    if-ge p2, v6, :cond_15d

    .line 437
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 440
    :try_start_b
    const-string v0, "ALTER TABLE favorites ADD COLUMN appWidgetId INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_ba
    .catch Landroid/database/SQLException; {:try_start_b .. :try_end_13} :catch_aa

    .line 448
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, v6

    .line 452
    :goto_17
    if-ne v1, v6, :cond_1c

    .line 453
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 457
    :cond_1c
    :goto_1c
    const/4 v0, 0x4

    if-ge v1, v0, :cond_20

    .line 458
    const/4 v1, 0x4

    .line 469
    :cond_20
    const/4 v0, 0x6

    if-ge v1, v0, :cond_15a

    .line 471
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 473
    :try_start_26
    const-string v0, "UPDATE favorites SET screen=(screen + 1);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2e
    .catchall {:try_start_26 .. :try_end_2e} :catchall_ce
    .catch Landroid/database/SQLException; {:try_start_26 .. :try_end_2e} :catch_bf

    .line 479
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 483
    :goto_31
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->b(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-eqz v0, :cond_15a

    .line 484
    const/4 v0, 0x6

    .line 488
    :goto_38
    const/4 v1, 0x7

    if-ge v0, v1, :cond_3f

    .line 490
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 491
    const/4 v0, 0x7

    .line 494
    :cond_3f
    const/16 v1, 0x8

    if-ge v0, v1, :cond_7e

    .line 498
    const-string v0, "Launcher.LauncherProvider"

    const-string v1, "normalizing icons"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_4d
    const-string v0, "UPDATE favorites SET icon=? WHERE _id=?"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    :try_end_52
    .catchall {:try_start_4d .. :try_end_52} :catchall_145
    .catch Landroid/database/SQLException; {:try_start_4d .. :try_end_52} :catch_157

    move-result-object v1

    :try_start_53
    const-string v0, "SELECT _id, icon FROM favorites WHERE iconType=1"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    const-string v0, "icon"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    :cond_66
    :goto_66
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_d3

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6f
    .catchall {:try_start_53 .. :try_end_6f} :catchall_155
    .catch Landroid/database/SQLException; {:try_start_53 .. :try_end_6f} :catch_12e

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz v1, :cond_77

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_77
    if-eqz v2, :cond_7c

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 499
    :cond_7c
    :goto_7c
    const/16 v0, 0x8

    .line 502
    :cond_7e
    const/16 v1, 0x9

    if-ge v0, v1, :cond_92

    .line 505
    iget-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_90

    .line 506
    invoke-static {p1}, Lcom/android/launcher2/hi;->c(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/hi;->c:J

    .line 511
    :cond_90
    const/16 v0, 0x9

    .line 517
    :cond_92
    if-ge v0, v3, :cond_98

    .line 521
    invoke-direct {p0, p1}, Lcom/android/launcher2/hi;->b(Landroid/database/sqlite/SQLiteDatabase;)Z

    move v0, v3

    .line 525
    :cond_98
    if-eq v0, v3, :cond_a9

    .line 526
    const-string v0, "Launcher.LauncherProvider"

    const-string v1, "Destroying all old data."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    const-string v0, "DROP TABLE IF EXISTS favorites"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 528
    invoke-virtual {p0, p1}, Lcom/android/launcher2/hi;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 530
    :cond_a9
    return-void

    .line 444
    :catch_aa
    move-exception v0

    .line 446
    :try_start_ab
    const-string v1, "Launcher.LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b4
    .catchall {:try_start_ab .. :try_end_b4} :catchall_ba

    .line 448
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v1, p2

    goto/16 :goto_17

    .line 447
    :catchall_ba
    move-exception v0

    .line 448
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 449
    throw v0

    .line 475
    :catch_bf
    move-exception v0

    .line 477
    :try_start_c0
    const-string v6, "Launcher.LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c9
    .catchall {:try_start_c0 .. :try_end_c9} :catchall_ce

    .line 479
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_31

    .line 478
    :catchall_ce
    move-exception v0

    .line 479
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 480
    throw v0

    .line 498
    :cond_d3
    :try_start_d3
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_da
    .catchall {:try_start_d3 .. :try_end_da} :catchall_155
    .catch Landroid/database/SQLException; {:try_start_d3 .. :try_end_da} :catch_12e

    move-result-object v0

    const/4 v10, 0x0

    :try_start_dc
    array-length v11, v0

    invoke-static {v0, v10, v11}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v10, p0, Lcom/android/launcher2/hi;->a:Landroid/content/Context;

    invoke-static {v0, v10}, Lcom/android/launcher2/jj;->c(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_66

    const/4 v10, 0x1

    invoke-virtual {v1, v10, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    invoke-static {v0}, Lcom/android/launcher2/di;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v10

    if-eqz v10, :cond_fa

    const/4 v11, 0x2

    invoke-virtual {v1, v11, v10}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    :cond_fa
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_fd
    .catchall {:try_start_dc .. :try_end_fd} :catchall_155
    .catch Ljava/lang/Exception; {:try_start_dc .. :try_end_fd} :catch_ff
    .catch Landroid/database/SQLException; {:try_start_dc .. :try_end_fd} :catch_12e

    goto/16 :goto_66

    :catch_ff
    move-exception v0

    if-nez v4, :cond_119

    :try_start_102
    const-string v4, "Launcher.LauncherProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Failed normalizing icon "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_116
    move v4, v5

    goto/16 :goto_66

    :cond_119
    const-string v0, "Launcher.LauncherProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "Also failed normalizing icon "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12d
    .catchall {:try_start_102 .. :try_end_12d} :catchall_155
    .catch Landroid/database/SQLException; {:try_start_102 .. :try_end_12d} :catch_12e

    goto :goto_116

    :catch_12e
    move-exception v0

    :goto_12f
    :try_start_12f
    const-string v4, "Launcher.LauncherProvider"

    const-string v5, "Problem while allocating appWidgetIds for existing widgets"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_136
    .catchall {:try_start_12f .. :try_end_136} :catchall_155

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz v1, :cond_13e

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_13e
    if-eqz v2, :cond_7c

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_7c

    :catchall_145
    move-exception v0

    move-object v1, v2

    :goto_147
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz v1, :cond_14f

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_14f
    if-eqz v2, :cond_154

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_154
    throw v0

    :catchall_155
    move-exception v0

    goto :goto_147

    :catch_157
    move-exception v0

    move-object v1, v2

    goto :goto_12f

    :cond_15a
    move v0, v1

    goto/16 :goto_38

    :cond_15d
    move v1, p2

    goto/16 :goto_1c
.end method
