.class public final Lcom/android/launcher2/ho;
.super Landroid/animation/Animator;
.source "SourceFile"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field a:Ljava/util/EnumSet;

.field b:Landroid/view/ViewPropertyAnimator;

.field c:Landroid/view/View;

.field d:F

.field e:F

.field f:F

.field g:F

.field h:F

.field i:F

.field j:J

.field k:J

.field l:Landroid/animation/TimeInterpolator;

.field m:Ljava/util/ArrayList;

.field n:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 40
    const-class v0, Lcom/android/launcher2/hp;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/ho;->n:Z

    .line 57
    iput-object p1, p0, Lcom/android/launcher2/ho;->c:Landroid/view/View;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(F)Lcom/android/launcher2/ho;
    .registers 4
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->a:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 222
    iput p1, p0, Lcom/android/launcher2/ho;->d:F

    .line 223
    return-object p0
.end method

.method public final addListener(Landroid/animation/Animator$AnimatorListener;)V
    .registers 3
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public final b(F)Lcom/android/launcher2/ho;
    .registers 4
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->b:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 228
    iput p1, p0, Lcom/android/launcher2/ho;->e:F

    .line 229
    return-object p0
.end method

.method public final c(F)Lcom/android/launcher2/ho;
    .registers 4
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->c:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 234
    iput p1, p0, Lcom/android/launcher2/ho;->f:F

    .line 235
    return-object p0
.end method

.method public final cancel()V
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_9

    .line 69
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 71
    :cond_9
    return-void
.end method

.method public final clone()Landroid/animation/Animator;
    .registers 3

    .prologue
    .line 75
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d(F)Lcom/android/launcher2/ho;
    .registers 4
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->d:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 240
    iput p1, p0, Lcom/android/launcher2/ho;->g:F

    .line 241
    return-object p0
.end method

.method public final e(F)Lcom/android/launcher2/ho;
    .registers 4
    .parameter

    .prologue
    .line 251
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->f:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 252
    iput p1, p0, Lcom/android/launcher2/ho;->i:F

    .line 253
    return-object p0
.end method

.method public final end()V
    .registers 3

    .prologue
    .line 80
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getDuration()J
    .registers 3

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/android/launcher2/ho;->k:J

    return-wide v0
.end method

.method public final getListeners()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getStartDelay()J
    .registers 3

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/android/launcher2/ho;->j:J

    return-wide v0
.end method

.method public final isRunning()Z
    .registers 2

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/android/launcher2/ho;->n:Z

    return v0
.end method

.method public final isStarted()Z
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 100
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_d

    .line 104
    iput-boolean v2, p0, Lcom/android/launcher2/ho;->n:Z

    .line 105
    return-void

    .line 101
    :cond_d
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 102
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 109
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_d

    .line 113
    iput-boolean v2, p0, Lcom/android/launcher2/ho;->n:Z

    .line 114
    return-void

    .line 110
    :cond_d
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 111
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 118
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_b

    .line 122
    return-void

    .line 119
    :cond_b
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 120
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 118
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 126
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_e

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/ho;->n:Z

    .line 131
    return-void

    .line 127
    :cond_e
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 128
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 126
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public final removeAllListeners()V
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 146
    return-void
.end method

.method public final removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .registers 3
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/launcher2/ho;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public final setDuration(J)Landroid/animation/Animator;
    .registers 5
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->h:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 156
    iput-wide p1, p0, Lcom/android/launcher2/ho;->k:J

    .line 157
    return-object p0
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)V
    .registers 4
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->i:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 163
    iput-object p1, p0, Lcom/android/launcher2/ho;->l:Landroid/animation/TimeInterpolator;

    .line 164
    return-void
.end method

.method public final setStartDelay(J)V
    .registers 5
    .parameter

    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->g:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 169
    iput-wide p1, p0, Lcom/android/launcher2/ho;->j:J

    .line 170
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 174
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setupEndValues()V
    .registers 1

    .prologue
    .line 180
    return-void
.end method

.method public final setupStartValues()V
    .registers 1

    .prologue
    .line 184
    return-void
.end method

.method public final start()V
    .registers 4

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/launcher2/ho;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    .line 189
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->a:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 190
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->d:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 192
    :cond_19
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->b:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 193
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->e:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 195
    :cond_2a
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->c:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 196
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->f:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 198
    :cond_3b
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->e:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 199
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->h:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    .line 201
    :cond_4c
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->d:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 202
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->g:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 204
    :cond_5d
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->f:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 205
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Lcom/android/launcher2/ho;->i:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 207
    :cond_6e
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->g:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 208
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget-wide v1, p0, Lcom/android/launcher2/ho;->j:J

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 210
    :cond_7f
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->h:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 211
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget-wide v1, p0, Lcom/android/launcher2/ho;->k:J

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 213
    :cond_90
    iget-object v0, p0, Lcom/android/launcher2/ho;->a:Ljava/util/EnumSet;

    sget-object v1, Lcom/android/launcher2/hp;->i:Lcom/android/launcher2/hp;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 214
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/android/launcher2/ho;->l:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 216
    :cond_a1
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 217
    iget-object v0, p0, Lcom/android/launcher2/ho;->b:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 218
    return-void
.end method
