.class final enum Lcom/android/launcher2/hp;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/android/launcher2/hp;

.field public static final enum b:Lcom/android/launcher2/hp;

.field public static final enum c:Lcom/android/launcher2/hp;

.field public static final enum d:Lcom/android/launcher2/hp;

.field public static final enum e:Lcom/android/launcher2/hp;

.field public static final enum f:Lcom/android/launcher2/hp;

.field public static final enum g:Lcom/android/launcher2/hp;

.field public static final enum h:Lcom/android/launcher2/hp;

.field public static final enum i:Lcom/android/launcher2/hp;

.field private static final synthetic j:[Lcom/android/launcher2/hp;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "TRANSLATION_X"

    invoke-direct {v0, v1, v3}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->a:Lcom/android/launcher2/hp;

    .line 31
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "TRANSLATION_Y"

    invoke-direct {v0, v1, v4}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->b:Lcom/android/launcher2/hp;

    .line 32
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "SCALE_X"

    invoke-direct {v0, v1, v5}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->c:Lcom/android/launcher2/hp;

    .line 33
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "SCALE_Y"

    invoke-direct {v0, v1, v6}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->d:Lcom/android/launcher2/hp;

    .line 34
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "ROTATION_Y"

    invoke-direct {v0, v1, v7}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->e:Lcom/android/launcher2/hp;

    .line 35
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "ALPHA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->f:Lcom/android/launcher2/hp;

    .line 36
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "START_DELAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->g:Lcom/android/launcher2/hp;

    .line 37
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "DURATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->h:Lcom/android/launcher2/hp;

    .line 38
    new-instance v0, Lcom/android/launcher2/hp;

    const-string v1, "INTERPOLATOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/hp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/hp;->i:Lcom/android/launcher2/hp;

    .line 29
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/android/launcher2/hp;

    sget-object v1, Lcom/android/launcher2/hp;->a:Lcom/android/launcher2/hp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/launcher2/hp;->b:Lcom/android/launcher2/hp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/launcher2/hp;->c:Lcom/android/launcher2/hp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/launcher2/hp;->d:Lcom/android/launcher2/hp;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/launcher2/hp;->e:Lcom/android/launcher2/hp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/launcher2/hp;->f:Lcom/android/launcher2/hp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/launcher2/hp;->g:Lcom/android/launcher2/hp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/launcher2/hp;->h:Lcom/android/launcher2/hp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/launcher2/hp;->i:Lcom/android/launcher2/hp;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/launcher2/hp;->j:[Lcom/android/launcher2/hp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/launcher2/hp;
    .registers 2
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/android/launcher2/hp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/hp;

    return-object v0
.end method

.method public static values()[Lcom/android/launcher2/hp;
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/android/launcher2/hp;->j:[Lcom/android/launcher2/hp;

    array-length v1, v0

    new-array v2, v1, [Lcom/android/launcher2/hp;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
