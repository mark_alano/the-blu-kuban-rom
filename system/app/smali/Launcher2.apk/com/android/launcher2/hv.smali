.class public final Lcom/android/launcher2/hv;
.super Lcom/android/launcher2/ih;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2932
    iput-object p1, p0, Lcom/android/launcher2/hv;->a:Lcom/android/launcher2/PagedView;

    invoke-direct {p0, p1}, Lcom/android/launcher2/ih;-><init>(Lcom/android/launcher2/PagedView;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;F)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 2935
    iget-object v0, p0, Lcom/android/launcher2/hv;->a:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/hv;->a:Lcom/android/launcher2/PagedView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/hv;->a:Lcom/android/launcher2/PagedView;

    iget v1, v1, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    .line 2936
    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2937
    const/high16 v0, -0x3d4c

    mul-float/2addr v0, p2

    .line 2938
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotationY(F)V

    .line 2939
    float-to-double v0, p2

    const-wide v2, -0x401051eb851eb852L

    cmpg-double v0, v0, v2

    if-lez v0, :cond_31

    float-to-double v0, p2

    const-wide v2, 0x3fefae147ae147aeL

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_36

    .line 2940
    :cond_31
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2945
    :goto_35
    return-void

    .line 2943
    :cond_36
    const/high16 v0, 0x3f80

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_35
.end method
