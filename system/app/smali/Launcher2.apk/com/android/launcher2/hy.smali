.class public final Lcom/android/launcher2/hy;
.super Lcom/android/launcher2/ih;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2970
    iput-object p1, p0, Lcom/android/launcher2/hy;->a:Lcom/android/launcher2/PagedView;

    invoke-direct {p0, p1}, Lcom/android/launcher2/ih;-><init>(Lcom/android/launcher2/PagedView;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2973
    iget-object v0, p0, Lcom/android/launcher2/hy;->a:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/hy;->a:Lcom/android/launcher2/PagedView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/hy;->a:Lcom/android/launcher2/PagedView;

    iget v1, v1, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    .line 2974
    const/high16 v1, -0x3ccc

    mul-float/2addr v1, p2

    .line 2975
    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2976
    invoke-virtual {p1, v1}, Landroid/view/View;->setRotationY(F)V

    .line 2977
    const/high16 v0, 0x3f80

    .line 2978
    const/high16 v2, -0x3d4c

    cmpg-float v2, v1, v2

    if-lez v2, :cond_2b

    const/high16 v2, 0x42b4

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_30

    .line 2979
    :cond_2b
    const/4 v0, 0x0

    .line 2980
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2982
    :cond_30
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2983
    return-void
.end method
