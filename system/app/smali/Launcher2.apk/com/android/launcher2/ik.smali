.class public final Lcom/android/launcher2/ik;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/hr;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field protected g:Lcom/android/launcher2/im;

.field protected h:Landroid/widget/ScrollView;

.field public i:I

.field private j:I

.field private k:I

.field private l:Lcom/android/launcher2/Launcher;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;B)V

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;C)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;Z)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 571
    iput-boolean v3, p0, Lcom/android/launcher2/ik;->m:Z

    .line 573
    iput v3, p0, Lcom/android/launcher2/ik;->i:I

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/ik;->l:Lcom/android/launcher2/Launcher;

    .line 66
    iput-boolean p2, p0, Lcom/android/launcher2/ik;->m:Z

    .line 67
    invoke-virtual {p0, v3}, Lcom/android/launcher2/ik;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 71
    iput v1, p0, Lcom/android/launcher2/ik;->c:I

    iput v1, p0, Lcom/android/launcher2/ik;->j:I

    .line 74
    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    iput v0, p0, Lcom/android/launcher2/ik;->d:I

    iput v0, p0, Lcom/android/launcher2/ik;->k:I

    .line 75
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/ik;->a:I

    .line 76
    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/ik;->b:I

    .line 80
    new-instance v0, Lcom/android/launcher2/im;

    invoke-direct {v0, p1}, Lcom/android/launcher2/im;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    .line 81
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    iget v1, p0, Lcom/android/launcher2/ik;->c:I

    iget v2, p0, Lcom/android/launcher2/ik;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/im;->b(II)V

    .line 82
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    iget v1, p0, Lcom/android/launcher2/ik;->e:I

    iget v2, p0, Lcom/android/launcher2/ik;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/im;->a(II)V

    .line 83
    iget-boolean v0, p0, Lcom/android/launcher2/ik;->m:Z

    if-nez v0, :cond_5d

    .line 84
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/ik;->addView(Landroid/view/View;)V

    .line 94
    :goto_5c
    return-void

    .line 86
    :cond_5d
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    .line 87
    iget-object v0, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 89
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 90
    iget-object v1, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/ik;->addView(Landroid/view/View;)V

    goto :goto_5c
.end method

.method public constructor <init>(Landroid/content/Context;ZB)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 576
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;Z)V

    .line 577
    return-void
.end method


# virtual methods
.method public final a(I)I
    .registers 3
    .parameter

    .prologue
    .line 432
    iget v0, p0, Lcom/android/launcher2/ik;->c:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0}, Lcom/android/launcher2/im;->removeAllViews()V

    .line 151
    iget-object v0, p0, Lcom/android/launcher2/ik;->l:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->I()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 152
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/ik;->setLayerType(ILandroid/graphics/Paint;)V

    .line 154
    :cond_12
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/im;->a(II)V

    .line 359
    return-void
.end method

.method public final a(Landroid/view/View;ILcom/android/launcher2/il;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    .line 133
    iget v0, p3, Lcom/android/launcher2/il;->a:I

    if-ltz v0, :cond_18

    iget v0, p3, Lcom/android/launcher2/il;->a:I

    iget v1, p0, Lcom/android/launcher2/ik;->a:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_18

    .line 134
    iget v0, p3, Lcom/android/launcher2/il;->b:I

    if-ltz v0, :cond_18

    iget v0, p3, Lcom/android/launcher2/il;->b:I

    iget v1, p0, Lcom/android/launcher2/ik;->b:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1c

    :cond_18
    iget-boolean v0, p0, Lcom/android/launcher2/ik;->m:Z

    if-eqz v0, :cond_37

    .line 137
    :cond_1c
    iget v0, p3, Lcom/android/launcher2/il;->c:I

    if-gez v0, :cond_24

    iget v0, p0, Lcom/android/launcher2/ik;->a:I

    iput v0, p3, Lcom/android/launcher2/il;->c:I

    .line 138
    :cond_24
    iget v0, p3, Lcom/android/launcher2/il;->d:I

    if-gez v0, :cond_2c

    iget v0, p0, Lcom/android/launcher2/ik;->b:I

    iput v0, p3, Lcom/android/launcher2/il;->d:I

    .line 140
    :cond_2c
    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    .line 141
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1, p3}, Lcom/android/launcher2/im;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 143
    const/4 v0, 0x1

    .line 145
    :goto_36
    return v0

    :cond_37
    const/4 v0, 0x0

    goto :goto_36
.end method

.method public final b(I)I
    .registers 3
    .parameter

    .prologue
    .line 440
    iget v0, p0, Lcom/android/launcher2/ik;->d:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public final b()V
    .registers 5

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0}, Lcom/android/launcher2/im;->getChildCount()I

    move-result v1

    .line 166
    const/4 v0, 0x0

    :goto_7
    if-lt v0, v1, :cond_a

    .line 169
    return-void

    .line 167
    :cond_a
    iget-object v2, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/im;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_7
.end method

.method public final cancelLongPress()V
    .registers 4

    .prologue
    .line 117
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 120
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getChildCount()I

    move-result v1

    .line 121
    const/4 v0, 0x0

    :goto_8
    if-lt v0, v1, :cond_b

    .line 125
    return-void

    .line 122
    :cond_b
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ik;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter

    .prologue
    .line 450
    instance-of v0, p1, Lcom/android/launcher2/il;

    return v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter

    .prologue
    .line 445
    new-instance v0, Lcom/android/launcher2/il;

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/launcher2/il;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter

    .prologue
    .line 455
    new-instance v0, Lcom/android/launcher2/il;

    invoke-direct {v0, p1}, Lcom/android/launcher2/il;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final getCellCountX()I
    .registers 2

    .prologue
    .line 191
    iget v0, p0, Lcom/android/launcher2/ik;->a:I

    return v0
.end method

.method public final getCellCountY()I
    .registers 2

    .prologue
    .line 195
    iget v0, p0, Lcom/android/launcher2/ik;->b:I

    return v0
.end method

.method public final getCellHeight()I
    .registers 2

    .prologue
    .line 101
    iget v0, p0, Lcom/android/launcher2/ik;->d:I

    return v0
.end method

.method public final getCellWidth()I
    .registers 2

    .prologue
    .line 97
    iget v0, p0, Lcom/android/launcher2/ik;->c:I

    return v0
.end method

.method public final getChildrenLayout()Lcom/android/launcher2/im;
    .registers 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    return-object v0
.end method

.method final getContentHeight()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 299
    iget v1, p0, Lcom/android/launcher2/ik;->b:I

    if-lez v1, :cond_16

    .line 300
    iget v1, p0, Lcom/android/launcher2/ik;->b:I

    iget v2, p0, Lcom/android/launcher2/ik;->d:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/launcher2/ik;->b:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/launcher2/ik;->f:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 302
    :cond_16
    return v0
.end method

.method final getContentWidth()I
    .registers 3

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getWidthBeforeFirstLayout()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getDesiredHeight()I
    .registers 4

    .prologue
    .line 580
    iget v0, p0, Lcom/android/launcher2/ik;->i:I

    iget v1, p0, Lcom/android/launcher2/ik;->d:I

    mul-int/2addr v0, v1

    .line 581
    iget v1, p0, Lcom/android/launcher2/ik;->i:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/ik;->f:I

    mul-int/2addr v1, v2

    .line 580
    add-int/2addr v0, v1

    return v0
.end method

.method public final getPageChildCount()I
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0}, Lcom/android/launcher2/im;->getChildCount()I

    move-result v0

    return v0
.end method

.method final getWidthBeforeFirstLayout()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 306
    iget v1, p0, Lcom/android/launcher2/ik;->a:I

    if-lez v1, :cond_16

    .line 307
    iget v1, p0, Lcom/android/launcher2/ik;->a:I

    iget v2, p0, Lcom/android/launcher2/ik;->c:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/launcher2/ik;->a:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/launcher2/ik;->e:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 309
    :cond_16
    return v0
.end method

.method protected final onLayout(ZIIII)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getChildCount()I

    move-result v1

    .line 315
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_8

    .line 320
    return-void

    .line 316
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ik;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 317
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingTop()I

    move-result v4

    .line 318
    sub-int v5, p4, p2

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v6, p5, p3

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    .line 317
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method protected final onMeasure(II)V
    .registers 16
    .parameter
    .parameter

    .prologue
    const/high16 v12, 0x4000

    const/4 v1, 0x0

    .line 199
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 200
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 202
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 203
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 205
    sget v5, Landroid/view/View$MeasureSpec;->UNSPECIFIED:I

    if-eq v4, v5, :cond_1b

    sget v5, Landroid/view/View$MeasureSpec;->UNSPECIFIED:I

    if-ne v0, v5, :cond_23

    .line 206
    :cond_1b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_23
    iget v0, p0, Lcom/android/launcher2/ik;->a:I

    add-int/lit8 v0, v0, -0x1

    .line 209
    iget v5, p0, Lcom/android/launcher2/ik;->b:I

    add-int/lit8 v5, v5, -0x1

    .line 211
    iget v6, p0, Lcom/android/launcher2/ik;->mPaddingLeft:I

    sub-int v6, v3, v6

    iget v7, p0, Lcom/android/launcher2/ik;->mPaddingRight:I

    sub-int/2addr v6, v7

    .line 212
    iget v7, p0, Lcom/android/launcher2/ik;->mPaddingTop:I

    sub-int v7, v2, v7

    iget v8, p0, Lcom/android/launcher2/ik;->mPaddingBottom:I

    sub-int/2addr v7, v8

    .line 214
    iget v8, p0, Lcom/android/launcher2/ik;->a:I

    iget v9, p0, Lcom/android/launcher2/ik;->j:I

    mul-int/2addr v8, v9

    sub-int v8, v6, v8

    .line 215
    iget v9, p0, Lcom/android/launcher2/ik;->b:I

    iget v10, p0, Lcom/android/launcher2/ik;->k:I

    mul-int/2addr v9, v10

    sub-int v9, v7, v9

    .line 217
    iget v10, p0, Lcom/android/launcher2/ik;->a:I

    if-lez v10, :cond_5d

    .line 218
    iget v10, p0, Lcom/android/launcher2/ik;->a:I

    div-int v10, v6, v10

    iget v11, p0, Lcom/android/launcher2/ik;->j:I

    if-le v10, v11, :cond_c1

    .line 219
    if-lez v0, :cond_bf

    div-int v0, v8, v0

    :goto_57
    iput v0, p0, Lcom/android/launcher2/ik;->e:I

    .line 220
    iget v0, p0, Lcom/android/launcher2/ik;->j:I

    iput v0, p0, Lcom/android/launcher2/ik;->c:I

    .line 226
    :cond_5d
    :goto_5d
    iget v0, p0, Lcom/android/launcher2/ik;->b:I

    if-lez v0, :cond_73

    .line 227
    iget v0, p0, Lcom/android/launcher2/ik;->b:I

    div-int v0, v7, v0

    iget v6, p0, Lcom/android/launcher2/ik;->k:I

    if-le v0, v6, :cond_cc

    .line 228
    if-lez v5, :cond_ca

    div-int v0, v9, v5

    :goto_6d
    iput v0, p0, Lcom/android/launcher2/ik;->f:I

    .line 229
    iget v0, p0, Lcom/android/launcher2/ik;->k:I

    iput v0, p0, Lcom/android/launcher2/ik;->d:I

    .line 235
    :cond_73
    :goto_73
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    iget v5, p0, Lcom/android/launcher2/ik;->c:I

    iget v6, p0, Lcom/android/launcher2/ik;->d:I

    invoke-virtual {v0, v5, v6}, Lcom/android/launcher2/im;->b(II)V

    .line 236
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    iget v5, p0, Lcom/android/launcher2/ik;->e:I

    iget v6, p0, Lcom/android/launcher2/ik;->f:I

    invoke-virtual {v0, v5, v6}, Lcom/android/launcher2/im;->a(II)V

    .line 254
    iget-boolean v0, p0, Lcom/android/launcher2/ik;->m:Z

    if-eqz v0, :cond_d5

    .line 256
    iget v0, p0, Lcom/android/launcher2/ik;->mPaddingLeft:I

    sub-int v0, v3, v0

    .line 257
    iget v1, p0, Lcom/android/launcher2/ik;->mPaddingRight:I

    .line 256
    sub-int/2addr v0, v1

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 258
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getDesiredHeight()I

    move-result v0

    iget-object v4, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v4}, Lcom/android/launcher2/im;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v4}, Lcom/android/launcher2/im;->getPaddingBottom()I

    move-result v4

    add-int/2addr v0, v4

    .line 260
    if-ge v0, v2, :cond_a9

    move v0, v2

    .line 264
    :cond_a9
    iget-object v4, p0, Lcom/android/launcher2/ik;->h:Landroid/widget/ScrollView;

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v1, v5}, Landroid/widget/ScrollView;->measure(II)V

    .line 265
    iget-object v4, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v4, v1, v0}, Lcom/android/launcher2/im;->measure(II)V

    .line 291
    :goto_bb
    invoke-virtual {p0, v3, v2}, Lcom/android/launcher2/ik;->setMeasuredDimension(II)V

    .line 292
    return-void

    :cond_bf
    move v0, v1

    .line 219
    goto :goto_57

    .line 222
    :cond_c1
    iput v1, p0, Lcom/android/launcher2/ik;->e:I

    .line 223
    iget v0, p0, Lcom/android/launcher2/ik;->a:I

    div-int v0, v6, v0

    iput v0, p0, Lcom/android/launcher2/ik;->c:I

    goto :goto_5d

    :cond_ca
    move v0, v1

    .line 228
    goto :goto_6d

    .line 231
    :cond_cc
    iput v1, p0, Lcom/android/launcher2/ik;->f:I

    .line 232
    iget v0, p0, Lcom/android/launcher2/ik;->b:I

    div-int v0, v7, v0

    iput v0, p0, Lcom/android/launcher2/ik;->d:I

    goto :goto_73

    .line 267
    :cond_d5
    const/high16 v0, -0x8000

    if-ne v4, v0, :cond_144

    .line 268
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/android/launcher2/ik;->a:I

    iget v3, p0, Lcom/android/launcher2/ik;->c:I

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 269
    iget v2, p0, Lcom/android/launcher2/ik;->a:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/launcher2/ik;->e:I

    mul-int/2addr v2, v3

    .line 268
    add-int/2addr v0, v2

    .line 270
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/ik;->b:I

    iget v4, p0, Lcom/android/launcher2/ik;->d:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 271
    iget v3, p0, Lcom/android/launcher2/ik;->b:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/android/launcher2/ik;->f:I

    mul-int/2addr v3, v4

    .line 270
    add-int/2addr v2, v3

    .line 272
    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/ik;->setMeasuredDimension(II)V

    .line 275
    :goto_10a
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getSuggestedMinimumHeight()I

    move-result v3

    if-ge v2, v3, :cond_114

    .line 276
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getSuggestedMinimumHeight()I

    move-result v2

    .line 279
    :cond_114
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getChildCount()I

    move-result v3

    .line 280
    :goto_118
    if-lt v1, v3, :cond_11c

    move v3, v0

    goto :goto_bb

    .line 281
    :cond_11c
    invoke-virtual {p0, v1}, Lcom/android/launcher2/ik;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 283
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingLeft()I

    move-result v5

    sub-int v5, v0, v5

    .line 284
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingRight()I

    move-result v6

    .line 283
    sub-int/2addr v5, v6

    invoke-static {v5, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 286
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingTop()I

    move-result v6

    sub-int v6, v2, v6

    .line 287
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPaddingBottom()I

    move-result v7

    .line 286
    sub-int/2addr v6, v7

    invoke-static {v6, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 288
    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_118

    :cond_144
    move v0, v3

    goto :goto_10a
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter

    .prologue
    .line 324
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 325
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPageChildCount()I

    move-result v1

    .line 326
    if-lez v1, :cond_3e

    .line 328
    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/im;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 329
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 330
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getPageChildCount()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getCellCountX()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 331
    invoke-virtual {p0}, Lcom/android/launcher2/ik;->getCellCountY()I

    move-result v3

    if-ge v2, v3, :cond_32

    .line 333
    iget v2, p0, Lcom/android/launcher2/ik;->d:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 335
    :cond_32
    if-nez v0, :cond_3f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_3f

    const/4 v0, 0x0

    .line 337
    :cond_3e
    :goto_3e
    return v0

    .line 335
    :cond_3f
    const/4 v0, 0x1

    goto :goto_3e
.end method

.method protected final setChildrenDrawingCacheEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/im;->setChildrenDrawingCacheEnabled(Z)V

    .line 347
    return-void
.end method

.method protected final setChildrenDrawnWithCacheEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 586
    iget-object v0, p0, Lcom/android/launcher2/ik;->g:Lcom/android/launcher2/im;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/im;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 587
    return-void
.end method
