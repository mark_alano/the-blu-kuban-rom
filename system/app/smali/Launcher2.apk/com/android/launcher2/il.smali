.class public final Lcom/android/launcher2/il;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field e:I

.field f:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 499
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 500
    iput v1, p0, Lcom/android/launcher2/il;->c:I

    .line 501
    iput v1, p0, Lcom/android/launcher2/il;->d:I

    .line 502
    return-void
.end method

.method public constructor <init>(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 525
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 526
    iput p1, p0, Lcom/android/launcher2/il;->a:I

    .line 527
    iput p2, p0, Lcom/android/launcher2/il;->b:I

    .line 528
    iput v1, p0, Lcom/android/launcher2/il;->c:I

    .line 529
    iput v1, p0, Lcom/android/launcher2/il;->d:I

    .line 530
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 505
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 506
    iput v0, p0, Lcom/android/launcher2/il;->c:I

    .line 507
    iput v0, p0, Lcom/android/launcher2/il;->d:I

    .line 508
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 511
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 512
    iput v0, p0, Lcom/android/launcher2/il;->c:I

    .line 513
    iput v0, p0, Lcom/android/launcher2/il;->d:I

    .line 514
    return-void
.end method


# virtual methods
.method public final a(IIIIII)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 535
    iget v0, p0, Lcom/android/launcher2/il;->c:I

    .line 536
    iget v1, p0, Lcom/android/launcher2/il;->d:I

    .line 537
    iget v2, p0, Lcom/android/launcher2/il;->a:I

    .line 538
    iget v3, p0, Lcom/android/launcher2/il;->b:I

    .line 540
    mul-int v4, v0, p1

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, p3

    add-int/2addr v0, v4

    .line 541
    iget v4, p0, Lcom/android/launcher2/il;->leftMargin:I

    sub-int/2addr v0, v4

    iget v4, p0, Lcom/android/launcher2/il;->rightMargin:I

    sub-int/2addr v0, v4

    .line 540
    iput v0, p0, Lcom/android/launcher2/il;->width:I

    .line 542
    mul-int v0, v1, p2

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p4

    add-int/2addr v0, v1

    .line 543
    iget v1, p0, Lcom/android/launcher2/il;->topMargin:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/il;->bottomMargin:I

    sub-int/2addr v0, v1

    .line 542
    iput v0, p0, Lcom/android/launcher2/il;->height:I

    .line 546
    add-int v0, p1, p3

    mul-int/2addr v0, v2

    add-int/2addr v0, p5

    iget v1, p0, Lcom/android/launcher2/il;->leftMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/il;->e:I

    .line 547
    add-int v0, p2, p4

    mul-int/2addr v0, v3

    add-int/2addr v0, p6

    iget v1, p0, Lcom/android/launcher2/il;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/il;->f:I

    .line 552
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/launcher2/il;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/il;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 564
    iget v1, p0, Lcom/android/launcher2/il;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/il;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
