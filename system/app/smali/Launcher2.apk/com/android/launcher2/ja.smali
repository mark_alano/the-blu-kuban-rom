.class public final Lcom/android/launcher2/ja;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private final a:[I

.field private final b:Landroid/app/WallpaperManager;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/ja;->a:[I

    .line 48
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/ja;->b:Landroid/app/WallpaperManager;

    .line 49
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/ja;->g:Lcom/android/launcher2/Launcher;

    .line 50
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 11

    .prologue
    const/4 v4, 0x0

    .line 198
    iget-object v0, p0, Lcom/android/launcher2/ja;->g:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aY:Z

    if-eqz v0, :cond_2a

    .line 199
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 200
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v6

    move v3, v4

    .line 201
    :goto_13
    if-lt v3, v6, :cond_2b

    .line 214
    new-instance v0, Lcom/android/launcher2/jc;

    invoke-direct {v0, p0}, Lcom/android/launcher2/jc;-><init>(Lcom/android/launcher2/ja;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 215
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->removeAllViews()V

    .line 216
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_64

    .line 220
    :cond_2a
    return-void

    .line 202
    :cond_2b
    invoke-virtual {p0, v3}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 203
    new-instance v7, Lcom/android/launcher2/jb;

    invoke-direct {v7, p0, v4}, Lcom/android/launcher2/jb;-><init>(Lcom/android/launcher2/ja;B)V

    .line 204
    iput-object v0, v7, Lcom/android/launcher2/jb;->a:Landroid/view/View;

    .line 205
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 206
    const-wide/16 v1, 0x0

    .line 207
    instance-of v8, v0, Lcom/android/launcher2/di;

    if-eqz v8, :cond_70

    .line 208
    check-cast v0, Lcom/android/launcher2/di;

    .line 209
    iget-object v1, p0, Lcom/android/launcher2/ja;->g:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-wide v8, v0, Lcom/android/launcher2/di;->h:J

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "index_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/preference/bc;->e(Ljava/lang/String;)J

    move-result-wide v0

    .line 211
    :goto_5b
    iput-wide v0, v7, Lcom/android/launcher2/jb;->b:J

    .line 212
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_13

    .line 216
    :cond_64
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jb;

    .line 217
    iget-object v0, v0, Lcom/android/launcher2/jb;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/ja;->addView(Landroid/view/View;)V

    goto :goto_24

    :cond_70
    move-wide v0, v1

    goto :goto_5b
.end method

.method public final a(IIII)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    iput p1, p0, Lcom/android/launcher2/ja;->c:I

    .line 58
    iput p2, p0, Lcom/android/launcher2/ja;->d:I

    .line 59
    iput p3, p0, Lcom/android/launcher2/ja;->e:I

    .line 60
    iput p4, p0, Lcom/android/launcher2/ja;->f:I

    .line 61
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    const/high16 v5, 0x4000

    .line 112
    iget v1, p0, Lcom/android/launcher2/ja;->c:I

    .line 113
    iget v2, p0, Lcom/android/launcher2/ja;->d:I

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 116
    iget v3, p0, Lcom/android/launcher2/ja;->e:I

    iget v4, p0, Lcom/android/launcher2/ja;->f:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/CellLayout$LayoutParams;->a(IIII)V

    .line 117
    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 118
    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 120
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 121
    return-void
.end method

.method public final cancelLongPress()V
    .registers 4

    .prologue
    .line 166
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 169
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v1

    .line 170
    const/4 v0, 0x0

    :goto_8
    if-lt v0, v1, :cond_b

    .line 174
    return-void

    .line 171
    :cond_b
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 172
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 93
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v8

    .line 126
    const/4 v0, 0x0

    move v7, v0

    :goto_6
    if-lt v7, v8, :cond_9

    .line 147
    return-void

    .line 127
    :cond_9
    invoke-virtual {p0, v7}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_54

    .line 129
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 131
    iget v3, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 132
    iget v5, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 133
    iget v0, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    add-int/2addr v0, v3

    iget v2, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    add-int/2addr v2, v5

    invoke-virtual {v1, v3, v5, v0, v2}, Landroid/view/View;->layout(IIII)V

    .line 135
    iget-boolean v0, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->l:Z

    if-eqz v0, :cond_54

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->l:Z

    .line 138
    iget-object v6, p0, Lcom/android/launcher2/ja;->a:[I

    .line 139
    invoke-virtual {p0, v6}, Lcom/android/launcher2/ja;->getLocationOnScreen([I)V

    .line 140
    iget-object v0, p0, Lcom/android/launcher2/ja;->b:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 141
    const-string v2, "android.home.drop"

    .line 142
    const/4 v9, 0x0

    aget v9, v6, v9

    add-int/2addr v3, v9

    iget v9, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v3, v9

    .line 143
    const/4 v9, 0x1

    aget v6, v6, v9

    add-int/2addr v5, v6

    iget v4, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 140
    invoke-virtual/range {v0 .. v6}, Landroid/app/WallpaperManager;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V

    .line 126
    :cond_54
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_6
.end method

.method protected final onMeasure(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v1

    .line 98
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_13

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 103
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 104
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/ja;->setMeasuredDimension(II)V

    .line 105
    return-void

    .line 99
    :cond_13
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 100
    invoke-virtual {p0, v2}, Lcom/android/launcher2/ja;->a(Landroid/view/View;)V

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 157
    if-eqz p1, :cond_10

    .line 158
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 159
    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 160
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ja;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    .line 162
    :cond_10
    return-void
.end method

.method protected final setChildrenDrawingCacheEnabled(Z)V
    .registers 6
    .parameter

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v1

    .line 179
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_8

    .line 187
    return-void

    .line 180
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 181
    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 183
    invoke-virtual {v2}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v3

    if-nez v3, :cond_1b

    if-eqz p1, :cond_1b

    .line 184
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 179
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method protected final setChildrenDrawnWithCacheEnabled(Z)V
    .registers 2
    .parameter

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 192
    return-void
.end method

.method public final setupLp(Lcom/android/launcher2/CellLayout$LayoutParams;)V
    .registers 6
    .parameter

    .prologue
    .line 108
    iget v0, p0, Lcom/android/launcher2/ja;->c:I

    iget v1, p0, Lcom/android/launcher2/ja;->d:I

    iget v2, p0, Lcom/android/launcher2/ja;->e:I

    iget v3, p0, Lcom/android/launcher2/ja;->f:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/android/launcher2/CellLayout$LayoutParams;->a(IIII)V

    .line 109
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .registers 2

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method
