.class public final Lcom/android/launcher2/jd;
.super Lcom/android/launcher2/di;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/content/Intent;

.field c:Z

.field d:Z

.field e:Landroid/content/Intent$ShortcutIconResource;

.field f:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/launcher2/di;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/launcher2/jd;->i:I

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/h;)V
    .registers 4
    .parameter

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/launcher2/di;-><init>(Lcom/android/launcher2/di;)V

    .line 85
    iget-object v0, p1, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 86
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p1, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/jd;->c:Z

    .line 88
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_7

    .line 96
    invoke-virtual {p0, p1}, Lcom/android/launcher2/jd;->b(Lcom/android/launcher2/da;)V

    .line 98
    :cond_7
    iget-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method final a(Landroid/content/ContentValues;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 133
    invoke-super {p0, p1}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;)V

    .line 135
    iget-object v0, p0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    :goto_f
    const-string v2, "title"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    :cond_1e
    const-string v0, "intent"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-boolean v0, p0, Lcom/android/launcher2/jd;->c:Z

    if-eqz v0, :cond_39

    .line 142
    const-string v0, "iconType"

    .line 143
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 142
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 144
    iget-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/android/launcher2/jd;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 158
    :cond_36
    :goto_36
    return-void

    :cond_37
    move-object v0, v1

    .line 135
    goto :goto_f

    .line 146
    :cond_39
    iget-boolean v0, p0, Lcom/android/launcher2/jd;->d:Z

    if-nez v0, :cond_42

    .line 147
    iget-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/android/launcher2/jd;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 149
    :cond_42
    const-string v0, "iconType"

    .line 150
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 149
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    iget-object v0, p0, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    if-eqz v0, :cond_36

    .line 152
    const-string v0, "iconPackage"

    .line 153
    iget-object v1, p0, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    .line 152
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "iconResource"

    .line 155
    iget-object v1, p0, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    .line 154
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_36
.end method

.method public final b(Lcom/android/launcher2/da;)V
    .registers 4
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Lcom/android/launcher2/da;->a(Landroid/content/Intent;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    .line 113
    iget-object v0, p0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    iget-object v1, p1, Lcom/android/launcher2/da;->a:Landroid/graphics/Bitmap;

    if-ne v1, v0, :cond_12

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lcom/android/launcher2/jd;->d:Z

    .line 114
    return-void

    .line 113
    :cond_12
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 163
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ShortcutInfo(title="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 164
    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " container="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/launcher2/jd;->j:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 165
    const-string v1, " cellX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/jd;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 166
    const-string v1, " isGesture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/launcher2/jd;->r:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dropPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/jd;->s:[I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_9e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9e} :catch_a0

    move-result-object v0

    .line 168
    :goto_9f
    return-object v0

    :catch_a0
    move-exception v0

    const-string v0, "ShortcutInfo()"

    goto :goto_9f
.end method
