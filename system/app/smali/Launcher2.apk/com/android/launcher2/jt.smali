.class final Lcom/android/launcher2/jt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Workspace;

.field private final synthetic b:Landroid/view/ViewGroup;

.field private final synthetic c:Ljava/util/HashSet;

.field private final synthetic d:Lcom/android/launcher2/CellLayout;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Workspace;Landroid/view/ViewGroup;Ljava/util/HashSet;Lcom/android/launcher2/CellLayout;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/jt;->a:Lcom/android/launcher2/Workspace;

    iput-object p2, p0, Lcom/android/launcher2/jt;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/android/launcher2/jt;->c:Ljava/util/HashSet;

    iput-object p4, p0, Lcom/android/launcher2/jt;->d:Lcom/android/launcher2/CellLayout;

    .line 4034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 13

    .prologue
    const/4 v2, 0x0

    .line 4036
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 4037
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 4039
    iget-object v0, p0, Lcom/android/launcher2/jt;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v2

    .line 4040
    :goto_10
    if-lt v4, v6, :cond_26

    .line 4089
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 4090
    :goto_17
    if-lt v1, v3, :cond_cf

    .line 4100
    if-lez v3, :cond_25

    .line 4101
    iget-object v0, p0, Lcom/android/launcher2/jt;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 4102
    iget-object v0, p0, Lcom/android/launcher2/jt;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 4104
    :cond_25
    return-void

    .line 4041
    :cond_26
    iget-object v0, p0, Lcom/android/launcher2/jt;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4042
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 4044
    instance-of v3, v0, Lcom/android/launcher2/jd;

    if-eqz v3, :cond_5a

    .line 4045
    check-cast v0, Lcom/android/launcher2/jd;

    .line 4046
    iget-object v3, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 4047
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 4049
    if-eqz v3, :cond_56

    .line 4050
    iget-object v7, p0, Lcom/android/launcher2/jt;->c:Ljava/util/HashSet;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 4051
    iget-object v3, p0, Lcom/android/launcher2/jt;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v3}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 4052
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4040
    :cond_56
    :goto_56
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_10

    .line 4055
    :cond_5a
    instance-of v3, v0, Lcom/android/launcher2/cu;

    if-eqz v3, :cond_ac

    .line 4056
    check-cast v0, Lcom/android/launcher2/cu;

    .line 4057
    iget-object v7, v0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    .line 4058
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 4060
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 4062
    :goto_6c
    if-lt v3, v8, :cond_8b

    .line 4073
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_72
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_56

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/jd;

    .line 4074
    invoke-virtual {v0, v1}, Lcom/android/launcher2/cu;->b(Lcom/android/launcher2/di;)V

    .line 4075
    iget-object v7, p0, Lcom/android/launcher2/jt;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v7}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_72

    .line 4063
    :cond_8b
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/jd;

    .line 4064
    iget-object v10, v1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 4065
    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    .line 4067
    if-eqz v10, :cond_a8

    .line 4068
    iget-object v11, p0, Lcom/android/launcher2/jt;->c:Ljava/util/HashSet;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a8

    .line 4069
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4062
    :cond_a8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6c

    .line 4077
    :cond_ac
    instance-of v3, v0, Lcom/android/launcher2/fz;

    if-eqz v3, :cond_56

    .line 4078
    check-cast v0, Lcom/android/launcher2/fz;

    .line 4079
    iget-object v3, v0, Lcom/android/launcher2/fz;->b:Landroid/content/ComponentName;

    .line 4080
    if-eqz v3, :cond_56

    .line 4081
    iget-object v7, p0, Lcom/android/launcher2/jt;->c:Ljava/util/HashSet;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 4082
    iget-object v3, p0, Lcom/android/launcher2/jt;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v3}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 4083
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_56

    .line 4091
    :cond_cf
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 4094
    iget-object v2, p0, Lcom/android/launcher2/jt;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/CellLayout;->removeViewInLayout(Landroid/view/View;)V

    .line 4095
    instance-of v2, v0, Lcom/android/launcher2/bx;

    if-eqz v2, :cond_e9

    .line 4096
    iget-object v2, p0, Lcom/android/launcher2/jt;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v2}, Lcom/android/launcher2/Workspace;->l(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/bk;

    move-result-object v2

    check-cast v0, Lcom/android/launcher2/bx;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->b(Lcom/android/launcher2/bx;)V

    .line 4090
    :cond_e9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_17
.end method
