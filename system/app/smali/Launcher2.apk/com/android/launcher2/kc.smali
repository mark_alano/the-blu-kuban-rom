.class final Lcom/android/launcher2/kc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Workspace;

.field private final synthetic b:Lcom/android/launcher2/iu;

.field private final synthetic c:Lcom/android/launcher2/di;

.field private final synthetic d:J

.field private final synthetic e:I


# direct methods
.method constructor <init>(Lcom/android/launcher2/Workspace;Lcom/android/launcher2/iu;Lcom/android/launcher2/di;JI)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    iput-object p2, p0, Lcom/android/launcher2/kc;->b:Lcom/android/launcher2/iu;

    iput-object p3, p0, Lcom/android/launcher2/kc;->c:Lcom/android/launcher2/di;

    iput-wide p4, p0, Lcom/android/launcher2/kc;->d:J

    iput p6, p0, Lcom/android/launcher2/kc;->e:I

    .line 3398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3403
    iget-object v0, p0, Lcom/android/launcher2/kc;->b:Lcom/android/launcher2/iu;

    iget v0, v0, Lcom/android/launcher2/iu;->i:I

    sparse-switch v0, :sswitch_data_84

    .line 3423
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3424
    iget-object v2, p0, Lcom/android/launcher2/kc;->b:Lcom/android/launcher2/iu;

    iget v2, v2, Lcom/android/launcher2/iu;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3423
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3405
    :sswitch_23
    new-array v6, v3, [I

    .line 3406
    iget-object v0, p0, Lcom/android/launcher2/kc;->c:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->n:I

    aput v0, v6, v1

    .line 3407
    iget-object v0, p0, Lcom/android/launcher2/kc;->c:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->o:I

    aput v0, v6, v2

    .line 3408
    iget-object v0, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v0}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/kc;->b:Lcom/android/launcher2/iu;

    check-cast v1, Lcom/android/launcher2/iw;

    .line 3409
    iget-wide v2, p0, Lcom/android/launcher2/kc;->d:J

    iget v4, p0, Lcom/android/launcher2/kc;->e:I

    iget-object v5, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v5}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/Workspace;)[I

    move-result-object v5

    .line 3408
    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/iw;JI[I[I)V

    .line 3421
    :goto_48
    return-void

    .line 3412
    :sswitch_49
    iget-object v0, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v0}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/kc;->b:Lcom/android/launcher2/iu;

    iget-object v1, v1, Lcom/android/launcher2/iu;->a:Landroid/content/ComponentName;

    .line 3413
    iget-wide v2, p0, Lcom/android/launcher2/kc;->d:J

    iget v4, p0, Lcom/android/launcher2/kc;->e:I

    iget-object v5, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v5}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/Workspace;)[I

    move-result-object v5

    .line 3412
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/Launcher;->a(Landroid/content/ComponentName;JI[I)V

    goto :goto_48

    .line 3416
    :sswitch_61
    new-array v5, v3, [I

    .line 3417
    iget-object v0, p0, Lcom/android/launcher2/kc;->c:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->n:I

    aput v0, v5, v1

    .line 3418
    iget-object v0, p0, Lcom/android/launcher2/kc;->c:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->o:I

    aput v0, v5, v2

    .line 3419
    iget-object v0, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v0}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/launcher2/kc;->d:J

    .line 3420
    iget v3, p0, Lcom/android/launcher2/kc;->e:I

    iget-object v4, p0, Lcom/android/launcher2/kc;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v4}, Lcom/android/launcher2/Workspace;->f(Lcom/android/launcher2/Workspace;)[I

    move-result-object v4

    .line 3419
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/Launcher;->a(JI[I[I)V

    goto :goto_48

    .line 3403
    nop

    :sswitch_data_84
    .sparse-switch
        0x1 -> :sswitch_49
        0x4 -> :sswitch_23
        0x3e9 -> :sswitch_61
    .end sparse-switch
.end method
