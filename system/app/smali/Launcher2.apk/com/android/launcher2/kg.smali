.class final enum Lcom/android/launcher2/kg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/android/launcher2/kg;

.field public static final enum b:Lcom/android/launcher2/kg;

.field public static final enum c:Lcom/android/launcher2/kg;

.field private static final synthetic d:[Lcom/android/launcher2/kg;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 168
    new-instance v0, Lcom/android/launcher2/kg;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/kg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    new-instance v0, Lcom/android/launcher2/kg;

    const-string v1, "SPRING_LOADED"

    invoke-direct {v0, v1, v3}, Lcom/android/launcher2/kg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    new-instance v0, Lcom/android/launcher2/kg;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v4}, Lcom/android/launcher2/kg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/launcher2/kg;

    sget-object v1, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/launcher2/kg;->d:[Lcom/android/launcher2/kg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/launcher2/kg;
    .registers 2
    .parameter

    .prologue
    .line 1
    const-class v0, Lcom/android/launcher2/kg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/kg;

    return-object v0
.end method

.method public static values()[Lcom/android/launcher2/kg;
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/android/launcher2/kg;->d:[Lcom/android/launcher2/kg;

    array-length v1, v0

    new-array v2, v1, [Lcom/android/launcher2/kg;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
