.class public final Lcom/android/launcher2/kk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private final a:Landroid/view/animation/DecelerateInterpolator;

.field private final b:Lcom/android/launcher2/ki;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 1770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f40

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/kk;->a:Landroid/view/animation/DecelerateInterpolator;

    .line 1772
    new-instance v0, Lcom/android/launcher2/ki;

    const v1, 0x3e051eb8

    invoke-direct {v0, v1}, Lcom/android/launcher2/ki;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/kk;->b:Lcom/android/launcher2/ki;

    .line 1770
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .registers 4
    .parameter

    .prologue
    .line 1775
    iget-object v0, p0, Lcom/android/launcher2/kk;->a:Landroid/view/animation/DecelerateInterpolator;

    iget-object v1, p0, Lcom/android/launcher2/kk;->b:Lcom/android/launcher2/ki;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ki;->getInterpolation(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
