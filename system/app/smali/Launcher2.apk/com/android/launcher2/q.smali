.class final Lcom/android/launcher2/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/AppsCustomizePagedView;

.field private final synthetic b:I

.field private final synthetic c:I

.field private final synthetic d:Lcom/android/launcher2/in;

.field private final synthetic e:Z

.field private final synthetic f:I

.field private final synthetic g:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;IILcom/android/launcher2/in;ZILjava/util/ArrayList;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    iput p2, p0, Lcom/android/launcher2/q;->b:I

    iput p3, p0, Lcom/android/launcher2/q;->c:I

    iput-object p4, p0, Lcom/android/launcher2/q;->d:Lcom/android/launcher2/in;

    iput-boolean p5, p0, Lcom/android/launcher2/q;->e:Z

    iput p6, p0, Lcom/android/launcher2/q;->f:I

    iput-object p7, p0, Lcom/android/launcher2/q;->g:Ljava/util/ArrayList;

    .line 1433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1436
    iget v3, p0, Lcom/android/launcher2/q;->b:I

    .line 1437
    iget v4, p0, Lcom/android/launcher2/q;->c:I

    .line 1438
    iget-object v0, p0, Lcom/android/launcher2/q;->d:Lcom/android/launcher2/in;

    invoke-virtual {v0}, Lcom/android/launcher2/in;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1f

    .line 1439
    iget-object v0, p0, Lcom/android/launcher2/q;->d:Lcom/android/launcher2/in;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/in;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewWidget;

    .line 1440
    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewWidget;->getPreviewSize()[I

    move-result-object v0

    .line 1441
    aget v3, v0, v1

    .line 1442
    const/4 v1, 0x1

    aget v4, v0, v1

    .line 1444
    :cond_1f
    iget-boolean v0, p0, Lcom/android/launcher2/q;->e:Z

    if-eqz v0, :cond_38

    .line 1445
    new-instance v0, Lcom/android/launcher2/af;

    iget v1, p0, Lcom/android/launcher2/q;->f:I

    iget-object v2, p0, Lcom/android/launcher2/q;->g:Ljava/util/ArrayList;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/af;-><init>(ILjava/util/ArrayList;IILcom/android/launcher2/ae;Lcom/android/launcher2/ae;)V

    .line 1447
    iget-object v1, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1, v5, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/i;Lcom/android/launcher2/af;)V

    .line 1448
    iget-object v1, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/af;)V

    .line 1457
    :goto_37
    return-void

    .line 1450
    :cond_38
    iget-object v0, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->g(Lcom/android/launcher2/AppsCustomizePagedView;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 1451
    iget-object v0, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->h(Lcom/android/launcher2/AppsCustomizePagedView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_37

    .line 1453
    :cond_4a
    iget-object v0, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    iget v1, p0, Lcom/android/launcher2/q;->f:I

    iget-object v2, p0, Lcom/android/launcher2/q;->g:Ljava/util/ArrayList;

    .line 1454
    iget-object v5, p0, Lcom/android/launcher2/q;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v5}, Lcom/android/launcher2/AppsCustomizePagedView;->i(Lcom/android/launcher2/AppsCustomizePagedView;)I

    .line 1453
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;ILjava/util/ArrayList;II)V

    goto :goto_37
.end method
