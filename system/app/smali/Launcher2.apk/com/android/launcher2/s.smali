.class final Lcom/android/launcher2/s;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:I

.field c:I

.field d:I

.field e:I

.field final synthetic f:Lcom/android/launcher2/AppsCustomizePagedView;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;Ljava/util/ArrayList;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2600
    iput-object p1, p0, Lcom/android/launcher2/s;->f:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2601
    iput-object p2, p0, Lcom/android/launcher2/s;->a:Ljava/util/ArrayList;

    .line 2602
    iput p3, p0, Lcom/android/launcher2/s;->b:I

    .line 2603
    iput p4, p0, Lcom/android/launcher2/s;->c:I

    .line 2604
    invoke-virtual {p1}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, p3, v0

    .line 2605
    invoke-virtual {p1}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0062

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2604
    iput v0, p0, Lcom/android/launcher2/s;->d:I

    .line 2606
    invoke-virtual {p1}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, p4, v0

    iput v0, p0, Lcom/android/launcher2/s;->e:I

    .line 2607
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 2611
    iget-object v0, p0, Lcom/android/launcher2/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 2616
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher2/s;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 2617
    :cond_8
    const/4 v0, 0x0

    .line 2619
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_9
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 2624
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2629
    if-ltz p1, :cond_b

    invoke-virtual {p0}, Lcom/android/launcher2/s;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_c

    .line 2645
    :cond_b
    :goto_b
    return-object v5

    .line 2632
    :cond_c
    invoke-virtual {p0, p1}, Lcom/android/launcher2/s;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    .line 2633
    instance-of v0, v7, Lcom/android/launcher2/t;

    if-eqz v0, :cond_2f

    move-object v0, v7

    .line 2634
    check-cast v0, Lcom/android/launcher2/t;

    .line 2635
    new-instance v5, Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher2/s;->f:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Lcom/android/launcher2/AppsCustomizePagedView;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2636
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget v2, p0, Lcom/android/launcher2/s;->b:I

    iget v0, v0, Lcom/android/launcher2/t;->a:I

    invoke-direct {v1, v2, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b

    .line 2639
    :cond_2f
    iget-object v0, p0, Lcom/android/launcher2/s;->f:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v0, p2, v7, p3}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;Landroid/view/View;Ljava/lang/Object;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewWidget;

    move-result-object v8

    .line 2640
    invoke-virtual {v8}, Lcom/android/launcher2/PagedViewWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, p0, Lcom/android/launcher2/s;->b:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2641
    invoke-virtual {v8}, Lcom/android/launcher2/PagedViewWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, p0, Lcom/android/launcher2/s;->c:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2643
    invoke-virtual {v8}, Lcom/android/launcher2/PagedViewWidget;->getPreviewSize()[I

    move-result-object v4

    aget v0, v4, v1

    if-gtz v0, :cond_51

    iget v0, p0, Lcom/android/launcher2/s;->d:I

    aput v0, v4, v1

    :cond_51
    aget v0, v4, v6

    if-gtz v0, :cond_59

    iget v0, p0, Lcom/android/launcher2/s;->e:I

    aput v0, v4, v6

    :cond_59
    new-instance v0, Lcom/android/launcher2/af;

    iget-object v2, p0, Lcom/android/launcher2/s;->a:Ljava/util/ArrayList;

    aget v3, v4, v1

    aget v4, v4, v6

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/af;-><init>(ILjava/util/ArrayList;IILcom/android/launcher2/ae;Lcom/android/launcher2/ae;)V

    iget-object v1, p0, Lcom/android/launcher2/s;->f:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizePagedView;->m:Lcom/android/launcher2/u;

    iget v2, p0, Lcom/android/launcher2/s;->d:I

    invoke-virtual {v1, v8, v7, v0, v2}, Lcom/android/launcher2/u;->a(Lcom/android/launcher2/PagedViewWidget;Ljava/lang/Object;Lcom/android/launcher2/af;I)V

    move-object v5, v8

    .line 2645
    goto :goto_b
.end method
