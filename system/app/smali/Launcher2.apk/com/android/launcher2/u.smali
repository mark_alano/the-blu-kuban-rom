.class final Lcom/android/launcher2/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/HashMap;

.field b:Lcom/android/launcher2/x;

.field c:Lcom/android/launcher2/w;

.field final synthetic d:Lcom/android/launcher2/AppsCustomizePagedView;

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;)V
    .registers 3
    .parameter

    .prologue
    .line 2169
    iput-object p1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    .line 2172
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/u;->e:Ljava/lang/Object;

    .line 2173
    new-instance v0, Lcom/android/launcher2/x;

    invoke-direct {v0, p0}, Lcom/android/launcher2/x;-><init>(Lcom/android/launcher2/u;)V

    iput-object v0, p0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    .line 2174
    new-instance v0, Lcom/android/launcher2/w;

    invoke-direct {v0, p0}, Lcom/android/launcher2/w;-><init>(Lcom/android/launcher2/u;)V

    iput-object v0, p0, Lcom/android/launcher2/u;->c:Lcom/android/launcher2/w;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2169
    invoke-direct {p0, p1}, Lcom/android/launcher2/u;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;)V

    return-void
.end method

.method private a(Landroid/content/pm/ResolveInfo;II)Landroid/graphics/Bitmap;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2251
    :try_start_2
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizePagedView;->f:Lcom/android/launcher2/ah;

    invoke-virtual {v1}, Lcom/android/launcher2/ah;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 2252
    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v2, v2, Lcom/android/launcher2/AppsCustomizePagedView;->h:Lcom/android/launcher2/al;

    invoke-virtual {v2}, Lcom/android/launcher2/al;->b()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/Canvas;

    move-object v7, v0

    .line 2253
    if-eqz v1, :cond_26

    .line 2254
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, p2, :cond_26

    .line 2255
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, p3, :cond_c4

    .line 2256
    :cond_26
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2257
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizePagedView;->f:Lcom/android/launcher2/ah;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/ah;->a(Ljava/lang/Object;)V

    .line 2264
    :goto_33
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->c(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/da;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/launcher2/da;->a(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2267
    iget-object v3, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v3}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0069

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 2269
    iget-object v3, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v3}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b0067

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 2271
    iget-object v5, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v5}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0068

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 2273
    sub-int v6, p2, v3

    sub-int v5, v6, v5

    .line 2276
    iget-object v6, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    move v6, v5

    invoke-static/range {v1 .. v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    .line 2279
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 2280
    invoke-virtual {v7, v8}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2281
    iget-object v3, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v3, v3, Lcom/android/launcher2/AppsCustomizePagedView;->g:Lcom/android/launcher2/it;

    invoke-virtual {v3}, Lcom/android/launcher2/it;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Paint;

    .line 2282
    if-nez v3, :cond_a5

    .line 2283
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 2284
    new-instance v4, Landroid/graphics/ColorMatrix;

    invoke-direct {v4}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 2285
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 2286
    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v5, v4}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2287
    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2290
    iget-object v4, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v4, v4, Lcom/android/launcher2/AppsCustomizePagedView;->g:Lcom/android/launcher2/it;

    invoke-virtual {v4, v3}, Lcom/android/launcher2/it;->a(Ljava/lang/Object;)V

    .line 2292
    :cond_a5
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v7, v2, v4, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2293
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2295
    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v5

    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v6

    move-object v2, v8

    invoke-static/range {v1 .. v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    move-object v1, v8

    .line 2301
    :goto_c3
    return-object v1

    .line 2259
    :cond_c4
    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2260
    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2261
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_d1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_d1} :catch_d4

    move-object v2, v1

    goto/16 :goto_33

    .line 2299
    :catch_d4
    move-exception v1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2300
    invoke-virtual {v1, v9}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_c3
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;IIIII)Landroid/graphics/Bitmap;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2309
    :try_start_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 2310
    if-gez p6, :cond_9

    const p6, 0x7fffffff

    .line 2311
    :cond_9
    const/4 v1, 0x0

    .line 2314
    if-eqz p2, :cond_1b5

    .line 2315
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->e(Lcom/android/launcher2/AppsCustomizePagedView;)Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v5, p2, v2}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2316
    if-nez v1, :cond_3b

    .line 2317
    const-string v2, "AppsCustomizePagedView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t load widget preview drawable 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2318
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for provider: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2317
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3b
    move-object v10, v1

    .line 2324
    :goto_3c
    const/4 v2, 0x0

    .line 2325
    if-eqz v10, :cond_7a

    const/4 v1, 0x1

    move v9, v1

    .line 2326
    :goto_41
    if-eqz v9, :cond_7d

    .line 2327
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 2328
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    move-object v7, v2

    .line 2384
    :goto_4c
    const/high16 v1, 0x3f80

    .line 2385
    move/from16 v0, p6

    if-le v5, v0, :cond_57

    .line 2386
    move/from16 v0, p6

    int-to-float v1, v0

    int-to-float v2, v5

    div-float/2addr v1, v2

    .line 2388
    :cond_57
    const/high16 v2, 0x3f80

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_63

    .line 2389
    int-to-float v2, v5

    mul-float/2addr v2, v1

    float-to-int v5, v2

    .line 2390
    int-to-float v2, v6

    mul-float/2addr v1, v2

    float-to-int v6, v1

    .line 2392
    :cond_63
    if-gtz v5, :cond_66

    .line 2393
    const/4 v5, 0x1

    .line 2395
    :cond_66
    if-gtz v6, :cond_69

    .line 2396
    const/4 v6, 0x1

    .line 2399
    :cond_69
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2398
    invoke-static {v5, v6, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2402
    if-eqz v9, :cond_155

    .line 2403
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, v10

    invoke-static/range {v1 .. v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    .line 2426
    :goto_79
    return-object v2

    .line 2325
    :cond_7a
    const/4 v1, 0x0

    move v9, v1

    goto :goto_41

    .line 2331
    :cond_7d
    if-gtz p4, :cond_81

    const/16 p4, 0x1

    .line 2332
    :cond_81
    if-gtz p5, :cond_85

    const/16 p5, 0x1

    .line 2334
    :cond_85
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2335
    const v2, 0x7f02008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2334
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 2337
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v4

    .line 2339
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I
    :try_end_9b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_9b} :catch_140

    move-result v6

    .line 2340
    mul-int v8, v4, p4

    .line 2341
    mul-int v7, v6, p5

    .line 2345
    :try_start_a0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2344
    invoke-static {v8, v7, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_a5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a0 .. :try_end_a5} :catch_131

    move-result-object v2

    .line 2350
    :goto_a6
    :try_start_a6
    iget-object v3, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v3, v3, Lcom/android/launcher2/AppsCustomizePagedView;->i:Lcom/android/launcher2/al;

    invoke-virtual {v3}, Lcom/android/launcher2/al;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Canvas;

    .line 2351
    invoke-virtual {v3, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2352
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v11, v12, v8, v7}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 2353
    sget-object v11, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    .line 2354
    sget-object v12, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    .line 2353
    invoke-virtual {v1, v11, v12}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 2355
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 2356
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2359
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3e80

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 2360
    invoke-static {v8, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2361
    int-to-float v3, v3

    .line 2362
    iget-object v11, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v11}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v11

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v11

    int-to-float v1, v1

    .line 2361
    div-float v1, v3, v1

    .line 2362
    const/high16 v3, 0x3f80

    .line 2361
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F
    :try_end_e7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a6 .. :try_end_e7} :catch_140

    move-result v11

    .line 2365
    const/4 v1, 0x0

    .line 2367
    int-to-float v3, v4

    :try_start_ea
    iget-object v4, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v4}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v11

    sub-float/2addr v3, v4

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 2369
    int-to-float v4, v6

    iget-object v6, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v6}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v11

    sub-float/2addr v4, v6

    const/high16 v6, 0x4000

    div-float/2addr v4, v6

    float-to-int v4, v4

    .line 2370
    if-lez p3, :cond_113

    .line 2371
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->c(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/da;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v5, v0}, Lcom/android/launcher2/da;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2373
    :cond_113
    if-eqz v1, :cond_150

    .line 2374
    iget-object v5, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    .line 2375
    iget-object v5, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v5}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v11

    float-to-int v5, v5

    .line 2376
    iget-object v6, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v6}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Lcom/android/launcher2/AppsCustomizePagedView;)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v11

    float-to-int v6, v6

    .line 2374
    invoke-static/range {v1 .. v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    :try_end_12c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_ea .. :try_end_12c} :catch_14f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_ea .. :try_end_12c} :catch_140

    move v6, v7

    move v5, v8

    move-object v7, v2

    goto/16 :goto_4c

    .line 2347
    :catch_131
    move-exception v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    :try_start_134
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2348
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V
    :try_end_13e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_134 .. :try_end_13e} :catch_140

    goto/16 :goto_a6

    .line 2424
    :catch_140
    move-exception v1

    const/4 v1, 0x1

    const/4 v2, 0x1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2425
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto/16 :goto_79

    :catch_14f
    move-exception v1

    :cond_150
    move v6, v7

    move v5, v8

    move-object v7, v2

    goto/16 :goto_4c

    .line 2406
    :cond_155
    :try_start_155
    iget-object v1, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizePagedView;->i:Lcom/android/launcher2/al;

    invoke-virtual {v1}, Lcom/android/launcher2/al;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Canvas;

    .line 2407
    iget-object v3, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v3, v3, Lcom/android/launcher2/AppsCustomizePagedView;->j:Lcom/android/launcher2/iy;

    invoke-virtual {v3}, Lcom/android/launcher2/iy;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 2408
    iget-object v4, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v4, v4, Lcom/android/launcher2/AppsCustomizePagedView;->k:Lcom/android/launcher2/iy;

    invoke-virtual {v4}, Lcom/android/launcher2/iy;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 2409
    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2410
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v3, v5, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 2411
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v4, v5, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 2413
    iget-object v5, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v5, v5, Lcom/android/launcher2/AppsCustomizePagedView;->l:Lcom/android/launcher2/it;

    invoke-virtual {v5}, Lcom/android/launcher2/it;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Paint;

    .line 2414
    if-nez v5, :cond_1ac

    .line 2415
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2416
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2417
    iget-object v6, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v6, v6, Lcom/android/launcher2/AppsCustomizePagedView;->l:Lcom/android/launcher2/it;

    invoke-virtual {v6, v5}, Lcom/android/launcher2/it;->a(Ljava/lang/Object;)V

    .line 2419
    :cond_1ac
    invoke-virtual {v1, v7, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2420
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1b3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_155 .. :try_end_1b3} :catch_140

    goto/16 :goto_79

    :cond_1b5
    move-object v10, v1

    goto/16 :goto_3c
.end method

.method final a(Ljava/lang/Object;Lcom/android/launcher2/af;)Landroid/graphics/Bitmap;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2208
    const/4 v0, 0x0

    .line 2209
    instance-of v1, p1, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v1, :cond_46

    .line 2210
    check-cast p1, Landroid/appwidget/AppWidgetProviderInfo;

    .line 2211
    iget-object v0, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v0

    .line 2213
    iget v1, p2, Lcom/android/launcher2/af;->d:I

    .line 2214
    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/ik;

    move-result-object v2

    aget v3, v0, v4

    invoke-virtual {v2, v3}, Lcom/android/launcher2/ik;->a(I)I

    move-result v2

    .line 2213
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2215
    iget v1, p2, Lcom/android/launcher2/af;->e:I

    .line 2216
    iget-object v2, p0, Lcom/android/launcher2/u;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/ik;

    move-result-object v2

    aget v3, v0, v5

    invoke-virtual {v2, v3}, Lcom/android/launcher2/ik;->b(I)I

    move-result v2

    .line 2215
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 2217
    iget-object v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iget v3, p1, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    .line 2218
    aget v4, v0, v4

    aget v5, v0, v5

    move-object v0, p0

    .line 2217
    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/u;->a(Landroid/content/ComponentName;IIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2224
    :cond_45
    :goto_45
    return-object v0

    .line 2219
    :cond_46
    instance-of v1, p1, Landroid/content/pm/ResolveInfo;

    if-eqz v1, :cond_45

    .line 2221
    check-cast p1, Landroid/content/pm/ResolveInfo;

    .line 2222
    iget v0, p2, Lcom/android/launcher2/af;->d:I

    iget v1, p2, Lcom/android/launcher2/af;->e:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/launcher2/u;->a(Landroid/content/pm/ResolveInfo;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_45
.end method

.method final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter

    .prologue
    .line 2228
    iget-object v1, p0, Lcom/android/launcher2/u;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 2229
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 2230
    iget-object v0, p0, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 2231
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2232
    if-eqz v0, :cond_1d

    .line 2233
    monitor-exit v1

    .line 2238
    :goto_1c
    return-object v0

    .line 2235
    :cond_1d
    iget-object v0, p0, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238
    :cond_22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_25

    const/4 v0, 0x0

    goto :goto_1c

    .line 2228
    :catchall_25
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/android/launcher2/PagedViewWidget;Ljava/lang/Object;Lcom/android/launcher2/af;I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2187
    invoke-static {p2}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2188
    invoke-virtual {p0, v2}, Lcom/android/launcher2/u;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2189
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 2191
    :cond_10
    iget-object v3, p0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v4, v3, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    monitor-enter v4

    const/4 v0, 0x0

    move v1, v0

    :goto_17
    :try_start_17
    iget-object v0, v3, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lt v1, v0, :cond_4e

    monitor-exit v4
    :try_end_20
    .catchall {:try_start_17 .. :try_end_20} :catchall_60

    .line 2192
    new-instance v0, Lcom/android/launcher2/y;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/y;-><init>(Lcom/android/launcher2/u;Ljava/lang/String;Lcom/android/launcher2/PagedViewWidget;Ljava/lang/Object;Lcom/android/launcher2/af;I)V

    .line 2193
    iget-object v1, p0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v1, v1, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    monitor-enter v1

    .line 2194
    :try_start_2f
    iget-object v2, p0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v2, v2, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2195
    iget-object v0, p0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v0, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 2193
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_2f .. :try_end_3e} :catchall_67

    .line 2199
    iget-object v0, p0, Lcom/android/launcher2/u;->c:Lcom/android/launcher2/w;

    invoke-virtual {v0}, Lcom/android/launcher2/w;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_4d

    .line 2200
    iget-object v0, p0, Lcom/android/launcher2/u;->c:Lcom/android/launcher2/w;

    invoke-virtual {v0}, Lcom/android/launcher2/w;->start()V

    .line 2205
    :cond_4d
    :goto_4d
    return-void

    .line 2191
    :cond_4e
    :try_start_4e
    iget-object v0, v3, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/y;

    iget-object v0, v0, Lcom/android/launcher2/y;->b:Lcom/android/launcher2/PagedViewWidget;

    if-ne v0, p1, :cond_63

    iget-object v0, v3, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;
    :try_end_5f
    .catchall {:try_start_4e .. :try_end_5f} :catchall_60

    goto :goto_17

    :catchall_60
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 2193
    :catchall_67
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2203
    :cond_6a
    new-instance v1, Lcom/android/launcher2/ca;

    invoke-direct {v1, v0}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1, p4}, Lcom/android/launcher2/PagedViewWidget;->a(Lcom/android/launcher2/ca;I)V

    goto :goto_4d
.end method

.method final a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2243
    iget-object v1, p0, Lcom/android/launcher2/u;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 2244
    :try_start_3
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 2245
    iget-object v2, p0, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2243
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_f

    return-void

    :catchall_f
    move-exception v0

    monitor-exit v1

    throw v0
.end method
