.class final Lcom/google/android/apps/analytics/h;
.super Ljava/lang/Object;


# instance fields
.field final a:J

.field final b:Ljava/lang/String;

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:Z

.field k:Z

.field final l:Ljava/lang/String;

.field final m:Ljava/lang/String;

.field final n:Ljava/lang/String;

.field final o:I

.field final p:I

.field final q:I

.field r:Lcom/google/android/apps/analytics/e;

.field s:Lcom/google/android/apps/analytics/aa;

.field t:Lcom/google/android/apps/analytics/p;


# direct methods
.method constructor <init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .registers 16

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/analytics/h;->a:J

    iput-object p3, p0, Lcom/google/android/apps/analytics/h;->b:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/analytics/h;->c:I

    iput p5, p0, Lcom/google/android/apps/analytics/h;->e:I

    iput p6, p0, Lcom/google/android/apps/analytics/h;->f:I

    iput p7, p0, Lcom/google/android/apps/analytics/h;->g:I

    iput p8, p0, Lcom/google/android/apps/analytics/h;->h:I

    iput-object p9, p0, Lcom/google/android/apps/analytics/h;->l:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/analytics/h;->m:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/apps/analytics/h;->n:Ljava/lang/String;

    iput p12, p0, Lcom/google/android/apps/analytics/h;->o:I

    iput p14, p0, Lcom/google/android/apps/analytics/h;->q:I

    iput p13, p0, Lcom/google/android/apps/analytics/h;->p:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/analytics/h;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/h;->k:Z

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/analytics/h;Ljava/lang/String;)V
    .registers 19

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/apps/analytics/h;->a:J

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/analytics/h;->c:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/analytics/h;->e:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/analytics/h;->f:I

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/apps/analytics/h;->g:I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/apps/analytics/h;->h:I

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/apps/analytics/h;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/apps/analytics/h;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/analytics/h;->n:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v13, v0, Lcom/google/android/apps/analytics/h;->o:I

    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/analytics/h;->p:I

    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/android/apps/analytics/h;->q:I

    move-object/from16 v1, p0

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/analytics/h;-><init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/apps/analytics/h;->d:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/analytics/h;->d:I

    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/apps/analytics/h;->i:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/analytics/h;->i:I

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/google/android/apps/analytics/h;->j:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/analytics/h;->j:Z

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/google/android/apps/analytics/h;->k:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/analytics/h;->k:Z

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/h;->r:Lcom/google/android/apps/analytics/e;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/h;->r:Lcom/google/android/apps/analytics/e;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/h;->s:Lcom/google/android/apps/analytics/aa;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/h;->s:Lcom/google/android/apps/analytics/aa;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/h;->t:Lcom/google/android/apps/analytics/p;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/h;->t:Lcom/google/android/apps/analytics/p;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .registers 23

    const-wide/16 v1, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    move-object v0, p0

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/analytics/h;-><init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/analytics/h;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " random:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampCurrent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampPrevious:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampFirst:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " visits:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " category:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/h;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/h;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " label:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/h;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/h;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
