.class public Lorg/acra/ErrorReporter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field private static j:Z


# instance fields
.field private a:Z

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Ljava/util/List;

.field private final e:Lorg/acra/b/d;

.field private final f:Lorg/acra/i;

.field private final g:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private h:Ljava/lang/Thread;

.field private i:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 103
    const/4 v0, 0x1

    sput-boolean v0, Lorg/acra/ErrorReporter;->j:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-boolean v2, p0, Lorg/acra/ErrorReporter;->a:Z

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/acra/ErrorReporter;->d:Ljava/util/List;

    .line 89
    new-instance v0, Lorg/acra/i;

    invoke-direct {v0}, Lorg/acra/i;-><init>()V

    iput-object v0, p0, Lorg/acra/ErrorReporter;->f:Lorg/acra/i;

    .line 118
    iput-object p1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    .line 119
    iput-object p2, p0, Lorg/acra/ErrorReporter;->c:Landroid/content/SharedPreferences;

    .line 120
    iput-boolean p3, p0, Lorg/acra/ErrorReporter;->a:Z

    .line 123
    iget-object v0, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-static {v0}, Lorg/acra/b/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 128
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 129
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 131
    new-instance v4, Lorg/acra/b/d;

    iget-object v5, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-direct {v4, v5, p2, v3, v0}, Lorg/acra/b/d;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/text/format/Time;Ljava/lang/String;)V

    iput-object v4, p0, Lorg/acra/ErrorReporter;->e:Lorg/acra/b/d;

    .line 135
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 136
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 139
    iget-object v0, p0, Lorg/acra/ErrorReporter;->c:Landroid/content/SharedPreferences;

    const-string v3, "acra.lastVersionNr"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v3, v0

    new-instance v0, Lorg/acra/e/g;

    iget-object v5, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-direct {v0, v5}, Lorg/acra/e/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/acra/e/g;->a()Landroid/content/pm/PackageInfo;

    move-result-object v5

    if-eqz v5, :cond_ed

    iget v0, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v6, v0

    cmp-long v0, v6, v3

    if-lez v0, :cond_ed

    move v0, v1

    :goto_59
    if-eqz v0, :cond_78

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->f()Z

    move-result v0

    if-eqz v0, :cond_68

    invoke-direct {p0, v1, v2}, Lorg/acra/ErrorReporter;->a(ZI)V

    :cond_68
    iget-object v0, p0, Lorg/acra/ErrorReporter;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "acra.lastVersionNr"

    iget v4, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_78
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    sget-object v3, Lorg/acra/ReportingInteractionMode;->NOTIFICATION:Lorg/acra/ReportingInteractionMode;

    if-eq v0, v3, :cond_90

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    sget-object v3, Lorg/acra/ReportingInteractionMode;->DIALOG:Lorg/acra/ReportingInteractionMode;

    if-ne v0, v3, :cond_9d

    :cond_90
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->e()Z

    move-result v0

    if-eqz v0, :cond_9d

    invoke-virtual {p0, v1}, Lorg/acra/ErrorReporter;->b(Z)V

    :cond_9d
    new-instance v0, Lorg/acra/j;

    iget-object v1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/acra/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/acra/j;->a()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_ec

    array-length v1, v1

    if-lez v1, :cond_ec

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v1

    invoke-virtual {v0}, Lorg/acra/j;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->b([Ljava/lang/String;)Z

    move-result v3

    sget-object v4, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    if-eq v1, v4, :cond_cf

    sget-object v4, Lorg/acra/ReportingInteractionMode;->TOAST:Lorg/acra/ReportingInteractionMode;

    if-eq v1, v4, :cond_cf

    if-eqz v3, :cond_f0

    sget-object v4, Lorg/acra/ReportingInteractionMode;->NOTIFICATION:Lorg/acra/ReportingInteractionMode;

    if-eq v1, v4, :cond_cf

    sget-object v4, Lorg/acra/ReportingInteractionMode;->DIALOG:Lorg/acra/ReportingInteractionMode;

    if-ne v1, v4, :cond_f0

    :cond_cf
    sget-object v0, Lorg/acra/ReportingInteractionMode;->TOAST:Lorg/acra/ReportingInteractionMode;

    if-ne v1, v0, :cond_e2

    if-nez v3, :cond_e2

    iget-object v0, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->B()I

    move-result v1

    invoke-static {v0, v1}, Lorg/acra/e/i;->a(Landroid/content/Context;I)V

    :cond_e2
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "About to start ReportSenderWorker from #checkReportOnApplicationStart"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2, v2}, Lorg/acra/ErrorReporter;->a(ZZ)Lorg/acra/p;

    .line 140
    :cond_ec
    :goto_ec
    return-void

    :cond_ed
    move v0, v2

    .line 139
    goto/16 :goto_59

    :cond_f0
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v1

    sget-object v2, Lorg/acra/ReportingInteractionMode;->NOTIFICATION:Lorg/acra/ReportingInteractionMode;

    if-ne v1, v2, :cond_104

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->b(Ljava/lang/String;)V

    goto :goto_ec

    :cond_104
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v1

    sget-object v2, Lorg/acra/ReportingInteractionMode;->DIALOG:Lorg/acra/ReportingInteractionMode;

    if-ne v1, v2, :cond_ec

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/acra/ErrorReporter;->a(Ljava/lang/String;)V

    goto :goto_ec
.end method

.method static synthetic a(Lorg/acra/ErrorReporter;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a([Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 782
    if-eqz p1, :cond_22

    array-length v0, p1

    if-lez v0, :cond_22

    .line 783
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_1c

    .line 784
    iget-object v1, p0, Lorg/acra/ErrorReporter;->f:Lorg/acra/i;

    aget-object v1, p1, v0

    sget-object v2, Lorg/acra/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 785
    aget-object v0, p1, v0

    .line 791
    :goto_18
    return-object v0

    .line 783
    :cond_19
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 789
    :cond_1c
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_18

    .line 791
    :cond_22
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private a(Ljava/lang/Throwable;Lorg/acra/ReportingInteractionMode;ZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 531
    iget-boolean v0, p0, Lorg/acra/ErrorReporter;->a:Z

    if-nez v0, :cond_7

    .line 677
    :goto_6
    return-void

    .line 536
    :cond_7
    if-nez p2, :cond_cf

    .line 539
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object p2

    move v6, v1

    .line 551
    :goto_12
    if-nez p1, :cond_1b

    .line 552
    new-instance p1, Ljava/lang/Exception;

    const-string v0, "Report requested by developer"

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 555
    :cond_1b
    sget-object v0, Lorg/acra/ReportingInteractionMode;->TOAST:Lorg/acra/ReportingInteractionMode;

    if-eq p2, v0, :cond_31

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->B()I

    move-result v0

    if-eqz v0, :cond_e2

    sget-object v0, Lorg/acra/ReportingInteractionMode;->NOTIFICATION:Lorg/acra/ReportingInteractionMode;

    if-eq p2, v0, :cond_31

    sget-object v0, Lorg/acra/ReportingInteractionMode;->DIALOG:Lorg/acra/ReportingInteractionMode;

    if-ne p2, v0, :cond_e2

    :cond_31
    move v5, v3

    .line 558
    :goto_32
    if-eqz v5, :cond_3c

    .line 559
    new-instance v0, Lorg/acra/m;

    invoke-direct {v0, p0}, Lorg/acra/m;-><init>(Lorg/acra/ErrorReporter;)V

    invoke-virtual {v0}, Lorg/acra/m;->start()V

    .line 579
    :cond_3c
    iget-object v0, p0, Lorg/acra/ErrorReporter;->e:Lorg/acra/b/d;

    iget-object v2, p0, Lorg/acra/ErrorReporter;->h:Ljava/lang/Thread;

    invoke-virtual {v0, p1, p3, v2}, Lorg/acra/b/d;->a(Ljava/lang/Throwable;ZLjava/lang/Thread;)Lorg/acra/b/c;

    move-result-object v2

    .line 584
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v7

    sget-object v0, Lorg/acra/ReportField;->IS_SILENT:Lorg/acra/ReportField;

    invoke-virtual {v2, v0}, Lorg/acra/b/c;->a(Lorg/acra/ReportField;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_e5

    sget-object v0, Lorg/acra/e;->a:Ljava/lang/String;

    :goto_63
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ".stacktrace"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 585
    :try_start_71
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Writing crash report file "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lorg/acra/l;

    iget-object v7, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-direct {v0, v7}, Lorg/acra/l;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v4}, Lorg/acra/l;->a(Lorg/acra/b/c;Ljava/lang/String;)V
    :try_end_95
    .catch Ljava/lang/Exception; {:try_start_71 .. :try_end_95} :catch_e9

    .line 587
    :goto_95
    const/4 v2, 0x0

    .line 589
    sget-object v0, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    if-eq p2, v0, :cond_a8

    sget-object v0, Lorg/acra/ReportingInteractionMode;->TOAST:Lorg/acra/ReportingInteractionMode;

    if-eq p2, v0, :cond_a8

    iget-object v0, p0, Lorg/acra/ErrorReporter;->c:Landroid/content/SharedPreferences;

    const-string v7, "acra.alwaysaccept"

    invoke-interface {v0, v7, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_f2

    .line 594
    :cond_a8
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v2, "About to start ReportSenderWorker from #handleException"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    invoke-virtual {p0, v6, v3}, Lorg/acra/ErrorReporter;->a(ZZ)Lorg/acra/p;

    move-result-object v2

    .line 610
    :cond_b3
    :goto_b3
    if-eqz v5, :cond_bf

    .line 615
    sput-boolean v1, Lorg/acra/ErrorReporter;->j:Z

    .line 616
    new-instance v0, Lorg/acra/n;

    invoke-direct {v0, p0}, Lorg/acra/n;-><init>(Lorg/acra/ErrorReporter;)V

    invoke-virtual {v0}, Lorg/acra/n;->start()V

    .line 643
    :cond_bf
    sget-object v0, Lorg/acra/ReportingInteractionMode;->DIALOG:Lorg/acra/ReportingInteractionMode;

    if-ne p2, v0, :cond_101

    .line 646
    :goto_c3
    new-instance v0, Lorg/acra/o;

    move-object v1, p0

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/acra/o;-><init>(Lorg/acra/ErrorReporter;Lorg/acra/p;ZLjava/lang/String;Z)V

    invoke-virtual {v0}, Lorg/acra/o;->start()V

    goto/16 :goto_6

    .line 545
    :cond_cf
    sget-object v0, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    if-ne p2, v0, :cond_103

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    sget-object v2, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    if-eq v0, v2, :cond_103

    move v6, v3

    .line 547
    goto/16 :goto_12

    :cond_e2
    move v5, v1

    .line 555
    goto/16 :goto_32

    .line 584
    :cond_e5
    const-string v0, ""

    goto/16 :goto_63

    .line 585
    :catch_e9
    move-exception v0

    sget-object v2, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v7, "An error occurred while writing the report file..."

    invoke-static {v2, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_95

    .line 596
    :cond_f2
    sget-object v0, Lorg/acra/ReportingInteractionMode;->NOTIFICATION:Lorg/acra/ReportingInteractionMode;

    if-ne p2, v0, :cond_b3

    .line 598
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v6, "About to send status bar notification from #handleException"

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    invoke-direct {p0, v4}, Lorg/acra/ErrorReporter;->b(Ljava/lang/String;)V

    goto :goto_b3

    :cond_101
    move v3, v1

    .line 643
    goto :goto_c3

    :cond_103
    move v6, v1

    goto/16 :goto_12
.end method

.method private a(Lorg/acra/d/e;)V
    .registers 3
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, Lorg/acra/ErrorReporter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method private a(ZI)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 809
    new-instance v0, Lorg/acra/j;

    iget-object v1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/acra/j;-><init>(Landroid/content/Context;)V

    .line 810
    invoke-virtual {v0}, Lorg/acra/j;->a()[Ljava/lang/String;

    move-result-object v1

    .line 811
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 812
    if-eqz v1, :cond_4b

    .line 813
    const/4 v0, 0x0

    :goto_11
    array-length v2, v1

    sub-int/2addr v2, p2

    if-ge v0, v2, :cond_4b

    .line 814
    aget-object v2, v1, v0

    .line 815
    iget-object v3, p0, Lorg/acra/ErrorReporter;->f:Lorg/acra/i;

    invoke-static {v2}, Lorg/acra/i;->a(Ljava/lang/String;)Z

    move-result v3

    .line 816
    if-eqz v3, :cond_21

    if-nez p1, :cond_23

    :cond_21
    if-nez v3, :cond_48

    .line 817
    :cond_23
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 818
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_48

    .line 819
    sget-object v2, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not delete report : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :cond_48
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 824
    :cond_4b
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 10
    .parameter

    .prologue
    .line 710
    iget-object v0, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 713
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    .line 716
    invoke-interface {v1}, Lorg/acra/a/a;->x()I

    move-result v2

    .line 718
    iget-object v3, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-interface {v1}, Lorg/acra/a/a;->z()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 719
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 720
    new-instance v6, Landroid/app/Notification;

    invoke-direct {v6, v2, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 722
    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-interface {v1}, Lorg/acra/a/a;->A()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 723
    iget-object v3, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-interface {v1}, Lorg/acra/a/a;->y()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 725
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    const-class v5, Lorg/acra/f;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 726
    sget-object v4, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Creating Notification for "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const-string v4, "REPORT_FILE_NAME"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 728
    iget-object v4, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    const/4 v5, 0x0

    const/high16 v7, 0x800

    invoke-static {v4, v5, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 731
    iget-object v4, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v6, v4, v2, v1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 734
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 735
    const/16 v1, 0x29a

    invoke-virtual {v0, v1, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 736
    return-void
.end method

.method static synthetic b(Lorg/acra/ErrorReporter;)V
    .registers 4
    .parameter

    .prologue
    .line 75
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    sget-object v1, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    if-eq v0, v1, :cond_22

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    sget-object v1, Lorg/acra/ReportingInteractionMode;->TOAST:Lorg/acra/ReportingInteractionMode;

    if-ne v0, v1, :cond_2c

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->h()Z

    move-result v0

    if-eqz v0, :cond_2c

    :cond_22
    iget-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    iget-object v1, p0, Lorg/acra/ErrorReporter;->h:Ljava/lang/Thread;

    iget-object v2, p0, Lorg/acra/ErrorReporter;->i:Ljava/lang/Throwable;

    invoke-interface {v0, v1, v2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :goto_2b
    return-void

    :cond_2c
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fatal error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/acra/ErrorReporter;->i:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/acra/ErrorReporter;->i:Ljava/lang/Throwable;

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_2b
.end method

.method private b(Lorg/acra/d/e;)V
    .registers 2
    .parameter

    .prologue
    .line 272
    invoke-direct {p0}, Lorg/acra/ErrorReporter;->d()V

    .line 273
    invoke-direct {p0, p1}, Lorg/acra/ErrorReporter;->a(Lorg/acra/d/e;)V

    .line 274
    return-void
.end method

.method static synthetic b()Z
    .registers 1

    .prologue
    .line 75
    const/4 v0, 0x1

    sput-boolean v0, Lorg/acra/ErrorReporter;->j:Z

    return v0
.end method

.method private b([Ljava/lang/String;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 836
    array-length v2, p1

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_13

    aget-object v3, p1, v1

    .line 837
    iget-object v4, p0, Lorg/acra/ErrorReporter;->f:Lorg/acra/i;

    invoke-static {v3}, Lorg/acra/i;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 841
    :goto_f
    return v0

    .line 836
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 841
    :cond_13
    const/4 v0, 0x1

    goto :goto_f
.end method

.method static synthetic c()Z
    .registers 1

    .prologue
    .line 75
    sget-boolean v0, Lorg/acra/ErrorReporter;->j:Z

    return v0
.end method

.method private d()V
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lorg/acra/ErrorReporter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 262
    return-void
.end method


# virtual methods
.method final a(ZZ)Lorg/acra/p;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 380
    new-instance v0, Lorg/acra/p;

    iget-object v1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    iget-object v2, p0, Lorg/acra/ErrorReporter;->d:Ljava/util/List;

    invoke-direct {v0, v1, v2, p1, p2}, Lorg/acra/p;-><init>(Landroid/content/Context;Ljava/util/List;ZZ)V

    .line 381
    invoke-virtual {v0}, Lorg/acra/p;->start()V

    .line 382
    return-object v0
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 849
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    .line 850
    invoke-static {}, Lorg/acra/ACRA;->getApplication()Landroid/app/Application;

    move-result-object v1

    .line 851
    invoke-direct {p0}, Lorg/acra/ErrorReporter;->d()V

    .line 855
    const-string v2, ""

    invoke-interface {v0}, Lorg/acra/a/a;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3c

    .line 856
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reports will be sent by email (if accepted by user)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    new-instance v0, Lorg/acra/d/a;

    invoke-direct {v0, v1}, Lorg/acra/d/a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->b(Lorg/acra/d/e;)V

    .line 888
    :cond_3b
    :goto_3b
    return-void

    .line 861
    :cond_3c
    new-instance v2, Lorg/acra/e/g;

    invoke-direct {v2, v1}, Lorg/acra/e/g;-><init>(Landroid/content/Context;)V

    .line 862
    const-string v3, "android.permission.INTERNET"

    invoke-virtual {v2, v3}, Lorg/acra/e/g;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_66

    .line 868
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be granted permission android.permission.INTERNET if you want your crash reports to be sent. If you don\'t want to add this permission to your application you can also enable sending reports by email. If this is your will then provide your email address in @ReportsCrashes(mailTo=\"your.account@domain.com\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3b

    .line 878
    :cond_66
    invoke-interface {v0}, Lorg/acra/a/a;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_81

    const-string v1, ""

    invoke-interface {v0}, Lorg/acra/a/a;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_81

    .line 879
    new-instance v0, Lorg/acra/d/d;

    invoke-direct {v0}, Lorg/acra/d/d;-><init>()V

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->b(Lorg/acra/d/e;)V

    goto :goto_3b

    .line 885
    :cond_81
    invoke-interface {v0}, Lorg/acra/a/a;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3b

    const-string v1, ""

    invoke-interface {v0}, Lorg/acra/a/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 886
    new-instance v0, Lorg/acra/d/b;

    invoke-direct {v0}, Lorg/acra/d/b;-><init>()V

    invoke-direct {p0, v0}, Lorg/acra/ErrorReporter;->a(Lorg/acra/d/e;)V

    goto :goto_3b
.end method

.method final a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 688
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating Dialog for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    const-class v2, Lorg/acra/f;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 690
    const-string v1, "REPORT_FILE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 691
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 692
    iget-object v1, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 693
    return-void
.end method

.method public final a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 366
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "ACRA is "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_2b

    const-string v0, "enabled"

    :goto_d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iput-boolean p1, p0, Lorg/acra/ErrorReporter;->a:Z

    .line 368
    return-void

    .line 366
    :cond_2b
    const-string v0, "disabled"

    goto :goto_d
.end method

.method public addCustomData(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lorg/acra/ErrorReporter;->e:Lorg/acra/b/d;

    invoke-virtual {v0, p1, p2}, Lorg/acra/b/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 165
    return-void
.end method

.method final b(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 480
    if-eqz p1, :cond_8

    const/4 v0, 0x1

    .line 481
    :goto_4
    invoke-direct {p0, v1, v0}, Lorg/acra/ErrorReporter;->a(ZI)V

    .line 482
    return-void

    :cond_8
    move v0, v1

    .line 480
    goto :goto_4
.end method

.method public handleSilentException(Ljava/lang/Throwable;)V
    .registers 5
    .parameter

    .prologue
    .line 349
    iget-boolean v0, p0, Lorg/acra/ErrorReporter;->a:Z

    if-eqz v0, :cond_13

    .line 350
    sget-object v0, Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/acra/ErrorReporter;->a(Ljava/lang/Throwable;Lorg/acra/ReportingInteractionMode;ZZ)V

    .line 351
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "ACRA sent Silent report."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :goto_12
    return-void

    .line 355
    :cond_13
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "ACRA is disabled. Silent report not sent."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12
.end method

.method public putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, Lorg/acra/ErrorReporter;->e:Lorg/acra/b/d;

    invoke-virtual {v0, p1, p2}, Lorg/acra/b/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeCustomData(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lorg/acra/ErrorReporter;->e:Lorg/acra/b/d;

    iget-object v0, v0, Lorg/acra/b/d;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 287
    :try_start_0
    iget-boolean v0, p0, Lorg/acra/ErrorReporter;->a:Z

    if-nez v0, :cond_5a

    .line 288
    iget-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2e

    .line 289
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ACRA is disabled for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - forwarding uncaught Exception on to default ExceptionHandler"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 315
    :cond_2d
    :goto_2d
    return-void

    .line 293
    :cond_2e
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ACRA is disabled for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - no default ExceptionHandler"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_4e} :catch_4f

    goto :goto_2d

    .line 311
    :catch_4f
    move-exception v0

    iget-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2d

    .line 312
    iget-object v0, p0, Lorg/acra/ErrorReporter;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_2d

    .line 299
    :cond_5a
    :try_start_5a
    iput-object p1, p0, Lorg/acra/ErrorReporter;->h:Ljava/lang/Thread;

    .line 300
    iput-object p2, p0, Lorg/acra/ErrorReporter;->i:Ljava/lang/Throwable;

    .line 302
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ACRA caught a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/acra/ErrorReporter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Building report."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->q()Lorg/acra/ReportingInteractionMode;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Lorg/acra/ErrorReporter;->a(Ljava/lang/Throwable;Lorg/acra/ReportingInteractionMode;ZZ)V
    :try_end_9d
    .catch Ljava/lang/Throwable; {:try_start_5a .. :try_end_9d} :catch_4f

    goto :goto_2d
.end method
