.class public final Lorg/acra/d/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/acra/d/e;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lorg/acra/d/d;->a:Landroid/net/Uri;

    .line 95
    iput-object v0, p0, Lorg/acra/d/d;->b:Ljava/util/Map;

    .line 96
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    if-eqz p0, :cond_a

    const-string v0, "ACRA-NULL-STRING"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public final a(Lorg/acra/b/c;)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 121
    :try_start_1
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->d()[Lorg/acra/ReportField;

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_e5

    sget-object v1, Lorg/acra/ACRA;->DEFAULT_REPORT_FIELDS:[Lorg/acra/ReportField;

    move-object v2, v1

    :goto_f
    new-instance v3, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/HashMap;-><init>(I)V

    array-length v4, v2

    const/4 v1, 0x0

    :goto_1a
    if-ge v1, v4, :cond_4f

    aget-object v5, v2, v1

    iget-object v6, p0, Lorg/acra/d/d;->b:Ljava/util/Map;

    if-eqz v6, :cond_2a

    iget-object v6, p0, Lorg/acra/d/d;->b:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_38

    :cond_2a
    invoke-virtual {v5}, Lorg/acra/ReportField;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_35
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    :cond_38
    iget-object v6, p0, Lorg/acra/d/d;->b:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_45
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_45} :catch_46

    goto :goto_35

    .line 138
    :catch_46
    move-exception v0

    .line 139
    new-instance v1, Lorg/acra/d/f;

    const-string v2, "Error while sending report to Http Post Form."

    invoke-direct {v1, v2, v0}, Lorg/acra/d/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 122
    :cond_4f
    :try_start_4f
    iget-object v1, p0, Lorg/acra/d/d;->a:Landroid/net/Uri;

    if-nez v1, :cond_c6

    new-instance v1, Ljava/net/URL;

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v2

    invoke-virtual {v2}, Lorg/acra/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    .line 123
    :goto_61
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Connect to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/acra/d/d;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d3

    move-object v1, v0

    .line 127
    :goto_88
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v4

    invoke-virtual {v4}, Lorg/acra/c;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/acra/d/d;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_dc

    .line 130
    :goto_96
    new-instance v4, Lorg/acra/e/c;

    invoke-direct {v4}, Lorg/acra/e/c;-><init>()V

    .line 131
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v5

    invoke-virtual {v5}, Lorg/acra/c;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/acra/e/c;->a(I)V

    .line 132
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v5

    invoke-virtual {v5}, Lorg/acra/c;->E()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/acra/e/c;->b(I)V

    .line 133
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v5

    invoke-virtual {v5}, Lorg/acra/c;->p()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/acra/e/c;->c(I)V

    .line 134
    invoke-virtual {v4, v1}, Lorg/acra/e/c;->a(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v4, v0}, Lorg/acra/e/c;->b(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v4, v2, v3}, Lorg/acra/e/c;->a(Ljava/net/URL;Ljava/util/Map;)V

    .line 140
    return-void

    .line 122
    :cond_c6
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lorg/acra/d/d;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_61

    .line 125
    :cond_d3
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/acra/c;->k()Ljava/lang/String;

    move-result-object v1

    goto :goto_88

    .line 127
    :cond_dc
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->l()Ljava/lang/String;
    :try_end_e3
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_e3} :catch_46

    move-result-object v0

    goto :goto_96

    :cond_e5
    move-object v2, v1

    goto/16 :goto_f
.end method
