.class public final Lorg/acra/e/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 35
    const-class v1, Lorg/acra/e/e;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lorg/acra/e/e;->a:Ljava/lang/String;

    if-nez v0, :cond_35

    .line 36
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "ACRA-INSTALLATION"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_76

    .line 38
    :try_start_12
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2f

    .line 39
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_76
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_1d} :catch_3e
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_1d} :catch_5a

    :try_start_1d
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2c
    .catchall {:try_start_1d .. :try_end_2c} :catchall_39

    :try_start_2c
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 41
    :cond_2f
    invoke-static {v0}, Lorg/acra/e/e;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/acra/e/e;->a:Ljava/lang/String;
    :try_end_35
    .catchall {:try_start_2c .. :try_end_35} :catchall_76
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_35} :catch_3e
    .catch Ljava/lang/RuntimeException; {:try_start_2c .. :try_end_35} :catch_5a

    .line 50
    :cond_35
    :try_start_35
    sget-object v0, Lorg/acra/e/e;->a:Ljava/lang/String;
    :try_end_37
    .catchall {:try_start_35 .. :try_end_37} :catchall_76

    :goto_37
    monitor-exit v1

    return-object v0

    .line 39
    :catchall_39
    move-exception v0

    :try_start_3a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_3e
    .catchall {:try_start_3a .. :try_end_3e} :catchall_76
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3e} :catch_3e
    .catch Ljava/lang/RuntimeException; {:try_start_3a .. :try_end_3e} :catch_5a

    .line 42
    :catch_3e
    move-exception v0

    .line 43
    :try_start_3f
    sget-object v2, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t retrieve InstallationId for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 44
    const-string v0, "Couldn\'t retrieve InstallationId"

    goto :goto_37

    .line 45
    :catch_5a
    move-exception v0

    .line 46
    sget-object v2, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t retrieve InstallationId for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 47
    const-string v0, "Couldn\'t retrieve InstallationId"
    :try_end_75
    .catchall {:try_start_3f .. :try_end_75} :catchall_76

    goto :goto_37

    .line 35
    :catchall_76
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/io/File;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 54
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 55
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v1

    long-to-int v1, v1

    new-array v1, v1, [B

    .line 57
    :try_start_e
    invoke-virtual {v0, v1}, Ljava/io/RandomAccessFile;->readFully([B)V
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_1a

    .line 59
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 61
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0

    .line 59
    :catchall_1a
    move-exception v1

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    throw v1
.end method
