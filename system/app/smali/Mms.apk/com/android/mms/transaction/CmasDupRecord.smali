.class public Lcom/android/mms/transaction/CmasDupRecord;
.super Ljava/lang/Object;
.source "CmasDupRecord.java"


# static fields
.field private static FILENAME:Ljava/lang/String;

.field private static sInstance:Lcom/android/mms/transaction/CmasDupRecord;


# instance fields
.field private cmasrecord:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/android/mms/transaction/CmasDupRecord;->sInstance:Lcom/android/mms/transaction/CmasDupRecord;

    .line 22
    const-string v0, "cmasdup.rec"

    sput-object v0, Lcom/android/mms/transaction/CmasDupRecord;->FILENAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 8

    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    .line 36
    invoke-static {}, Lcom/android/mms/MmsApp;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 38
    .local v1, mContext:Landroid/content/Context;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    sget-object v6, Lcom/android/mms/transaction/CmasDupRecord;->FILENAME:Ljava/lang/String;

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 39
    .local v2, mfile:Ljava/io/File;
    if-eqz v2, :cond_35

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    .line 40
    const/4 v3, 0x0

    .line 42
    .local v3, oIs:Ljava/io/ObjectInputStream;
    :try_start_1e
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 43
    .local v0, f:Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_28
    .catchall {:try_start_1e .. :try_end_28} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_28} :catch_41

    .line 44
    .end local v3           #oIs:Ljava/io/ObjectInputStream;
    .local v4, oIs:Ljava/io/ObjectInputStream;
    :try_start_28
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    iput-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;
    :try_end_30
    .catchall {:try_start_28 .. :try_end_30} :catchall_55
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_30} :catch_58

    .line 49
    if-eqz v4, :cond_35

    .line 50
    :try_start_32
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_35} :catch_51

    .line 58
    .end local v0           #f:Ljava/io/FileInputStream;
    .end local v4           #oIs:Ljava/io/ObjectInputStream;
    :cond_35
    :goto_35
    iget-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    if-nez v5, :cond_40

    .line 59
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    .line 61
    :cond_40
    return-void

    .line 45
    .restart local v3       #oIs:Ljava/io/ObjectInputStream;
    :catch_41
    move-exception v5

    .line 49
    :goto_42
    if-eqz v3, :cond_35

    .line 50
    :try_start_44
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_47} :catch_48

    goto :goto_35

    .line 51
    :catch_48
    move-exception v5

    goto :goto_35

    .line 48
    :catchall_4a
    move-exception v5

    .line 49
    :goto_4b
    if-eqz v3, :cond_50

    .line 50
    :try_start_4d
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_50
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_50} :catch_53

    .line 53
    :cond_50
    :goto_50
    throw v5

    .line 51
    .end local v3           #oIs:Ljava/io/ObjectInputStream;
    .restart local v0       #f:Ljava/io/FileInputStream;
    .restart local v4       #oIs:Ljava/io/ObjectInputStream;
    :catch_51
    move-exception v5

    goto :goto_35

    .end local v0           #f:Ljava/io/FileInputStream;
    .end local v4           #oIs:Ljava/io/ObjectInputStream;
    .restart local v3       #oIs:Ljava/io/ObjectInputStream;
    :catch_53
    move-exception v6

    goto :goto_50

    .line 48
    .end local v3           #oIs:Ljava/io/ObjectInputStream;
    .restart local v0       #f:Ljava/io/FileInputStream;
    .restart local v4       #oIs:Ljava/io/ObjectInputStream;
    :catchall_55
    move-exception v5

    move-object v3, v4

    .end local v4           #oIs:Ljava/io/ObjectInputStream;
    .restart local v3       #oIs:Ljava/io/ObjectInputStream;
    goto :goto_4b

    .line 45
    .end local v3           #oIs:Ljava/io/ObjectInputStream;
    .restart local v4       #oIs:Ljava/io/ObjectInputStream;
    :catch_58
    move-exception v5

    move-object v3, v4

    .end local v4           #oIs:Ljava/io/ObjectInputStream;
    .restart local v3       #oIs:Ljava/io/ObjectInputStream;
    goto :goto_42
.end method

.method private add(IJ)V
    .registers 12
    .parameter "msgId"
    .parameter "expiry"

    .prologue
    .line 72
    iget-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {}, Lcom/android/mms/MmsApp;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 75
    .local v1, mContext:Landroid/content/Context;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    sget-object v6, Lcom/android/mms/transaction/CmasDupRecord;->FILENAME:Ljava/lang/String;

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 76
    .local v2, mfile:Ljava/io/File;
    const/4 v3, 0x0

    .line 78
    .local v3, oOs:Ljava/io/ObjectOutputStream;
    :try_start_1d
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 79
    .local v0, f:Ljava/io/FileOutputStream;
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_27
    .catchall {:try_start_1d .. :try_end_27} :catchall_3f
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_27} :catch_36

    .line 80
    .end local v3           #oOs:Ljava/io/ObjectOutputStream;
    .local v4, oOs:Ljava/io/ObjectOutputStream;
    :try_start_27
    iget-object v5, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_48
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2c} :catch_4b

    .line 85
    if-eqz v4, :cond_31

    .line 86
    :try_start_2e
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_31} :catch_33

    :cond_31
    move-object v3, v4

    .line 92
    .end local v0           #f:Ljava/io/FileOutputStream;
    .end local v4           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v3       #oOs:Ljava/io/ObjectOutputStream;
    :cond_32
    :goto_32
    return-void

    .line 87
    .end local v3           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v0       #f:Ljava/io/FileOutputStream;
    .restart local v4       #oOs:Ljava/io/ObjectOutputStream;
    :catch_33
    move-exception v5

    move-object v3, v4

    .line 90
    .end local v4           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v3       #oOs:Ljava/io/ObjectOutputStream;
    goto :goto_32

    .line 81
    .end local v0           #f:Ljava/io/FileOutputStream;
    :catch_36
    move-exception v5

    .line 85
    :goto_37
    if-eqz v3, :cond_32

    .line 86
    :try_start_39
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_3c} :catch_3d

    goto :goto_32

    .line 87
    :catch_3d
    move-exception v5

    goto :goto_32

    .line 84
    :catchall_3f
    move-exception v5

    .line 85
    :goto_40
    if-eqz v3, :cond_45

    .line 86
    :try_start_42
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_45} :catch_46

    .line 89
    :cond_45
    :goto_45
    throw v5

    .line 87
    :catch_46
    move-exception v6

    goto :goto_45

    .line 84
    .end local v3           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v0       #f:Ljava/io/FileOutputStream;
    .restart local v4       #oOs:Ljava/io/ObjectOutputStream;
    :catchall_48
    move-exception v5

    move-object v3, v4

    .end local v4           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v3       #oOs:Ljava/io/ObjectOutputStream;
    goto :goto_40

    .line 81
    .end local v3           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v4       #oOs:Ljava/io/ObjectOutputStream;
    :catch_4b
    move-exception v5

    move-object v3, v4

    .end local v4           #oOs:Ljava/io/ObjectOutputStream;
    .restart local v3       #oOs:Ljava/io/ObjectOutputStream;
    goto :goto_37
.end method

.method private static getInstance()Lcom/android/mms/transaction/CmasDupRecord;
    .registers 1

    .prologue
    .line 27
    sget-object v0, Lcom/android/mms/transaction/CmasDupRecord;->sInstance:Lcom/android/mms/transaction/CmasDupRecord;

    if-nez v0, :cond_b

    .line 28
    new-instance v0, Lcom/android/mms/transaction/CmasDupRecord;

    invoke-direct {v0}, Lcom/android/mms/transaction/CmasDupRecord;-><init>()V

    sput-object v0, Lcom/android/mms/transaction/CmasDupRecord;->sInstance:Lcom/android/mms/transaction/CmasDupRecord;

    .line 30
    :cond_b
    sget-object v0, Lcom/android/mms/transaction/CmasDupRecord;->sInstance:Lcom/android/mms/transaction/CmasDupRecord;

    return-object v0
.end method

.method public static isduplicate(IJ)Z
    .registers 9
    .parameter "msgId"
    .parameter "expiry"

    .prologue
    const/4 v3, 0x0

    .line 105
    if-nez p0, :cond_4

    .line 123
    :cond_3
    :goto_3
    return v3

    .line 108
    :cond_4
    invoke-static {}, Lcom/android/mms/transaction/CmasDupRecord;->getInstance()Lcom/android/mms/transaction/CmasDupRecord;

    move-result-object v2

    .line 109
    .local v2, rec:Lcom/android/mms/transaction/CmasDupRecord;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 112
    .local v0, curtime:J
    invoke-direct {v2, v0, v1}, Lcom/android/mms/transaction/CmasDupRecord;->purge(J)V

    .line 114
    iget-object v4, v2, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 115
    const/4 v3, 0x1

    goto :goto_3

    .line 120
    :cond_1d
    cmp-long v4, v0, p1

    if-gez v4, :cond_3

    .line 121
    invoke-direct {v2, p0, p1, p2}, Lcom/android/mms/transaction/CmasDupRecord;->add(IJ)V

    goto :goto_3
.end method

.method private purge(J)V
    .registers 8
    .parameter "curtime"

    .prologue
    .line 136
    iget-object v2, p0, Lcom/android/mms/transaction/CmasDupRecord;->cmasrecord:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 137
    .local v0, entries:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Long;>;>;"
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_44

    .line 138
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 139
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Long;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gez v2, :cond_a

    .line 140
    const-string v2, "CmasDuplicate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removed entry"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_a

    .line 145
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_44
    return-void
.end method
