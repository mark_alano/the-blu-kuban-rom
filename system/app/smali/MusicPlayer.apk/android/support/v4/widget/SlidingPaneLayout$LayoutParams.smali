.class public Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SlidingPaneLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/widget/SlidingPaneLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field private static final ATTRS:[I


# instance fields
.field canSlide:Z

.field dimPaint:Landroid/graphics/Paint;

.field dimWhenOffset:Z

.field public gravity:I

.field range:I

.field slideOffset:F

.field public weight:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1010
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->ATTRS:[I

    return-void

    nop

    :array_a
    .array-data 0x4
        0xb3t 0x0t 0x1t 0x1t
        0x81t 0x1t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1039
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1015
    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1016
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1018
    iput-boolean v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    .line 1040
    return-void
.end method

.method public constructor <init>(II)V
    .registers 5
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v1, 0x0

    .line 1043
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1015
    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1016
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1018
    iput-boolean v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    .line 1044
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "c"
    .parameter "attrs"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1061
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1015
    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1016
    iput v3, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1018
    iput-boolean v2, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    .line 1063
    sget-object v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->ATTRS:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1064
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1065
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1066
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1067
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "source"

    .prologue
    const/4 v1, 0x0

    .line 1047
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1015
    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1016
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1018
    iput-boolean v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    .line 1048
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 4
    .parameter "source"

    .prologue
    const/4 v1, 0x0

    .line 1051
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1015
    iput v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->gravity:I

    .line 1016
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    .line 1018
    iput-boolean v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    .line 1052
    return-void
.end method
