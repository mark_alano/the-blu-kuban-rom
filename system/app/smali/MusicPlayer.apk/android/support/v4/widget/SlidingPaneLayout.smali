.class public Landroid/support/v4/widget/SlidingPaneLayout;
.super Landroid/view/ViewGroup;
.source "SlidingPaneLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB;,
        Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase;,
        Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;,
        Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;,
        Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
    }
.end annotation


# static fields
.field static final IMPL:Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;

.field private static final sInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mActivePointerId:I

.field private mCanSlide:Z

.field private mDraggingPane:Landroid/view/View;

.field private mFixedPanelWidth:I

.field private final mGutterSize:I

.field private mInitialMotionX:F

.field private mIsUnableToDrag:Z

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mMaxVelocity:F

.field private final mOverhangSize:I

.field private mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

.field private mScrollState:I

.field private final mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 157
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$1;

    invoke-direct {v0}, Landroid/support/v4/widget/SlidingPaneLayout$1;-><init>()V

    sput-object v0, Landroid/support/v4/widget/SlidingPaneLayout;->sInterpolator:Landroid/view/animation/Interpolator;

    .line 172
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_15

    .line 173
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB;

    invoke-direct {v0}, Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB;-><init>()V

    sput-object v0, Landroid/support/v4/widget/SlidingPaneLayout;->IMPL:Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;

    .line 177
    :goto_14
    return-void

    .line 175
    :cond_15
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase;

    invoke-direct {v0}, Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase;-><init>()V

    sput-object v0, Landroid/support/v4/widget/SlidingPaneLayout;->IMPL:Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;

    goto :goto_14
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 221
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/SlidingPaneLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/widget/SlidingPaneLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/high16 v4, 0x3f00

    .line 229
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    const/4 v2, -0x1

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 152
    const/4 v2, 0x0

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    .line 231
    new-instance v2, Landroid/widget/Scroller;

    sget-object v3, Landroid/support/v4/widget/SlidingPaneLayout;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v2, p1, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    .line 233
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 234
    .local v0, density:F
    const/high16 v2, 0x4180

    mul-float/2addr v2, v0

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mGutterSize:I

    .line 235
    const/high16 v2, 0x42a0

    mul-float/2addr v2, v0

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mOverhangSize:I

    .line 237
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 238
    .local v1, viewConfig:Landroid/view/ViewConfiguration;
    invoke-static {v1}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    .line 239
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mMaxVelocity:F

    .line 240
    return-void
.end method

.method static canSlide(Landroid/view/View;)Z
    .registers 2
    .parameter "child"

    .prologue
    .line 979
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    return v0
.end method

.method private closePane(Landroid/view/View;I)V
    .registers 4
    .parameter "pane"
    .parameter "initialVelocity"

    .prologue
    .line 684
    iput-object p1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    .line 685
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Landroid/support/v4/widget/SlidingPaneLayout;->smoothSlideTo(FI)V

    .line 686
    return-void
.end method

.method private dimChildViewForRange(Landroid/view/View;)V
    .registers 10
    .parameter "v"

    .prologue
    const/4 v7, 0x2

    .line 770
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 771
    .local v2, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    iget-boolean v4, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimWhenOffset:Z

    if-nez v4, :cond_c

    .line 789
    :cond_b
    :goto_b
    return-void

    .line 773
    :cond_c
    iget v3, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    .line 775
    .local v3, mag:F
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_4c

    .line 776
    const/high16 v4, 0x4333

    const/high16 v5, 0x3f80

    sub-float/2addr v5, v3

    mul-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v1, v4, 0x4c

    .line 777
    .local v1, imag:I
    const/high16 v4, -0x100

    shl-int/lit8 v5, v1, 0x10

    or-int/2addr v4, v5

    shl-int/lit8 v5, v1, 0x8

    or-int/2addr v4, v5

    or-int v0, v4, v1

    .line 778
    .local v0, color:I
    iget-object v4, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimPaint:Landroid/graphics/Paint;

    if-nez v4, :cond_31

    .line 779
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimPaint:Landroid/graphics/Paint;

    .line 781
    :cond_31
    iget-object v4, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/PorterDuffColorFilter;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v0, v6}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 782
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getLayerType(Landroid/view/View;)I

    move-result v4

    if-eq v4, v7, :cond_48

    .line 783
    iget-object v4, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimPaint:Landroid/graphics/Paint;

    invoke-static {p1, v7, v4}, Landroid/support/v4/view/ViewCompat;->setLayerType(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 785
    :cond_48
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SlidingPaneLayout;->invalidateChildRegion(Landroid/view/View;)V

    goto :goto_b

    .line 786
    .end local v0           #color:I
    .end local v1           #imag:I
    :cond_4c
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getLayerType(Landroid/view/View;)I

    move-result v4

    if-eqz v4, :cond_b

    .line 787
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, Landroid/support/v4/view/ViewCompat;->setLayerType(Landroid/view/View;ILandroid/graphics/Paint;)V

    goto :goto_b
.end method

.method private invalidateChildRegion(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 814
    sget-object v0, Landroid/support/v4/widget/SlidingPaneLayout;->IMPL:Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;->invalidateChildRegion(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V

    .line 815
    return-void
.end method

.method static isDimmed(Landroid/view/View;)Z
    .registers 4
    .parameter "child"

    .prologue
    .line 983
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 984
    .local v0, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    iget-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    if-eqz v1, :cond_17

    iget-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimWhenOffset:Z

    if-eqz v1, :cond_17

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_17

    const/4 v1, 0x1

    :goto_16
    return v1

    :cond_17
    const/4 v1, 0x0

    goto :goto_16
.end method

.method private isGutterDrag(FF)Z
    .registers 6
    .parameter "x"
    .parameter "dx"

    .prologue
    const/4 v2, 0x0

    .line 818
    iget v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mGutterSize:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_c

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1c

    :cond_c
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mGutterSize:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1e

    cmpg-float v0, p2, v2

    if-gez v0, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "ev"

    .prologue
    .line 951
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v2

    .line 952
    .local v2, pointerIndex:I
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 953
    .local v1, pointerId:I
    iget v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    if-ne v1, v3, :cond_2a

    .line 956
    if-nez v2, :cond_2b

    const/4 v0, 0x1

    .line 957
    .local v0, newPointerIndex:I
    :goto_f
    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v3

    iput v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 958
    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v3

    iput v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    .line 959
    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v3

    iput v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 960
    iget-object v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_2a

    .line 961
    iget-object v3, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    .line 964
    .end local v0           #newPointerIndex:I
    :cond_2a
    return-void

    .line 956
    :cond_2b
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private openPane(Landroid/view/View;I)V
    .registers 4
    .parameter "pane"
    .parameter "initialVelocity"

    .prologue
    .line 689
    iput-object p1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    .line 690
    const/high16 v0, 0x3f80

    invoke-virtual {p0, v0, p2}, Landroid/support/v4/widget/SlidingPaneLayout;->smoothSlideTo(FI)V

    .line 691
    return-void
.end method

.method private performDrag(F)Z
    .registers 15
    .parameter "x"

    .prologue
    .line 746
    iget v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    sub-float v0, p1, v8

    .line 747
    .local v0, dxMotion:F
    iput p1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 749
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 750
    .local v3, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v8

    iget v9, v3, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    add-int v2, v8, v9

    .line 751
    .local v2, leftBound:I
    iget v8, v3, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    add-int v7, v2, v8

    .line 753
    .local v7, rightBound:I
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v8

    int-to-float v6, v8

    .line 754
    .local v6, oldLeft:F
    add-float v8, v6, v0

    int-to-float v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    int-to-float v9, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 755
    .local v4, newLeft:F
    sub-float v1, v4, v6

    .line 756
    .local v1, dxPane:F
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v8

    int-to-float v8, v8

    add-float v5, v8, v1

    .line 758
    .local v5, newRight:F
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    float-to-int v9, v4

    iget-object v10, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v10

    float-to-int v11, v5

    iget-object v12, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v12

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 760
    int-to-float v8, v2

    sub-float v8, v4, v8

    iget v9, v3, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    iput v8, v3, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    .line 762
    iget v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    float-to-int v9, v4

    int-to-float v9, v9

    sub-float v9, v4, v9

    add-float/2addr v8, v9

    iput v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 763
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-direct {p0, v8}, Landroid/support/v4/widget/SlidingPaneLayout;->dimChildViewForRange(Landroid/view/View;)V

    .line 764
    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v8}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelSlide(Landroid/view/View;)V

    .line 766
    const/4 v8, 0x0

    return v8
.end method


# virtual methods
.method protected canScroll(Landroid/view/View;ZIII)Z
    .registers 17
    .parameter "v"
    .parameter "checkV"
    .parameter "dx"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 928
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_59

    move-object v7, p1

    .line 929
    check-cast v7, Landroid/view/ViewGroup;

    .line 930
    .local v7, group:Landroid/view/ViewGroup;
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v9

    .line 931
    .local v9, scrollX:I
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v10

    .line 932
    .local v10, scrollY:I
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 934
    .local v6, count:I
    add-int/lit8 v8, v6, -0x1

    .local v8, i:I
    :goto_15
    if-ltz v8, :cond_59

    .line 937
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 938
    .local v1, child:Landroid/view/View;
    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_56

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_56

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_56

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_56

    const/4 v2, 0x1

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/SlidingPaneLayout;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 942
    const/4 v0, 0x1

    .line 947
    .end local v1           #child:Landroid/view/View;
    .end local v6           #count:I
    .end local v7           #group:Landroid/view/ViewGroup;
    .end local v8           #i:I
    .end local v9           #scrollX:I
    .end local v10           #scrollY:I
    :goto_55
    return v0

    .line 934
    .restart local v1       #child:Landroid/view/View;
    .restart local v6       #count:I
    .restart local v7       #group:Landroid/view/ViewGroup;
    .restart local v8       #i:I
    .restart local v9       #scrollX:I
    .restart local v10       #scrollY:I
    :cond_56
    add-int/lit8 v8, v8, -0x1

    goto :goto_15

    .line 947
    .end local v1           #child:Landroid/view/View;
    .end local v6           #count:I
    .end local v7           #group:Landroid/view/ViewGroup;
    .end local v8           #i:I
    .end local v9           #scrollX:I
    .end local v10           #scrollY:I
    :cond_59
    if-eqz p2, :cond_64

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->canScrollHorizontally(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_64

    const/4 v0, 0x1

    goto :goto_55

    :cond_64
    const/4 v0, 0x0

    goto :goto_55
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    .prologue
    .line 1001
    instance-of v0, p1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    if-eqz v0, :cond_c

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public computeScroll()V
    .registers 10

    .prologue
    .line 884
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->isFinished()Z

    move-result v5

    if-nez v5, :cond_19

    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 885
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-nez v5, :cond_1a

    .line 886
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    .line 914
    :cond_19
    :goto_19
    return-void

    .line 890
    :cond_1a
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 891
    .local v4, oldLeft:I
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    .line 892
    .local v3, newLeft:I
    sub-int v0, v3, v4

    .line 893
    .local v0, dx:I
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    iget-object v6, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    iget-object v7, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v7

    add-int/2addr v7, v0

    iget-object v8, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v5, v3, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 896
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 897
    .local v2, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v5

    iget v6, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    add-int v1, v5, v6

    .line 898
    .local v1, leftBound:I
    sub-int v5, v3, v1

    int-to-float v5, v5

    iget v6, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    iput v5, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    .line 899
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-direct {p0, v5}, Landroid/support/v4/widget/SlidingPaneLayout;->dimChildViewForRange(Landroid/view/View;)V

    .line 900
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v5}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelSlide(Landroid/view/View;)V

    .line 902
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->isFinished()Z

    move-result v5

    if-eqz v5, :cond_7e

    .line 903
    iget v5, v2, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_82

    .line 904
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v5}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelClosed(Landroid/view/View;)V

    .line 908
    :goto_77
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 909
    const/4 v5, 0x0

    iput-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    .line 911
    :cond_7e
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_19

    .line 906
    :cond_82
    iget-object v5, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v5}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelOpened(Landroid/view/View;)V

    goto :goto_77
.end method

.method dispatchOnPanelClosed(Landroid/view/View;)V
    .registers 3
    .parameter "panel"

    .prologue
    .line 266
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    if-eqz v0, :cond_9

    .line 267
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;->onPanelClosed(Landroid/view/View;)V

    .line 269
    :cond_9
    return-void
.end method

.method dispatchOnPanelOpened(Landroid/view/View;)V
    .registers 3
    .parameter "panel"

    .prologue
    .line 260
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    if-eqz v0, :cond_9

    .line 261
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;->onPanelOpened(Landroid/view/View;)V

    .line 263
    :cond_9
    return-void
.end method

.method dispatchOnPanelSlide(Landroid/view/View;)V
    .registers 4
    .parameter "panel"

    .prologue
    .line 253
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    if-eqz v0, :cond_11

    .line 254
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    invoke-interface {v1, p1, v0}, Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;->onPanelSlide(Landroid/view/View;F)V

    .line 257
    :cond_11
    return-void
.end method

.method distanceInfluenceForSnapDuration(F)F
    .registers 6
    .parameter "f"

    .prologue
    .line 877
    const/high16 v0, 0x3f00

    sub-float/2addr p1, v0

    .line 878
    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L

    mul-double/2addr v0, v2

    double-to-float p1, v0

    .line 879
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 11
    .parameter "canvas"
    .parameter "child"
    .parameter "drawingTime"

    .prologue
    const/4 v2, 0x0

    .line 793
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_c

    .line 794
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    .line 809
    :goto_b
    return v2

    .line 797
    :cond_c
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 798
    .local v1, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    iget-boolean v3, v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimWhenOffset:Z

    if-eqz v3, :cond_3b

    iget v3, v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3b

    .line 799
    invoke-virtual {p2}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v3

    if-nez v3, :cond_27

    .line 800
    const/4 v3, 0x1

    invoke-virtual {p2, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 802
    :cond_27
    invoke-virtual {p2}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 803
    .local v0, cache:Landroid/graphics/Bitmap;
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_b

    .line 806
    .end local v0           #cache:Landroid/graphics/Bitmap;
    :cond_3b
    invoke-virtual {p2}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v3

    if-eqz v3, :cond_44

    .line 807
    invoke-virtual {p2, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 809
    :cond_44
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    goto :goto_b
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 989
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    invoke-direct {v0}, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    .prologue
    .line 1006
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    .prologue
    .line 994
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_c

    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local p1
    invoke-direct {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    :goto_b
    return-object v0

    .restart local p1
    :cond_c
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b
.end method

.method getChildUnderPoint(FF)Landroid/view/View;
    .registers 7
    .parameter "x"
    .parameter "y"

    .prologue
    .line 967
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v1

    .line 968
    .local v1, childCount:I
    add-int/lit8 v2, v1, -0x1

    .local v2, i:I
    :goto_6
    if-ltz v2, :cond_34

    .line 969
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 970
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-ltz v3, :cond_31

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-gez v3, :cond_31

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_31

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-gez v3, :cond_31

    .line 975
    .end local v0           #child:Landroid/view/View;
    :goto_30
    return-object v0

    .line 968
    .restart local v0       #child:Landroid/view/View;
    :cond_31
    add-int/lit8 v2, v2, -0x1

    goto :goto_6

    .line 975
    .end local v0           #child:Landroid/view/View;
    :cond_34
    const/4 v0, 0x0

    goto :goto_30
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 19
    .parameter "ev"

    .prologue
    .line 485
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mCanSlide:Z

    if-nez v1, :cond_b

    .line 486
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 570
    :goto_a
    return v1

    .line 489
    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v7, v1, 0xff

    .line 491
    .local v7, action:I
    const/4 v1, 0x3

    if-eq v7, v1, :cond_17

    const/4 v1, 0x1

    if-ne v7, v1, :cond_35

    .line 492
    :cond_17
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    .line 493
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 494
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_33

    .line 495
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 496
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 498
    :cond_33
    const/4 v1, 0x0

    goto :goto_a

    .line 501
    :cond_35
    const/4 v11, 0x0

    .line 503
    .local v11, interceptTap:Z
    sparse-switch v7, :sswitch_data_17a

    .line 566
    :cond_39
    :goto_39
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v1, :cond_47

    .line 567
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 569
    :cond_47
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 570
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_59

    if-eqz v11, :cond_176

    :cond_59
    const/4 v1, 0x1

    goto :goto_a

    .line 505
    :sswitch_5b
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 506
    .local v8, activePointerId:I
    const/4 v1, -0x1

    if-eq v8, v1, :cond_39

    .line 511
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v12

    .line 512
    .local v12, pointerIndex:I
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v13

    .line 513
    .local v13, x:F
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    sub-float v10, v13, v1

    .line 514
    .local v10, dx:F
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v14

    .line 515
    .local v14, xDiff:F
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v15

    .line 516
    .local v15, y:F
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    sub-float v1, v15, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v16

    .line 518
    .local v16, yDiff:F
    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-eqz v1, :cond_bb

    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v10}, Landroid/support/v4/widget/SlidingPaneLayout;->isGutterDrag(FF)Z

    move-result v1

    if-nez v1, :cond_bb

    const/4 v3, 0x0

    float-to-int v4, v10

    float-to-int v5, v13

    float-to-int v6, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Landroid/support/v4/widget/SlidingPaneLayout;->canScroll(Landroid/view/View;ZIII)Z

    move-result v1

    if-eqz v1, :cond_bb

    .line 520
    move-object/from16 v0, p0

    iput v13, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    move-object/from16 v0, p0

    iput v13, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    .line 521
    move-object/from16 v0, p0

    iput v15, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    .line 522
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mIsUnableToDrag:Z

    .line 523
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 525
    :cond_bb
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v1, v14, v1

    if-lez v1, :cond_10b

    cmpl-float v1, v14, v16

    if-lez v1, :cond_10b

    .line 526
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v15}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildUnderPoint(FF)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-eqz v1, :cond_ed

    .line 527
    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-lez v1, :cond_100

    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    :goto_e3
    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 529
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 534
    :cond_ed
    :goto_ed
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-eqz v1, :cond_39

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Landroid/support/v4/widget/SlidingPaneLayout;->performDrag(F)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 535
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_39

    .line 527
    :cond_100
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    goto :goto_e3

    .line 531
    :cond_10b
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v1, v16, v1

    if-lez v1, :cond_ed

    .line 532
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mIsUnableToDrag:Z

    goto :goto_ed

    .line 541
    .end local v8           #activePointerId:I
    .end local v10           #dx:F
    .end local v12           #pointerIndex:I
    .end local v13           #x:F
    .end local v14           #xDiff:F
    .end local v15           #y:F
    .end local v16           #yDiff:F
    :sswitch_11a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    .line 542
    .restart local v13       #x:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    .line 543
    .restart local v15       #y:F
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v15}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildUnderPoint(FF)Landroid/view/View;

    move-result-object v9

    .line 544
    .local v9, dragTarget:Landroid/view/View;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mIsUnableToDrag:Z

    .line 545
    if-eqz v9, :cond_135

    invoke-static {v9}, Landroid/support/v4/widget/SlidingPaneLayout;->canSlide(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_13c

    .line 546
    :cond_135
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    goto/16 :goto_39

    .line 548
    :cond_13c
    move-object/from16 v0, p0

    iput v13, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move-object/from16 v0, p0

    iput v13, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 549
    move-object/from16 v0, p0

    iput v15, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    .line 550
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 551
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_168

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-ne v1, v9, :cond_168

    .line 553
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    goto/16 :goto_39

    .line 554
    :cond_168
    invoke-static {v9}, Landroid/support/v4/widget/SlidingPaneLayout;->isDimmed(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 555
    const/4 v11, 0x1

    goto/16 :goto_39

    .line 562
    .end local v9           #dragTarget:Landroid/view/View;
    .end local v13           #x:F
    .end local v15           #y:F
    :sswitch_171
    invoke-direct/range {p0 .. p1}, Landroid/support/v4/widget/SlidingPaneLayout;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_39

    .line 570
    :cond_176
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 503
    nop

    :sswitch_data_17a
    .sparse-switch
        0x0 -> :sswitch_11a
        0x2 -> :sswitch_5b
        0x6 -> :sswitch_171
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 29
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 446
    sub-int v18, p4, p2

    .line 447
    .local v18, width:I
    sub-int v9, p5, p3

    .line 448
    .local v9, height:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v14

    .line 449
    .local v14, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingRight()I

    move-result v15

    .line 450
    .local v15, paddingRight:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingTop()I

    move-result v16

    .line 451
    .local v16, paddingTop:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingBottom()I

    move-result v13

    .line 453
    .local v13, paddingBottom:I
    move/from16 v19, v14

    .line 454
    .local v19, xStart:I
    move/from16 v12, v19

    .line 456
    .local v12, nextXStart:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v4

    .line 457
    .local v4, childCount:I
    const/4 v10, 0x0

    .local v10, i:I
    :goto_1d
    if-ge v10, v4, :cond_99

    .line 458
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 459
    .local v2, child:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 461
    .local v11, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 463
    .local v8, childWidth:I
    iget-boolean v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    move/from16 v20, v0

    if-eqz v20, :cond_96

    .line 464
    sub-int v20, v12, v19

    sub-int v21, v18, v15

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mOverhangSize:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    sub-int v21, v21, v19

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 466
    .local v17, range:I
    move/from16 v0, v17

    iput v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    .line 467
    sub-int v20, v18, v15

    add-int v21, v19, v17

    sub-int v20, v20, v21

    div-int/lit8 v21, v8, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_93

    const/16 v20, 0x1

    :goto_5b
    move/from16 v0, v20

    iput-boolean v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->dimWhenOffset:Z

    .line 468
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    iget v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v19, v19, v20

    .line 473
    .end local v17           #range:I
    :goto_71
    iget v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v5, v19, v20

    .line 474
    .local v5, childLeft:I
    add-int v6, v5, v8

    .line 475
    .local v6, childRight:I
    move/from16 v7, v16

    .line 476
    .local v7, childTop:I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v20

    add-int v3, v7, v20

    .line 477
    .local v3, childBottom:I
    move/from16 v0, v16

    invoke-virtual {v2, v5, v0, v6, v3}, Landroid/view/View;->layout(IIII)V

    .line 479
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v20

    iget v0, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    move/from16 v21, v0

    add-int v12, v20, v21

    .line 457
    add-int/lit8 v10, v10, 0x1

    goto :goto_1d

    .line 467
    .end local v3           #childBottom:I
    .end local v5           #childLeft:I
    .end local v6           #childRight:I
    .end local v7           #childTop:I
    .restart local v17       #range:I
    :cond_93
    const/16 v20, 0x0

    goto :goto_5b

    .line 470
    .end local v17           #range:I
    :cond_96
    move/from16 v19, v12

    goto :goto_71

    .line 481
    .end local v2           #child:Landroid/view/View;
    .end local v8           #childWidth:I
    .end local v11           #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    :cond_99
    return-void
.end method

.method protected onMeasure(II)V
    .registers 33
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 273
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v24

    .line 274
    .local v24, widthMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v26

    .line 275
    .local v26, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    .line 276
    .local v13, heightMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 278
    .local v14, heightSize:I
    const/high16 v28, 0x4000

    move/from16 v0, v24

    move/from16 v1, v28

    if-eq v0, v1, :cond_20

    .line 279
    new-instance v28, Ljava/lang/IllegalStateException;

    const-string v29, "Width must have an exact value or MATCH_PARENT"

    invoke-direct/range {v28 .. v29}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 280
    :cond_20
    if-nez v13, :cond_2a

    .line 281
    new-instance v28, Ljava/lang/IllegalStateException;

    const-string v29, "Height must not be UNSPECIFIED"

    invoke-direct/range {v28 .. v29}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 284
    :cond_2a
    const/16 v17, 0x0

    .line 285
    .local v17, layoutHeight:I
    const/16 v19, -0x1

    .line 286
    .local v19, maxLayoutHeight:I
    sparse-switch v13, :sswitch_data_304

    .line 295
    :goto_31
    const/16 v23, 0x0

    .line 296
    .local v23, weightSum:F
    const/4 v4, 0x0

    .line 297
    .local v4, canSlide:Z
    const/4 v12, 0x0

    .line 298
    .local v12, foundDraggingPane:Z
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v28

    sub-int v28, v26, v28

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingRight()I

    move-result v29

    sub-int v25, v28, v29

    .line 299
    .local v25, widthRemaining:I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v6

    .line 301
    .local v6, childCount:I
    const/16 v28, 0x2

    move/from16 v0, v28

    if-le v6, v0, :cond_52

    .line 302
    const-string v28, "SlidingPaneLayout"

    const-string v29, "onMeasure: More than two panes are not currently supported. We\'re in bat country!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_52
    const/16 v16, 0x0

    .local v16, i:I
    :goto_54
    move/from16 v0, v16

    if-ge v0, v6, :cond_160

    .line 308
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 309
    .local v5, child:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 311
    .local v18, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_71

    .line 312
    const/4 v12, 0x1

    .line 315
    :cond_71
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-lez v28, :cond_ac

    .line 316
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    add-float v23, v23, v28

    .line 320
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    if-nez v28, :cond_ac

    .line 307
    :goto_8d
    add-int/lit8 v16, v16, 0x1

    goto :goto_54

    .line 288
    .end local v4           #canSlide:Z
    .end local v5           #child:Landroid/view/View;
    .end local v6           #childCount:I
    .end local v12           #foundDraggingPane:Z
    .end local v16           #i:I
    .end local v18           #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    .end local v23           #weightSum:F
    .end local v25           #widthRemaining:I
    :sswitch_90
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingTop()I

    move-result v28

    sub-int v28, v14, v28

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingBottom()I

    move-result v29

    sub-int v19, v28, v29

    move/from16 v17, v19

    .line 289
    goto :goto_31

    .line 291
    :sswitch_9f
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingTop()I

    move-result v28

    sub-int v28, v14, v28

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingBottom()I

    move-result v29

    sub-int v19, v28, v29

    goto :goto_31

    .line 324
    .restart local v4       #canSlide:Z
    .restart local v5       #child:Landroid/view/View;
    .restart local v6       #childCount:I
    .restart local v12       #foundDraggingPane:Z
    .restart local v16       #i:I
    .restart local v18       #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    .restart local v23       #weightSum:F
    .restart local v25       #widthRemaining:I
    :cond_ac
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    move/from16 v28, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    move/from16 v29, v0

    add-int v15, v28, v29

    .line 325
    .local v15, horizontalMargin:I
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    const/16 v29, -0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_113

    .line 326
    sub-int v28, v26, v15

    const/high16 v29, -0x8000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 336
    .local v10, childWidthSpec:I
    :goto_d0
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_137

    .line 337
    const/high16 v28, -0x8000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 344
    .local v8, childHeightSpec:I
    :goto_e8
    invoke-virtual {v5, v10, v8}, Landroid/view/View;->measure(II)V

    .line 345
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 346
    .local v9, childWidth:I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 348
    .local v7, childHeight:I
    const/high16 v28, -0x8000

    move/from16 v0, v28

    if-ne v13, v0, :cond_103

    move/from16 v0, v17

    if-le v7, v0, :cond_103

    .line 349
    move/from16 v0, v19

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 352
    :cond_103
    sub-int v25, v25, v9

    .line 353
    if-gez v25, :cond_15d

    const/16 v28, 0x1

    :goto_109
    move/from16 v0, v28

    move-object/from16 v1, v18

    iput-boolean v0, v1, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    or-int v4, v4, v28

    goto/16 :goto_8d

    .line 328
    .end local v7           #childHeight:I
    .end local v8           #childHeightSpec:I
    .end local v9           #childWidth:I
    .end local v10           #childWidthSpec:I
    :cond_113
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_12a

    .line 329
    sub-int v28, v26, v15

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .restart local v10       #childWidthSpec:I
    goto :goto_d0

    .line 332
    .end local v10           #childWidthSpec:I
    :cond_12a
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .restart local v10       #childWidthSpec:I
    goto :goto_d0

    .line 338
    :cond_137
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_150

    .line 339
    const/high16 v28, 0x4000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_e8

    .line 341
    .end local v8           #childHeightSpec:I
    :cond_150
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_e8

    .line 353
    .restart local v7       #childHeight:I
    .restart local v9       #childWidth:I
    :cond_15d
    const/16 v28, 0x0

    goto :goto_109

    .line 357
    .end local v5           #child:Landroid/view/View;
    .end local v7           #childHeight:I
    .end local v8           #childHeightSpec:I
    .end local v9           #childWidth:I
    .end local v10           #childWidthSpec:I
    .end local v15           #horizontalMargin:I
    .end local v18           #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    :cond_160
    if-nez v4, :cond_168

    const/16 v28, 0x0

    cmpl-float v28, v23, v28

    if-lez v28, :cond_2d9

    .line 358
    :cond_168
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mOverhangSize:I

    move/from16 v28, v0

    sub-int v11, v26, v28

    .line 360
    .local v11, fixedPanelWidthLimit:I
    const/16 v16, 0x0

    :goto_172
    move/from16 v0, v16

    if-ge v0, v6, :cond_2d9

    .line 361
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 362
    .restart local v5       #child:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 364
    .restart local v18       #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    if-nez v28, :cond_1ec

    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-lez v28, :cond_1ec

    const/16 v22, 0x1

    .line 365
    .local v22, skippedFirstPass:Z
    :goto_19a
    if-eqz v22, :cond_1ef

    const/16 v20, 0x0

    .line 366
    .local v20, measuredWidth:I
    :goto_19e
    if-eqz v4, :cond_22c

    move-object/from16 v0, v18

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    move/from16 v28, v0

    if-nez v28, :cond_22c

    .line 367
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    if-gez v28, :cond_225

    move/from16 v0, v20

    if-gt v0, v11, :cond_1c0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-lez v28, :cond_225

    .line 371
    :cond_1c0
    if-eqz v22, :cond_21a

    .line 374
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_1f4

    .line 375
    const/high16 v28, -0x8000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 388
    .restart local v8       #childHeightSpec:I
    :goto_1da
    const/high16 v28, 0x4000

    move/from16 v0, v28

    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 390
    .restart local v10       #childWidthSpec:I
    invoke-virtual {v5, v10, v8}, Landroid/view/View;->measure(II)V

    .line 391
    move-object/from16 v0, p0

    iput v11, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mFixedPanelWidth:I

    .line 360
    .end local v8           #childHeightSpec:I
    .end local v10           #childWidthSpec:I
    :cond_1e9
    :goto_1e9
    add-int/lit8 v16, v16, 0x1

    goto :goto_172

    .line 364
    .end local v20           #measuredWidth:I
    .end local v22           #skippedFirstPass:Z
    :cond_1ec
    const/16 v22, 0x0

    goto :goto_19a

    .line 365
    .restart local v22       #skippedFirstPass:Z
    :cond_1ef
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    goto :goto_19e

    .line 377
    .restart local v20       #measuredWidth:I
    :cond_1f4
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_20d

    .line 378
    const/high16 v28, 0x4000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_1da

    .line 381
    .end local v8           #childHeightSpec:I
    :cond_20d
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_1da

    .line 385
    .end local v8           #childHeightSpec:I
    :cond_21a
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v28

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_1da

    .line 393
    .end local v8           #childHeightSpec:I
    :cond_225
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mFixedPanelWidth:I

    goto :goto_1e9

    .line 395
    :cond_22c
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    const/16 v29, 0x0

    cmpl-float v28, v28, v29

    if-lez v28, :cond_1e9

    .line 397
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->width:I

    move/from16 v28, v0

    if-nez v28, :cond_2a5

    .line 399
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_27f

    .line 400
    const/high16 v28, -0x8000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 414
    .restart local v8       #childHeightSpec:I
    :goto_258
    if-eqz v4, :cond_2b0

    .line 416
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    move/from16 v28, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    move/from16 v29, v0

    add-int v15, v28, v29

    .line 417
    .restart local v15       #horizontalMargin:I
    sub-int v21, v26, v15

    .line 418
    .local v21, newWidth:I
    const/high16 v28, 0x4000

    move/from16 v0, v21

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 420
    .restart local v10       #childWidthSpec:I
    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_1e9

    .line 421
    invoke-virtual {v5, v10, v8}, Landroid/view/View;->measure(II)V

    goto/16 :goto_1e9

    .line 402
    .end local v8           #childHeightSpec:I
    .end local v10           #childWidthSpec:I
    .end local v15           #horizontalMargin:I
    .end local v21           #newWidth:I
    :cond_27f
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_298

    .line 403
    const/high16 v28, 0x4000

    move/from16 v0, v19

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_258

    .line 406
    .end local v8           #childHeightSpec:I
    :cond_298
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->height:I

    move/from16 v28, v0

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_258

    .line 410
    .end local v8           #childHeightSpec:I
    :cond_2a5
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v28

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8       #childHeightSpec:I
    goto :goto_258

    .line 425
    :cond_2b0
    const/16 v28, 0x0

    move/from16 v0, v28

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v27

    .line 426
    .local v27, widthToDistribute:I
    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->weight:F

    move/from16 v28, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v29, v0

    mul-float v28, v28, v29

    div-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v3, v0

    .line 427
    .local v3, addedWidth:I
    add-int v28, v20, v3

    const/high16 v29, 0x4000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 429
    .restart local v10       #childWidthSpec:I
    invoke-virtual {v5, v10, v8}, Landroid/view/View;->measure(II)V

    goto/16 :goto_1e9

    .line 435
    .end local v3           #addedWidth:I
    .end local v5           #child:Landroid/view/View;
    .end local v8           #childHeightSpec:I
    .end local v10           #childWidthSpec:I
    .end local v11           #fixedPanelWidthLimit:I
    .end local v18           #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    .end local v20           #measuredWidth:I
    .end local v22           #skippedFirstPass:Z
    .end local v27           #widthToDistribute:I
    :cond_2d9
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->setMeasuredDimension(II)V

    .line 436
    move-object/from16 v0, p0

    iput-boolean v4, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mCanSlide:Z

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v28, v0

    if-eqz v28, :cond_303

    if-eqz v4, :cond_2f2

    if-nez v12, :cond_303

    .line 439
    :cond_2f2
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    .line 440
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 442
    :cond_303
    return-void

    .line 286
    :sswitch_data_304
    .sparse-switch
        -0x80000000 -> :sswitch_9f
        0x40000000 -> :sswitch_90
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 22
    .parameter "ev"

    .prologue
    .line 575
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mCanSlide:Z

    move/from16 v18, v0

    if-nez v18, :cond_d

    .line 576
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v15

    .line 680
    :cond_c
    :goto_c
    return v15

    .line 579
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    if-nez v18, :cond_1f

    .line 580
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 582
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 584
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 585
    .local v3, action:I
    const/4 v11, 0x0

    .line 586
    .local v11, needsInvalidate:Z
    const/4 v15, 0x0

    .line 588
    .local v15, wantTouchEvents:Z
    and-int/lit16 v0, v3, 0xff

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_27e

    .line 677
    :cond_39
    :goto_39
    :pswitch_39
    if-eqz v11, :cond_c

    .line 678
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_c

    .line 590
    :pswitch_3f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v16

    .line 591
    .local v16, x:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    .line 592
    .local v17, y:F
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildUnderPoint(FF)Landroid/view/View;

    move-result-object v13

    .line 593
    .local v13, view:Landroid/view/View;
    if-eqz v13, :cond_8e

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    move-object/from16 v10, v18

    .line 595
    .local v10, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    :goto_5b
    if-eqz v13, :cond_39

    iget-boolean v0, v10, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->canSlide:Z

    move/from16 v18, v0

    if-eqz v18, :cond_39

    .line 596
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Scroller;->abortAnimation()V

    .line 597
    const/4 v15, 0x1

    .line 598
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 599
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 600
    move-object/from16 v0, p0

    iput-object v13, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    goto :goto_39

    .line 593
    .end local v10           #lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    :cond_8e
    const/4 v10, 0x0

    goto :goto_5b

    .line 606
    .end local v13           #view:Landroid/view/View;
    .end local v16           #x:F
    .end local v17           #y:F
    :pswitch_90
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_125

    .line 607
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v12

    .line 609
    .local v12, pointerIndex:I
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v16

    .line 610
    .restart local v16       #x:F
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v17

    .line 611
    .restart local v17       #y:F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    move/from16 v18, v0

    sub-float v18, v16, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 612
    .local v6, dx:F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    move/from16 v18, v0

    sub-float v18, v17, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v7

    .line 613
    .local v7, dy:F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    cmpl-float v18, v6, v18

    if-lez v18, :cond_125

    cmpl-float v18, v6, v7

    if-lez v18, :cond_125

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildUnderPoint(FF)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-eqz v18, :cond_125

    .line 615
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move/from16 v18, v0

    sub-float v18, v16, v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_153

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v18, v18, v19

    :goto_116
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 617
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 620
    .end local v6           #dx:F
    .end local v7           #dy:F
    .end local v12           #pointerIndex:I
    .end local v16           #x:F
    .end local v17           #y:F
    :cond_125
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_39

    .line 621
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 623
    .local v4, activePointerIndex:I
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v16

    .line 624
    .restart local v16       #x:F
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->performDrag(F)Z

    move-result v18

    or-int v11, v11, v18

    .line 625
    goto/16 :goto_39

    .line 615
    .end local v4           #activePointerIndex:I
    .restart local v6       #dx:F
    .restart local v7       #dy:F
    .restart local v12       #pointerIndex:I
    .restart local v17       #y:F
    :cond_153
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mInitialMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mTouchSlop:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sub-float v18, v18, v19

    goto :goto_116

    .line 630
    .end local v6           #dx:F
    .end local v7           #dy:F
    .end local v12           #pointerIndex:I
    .end local v16           #x:F
    .end local v17           #y:F
    :pswitch_167
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1d6

    .line 631
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 632
    .local v14, vt:Landroid/view/VelocityTracker;
    const/16 v18, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mMaxVelocity:F

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 633
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v14, v0}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v9, v0

    .line 635
    .local v9, initialVelocity:I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 636
    .local v5, dragLp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    if-ltz v9, :cond_1b1

    if-nez v9, :cond_1c8

    iget v0, v5, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    move/from16 v18, v0

    const/high16 v19, 0x3f00

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1c8

    .line 638
    :cond_1b1
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v9}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane(Landroid/view/View;I)V

    .line 642
    :goto_1be
    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    goto/16 :goto_39

    .line 640
    :cond_1c8
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v9}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane(Landroid/view/View;I)V

    goto :goto_1be

    .line 643
    .end local v5           #dragLp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    .end local v9           #initialVelocity:I
    .end local v14           #vt:Landroid/view/VelocityTracker;
    :cond_1d6
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/support/v4/widget/SlidingPaneLayout;->isDimmed(Landroid/view/View;)Z

    move-result v18

    if-eqz v18, :cond_39

    .line 645
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane(Landroid/view/View;I)V

    goto/16 :goto_39

    .line 651
    :pswitch_1fd
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_39

    .line 652
    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    .line 653
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 654
    .restart local v5       #dragLp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    iget v0, v5, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    move/from16 v18, v0

    const/high16 v19, 0x3f00

    cmpg-float v18, v18, v19

    if-gez v18, :cond_23c

    .line 655
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane(Landroid/view/View;I)V

    goto/16 :goto_39

    .line 657
    :cond_23c
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane(Landroid/view/View;I)V

    goto/16 :goto_39

    .line 664
    .end local v5           #dragLp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    :pswitch_24f
    invoke-static/range {p1 .. p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v8

    .line 665
    .local v8, index:I
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionX:F

    .line 666
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mLastMotionY:F

    .line 667
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/widget/SlidingPaneLayout;->mActivePointerId:I

    goto/16 :goto_39

    .line 672
    .end local v8           #index:I
    :pswitch_279
    invoke-direct/range {p0 .. p1}, Landroid/support/v4/widget/SlidingPaneLayout;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_39

    .line 588
    :pswitch_data_27e
    .packed-switch 0x0
        :pswitch_3f
        :pswitch_167
        :pswitch_90
        :pswitch_1fd
        :pswitch_39
        :pswitch_24f
        :pswitch_279
    .end packed-switch
.end method

.method public setPanelSlideListener(Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 249
    iput-object p1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mPanelSlideListener:Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

    .line 250
    return-void
.end method

.method setScrollState(I)V
    .registers 3
    .parameter "state"

    .prologue
    .line 243
    iget v0, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    if-eq v0, p1, :cond_6

    .line 244
    iput p1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScrollState:I

    .line 246
    :cond_6
    return-void
.end method

.method smoothSlideTo(FI)V
    .registers 18
    .parameter "slideOffset"
    .parameter "velocity"

    .prologue
    .line 828
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    if-nez v1, :cond_5

    .line 870
    :goto_4
    return-void

    .line 833
    :cond_5
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 835
    .local v11, lp:Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v1

    iget v3, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    add-int v10, v1, v3

    .line 836
    .local v10, leftBound:I
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 837
    .local v2, sx:I
    int-to-float v1, v10

    iget v3, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    int-to-float v3, v3

    mul-float v3, v3, p1

    add-float/2addr v1, v3

    float-to-int v14, v1

    .line 838
    .local v14, x:I
    sub-int v4, v14, v2

    .line 839
    .local v4, dx:I
    if-nez v4, :cond_41

    .line 840
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 841
    iget v1, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->slideOffset:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-nez v1, :cond_3b

    .line 842
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelClosed(Landroid/view/View;)V

    .line 846
    :goto_37
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    goto :goto_4

    .line 844
    :cond_3b
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mDraggingPane:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->dispatchOnPanelOpened(Landroid/view/View;)V

    goto :goto_37

    .line 850
    :cond_41
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setScrollState(I)V

    .line 852
    invoke-virtual {p0}, Landroid/support/v4/widget/SlidingPaneLayout;->getWidth()I

    move-result v13

    .line 853
    .local v13, width:I
    div-int/lit8 v9, v13, 0x2

    .line 854
    .local v9, halfWidth:I
    const/high16 v1, 0x3f80

    const/high16 v3, 0x3f80

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v3, v5

    int-to-float v5, v13

    div-float/2addr v3, v5

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 855
    .local v8, distanceRatio:F
    int-to-float v1, v9

    int-to-float v3, v9

    invoke-virtual {p0, v8}, Landroid/support/v4/widget/SlidingPaneLayout;->distanceInfluenceForSnapDuration(F)F

    move-result v5

    mul-float/2addr v3, v5

    add-float v7, v1, v3

    .line 858
    .local v7, distance:F
    const/4 v6, 0x0

    .line 859
    .local v6, duration:I
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    .line 860
    if-lez p2, :cond_8f

    .line 861
    const/high16 v1, 0x447a

    move/from16 v0, p2

    int-to-float v3, v0

    div-float v3, v7, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-int/lit8 v6, v1, 0x4

    .line 866
    :goto_7d
    const/16 v1, 0x258

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 868
    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 869
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_4

    .line 863
    :cond_8f
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v3, v11, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->range:I

    int-to-float v3, v3

    div-float v12, v1, v3

    .line 864
    .local v12, range:F
    const/high16 v1, 0x3f80

    add-float/2addr v1, v12

    const/high16 v3, 0x42c8

    mul-float/2addr v1, v3

    float-to-int v6, v1

    goto :goto_7d
.end method
