.class Lcom/google/android/common/gesture/GestureDetector$GestureHandler;
.super Landroid/os/Handler;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/common/gesture/GestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/common/gesture/GestureDetector;


# direct methods
.method constructor <init>(Lcom/google/android/common/gesture/GestureDetector;)V
    .registers 2
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    .line 112
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 113
    return-void
.end method

.method constructor <init>(Lcom/google/android/common/gesture/GestureDetector;Landroid/os/Handler;)V
    .registers 4
    .parameter
    .parameter "handler"

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    .line 116
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 117
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    .prologue
    .line 121
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_54

    .line 138
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;
    invoke-static {v0}, Lcom/google/android/common/gesture/GestureDetector;->access$100(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Lcom/google/android/common/gesture/GestureDetector;->access$000(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 140
    :cond_2d
    :goto_2d
    return-void

    .line 127
    :pswitch_2e
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #calls: Lcom/google/android/common/gesture/GestureDetector;->dispatchLongPress()V
    invoke-static {v0}, Lcom/google/android/common/gesture/GestureDetector;->access$200(Lcom/google/android/common/gesture/GestureDetector;)V

    goto :goto_2d

    .line 132
    :pswitch_34
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;
    invoke-static {v0}, Lcom/google/android/common/gesture/GestureDetector;->access$300(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z
    invoke-static {v0}, Lcom/google/android/common/gesture/GestureDetector;->access$400(Lcom/google/android/common/gesture/GestureDetector;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 133
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;
    invoke-static {v0}, Lcom/google/android/common/gesture/GestureDetector;->access$300(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;->this$0:Lcom/google/android/common/gesture/GestureDetector;

    #getter for: Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Lcom/google/android/common/gesture/GestureDetector;->access$000(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_2d

    .line 121
    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2e
        :pswitch_34
    .end packed-switch
.end method
