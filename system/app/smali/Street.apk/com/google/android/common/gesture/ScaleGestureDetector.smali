.class public Lcom/google/android/common/gesture/ScaleGestureDetector;
.super Ljava/lang/Object;
.source "ScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrSpan:F

.field private mCurrSpanX:F

.field private mCurrSpanY:F

.field private mFocusX:F

.field private mFocusY:F

.field private mInProgress:Z

.field private mInitialSpan:F

.field private final mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

.field private mPrevSpan:F

.field private mPrevSpanX:F

.field private mPrevSpanY:F

.field private final mSpanSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;)V
    .registers 4
    .parameter "context"
    .parameter "listener"

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mSpanSlop:I

    .line 169
    iput-object p1, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mContext:Landroid/content/Context;

    .line 170
    iput-object p2, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    .line 176
    return-void
.end method


# virtual methods
.method public getCurrentSpan()F
    .registers 2

    .prologue
    .line 340
    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    return v0
.end method

.method public getFocusX()F
    .registers 2

    .prologue
    .line 316
    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusX:F

    return v0
.end method

.method public getFocusY()F
    .registers 2

    .prologue
    .line 330
    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusY:F

    return v0
.end method

.method public getPreviousSpan()F
    .registers 2

    .prologue
    .line 372
    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 27
    .parameter "event"

    .prologue
    .line 199
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 201
    .local v2, action:I
    const/16 v23, 0x1

    move v0, v2

    move/from16 v1, v23

    if-eq v0, v1, :cond_12

    const/16 v23, 0x3

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_44

    :cond_12
    const/16 v23, 0x1

    move/from16 v18, v23

    .line 203
    .local v18, streamComplete:Z
    :goto_16
    if-eqz v2, :cond_1a

    if-eqz v18, :cond_49

    .line 207
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3f

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Lcom/google/android/common/gesture/ScaleGestureDetector;)V

    .line 209
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    .line 210
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    .line 213
    :cond_3f
    if-eqz v18, :cond_49

    .line 214
    const/16 v23, 0x1

    .line 295
    :goto_43
    return v23

    .line 201
    .end local v18           #streamComplete:Z
    :cond_44
    const/16 v23, 0x0

    move/from16 v18, v23

    goto :goto_16

    .line 218
    .restart local v18       #streamComplete:Z
    :cond_49
    const/16 v23, 0x6

    move v0, v2

    move/from16 v1, v23

    if-eq v0, v1, :cond_57

    const/16 v23, 0x5

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_7e

    :cond_57
    const/16 v23, 0x1

    move/from16 v3, v23

    .line 221
    .local v3, configChanged:Z
    :goto_5b
    const/16 v23, 0x6

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_83

    const/16 v23, 0x1

    move/from16 v13, v23

    .line 222
    .local v13, pointerUp:Z
    :goto_66
    if-eqz v13, :cond_88

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v23

    move/from16 v14, v23

    .line 225
    .local v14, skipIndex:I
    :goto_6e
    const/16 v19, 0x0

    .local v19, sumX:F
    const/16 v20, 0x0

    .line 226
    .local v20, sumY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    .line 227
    .local v4, count:I
    const/4 v12, 0x0

    .local v12, i:I
    :goto_77
    if-ge v12, v4, :cond_a0

    .line 228
    if-ne v14, v12, :cond_8d

    .line 227
    :goto_7b
    add-int/lit8 v12, v12, 0x1

    goto :goto_77

    .line 218
    .end local v3           #configChanged:Z
    .end local v4           #count:I
    .end local v12           #i:I
    .end local v13           #pointerUp:Z
    .end local v14           #skipIndex:I
    .end local v19           #sumX:F
    .end local v20           #sumY:F
    :cond_7e
    const/16 v23, 0x0

    move/from16 v3, v23

    goto :goto_5b

    .line 221
    .restart local v3       #configChanged:Z
    :cond_83
    const/16 v23, 0x0

    move/from16 v13, v23

    goto :goto_66

    .line 222
    .restart local v13       #pointerUp:Z
    :cond_88
    const/16 v23, -0x1

    move/from16 v14, v23

    goto :goto_6e

    .line 229
    .restart local v4       #count:I
    .restart local v12       #i:I
    .restart local v14       #skipIndex:I
    .restart local v19       #sumX:F
    .restart local v20       #sumY:F
    :cond_8d
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    add-float v19, v19, v23

    .line 230
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    add-float v20, v20, v23

    goto :goto_7b

    .line 232
    :cond_a0
    if-eqz v13, :cond_be

    const/16 v23, 0x1

    sub-int v23, v4, v23

    move/from16 v9, v23

    .line 233
    .local v9, div:I
    :goto_a8
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v10, v19, v23

    .line 234
    .local v10, focusX:F
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v11, v20, v23

    .line 237
    .local v11, focusY:F
    const/4 v5, 0x0

    .local v5, devSumX:F
    const/4 v6, 0x0

    .line 238
    .local v6, devSumY:F
    const/4 v12, 0x0

    :goto_b7
    if-ge v12, v4, :cond_df

    .line 239
    if-ne v14, v12, :cond_c0

    .line 238
    :goto_bb
    add-int/lit8 v12, v12, 0x1

    goto :goto_b7

    .end local v5           #devSumX:F
    .end local v6           #devSumY:F
    .end local v9           #div:I
    .end local v10           #focusX:F
    .end local v11           #focusY:F
    :cond_be
    move v9, v4

    .line 232
    goto :goto_a8

    .line 240
    .restart local v5       #devSumX:F
    .restart local v6       #devSumY:F
    .restart local v9       #div:I
    .restart local v10       #focusX:F
    .restart local v11       #focusY:F
    :cond_c0
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    sub-float v23, v23, v10

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    add-float v5, v5, v23

    .line 241
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    sub-float v23, v23, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    add-float v6, v6, v23

    goto :goto_bb

    .line 243
    :cond_df
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v7, v5, v23

    .line 244
    .local v7, devX:F
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v8, v6, v23

    .line 249
    .local v8, devY:F
    const/high16 v23, 0x4000

    mul-float v16, v7, v23

    .line 250
    .local v16, spanX:F
    const/high16 v23, 0x4000

    mul-float v17, v8, v23

    .line 251
    .local v17, spanY:F
    mul-float v23, v16, v16

    mul-float v24, v17, v17

    add-float v23, v23, v24

    invoke-static/range {v23 .. v23}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v15

    .line 256
    .local v15, span:F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v22, v0

    .line 257
    .local v22, wasInProgress:Z
    move v0, v10

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusX:F

    .line 258
    move v0, v11

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusY:F

    .line 259
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_137

    const/16 v23, 0x0

    cmpl-float v23, v15, v23

    if-eqz v23, :cond_11d

    if-eqz v3, :cond_137

    .line 260
    :cond_11d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Lcom/google/android/common/gesture/ScaleGestureDetector;)V

    .line 261
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    .line 262
    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    .line 264
    :cond_137
    if-eqz v3, :cond_160

    .line 265
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    .line 266
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    .line 267
    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    .line 269
    :cond_160
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-nez v23, :cond_1b8

    const/16 v23, 0x0

    cmpl-float v23, v15, v23

    if-eqz v23, :cond_1b8

    if-nez v22, :cond_182

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    move/from16 v23, v0

    sub-float v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    const/high16 v24, -0x4080

    cmpl-float v23, v23, v24

    if-lez v23, :cond_1b8

    .line 271
    :cond_182
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    .line 272
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    .line 273
    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleBegin(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    .line 278
    :cond_1b8
    const/16 v23, 0x2

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_20e

    .line 279
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    .line 280
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    .line 281
    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    .line 283
    const/16 v21, 0x1

    .line 284
    .local v21, updatePrev:Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1e8

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScale(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z

    move-result v21

    .line 288
    :cond_1e8
    if-eqz v21, :cond_20e

    .line 289
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    .line 290
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    .line 291
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    .line 295
    .end local v21           #updatePrev:Z
    :cond_20e
    const/16 v23, 0x1

    goto/16 :goto_43
.end method
