.class public final Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"


# static fields
.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x2

.field public static final OPTEDINTOPLUSONE_FIELD_NUMBER:I = 0x1

.field public static final THUMBNAILURL_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;


# instance fields
.field private displayName_:Ljava/lang/String;

.field private hasDisplayName:Z

.field private hasOptedIntoPlusOne:Z

.field private hasThumbnailUrl:Z

.field private memoizedSerializedSize:I

.field private optedIntoPlusOne_:Z

.field private thumbnailUrl_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1097
    new-instance v0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;-><init>(Z)V

    .line 1098
    sput-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 1100
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 772
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->optedIntoPlusOne_:Z

    .line 796
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->displayName_:Ljava/lang/String;

    .line 803
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->thumbnailUrl_:Ljava/lang/String;

    .line 827
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->memoizedSerializedSize:I

    .line 774
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/plus1/proto/a;)V
    .registers 2
    .parameter

    .prologue
    .line 769
    invoke-direct {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->optedIntoPlusOne_:Z

    .line 796
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->displayName_:Ljava/lang/String;

    .line 803
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->thumbnailUrl_:Ljava/lang/String;

    .line 827
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->memoizedSerializedSize:I

    .line 775
    return-void
.end method

.method static synthetic access$2202(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->optedIntoPlusOne_:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->displayName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl:Z

    return p1
.end method

.method static synthetic access$2702(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 769
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->thumbnailUrl_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 1

    .prologue
    .line 779
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    return-object v0
.end method

.method private initFields()V
    .registers 1

    .prologue
    .line 808
    return-void
.end method

.method public static newBuilder()Lcom/google/android/plus1/proto/b;
    .registers 1

    .prologue
    .line 916
    invoke-static {}, Lcom/google/android/plus1/proto/b;->g()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;
    .registers 2
    .parameter

    .prologue
    .line 919
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3
    .parameter

    .prologue
    .line 885
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    .line 886
    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->b(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 887
    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    .line 889
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 896
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    .line 897
    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/b;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 898
    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    .line 900
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 852
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 858
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 906
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 912
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 874
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->a(Ljava/io/InputStream;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 880
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/b;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 863
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/b;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 869
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/b;->a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/b;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2

    .prologue
    .line 783
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->displayName_:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptedIntoPlusOne()Z
    .registers 2

    .prologue
    .line 791
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->optedIntoPlusOne_:Z

    return v0
.end method

.method public final getSerializedSize()I
    .registers 4

    .prologue
    .line 829
    iget v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->memoizedSerializedSize:I

    .line 830
    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 846
    :goto_5
    return v0

    .line 832
    :cond_6
    const/4 v0, 0x0

    .line 833
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 834
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getOptedIntoPlusOne()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 837
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 838
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 841
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 842
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 845
    :cond_38
    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->memoizedSerializedSize:I

    goto :goto_5
.end method

.method public final getThumbnailUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->thumbnailUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public final hasDisplayName()Z
    .registers 2

    .prologue
    .line 797
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName:Z

    return v0
.end method

.method public final hasOptedIntoPlusOne()Z
    .registers 2

    .prologue
    .line 790
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne:Z

    return v0
.end method

.method public final hasThumbnailUrl()Z
    .registers 2

    .prologue
    .line 804
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 810
    const/4 v0, 0x1

    return v0
.end method

.method public final newBuilderForType()Lcom/google/android/plus1/proto/b;
    .registers 2

    .prologue
    .line 917
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilderForType()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/plus1/proto/b;
    .registers 2

    .prologue
    .line 921
    invoke-static {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->toBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getSerializedSize()I

    .line 816
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 817
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getOptedIntoPlusOne()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 819
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 820
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 822
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 823
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 825
    :cond_2d
    return-void
.end method
