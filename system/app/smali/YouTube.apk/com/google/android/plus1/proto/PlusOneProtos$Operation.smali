.class public final Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"


# static fields
.field public static final ABUSETOKEN_FIELD_NUMBER:I = 0x4

.field public static final TYPE_FIELD_NUMBER:I = 0x1

.field public static final URI_FIELD_NUMBER:I = 0x3

.field public static final VALUE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;


# instance fields
.field private abuseToken_:Ljava/lang/String;

.field private hasAbuseToken:Z

.field private hasType:Z

.field private hasUri:Z

.field private hasValue:Z

.field private memoizedSerializedSize:I

.field private type_:I

.field private uri_:Ljava/lang/String;

.field private value_:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1472
    new-instance v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;-><init>(Z)V

    .line 1473
    sput-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1475
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 1108
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1125
    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->type_:I

    .line 1132
    iput-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->value_:Z

    .line 1139
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->uri_:Ljava/lang/String;

    .line 1146
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->abuseToken_:Ljava/lang/String;

    .line 1173
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->memoizedSerializedSize:I

    .line 1110
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/plus1/proto/a;)V
    .registers 2
    .parameter

    .prologue
    .line 1105
    invoke-direct {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1111
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1125
    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->type_:I

    .line 1132
    iput-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->value_:Z

    .line 1139
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->uri_:Ljava/lang/String;

    .line 1146
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->abuseToken_:Ljava/lang/String;

    .line 1173
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->memoizedSerializedSize:I

    .line 1111
    return-void
.end method

.method static synthetic access$3102(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType:Z

    return p1
.end method

.method static synthetic access$3202(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->type_:I

    return p1
.end method

.method static synthetic access$3302(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue:Z

    return p1
.end method

.method static synthetic access$3402(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->value_:Z

    return p1
.end method

.method static synthetic access$3502(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->uri_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1105
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->abuseToken_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 1

    .prologue
    .line 1115
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    return-object v0
.end method

.method private initFields()V
    .registers 1

    .prologue
    .line 1151
    return-void
.end method

.method public static newBuilder()Lcom/google/android/plus1/proto/c;
    .registers 1

    .prologue
    .line 1266
    invoke-static {}, Lcom/google/android/plus1/proto/c;->g()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;)Lcom/google/android/plus1/proto/c;
    .registers 2
    .parameter

    .prologue
    .line 1269
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter

    .prologue
    .line 1235
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    .line 1236
    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->b(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1237
    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    .line 1239
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1246
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    .line 1247
    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/c;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1248
    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    .line 1250
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2
    .parameter

    .prologue
    .line 1202
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1208
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2
    .parameter

    .prologue
    .line 1256
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1262
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2
    .parameter

    .prologue
    .line 1224
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->a(Ljava/io/InputStream;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1230
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/c;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2
    .parameter

    .prologue
    .line 1213
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/c;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1219
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/c;->a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/c;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAbuseToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->abuseToken_:Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2

    .prologue
    .line 1119
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1105
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 4

    .prologue
    .line 1175
    iget v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->memoizedSerializedSize:I

    .line 1176
    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 1196
    :goto_5
    return v0

    .line 1178
    :cond_6
    const/4 v0, 0x0

    .line 1179
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1180
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1183
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 1184
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1187
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 1188
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1191
    :cond_38
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken()Z

    move-result v1

    if-eqz v1, :cond_48

    .line 1192
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getAbuseToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1195
    :cond_48
    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->memoizedSerializedSize:I

    goto :goto_5
.end method

.method public final getType()I
    .registers 2

    .prologue
    .line 1127
    iget v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->type_:I

    return v0
.end method

.method public final getUri()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Z
    .registers 2

    .prologue
    .line 1134
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->value_:Z

    return v0
.end method

.method public final hasAbuseToken()Z
    .registers 2

    .prologue
    .line 1147
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken:Z

    return v0
.end method

.method public final hasType()Z
    .registers 2

    .prologue
    .line 1126
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType:Z

    return v0
.end method

.method public final hasUri()Z
    .registers 2

    .prologue
    .line 1140
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri:Z

    return v0
.end method

.method public final hasValue()Z
    .registers 2

    .prologue
    .line 1133
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue:Z

    return v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 1153
    const/4 v0, 0x1

    return v0
.end method

.method public final newBuilderForType()Lcom/google/android/plus1/proto/c;
    .registers 2

    .prologue
    .line 1267
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1105
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilderForType()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/plus1/proto/c;
    .registers 2

    .prologue
    .line 1271
    invoke-static {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1105
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->toBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 1158
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getSerializedSize()I

    .line 1159
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1160
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    .line 1162
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1163
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 1165
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 1166
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 1168
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 1169
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getAbuseToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 1171
    :cond_3b
    return-void
.end method
