.class public final Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"


# static fields
.field public static final ABUSETOKEN_FIELD_NUMBER:I = 0x5

.field public static final FRIEND_FIELD_NUMBER:I = 0x4

.field public static final TOTALPLUSONECOUNT_FIELD_NUMBER:I = 0x3

.field public static final URI_FIELD_NUMBER:I = 0x1

.field public static final VALUE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;


# instance fields
.field private abuseToken_:Ljava/lang/String;

.field private friend_:Ljava/util/List;

.field private hasAbuseToken:Z

.field private hasTotalPlusOneCount:Z

.field private hasUri:Z

.field private hasValue:Z

.field private memoizedSerializedSize:I

.field private totalPlusOneCount_:I

.field private uri_:Ljava/lang/String;

.field private value_:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 761
    new-instance v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;-><init>(Z)V

    .line 762
    sput-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 764
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 311
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 328
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->uri_:Ljava/lang/String;

    .line 335
    iput-boolean v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->value_:Z

    .line 342
    iput v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->totalPlusOneCount_:I

    .line 348
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    .line 361
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->abuseToken_:Ljava/lang/String;

    .line 391
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->memoizedSerializedSize:I

    .line 313
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/plus1/proto/a;)V
    .registers 2
    .parameter

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 314
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 328
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->uri_:Ljava/lang/String;

    .line 335
    iput-boolean v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->value_:Z

    .line 342
    iput v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->totalPlusOneCount_:I

    .line 348
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    .line 361
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->abuseToken_:Ljava/lang/String;

    .line 391
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->memoizedSerializedSize:I

    .line 314
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->uri_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->value_:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->totalPlusOneCount_:I

    return p1
.end method

.method static synthetic access$1702(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->abuseToken_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 1

    .prologue
    .line 318
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    return-object v0
.end method

.method private initFields()V
    .registers 1

    .prologue
    .line 366
    return-void
.end method

.method public static newBuilder()Lcom/google/android/plus1/proto/e;
    .registers 1

    .prologue
    .line 488
    invoke-static {}, Lcom/google/android/plus1/proto/e;->g()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Lcom/google/android/plus1/proto/e;
    .registers 2
    .parameter

    .prologue
    .line 491
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter

    .prologue
    .line 457
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    .line 458
    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->b(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 459
    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    .line 461
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 468
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    .line 469
    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/e;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 470
    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    .line 472
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2
    .parameter

    .prologue
    .line 424
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 430
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2
    .parameter

    .prologue
    .line 478
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 484
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2
    .parameter

    .prologue
    .line 446
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->a(Ljava/io/InputStream;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 452
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/e;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2
    .parameter

    .prologue
    .line 435
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/e;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 441
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/e;->a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/e;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAbuseToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->abuseToken_:Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2

    .prologue
    .line 322
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public final getFriend(I)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    return-object v0
.end method

.method public final getFriendCount()I
    .registers 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getFriendList()Ljava/util/List;
    .registers 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 5

    .prologue
    .line 393
    iget v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->memoizedSerializedSize:I

    .line 394
    const/4 v0, -0x1

    if-eq v1, v0, :cond_6

    .line 418
    :goto_5
    return v1

    .line 396
    :cond_6
    const/4 v0, 0x0

    .line 397
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 398
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 401
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 402
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 406
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getTotalPlusOneCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 409
    :cond_38
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getFriendList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_41
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 410
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_41

    .line 413
    :cond_55
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 414
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getAbuseToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 417
    :cond_65
    iput v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->memoizedSerializedSize:I

    goto :goto_5
.end method

.method public final getTotalPlusOneCount()I
    .registers 2

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->totalPlusOneCount_:I

    return v0
.end method

.method public final getUri()Ljava/lang/String;
    .registers 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Z
    .registers 2

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->value_:Z

    return v0
.end method

.method public final hasAbuseToken()Z
    .registers 2

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken:Z

    return v0
.end method

.method public final hasTotalPlusOneCount()Z
    .registers 2

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount:Z

    return v0
.end method

.method public final hasUri()Z
    .registers 2

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri:Z

    return v0
.end method

.method public final hasValue()Z
    .registers 2

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue:Z

    return v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 368
    const/4 v0, 0x1

    return v0
.end method

.method public final newBuilderForType()Lcom/google/android/plus1/proto/e;
    .registers 2

    .prologue
    .line 489
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilderForType()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/plus1/proto/e;
    .registers 2

    .prologue
    .line 493
    invoke-static {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->toBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getSerializedSize()I

    .line 374
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 375
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 377
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 378
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 380
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 381
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getTotalPlusOneCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    .line 383
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getFriendList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_35
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 384
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    goto :goto_35

    .line 386
    :cond_46
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 387
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getAbuseToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 389
    :cond_54
    return-void
.end method
