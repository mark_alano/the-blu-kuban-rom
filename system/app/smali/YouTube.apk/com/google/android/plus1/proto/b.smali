.class public final Lcom/google/android/plus1/proto/b;
.super Lcom/google/protobuf/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 929
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/plus1/proto/b;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 923
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/b;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/b;->a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;
    .registers 4
    .parameter

    .prologue
    .line 1059
    if-nez p1, :cond_8

    .line 1060
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1062
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2402(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z

    .line 1063
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->displayName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2502(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Ljava/lang/String;)Ljava/lang/String;

    .line 1064
    return-object p0
.end method

.method private a(Z)Lcom/google/android/plus1/proto/b;
    .registers 4
    .parameter

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2202(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z

    .line 1042
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->optedIntoPlusOne_:Z
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2302(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z

    .line 1043
    return-object p0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;
    .registers 4
    .parameter

    .prologue
    .line 1080
    if-nez p1, :cond_8

    .line 1081
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1083
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2602(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Z)Z

    .line 1084
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->thumbnailUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->access$2702(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;Ljava/lang/String;)Ljava/lang/String;

    .line 1085
    return-object p0
.end method

.method static synthetic g()Lcom/google/android/plus1/proto/b;
    .registers 1

    .prologue
    .line 923
    invoke-static {}, Lcom/google/android/plus1/proto/b;->h()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/android/plus1/proto/b;
    .registers 3

    .prologue
    .line 932
    new-instance v0, Lcom/google/android/plus1/proto/b;

    invoke-direct {v0}, Lcom/google/android/plus1/proto/b;-><init>()V

    .line 933
    new-instance v1, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;-><init>(Lcom/google/android/plus1/proto/a;)V

    iput-object v1, v0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 934
    return-object v0
.end method

.method private i()Lcom/google/android/plus1/proto/b;
    .registers 3

    .prologue
    .line 951
    invoke-static {}, Lcom/google/android/plus1/proto/b;->h()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 3

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    if-nez v0, :cond_c

    .line 979
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 982
    :cond_c
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 983
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 984
    return-object v0
.end method

.method public final a(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;
    .registers 3
    .parameter

    .prologue
    .line 988
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 998
    :cond_6
    :goto_6
    return-object p0

    .line 989
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasOptedIntoPlusOne()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 990
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getOptedIntoPlusOne()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->a(Z)Lcom/google/android/plus1/proto/b;

    .line 992
    :cond_14
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 993
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;

    .line 995
    :cond_21
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->hasThumbnailUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 996
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;

    goto :goto_6
.end method

.method public final a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1006
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 1007
    sparse-switch v0, :sswitch_data_26

    .line 1011
    invoke-virtual {p1, v0}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1012
    :sswitch_d
    return-object p0

    .line 1017
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->a(Z)Lcom/google/android/plus1/proto/b;

    goto :goto_0

    .line 1021
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;

    goto :goto_0

    .line 1025
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/b;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/b;

    goto :goto_0

    .line 1007
    :sswitch_data_26
    .sparse-switch
        0x0 -> :sswitch_d
        0x8 -> :sswitch_e
        0x12 -> :sswitch_16
        0x1a -> :sswitch_1e
    .end sparse-switch
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 923
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 923
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 923
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 923
    invoke-direct {p0}, Lcom/google/android/plus1/proto/b;->i()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 923
    invoke-direct {p0}, Lcom/google/android/plus1/proto/b;->i()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 923
    invoke-direct {p0}, Lcom/google/android/plus1/proto/b;->i()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 923
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/b;->a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 923
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/b;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    invoke-static {v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/b;->a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 923
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/plus1/proto/b;->a:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->isInitialized()Z

    move-result v0

    return v0
.end method
