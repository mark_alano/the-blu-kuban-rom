.class public final Lcom/google/android/plus1/proto/c;
.super Lcom/google/protobuf/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 1279
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/plus1/proto/c;)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 2
    .parameter

    .prologue
    .line 1273
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/c;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/c;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/google/android/plus1/proto/c;
    .registers 4
    .parameter

    .prologue
    .line 1398
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3102(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z

    .line 1399
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->type_:I
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3202(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;I)I

    .line 1400
    return-object p0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;
    .registers 4
    .parameter

    .prologue
    .line 1434
    if-nez p1, :cond_8

    .line 1435
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1437
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3502(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z

    .line 1438
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->uri_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3602(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Ljava/lang/String;)Ljava/lang/String;

    .line 1439
    return-object p0
.end method

.method private a(Z)Lcom/google/android/plus1/proto/c;
    .registers 4
    .parameter

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3302(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z

    .line 1417
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->value_:Z
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3402(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z

    .line 1418
    return-object p0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;
    .registers 4
    .parameter

    .prologue
    .line 1455
    if-nez p1, :cond_8

    .line 1456
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1458
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3702(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Z)Z

    .line 1459
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->abuseToken_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->access$3802(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;Ljava/lang/String;)Ljava/lang/String;

    .line 1460
    return-object p0
.end method

.method static synthetic g()Lcom/google/android/plus1/proto/c;
    .registers 1

    .prologue
    .line 1273
    invoke-static {}, Lcom/google/android/plus1/proto/c;->h()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/android/plus1/proto/c;
    .registers 3

    .prologue
    .line 1282
    new-instance v0, Lcom/google/android/plus1/proto/c;

    invoke-direct {v0}, Lcom/google/android/plus1/proto/c;-><init>()V

    .line 1283
    new-instance v1, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;-><init>(Lcom/google/android/plus1/proto/a;)V

    iput-object v1, v0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1284
    return-object v0
.end method

.method private i()Lcom/google/android/plus1/proto/c;
    .registers 3

    .prologue
    .line 1301
    invoke-static {}, Lcom/google/android/plus1/proto/c;->h()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    if-nez v0, :cond_c

    .line 1329
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1332
    :cond_c
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1333
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1334
    return-object v0
.end method

.method public final a(Lcom/google/android/plus1/proto/PlusOneProtos$Operation;)Lcom/google/android/plus1/proto/c;
    .registers 3
    .parameter

    .prologue
    .line 1338
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 1351
    :cond_6
    :goto_6
    return-object p0

    .line 1339
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasType()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1340
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(I)Lcom/google/android/plus1/proto/c;

    .line 1342
    :cond_14
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1343
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(Z)Lcom/google/android/plus1/proto/c;

    .line 1345
    :cond_21
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1346
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;

    .line 1348
    :cond_2e
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->hasAbuseToken()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1349
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getAbuseToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;

    goto :goto_6
.end method

.method public final a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/c;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1359
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 1360
    sparse-switch v0, :sswitch_data_2e

    .line 1364
    invoke-virtual {p1, v0}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1365
    :sswitch_d
    return-object p0

    .line 1370
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(I)Lcom/google/android/plus1/proto/c;

    goto :goto_0

    .line 1374
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(Z)Lcom/google/android/plus1/proto/c;

    goto :goto_0

    .line 1378
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;

    goto :goto_0

    .line 1382
    :sswitch_26
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/c;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/c;

    goto :goto_0

    .line 1360
    :sswitch_data_2e
    .sparse-switch
        0x0 -> :sswitch_d
        0x8 -> :sswitch_e
        0x10 -> :sswitch_16
        0x1a -> :sswitch_1e
        0x22 -> :sswitch_26
    .end sparse-switch
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 1273
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1273
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1273
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/google/android/plus1/proto/c;->i()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/google/android/plus1/proto/c;->i()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/google/android/plus1/proto/c;->i()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1273
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/c;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/c;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    invoke-static {v0}, Lcom/google/android/plus1/proto/c;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/c;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1273
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/google/android/plus1/proto/c;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->isInitialized()Z

    move-result v0

    return v0
.end method
