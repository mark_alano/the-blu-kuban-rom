.class public final Lcom/google/android/youtube/app/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final b:Lcom/google/android/youtube/core/async/av;

.field private final c:Lcom/google/android/youtube/core/async/c;

.field private final d:Lcom/google/android/youtube/core/async/c;

.field private final e:Lcom/google/android/youtube/core/model/UserAuth;

.field private f:Ljava/util/List;

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/av;Landroid/app/Activity;Lcom/google/android/youtube/core/async/c;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "activity cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v0, "gdataClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/a/b;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 52
    const-string v0, "requester cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/b;->b:Lcom/google/android/youtube/core/async/av;

    .line 53
    invoke-static {p3, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/a/b;->c:Lcom/google/android/youtube/core/async/c;

    .line 54
    const-string v0, "responseCallback cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/c;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/b;->d:Lcom/google/android/youtube/core/async/c;

    .line 56
    const-string v0, "userAuth cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/b;->e:Lcom/google/android/youtube/core/model/UserAuth;

    .line 57
    return-void
.end method

.method private a(ILcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 105
    if-gt p1, v0, :cond_4

    .line 117
    :cond_3
    return-void

    .line 109
    :cond_4
    iget-object v1, p2, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 110
    add-int/lit8 v2, p1, -0x1

    iput v2, p0, Lcom/google/android/youtube/app/a/b;->i:I

    .line 111
    :goto_17
    if-ge v0, p1, :cond_3

    .line 112
    iget v2, p0, Lcom/google/android/youtube/app/a/b;->h:I

    mul-int/2addr v2, v0

    add-int/lit8 v2, v2, 0x1

    .line 113
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "start-index"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 114
    iget-object v3, p0, Lcom/google/android/youtube/app/a/b;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/a/b;->e:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    .line 115
    iget-object v3, p0, Lcom/google/android/youtube/app/a/b;->b:Lcom/google/android/youtube/core/async/av;

    iget-object v4, p0, Lcom/google/android/youtube/app/a/b;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v3, v2, v4}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_17
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 60
    iget v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    if-nez v0, :cond_2e

    .line 61
    iput v3, p0, Lcom/google/android/youtube/app/a/b;->i:I

    .line 62
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "start-index"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/google/android/youtube/app/a/b;->e:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/google/android/youtube/app/a/b;->b:Lcom/google/android/youtube/core/async/av;

    iget-object v2, p0, Lcom/google/android/youtube/app/a/b;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 70
    :cond_2e
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/a/b;->d:Lcom/google/android/youtube/core/async/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/async/c;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    iget v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 30
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    if-ne v0, v3, :cond_2d

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->elementsPerPage:I

    iput v0, p0, Lcom/google/android/youtube/app/a/b;->h:I

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    iput v0, p0, Lcom/google/android/youtube/app/a/b;->g:I

    iget v0, p0, Lcom/google/android/youtube/app/a/b;->g:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/youtube/app/a/b;->h:I

    int-to-double v4, v2

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/a/b;->f:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/app/a/b;->a(ILcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_2d
    iget-object v0, p0, Lcom/google/android/youtube/app/a/b;->f:Ljava/util/List;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget v0, p0, Lcom/google/android/youtube/app/a/b;->i:I

    if-nez v0, :cond_4c

    new-instance v0, Lcom/google/android/youtube/core/model/Page;

    iget v1, p0, Lcom/google/android/youtube/app/a/b;->g:I

    iget v2, p0, Lcom/google/android/youtube/app/a/b;->h:I

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Page;->previousUri:Landroid/net/Uri;

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/youtube/app/a/b;->f:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/a/b;->d:Lcom/google/android/youtube/core/async/c;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/youtube/core/async/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4c
    return-void
.end method
