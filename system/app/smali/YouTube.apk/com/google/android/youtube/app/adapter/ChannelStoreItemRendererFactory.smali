.class public final Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/WeakHashMap;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final f:Lcom/google/android/youtube/core/Analytics;

.field private final g:Lcom/google/android/youtube/core/b/al;

.field private final h:Lcom/google/android/youtube/app/a;

.field private final i:Lcom/google/android/youtube/core/d;

.field private final j:Lcom/google/android/youtube/app/adapter/u;

.field private final k:Ljava/util/Map;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/util/Map;

.field private final n:Lcom/google/android/youtube/app/ui/cx;

.field private final o:Lcom/google/android/youtube/core/async/c;

.field private final p:Landroid/app/Activity;

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cx;Landroid/app/Activity;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/youtube/core/Analytics;

    .line 101
    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/b/al;

    .line 102
    iput-object p7, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/youtube/core/d;

    .line 103
    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 104
    iput-object p5, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 105
    iput-object p6, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/youtube/app/a;

    .line 106
    new-instance v0, Lcom/google/android/youtube/app/adapter/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/u;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/youtube/app/adapter/u;

    .line 107
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    .line 111
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    .line 112
    iput-object p8, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cx;

    .line 113
    invoke-static {p9, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/youtube/core/async/c;

    .line 114
    iput-object p9, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    .line 115
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/adapter/u;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/youtube/app/adapter/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cx;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cx;->b(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Landroid/net/Uri;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    if-ne v0, p2, :cond_a

    .line 158
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    .line 162
    :goto_9
    return-void

    .line 160
    :cond_a
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    goto :goto_9
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return v0
.end method

.method private b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 527
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    .line 528
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method private static c(Landroid/net/Uri;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 533
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    const-string v2, "http"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    .line 129
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_a

    .line 131
    :cond_1a
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Z
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method private d()V
    .registers 6

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    .line 487
    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_a

    .line 488
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    iget-object v4, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_49

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_45
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_a

    :cond_49
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_45

    .line 491
    :cond_53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    .line 492
    return-void
.end method

.method private d(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    .line 538
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 539
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_a

    .line 542
    :cond_26
    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/YouTubeApplication;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/ui/cx;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cx;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/a;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/youtube/app/a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-direct {v0, p0, p1, p2, p0}, Lcom/google/android/youtube/app/adapter/z;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    if-nez v1, :cond_17

    .line 121
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    .line 122
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 124
    :cond_17
    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    .line 513
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 521
    invoke-static {p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    move-result-object v0

    .line 522
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    .line 524
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/core/model/UserProfile;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/adapter/w;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/w;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/core/model/UserProfile;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v1

    invoke-interface {v0, p4, p5, v1}, Lcom/google/android/youtube/core/b/al;->g(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 337
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 5
    .parameter

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    .line 507
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 9
    .parameter

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->x()Lcom/google/android/youtube/core/async/av;

    move-result-object v2

    .line 455
    sget-object v6, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h:Landroid/net/Uri;

    .line 456
    new-instance v0, Lcom/google/android/youtube/app/a/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/youtube/core/async/c;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/a/b;-><init>(Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/av;Landroid/app/Activity;Lcom/google/android/youtube/core/async/c;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 458
    invoke-virtual {v0, v6}, Lcom/google/android/youtube/app/a/b;->a(Landroid/net/Uri;)V

    .line 459
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 55
    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subscription;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    :cond_23
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 468
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    .line 469
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 135
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    .line 136
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    .line 518
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 463
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    .line 464
    return-void
.end method
