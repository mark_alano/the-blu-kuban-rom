.class public abstract Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
.super Lcom/google/android/youtube/app/adapter/n;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/an;

.field private final b:Lcom/google/android/youtube/core/utils/l;

.field private final c:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

.field private final d:Z

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 51
    const v0, 0x7f080040

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/adapter/n;-><init>(Landroid/content/Context;I)V

    .line 52
    const-string v0, "imageClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/an;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a:Lcom/google/android/youtube/core/b/an;

    .line 53
    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->b:Lcom/google/android/youtube/core/utils/l;

    .line 54
    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    .line 55
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 57
    if-nez v0, :cond_27

    move v0, v1

    .line 60
    :goto_24
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    .line 62
    return-void

    .line 60
    :cond_27
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v2, 0x1e0

    if-lt v0, v2, :cond_39

    move v0, v1

    goto :goto_24

    :cond_39
    const/4 v0, 0x0

    goto :goto_24
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected final a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/high16 v7, 0x3f00

    .line 100
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 102
    invoke-virtual {p3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    .line 103
    invoke-virtual {p3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v3

    .line 105
    invoke-virtual {p2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    sub-int v4, v2, v4

    .line 107
    invoke-virtual {p2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v5

    sub-int v5, v2, v5

    .line 113
    mul-int v2, v1, v5

    mul-int v6, v4, v3

    if-le v2, v6, :cond_48

    .line 114
    int-to-float v2, v5

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 115
    int-to-float v3, v4

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float v1, v3, v1

    mul-float/2addr v1, v7

    .line 120
    :goto_3b
    invoke-virtual {p1, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 121
    add-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 122
    return-void

    .line 117
    :cond_48
    int-to-float v2, v4

    int-to-float v1, v1

    div-float/2addr v2, v1

    .line 118
    int-to-float v1, v5

    int-to-float v3, v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    const v3, 0x3eb33333

    mul-float/2addr v1, v3

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_3b
.end method

.method protected a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->c(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v0

    .line 67
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->b(Ljava/lang/Object;)Landroid/net/Uri;

    .line 68
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v1

    .line 69
    if-eqz v0, :cond_47

    .line 71
    :goto_d
    if-eqz v0, :cond_46

    .line 72
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    if-eq v1, v2, :cond_3d

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    if-nez v1, :cond_49

    iget-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    if-eqz v1, :cond_49

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->f:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-lez v1, :cond_39

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->b:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/l;->f()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->f:J

    :cond_39
    iget-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    if-eqz v1, :cond_49

    .line 74
    :cond_3d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a:Lcom/google/android/youtube/core/b/an;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->b(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 79
    :cond_46
    :goto_46
    return-void

    :cond_47
    move-object v0, v1

    .line 69
    goto :goto_d

    .line 76
    :cond_49
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a:Lcom/google/android/youtube/core/b/an;

    invoke-interface {v1, v0, p3}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_46
.end method

.method protected abstract b(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected abstract c(Ljava/lang/Object;)Landroid/net/Uri;
.end method
