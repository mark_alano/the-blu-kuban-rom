.class public final Lcom/google/android/youtube/app/adapter/ac;
.super Lcom/google/android/youtube/app/adapter/be;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected final a:Lcom/google/android/youtube/core/a/e;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/e;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    const v0, 0x7f040020

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/app/adapter/be;-><init>(Landroid/app/Activity;ILcom/google/android/youtube/core/a/g;Ljava/lang/String;)V

    .line 32
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/be;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/e;->l()Z

    move-result v1

    if-eqz v1, :cond_19

    const v1, 0x7f020089

    .line 40
    :goto_12
    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 41
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    return-object v0

    .line 38
    :cond_19
    const v1, 0x7f020087

    goto :goto_12
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->m()V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/ac;->k()V

    .line 48
    return-void
.end method
