.class public final Lcom/google/android/youtube/app/adapter/ad;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ad;->a:Landroid/content/res/Resources;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ad;)Landroid/content/res/Resources;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ad;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 30
    new-instance v1, Lcom/google/android/youtube/app/adapter/ae;

    const-string v0, "view cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/adapter/ae;-><init>(Lcom/google/android/youtube/app/adapter/ad;Landroid/view/View;)V

    return-object v1
.end method
