.class final Lcom/google/android/youtube/app/adapter/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ad;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/ad;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ae;->a:Lcom/google/android/youtube/app/adapter/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/ae;->b:Landroid/view/View;

    .line 43
    const v0, 0x7f08006c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f08006e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f08006d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f08006b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    .line 47
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 33
    check-cast p2, Lcom/google/android/youtube/core/model/Comment;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Comment;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_22

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Comment;->publishedDate:Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->a:Lcom/google/android/youtube/app/adapter/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/ad;->a(Lcom/google/android/youtube/app/adapter/ad;)Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/ab;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_22
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Comment;->content:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    if-eqz v0, :cond_3a

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    if-nez p1, :cond_3d

    const/16 v0, 0x8

    :goto_37
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3a
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->b:Landroid/view/View;

    return-object v0

    :cond_3d
    const/4 v0, 0x0

    goto :goto_37
.end method
