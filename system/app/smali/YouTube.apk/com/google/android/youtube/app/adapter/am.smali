.class public final Lcom/google/android/youtube/app/adapter/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/al;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/al;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/am;->a:Lcom/google/android/youtube/app/adapter/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const v0, 0x7f08007f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 36
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/am;->b:Landroid/view/View;

    .line 37
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 30
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/adapter/am;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/am;->a:Lcom/google/android/youtube/app/adapter/al;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/al;->a(Lcom/google/android/youtube/app/adapter/al;)Lcom/google/android/youtube/app/ui/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/am;->b:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/ui/s;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/am;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/am;->b:Landroid/view/View;

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 46
    return-void

    .line 45
    :cond_9
    const/16 v0, 0x8

    goto :goto_5
.end method
