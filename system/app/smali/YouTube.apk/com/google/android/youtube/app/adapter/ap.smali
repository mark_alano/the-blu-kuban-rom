.class public final Lcom/google/android/youtube/app/adapter/ap;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/google/android/youtube/app/adapter/cb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/cb;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 30
    const-string v0, "resources cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ap;->a:Landroid/content/res/Resources;

    .line 31
    const-string v0, "videoRendererFactory cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cb;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ap;->b:Lcom/google/android/youtube/app/adapter/cb;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ap;)Lcom/google/android/youtube/app/adapter/cb;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ap;->b:Lcom/google/android/youtube/app/adapter/cb;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ap;)Landroid/content/res/Resources;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ap;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/youtube/app/adapter/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/aq;-><init>(Lcom/google/android/youtube/app/adapter/ap;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .registers 5
    .parameter

    .prologue
    .line 37
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 38
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Event;

    .line 39
    iget-object v0, v0, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 41
    :cond_1b
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ap;->b:Lcom/google/android/youtube/app/adapter/cb;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/adapter/cb;->a(Ljava/lang/Iterable;)V

    .line 42
    return-void
.end method
