.class final Lcom/google/android/youtube/app/adapter/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ap;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/youtube/app/adapter/bs;

.field private final e:Landroid/text/SpannableStringBuilder;

.field private final f:Landroid/text/style/StyleSpan;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ap;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Lcom/google/android/youtube/app/adapter/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const v0, 0x7f080079

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->c:Landroid/widget/TextView;

    .line 62
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/ap;->a(Lcom/google/android/youtube/app/adapter/ap;)Lcom/google/android/youtube/app/adapter/cb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    invoke-interface {v0, v1, p3}, Lcom/google/android/youtube/app/adapter/cb;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->d:Lcom/google/android/youtube/app/adapter/bs;

    .line 63
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->e:Landroid/text/SpannableStringBuilder;

    .line 64
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->f:Landroid/text/style/StyleSpan;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ap;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/aq;-><init>(Lcom/google/android/youtube/app/adapter/ap;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 48
    check-cast p2, Lcom/google/android/youtube/core/model/Event;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aq;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Lcom/google/android/youtube/app/adapter/ap;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ap;->b(Lcom/google/android/youtube/app/adapter/ap;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    iget v2, v2, Lcom/google/android/youtube/core/model/Event$Action;->stringId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    sget-object v3, Lcom/google/android/youtube/core/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/youtube/core/model/Event$Action;

    if-ne v0, v3, :cond_7f

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Lcom/google/android/youtube/app/adapter/ap;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ap;->b(Lcom/google/android/youtube/app/adapter/ap;)Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0006

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2a
    const-string v3, "%1$s"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4c

    const-string v4, "%1$s"

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/aq;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/aq;->e:Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/aq;->f:Landroid/text/style/StyleSpan;

    const/16 v5, 0x21

    invoke-virtual {v2, v4, v3, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4c
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->d:Lcom/google/android/youtube/app/adapter/bs;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bs;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Lcom/google/android/youtube/app/adapter/ap;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ap;->b(Lcom/google/android/youtube/app/adapter/ap;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:Landroid/view/View;

    return-object v0

    :cond_7f
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Event;->subject:Ljava/lang/String;

    goto :goto_2a
.end method
