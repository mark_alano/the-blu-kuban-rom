.class public Lcom/google/android/youtube/app/adapter/be;
.super Lcom/google/android/youtube/app/adapter/cc;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private c:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;ILcom/google/android/youtube/core/a/g;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040020

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/youtube/app/adapter/cc;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/a/g;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/app/Activity;

    .line 41
    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/be;->c:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/cc;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/be;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    new-instance v1, Lcom/google/android/youtube/app/ui/dh;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 54
    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/be;->c:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/be;->k()V

    .line 47
    return-void
.end method
