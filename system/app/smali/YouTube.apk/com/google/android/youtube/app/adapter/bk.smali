.class public final Lcom/google/android/youtube/app/adapter/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/plus1/ac;
.implements Lcom/google/android/youtube/app/adapter/cb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/app/b/g;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/WeakHashMap;

.field private final e:Landroid/os/Handler;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/b/g;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->a:Landroid/content/Context;

    .line 46
    const-string v0, "plusOneClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/b/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->b:Lcom/google/android/youtube/app/b/g;

    .line 47
    new-instance v0, Lcom/google/android/youtube/app/adapter/bl;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/bl;-><init>(Lcom/google/android/youtube/app/adapter/bk;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->e:Landroid/os/Handler;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->c:Ljava/util/Map;

    .line 54
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->d:Ljava/util/WeakHashMap;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->a()V

    goto :goto_a

    :cond_1a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/bk;->f:Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/bk;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/bk;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/youtube/app/adapter/bm;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Lcom/google/android/youtube/app/adapter/bk;Landroid/view/View;)V

    .line 88
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bk;->d:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-object v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .registers 5
    .parameter

    .prologue
    .line 58
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 59
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 60
    invoke-static {v0}, Lcom/google/android/youtube/plus1/c;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 62
    :cond_1d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->b:Lcom/google/android/youtube/app/b/g;

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/app/b/g;->a(Ljava/util/Set;Lcom/google/android/plus1/ac;)V

    .line 63
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/plus1/u;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/plus1/u;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/bk;->f:Z

    if-nez v0, :cond_16

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/bk;->f:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bk;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_16
    return-void
.end method
