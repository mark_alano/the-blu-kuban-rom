.class public final Lcom/google/android/youtube/app/adapter/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/cx;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 436
    new-instance v11, Lcom/google/android/youtube/app/adapter/bw;

    const v1, 0x7f08005a

    move-object/from16 v0, p7

    invoke-direct {v11, p0, v1, v0}, Lcom/google/android/youtube/app/adapter/bw;-><init>(Landroid/content/Context;ILcom/google/android/youtube/core/b/an;)V

    .line 448
    new-instance v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cx;Landroid/app/Activity;)V

    .line 460
    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v11}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    .line 465
    new-instance v3, Lcom/google/android/youtube/app/adapter/ab;

    const v4, 0x7f04001c

    invoke-direct {v3, p0, v4, v2, v1}, Lcom/google/android/youtube/app/adapter/ab;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-object v3
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 377
    new-instance v0, Lcom/google/android/youtube/app/adapter/bt;

    const v1, 0x7f04002d

    new-instance v2, Lcom/google/android/youtube/app/adapter/p;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/p;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 382
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    new-instance v8, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v8, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    .line 97
    new-instance v9, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v9, p0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    .line 98
    invoke-interface/range {p6 .. p6}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/google/android/youtube/app/adapter/ct;->a(Landroid/graphics/Typeface;)V

    .line 100
    invoke-static {p0, p2, p4}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v10

    .line 104
    new-instance v1, Lcom/google/android/youtube/app/adapter/h;

    move-object/from16 v0, p6

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v5

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p3

    move-object/from16 v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    .line 113
    new-instance v2, Lcom/google/android/youtube/app/adapter/bv;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/bv;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V

    .line 121
    new-instance v3, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v3, v8}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v1

    .line 129
    new-instance v2, Lcom/google/android/youtube/app/adapter/bt;

    const v3, 0x7f0400b6

    invoke-direct {v2, p0, v3, v1}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 133
    return-object v2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 523
    new-instance v0, Lcom/google/android/youtube/app/adapter/bx;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/bx;-><init>(Landroid/content/Context;)V

    .line 541
    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/youtube/app/adapter/bf;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bf;

    move-result-object v1

    .line 545
    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    .line 550
    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f040029

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 554
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/s;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 564
    new-instance v0, Lcom/google/android/youtube/app/adapter/bz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/bz;-><init>(Landroid/content/Context;)V

    .line 582
    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/youtube/app/adapter/bf;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bf;

    move-result-object v1

    .line 586
    new-instance v2, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v2, p2}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/s;)V

    .line 589
    new-instance v3, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    .line 595
    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f040029

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 599
    return-object v1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 278
    new-instance v1, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 279
    check-cast v0, Lcom/google/android/youtube/app/adapter/ct;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/ct;->a(Z)V

    .line 281
    sget-object v0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v0}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v0

    .line 285
    new-instance v2, Lcom/google/android/youtube/app/adapter/al;

    invoke-direct {v2, p2}, Lcom/google/android/youtube/app/adapter/al;-><init>(Lcom/google/android/youtube/app/ui/s;)V

    .line 288
    new-instance v3, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    .line 294
    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v1, p0, p3, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 298
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 182
    const-string v0, "prefetchStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/b/g;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/b/g;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 208
    new-instance v7, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v7, p0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    .line 210
    sget-object v0, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v0}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v8

    .line 214
    new-instance v0, Lcom/google/android/youtube/app/adapter/h;

    invoke-interface {p4, p0}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0xf

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    .line 223
    new-instance v1, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v1}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v1, v7}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    .line 229
    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f04002e

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 239
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    const-string v0, "prefetchStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    const v0, 0x7f04002b

    invoke-static {p0, p1, p5, v0}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    .line 169
    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/b/g;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 265
    const v0, 0x7f04002b

    invoke-static {p0, p1, p4, v0}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    return-object v0
.end method
