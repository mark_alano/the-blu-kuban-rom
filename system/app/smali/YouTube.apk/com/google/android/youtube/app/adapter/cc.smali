.class public Lcom/google/android/youtube/app/adapter/cc;
.super Lcom/google/android/youtube/core/a/m;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/a/g;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p3}, Lcom/google/android/youtube/core/a/m;-><init>(Lcom/google/android/youtube/core/a/g;)V

    .line 22
    const-string v0, "inflater cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Landroid/view/LayoutInflater;

    .line 23
    iput p2, p0, Lcom/google/android/youtube/app/adapter/cc;->c:I

    .line 24
    return-void
.end method


# virtual methods
.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    if-nez p2, :cond_b

    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cc;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 31
    :cond_b
    return-object p2
.end method
