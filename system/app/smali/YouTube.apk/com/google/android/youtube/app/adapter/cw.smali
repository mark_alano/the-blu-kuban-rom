.class final Lcom/google/android/youtube/app/adapter/cw;
.super Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/cv;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/adapter/cv;Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/cw;->a:Lcom/google/android/youtube/app/adapter/cv;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 52
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/cv;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    :goto_e
    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_12
    return-void

    :cond_13
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_e

    :cond_16
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_12
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 52
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 52
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
