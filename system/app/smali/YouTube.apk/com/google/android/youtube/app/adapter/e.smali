.class final Lcom/google/android/youtube/app/adapter/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/a;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/e;->a:Lcom/google/android/youtube/app/adapter/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/e;->b:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->b:Landroid/view/View;

    const v1, 0x7f080044

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->c:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->b:Landroid/view/View;

    const v1, 0x7f080045

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->d:Landroid/widget/TextView;

    .line 59
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 49
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/e;->a:Lcom/google/android/youtube/app/adapter/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/a;->a(Lcom/google/android/youtube/app/adapter/a;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b01d8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p2, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_33
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/e;->b:Landroid/view/View;

    return-object v0
.end method
