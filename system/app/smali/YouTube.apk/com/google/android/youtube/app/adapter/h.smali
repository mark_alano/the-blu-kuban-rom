.class public final Lcom/google/android/youtube/app/adapter/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/cb;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/WeakHashMap;

.field private final c:Lcom/google/android/youtube/core/b/ap;

.field private final d:Ljava/util/Set;

.field private final e:Lcom/google/android/youtube/core/async/l;

.field private final f:Lcom/google/android/youtube/app/prefetch/d;

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->a:Landroid/os/Handler;

    .line 71
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/h;->c:Lcom/google/android/youtube/core/b/ap;

    .line 72
    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/h;->f:Lcom/google/android/youtube/app/prefetch/d;

    .line 73
    iput-boolean p4, p0, Lcom/google/android/youtube/app/adapter/h;->g:Z

    .line 74
    if-eqz p5, :cond_57

    move v0, v1

    :goto_1a
    const-string v3, "Omit this renderer all together instead of using it with no badges"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 76
    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_59

    move v0, v1

    :goto_24
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->i:Z

    .line 77
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_5b

    move v0, v1

    :goto_2b
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->j:Z

    .line 78
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_5d

    move v0, v1

    :goto_32
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->k:Z

    .line 79
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_5f

    :goto_38
    iput-boolean v1, p0, Lcom/google/android/youtube/app/adapter/h;->l:Z

    .line 80
    iput-boolean p6, p0, Lcom/google/android/youtube/app/adapter/h;->h:Z

    .line 82
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->b:Ljava/util/WeakHashMap;

    .line 83
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->k:Z

    if-eqz v0, :cond_61

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->d:Ljava/util/Set;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->a:Landroid/os/Handler;

    invoke-static {v0, p0}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->e:Lcom/google/android/youtube/core/async/l;

    .line 90
    :goto_56
    return-void

    :cond_57
    move v0, v2

    .line 74
    goto :goto_1a

    :cond_59
    move v0, v2

    .line 76
    goto :goto_24

    :cond_5b
    move v0, v2

    .line 77
    goto :goto_2b

    :cond_5d
    move v0, v2

    .line 78
    goto :goto_32

    :cond_5f
    move v1, v2

    .line 79
    goto :goto_38

    .line 87
    :cond_61
    iput-object v4, p0, Lcom/google/android/youtube/app/adapter/h;->d:Ljava/util/Set;

    .line 88
    iput-object v4, p0, Lcom/google/android/youtube/app/adapter/h;->e:Lcom/google/android/youtube/core/async/l;

    goto :goto_56
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->k:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->l:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/adapter/h;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/adapter/h;)Lcom/google/android/youtube/app/prefetch/d;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->f:Lcom/google/android/youtube/app/prefetch/d;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/adapter/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->h:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/youtube/app/adapter/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/adapter/i;-><init>(Lcom/google/android/youtube/app/adapter/h;Landroid/view/View;)V

    .line 117
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/h;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .registers 5
    .parameter

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->k:Z

    if-eqz v0, :cond_26

    .line 94
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 95
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 96
    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 98
    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->c:Lcom/google/android/youtube/core/b/ap;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/h;->e:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/ap;->a(Ljava/util/List;Lcom/google/android/youtube/core/async/l;)V

    .line 100
    :cond_26
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 32
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 32
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/h;->d:Ljava/util/Set;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/i;->a()V

    goto :goto_24

    :cond_34
    return-void
.end method
