.class final Lcom/google/android/youtube/app/adapter/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/p;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/youtube/app/adapter/k;

.field private g:Lcom/google/android/youtube/core/async/l;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/p;Landroid/view/View;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const v4, 0x7f08005a

    .line 53
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/q;->a:Lcom/google/android/youtube/app/adapter/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/q;->b:Landroid/view/View;

    .line 55
    const v0, 0x7f080067

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->c:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f08008c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->d:Landroid/widget/TextView;

    .line 57
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->e:Landroid/widget/ImageView;

    .line 58
    new-instance v0, Lcom/google/android/youtube/app/adapter/r;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/p;->a(Lcom/google/android/youtube/app/adapter/p;)Landroid/content/Context;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/r;-><init>(Lcom/google/android/youtube/app/adapter/q;Landroid/content/Context;Landroid/view/View;ILcom/google/android/youtube/app/adapter/p;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->f:Lcom/google/android/youtube/app/adapter/k;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/p;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/q;-><init>(Lcom/google/android/youtube/app/adapter/p;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/q;)Lcom/google/android/youtube/core/async/l;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->g:Lcom/google/android/youtube/core/async/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/q;)Lcom/google/android/youtube/app/adapter/k;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->f:Lcom/google/android/youtube/app/adapter/k;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 45
    check-cast p2, Lcom/google/android/youtube/core/model/Channel;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Channel;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/q;->a:Lcom/google/android/youtube/app/adapter/p;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/p;->a(Lcom/google/android/youtube/app/adapter/p;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01ca

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p2, Lcom/google/android/youtube/core/model/Channel;->videoCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/s;

    invoke-direct {v0, p0, p1, v5}, Lcom/google/android/youtube/app/adapter/s;-><init>(Lcom/google/android/youtube/app/adapter/q;IB)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->g:Lcom/google/android/youtube/core/async/l;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->a:Lcom/google/android/youtube/app/adapter/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/p;->d(Lcom/google/android/youtube/app/adapter/p;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Channel;->userProfileUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/q;->a:Lcom/google/android/youtube/app/adapter/p;

    invoke-static {v2}, Lcom/google/android/youtube/app/adapter/p;->c(Lcom/google/android/youtube/app/adapter/p;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/q;->g:Lcom/google/android/youtube/core/async/l;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/q;->b:Landroid/view/View;

    return-object v0
.end method
