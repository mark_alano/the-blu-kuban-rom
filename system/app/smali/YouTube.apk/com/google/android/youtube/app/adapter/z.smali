.class final Lcom/google/android/youtube/app/adapter/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private h:Lcom/google/android/youtube/core/model/UserProfile;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 164
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/youtube/app/adapter/z;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/z;->b:Landroid/view/View;

    .line 177
    const v0, 0x7f080066

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->d:Landroid/view/View;

    .line 178
    const v0, 0x7f080067

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->c:Landroid/widget/TextView;

    .line 179
    const v0, 0x7f080069

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->f:Landroid/widget/ProgressBar;

    .line 180
    const v0, 0x7f080068

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->d:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/adapter/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/z;->g:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    .line 184
    return-void
.end method

.method private a()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Z)Z

    .line 194
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->g:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 196
    :cond_2a
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->d:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    .line 200
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v0, v4, :cond_80

    :goto_57
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 201
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v0, v1, :cond_62

    if-nez v0, :cond_82

    :cond_62
    const/4 v1, 0x4

    :goto_63
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 203
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->e:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v1, :cond_84

    const v1, 0x7f020084

    :goto_6f
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 205
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/z;->f:Landroid/widget/ProgressBar;

    sget-object v3, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v0, v3, :cond_7a

    if-nez v0, :cond_88

    :cond_7a
    :goto_7a
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->b:Landroid/view/View;

    return-object v0

    :cond_80
    move v1, v2

    .line 200
    goto :goto_57

    :cond_82
    move v1, v2

    .line 201
    goto :goto_63

    .line 203
    :cond_84
    const v1, 0x7f020085

    goto :goto_6f

    .line 205
    :cond_88
    const/16 v2, 0x8

    goto :goto_7a
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/z;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/z;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/z;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 9
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-static {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v6

    new-instance v0, Lcom/google/android/youtube/app/adapter/t;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    const/4 v5, 0x0

    move-object v2, p0

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/t;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;ZB)V

    invoke-virtual {v6, v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 212
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;
    .registers 2
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 164
    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/z;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->f(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->B()I

    move-result v1

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    .line 235
    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v2, :cond_6e

    .line 236
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v2, "ChannelStore"

    invoke-static {v0, v2, v1, v4}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZ)V

    .line 237
    new-instance v0, Lcom/google/android/youtube/app/adapter/aa;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/aa;-><init>(Lcom/google/android/youtube/app/adapter/z;)V

    new-instance v1, Lcom/google/android/youtube/core/ui/x;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b019c

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/adapter/z;->h:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/x;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 242
    :goto_6d
    return-void

    .line 239
    :cond_6e
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/z;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v2, "ChannelStore"

    invoke-static {v0, v2, v1, v6}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZ)V

    .line 240
    invoke-direct {p0, v4}, Lcom/google/android/youtube/app/adapter/z;->a(Z)V

    goto :goto_6d
.end method
