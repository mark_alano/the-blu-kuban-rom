.class public abstract Lcom/google/android/youtube/app/compat/SupportActionBar;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .registers 4
    .parameter

    .prologue
    .line 81
    instance-of v0, p0, Lcom/google/android/youtube/app/compat/j;

    if-eqz v0, :cond_b

    .line 82
    check-cast p0, Lcom/google/android/youtube/app/compat/j;

    invoke-interface {p0}, Lcom/google/android/youtube/app/compat/j;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    .line 85
    :goto_a
    return-object v0

    .line 84
    :cond_b
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_18

    .line 85
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    goto :goto_a

    .line 87
    :cond_18
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "support action bar not available for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .registers 3
    .parameter

    .prologue
    .line 92
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_d

    .line 93
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    .line 96
    :goto_c
    return-object v0

    .line 95
    :cond_d
    instance-of v0, p0, Lcom/google/android/youtube/app/compat/i;

    if-eqz v0, :cond_17

    .line 96
    new-instance v0, Lcom/google/android/youtube/app/compat/c;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/c;-><init>(Landroid/app/Activity;)V

    goto :goto_c

    .line 98
    :cond_17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sdk < 11 should implement SupportActionBarActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .registers 3
    .parameter

    .prologue
    .line 103
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 104
    if-nez v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/google/android/youtube/app/compat/a;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/compat/a;-><init>(Landroid/app/ActionBar;)V

    goto :goto_7
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/compat/h;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(Z)V
.end method

.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 3
    .parameter

    .prologue
    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public abstract b()V
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Lcom/google/android/youtube/app/compat/h;)V
.end method

.method public b(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 3
    .parameter

    .prologue
    .line 152
    const/4 v0, 0x1

    return v0
.end method

.method public abstract c()Z
.end method
