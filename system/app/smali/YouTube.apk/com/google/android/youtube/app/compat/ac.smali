.class public final Lcom/google/android/youtube/app/compat/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;
    .registers 3
    .parameter

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/youtube/app/compat/ad;

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/compat/ad;-><init>(Landroid/content/SharedPreferences$Editor;)V

    return-object v0
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_d

    .line 27
    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/app/compat/ac;->b(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 29
    :goto_c
    return-object v0

    :cond_d
    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/app/compat/ac;->c(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_c
.end method

.method private static b(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    :try_start_0
    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object p2

    .line 50
    :cond_4
    :goto_4
    return-object p2

    .line 43
    :catch_5
    move-exception v0

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/youtube/app/compat/ac;->c(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_4

    .line 49
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object p2, v0

    .line 50
    goto :goto_4
.end method

.method private static c(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-interface {p0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 75
    :cond_6
    :goto_6
    return-object p2

    .line 63
    :cond_7
    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_6

    .line 69
    :try_start_e
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->j(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 70
    instance-of v1, v0, Ljava/util/HashSet;

    if-nez v1, :cond_37

    .line 71
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can\'t cast "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "to HashSet"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :catch_35
    move-exception v0

    goto :goto_6

    .line 73
    :cond_37
    check-cast v0, Ljava/util/HashSet;
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_39} :catch_35

    move-object p2, v0

    goto :goto_6
.end method
