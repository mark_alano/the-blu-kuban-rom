.class public final Lcom/google/android/youtube/app/compat/c;
.super Lcom/google/android/youtube/app/compat/SupportActionBar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/ab;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/FrameLayout;

.field private j:Landroid/view/View;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private t:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x3

    const/4 v1, 0x1

    const/4 v8, -0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;-><init>()V

    .line 72
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    .line 73
    instance-of v0, p1, Lcom/google/android/youtube/app/compat/i;

    const-string v3, "activity must implement SupportActionBarActivity interface"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 76
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f08014d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1a1

    move v0, v1

    :goto_31
    const-string v3, "cannot find action_bar view"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f080150

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f08014e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->g:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->g:Landroid/view/View;

    new-instance v3, Lcom/google/android/youtube/app/compat/d;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/compat/d;-><init>(Lcom/google/android/youtube/app/compat/c;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f08014f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f0800f2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->d:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f080046

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f08009b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0c0032

    sget-object v3, Lcom/google/android/youtube/b;->j:[I

    invoke-virtual {p1, v0, v3}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    float-to-int v3, v3

    .line 99
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->d:Landroid/widget/ImageView;

    const v4, 0x7f0200ba

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 107
    const/4 v3, 0x2

    const v4, 0x7f090003

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 110
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 112
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 113
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v9, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/youtube/core/utils/Util;->a(FLandroid/content/res/Resources;)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 117
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    const/4 v5, 0x4

    const v6, 0x7f090002

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 125
    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    const/4 v5, 0x6

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 129
    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    const/4 v4, 0x7

    const v5, 0x7f09002c

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 139
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 141
    const v3, 0x7f0c0033

    sget-object v4, Lcom/google/android/youtube/b;->k:[I

    invoke-virtual {p1, v3, v4}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 146
    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->m:I

    .line 149
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->n:I

    .line 152
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->o:I

    .line 155
    const/4 v4, 0x6

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->p:I

    .line 158
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->q:I

    .line 160
    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/youtube/app/compat/c;->r:I

    .line 163
    invoke-virtual {v0, v1, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    .line 166
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 168
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    .line 169
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->l:Ljava/util/List;

    move-object v0, p1

    .line 171
    check-cast v0, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/i;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 173
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    move-object v1, p1

    .line 175
    check-cast v1, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v1}, Lcom/google/android/youtube/app/compat/i;->c_()Z

    move-result v1

    if-eqz v1, :cond_1a4

    .line 176
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 180
    :goto_192
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    const v0, 0x7f080151

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    .line 183
    return-void

    :cond_1a1
    move v0, v2

    .line 77
    goto/16 :goto_31

    .line 178
    :cond_1a4
    const v1, 0x7f08014d

    invoke-virtual {v0, v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_192
.end method

.method private static a(Landroid/content/Context;)I
    .registers 3
    .parameter

    .prologue
    .line 338
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 339
    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    const/4 v0, 0x5

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x3

    goto :goto_e
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/c;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    return-object v0
.end method

.method protected static a(Landroid/content/Context;Lcom/google/android/youtube/app/compat/m;)Ljava/util/List;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 371
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 372
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;)I

    move-result v4

    move v0, v1

    move v2, v1

    .line 374
    :goto_c
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v5

    if-ge v0, v5, :cond_29

    .line 375
    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v5

    .line 376
    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->i()Z

    move-result v6

    if-eqz v6, :cond_26

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_26

    .line 378
    add-int/lit8 v2, v2, 0x1

    .line 374
    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 381
    :cond_29
    :goto_29
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_54

    .line 382
    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    .line 383
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()Z

    move-result v5

    if-eqz v5, :cond_4d

    .line 384
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_4b

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_50

    if-ge v2, v4, :cond_50

    .line 389
    :cond_4b
    add-int/lit8 v2, v2, 0x1

    .line 381
    :cond_4d
    :goto_4d
    add-int/lit8 v1, v1, 0x1

    goto :goto_29

    .line 391
    :cond_50
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4d

    .line 394
    :cond_54
    return-object v3
.end method

.method private e()V
    .registers 11

    .prologue
    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 398
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 399
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_94

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/t;

    .line 400
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->e()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_84

    .line 402
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_75

    .line 403
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 404
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->h()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 406
    iget v2, p0, Lcom/google/android/youtube/app/compat/c;->r:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 407
    iget v2, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 408
    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v2, v4}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 415
    :goto_4f
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 419
    iget v4, p0, Lcom/google/android/youtube/app/compat/c;->q:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 420
    iget v4, p0, Lcom/google/android/youtube/app/compat/c;->m:I

    iget v5, p0, Lcom/google/android/youtube/app/compat/c;->o:I

    iget v6, p0, Lcom/google/android/youtube/app/compat/c;->n:I

    iget v7, p0, Lcom/google/android/youtube/app/compat/c;->p:I

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 425
    new-instance v4, Lcom/google/android/youtube/app/compat/e;

    iget-object v5, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v4, v5, v0}, Lcom/google/android/youtube/app/compat/e;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/compat/t;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v2

    .line 434
    :cond_6f
    :goto_6f
    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_d

    .line 411
    :cond_75
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 412
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4f

    .line 428
    :cond_84
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->e()Landroid/view/View;

    move-result-object v1

    .line 429
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 430
    if-nez v0, :cond_6f

    .line 431
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_6f

    .line 437
    :cond_94
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 309
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 232
    return-void
.end method

.method public final a(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 280
    and-int v0, p1, p2

    iget v1, p0, Lcom/google/android/youtube/app/compat/c;->t:I

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/compat/c;->t:I

    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->t:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_18
    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->t:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_23
    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->t:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 281
    :goto_2e
    return-void

    .line 280
    :cond_2f
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_18

    :cond_36
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_23

    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2e
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_28

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeX()Landroid/graphics/Shader$TileMode;

    move-result-object v1

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    if-ne v1, v2, :cond_28

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getTileModeY()Landroid/graphics/Shader$TileMode;

    move-result-object v1

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    if-ne v1, v2, :cond_28

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 291
    :cond_28
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_13

    new-instance v0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    :goto_f
    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V

    .line 212
    return-void

    .line 208
    :cond_13
    new-instance v0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(II)V

    goto :goto_f
.end method

.method public final a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 200
    if-eqz p1, :cond_c

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    :cond_c
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/c;->j:Landroid/view/View;

    .line 204
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/h;)V
    .registers 2
    .parameter

    .prologue
    .line 191
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x10

    .line 226
    if-eqz p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/compat/c;->a(II)V

    .line 227
    return-void

    .line 226
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 323
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 324
    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/z;

    .line 325
    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/compat/z;->a(Lcom/google/android/youtube/app/compat/ab;)V

    .line 323
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 327
    :cond_15
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 314
    return-void
.end method

.method public final b(I)V
    .registers 2
    .parameter

    .prologue
    .line 285
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/h;)V
    .registers 2
    .parameter

    .prologue
    .line 195
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 332
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;)I

    move-result v4

    move v0, v1

    move v2, v1

    :goto_e
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v5

    if-ge v0, v5, :cond_2b

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->i()Z

    move-result v6

    if-eqz v6, :cond_28

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_28

    add-int/lit8 v2, v2, 0x1

    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_2b
    :goto_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_59

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()Z

    move-result v5

    if-eqz v5, :cond_46

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_49

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_46
    :goto_46
    add-int/lit8 v1, v1, 0x1

    goto :goto_2b

    :cond_49
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->j()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_46

    if-ge v2, v4, :cond_46

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_46

    :cond_59
    iput-object v3, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    .line 333
    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/c;->e()V

    .line 334
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/i;->f_()V

    .line 187
    return-void
.end method
