.class final Lcom/google/android/youtube/app/compat/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/coreicecream/ui/g;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private final c:[Landroid/view/View;

.field private final d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;[Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/l;->a:Landroid/app/Activity;

    .line 57
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 58
    iput-object p2, p0, Lcom/google/android/youtube/app/compat/l;->c:[Landroid/view/View;

    .line 60
    const v0, 0x7f0c0032

    sget-object v1, Lcom/google/android/youtube/b;->j:[I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/app/compat/l;->d:I

    .line 63
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    const/16 v1, 0x400

    const/4 v2, 0x0

    .line 67
    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eq v0, p1, :cond_2f

    .line 68
    iput-boolean p1, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz p1, :cond_2b

    move v0, v1

    :goto_12
    invoke-virtual {v3, v0, v1}, Landroid/view/Window;->setFlags(II)V

    move v0, v2

    .line 72
    :goto_16
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/l;->c:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_2f

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/l;->c:[Landroid/view/View;

    aget-object v3, v1, v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eqz v1, :cond_2d

    iget v1, p0, Lcom/google/android/youtube/app/compat/l;->d:I

    :goto_25
    invoke-virtual {v3, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :cond_2b
    move v0, v2

    .line 69
    goto :goto_12

    :cond_2d
    move v1, v2

    .line 72
    goto :goto_25

    .line 74
    :cond_2f
    return-void
.end method

.method public final b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eqz v0, :cond_c

    if-eqz p1, :cond_c

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a()V

    .line 82
    :goto_b
    return-void

    .line 80
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    goto :goto_b
.end method
