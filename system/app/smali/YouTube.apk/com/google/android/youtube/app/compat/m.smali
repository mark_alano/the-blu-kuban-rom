.class public final Lcom/google/android/youtube/app/compat/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/Menu;

.field private c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Menu;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/m;->a:Landroid/content/Context;

    .line 33
    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Menu;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    .line 35
    const/4 v0, 0x0

    :goto_1f
    invoke-interface {p2}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_3f

    .line 36
    invoke-interface {p2, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 37
    if-eqz v1, :cond_3c

    .line 38
    iget-object v2, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 41
    :cond_3f
    return-void
.end method

.method private a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;
    .registers 5
    .parameter

    .prologue
    .line 46
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1d

    .line 47
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/youtube/app/compat/x;

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/app/compat/x;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    .line 53
    :goto_f
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-object v0

    .line 48
    :cond_1d
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2d

    .line 49
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/youtube/app/compat/w;

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/app/compat/w;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    goto :goto_f

    .line 51
    :cond_2d
    new-instance v0, Lcom/google/android/youtube/app/compat/z;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/app/compat/z;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    goto :goto_f
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/m;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/view/Menu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIII)Lcom/google/android/youtube/app/compat/t;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIILjava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/m;->a(Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 122
    return-void
.end method

.method public final a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 175
    return-void
.end method

.method public final a(IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/Menu;->setGroupCheckable(IZZ)V

    .line 171
    return-void
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->setQwertyMode(Z)V

    .line 183
    return-void
.end method

.method public final a(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/KeyEvent;I)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/SubMenu;
    .registers 4
    .parameter

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->addSubMenu(I)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(IIII)Landroid/view/SubMenu;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/Menu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 4
    .parameter

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    .line 126
    return-void
.end method

.method public final b(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 179
    return-void
.end method

.method public final c(I)Lcom/google/android/youtube/app/compat/t;
    .registers 4
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 130
    if-nez v0, :cond_a

    .line 131
    const/4 v0, 0x0

    .line 133
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/t;

    goto :goto_9
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    return v0
.end method

.method public final d(I)Lcom/google/android/youtube/app/compat/t;
    .registers 4
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 138
    if-nez v0, :cond_a

    .line 139
    const/4 v0, 0x0

    .line 141
    :goto_9
    return-object v0

    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/t;

    goto :goto_9
.end method

.method public final e(I)V
    .registers 3
    .parameter

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->removeGroup(I)V

    .line 162
    return-void
.end method

.method public final f(I)V
    .registers 4
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->removeItem(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/m;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    return-void
.end method
