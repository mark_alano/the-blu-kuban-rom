.class final Lcom/google/android/youtube/app/compat/q;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 118
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/q;->a:Landroid/view/LayoutInflater;

    .line 119
    iput-object p2, p0, Lcom/google/android/youtube/app/compat/q;->b:Ljava/util/List;

    .line 120
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/q;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 143
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 124
    if-nez p2, :cond_1d

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/q;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0400ae

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 129
    :goto_f
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/compat/q;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/t;

    .line 130
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->h()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-object p2

    .line 127
    :cond_1d
    check-cast p2, Landroid/widget/TextView;

    goto :goto_f
.end method
