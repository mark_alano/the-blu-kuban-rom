.class public final Lcom/google/android/youtube/app/compat/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/MenuInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuInflater;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/r;->a:Landroid/content/Context;

    .line 36
    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuInflater;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/r;->b:Landroid/view/MenuInflater;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/youtube/app/compat/m;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/r;->b:Landroid/view/MenuInflater;

    new-instance v1, Lcom/google/android/youtube/app/compat/s;

    invoke-direct {v1, p2}, Lcom/google/android/youtube/app/compat/s;-><init>(Lcom/google/android/youtube/app/compat/m;)V

    invoke-virtual {v0, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 45
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7e

    .line 46
    const/4 v1, 0x0

    :try_start_13
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/r;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v0

    :goto_21
    packed-switch v0, :pswitch_data_88

    :cond_24
    :goto_24
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    goto :goto_21

    :pswitch_29
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "item"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v2, "id"

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_24

    const-string v2, "http://schemas.android.com/apk/res/android"

    const-string v3, "showAsAction"

    const/4 v4, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_54

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v3

    if-eqz v3, :cond_54

    invoke-interface {v3, v2}, Lcom/google/android/youtube/app/compat/t;->c(I)V

    :cond_54
    const-string v2, "http://schemas.android.com/apk/res/android"

    const-string v3, "actionLayout"

    const/4 v4, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_24

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    if-eqz v0, :cond_24

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->a(I)Lcom/google/android/youtube/app/compat/t;
    :try_end_68
    .catchall {:try_start_13 .. :try_end_68} :catchall_72
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_13 .. :try_end_68} :catch_69
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_68} :catch_7f

    goto :goto_24

    :catch_69
    move-exception v0

    :try_start_6a
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_72
    .catchall {:try_start_6a .. :try_end_72} :catchall_72

    :catchall_72
    move-exception v0

    if-eqz v1, :cond_78

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_78
    throw v0

    :pswitch_79
    if-eqz v1, :cond_7e

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 48
    :cond_7e
    return-void

    .line 46
    :catch_7f
    move-exception v0

    :try_start_80
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_88
    .catchall {:try_start_80 .. :try_end_88} :catchall_72

    :pswitch_data_88
    .packed-switch 0x1
        :pswitch_79
        :pswitch_29
    .end packed-switch
.end method
