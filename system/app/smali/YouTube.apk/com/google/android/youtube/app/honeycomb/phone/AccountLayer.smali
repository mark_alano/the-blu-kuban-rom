.class public final Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;
.super Lcom/google/android/youtube/app/honeycomb/phone/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# static fields
.field private static m:Landroid/content/res/Resources;


# instance fields
.field private b:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private c:Lcom/google/android/youtube/core/model/UserAuth;

.field private d:Lcom/google/android/youtube/core/b/al;

.field private e:Lcom/google/android/youtube/core/b/an;

.field private f:Lcom/google/android/youtube/core/d;

.field private g:Lcom/google/android/youtube/app/ui/by;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ImageView;

.field private final n:Lcom/google/android/youtube/core/utils/l;

.field private o:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 95
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 96
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m:Landroid/content/res/Resources;

    .line 97
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/b/an;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n:Lcom/google/android/youtube/core/utils/l;

    .line 99
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/b/al;

    .line 100
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->f:Lcom/google/android/youtube/core/d;

    .line 101
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V
    .registers 1
    .parameter

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;Lcom/google/android/youtube/core/model/UserProfile;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->j:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p1, Lcom/google/android/youtube/core/model/UserProfile;->subscribersCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->k:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v3, p1, Lcom/google/android/youtube/core/model/UserProfile;->uploadViewsCount:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/i;

    invoke-direct {v3, p0, v5}, Lcom/google/android/youtube/app/honeycomb/phone/i;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    :goto_49
    return-void

    :cond_4a
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m()V

    goto :goto_49
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->f:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/app/ui/by;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/utils/l;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n:Lcom/google/android/youtube/core/utils/l;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 124
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    if-eqz v1, :cond_10

    .line 125
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 127
    :cond_10
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v1

    if-eqz v1, :cond_de

    .line 128
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040096

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    .line 134
    :goto_25
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 135
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    .line 136
    new-instance v0, Lcom/google/android/youtube/app/adapter/bt;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x7f04002a

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/c;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/c;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/by;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m:Landroid/content/res/Resources;

    const v1, 0x7f0a007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/a;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/a;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f080034

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_d0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d0016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    :cond_d0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->values()[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(Ljava/lang/Iterable;)V

    .line 137
    return-void

    .line 131
    :cond_de
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040001

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    goto/16 :goto_25
.end method

.method private m()V
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    const v1, 0x7f020187

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 187
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l()V

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_c

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 120
    :cond_c
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 4
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f110006

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 149
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->i:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/h;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/h;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;B)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/by;->notifyDataSetChanged()V

    .line 159
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 220
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 107
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 108
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/b/al;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/b/an;

    .line 110
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->f:Lcom/google/android/youtube/core/d;

    .line 111
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l()V

    .line 112
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 141
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->c()V

    .line 142
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l()V

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 144
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 217
    return-void
.end method
