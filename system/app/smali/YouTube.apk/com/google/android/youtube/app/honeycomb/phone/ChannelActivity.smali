.class public Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/bb;
.implements Lcom/google/android/youtube/app/ui/cr;


# static fields
.field private static final m:Ljava/util/Map;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/g;

.field private B:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private C:Ljava/lang/String;

.field private D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private E:Landroid/widget/ImageView;

.field private F:Landroid/widget/ProgressBar;

.field private G:Lcom/google/android/youtube/app/ui/by;

.field private H:Lcom/google/android/youtube/app/ui/by;

.field private I:Lcom/google/android/youtube/app/ui/by;

.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/av;

.field private p:Lcom/google/android/youtube/core/async/av;

.field private q:Lcom/google/android/youtube/core/async/av;

.field private r:Lcom/google/android/youtube/core/b/al;

.field private s:Lcom/google/android/youtube/core/b/ap;

.field private t:Lcom/google/android/youtube/core/b/an;

.field private u:Lcom/google/android/youtube/core/d;

.field private v:Lcom/google/android/youtube/core/j;

.field private w:Lcom/google/android/youtube/app/ui/eb;

.field private x:Lcom/google/android/youtube/app/ui/eb;

.field private y:Lcom/google/android/youtube/app/ui/am;

.field private z:Lcom/google/android/youtube/app/ui/ba;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 77
    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->m:Ljava/util/Map;

    const-string v1, "uploads"

    const v2, 0x7f080056

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->m:Ljava/util/Map;

    const-string v1, "favorites"

    const v2, 0x7f080057

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->m:Ljava/util/Map;

    const-string v1, "activity"

    const v2, 0x7f080058

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->m:Ljava/util/Map;

    const-string v1, "playlists"

    const v2, 0x7f080059

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 398
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 132
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;I)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selected_tab_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 127
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "username"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/app/ui/g;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)V
    .registers 7
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/am;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/am;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->z:Lcom/google/android/youtube/app/ui/ba;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ba;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v1, 0x7f0d000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 382
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_12

    .line 383
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 385
    :cond_12
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_1b

    .line 386
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 388
    :cond_1b
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_24

    .line 389
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 391
    :cond_24
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 145
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    .line 146
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/b/ap;

    .line 147
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->B:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 148
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    .line 149
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/b/an;

    .line 150
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    .line 151
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/j;

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->k()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Lcom/google/android/youtube/core/async/av;

    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->p()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->p:Lcom/google/android/youtube/core/async/av;

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->o()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->q:Lcom/google/android/youtube/core/async/av;

    .line 155
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 371
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/m;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_76

    .line 372
    :cond_14
    :goto_14
    return-void

    .line 371
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    const v1, 0x7f0b018c

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_14

    :pswitch_3c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    if-eqz v0, :cond_59

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    const v1, 0x7f0200c3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    const v1, 0x7f0b018b

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_14

    :pswitch_63
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    if-eqz v0, :cond_6c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_6c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_14

    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_15
        :pswitch_3c
        :pswitch_3c
        :pswitch_63
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/Playlist;)V
    .registers 5
    .parameter

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    .line 395
    iget-object v1, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;Z)V

    .line 396
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 340
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v0

    const v1, 0x7f080199

    if-ne v0, v1, :cond_10

    .line 341
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    .line 342
    const/4 v0, 0x1

    .line 344
    :goto_f
    return v0

    :cond_10
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_f
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 367
    const-string v0, "yt_channel"

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_9

    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    .line 351
    :cond_9
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 376
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 377
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->i()V

    .line 378
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 26
    .parameter

    .prologue
    .line 159
    invoke-super/range {p0 .. p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 160
    const v2, 0x7f040017

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->setContentView(I)V

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 163
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 164
    if-eqz v3, :cond_2ef

    .line 166
    invoke-static {v3}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    .line 171
    :goto_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 173
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->startActivity(Landroid/content/Intent;)V

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->finish()V

    .line 177
    :cond_33
    new-instance v2, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->B:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    const-string v9, "ChannelActivity"

    move-object/from16 v3, p0

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cr;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 180
    const v3, 0x7f080055

    const v4, 0x7f080054

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "selected_tab_id"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2fb

    const-string v5, "selected_tab_id"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    :goto_6b
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/youtube/core/ui/Workspace;->a(Landroid/app/Activity;III)Lcom/google/android/youtube/core/ui/Workspace;

    .line 183
    new-instance v2, Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/b/an;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v9

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v12

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/b/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/b/ap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/j;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v2

    .line 190
    new-instance v3, Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a0041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v5, 0x7f0a005d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v6, 0x7f0a005c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v7, 0x7f0a005d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 198
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->i()V

    .line 200
    new-instance v2, Lcom/google/android/youtube/app/ui/eb;

    const v3, 0x7f080056

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Lcom/google/android/youtube/core/async/av;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    const/4 v8, 0x1

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/youtube/app/m;->G:Lcom/google/android/youtube/core/b/aq;

    sget-object v13, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/eb;

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/eb;->d()V

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/b/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/b/ap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/j;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v2

    .line 216
    new-instance v3, Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a0041

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v5, 0x7f0a005d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v6, 0x7f0a005c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v7, 0x7f0a005d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 224
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->i()V

    .line 226
    new-instance v2, Lcom/google/android/youtube/app/ui/eb;

    const v3, 0x7f080057

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->p:Lcom/google/android/youtube/core/async/av;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    const/4 v8, 0x1

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/youtube/app/m;->H:Lcom/google/android/youtube/core/b/aq;

    sget-object v13, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelFavorites:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/eb;->d()V

    .line 240
    new-instance v13, Lcom/google/android/youtube/app/ui/am;

    const v2, 0x7f080058

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/google/android/youtube/core/ui/g;

    new-instance v17, Lcom/google/android/youtube/app/adapter/an;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/an;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    move-object/from16 v19, v0

    sget-object v21, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelActivity:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v22, Lcom/google/android/youtube/app/m;->F:Lcom/google/android/youtube/core/b/aq;

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object v15, v9

    move-object/from16 v20, v12

    invoke-direct/range {v13 .. v23}, Lcom/google/android/youtube/app/ui/am;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Z)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/am;

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/am;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/am;->d()V

    .line 253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/b/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/b/ap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/j;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v2

    .line 259
    new-instance v3, Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v5, 0x7f0a005d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v6, 0x7f0a005c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v7, 0x7f0a005d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Landroid/content/res/Resources;

    const v4, 0x7f0a007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 267
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->i()V

    .line 269
    new-instance v2, Lcom/google/android/youtube/app/ui/ba;

    const v3, 0x7f080059

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Lcom/google/android/youtube/app/ui/by;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->q:Lcom/google/android/youtube/core/async/av;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/d;

    move-object/from16 v3, p0

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/ui/ba;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/bb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->z:Lcom/google/android/youtube/app/ui/ba;

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/n;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/android/youtube/app/honeycomb/phone/n;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;B)V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/b/al;->d(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 280
    const v2, 0x7f08005c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    if-eqz v2, :cond_2bc

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    :cond_2bc
    const v2, 0x7f08005b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Landroid/widget/ProgressBar;

    .line 288
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2ee

    .line 289
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 291
    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 292
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 294
    :cond_2ee
    return-void

    .line 168
    :cond_2ef
    const-string v3, "username"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    goto/16 :goto_1d

    .line 180
    :cond_2fb
    const-string v5, "selected_tab_name"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_31c

    const-string v5, "selected_tab_name"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->m:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_319

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/16 :goto_6b

    :cond_319
    const/4 v2, 0x0

    goto/16 :goto_6b

    :cond_31c
    const/4 v2, 0x0

    goto/16 :goto_6b
.end method
