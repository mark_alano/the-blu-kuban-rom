.class public Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/async/av;

.field private q:Lcom/google/android/youtube/core/b/an;

.field private r:Lcom/google/android/youtube/core/d;

.field private s:Lcom/google/android/youtube/core/Analytics;

.field private t:Lcom/google/android/youtube/app/ui/by;

.field private u:Lcom/google/android/youtube/core/ui/j;

.field private v:Landroid/net/Uri;

.field private w:Ljava/lang/String;

.field private x:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)Landroid/content/Intent;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "category"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 194
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 73
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->q:Lcom/google/android/youtube/core/b/an;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->r:Lcom/google/android/youtube/core/d;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->s:Lcom/google/android/youtube/core/Analytics;

    .line 76
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 166
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Z

    if-eqz v0, :cond_1c

    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    const/16 v3, 0x18

    invoke-virtual {v2, p1, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 172
    :goto_1b
    return-void

    .line 170
    :cond_1c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_1b
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->finish()V

    .line 182
    return-void
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 198
    const-string v0, "yt_channel"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->finish()V

    .line 177
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->i()V

    .line 188
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter

    .prologue
    const v10, 0x7f0a005d

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Landroid/net/Uri;

    .line 84
    const-string v1, "category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w:Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->RECOMMENDED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Z

    .line 86
    const v0, 0x7f0400a5

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->setContentView(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->a(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->s:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->r:Lcom/google/android/youtube/core/d;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->q:Lcom/google/android/youtube/core/b/an;

    new-instance v8, Lcom/google/android/youtube/app/honeycomb/phone/s;

    invoke-direct {v8, p0}, Lcom/google/android/youtube/app/honeycomb/phone/s;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;)V

    move-object v0, p0

    move-object v9, p0

    invoke-static/range {v0 .. v9}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/cx;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/ui/by;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->i()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Z

    if-eqz v0, :cond_c4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->f()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    :goto_a5
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/async/av;

    new-instance v0, Lcom/google/android/youtube/core/ui/j;

    const v1, 0x7f080133

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->r:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/core/ui/j;

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 91
    return-void

    .line 88
    :cond_c4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->e()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    goto :goto_a5
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 160
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 162
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 152
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/core/ui/j;

    if-eqz v0, :cond_c

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/j;->e()V

    .line 156
    :cond_c
    return-void
.end method
