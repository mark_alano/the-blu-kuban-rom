.class public Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/phone/an;
.implements Lcom/google/android/youtube/app/ui/cx;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private A:Landroid/app/ProgressDialog;

.field private B:Lcom/google/android/youtube/core/model/UserAuth;

.field private C:Lcom/google/android/youtube/core/async/av;

.field private D:Lcom/google/android/youtube/core/async/av;

.field private E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private F:Z

.field private G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

.field private H:Z

.field private I:Ljava/lang/String;

.field private J:Lcom/google/android/youtube/app/honeycomb/phone/br;

.field private m:Lcom/google/android/youtube/app/YouTubeApplication;

.field private n:Landroid/content/SharedPreferences;

.field private o:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private p:Lcom/google/android/youtube/core/d;

.field private q:Lcom/google/android/youtube/core/Analytics;

.field private r:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private s:Lcom/google/android/youtube/app/ui/Slider;

.field private t:Landroid/view/View;

.field private u:Lcom/google/android/youtube/app/honeycomb/phone/y;

.field private v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

.field private w:Lcom/google/android/youtube/app/honeycomb/phone/t;

.field private x:Lcom/google/android/youtube/app/honeycomb/phone/u;

.field private y:Lcom/google/android/youtube/app/compat/t;

.field private z:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 52
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x1400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;)Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 326
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    if-eqz p1, :cond_a

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    :goto_6
    invoke-interface {v1, v0, p2}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V

    .line 327
    return-void

    .line 326
    :cond_a
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    goto :goto_6
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 111
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)Lcom/google/android/youtube/app/honeycomb/phone/y;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    .line 407
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()V

    .line 408
    return-void
.end method

.method private c(I)V
    .registers 5
    .parameter

    .prologue
    .line 449
    const-string v0, ""

    .line 450
    sparse-switch p1, :sswitch_data_2e

    .line 479
    :goto_5
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GuideSelection"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 481
    return-void

    .line 452
    :sswitch_12
    const-string v0, "Channel"

    goto :goto_5

    .line 455
    :sswitch_15
    const-string v0, "Account"

    goto :goto_5

    .line 458
    :sswitch_18
    const-string v0, "Category"

    goto :goto_5

    .line 461
    :sswitch_1b
    const-string v0, "ChannelStore"

    goto :goto_5

    .line 464
    :sswitch_1e
    const-string v0, "Recommended"

    goto :goto_5

    .line 467
    :sswitch_21
    const-string v0, "Trending"

    goto :goto_5

    .line 470
    :sswitch_24
    const-string v0, "TheFeed"

    goto :goto_5

    .line 473
    :sswitch_27
    const-string v0, "Live"

    goto :goto_5

    .line 476
    :sswitch_2a
    const-string v0, "RemoteQueue"

    goto :goto_5

    .line 450
    nop

    :sswitch_data_2e
    .sparse-switch
        0x10 -> :sswitch_12
        0x20 -> :sswitch_15
        0x40 -> :sswitch_18
        0x80 -> :sswitch_1b
        0x100 -> :sswitch_1e
        0x200 -> :sswitch_21
        0x400 -> :sswitch_24
        0x800 -> :sswitch_27
        0x1000 -> :sswitch_2a
    .end sparse-switch
.end method

.method private i()V
    .registers 2

    .prologue
    .line 229
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    .line 230
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f_()V

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    if-nez v0, :cond_f

    .line 232
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->j()V

    .line 237
    :goto_e
    return-void

    .line 235
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    goto :goto_e
.end method

.method private j()V
    .registers 4

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V

    .line 241
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-direct {v0, p0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/an;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V

    .line 243
    return-void
.end method

.method private k()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    if-eqz v0, :cond_1c

    .line 319
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100003

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    .line 321
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->i(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->a(I)V

    .line 323
    :cond_1c
    return-void
.end method

.method private l()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 375
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 376
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V

    .line 377
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V

    .line 379
    :cond_15
    iput-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    .line 380
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f_()V

    .line 381
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 383
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    const-string v4, "user_signed_out"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2c

    move v0, v1

    :cond_2c
    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/at;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/at;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)V

    invoke-virtual {v2, p0, v0, v1, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;ZZLcom/google/android/youtube/core/async/bn;)V

    .line 403
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 349
    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 350
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Landroid/net/Uri;)V

    .line 592
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-eqz v0, :cond_12

    .line 593
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/t;->a(Landroid/net/Uri;)V

    .line 595
    :cond_12
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-eqz v0, :cond_d

    .line 607
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/t;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 609
    :cond_d
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 3
    .parameter

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 584
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-eqz v0, :cond_12

    .line 585
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/t;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 587
    :cond_12
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 202
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    .line 203
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f_()V

    .line 204
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->j()V

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Ljava/lang/String;

    .line 209
    :cond_20
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 223
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    .line 225
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->i()V

    .line 226
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 259
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 260
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v1

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_32

    const v0, 0x7f110010

    :goto_11
    invoke-virtual {v1, v0, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 262
    const v0, 0x7f080198

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 266
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    if-eqz v0, :cond_30

    .line 267
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    .line 268
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->onSearchRequested()Z

    .line 271
    :cond_30
    const/4 v0, 0x1

    return v0

    .line 260
    :cond_32
    const v0, 0x7f110001

    goto :goto_11
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 294
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v1

    packed-switch v1, :pswitch_data_54

    .line 305
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v1

    if-nez v1, :cond_16

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v1

    if-eqz v1, :cond_51

    :cond_16
    :goto_16
    return v0

    .line 296
    :pswitch_17
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_42

    .line 297
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "SignOut"

    const-string v3, "Menu"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    const-string v2, "user_signed_out"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/compat/ad;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a(Landroid/content/Context;)V

    goto :goto_16

    .line 300
    :cond_42
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "SignIn"

    const-string v3, "Menu"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v1, "THE_FEED"

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->b(Ljava/lang/String;)V

    goto :goto_16

    .line 305
    :cond_51
    const/4 v0, 0x0

    goto :goto_16

    .line 294
    nop

    :pswitch_data_54
    .packed-switch 0x7f080198
        :pswitch_17
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Z)Z
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 484
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    .line 485
    const-string v0, "THE_FEED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 486
    if-eqz p2, :cond_19

    .line 487
    const/16 v0, 0x400

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 489
    :cond_19
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bw;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/au;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/au;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bw;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/bz;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    .line 551
    :goto_25
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 552
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->k()V

    .line 554
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V

    .line 555
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/av;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/av;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/u;->a(Landroid/view/View$OnClickListener;)V

    .line 562
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    const-string v1, "show_channel_store_turorial"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 563
    if-nez p2, :cond_4e

    if-nez v1, :cond_57

    .line 564
    :cond_4e
    if-eqz p2, :cond_1a5

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SELECTION:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    :goto_52
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 565
    invoke-direct {p0, v7, p2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    .line 568
    :cond_57
    if-nez p2, :cond_62

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    if-eqz v0, :cond_62

    .line 570
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->k()V

    .line 573
    :cond_62
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->t:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->setContentView(Landroid/view/View;)V

    .line 575
    if-eqz v1, :cond_6e

    .line 576
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->l()V

    :cond_6e
    move v7, v3

    .line 579
    :goto_6f
    return v7

    .line 494
    :cond_70
    const-string v0, "ACCOUNT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 495
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 496
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_85

    .line 497
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->b(Ljava/lang/String;)V

    goto :goto_6f

    .line 500
    :cond_85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    if-nez v0, :cond_90

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    :cond_90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto :goto_25

    .line 502
    :cond_95
    const-string v0, "CHANNEL_STORE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 503
    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 504
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    const-string v1, "is:channel"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_b3

    .line 506
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->b(Ljava/lang/String;)V

    goto :goto_6f

    .line 509
    :cond_b3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-nez v0, :cond_be

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-direct {v0, p0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/t;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/ui/cx;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/t;

    :cond_be
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/t;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 511
    :cond_c4
    const-string v0, "REMOTE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 512
    const/16 v0, 0x1000

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 513
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/br;

    if-nez v0, :cond_e2

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/br;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/br;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/br;

    :cond_e2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/br;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 514
    :cond_e8
    const-string v0, "RECOMMENDED_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_127

    .line 515
    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 516
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/x;

    const v1, 0x7f0b0182

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/youtube/app/m;->g:Lcom/google/android/youtube/core/b/aq;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->C:Lcom/google/android/youtube/core/async/av;

    const/4 v1, 0x2

    new-array v6, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v1, v8}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->l(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    aput-object v1, v6, v7

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v8, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v11, v9, v11}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    aput-object v1, v6, v3

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/async/av;[Lcom/google/android/youtube/core/async/GDataRequest;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 528
    :cond_127
    const-string v0, "TRENDING_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15f

    .line 529
    if-eqz p2, :cond_136

    .line 530
    const/16 v0, 0x200

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 532
    :cond_136
    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/x;

    const v0, 0x7f0b0183

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/google/android/youtube/app/m;->e:Lcom/google/android/youtube/core/b/aq;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D:Lcom/google/android/youtube/core/async/av;

    new-array v10, v3, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v11, v2, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v10, v7

    move-object v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/async/av;[Lcom/google/android/youtube/core/async/GDataRequest;)V

    iput-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 540
    :cond_15f
    const-string v0, "LIVE_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_175

    .line 541
    const/16 v0, 0x800

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 542
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->j()V

    goto/16 :goto_6f

    .line 544
    :cond_175
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_193

    .line 545
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 546
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/l;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/l;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 548
    :cond_193
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    .line 549
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/o;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/cx;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    goto/16 :goto_25

    .line 564
    :cond_1a5
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    goto/16 :goto_52
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/br;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/br;->b(I)Landroid/app/Dialog;

    move-result-object v0

    .line 614
    if-eqz v0, :cond_9

    .line 617
    :goto_8
    return-object v0

    :cond_9
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_8
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    .line 428
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f_()V

    .line 430
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GuideExpanded"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    if-eq v0, v1, :cond_29

    .line 432
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 434
    :cond_29
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 435
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-eqz v0, :cond_d

    .line 600
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/t;->b(Landroid/net/Uri;)V

    .line 602
    :cond_d
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 276
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    .line 277
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->b(Lcom/google/android/youtube/app/compat/m;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    .line 279
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_23

    const v0, 0x7f0b00e7

    :goto_1a
    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/compat/t;->d(I)Lcom/google/android/youtube/app/compat/t;

    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 282
    :cond_22
    return v2

    .line 279
    :cond_23
    const v0, 0x7f0b00e6

    goto :goto_1a
.end method

.method public final b_()Z
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 287
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->UP:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 288
    invoke-direct {p0, v1, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    .line 289
    return v1
.end method

.method public final e_()V
    .registers 4

    .prologue
    const/4 v1, 0x4

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    .line 439
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 443
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->f_()V

    .line 444
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GuideCollapsed"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 446
    return-void
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 336
    const-string v0, "yt_home"

    return-object v0
.end method

.method protected final h()Z
    .registers 2

    .prologue
    .line 331
    const/4 v0, 0x1

    return v0
.end method

.method public final i_()V
    .registers 4

    .prologue
    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    const-string v1, "user_signed_out"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 215
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->i()V

    .line 219
    :goto_16
    return-void

    .line 217
    :cond_17
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()V

    goto :goto_16
.end method

.method public onBackPressed()V
    .registers 4

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    if-eqz v0, :cond_23

    .line 413
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->m()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 414
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->n()V

    .line 422
    :goto_15
    return-void

    .line 416
    :cond_16
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->BACK:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 417
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V

    goto :goto_15

    .line 420
    :cond_23
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onBackPressed()V

    goto :goto_15
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 312
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 313
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->k()V

    .line 314
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Landroid/content/res/Configuration;)V

    .line 315
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 119
    const v1, 0x7f08003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 120
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/as;

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;Landroid/view/View;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->setContentView(Landroid/view/View;)V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Landroid/content/SharedPreferences;

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/d;

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    .line 137
    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->t()Lcom/google/android/youtube/core/async/av;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->C:Lcom/google/android/youtube/core/async/av;

    .line 139
    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->j()Lcom/google/android/youtube/core/async/av;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D:Lcom/google/android/youtube/core/async/av;

    .line 140
    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 142
    invoke-static {p0}, Lcom/google/android/youtube/app/ui/cm;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/ui/Slider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->t:Landroid/view/View;

    .line 146
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    .line 147
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    .line 149
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    const v1, 0x7f0b01c2

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 164
    :goto_bb
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_cf

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 166
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    .line 168
    :cond_cf
    return-void

    .line 161
    :cond_d0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->i_()V

    goto :goto_bb
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 185
    const-string v0, "com.google.android.youtube.action.search"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 186
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->onSearchRequested()Z

    .line 196
    :goto_f
    return-void

    .line 189
    :cond_10
    const-string v0, "guide_selection"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 190
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "guide_selection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GuideSelectionByIntent"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_f

    .line 195
    :cond_30
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_f
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 247
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    .line 248
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/Slider;->c()V

    .line 249
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 253
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 254
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/Slider;->d()V

    .line 255
    return-void
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    if-eqz v0, :cond_1f

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->p()V

    .line 176
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/o;

    if-eqz v0, :cond_20

    .line 177
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->l()V

    .line 182
    :cond_1f
    :goto_1f
    return-void

    .line 178
    :cond_20
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    if-eqz v0, :cond_1f

    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/u;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/t;->a()V

    goto :goto_1f
.end method

.method public final z()V
    .registers 1

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->i_()V

    .line 355
    return-void
.end method
