.class public Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/ed;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/av;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/b/an;

.field private q:Lcom/google/android/youtube/core/b/ap;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/model/UserAuth;

.field private t:Lcom/google/android/youtube/core/d;

.field private u:Lcom/google/android/youtube/core/j;

.field private v:Lcom/google/android/youtube/app/ui/by;

.field private w:Lcom/google/android/youtube/app/ui/eb;

.field private x:Lcom/google/android/youtube/app/adapter/bt;

.field private y:Lcom/google/android/youtube/app/ui/s;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bc;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bc;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->t:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 198
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 73
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->p:Lcom/google/android/youtube/core/b/an;

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->v()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->n:Lcom/google/android/youtube/core/async/av;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->q:Lcom/google/android/youtube/core/b/ap;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->t:Lcom/google/android/youtube/core/d;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->u:Lcom/google/android/youtube/core/j;

    .line 78
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 139
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/youtube/app/m;->J:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/b/aq;)V

    .line 151
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->finish()V

    .line 147
    return-void
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 155
    packed-switch p1, :pswitch_data_c

    .line 161
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 158
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 155
    :pswitch_data_c
    .packed-switch 0x3f0
        :pswitch_5
    .end packed-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 167
    const-string v0, "yt_your_channel"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->finish()V

    .line 143
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->a()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->i()V

    .line 192
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter

    .prologue
    const v6, 0x7f0a005d

    .line 82
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    const v0, 0x7f040076

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->setContentView(I)V

    .line 85
    const v0, 0x7f0b0190

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->e(I)V

    .line 87
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3f0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b01e2

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    .line 90
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 101
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->p:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->q:Lcom/google/android/youtube/core/b/ap;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->u:Lcom/google/android/youtube/core/j;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->y:Lcom/google/android/youtube/app/ui/s;

    const v5, 0x7f04002b

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    .line 104
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 112
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->i()V

    .line 114
    new-instance v0, Lcom/google/android/youtube/app/ui/eb;

    const v1, 0x7f080057

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/g;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->n:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->t:Lcom/google/android/youtube/core/d;

    const/4 v6, 0x1

    move-object v1, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/ui/ed;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    .line 122
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 132
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 134
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    .line 128
    return-void
.end method
