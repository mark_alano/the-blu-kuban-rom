.class public Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/youtube/app/ui/av;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/eb;

.field private B:Lcom/google/android/youtube/app/ui/n;

.field private C:Landroid/widget/Spinner;

.field private D:Landroid/widget/Spinner;

.field private E:Lcom/google/android/youtube/app/ui/at;

.field private F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

.field private G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

.field private H:Landroid/view/View;

.field private I:Lcom/google/android/youtube/core/ui/PagedListView;

.field private J:Lcom/google/android/youtube/core/ui/PagedListView;

.field private K:Lcom/google/android/youtube/app/ui/s;

.field private L:Landroid/provider/SearchRecentSuggestions;

.field private M:Z

.field private N:Z

.field private O:Landroid/view/inputmethod/InputMethodManager;

.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Landroid/content/SharedPreferences;

.field private q:Lcom/google/android/youtube/core/async/av;

.field private r:Lcom/google/android/youtube/core/async/av;

.field private s:Lcom/google/android/youtube/core/b/an;

.field private t:Lcom/google/android/youtube/core/b/ap;

.field private u:Lcom/google/android/youtube/core/d;

.field private v:Lcom/google/android/youtube/core/j;

.field private w:Lcom/google/android/youtube/app/b/g;

.field private x:Lcom/google/android/youtube/app/ui/by;

.field private y:Lcom/google/android/youtube/app/ui/by;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private i()V
    .registers 14

    .prologue
    const v5, 0x7f0a005d

    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 170
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Landroid/widget/Spinner;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Lcom/google/android/youtube/app/ui/at;

    if-nez v0, :cond_ff

    .line 177
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    if-nez v0, :cond_31

    .line 178
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 180
    :cond_31
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    const v2, 0x7f0400d9

    invoke-static {p0, p0, v0, v1, v2}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/av;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Lcom/google/android/youtube/app/ui/at;

    .line 186
    :goto_3e
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x406

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b00f5

    const v2, 0x7f0200b5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    .line 189
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bs;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bs;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->s:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->t:Lcom/google/android/youtube/core/b/ap;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/j;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    new-instance v2, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v12}, Lcom/google/android/youtube/app/adapter/ct;->a(Z)V

    sget-object v3, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, v0, v3}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v0

    new-instance v3, Lcom/google/android/youtube/app/adapter/al;

    invoke-direct {v3, v1}, Lcom/google/android/youtube/app/adapter/al;-><init>(Lcom/google/android/youtube/app/ui/s;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v1}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f04002e

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 202
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 210
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->l()V

    .line 212
    new-instance v0, Lcom/google/android/youtube/app/ui/eb;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->q:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v7

    sget-object v9, Lcom/google/android/youtube/app/m;->K:Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->SearchResults:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Lcom/google/android/youtube/app/ui/eb;

    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v12, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 226
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->k()V

    .line 227
    return-void

    .line 183
    :cond_ff
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    goto/16 :goto_3e
.end method

.method private j()V
    .registers 10

    .prologue
    const v5, 0x7f0a005d

    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Landroid/widget/Spinner;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    .line 234
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    .line 236
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->s:Lcom/google/android/youtube/core/b/an;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    .line 239
    new-instance v1, Lcom/google/android/youtube/app/ui/by;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    .line 240
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 247
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->l()V

    .line 249
    new-instance v0, Lcom/google/android/youtube/app/ui/n;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->r:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/ui/n;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->B:Lcom/google/android/youtube/app/ui/n;

    .line 260
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->B:Lcom/google/android/youtube/app/ui/n;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/n;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 261
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->k()V

    .line 262
    return-void
.end method

.method private k()V
    .registers 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    .line 303
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->O:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 305
    return-void
.end method

.method private l()V
    .registers 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 357
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 359
    :cond_12
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_24

    .line 360
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 363
    :cond_24
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m:Landroid/content/res/Resources;

    .line 128
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Landroid/content/SharedPreferences;

    .line 129
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 131
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->s:Lcom/google/android/youtube/core/b/an;

    .line 132
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->t:Lcom/google/android/youtube/core/b/ap;

    .line 133
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/d;

    .line 134
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->c()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->q:Lcom/google/android/youtube/core/async/av;

    .line 135
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->d()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->r:Lcom/google/android/youtube/core/async/av;

    .line 136
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->g()Landroid/provider/SearchRecentSuggestions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Landroid/provider/SearchRecentSuggestions;

    .line 137
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/j;

    .line 138
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->o()Lcom/google/android/youtube/app/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->w:Lcom/google/android/youtube/app/b/g;

    .line 139
    return-void
.end method

.method public final synthetic a(Ljava/lang/Enum;)V
    .registers 5
    .parameter

    .prologue
    .line 60
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "TimeFilter"

    invoke-virtual {p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->i()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 6
    .parameter

    .prologue
    .line 309
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    .line 310
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v1

    .line 311
    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->M:Z

    if-nez v2, :cond_12

    .line 312
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;Z)V

    .line 314
    :cond_12
    return v0
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 319
    sparse-switch p1, :sswitch_data_14

    .line 325
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 321
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 323
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 319
    nop

    :sswitch_data_14
    .sparse-switch
        0x3fa -> :sswitch_5
        0x406 -> :sswitch_c
    .end sparse-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 345
    const-string v0, "yt_results"

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 350
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->a()V

    .line 352
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->l()V

    .line 353
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 144
    const v0, 0x7f040094

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->setContentView(I)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    .line 147
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->O:Landroid/view/inputmethod/InputMethodManager;

    .line 148
    const v0, 0x7f080048

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    .line 149
    const v0, 0x7f080053

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 150
    const v0, 0x7f080133

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 151
    const v0, 0x7f0800be

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Landroid/view/View;

    .line 153
    const v0, 0x7f080132

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Landroid/widget/Spinner;

    .line 154
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v0, 0x1090008

    invoke-direct {v1, p0, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 156
    const v0, 0x1090009

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 157
    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->values()[Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_65
    if-ge v0, v3, :cond_75

    aget-object v4, v2, v0

    .line 158
    iget v4, v4, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->nameRes:I

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_65

    .line 160
    :cond_75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->B()Lcom/google/android/youtube/app/honeycomb/phone/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a(Z)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 366
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v0

    if-ne v0, p3, :cond_c

    .line 367
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->i()V

    .line 371
    :cond_b
    :goto_b
    return-void

    .line 368
    :cond_c
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v0

    if-ne v0, p3, :cond_b

    .line 369
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->j()V

    goto :goto_b
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 266
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->k()V

    .line 268
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    .line 269
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const-string v2, "is:channel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const-string v2, "is:channel"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    .line 271
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    .line 273
    :cond_2e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const-string v2, "is:video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 274
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const-string v2, "is:video"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    .line 275
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    .line 277
    :cond_4c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-nez v0, :cond_9e

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    :goto_52
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    .line 279
    const-string v0, "hide_query"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->M:Z

    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Landroid/content/SharedPreferences;

    const-string v2, "no_search_history"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a1

    const/4 v0, 0x1

    :goto_67
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->N:Z

    .line 282
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->M:Z

    if-nez v0, :cond_82

    .line 283
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->N:Z

    if-eqz v0, :cond_79

    .line 284
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Landroid/provider/SearchRecentSuggestions;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_79
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;Z)V

    .line 289
    :cond_82
    const-string v0, "selected_time_filter"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    if-nez v0, :cond_94

    .line 291
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 294
    :cond_94
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-ne v0, v1, :cond_a3

    .line 295
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->j()V

    .line 299
    :cond_9d
    :goto_9d
    return-void

    .line 277
    :cond_9e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    goto :goto_52

    :cond_a1
    move v0, v1

    .line 280
    goto :goto_67

    .line 296
    :cond_a3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-ne v0, v1, :cond_9d

    .line 297
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->i()V

    goto :goto_9d
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter

    .prologue
    .line 375
    return-void
.end method
