.class public abstract Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/b;


# instance fields
.field private m:Landroid/app/ActionBar;

.field private n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

.field private o:Ljava/util/ArrayList;

.field private p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/honeycomb/phone/bv;)V
    .registers 4
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    const-string v1, "setWorkspace() must be called before addTab()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1c

    .line 81
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->d()V

    .line 84
    :cond_1c
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 3
    .parameter

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    if-eqz v0, :cond_9

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 100
    :cond_9
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 105
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v0

    const v1, 0x7f0801a1

    if-ne v0, v1, :cond_29

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->a()Ljava/lang/String;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_21

    const v1, 0x7f0b021e

    invoke-static {p0, v1, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_18
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 112
    const/4 v0, 0x1

    .line 115
    :goto_20
    return v0

    .line 108
    :cond_21
    const v0, 0x7f0b021d

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    .line 114
    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 115
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_20
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final c(I)V
    .registers 4
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    if-nez v0, :cond_19

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/o;->b(Z)V

    .line 72
    const v0, 0x7f080055

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->m:Landroid/app/ActionBar;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;->a(Landroid/app/Activity;ILandroid/app/ActionBar;)Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;->setOnTabSelectedListener(Lcom/google/android/youtube/core/ui/b;)V

    .line 74
    return-void

    .line 70
    :cond_19
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final d(I)V
    .registers 4
    .parameter

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->c()V

    .line 126
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/bv;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 128
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->r:Z

    if-eqz v0, :cond_1f

    .line 129
    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->c()V

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->d()V

    .line 132
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->f_()V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->q:Z

    .line 134
    return-void
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Lcom/google/android/youtube/coreicecream/ui/ActionBarWorkspace;

    return-object v0
.end method

.method public final j()I
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->q:Z

    .line 122
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/bv;

    .line 158
    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->b()V

    goto :goto_9

    .line 160
    :cond_19
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->m:Landroid/app/ActionBar;

    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->m:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    .line 44
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    if-eqz v0, :cond_c

    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->c()V

    .line 52
    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->r:Z

    .line 53
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->r:Z

    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->q:Z

    if-nez v0, :cond_13

    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bv;->d()V

    .line 62
    :cond_13
    return-void
.end method
