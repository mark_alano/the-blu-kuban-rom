.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/av;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/b/an;

.field private q:Lcom/google/android/youtube/core/b/ap;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/model/UserAuth;

.field private t:Lcom/google/android/youtube/core/d;

.field private u:Lcom/google/android/youtube/core/j;

.field private v:Lcom/google/android/youtube/app/ui/by;

.field private w:Lcom/google/android/youtube/app/ui/eb;

.field private x:Lcom/google/android/youtube/core/a/a;

.field private y:Lcom/google/android/youtube/app/ui/s;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;)Lcom/google/android/youtube/core/a/a;
    .registers 2
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cj;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/b/al;->e(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->t:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 207
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 72
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    .line 73
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/b/an;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->q:Lcom/google/android/youtube/core/b/ap;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->t:Lcom/google/android/youtube/core/d;

    .line 78
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->u:Lcom/google/android/youtube/core/j;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->r()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->n:Lcom/google/android/youtube/core/async/av;

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 152
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->finish()V

    .line 160
    return-void
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 164
    packed-switch p1, :pswitch_data_c

    .line 170
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 167
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 164
    :pswitch_data_c
    .packed-switch 0x402
        :pswitch_5
    .end packed-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 176
    const-string v0, "yt_your_channel"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->finish()V

    .line 156
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 198
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->a()V

    .line 200
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->i()V

    .line 201
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 14
    .parameter

    .prologue
    const v8, 0x7f0a005d

    const/4 v6, 0x1

    .line 84
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v0, 0x7f0400cb

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->setContentView(I)V

    .line 87
    const v0, 0x7f0b0192

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->e(I)V

    .line 89
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x402

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b01f0

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    .line 93
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/ci;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ci;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 104
    new-instance v7, Lcom/google/android/youtube/app/adapter/ce;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->q:Lcom/google/android/youtube/core/b/ap;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->u:Lcom/google/android/youtube/core/j;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/s;

    const v5, 0x7f04002b

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/android/youtube/app/adapter/ce;-><init>(Lcom/google/android/youtube/core/a/a;)V

    iput-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    .line 112
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 120
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->i()V

    .line 122
    new-instance v0, Lcom/google/android/youtube/app/ui/eb;

    const v1, 0x7f080188

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/g;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->n:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v7

    sget-object v9, Lcom/google/android/youtube/app/m;->S:Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->WatchHistory:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    .line 134
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 144
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 146
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    .line 140
    return-void
.end method
