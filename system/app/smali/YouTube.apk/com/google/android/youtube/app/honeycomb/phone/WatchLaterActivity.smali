.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/prefetch/f;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/av;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/b/an;

.field private q:Lcom/google/android/youtube/core/b/ap;

.field private r:Lcom/google/android/youtube/core/j;

.field private s:Lcom/google/android/youtube/app/prefetch/d;

.field private t:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private u:Lcom/google/android/youtube/core/model/UserAuth;

.field private v:Lcom/google/android/youtube/core/d;

.field private w:Lcom/google/android/youtube/app/ui/eb;

.field private x:Lcom/google/android/youtube/app/adapter/bt;

.field private y:Lcom/google/android/youtube/app/ui/by;

.field private z:Lcom/google/android/youtube/app/ui/s;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cm;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cm;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->u:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/b/al;->d(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 243
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 245
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    .line 78
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 79
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 80
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/b/an;

    .line 81
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->q:Lcom/google/android/youtube/core/b/ap;

    .line 82
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    .line 83
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/d;

    .line 84
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->r:Lcom/google/android/youtube/core/j;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->q()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->n:Lcom/google/android/youtube/core/async/av;

    .line 86
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->u:Lcom/google/android/youtube/core/model/UserAuth;

    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 187
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->finish()V

    .line 195
    return-void
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 199
    packed-switch p1, :pswitch_data_c

    .line 204
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 201
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 199
    :pswitch_data_c
    .packed-switch 0x3fe
        :pswitch_5
    .end packed-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 228
    const-string v0, "yt_your_channel"

    return-object v0
.end method

.method public final g_()V
    .registers 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 233
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->finish()V

    .line 191
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 237
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 238
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->i()V

    .line 239
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter

    .prologue
    const v12, 0x7f0a005d

    const v11, 0x7f0800c3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v0, 0x7f0400d0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->setContentView(I)V

    .line 93
    const v0, 0x7f0b0191

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->e(I)V

    .line 95
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3fe

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/s;

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b01ee

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/s;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/ck;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ck;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 109
    invoke-virtual {p0, v11}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->findViewById(I)Landroid/view/View;

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->q:Lcom/google/android/youtube/core/b/ap;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->r:Lcom/google/android/youtube/core/j;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/s;

    new-instance v9, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v9, p0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v6}, Lcom/google/android/youtube/app/adapter/ct;->a(Z)V

    sget-object v4, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, v0, v4}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v10

    new-instance v0, Lcom/google/android/youtube/app/adapter/h;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0x8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/al;

    invoke-direct {v1, v8}, Lcom/google/android/youtube/app/adapter/al;-><init>(Lcom/google/android/youtube/app/ui/s;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v9}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f04002b

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    .line 135
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 143
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->i()V

    .line 145
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cl;

    invoke-virtual {p0, v11}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/g;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->n:Lcom/google/android/youtube/core/async/av;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v8

    sget-object v10, Lcom/google/android/youtube/app/m;->R:Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->WatchLater:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move-object v2, p0

    move v9, v7

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/app/honeycomb/phone/cl;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    .line 164
    return-void
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->b(Lcom/google/android/youtube/app/prefetch/f;)V

    .line 169
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onDestroy()V

    .line 170
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 180
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 182
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    .line 176
    return-void
.end method
