.class final Lcom/google/android/youtube/app/honeycomb/phone/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/y;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 2
    .parameter

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->i(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    if-eqz v0, :cond_15

    .line 243
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    .line 268
    :cond_14
    :goto_14
    return-void

    .line 246
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->j(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_25

    .line 247
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "REMOTE"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 248
    :cond_25
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->k(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_35

    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "ACCOUNT"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 250
    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->l(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_45

    .line 251
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "THE_FEED"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 252
    :cond_45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->m(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_55

    .line 253
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "CHANNEL_STORE"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 254
    :cond_55
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->n(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_65

    .line 255
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "RECOMMENDED_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 256
    :cond_65
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->o(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_75

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "TRENDING_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 258
    :cond_75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->p(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    if-ne p3, v0, :cond_85

    .line 259
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    const-string v1, "LIVE_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_14

    .line 260
    :cond_85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->b(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_14

    .line 261
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->b(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subscription;

    .line 264
    if-eqz v0, :cond_14

    .line 265
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ac;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto/16 :goto_14
.end method
