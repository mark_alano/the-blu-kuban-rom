.class final Lcom/google/android/youtube/app/honeycomb/phone/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/cf;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/y;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/TextView;

.field private e:Lcom/google/android/youtube/app/remote/ax;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 5
    .parameter

    .prologue
    .line 723
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 724
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->y(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->b:Landroid/view/View;

    .line 725
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->b:Landroid/view/View;

    const v1, 0x7f080040

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->c:Landroid/widget/ImageView;

    .line 726
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->b:Landroid/view/View;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->d:Landroid/widget/TextView;

    .line 727
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .registers 8
    .parameter

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x7f0b0237

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->e:Lcom/google/android/youtube/app/remote/ax;

    invoke-interface {v5}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->c:Landroid/widget/ImageView;

    const v1, 0x7f0200af

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->d:Landroid/widget/TextView;

    const-string v3, "REMOTE"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/ax;)V
    .registers 2
    .parameter

    .prologue
    .line 746
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ao;->e:Lcom/google/android/youtube/app/remote/ax;

    .line 747
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 742
    const/4 v0, 0x1

    return v0
.end method
