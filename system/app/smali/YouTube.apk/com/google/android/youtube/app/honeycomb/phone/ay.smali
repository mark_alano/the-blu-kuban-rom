.class public final Lcom/google/android/youtube/app/honeycomb/phone/ay;
.super Lcom/google/android/youtube/app/honeycomb/phone/bv;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/youtube/app/adapter/bt;

.field private final e:Lcom/google/android/youtube/core/ui/j;

.field private final f:Lcom/google/android/youtube/app/ui/by;

.field private final g:Lcom/google/android/youtube/app/a;

.field private final h:Lcom/google/android/youtube/core/b/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;ILcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/d;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bv;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;)V

    .line 53
    const-string v0, "navigation can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/a;

    .line 54
    const-string v0, "referrer can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->h:Lcom/google/android/youtube/core/b/aq;

    .line 55
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    .line 57
    invoke-virtual {p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/PagedListView;

    .line 58
    new-instance v0, Lcom/google/android/youtube/app/adapter/bt;

    const v1, 0x7f04006a

    new-instance v2, Lcom/google/android/youtube/app/adapter/as;

    invoke-direct {v2, p1, p3, p4}, Lcom/google/android/youtube/app/adapter/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/app/adapter/bt;

    .line 63
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 66
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a005d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v5, 0x7f0a005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v6, 0x7f0a005d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 71
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e()V

    .line 73
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/az;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    invoke-interface {p3}, Lcom/google/android/youtube/core/b/al;->u()Lcom/google/android/youtube/core/async/av;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v6, p8

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/honeycomb/phone/az;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ay;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/p;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/ui/j;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/by;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 104
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    const-string v0, "yt_live"

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 5
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 90
    return-void
.end method

.method protected final b()V
    .registers 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e()V

    .line 100
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    if-eqz p1, :cond_21

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 109
    :goto_6
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_20

    .line 110
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/LiveEvent;

    .line 111
    if-eqz v0, :cond_20

    .line 112
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/a;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->h:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)V

    .line 115
    :cond_20
    return-void

    .line 107
    :cond_21
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/app/adapter/bt;

    goto :goto_6
.end method
