.class public final Lcom/google/android/youtube/app/honeycomb/phone/ba;
.super Lcom/google/android/youtube/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/v;
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;


# instance fields
.field private a:Lcom/google/android/youtube/app/compat/t;

.field private b:Lcom/google/android/youtube/app/compat/t;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 33
    const v0, 0x800001

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/a/a;-><init>(Landroid/content/Context;I)V

    .line 28
    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c:Z

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->d:Z

    .line 30
    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 4
    .parameter

    .prologue
    .line 37
    const v0, 0x7f08019c

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    .line 39
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->e()Landroid/view/View;

    move-result-object v0

    .line 40
    const v1, 0x800001

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/e;->a(Landroid/view/View;I)V

    .line 41
    const v0, 0x7f08019d

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b:Lcom/google/android/youtube/app/compat/t;

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    .line 43
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e:Z

    .line 66
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 67
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->f()V

    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c:Z

    .line 61
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 62
    return-void
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->d:Z

    .line 71
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 72
    return-void
.end method

.method protected final c(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_b

    .line 84
    :cond_a
    :goto_a
    return-void

    .line 79
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->d:Z

    if-nez v0, :cond_40

    :cond_13
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e:Z

    if-nez v0, :cond_40

    move v0, v1

    .line 80
    :goto_18
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_42

    if-eqz p1, :cond_42

    move v3, v1

    :goto_1f
    invoke-interface {v4, v3}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 81
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_44

    if-eqz p1, :cond_44

    move v3, v1

    :goto_29
    invoke-interface {v4, v3}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    .line 82
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_46

    if-eqz p1, :cond_46

    move v3, v1

    :goto_33
    invoke-interface {v4, v3}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 83
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_48

    if-eqz p1, :cond_48

    :goto_3c
    invoke-interface {v3, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_a

    :cond_40
    move v0, v2

    .line 79
    goto :goto_18

    :cond_42
    move v3, v2

    .line 80
    goto :goto_1f

    :cond_44
    move v3, v2

    .line 81
    goto :goto_29

    :cond_46
    move v3, v2

    .line 82
    goto :goto_33

    :cond_48
    move v1, v2

    .line 83
    goto :goto_3c
.end method

.method public final h_()V
    .registers 2

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c:Z

    .line 56
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 57
    return-void
.end method
