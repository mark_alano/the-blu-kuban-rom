.class public final Lcom/google/android/youtube/app/honeycomb/phone/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/a;


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 2
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;

    if-nez v0, :cond_11

    .line 56
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 58
    :cond_11
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->a(Landroid/content/Context;Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method public final a(Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 113
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 92
    return-void
.end method

.method public final a(Landroid/net/Uri;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 4
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->a(Landroid/content/Context;Lcom/google/android/youtube/core/model/Video;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 117
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 147
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/b/aq;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 107
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 102
    return-void
.end method

.method public final a(Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 98
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 30
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 62
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 66
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 139
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 143
    return-void
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method

.method public final i()V
    .registers 5

    .prologue
    .line 120
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_17

    .line 121
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    const-class v3, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 127
    :goto_16
    return-void

    .line 124
    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    const-class v3, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_16
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bn;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/LiveActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void
.end method
