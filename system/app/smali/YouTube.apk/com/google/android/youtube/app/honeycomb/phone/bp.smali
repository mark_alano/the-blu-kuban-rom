.class final Lcom/google/android/youtube/app/honeycomb/phone/bp;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

.field private final b:Lcom/google/android/youtube/core/model/Video;

.field private final d:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/d;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    .line 123
    invoke-direct {p0, p4}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/d;)V

    .line 124
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->b:Lcom/google/android/youtube/core/model/Video;

    .line 125
    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->d:Landroid/app/Activity;

    .line 126
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/bp;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->b:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->d:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->f(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 131
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 116
    const-string v0, "Error deleting from playlist"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->c:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/s;->a(Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->d:Landroid/app/Activity;

    const v1, 0x7f0b01fc

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bq;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bq;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/bp;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/core/utils/p;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bp;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/s;->a(Z)V

    return-void
.end method
