.class public final Lcom/google/android/youtube/app/honeycomb/phone/br;
.super Lcom/google/android/youtube/app/honeycomb/phone/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/aj;
.implements Lcom/google/android/youtube/app/ui/y;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final b:Lcom/google/android/youtube/core/b/al;

.field private final c:Lcom/google/android/youtube/core/b/an;

.field private final d:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final e:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final f:Landroid/view/View;

.field private final g:Lcom/google/android/youtube/core/ui/g;

.field private final h:Landroid/content/res/Resources;

.field private final i:Lcom/google/android/youtube/app/ui/by;

.field private j:Lcom/google/android/youtube/core/model/UserAuth;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/youtube/app/adapter/bt;

.field private m:Lcom/google/android/youtube/app/ui/bv;

.field private n:Lcom/google/android/youtube/app/ui/s;

.field private o:I

.field private final p:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 64
    const-string v0, "analytics can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->p:Lcom/google/android/youtube/core/Analytics;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 68
    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->h:Landroid/content/res/Resources;

    .line 69
    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->b:Lcom/google/android/youtube/core/b/al;

    .line 70
    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->c:Lcom/google/android/youtube/core/b/an;

    .line 71
    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->q()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 72
    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 74
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v2, 0x3ec

    invoke-direct {v0, p1, v2}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->n:Lcom/google/android/youtube/app/ui/s;

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->n:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->n:Lcom/google/android/youtube/app/ui/s;

    const v2, 0x7f0b0239

    const v3, 0x7f02008c

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->o:I

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->c:Lcom/google/android/youtube/core/b/an;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->n:Lcom/google/android/youtube/app/ui/s;

    new-instance v6, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v6, p1}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    new-instance v7, Lcom/google/android/youtube/app/adapter/bo;

    invoke-direct {v7, p1, v4, v5, p3}, Lcom/google/android/youtube/app/adapter/bo;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/ui/s;Lcom/google/android/youtube/core/Analytics;)V

    invoke-interface {v3}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/youtube/app/adapter/ct;->a(Landroid/graphics/Typeface;)V

    invoke-static {p1, v0, v2}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v6}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/bt;

    const v3, 0x7f04002b

    invoke-direct {v2, p1, v3, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->l:Lcom/google/android/youtube/app/adapter/bt;

    .line 86
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->l:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p1, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->i:Lcom/google/android/youtube/app/ui/by;

    .line 87
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/br;->m()V

    .line 88
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040093

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/br;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->f:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->f:Landroid/view/View;

    const v2, 0x7f080131

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->g:Lcom/google/android/youtube/core/ui/g;

    .line 92
    new-instance v0, Lcom/google/android/youtube/app/ui/bv;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->g:Lcom/google/android/youtube/core/ui/g;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->i:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->b:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v4}, Lcom/google/android/youtube/core/b/al;->z()Lcom/google/android/youtube/core/async/av;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v5

    move-object v1, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->m:Lcom/google/android/youtube/app/ui/bv;

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->m:Lcom/google/android/youtube/app/ui/bv;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bv;->a()V

    .line 101
    return-void
.end method

.method private b(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->m:Lcom/google/android/youtube/app/ui/bv;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bv;->a(Ljava/util/List;)V

    .line 169
    return-void
.end method

.method private m()V
    .registers 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->i:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->h:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 200
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;->a(Landroid/content/res/Configuration;)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/br;->m()V

    .line 188
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->l:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 126
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 4
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_a

    .line 155
    :cond_a
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 2
    .parameter

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->j:Lcom/google/android/youtube/core/model/UserAuth;

    .line 106
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->k:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 160
    :cond_c
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->k:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 164
    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->l:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 165
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->j:Lcom/google/android/youtube/core/model/UserAuth;

    .line 111
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/br;->b(Ljava/util/List;)V

    .line 121
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 177
    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 39
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->o:I

    if-ne p1, v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteQueueDeleteQueueLayer"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 191
    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_b

    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->n:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    .line 194
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final d()Ljava/lang/String;
    .registers 6

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b0237

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->h()V

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_13

    .line 147
    :goto_12
    return-void

    .line 142
    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/br;->b(Ljava/util/List;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/aj;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/aj;)V

    .line 146
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/br;->m()V

    goto :goto_12
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/br;->j:Lcom/google/android/youtube/core/model/UserAuth;

    .line 116
    return-void
.end method

.method public final l()V
    .registers 1

    .prologue
    .line 173
    return-void
.end method
