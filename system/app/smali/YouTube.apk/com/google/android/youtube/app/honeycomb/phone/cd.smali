.class final Lcom/google/android/youtube/app/honeycomb/phone/cd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

.field private final b:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1316
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1317
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->b:Lcom/google/android/youtube/core/model/UserAuth;

    .line 1318
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->e(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/d;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1312
    check-cast p2, Lcom/google/android/youtube/core/model/ArtistBundle;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->artistTape:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    const-string v0, "empty artist tape"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->e(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    :goto_21
    return-void

    :cond_22
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->artistTape:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2d

    :cond_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->b:Lcom/google/android/youtube/core/model/UserAuth;

    const/4 v5, 0x0

    move-object v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    goto :goto_21
.end method
