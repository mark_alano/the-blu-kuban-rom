.class final Lcom/google/android/youtube/app/honeycomb/phone/d;
.super Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

.field final synthetic b:Lcom/google/android/youtube/app/honeycomb/phone/c;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/d;->b:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iput-object p6, p0, Lcom/google/android/youtube/app/honeycomb/phone/d;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, p4, v0}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 275
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 275
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .registers 3
    .parameter

    .prologue
    .line 275
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
