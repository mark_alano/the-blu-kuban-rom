.class final Lcom/google/android/youtube/app/honeycomb/phone/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/c;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/youtube/app/adapter/k;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    const v0, 0x7f080082

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    .line 307
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    .line 308
    const v0, 0x7f080083

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    .line 309
    const v0, 0x7f080040

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    .line 310
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/c;->a(Lcom/google/android/youtube/app/honeycomb/phone/c;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->f:Lcom/google/android/youtube/app/adapter/k;

    .line 312
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 297
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/phone/e;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/e;)Lcom/google/android/youtube/app/adapter/k;
    .registers 2
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->f:Lcom/google/android/youtube/app/adapter/k;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 297
    check-cast p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    iget v1, p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->labelStringId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    iget v1, p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->defaultThumbnailId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_23
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_38

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/b;->a:[I

    invoke-virtual {p2}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_f6

    :cond_38
    :goto_38
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    return-object v0

    :pswitch_3b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/g;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_38

    :pswitch_60
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/g;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/youtube/core/b/al;->d(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_38

    :pswitch_85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/g;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/youtube/core/b/al;->b(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_38

    :pswitch_aa
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/g;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/youtube/core/b/al;->c(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto/16 :goto_38

    :pswitch_d0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/f;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/f;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/youtube/core/b/al;->e(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto/16 :goto_38

    nop

    :pswitch_data_f6
    .packed-switch 0x1
        :pswitch_60
        :pswitch_3b
        :pswitch_85
        :pswitch_d0
        :pswitch_aa
    .end packed-switch
.end method
