.class public final Lcom/google/android/youtube/app/honeycomb/phone/t;
.super Lcom/google/android/youtube/app/honeycomb/phone/u;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Lcom/google/android/youtube/app/ui/cx;

.field private final f:Lcom/google/android/youtube/core/b/al;

.field private final g:Lcom/google/android/youtube/core/b/an;

.field private final h:Lcom/google/android/youtube/app/a;

.field private final i:Lcom/google/android/youtube/core/d;

.field private final j:Lcom/google/android/youtube/core/Analytics;

.field private final k:Lcom/google/android/youtube/core/a/i;

.field private final l:Landroid/widget/ListView;

.field private m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/ui/cx;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->c:Landroid/view/LayoutInflater;

    .line 50
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->c:Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_62

    const v0, 0x7f040099

    move v1, v0

    :goto_15
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/t;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->d:Landroid/view/View;

    .line 53
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->e:Lcom/google/android/youtube/app/ui/cx;

    .line 54
    new-instance v0, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v0}, Lcom/google/android/youtube/core/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->k:Lcom/google/android/youtube/core/a/i;

    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->d:Landroid/view/View;

    const v1, 0x7f08006a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->l:Landroid/widget/ListView;

    .line 57
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->f:Lcom/google/android/youtube/core/b/al;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 60
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->h:Lcom/google/android/youtube/app/a;

    .line 61
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->g:Lcom/google/android/youtube/core/b/an;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->i:Lcom/google/android/youtube/core/d;

    .line 63
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->j:Lcom/google/android/youtube/core/Analytics;

    .line 64
    return-void

    .line 50
    :cond_62
    const v0, 0x7f04001d

    move v1, v0

    goto :goto_15
.end method

.method private l()V
    .registers 11

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->j:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->f:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->h:Lcom/google/android/youtube/app/a;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->i:Lcom/google/android/youtube/core/d;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->g:Lcom/google/android/youtube/core/b/an;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->e:Lcom/google/android/youtube/app/ui/cx;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->d:Landroid/view/View;

    invoke-static/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/cx;Landroid/view/View;)Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->k:Lcom/google/android/youtube/core/a/i;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->k:Lcom/google/android/youtube/core/a/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_9

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->b()V

    .line 102
    :cond_9
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/t;->l()V

    .line 79
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;)V

    .line 114
    :cond_9
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_9

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 126
    :cond_9
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_9

    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 108
    :cond_9
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/t;->l()V

    .line 69
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_9

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/t;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->b(Landroid/net/Uri;)V

    .line 120
    :cond_9
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->c()V

    .line 74
    return-void
.end method
