.class public final Lcom/google/android/youtube/app/honeycomb/tablet/ab;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/core/async/av;

.field private final i:Lcom/google/android/youtube/core/b/al;

.field private final j:Lcom/google/android/youtube/core/b/an;

.field private final k:Lcom/google/android/youtube/core/b/ap;

.field private final l:Lcom/google/android/youtube/core/d;

.field private final m:Z

.field private n:Ljava/lang/String;

.field private o:Landroid/net/Uri;

.field private p:Lcom/google/android/youtube/app/ui/dw;

.field private q:Lcom/google/android/youtube/core/ui/PagedGridView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private final u:Lcom/google/android/youtube/core/async/l;

.field private v:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 79
    const-string v5, "yt_playlist"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 81
    const v0, 0x7f0b0215

    invoke-virtual {p2, v0}, Landroid/app/Activity;->setTitle(I)V

    .line 83
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 84
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->l:Lcom/google/android/youtube/core/d;

    .line 85
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_6a

    .line 87
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/n;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/n;

    move-result-object v1

    .line 88
    if-eqz v1, :cond_66

    iget-object v0, v1, Lcom/google/android/youtube/core/utils/n;->a:Ljava/lang/String;

    :goto_2f
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->n:Ljava/lang/String;

    .line 89
    if-eqz v1, :cond_68

    iget-boolean v0, v1, Lcom/google/android/youtube/core/utils/n;->b:Z

    :goto_35
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->v:Z

    .line 94
    :goto_37
    if-eqz p4, :cond_3f

    const-string v0, "authenticate"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    :cond_3f
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    .line 97
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    .line 98
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->j:Lcom/google/android/youtube/core/b/an;

    .line 99
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->k:Lcom/google/android/youtube/core/b/ap;

    .line 101
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    if-eqz v0, :cond_75

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->i()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    :goto_5d
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->h:Lcom/google/android/youtube/core/async/av;

    .line 105
    invoke-static {p2, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->u:Lcom/google/android/youtube/core/async/l;

    .line 106
    return-void

    .line 88
    :cond_66
    const/4 v0, 0x0

    goto :goto_2f

    :cond_68
    move v0, v6

    .line 89
    goto :goto_35

    .line 91
    :cond_6a
    const-string v0, "playlist_uri"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    goto :goto_37

    .line 101
    :cond_75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->h()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    goto :goto_5d
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ab;)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/tablet/ab;)Z
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    return v0
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 110
    const v0, 0x7f0400dd

    return v0
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 19
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-super/range {p0 .. p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 116
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    if-nez v1, :cond_1f

    .line 117
    const-string v1, "Invalid intent: Playlist Uri or Playlist Id not set"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 118
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 121
    :cond_1f
    const v1, 0x7f080046

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->r:Landroid/widget/TextView;

    .line 122
    const v1, 0x7f0800c7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->s:Landroid/widget/TextView;

    .line 123
    const v1, 0x7f08008f

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    .line 125
    const v1, 0x7f080053

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ui/PagedGridView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->q:Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 126
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->q:Lcom/google/android/youtube/core/ui/PagedGridView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->a(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 127
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/ac;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->q:Lcom/google/android/youtube/core/ui/PagedGridView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->h:Lcom/google/android/youtube/core/async/av;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->j:Lcom/google/android/youtube/core/b/an;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->k:Lcom/google/android/youtube/core/b/ap;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v12

    sget-object v13, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Playlist:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v14, Lcom/google/android/youtube/app/m;->O:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->l:Lcom/google/android/youtube/core/d;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v15}, Lcom/google/android/youtube/app/honeycomb/tablet/ac;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ab;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;ZZLcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    .line 146
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->u:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 170
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 41
    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->v:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->d:Lcom/google/android/youtube/app/a;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    sget-object v3, Lcom/google/android/youtube/app/m;->O:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v0, v1, v5, v2, v3}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)V

    iput-boolean v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->v:Z

    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    if-nez v0, :cond_34

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    new-array v1, v6, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->r:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_75

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->s:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4f
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b2

    iget v0, p2, Lcom/google/android/youtube/core/model/Playlist;->size:I

    if-nez v0, :cond_7b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    const v2, 0x7f0b01d2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_74
    return-void

    :cond_75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4f

    :cond_7b
    iget v0, p2, Lcom/google/android/youtube/core/model/Playlist;->size:I

    if-ne v0, v6, :cond_94

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    const v2, 0x7f0b01d3

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6f

    :cond_94
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    const v2, 0x7f0b01d4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    aput-object v4, v3, v5

    iget v4, p2, Lcom/google/android/youtube/core/model/Playlist;->size:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6f

    :cond_b2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_74
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->l:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 175
    return-void
.end method

.method protected final b()V
    .registers 5

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 151
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->m:Z

    if-eqz v0, :cond_f

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 164
    :goto_e
    return-void

    .line 154
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    if-eqz v0, :cond_2e

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->u:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->o:Landroid/net/Uri;

    invoke-static {v3}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_e

    .line 161
    :cond_2e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->u:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_e
.end method

.method protected final c()V
    .registers 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->p:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    .line 231
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 232
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ab;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 180
    return-void
.end method
