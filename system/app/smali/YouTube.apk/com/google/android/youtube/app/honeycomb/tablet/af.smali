.class public final Lcom/google/android/youtube/app/honeycomb/tablet/af;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private final h:Lcom/google/android/youtube/core/async/av;

.field private final i:Lcom/google/android/youtube/core/b/an;

.field private final j:Lcom/google/android/youtube/core/b/ap;

.field private final k:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final l:Lcom/google/android/youtube/core/d;

.field private m:Lcom/google/android/youtube/app/ui/dw;

.field private n:Landroid/view/ViewGroup;

.field private o:Lcom/google/android/youtube/app/ui/at;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    const-string v5, "yt_results"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->c()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->h:Lcom/google/android/youtube/core/async/av;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->i:Lcom/google/android/youtube/core/b/an;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->j:Lcom/google/android/youtube/core/b/ap;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->k:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->l:Lcom/google/android/youtube/core/d;

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/af;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/af;->f()V

    return-void
.end method

.method private f()V
    .registers 7

    .prologue
    .line 162
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->m:Lcom/google/android/youtube/app/ui/dw;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->k:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->o:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v4, v5, v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 163
    return-void
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 82
    const v0, 0x7f0400e0

    return v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/os/Bundle;)V

    .line 144
    const-string v0, "selected_time_filter"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->o:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 145
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 18
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-super/range {p0 .. p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/af;->r()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->a:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->g()Landroid/provider/SearchRecentSuggestions;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const v1, 0x7f080053

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 93
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->a(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 94
    new-instance v1, Lcom/google/android/youtube/app/ui/dw;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->o()Lcom/google/android/youtube/app/b/g;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->d:Lcom/google/android/youtube/app/a;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    invoke-static {v7}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->h:Lcom/google/android/youtube/core/async/av;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->i:Lcom/google/android/youtube/core/b/an;

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->j:Lcom/google/android/youtube/core/b/ap;

    iget-object v11, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v11}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->SearchResults:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v13, Lcom/google/android/youtube/app/m;->K:Lcom/google/android/youtube/core/b/aq;

    iget-object v14, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->l:Lcom/google/android/youtube/core/d;

    invoke-direct/range {v1 .. v14}, Lcom/google/android/youtube/app/ui/dw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/plus1/a;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->m:Lcom/google/android/youtube/app/ui/dw;

    .line 109
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    const v2, 0x7f0b0216

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTitle(I)V

    .line 111
    new-instance v3, Lcom/google/android/youtube/app/honeycomb/tablet/ag;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ag;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/af;)V

    .line 118
    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 119
    if-eqz p2, :cond_b9

    .line 120
    const-string v1, "selected_time_filter"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-object v2, v1

    .line 130
    :cond_7e
    :goto_7e
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f0400df

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->n:Landroid/view/ViewGroup;

    .line 132
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->n:Landroid/view/ViewGroup;

    const v4, 0x7f080048

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 133
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    const v5, 0x7f0400d9

    invoke-static {v4, v3, v2, v1, v5}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/av;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/at;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->o:Lcom/google/android/youtube/app/ui/at;

    .line 136
    new-instance v1, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    const/16 v2, 0x13

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(I)V

    .line 138
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->g:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->n:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V

    .line 139
    return-void

    .line 123
    :cond_b9
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/af;->r()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "selected_time_filter"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 125
    if-eqz v1, :cond_7e

    move-object v2, v1

    .line 126
    goto :goto_7e
.end method

.method protected final b()V
    .registers 4

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->o:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/af;->f()V

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;Z)V

    .line 153
    return-void
.end method

.method protected final c()V
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/af;->m:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    .line 158
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 159
    return-void
.end method
