.class public final Lcom/google/android/youtube/app/honeycomb/tablet/ai;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/ui/ae;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final a:Lcom/google/android/youtube/core/d;

.field private h:Lcom/google/android/youtube/app/k;

.field private i:Lcom/google/android/youtube/app/honeycomb/ui/x;

.field private j:Landroid/app/Dialog;

.field private k:Landroid/view/View;

.field private final l:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method protected constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    const-string v5, "yt_upload"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->a:Lcom/google/android/youtube/core/d;

    .line 47
    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->k:Landroid/view/View;

    .line 48
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 51
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/aj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/aj;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ai;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->l:Landroid/content/DialogInterface$OnDismissListener;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ai;)Lcom/google/android/youtube/app/honeycomb/ui/x;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/tablet/ai;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 63
    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-eqz v0, :cond_27

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->k:Landroid/view/View;

    const v1, 0x7f080054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/TabRow;

    .line 65
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->k:Landroid/view/View;

    const v2, 0x7f080055

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ui/Workspace;

    .line 66
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-static {v2, v1, v0}, Lcom/google/android/youtube/core/ui/Workspace;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v0

    .line 67
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/Workspace;->setCurrentScreen(I)V

    .line 70
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/b/k;

    .line 72
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/x;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->k:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->a:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->m()Lcom/google/android/youtube/app/compat/r;

    move-result-object v6

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/ui/x;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/honeycomb/ui/ae;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/compat/r;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->h:Lcom/google/android/youtube/app/k;

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->l:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 84
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 117
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 4
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->e()V

    .line 122
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 125
    const-string v0, "Error authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->a:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 128
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 129
    return-void
.end method

.method protected final b()V
    .registers 5

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->e:Lcom/google/android/youtube/app/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    const v2, 0x7f0b01bf

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->h:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->e()Z

    move-result v3

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 141
    return-void
.end method

.method protected final c(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final c()V
    .registers 3

    .prologue
    .line 145
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a()V

    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 149
    return-void
.end method

.method protected final e()V
    .registers 2

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->e()V

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->i:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->e()V

    .line 90
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 104
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->d:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->d()V

    .line 105
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 134
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ai;->d:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->d()V

    .line 111
    return-void
.end method
