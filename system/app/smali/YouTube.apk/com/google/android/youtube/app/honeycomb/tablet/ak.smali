.class public final Lcom/google/android/youtube/app/honeycomb/tablet/ak;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/compat/h;
.implements Lcom/google/android/youtube/app/remote/af;
.implements Lcom/google/android/youtube/app/ui/bs;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/player/aa;
.implements Lcom/google/android/youtube/core/ui/m;
.implements Lcom/google/android/youtube/coreicecream/ui/h;


# instance fields
.field private A:Landroid/widget/TabHost;

.field private B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

.field private C:Landroid/widget/ImageButton;

.field private D:Landroid/widget/ImageButton;

.field private E:Landroid/widget/ImageButton;

.field private F:Z

.field private G:Z

.field private H:Lcom/google/android/youtube/app/honeycomb/tablet/ap;

.field private I:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private J:Lcom/google/android/youtube/core/Analytics;

.field private K:Z

.field private L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

.field private M:Lcom/google/android/youtube/core/b/al;

.field private N:Lcom/google/android/youtube/core/b/au;

.field private O:Lcom/google/android/youtube/app/remote/bb;

.field private P:Lcom/google/android/youtube/app/ui/bg;

.field private Q:Lcom/google/android/youtube/app/ui/ca;

.field private R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

.field private S:Lcom/google/android/youtube/app/k;

.field private T:Lcom/google/android/youtube/core/model/Video;

.field private U:Z

.field private V:Lcom/google/android/youtube/app/remote/ab;

.field private W:Landroid/app/KeyguardManager;

.field private X:Z

.field private a:Landroid/media/AudioManager;

.field private h:Lcom/google/android/youtube/app/ui/eo;

.field private i:Lcom/google/android/youtube/app/ui/fg;

.field private j:Lcom/google/android/youtube/app/ui/ej;

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Landroid/content/SharedPreferences;

.field private q:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private r:Lcom/google/android/youtube/core/d;

.field private s:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private t:Lcom/google/android/youtube/core/player/bx;

.field private u:Lcom/google/android/youtube/core/player/Director;

.field private v:Lcom/google/android/youtube/core/async/a/c;

.field private w:Z

.field private x:I

.field private y:Lcom/google/android/youtube/app/ui/di;

.field private z:Lcom/google/android/youtube/coreicecream/ui/g;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 301
    const-string v5, "yt_watch"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 192
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->F:Z

    .line 193
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->G:Z

    .line 302
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)Landroid/os/Bundle;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 270
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 271
    const-string v2, "playlist_uri"

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 272
    const-string v0, "playlist_start_position"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 273
    const-string v0, "authenticate"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 274
    const-string v2, "referrer"

    const-string v0, "referrer cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-object v1
.end method

.method public static a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/b/aq;)Landroid/os/Bundle;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 287
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 288
    const-string v0, "video_id"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v2, "unfavorite_uri"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 290
    const-string v2, "referrer"

    const-string v0, "referrer cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-object v1
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/os/Bundle;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 231
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 232
    const-string v2, "video_id"

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v2, "referrer"

    const-string v0, "referrer cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    return-object v1
.end method

.method public static a(Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)Landroid/os/Bundle;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 245
    invoke-static {p0, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/os/Bundle;

    move-result-object v0

    .line 246
    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 247
    return-object v0
.end method

.method private a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 558
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    if-ne v0, v1, :cond_35

    .line 559
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 560
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_35

    .line 561
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->c()Lcom/google/android/youtube/core/async/av;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/tablet/aq;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/aq;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 574
    :goto_34
    return-void

    .line 567
    :cond_35
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "playlist_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const-string v1, "playlist_start_position"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v1, "artist_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_63

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_63
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;ILjava/lang/String;)V

    goto :goto_34
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;ILjava/lang/String;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 137
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;ILjava/lang/String;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 605
    if-eqz p3, :cond_38

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_38

    .line 607
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_31

    .line 608
    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 609
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/ca;->a(Ljava/lang/String;)V

    .line 610
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p1}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    .line 615
    :goto_27
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->x:I

    invoke-virtual {v1, v0, p1, v2, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V

    .line 646
    :cond_30
    :goto_30
    return-void

    .line 613
    :cond_31
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-static {v0, p3, p1, v6}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    goto :goto_27

    .line 616
    :cond_38
    if-eqz p4, :cond_4e

    .line 617
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-static {v0, p4, p1, p5}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->v:Lcom/google/android/youtube/core/async/a/c;

    .line 619
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->v:Lcom/google/android/youtube/core/async/a/c;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->x:I

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V

    goto :goto_30

    .line 620
    :cond_4e
    if-nez p6, :cond_30

    .line 622
    if-eqz p2, :cond_a8

    .line 623
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;

    move-result-object v2

    .line 624
    if-eqz v2, :cond_a2

    .line 625
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->x:I

    if-lez v0, :cond_8c

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->x:I

    move v1, v0

    .line 626
    :goto_5f
    iget-object v0, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_90

    .line 627
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    iget-object v0, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0, p1}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    iget-object v5, v2, Lcom/google/android/youtube/core/utils/ag;->b:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v1, v5}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILjava/lang/String;)V

    .line 637
    :goto_7e
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    iget-object v0, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/ca;->a(Ljava/lang/String;)V

    goto :goto_30

    .line 625
    :cond_8c
    iget v0, v2, Lcom/google/android/youtube/core/utils/ag;->c:I

    move v1, v0

    goto :goto_5f

    .line 632
    :cond_90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    iget-object v4, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-static {v3, v4, p1, v6}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    iget-object v5, v2, Lcom/google/android/youtube/core/utils/ag;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v1, v5}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILjava/lang/String;)V

    goto :goto_7e

    .line 639
    :cond_a2
    const-string v0, "invalid intercepted URI"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_30

    .line 643
    :cond_a8
    const-string v0, "invalid arguments format"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_30
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->U:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Lcom/google/android/youtube/app/remote/bb;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->O:Lcom/google/android/youtube/app/remote/bb;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Lcom/google/android/youtube/app/ui/bg;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Lcom/google/android/youtube/core/player/bx;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Z
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->U:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Lcom/google/android/youtube/app/ui/ca;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method private i()Z
    .registers 3

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 915
    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    return-object v0
.end method

.method private u()V
    .registers 3

    .prologue
    .line 1015
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->F:Z

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->G:Z

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->e()Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()Z

    move-result v0

    if-nez v0, :cond_22

    .line 1017
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 1019
    :cond_22
    return-void
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 306
    const v0, 0x7f0400e5

    return v0
.end method

.method protected final a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 829
    sparse-switch p1, :sswitch_data_46

    .line 840
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 834
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/di;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 836
    :sswitch_f
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/ca;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    const v1, 0x7f0b022a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/an;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/an;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/ao;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ao;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_7

    .line 838
    :sswitch_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ca;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 829
    :sswitch_data_46
    .sparse-switch
        0x3ed -> :sswitch_8
        0x3f3 -> :sswitch_f
        0x3f4 -> :sswitch_8
        0x3f7 -> :sswitch_8
        0x3ff -> :sswitch_8
        0x403 -> :sswitch_3f
    .end sparse-switch
.end method

.method protected final a(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 651
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ag;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;
    :try_end_8
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_8} :catch_24

    move-result-object v0

    .line 657
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2b

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 664
    :goto_23
    return-void

    .line 652
    :catch_24
    move-exception v0

    .line 653
    const-string v1, "invalid intercepted URI"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_23

    .line 662
    :cond_2b
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    iget-object v0, v0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/app/m;->T:Lcom/google/android/youtube/core/b/aq;

    invoke-static {v2, v0, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_23
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 682
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/os/Bundle;)V

    .line 683
    const-string v0, "selected_tab_index"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 684
    const-string v0, "fullscreen"

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 685
    const-string v0, "stopped"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 686
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 26
    .parameter
    .parameter

    .prologue
    .line 311
    invoke-super/range {p0 .. p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 312
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v3, 0x7f0b0214

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setTitle(I)V

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/h;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->S:Lcom/google/android/youtube/app/k;

    .line 317
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Landroid/view/View;F)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(Z)V

    .line 320
    new-instance v2, Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    iget-object v4, v4, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/PlayerView;->a()Lcom/google/android/youtube/core/player/au;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/app/player/j;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->S:Lcom/google/android/youtube/app/k;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v8}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/youtube/app/player/j;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/l;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/core/player/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;Lcom/google/android/youtube/core/player/cc;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->S:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->i()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/bx;->a(Z)V

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v6

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/b/au;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->N:Lcom/google/android/youtube/core/b/au;

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->k()Lcom/google/android/youtube/core/b/a;

    move-result-object v7

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->m()Lcom/google/android/youtube/core/b/as;

    move-result-object v9

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v20

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->p:Landroid/content/SharedPreferences;

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->I:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    .line 339
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    .line 342
    new-instance v10, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->p:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-direct {v10, v2, v3, v4, v5}, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;)V

    .line 345
    new-instance v2, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    .line 346
    new-instance v16, Lcom/google/android/youtube/core/player/DefaultAdOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d()I

    move-result v3

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;-><init>(Landroid/content/Context;I)V

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setShowButtonsWhenNotFullscreen(Z)V

    .line 350
    new-instance v15, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-direct {v15, v2}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    .line 351
    new-instance v17, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    .line 352
    new-instance v18, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;-><init>(Landroid/content/Context;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/google/android/youtube/core/player/as;

    const/4 v4, 0x0

    aput-object v15, v3, v4

    const/4 v4, 0x1

    aput-object v18, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v16, v3, v4

    const/4 v4, 0x4

    aput-object v17, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->a([Lcom/google/android/youtube/core/player/as;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->p:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->N:Lcom/google/android/youtube/core/b/au;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v12}, Lcom/google/android/youtube/app/YouTubeApplication;->c()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Lcom/google/android/youtube/core/player/ax;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/player/e;

    move-result-object v22

    move-object/from16 v13, p0

    invoke-static/range {v2 .. v22}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    .line 380
    new-instance v15, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {v15, v2, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V

    .line 381
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setAutoHide(Z)V

    .line 382
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setShowButtonsWhenNotFullscreen(Z)V

    .line 383
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setHideOnTap(Z)V

    .line 384
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object v12, v6

    move-object v13, v9

    move-object/from16 v16, p0

    invoke-static/range {v10 .. v16}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;)Lcom/google/android/youtube/app/ui/bg;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/bg;->b(Z)V

    .line 393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/bg;->r()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/Director;->q()[Landroid/view/View;

    move-result-object v2

    .line 396
    array-length v3, v2

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Landroid/view/View;

    .line 397
    array-length v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/bg;->u()Landroid/view/View;

    move-result-object v3

    aput-object v3, v11, v2

    .line 399
    new-instance v7, Lcom/google/android/youtube/coreicecream/ui/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    iget-object v10, v2, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v12, p0

    invoke-direct/range {v7 .. v12}, Lcom/google/android/youtube/coreicecream/ui/i;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;[Landroid/view/View;Lcom/google/android/youtube/coreicecream/ui/h;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    .line 406
    new-instance v10, Lcom/google/android/youtube/app/ui/eo;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->o()Lcom/google/android/youtube/app/b/g;

    move-result-object v19

    move-object/from16 v12, p1

    move-object/from16 v16, v6

    move-object/from16 v17, p0

    invoke-direct/range {v10 .. v20}, Lcom/google/android/youtube/app/ui/eo;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/ui/m;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/b/g;Lcom/google/android/youtube/core/utils/l;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    .line 410
    new-instance v2, Lcom/google/android/youtube/app/adapter/ct;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, v20

    invoke-static {v3, v6, v0}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v3

    .line 416
    new-instance v4, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v4}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v4, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    .line 422
    new-instance v5, Lcom/google/android/youtube/app/adapter/bt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v4, 0x7f0400c7

    invoke-direct {v5, v3, v4, v2}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 425
    new-instance v2, Lcom/google/android/youtube/app/ui/fg;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v4, 0x7f0800e3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/g;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v7}, Lcom/google/android/youtube/core/b/al;->y()Lcom/google/android/youtube/core/async/av;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v9}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    invoke-direct/range {v2 .. v11}, Lcom/google/android/youtube/app/ui/fg;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i:Lcom/google/android/youtube/app/ui/fg;

    .line 435
    new-instance v2, Lcom/google/android/youtube/app/ui/ej;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v4, 0x7f0800e4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedListView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->e:Lcom/google/android/youtube/app/g;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/app/ui/ej;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedListView;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/d;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->j:Lcom/google/android/youtube/app/ui/ej;

    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->q()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->O:Lcom/google/android/youtube/app/remote/bb;

    .line 440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->r()Lcom/google/android/youtube/app/remote/bm;

    .line 442
    new-instance v2, Lcom/google/android/youtube/app/ui/di;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->e:Lcom/google/android/youtube/app/g;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->I:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/di;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/dq;Lcom/google/android/youtube/app/YouTubePlatformUtil;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/player/bx;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    .line 454
    new-instance v9, Lcom/google/android/youtube/app/honeycomb/tablet/al;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/al;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)V

    .line 460
    new-instance v2, Lcom/google/android/youtube/app/ui/ca;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v5}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/app/remote/bl;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->r()Lcom/google/android/youtube/app/remote/bm;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->O:Lcom/google/android/youtube/app/remote/bb;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->J:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    const v11, 0x7f0400e1

    invoke-direct/range {v2 .. v11}, Lcom/google/android/youtube/app/ui/ca;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ab;Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/cl;Lcom/google/android/youtube/core/d;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    .line 471
    const/4 v2, 0x0

    .line 472
    if-eqz p2, :cond_3e4

    .line 473
    const-string v2, "selected_tab_index"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 474
    const-string v3, "fullscreen"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    :cond_3e4
    move v5, v2

    .line 477
    const v2, 0x1020012

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TabHost;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    .line 478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->setup()V

    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    .line 482
    const v2, 0x1020011

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 483
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    .line 484
    const/4 v3, 0x0

    move v6, v3

    :goto_414
    if-ge v6, v8, :cond_450

    .line 485
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 486
    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 487
    const v4, 0x7f0400b2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    const/4 v11, 0x0

    invoke-virtual {v7, v4, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 488
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    invoke-virtual {v11, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 484
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_414

    .line 492
    :cond_450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->A:Landroid/widget/TabHost;

    invoke-virtual {v2, v5}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 494
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    .line 495
    const v2, 0x7f0800e8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->C:Landroid/widget/ImageButton;

    .line 496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->C:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 497
    const v2, 0x7f0800e7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->D:Landroid/widget/ImageButton;

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->D:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->C:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->D:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/view/View;Landroid/view/View;)V

    .line 501
    const v2, 0x7f0800e9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 505
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->t:Lcom/google/android/youtube/core/player/bx;

    new-instance v3, Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/bx;->a(Landroid/os/Handler;)V

    .line 507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a:Landroid/media/AudioManager;

    .line 510
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/tablet/ap;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->H:Lcom/google/android/youtube/app/honeycomb/tablet/ap;

    .line 512
    if-eqz p2, :cond_517

    const-string v2, "stopped"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_517

    const/4 v2, 0x1

    :goto_4f6
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->V:Lcom/google/android/youtube/app/remote/ab;

    .line 515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->W:Landroid/app/KeyguardManager;

    .line 516
    return-void

    .line 512
    :cond_517
    const/4 v2, 0x0

    goto :goto_4f6
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 6
    .parameter

    .prologue
    const v3, 0x7f0801a7

    const/4 v1, 0x0

    .line 714
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 715
    invoke-virtual {p1, v3}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i()Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_59

    :cond_19
    const/4 v0, 0x2

    :goto_1a
    invoke-interface {v2, v0}, Lcom/google/android/youtube/app/compat/t;->c(I)V

    .line 718
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    if-eqz v0, :cond_5b

    .line 719
    :goto_21
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 720
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->D:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 721
    const v0, 0x7f0801a6

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 722
    invoke-virtual {p1, v3}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 723
    const v0, 0x7f0801a9

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 724
    const v0, 0x7f0801a8

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 725
    return-void

    :cond_59
    move v0, v1

    .line 715
    goto :goto_1a

    .line 718
    :cond_5b
    const/4 v1, 0x4

    goto :goto_21
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 690
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 691
    const v0, 0x7f110005

    invoke-virtual {p2, v0, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 693
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_37

    .line 694
    const v0, 0x7f0801a7

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ShareActionProvider;

    if-eqz v0, :cond_37

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->T:Lcom/google/android/youtube/core/model/Video;

    if-eqz v1, :cond_37

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->T:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Lcom/google/android/youtube/core/model/Video;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ShareActionProvider;->setShareIntent(Landroid/content/Intent;)V

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/am;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/am;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)V

    invoke-virtual {v0, v1}, Landroid/widget/ShareActionProvider;->setOnShareTargetSelectedListener(Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;)V

    .line 696
    :cond_37
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 4
    .parameter

    .prologue
    .line 1231
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 1232
    if-nez p1, :cond_2f

    .line 1233
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->s()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1234
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->X:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->W:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_29

    .line 1235
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    .line 1236
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    .line 1238
    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->a()V

    .line 1244
    :cond_2e
    :goto_2e
    return-void

    .line 1241
    :cond_2f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    goto :goto_2e
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 3
    .parameter

    .prologue
    .line 963
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eo;->a(Lcom/google/android/youtube/core/model/Branding;)V

    .line 964
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i:Lcom/google/android/youtube/app/ui/fg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fg;->a(Lcom/google/android/youtube/core/model/Branding;)V

    .line 965
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 668
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 4
    .parameter

    .prologue
    .line 971
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 972
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 745
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->T:Lcom/google/android/youtube/core/model/Video;

    .line 746
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v3, 0x7f0b0218

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    aput-object v5, v4, v1

    iget-object v5, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->m:Ljava/lang/String;

    .line 748
    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v2, :cond_25

    .line 749
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    .line 752
    :cond_25
    if-eqz p1, :cond_80

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v2, v3, :cond_80

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->watchUri:Landroid/net/Uri;

    if-eqz v2, :cond_80

    :goto_31
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    .line 754
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 755
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "unfavorite_uri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 757
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 759
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eo;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 760
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i:Lcom/google/android/youtube/app/ui/fg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fg;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 761
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->j:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ej;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 762
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 764
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    .line 765
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->K:Z

    .line 766
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/ui/dm;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->M:Lcom/google/android/youtube/core/b/al;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r:Lcom/google/android/youtube/core/d;

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/dm;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 767
    return-void

    :cond_80
    move v0, v1

    .line 752
    goto :goto_31
.end method

.method public final a(Lcom/google/android/youtube/core/player/Director$StopReason;)V
    .registers 5
    .parameter

    .prologue
    .line 978
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_13

    .line 979
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v1, 0x7f0b0068

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    .line 982
    :cond_d
    :goto_d
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 984
    :cond_12
    return-void

    .line 981
    :cond_13
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->n:Z

    if-eqz v0, :cond_12

    goto :goto_d
.end method

.method public final a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .registers 4
    .parameter

    .prologue
    .line 774
    iget-object v0, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    sget-object v1, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    if-ne v0, v1, :cond_18

    .line 775
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bg;->b(Ljava/lang/String;)V

    .line 778
    :cond_18
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 671
    const-string v0, "error authenticating for playlist request"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 672
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 673
    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/ui/ca;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1224
    const/4 v0, 0x1

    .line 1226
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(IILandroid/content/Intent;)Z

    move-result v0

    goto :goto_9
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 873
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v1

    packed-switch v1, :pswitch_data_32

    .line 896
    :pswitch_8
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    :cond_c
    :goto_c
    return v0

    .line 875
    :pswitch_d
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->f()V

    goto :goto_c

    .line 879
    :pswitch_13
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_c

    .line 880
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->c()V

    goto :goto_c

    .line 885
    :pswitch_1f
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->e()V

    goto :goto_c

    .line 889
    :pswitch_25
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->d()V

    goto :goto_c

    .line 893
    :pswitch_2b
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->g()V

    goto :goto_c

    .line 873
    nop

    :pswitch_data_32
    .packed-switch 0x7f08019f
        :pswitch_2b
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_d
        :pswitch_13
        :pswitch_25
        :pswitch_1f
    .end packed-switch
.end method

.method public final a_(Z)V
    .registers 2
    .parameter

    .prologue
    .line 993
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->G:Z

    .line 994
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u()V

    .line 995
    return-void
.end method

.method protected final b()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 520
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 522
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->r()Landroid/os/Bundle;

    move-result-object v0

    .line 523
    const-string v1, "authenticate"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 525
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 526
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/bg;->d(Z)V

    .line 527
    const-string v2, "force_fullscreen"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 528
    iput-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->k:Z

    .line 529
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v2, v5}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    .line 530
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 531
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 533
    :cond_38
    const-string v2, "finish_on_ended"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->n:Z

    .line 535
    const-string v2, "referrer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 536
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6c

    .line 537
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->N:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/b/au;->b(Ljava/lang/String;)V

    .line 543
    :cond_51
    :goto_51
    if-eqz v1, :cond_86

    .line 544
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 550
    :goto_5a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a:Landroid/media/AudioManager;

    const/high16 v1, -0x8000

    invoke-virtual {v0, v6, v1, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 552
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->H:Lcom/google/android/youtube/app/honeycomb/tablet/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ap;->a()V

    .line 554
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->V:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/af;)V

    .line 555
    return-void

    .line 538
    :cond_6c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    if-ne v0, v2, :cond_51

    .line 540
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->N:Lcom/google/android/youtube/core/b/au;

    sget-object v2, Lcom/google/android/youtube/app/m;->L:Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/b/au;->b(Ljava/lang/String;)V

    goto :goto_51

    .line 546
    :cond_86
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_5a
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)V
    .registers 2
    .parameter

    .prologue
    .line 975
    return-void
.end method

.method public final b(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x400

    .line 928
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    .line 930
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/coreicecream/ui/g;->a(Z)V

    .line 931
    if-nez p1, :cond_4f

    .line 932
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 933
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFlags(II)V

    .line 934
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const v1, 0x7f0b0214

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    .line 935
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 936
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    .line 938
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    .line 948
    :goto_36
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i()Z

    move-result v0

    .line 949
    if-eqz p1, :cond_79

    .line 951
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 952
    if-eqz v0, :cond_49

    .line 953
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a()V

    .line 959
    :cond_49
    :goto_49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->d(Z)V

    .line 960
    return-void

    .line 940
    :cond_4f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 942
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 943
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 945
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    goto :goto_36

    .line 956
    :cond_79
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 957
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(Z)V

    goto :goto_49
.end method

.method protected final c()V
    .registers 3

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->a:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 810
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->H:Lcom/google/android/youtube/app/honeycomb/tablet/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ap;->b()V

    .line 811
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i:Lcom/google/android/youtube/app/ui/fg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/fg;->b()V

    .line 812
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->j:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ej;->b()V

    .line 813
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 814
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->w()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->x:I

    .line 815
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->w:Z

    .line 816
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->V:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->b(Lcom/google/android/youtube/app/remote/af;)V

    .line 817
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 818
    return-void
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 919
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v0, :cond_f

    .line 920
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a()V

    .line 924
    :goto_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 925
    return-void

    .line 922
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->B:Lcom/google/android/youtube/app/honeycomb/tablet/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(Z)V

    goto :goto_9
.end method

.method protected final d()V
    .registers 2

    .prologue
    .line 822
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->d()V

    .line 823
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->u()V

    .line 824
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Lcom/google/android/youtube/app/compat/h;)V

    .line 825
    return-void
.end method

.method protected final e()V
    .registers 3

    .prologue
    .line 786
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->e()V

    .line 787
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->v()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    .line 788
    if-eqz v0, :cond_19

    .line 789
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 794
    :goto_10
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->b()V

    .line 795
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->X:Z

    .line 796
    return-void

    .line 791
    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->a()V

    .line 792
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_10
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    .line 782
    return-void
.end method

.method protected final g()V
    .registers 2

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->c()V

    .line 801
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->t()V

    .line 802
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->Q:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ca;->b()V

    .line 803
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->X:Z

    .line 804
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g()V

    .line 805
    return-void
.end method

.method public final h()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1023
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v1, :cond_f

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->k:Z

    if-nez v1, :cond_f

    .line 1024
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 1025
    const/4 v0, 0x1

    .line 1027
    :cond_f
    return v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1031
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_30

    :pswitch_7
    move v0, v1

    .line 1050
    :cond_8
    :goto_8
    return v0

    .line 1033
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const v2, 0x7f020217

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_8

    .line 1039
    :pswitch_12
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_8

    .line 1043
    :pswitch_19
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    const v3, 0x7f020216

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1044
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->V:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    if-nez v2, :cond_8

    .line 1045
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_8

    .line 1031
    nop

    :pswitch_data_30
    .packed-switch 0x2
        :pswitch_19
        :pswitch_9
        :pswitch_12
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 677
    return-void
.end method

.method public final j()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 732
    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->T:Lcom/google/android/youtube/core/model/Video;

    .line 733
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->o:Z

    .line 734
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 735
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 736
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->h:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eo;->b()V

    .line 737
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->i:Lcom/google/android/youtube/app/ui/fg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/fg;->b()V

    .line 738
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->j:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ej;->b()V

    .line 739
    return-void
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 998
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->F:Z

    .line 999
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->K:Z

    if-eqz v0, :cond_c

    .line 1000
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->a()V

    .line 1002
    :cond_c
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u()V

    .line 1003
    return-void
.end method

.method public final l()V
    .registers 3

    .prologue
    .line 1006
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->F:Z

    .line 1007
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->L:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    .line 1008
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v0, :cond_17

    .line 1009
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->z:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 1010
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->s:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    .line 1012
    :cond_17
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 900
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    .line 911
    :goto_7
    return-void

    .line 902
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->a()V

    goto :goto_7

    .line 905
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->y:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->b()V

    goto :goto_7

    .line 908
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->l()V

    goto :goto_7

    .line 900
    :pswitch_data_1a
    .packed-switch 0x7f0800e7
        :pswitch_e
        :pswitch_8
        :pswitch_14
    .end packed-switch
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 1216
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l:Z

    if-eqz v0, :cond_9

    .line 1217
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->R:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    .line 1219
    :cond_9
    return-void
.end method

.method public final s()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()I
    .registers 2

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1252
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->P:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->t()I

    move-result v0

    .line 1254
    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->u:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->w()I

    move-result v0

    goto :goto_e
.end method
