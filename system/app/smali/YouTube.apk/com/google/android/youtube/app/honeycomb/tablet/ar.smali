.class final Lcom/google/android/youtube/app/honeycomb/tablet/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/widget/LinearLayout;

.field public final c:Landroid/widget/LinearLayout;

.field public final d:Lcom/google/android/youtube/core/player/PlayerView;

.field public final e:Landroid/view/View;

.field public final f:Landroid/view/View;

.field public final g:Landroid/view/View;

.field public final h:Lcom/google/android/youtube/core/ui/PagedGridView;

.field final synthetic i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

.field private final j:F

.field private k:[I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/tablet/ak;Landroid/view/View;F)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1095
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    .line 1096
    const v0, 0x7f0800de

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    .line 1097
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0800df

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    .line 1098
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0800e0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/PlayerView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    .line 1099
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0800e1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->e:Landroid/view/View;

    .line 1100
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0800e6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->f:Landroid/view/View;

    .line 1101
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x1020012

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    .line 1102
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0800e3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedGridView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->h:Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 1104
    iput p3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->j:F

    .line 1105
    return-void
.end method

.method private a(I)I
    .registers 4
    .parameter

    .prologue
    .line 1210
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->j:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .registers 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1184
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    if-nez v0, :cond_30

    .line 1186
    const/4 v0, 0x4

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    .line 1190
    :cond_30
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 1192
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1194
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1195
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1197
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1198
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1199
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1201
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1202
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1203
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1205
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1206
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1207
    return-void
.end method

.method public final a(Z)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x10

    const/16 v7, 0x20

    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 1108
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->b:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_e5

    move v0, v1

    :goto_c
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1109
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    if-eqz v0, :cond_2e

    .line 1111
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    aget v2, v2, v1

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    const/4 v6, 0x3

    aget v5, v5, v6

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 1113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->k:[I

    .line 1116
    :cond_2e
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1117
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->j(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1119
    if-eqz p1, :cond_e8

    .line 1120
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->k(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0098

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 1121
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->l(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a0099

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 1123
    invoke-direct {p0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v5

    .line 1124
    invoke-direct {p0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v6

    .line 1126
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1128
    const/high16 v7, 0x3f00

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v7

    float-to-int v2, v2

    add-int/2addr v2, v3

    add-int/2addr v2, v5

    add-int/2addr v2, v6

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1130
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1131
    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1134
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1135
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1136
    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3, v4, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1140
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1141
    invoke-virtual {v0, v5, v1, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1142
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1143
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1144
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v2

    invoke-direct {p0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1172
    :goto_c0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->h:Lcom/google/android/youtube/core/ui/PagedGridView;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->i:Lcom/google/android/youtube/app/honeycomb/tablet/ak;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/tablet/ak;->m(Lcom/google/android/youtube/app/honeycomb/tablet/ak;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->e(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 1175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1176
    iput v8, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1177
    const/4 v2, -0x2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1179
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1180
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1181
    return-void

    .line 1108
    :cond_e5
    const/4 v0, 0x1

    goto/16 :goto_c

    .line 1146
    :cond_e8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1148
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1152
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    const/high16 v3, 0x3f10

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1156
    const/16 v3, 0x60

    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1157
    const/high16 v2, 0x420c

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1158
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1159
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1160
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1161
    const/high16 v2, 0x4282

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1163
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1164
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->d:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1165
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1167
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->f:Landroid/view/View;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v2

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->e:Landroid/view/View;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v2

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1169
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->g:Landroid/view/View;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v2

    invoke-direct {p0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v3

    invoke-direct {p0, v7}, Lcom/google/android/youtube/app/honeycomb/tablet/ar;->a(I)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_c0
.end method
