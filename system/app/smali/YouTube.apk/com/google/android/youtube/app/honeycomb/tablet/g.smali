.class public final Lcom/google/android/youtube/app/honeycomb/tablet/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/youtube/app/honeycomb/tablet/h;

.field private static final b:Lcom/google/android/youtube/app/honeycomb/tablet/h;

.field private static final c:Lcom/google/android/youtube/app/honeycomb/tablet/h;

.field private static final d:Lcom/google/android/youtube/app/honeycomb/tablet/h;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/h;

    const v1, 0x4099999a

    invoke-direct {v0, v3, v5, v1, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/h;-><init>(IIFB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->a:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    .line 18
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/h;

    const/high16 v1, 0x4140

    invoke-direct {v0, v5, v3, v1, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/h;-><init>(IIFB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->b:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    .line 20
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/h;

    const v1, 0x41033333

    invoke-direct {v0, v4, v6, v1, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/h;-><init>(IIFB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->c:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    .line 22
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/h;

    const/high16 v1, 0x4178

    invoke-direct {v0, v6, v4, v1, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/h;-><init>(IIFB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->d:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    return-void
.end method

.method protected static a(Landroid/content/Context;Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 26
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->i(Landroid/content/Context;)I

    move-result v1

    .line 27
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    .line 29
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->a:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    .line 31
    const/16 v3, 0x500

    if-lt v1, v3, :cond_30

    const/4 v3, 0x2

    if-ne v2, v3, :cond_30

    .line 32
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->c:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    .line 39
    :cond_1a
    :goto_1a
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/h;->a(Lcom/google/android/youtube/app/honeycomb/tablet/h;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->setRowCount(I)V

    .line 40
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/h;->b(Lcom/google/android/youtube/app/honeycomb/tablet/h;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->setVisibleSlots(I)V

    .line 41
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/h;->c(Lcom/google/android/youtube/app/honeycomb/tablet/h;)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->setCameraZ(F)V

    .line 42
    return-void

    .line 33
    :cond_30
    const/16 v3, 0x320

    if-lt v1, v3, :cond_39

    if-ne v2, v4, :cond_39

    .line 34
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->d:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    goto :goto_1a

    .line 35
    :cond_39
    if-ne v2, v4, :cond_1a

    .line 36
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/g;->b:Lcom/google/android/youtube/app/honeycomb/tablet/h;

    goto :goto_1a
.end method
