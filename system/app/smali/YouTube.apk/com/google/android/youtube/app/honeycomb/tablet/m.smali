.class public final Lcom/google/android/youtube/app/honeycomb/tablet/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/youtube/app/honeycomb/tablet/n;

.field private static final b:Lcom/google/android/youtube/app/honeycomb/tablet/n;

.field private static final c:Lcom/google/android/youtube/app/honeycomb/tablet/n;

.field private static final d:Lcom/google/android/youtube/app/honeycomb/tablet/n;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/n;

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/n;-><init>(IIIIIB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->a:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    .line 17
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/n;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/n;-><init>(IIIIIB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    .line 19
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/n;

    const/4 v1, 0x5

    const/4 v2, 0x4

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/n;-><init>(IIIIIB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->c:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    .line 21
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/n;

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/tablet/n;-><init>(IIIIIB)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->d:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    return-void
.end method

.method protected static a(Landroid/content/Context;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/n;->a(Lcom/google/android/youtube/app/honeycomb/tablet/n;)I

    move-result v0

    return v0
.end method

.method protected static b(Landroid/content/Context;)I
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/n;->b(Lcom/google/android/youtube/app/honeycomb/tablet/n;)I

    move-result v0

    return v0
.end method

.method protected static c(Landroid/content/Context;)I
    .registers 2
    .parameter

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/n;->c(Lcom/google/android/youtube/app/honeycomb/tablet/n;)I

    move-result v0

    return v0
.end method

.method protected static d(Landroid/content/Context;)I
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/n;->d(Lcom/google/android/youtube/app/honeycomb/tablet/n;)I

    move-result v0

    return v0
.end method

.method protected static e(Landroid/content/Context;)I
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/n;->e(Lcom/google/android/youtube/app/honeycomb/tablet/n;)I

    move-result v0

    return v0
.end method

.method private static f(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/tablet/n;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 45
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->i(Landroid/content/Context;)I

    move-result v1

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    .line 48
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->a:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    .line 50
    const/16 v3, 0x500

    if-lt v1, v3, :cond_1b

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1b

    .line 51
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->c:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    .line 58
    :cond_1a
    :goto_1a
    return-object v0

    .line 52
    :cond_1b
    const/16 v3, 0x320

    if-lt v1, v3, :cond_24

    if-ne v2, v4, :cond_24

    .line 53
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->d:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    goto :goto_1a

    .line 54
    :cond_24
    if-ne v2, v4, :cond_1a

    .line 55
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b:Lcom/google/android/youtube/app/honeycomb/tablet/n;

    goto :goto_1a
.end method
