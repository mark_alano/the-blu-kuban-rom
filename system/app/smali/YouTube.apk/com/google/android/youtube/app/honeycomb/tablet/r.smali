.class final Lcom/google/android/youtube/app/honeycomb/tablet/r;
.super Lcom/google/android/youtube/app/ui/dd;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/ui/PagedGridView;

.field final synthetic b:Lcom/google/android/youtube/app/honeycomb/tablet/q;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cl;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/ui/PagedGridView;)V
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->a:Lcom/google/android/youtube/core/ui/PagedGridView;

    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v1 .. v14}, Lcom/google/android/youtube/app/ui/dd;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cl;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Lcom/google/android/youtube/core/model/Video;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a(Landroid/view/View;Lcom/google/android/youtube/core/model/Video;)V

    .line 170
    :goto_15
    return-void

    .line 168
    :cond_16
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/ui/dd;->a(Landroid/view/View;Lcom/google/android/youtube/core/model/Video;I)V

    goto :goto_15
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .registers 4
    .parameter

    .prologue
    .line 207
    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v1, :cond_f

    .line 208
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->b()V

    .line 210
    :cond_f
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/ui/dd;->a(Lcom/google/android/youtube/core/transfer/Transfer;)V

    .line 211
    return-void
.end method

.method protected final a(Landroid/view/View;Lcom/google/android/youtube/core/model/Video;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a()Z

    move-result v0

    if-nez v0, :cond_17

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->a:Lcom/google/android/youtube/core/ui/PagedGridView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 177
    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a(Landroid/view/View;Lcom/google/android/youtube/core/model/Video;)V

    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .registers 6
    .parameter

    .prologue
    .line 215
    iget-wide v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->e:J

    iget-wide v2, p1, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_11

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->b()V

    .line 218
    :cond_11
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/ui/dd;->b(Lcom/google/android/youtube/core/transfer/Transfer;)V

    .line 219
    return-void
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->b(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/adapter/cl;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/cl;->a(I)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_11

    .line 193
    invoke-super/range {p0 .. p5}, Lcom/google/android/youtube/app/ui/dd;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    .line 202
    :goto_10
    return v0

    .line 195
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->b(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/adapter/cl;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/cl;->b(I)Lcom/google/android/youtube/core/transfer/Transfer;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_49

    .line 197
    iget-object v1, v0, Lcom/google/android/youtube/core/transfer/Transfer;->h:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "video_id"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198
    if-nez v1, :cond_49

    .line 199
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a()Z

    move-result v1

    if-nez v1, :cond_3e

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->a:Lcom/google/android/youtube/core/ui/PagedGridView;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/PagedGridView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    :cond_3e
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/r;->b:Lcom/google/android/youtube/app/honeycomb/tablet/q;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->a(Landroid/view/View;Lcom/google/android/youtube/core/transfer/Transfer;)V

    const/4 v0, 0x1

    goto :goto_10

    .line 202
    :cond_49
    invoke-super/range {p0 .. p5}, Lcom/google/android/youtube/app/ui/dd;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_10
.end method
