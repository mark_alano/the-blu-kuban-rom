.class public abstract Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.super Lcom/google/android/youtube/app/ui/ay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/ui/ay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    .line 84
    return-void
.end method

.method public static final a(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    .line 65
    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 66
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/f;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/f;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    .line 72
    :goto_d
    return-object v0

    .line 67
    :cond_e
    const/16 v1, 0xd

    if-ne v0, v1, :cond_18

    .line 68
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/d;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_d

    .line 69
    :cond_18
    const/16 v1, 0xb

    if-lt v0, v1, :cond_22

    .line 70
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_d

    .line 71
    :cond_22
    const/16 v1, 0x8

    if-lt v0, v1, :cond_2c

    .line 72
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/h;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/h;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_d

    .line 74
    :cond_2c
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to instantiate ActionBarMenuHelper on an unsupported platform version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public abstract a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
.end method

.method public abstract a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method
