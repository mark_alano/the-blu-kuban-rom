.class final Lcom/google/android/youtube/app/honeycomb/ui/f;
.super Lcom/google/android/youtube/app/honeycomb/ui/a;
.source "SourceFile"


# instance fields
.field private final i:Lcom/google/android/youtube/app/honeycomb/ui/g;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 385
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/ui/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    .line 386
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/g;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/f;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->i:Lcom/google/android/youtube/app/honeycomb/ui/g;

    .line 387
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 407
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Z)V

    .line 408
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_c

    .line 418
    :cond_b
    :goto_b
    return-void

    .line 411
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_b

    .line 414
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->c()Z

    .line 415
    if-nez p1, :cond_b

    .line 416
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_b
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 5
    .parameter

    .prologue
    .line 391
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    .line 392
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v1, v2, :cond_28

    .line 393
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->i:Lcom/google/android/youtube/app/honeycomb/ui/g;

    if-eqz v1, :cond_15

    .line 394
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->i:Lcom/google/android/youtube/app/honeycomb/ui/g;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/u;)Lcom/google/android/youtube/app/compat/t;

    .line 396
    :cond_15
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v1, v2, :cond_28

    .line 397
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    const v2, 0x2000003

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 399
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 402
    :cond_28
    return v0
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 422
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/ui/a;->c()V

    .line 423
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_c

    .line 430
    :cond_b
    :goto_b
    return-void

    .line 426
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_b

    .line 429
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->b()Z

    goto :goto_b
.end method
