.class final Lcom/google/android/youtube/app/honeycomb/ui/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/p;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->b:Ljava/lang/String;

    .line 227
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Ljava/lang/String;B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/ui/u;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 246
    :goto_e
    return-void

    .line 245
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->g(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/p;->f(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_e
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 236
    const-string v0, "Error while authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->g(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/p;->f(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 238
    return-void
.end method

.method public final i_()V
    .registers 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->g(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/u;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/p;->f(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 232
    return-void
.end method
