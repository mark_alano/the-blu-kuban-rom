.class public final Lcom/google/android/youtube/app/honeycomb/ui/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/aa;


# static fields
.field public static a:Z

.field private static final b:[Ljava/lang/String;

.field private static final c:Landroid/net/Uri;


# instance fields
.field private final A:Landroid/widget/TextView;

.field private final B:Lcom/google/android/youtube/app/ui/df;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Lcom/google/android/youtube/core/model/Video$Privacy;

.field private G:Z

.field private final H:Ljava/util/List;

.field private I:I

.field private J:Z

.field private K:J

.field private final L:Lcom/google/android/youtube/core/b/al;

.field private M:Z

.field private N:Z

.field private O:Landroid/widget/Button;

.field private P:Lcom/google/android/youtube/app/compat/t;

.field private Q:Ljava/util/Map;

.field private R:Ljava/util/Map;

.field private S:Ljava/util/Map;

.field private T:Landroid/widget/Button;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;

.field private final d:Landroid/app/Activity;

.field private final e:Landroid/content/res/Resources;

.field private final f:Landroid/content/ContentResolver;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Lcom/google/android/youtube/core/Analytics;

.field private final i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

.field private final j:Lcom/google/android/youtube/core/transfer/x;

.field private final k:Lcom/google/android/youtube/core/d;

.field private final l:Lcom/google/android/youtube/app/compat/r;

.field private m:Lcom/google/android/youtube/core/async/l;

.field private n:Lcom/google/android/youtube/core/async/l;

.field private o:Lcom/google/android/youtube/core/model/UserAuth;

.field private p:Ljava/lang/String;

.field private q:Landroid/graphics/Bitmap;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/CheckBox;

.field private final x:Landroid/widget/EditText;

.field private final y:Landroid/widget/EditText;

.field private final z:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 90
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "longitude"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->b:[Ljava/lang/String;

    .line 105
    sput-boolean v3, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    .line 109
    const-string v0, "http://m.youtube.com/#/account_sharing"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/honeycomb/ui/ae;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/compat/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->L:Lcom/google/android/youtube/core/b/al;

    .line 178
    const-string v0, "activity can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->d:Landroid/app/Activity;

    .line 179
    iput-object p4, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

    .line 180
    const-string v0, "errorHelper can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->k:Lcom/google/android/youtube/core/d;

    .line 181
    const-string v0, "menuInflater can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/r;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->l:Lcom/google/android/youtube/app/compat/r;

    .line 183
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 185
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->e:Landroid/content/res/Resources;

    .line 186
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->f:Landroid/content/ContentResolver;

    .line 187
    const-string v1, "youtube"

    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->g:Landroid/content/SharedPreferences;

    .line 188
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->h:Lcom/google/android/youtube/core/Analytics;

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->g:Landroid/content/SharedPreferences;

    const-string v1, "upload_privacy"

    sget-object v2, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/model/Video$Privacy;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    .line 193
    const v0, 0x7f080046

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->r:Landroid/widget/TextView;

    .line 194
    const v0, 0x7f080040

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->t:Landroid/widget/ImageView;

    .line 195
    const v0, 0x7f08006c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->s:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f0800c8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->v:Landroid/widget/TextView;

    .line 198
    const v0, 0x7f080174

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->x:Landroid/widget/EditText;

    .line 199
    const v0, 0x7f080175

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->y:Landroid/widget/EditText;

    .line 200
    const v0, 0x7f080176

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->z:Landroid/widget/EditText;

    .line 201
    const v0, 0x7f080095

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->setPrivacy(Lcom/google/android/youtube/core/model/Video$Privacy;)V

    .line 203
    const v0, 0x7f080177

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->w:Landroid/widget/CheckBox;

    .line 205
    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-eqz v0, :cond_1a5

    .line 206
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->R:Ljava/util/Map;

    .line 208
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->S:Ljava/util/Map;

    .line 209
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f08017b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f08017e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f080181

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f08017c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f08017f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f080182

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f08017d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f080180

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f080183

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const v0, 0x7f08017a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->V:Landroid/view/View;

    .line 228
    const v0, 0x7f080178

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->U:Landroid/view/View;

    .line 229
    const v0, 0x7f080179

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->T:Landroid/widget/Button;

    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->T:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/y;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/y;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    const v0, 0x7f080184

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 240
    :cond_1a5
    const v0, 0x7f080092

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    if-eqz v0, :cond_1cb

    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    const v1, 0x7f0b0160

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 243
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 244
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/z;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/z;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    :cond_1cb
    const v0, 0x7f080093

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->A:Landroid/widget/TextView;

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->A:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 259
    new-instance v0, Lcom/google/android/youtube/app/ui/df;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/aa;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/aa;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/app/ui/df;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->B:Lcom/google/android/youtube/app/ui/df;

    .line 266
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/af;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/af;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->m:Lcom/google/android/youtube/core/async/l;

    .line 267
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ag;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/ag;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->n:Lcom/google/android/youtube/core/async/l;

    .line 269
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    .line 270
    new-instance v1, Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v1, p1, p3, v0}, Lcom/google/android/youtube/core/transfer/x;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->j:Lcom/google/android/youtube/core/transfer/x;

    .line 272
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/Button;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ah;
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 492
    const-string v0, "contentUri may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    :try_start_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->f:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/x;->b:[Ljava/lang/String;

    const-string v3, "mime_type LIKE \'video/%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_12
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_12} :catch_17

    move-result-object v1

    .line 496
    if-nez v1, :cond_3a

    move-object v0, v6

    .line 540
    :goto_16
    return-object v0

    .line 499
    :catch_17
    move-exception v0

    .line 500
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error resolving content from URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    move-object v0, v6

    .line 501
    goto :goto_16

    .line 504
    :cond_3a
    :try_start_3a
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_e6

    move-result v0

    if-nez v0, :cond_45

    .line 505
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_16

    .line 507
    :cond_45
    :try_start_45
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ah;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;-><init>(B)V

    .line 508
    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/Long;)Ljava/lang/Long;

    .line 509
    const-string v2, "_data"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 510
    const-string v2, "_display_name"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 511
    const-string v2, "_size"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/Long;)Ljava/lang/Long;

    .line 512
    const-string v2, "mime_type"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->c(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 513
    const-string v2, "duration"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->c(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/Long;)Ljava/lang/Long;

    .line 514
    const-string v2, "latitude"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->d(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 515
    const-string v2, "longitude"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->e(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 516
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->q:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_d2

    .line 517
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->q:Landroid/graphics/Bitmap;

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 522
    :goto_9c
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->i(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_b4

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->j(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_eb

    .line 523
    :cond_b4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unable to read video file ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_cc
    .catchall {:try_start_45 .. :try_end_cc} :catchall_e6

    .line 524
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_16

    .line 519
    :cond_d2
    :try_start_d2
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->f:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->i(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v7, 0x0

    invoke-static {v2, v3, v4, v5, v7}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_e5
    .catchall {:try_start_d2 .. :try_end_e5} :catchall_e6

    goto :goto_9c

    .line 540
    :catchall_e6
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 526
    :cond_eb
    :try_start_eb
    new-instance v2, Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->j(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 527
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 528
    const-string v3, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_122

    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "not a file uri ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_11c
    .catchall {:try_start_eb .. :try_end_11c} :catchall_e6

    .line 530
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_16

    .line 532
    :cond_122
    :try_start_122
    invoke-static {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;Landroid/net/Uri;)Landroid/net/Uri;

    .line 533
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->f(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 534
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->k(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15a

    .line 535
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid file type ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->k(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_154
    .catchall {:try_start_122 .. :try_end_154} :catchall_e6

    .line 536
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_16

    .line 540
    :cond_15a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_16
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/x;Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ah;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ah;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Lcom/google/android/youtube/core/transfer/d;
    .registers 8
    .parameter

    .prologue
    .line 442
    new-instance v0, Lcom/google/android/youtube/core/transfer/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/transfer/d;-><init>()V

    .line 443
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->p:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 444
    const-string v1, "username"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_10
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1f

    .line 447
    const-string v1, "upload_title"

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :cond_1f
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->d(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_32

    .line 450
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->d(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    .line 451
    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;[B)V

    .line 453
    :cond_32
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->h(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_48

    .line 454
    const-string v1, "upload_file_duration"

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->h(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;J)V

    .line 456
    :cond_48
    const-string v1, "upload_start_time_millis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;J)V

    .line 457
    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v1, "upload_description"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v1, "upload_keywords"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->G:Z

    if-eqz v1, :cond_9b

    .line 462
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/util/Pair;

    move-result-object v1

    .line 463
    const-string v2, "upload_location"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_9b
    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 545
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 546
    if-gez v0, :cond_8

    .line 547
    const/4 v0, 0x0

    .line 549
    :goto_7
    return-object v0

    :cond_8
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_7
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/x;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/x;Lcom/google/android/youtube/core/model/SocialSettings;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 83
    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-eqz v0, :cond_28

    if-eqz p1, :cond_29

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->V:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->U:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->facebook:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->twitter:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->orkut:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    :cond_28
    :goto_28
    return-void

    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->V:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->U:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->T:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_28
.end method

.method private a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v6, 0x0

    .line 753
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 754
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->R:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 755
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->S:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 756
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 757
    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 758
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 759
    if-eqz p2, :cond_41

    .line 760
    iget-boolean v2, p2, Lcom/google/android/youtube/core/model/k;->b:Z

    if-eqz v2, :cond_3d

    iget-object v2, p2, Lcom/google/android/youtube/core/model/k;->c:Ljava/util/Set;

    new-instance v3, Lcom/google/android/youtube/core/model/SocialSettings$Action;

    sget-object v4, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->UPLOAD:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/core/model/SocialSettings$Action;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;Z)V

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 763
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 776
    :goto_3c
    return-void

    .line 765
    :cond_3d
    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setVisibility(I)V

    goto :goto_3c

    .line 768
    :cond_41
    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 769
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3c
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/x;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->J:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/x;)J
    .registers 3
    .parameter

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->K:J

    return-wide v0
.end method

.method private static b(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/util/Pair;
    .registers 3
    .parameter

    .prologue
    .line 469
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 553
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 554
    if-gez v0, :cond_8

    .line 555
    const/4 v0, 0x0

    .line 557
    :goto_7
    return-object v0

    :cond_8
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/x;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->N:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/ui/x;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->h:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/ui/x;)V
    .registers 1
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->h()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/ui/x;)V
    .registers 3
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->B:Lcom/google/android/youtube/app/ui/df;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/df;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/transfer/x;->a(Lcom/google/android/youtube/core/transfer/aa;)V

    :goto_d
    return-void

    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->d:Landroid/app/Activity;

    const/16 v1, 0x3fd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_d
.end method

.method static synthetic f()Landroid/net/Uri;
    .registers 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->c:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/ui/x;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private g()V
    .registers 4

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->g:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 632
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

    if-eqz v0, :cond_1e

    .line 633
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ae;->i()V

    .line 635
    :cond_1e
    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->t:Landroid/widget/ImageView;

    return-object v0
.end method

.method private h()V
    .registers 18

    .prologue
    .line 638
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    .line 639
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->x:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    .line 640
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->D:Ljava/lang/String;

    .line 641
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->z:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->E:Ljava/lang/String;

    .line 642
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->a()Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    .line 643
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->w:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->G:Z

    .line 645
    sget-boolean v1, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-nez v1, :cond_c5

    const/4 v1, 0x0

    new-array v14, v1, [Ljava/lang/String;

    .line 647
    :goto_5b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_108

    .line 648
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/android/youtube/app/honeycomb/ui/ah;

    .line 649
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_84

    .line 650
    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->f(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    .line 652
    :cond_84
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 653
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->l(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->f(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->D:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->G:Z

    if-eqz v10, :cond_106

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/util/Pair;

    move-result-object v10

    :goto_b6
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->J:Z

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v14}, Lcom/google/android/youtube/core/transfer/x;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/transfer/d;ZZ[Ljava/lang/String;)V

    .line 692
    :cond_c4
    return-void

    .line 645
    :cond_c5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->Q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_d6
    :goto_d6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_fa

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_d6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d6

    :cond_fa
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v14, v1

    goto/16 :goto_5b

    .line 653
    :cond_106
    const/4 v10, 0x0

    goto :goto_b6

    .line 668
    :cond_108
    const/4 v1, 0x1

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v2, v1

    :goto_112
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/android/youtube/app/honeycomb/ui/ah;

    .line 670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16c

    .line 671
    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->f(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    move v15, v2

    .line 676
    :goto_131
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->l(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->f(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->D:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->G:Z

    if-eqz v10, :cond_193

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Landroid/util/Pair;

    move-result-object v10

    :goto_15c
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/app/honeycomb/ui/ah;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->J:Z

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v14}, Lcom/google/android/youtube/core/transfer/x;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/transfer/d;ZZ[Ljava/lang/String;)V

    move v2, v15

    goto :goto_112

    .line 673
    :cond_16c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->C:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ah;->g(Lcom/google/android/youtube/app/honeycomb/ui/ah;Ljava/lang/String;)Ljava/lang/String;

    .line 674
    add-int/lit8 v15, v2, 0x1

    goto :goto_131

    .line 676
    :cond_193
    const/4 v10, 0x0

    goto :goto_15c
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method private i()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 708
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->M:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->N:Z

    if-eqz v0, :cond_12

    .line 709
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_13

    .line 710
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    .line 715
    :cond_12
    :goto_12
    return-void

    .line 711
    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    if-eqz v0, :cond_12

    .line 712
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_12
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/content/res/Resources;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->e:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->v:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->w:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/ui/x;)Lcom/google/android/youtube/app/compat/t;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->s:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/honeycomb/ui/x;)V
    .registers 1
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->i()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/youtube/app/honeycomb/ui/x;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->k:Lcom/google/android/youtube/core/d;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 432
    packed-switch p1, :pswitch_data_c

    .line 437
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 434
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->B:Lcom/google/android/youtube/app/ui/df;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/df;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 432
    :pswitch_data_c
    .packed-switch 0x3fd
        :pswitch_5
    .end packed-switch
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/x;->a()V

    .line 337
    return-void
.end method

.method public final a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 275
    const-string v0, "intent can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    const-string v0, "userAuth can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    .line 278
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 279
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 280
    const-string v1, "com.google.android.youtube.intent.action.UPLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 282
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 283
    if-eqz v1, :cond_32

    .line 284
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->q:Landroid/graphics/Bitmap;

    .line 285
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    :cond_32
    :goto_32
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9b

    move v1, v2

    .line 302
    :goto_39
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 303
    :cond_3d
    :goto_3d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 304
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 305
    if-eqz v0, :cond_57

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "content://media/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3d

    .line 306
    :cond_57
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Ignoring non-media-content uri: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 307
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_3d

    .line 287
    :cond_6d
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_83

    .line 289
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 290
    if-eqz v0, :cond_32

    .line 291
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 293
    :cond_83
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 295
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_32

    .line 297
    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_32

    :cond_9b
    move v1, v3

    .line 301
    goto :goto_39

    .line 310
    :cond_9d
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 311
    if-eqz v1, :cond_ad

    .line 312
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->k:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b00db

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 315
    :cond_ad
    const-string v0, "no media content uri(s)"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 325
    :goto_b7
    return-void

    .line 319
    :cond_b8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_c3

    .line 320
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->L:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->m:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 322
    :cond_c3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->K:J

    .line 323
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "UploadFormShown"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ab;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/ui/ab;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    new-array v1, v2, [Ljava/util/List;

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ab;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_b7
.end method

.method public final a(Ljava/lang/Exception;)V
    .registers 6
    .parameter

    .prologue
    const v2, 0x7f0b00cf

    const/4 v1, 0x1

    .line 603
    const-string v0, "Error requesting location for upload"

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 604
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    .line 605
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    if-nez v0, :cond_58

    .line 606
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_68

    .line 607
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->k:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_59

    .line 609
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->d(I)Lcom/google/android/youtube/app/compat/t;

    .line 610
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    .line 615
    :cond_2e
    :goto_2e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "UploadDestinationError"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_58
    :goto_58
    return-void

    .line 611
    :cond_59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    if-eqz v0, :cond_2e

    .line 612
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 613
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->O:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2e

    .line 618
    :cond_68
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->g()V

    goto :goto_58
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 584
    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    if-eqz p1, :cond_11

    const/4 v0, 0x0

    :goto_5
    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    .line 585
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    if-nez v0, :cond_10

    .line 586
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->g()V

    .line 588
    :cond_10
    return-void

    .line 584
    :cond_11
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 404
    const v1, 0x7f08019e

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v1

    .line 405
    if-eqz v1, :cond_30

    .line 406
    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 407
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->l:Lcom/google/android/youtube/app/compat/r;

    const v2, 0x7f11000d

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 408
    const v1, 0x7f0801ad

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    .line 409
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v1, :cond_30

    .line 410
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->P:Lcom/google/android/youtube/app/compat/t;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/ac;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/ac;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    .line 424
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->i()V

    .line 425
    const/4 v0, 0x1

    .line 428
    :cond_30
    return v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->h()V

    .line 579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->M:Z

    .line 580
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->i()V

    .line 581
    return-void
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 591
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    .line 592
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_21

    .line 593
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->d:Landroid/app/Activity;

    const v1, 0x7f0b0162

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    .line 594
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

    if-eqz v0, :cond_20

    .line 595
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->i:Lcom/google/android/youtube/app/honeycomb/ui/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ae;->j()V

    .line 600
    :cond_20
    :goto_20
    return-void

    .line 597
    :cond_21
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->I:I

    if-nez v0, :cond_20

    .line 598
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->g()V

    goto :goto_20
.end method

.method public final d()V
    .registers 1

    .prologue
    .line 625
    return-void
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 718
    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_17

    .line 719
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->T:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 720
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->L:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->o:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->n:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->e(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 722
    :cond_17
    return-void
.end method
