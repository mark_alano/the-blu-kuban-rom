.class public final Lcom/google/android/youtube/app/player/a;
.super Lcom/google/android/youtube/core/player/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/n;


# static fields
.field public static final a:Ljava/util/Set;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/youtube/core/utils/l;

.field private d:Landroid/content/Context;

.field private e:Landroid/net/Uri;

.field private f:Ljava/util/concurrent/atomic/AtomicReference;

.field private g:I

.field private h:J

.field private i:Lcom/google/android/youtube/core/transfer/b;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private final n:Ljava/util/concurrent/atomic/AtomicLong;

.field private final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final p:Ljava/util/concurrent/atomic/AtomicReference;

.field private final q:Lcom/google/android/youtube/core/utils/v;

.field private r:J

.field private final s:Lcom/google/android/youtube/app/player/h;

.field private t:Landroid/os/Handler;

.field private final u:I

.field private final v:I

.field private final w:Lcom/google/android/youtube/app/player/i;

.field private final x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "awf-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 62
    const-string v1, "video/mp4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    const-string v1, "video/3gpp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/player/a;->a:Ljava/util/Set;

    .line 67
    new-instance v0, Lcom/google/android/youtube/app/player/b;

    invoke-direct {v0}, Lcom/google/android/youtube/app/player/b;-><init>()V

    .line 86
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 87
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/aq;IILcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/player/i;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/s;-><init>(Lcom/google/android/youtube/core/player/aq;)V

    .line 513
    new-instance v0, Lcom/google/android/youtube/app/player/g;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/player/g;-><init>(Lcom/google/android/youtube/app/player/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    .line 148
    if-lez p2, :cond_66

    move v0, v1

    :goto_f
    const-string v3, "bufferLowMillis must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 149
    if-lez p3, :cond_68

    :goto_16
    const-string v0, "bufferFullMillis must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 150
    const-string v0, "errorListener cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/i;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->w:Lcom/google/android/youtube/app/player/i;

    .line 151
    const-string v0, "networkStatus cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->c:Lcom/google/android/youtube/core/utils/l;

    .line 152
    iput p2, p0, Lcom/google/android/youtube/app/player/a;->u:I

    .line 153
    iput p3, p0, Lcom/google/android/youtube/app/player/a;->v:I

    .line 154
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 155
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v3, 0x40000

    invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    .line 156
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 157
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    .line 158
    new-instance v0, Lcom/google/android/youtube/core/utils/v;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/v;

    .line 159
    new-instance v0, Lcom/google/android/youtube/app/player/h;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/player/h;-><init>(Lcom/google/android/youtube/app/player/a;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    .line 160
    const-string v0, ".*Awful.*"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    .line 161
    return-void

    :cond_66
    move v0, v2

    .line 148
    goto :goto_f

    :cond_68
    move v1, v2

    .line 149
    goto :goto_16
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;I)J
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(J)V
    .registers 15
    .parameter

    .prologue
    const/4 v9, 0x1

    .line 298
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->i()V

    .line 299
    iput-wide p1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    .line 300
    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_17

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_3e

    .line 301
    :cond_17
    new-instance v0, Lcom/google/android/youtube/core/transfer/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-wide/16 v5, 0x0

    const/high16 v8, 0x2

    const/4 v11, 0x0

    move-wide v3, p1

    move-object v7, p0

    move v10, v9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/core/transfer/b;-><init>(Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/youtube/core/transfer/n;IZZLcom/google/android/youtube/core/transfer/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    .line 303
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 305
    :cond_3e
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;)V
    .registers 1
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->m()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;J)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x1

    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/v;

    iget-wide v1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/youtube/core/utils/v;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_29

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "filled gap, downloading from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-long v3, v0, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    add-long v2, v0, v6

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/app/player/a;->a(J)V

    :cond_29
    const-wide/16 v2, 0x64

    mul-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v4, v6

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/player/a;->b(I)V

    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-eqz v2, :cond_7e

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_66

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gtz v2, :cond_51

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v2, v6

    cmp-long v2, v0, v2

    if-ltz v2, :cond_66

    :cond_51
    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_67

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Lcom/google/android/youtube/app/player/f;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/player/f;-><init>(Lcom/google/android/youtube/app/player/a;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/f;->start()V

    :cond_66
    :goto_66
    return-void

    :cond_67
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "start buffer full "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_66

    :cond_7e
    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-eqz v2, :cond_c1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->getCurrentPosition()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/app/player/a;->v:I

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gtz v2, :cond_98

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v2, v6

    cmp-long v2, v0, v2

    if-ltz v2, :cond_66

    :cond_98
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "full buffer at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->a(Z)V

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_b9

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/player/s;->seekTo(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    :cond_b9
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-nez v0, :cond_66

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->k()V

    goto :goto_66

    :cond_c1
    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/player/h;->d()Z

    move-result v2

    if-eqz v2, :cond_66

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->getCurrentPosition()I

    move-result v2

    const v3, 0x15f90

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_66

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->i()V

    goto :goto_66
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 249
    iput-boolean p1, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    .line 250
    if-eqz p1, :cond_b

    const/16 v0, 0x2bd

    :goto_6
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->a(II)Z

    .line 254
    return-void

    .line 250
    :cond_b
    const/16 v0, 0x2be

    goto :goto_6
.end method

.method static synthetic b(Lcom/google/android/youtube/app/player/a;)V
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    if-nez v1, :cond_1e

    add-int/lit16 v0, v0, 0x7530

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_1d

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->a(J)V

    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    iget v1, p0, Lcom/google/android/youtube/app/player/a;->u:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_1d

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "low buffer at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-direct {p0, v6}, Lcom/google/android/youtube/app/player/a;->a(Z)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->c()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->l()V

    goto :goto_1d

    :cond_5e
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v1, 0x2

    const/16 v2, -0xfa0

    invoke-static {v0, v1, v6, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1d
.end method

.method static synthetic b(Lcom/google/android/youtube/app/player/a;J)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/player/a;->a(J)V

    return-void
.end method

.method private c(I)J
    .registers 5
    .parameter

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 554
    if-eqz v0, :cond_2c

    .line 555
    div-int/lit16 v1, p1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    .line 556
    invoke-interface {v1}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2c

    .line 557
    invoke-interface {v1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 558
    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    .line 561
    :goto_2b
    return-wide v0

    :cond_2c
    const-wide/16 v0, 0x0

    goto :goto_2b
.end method

.method static synthetic c(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .registers 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/player/a;)I
    .registers 2
    .parameter

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->v:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/player/a;)J
    .registers 3
    .parameter

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    return-wide v0
.end method

.method private g()V
    .registers 6

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->b()V

    .line 209
    iput-object v3, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    .line 210
    iput v4, p0, Lcom/google/android/youtube/app/player/a;->g:I

    .line 211
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    .line 213
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->i()V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    if-eqz v0, :cond_1c

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 218
    :cond_1c
    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    .line 219
    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    .line 220
    iput v4, p0, Lcom/google/android/youtube/app/player/a;->m:I

    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/v;->a()V

    .line 223
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v1, 0x40000

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 228
    iput-object v3, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    .line 229
    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/app/player/a;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    return-object v0
.end method

.method private h()V
    .registers 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 233
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 234
    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 235
    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 236
    new-instance v1, Lcom/google/android/youtube/app/player/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/player/e;-><init>(Lcom/google/android/youtube/app/player/a;Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    .line 244
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 246
    :cond_23
    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private i()V
    .registers 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    if-eqz v0, :cond_c

    .line 292
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/b;->a()V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    .line 295
    :cond_c
    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    return-object v0
.end method

.method private j()V
    .registers 6

    .prologue
    .line 358
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->c(I)J

    move-result-wide v0

    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "seek offset "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 360
    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/v;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/youtube/core/utils/v;->a(J)J

    move-result-wide v1

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "next gap at "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 363
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->a(Z)V

    .line 364
    const-wide/16 v3, 0x1

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/google/android/youtube/app/player/a;->r:J

    .line 365
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->i()V

    .line 366
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {p0, v0, v3, v4}, Lcom/google/android/youtube/app/player/a;->b(Ljava/lang/String;J)V

    .line 367
    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/app/player/a;->a(J)V

    .line 368
    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/l;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->c:Lcom/google/android/youtube/core/utils/l;

    return-object v0
.end method

.method private k()V
    .registers 1

    .prologue
    .line 530
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->start()V

    .line 531
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->m()V

    .line 532
    return-void
.end method

.method static synthetic l(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/transfer/b;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    return-object v0
.end method

.method private l()V
    .registers 3

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 536
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->pause()V

    .line 537
    return-void
.end method

.method private m()V
    .registers 5

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 542
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, -0xfa1

    .line 448
    :try_start_4
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->prepare()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_7} :catch_1f

    .line 454
    :try_start_7
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/a;->g:I
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_d} :catch_24

    .line 461
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 462
    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    .line 463
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->f()V

    .line 464
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_2c

    .line 465
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->j()V

    .line 473
    :goto_1e
    return-void

    .line 450
    :catch_1f
    move-exception v0

    invoke-virtual {p0, p0, v3, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    goto :goto_1e

    .line 456
    :catch_24
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->release()V

    .line 458
    invoke-virtual {p0, p0, v3, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    goto :goto_1e

    .line 467
    :cond_2c
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-eqz v0, :cond_34

    .line 468
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->l()V

    goto :goto_1e

    .line 470
    :cond_34
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->k()V

    goto :goto_1e
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 259
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 567
    iput-wide p2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "size is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 569
    const-wide/32 v0, 0x6000000

    cmp-long v0, p2, v0

    if-lez v0, :cond_20

    .line 570
    const/4 v0, 0x1

    const/16 v1, -0xfa3

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    .line 572
    :cond_20
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x2

    const/16 v6, -0xfa0

    const/4 v1, 0x1

    .line 588
    const-string v0, "download error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 589
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    .line 590
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->c()Z

    move-result v0

    if-eqz v0, :cond_4e

    move v0, v1

    .line 591
    :goto_19
    iget-boolean v2, p2, Lcom/google/android/youtube/core/transfer/TransferException;->fatal:Z

    if-nez v2, :cond_1f

    if-eqz v0, :cond_5a

    .line 592
    :cond_1f
    invoke-virtual {p2}, Lcom/google/android/youtube/core/transfer/TransferException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_50

    .line 593
    new-instance v0, Landroid/os/StatFs;

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 594
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    .line 595
    const-wide/32 v4, 0x20000

    cmp-long v0, v2, v4

    if-gez v0, :cond_50

    .line 596
    const/16 v0, -0xfa2

    invoke-virtual {p0, p0, v1, v0}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    .line 608
    :cond_4d
    :goto_4d
    return-void

    .line 590
    :cond_4e
    const/4 v0, 0x0

    goto :goto_19

    .line 600
    :cond_50
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-static {v0, v7, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_4d

    .line 603
    :cond_5a
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-eqz v0, :cond_4d

    .line 604
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-static {v0, v7, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_4d
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/d;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/google/android/youtube/app/player/a;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 585
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 502
    const/4 v0, 0x1

    if-ne p2, v0, :cond_19

    const/16 v0, 0x10

    if-ne p3, v0, :cond_19

    .line 504
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 510
    :goto_18
    return v4

    .line 509
    :cond_19
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/s;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    goto :goto_18
.end method

.method public final b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 666
    sget-object v0, Lcom/google/android/youtube/app/player/a;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final b(Ljava/lang/String;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 577
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 522
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->g()V

    .line 523
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->h()V

    .line 524
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->w:Lcom/google/android/youtube/app/player/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/player/i;->a()V

    .line 525
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/s;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    .line 526
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 671
    const/4 v0, 0x0

    return v0
.end method

.method public final getCurrentPosition()I
    .registers 2

    .prologue
    .line 330
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_7

    .line 331
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    .line 337
    :goto_6
    return v0

    .line 334
    :cond_7
    :try_start_7
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->getCurrentPosition()I
    :try_end_a
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_a} :catch_c

    move-result v0

    goto :goto_6

    .line 337
    :catch_c
    move-exception v0

    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final isPlaying()Z
    .registers 2

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final pause()V
    .registers 2

    .prologue
    .line 309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    .line 310
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-nez v0, :cond_e

    .line 311
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->l()V

    .line 313
    :cond_e
    return-void
.end method

.method public final prepare()V
    .registers 2

    .prologue
    .line 263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final prepareAsync()V
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 268
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    :try_start_7
    sget-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    const-string v1, ".tmp"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    .line 273
    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "created file buffer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 275
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/player/s;->setDataSource(Ljava/lang/String;)V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_3a} :catch_42
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_3a} :catch_4f

    .line 286
    iput-boolean v3, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    .line 287
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->a()V

    .line 288
    :goto_41
    return-void

    .line 277
    :catch_42
    move-exception v0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/16 v1, -0xfa2

    invoke-static {v0, v4, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_41

    .line 281
    :catch_4f
    move-exception v0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/16 v1, -0xfa1

    invoke-static {v0, v4, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_41
.end method

.method public final release()V
    .registers 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->g()V

    .line 202
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->release()V

    .line 203
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->h()V

    .line 204
    return-void
.end method

.method public final seekTo(I)V
    .registers 4
    .parameter

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-eq p1, v0, :cond_25

    .line 345
    iput p1, p0, Lcom/google/android/youtube/app/player/a;->m:I

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "seek position "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 347
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_25

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->g:I

    if-lez v0, :cond_25

    .line 348
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->l()V

    .line 349
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->j()V

    .line 352
    :cond_25
    return-void
.end method

.method public final setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->g()V

    .line 166
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->h()V

    .line 167
    iput-object p1, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    .line 168
    iput-object p2, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    .line 170
    new-instance v0, Lcom/google/android/youtube/app/player/d;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/player/d;-><init>(Lcom/google/android/youtube/app/player/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    .line 190
    return-void
.end method

.method public final start()V
    .registers 2

    .prologue
    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    .line 318
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-nez v0, :cond_e

    .line 319
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->k()V

    .line 321
    :cond_e
    return-void
.end method
