.class final Lcom/google/android/youtube/app/player/h;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/player/a;

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/player/a;)V
    .registers 2
    .parameter

    .prologue
    .line 612
    iput-object p1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/player/a;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 612
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/player/h;-><init>(Lcom/google/android/youtube/app/player/a;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 3

    .prologue
    .line 619
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z

    if-nez v0, :cond_18

    .line 620
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 621
    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1a

    .line 624
    :cond_18
    monitor-exit p0

    return-void

    .line 619
    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .registers 2

    .prologue
    .line 627
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 628
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    .line 631
    :cond_19
    monitor-exit p0

    return-void

    .line 627
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .registers 2

    .prologue
    .line 655
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .registers 2

    .prologue
    .line 659
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->d:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 635
    monitor-enter p0

    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_51

    if-nez v1, :cond_8

    .line 652
    :cond_6
    :goto_6
    monitor-exit p0

    return-void

    .line 639
    :cond_8
    :try_start_8
    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/l;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    .line 640
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 641
    iget-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    if-eqz v1, :cond_39

    .line 642
    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/l;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/l;->c()Z

    move-result v1

    if-nez v1, :cond_54

    :goto_37
    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->d:Z

    .line 644
    :cond_39
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->l(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/transfer/b;

    move-result-object v0

    if-nez v0, :cond_6

    .line 645
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    if-eqz v0, :cond_56

    .line 646
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->g(Lcom/google/android/youtube/app/player/a;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/app/player/a;J)V
    :try_end_50
    .catchall {:try_start_8 .. :try_end_50} :catchall_51

    goto :goto_6

    .line 635
    :catchall_51
    move-exception v0

    monitor-exit p0

    throw v0

    .line 642
    :cond_54
    const/4 v0, 0x0

    goto :goto_37

    .line 648
    :cond_56
    :try_start_56
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    const/4 v2, 0x1

    const/16 v3, -0xfa0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/aq;II)Z
    :try_end_60
    .catchall {:try_start_56 .. :try_end_60} :catchall_51

    goto :goto_6
.end method
