.class public interface abstract Lcom/google/android/youtube/app/remote/RemoteControl;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(I)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/aj;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/an;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/ax;Ljava/lang/String;J)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/Video;)Z
.end method

.method public abstract b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Lcom/google/android/youtube/app/remote/aj;)V
.end method

.method public abstract b(Lcom/google/android/youtube/app/remote/an;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()V
.end method

.method public abstract c(Lcom/google/android/youtube/app/remote/aj;)V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract i()I
.end method

.method public abstract j()D
.end method

.method public abstract k()Ljava/util/List;
.end method

.method public abstract l()Z
.end method

.method public abstract m()Lcom/google/android/youtube/app/remote/ak;
.end method

.method public abstract n()V
.end method

.method public abstract o()I
.end method

.method public abstract p()Z
.end method

.method public abstract q()Z
.end method

.method public abstract r()V
.end method

.method public abstract s()Z
.end method

.method public abstract t()V
.end method

.method public abstract u()Lcom/google/android/youtube/app/remote/ax;
.end method

.method public abstract w()Lcom/google/android/youtube/app/remote/RemoteControl$State;
.end method

.method public abstract x()Lcom/google/android/youtube/app/remote/al;
.end method
