.class final Lcom/google/android/youtube/app/remote/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/aq;

.field private final b:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/model/Video;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object p2, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    .line 247
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 241
    const-string v0, "Error downloading thumbnail"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->i(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/at;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/remote/at;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcom/google/android/youtube/app/remote/ao;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 241
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0, p2}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/aq;->i(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/at;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/youtube/app/remote/at;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/aq;->d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/app/remote/ao;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_55

    const/4 v0, 0x1

    :goto_3d
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->s()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/as;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/remote/ao;->a(ZZZ)V

    return-void

    :cond_55
    const/4 v0, 0x0

    goto :goto_3d
.end method
