.class final Lcom/google/android/youtube/app/remote/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/aj;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/aq;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/aq;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/au;-><init>(Lcom/google/android/youtube/app/remote/aq;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 3
    .parameter

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v0, :cond_18

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->b(Lcom/google/android/youtube/app/remote/aq;)V

    .line 144
    :cond_e
    :goto_e
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/remote/ao;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    .line 145
    return-void

    .line 140
    :cond_18
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_24

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_24

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ADVERTISEMENT:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v0, :cond_e

    .line 142
    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/au;->a(Ljava/lang/String;)V

    goto :goto_e
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 4
    .parameter

    .prologue
    .line 148
    sget-object v0, Lcom/google/android/youtube/app/remote/ar;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_18

    .line 155
    :goto_b
    return-void

    .line 152
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->b(Lcom/google/android/youtube/app/remote/aq;)V

    goto :goto_b

    .line 148
    nop

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 158
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;)V

    .line 166
    :goto_b
    return-void

    .line 162
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->b(Lcom/google/android/youtube/app/remote/aq;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    new-instance v1, Lcom/google/android/youtube/core/async/m;

    new-instance v2, Lcom/google/android/youtube/app/remote/av;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, v4}, Lcom/google/android/youtube/app/remote/av;-><init>(Lcom/google/android/youtube/app/remote/aq;Ljava/lang/String;B)V

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/async/m;-><init>(Lcom/google/android/youtube/core/async/bn;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/m;

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->f(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/aq;->e(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    goto :goto_b
.end method

.method public final a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2f

    const/4 v0, 0x1

    :goto_17
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->s()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/remote/ao;->a(ZZZ)V

    .line 173
    return-void

    .line 169
    :cond_2f
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 179
    return-void
.end method

.method public final l()V
    .registers 1

    .prologue
    .line 176
    return-void
.end method
