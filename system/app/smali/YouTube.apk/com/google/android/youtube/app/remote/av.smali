.class final Lcom/google/android/youtube/app/remote/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/aq;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/aq;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p2, p0, Lcom/google/android/youtube/app/remote/av;->b:Ljava/lang/String;

    .line 198
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/aq;Ljava/lang/String;B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 192
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/av;-><init>(Lcom/google/android/youtube/app/remote/aq;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/m;

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->h(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/av;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->g(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/l;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 214
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 206
    const-string v0, "Error while authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/m;

    .line 208
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->h(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/av;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->g(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/l;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 209
    return-void
.end method

.method public final i_()V
    .registers 5

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/m;

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->h(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/av;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->g(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/l;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 203
    return-void
.end method
