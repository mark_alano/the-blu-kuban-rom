.class final Lcom/google/android/youtube/app/remote/b;
.super Landroid/support/place/api/broker/BrokerManager$ConnectionListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/AtHomeConnection;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V
    .registers 2
    .parameter

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-direct {p0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/b;-><init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method


# virtual methods
.method public final onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .registers 3
    .parameter

    .prologue
    .line 155
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 157
    return-void
.end method

.method public final onBrokerDisconnected()V
    .registers 2

    .prologue
    .line 173
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 175
    return-void
.end method

.method public final onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V
    .registers 3
    .parameter

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 181
    return-void
.end method

.method public final onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .registers 4
    .parameter

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlaceConnnected - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 163
    return-void
.end method

.method public final onPlaceDisconnected()V
    .registers 2

    .prologue
    .line 167
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 169
    return-void
.end method

.method public final onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V
    .registers 3
    .parameter

    .prologue
    .line 185
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    .line 187
    return-void
.end method
