.class public final Lcom/google/android/youtube/app/remote/bb;
.super Lcom/google/android/youtube/app/remote/n;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/bo;


# static fields
.field private static final a:Lorg/json/JSONObject;

.field private static final b:Landroid/content/IntentFilter;

.field private static final c:Ljava/util/Map;

.field private static final d:Ljava/util/Random;


# instance fields
.field private A:I

.field private B:D

.field private C:J

.field private D:Z

.field private final E:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Lcom/google/android/ytremote/backend/browserchannel/i;

.field private final h:Lcom/google/android/ytremote/logic/c;

.field private final i:Lcom/google/android/youtube/core/utils/l;

.field private final j:Lcom/google/android/youtube/app/remote/bd;

.field private final k:Lcom/google/android/ytremote/backend/a/a;

.field private final l:Lcom/google/android/ytremote/backend/logic/b;

.field private final m:Ljava/util/Map;

.field private final n:Z

.field private final o:Landroid/content/SharedPreferences;

.field private p:Z

.field private q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private r:Z

.field private s:Lcom/google/android/youtube/app/remote/bk;

.field private t:Lcom/google/android/ytremote/model/CloudScreen;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private final w:Ljava/util/List;

.field private final x:Ljava/util/Map;

.field private y:Landroid/os/Handler;

.field private z:Lcom/google/android/youtube/app/remote/bj;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 94
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bb;->a:Lorg/json/JSONObject;

    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 99
    sput-object v0, Lcom/google/android/youtube/app/remote/bb;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->LOUNGE_SERVER_CONNECTION_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bb;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_NO_NETWORK:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bb;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_IPV6_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 102
    const-string v1, "notFound"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string v1, "private"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v1, "restrictedRegion"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->REGION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v1, "limitedSyndication"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIMITED_SYNDICATION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/remote/bb;->c:Ljava/util/Map;

    .line 109
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bb;->d:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/l;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Z)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/n;-><init>(Landroid/content/Context;)V

    .line 135
    const/16 v1, 0x1e

    iput v1, p0, Lcom/google/android/youtube/app/remote/bb;->A:I

    .line 149
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->n:Z

    .line 151
    const-string v1, "context can not be null"

    invoke-static {p1, v1}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v1, "executor can not be null"

    invoke-static {p2, v1}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->f:Ljava/util/concurrent/Executor;

    .line 153
    const-string v1, "preferences can not be null"

    invoke-static {p4, v1}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->o:Landroid/content/SharedPreferences;

    .line 154
    const-string v1, "userAuthorizer can not be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->e:Landroid/content/Context;

    .line 158
    iput-object p3, p0, Lcom/google/android/youtube/app/remote/bb;->i:Lcom/google/android/youtube/core/utils/l;

    move-object v1, p1

    .line 160
    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 161
    new-instance v9, Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->W()Ljava/lang/String;

    move-result-object v1

    new-instance v10, Lcom/google/android/youtube/app/remote/bh;

    const/4 v3, 0x0

    invoke-direct {v10, p0, v3}, Lcom/google/android/youtube/app/remote/bh;-><init>(Lcom/google/android/youtube/app/remote/bb;B)V

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bb;->o:Landroid/content/SharedPreferences;

    const-string v4, "remote_id"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10a

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bb;->o:Landroid/content/SharedPreferences;

    const-string v4, "remote_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_5d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "android"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v1, "device"

    invoke-virtual {v2}, Lcom/google/android/ytremote/backend/model/DeviceType;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    const-string v5, "name"

    const-string v7, "app"

    invoke-static/range {v1 .. v8}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v7

    new-instance v1, Lcom/google/android/ytremote/backend/browserchannel/r;

    const-string v3, "www.youtube.com"

    const/16 v4, 0x50

    const-string v5, "/api/lounge/bc/"

    move-object v2, p1

    move-object v6, v10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/ytremote/backend/browserchannel/r;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/ytremote/backend/browserchannel/s;Ljava/util/Map;)V

    invoke-direct {v9, p1, v1}, Lcom/google/android/ytremote/backend/browserchannel/i;-><init>(Landroid/content/Context;Lcom/google/android/ytremote/backend/browserchannel/c;)V

    iput-object v9, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    .line 164
    new-instance v1, Lcom/google/android/ytremote/b/e;

    new-instance v2, Lcom/google/android/ytremote/backend/a/f;

    invoke-direct {v2}, Lcom/google/android/ytremote/backend/a/f;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/ytremote/b/e;-><init>(Lcom/google/android/ytremote/backend/logic/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->h:Lcom/google/android/ytremote/logic/c;

    .line 165
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->m:Ljava/util/Map;

    .line 166
    new-instance v1, Lcom/google/android/ytremote/backend/a/a;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/a/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->k:Lcom/google/android/ytremote/backend/a/a;

    .line 167
    new-instance v1, Lcom/google/android/ytremote/backend/a/d;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/a/d;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->l:Lcom/google/android/ytremote/backend/logic/b;

    .line 169
    new-instance v1, Lcom/google/android/youtube/app/remote/bd;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/remote/bd;-><init>(Lcom/google/android/youtube/app/remote/bb;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->j:Lcom/google/android/youtube/app/remote/bd;

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    .line 172
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->x:Ljava/util/Map;

    .line 174
    new-instance v1, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 177
    new-instance v2, Lcom/google/android/youtube/app/remote/bi;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v1, v3}, Lcom/google/android/youtube/app/remote/bi;-><init>(Lcom/google/android/youtube/app/remote/bb;Landroid/content/Context;Landroid/os/Looper;B)V

    iput-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    .line 179
    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    .line 180
    move-object/from16 v0, p5

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bo;)V

    .line 181
    return-void

    .line 161
    :cond_10a
    new-instance v3, Ljava/math/BigInteger;

    const/16 v4, 0x82

    sget-object v5, Lcom/google/android/youtube/app/remote/bb;->d:Ljava/util/Random;

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bb;->o:Landroid/content/SharedPreferences;

    invoke-static {v3}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v3

    const-string v5, "remote_id"

    invoke-virtual {v3, v5, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/app/compat/ad;->a()V

    goto/16 :goto_5d
.end method

.method static synthetic A()Landroid/content/IntentFilter;
    .registers 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/youtube/app/remote/bb;->b:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic B()Lorg/json/JSONObject;
    .registers 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/youtube/app/remote/bb;->a:Lorg/json/JSONObject;

    return-object v0
.end method

.method static synthetic C()Ljava/util/Map;
    .registers 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/youtube/app/remote/bb;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;I)Lcom/google/android/youtube/app/remote/al;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    packed-switch p1, :pswitch_data_12

    sget-object v0, Lcom/google/android/youtube/app/remote/am;->c:Lcom/google/android/youtube/app/remote/al;

    :goto_5
    return-object v0

    :pswitch_6
    sget-object v0, Lcom/google/android/youtube/app/remote/ba;->b:Lcom/google/android/youtube/app/remote/al;

    goto :goto_5

    :pswitch_9
    sget-object v0, Lcom/google/android/youtube/app/remote/ba;->d:Lcom/google/android/youtube/app/remote/al;

    goto :goto_5

    :pswitch_c
    sget-object v0, Lcom/google/android/youtube/app/remote/ba;->c:Lcom/google/android/youtube/app/remote/al;

    goto :goto_5

    :pswitch_f
    sget-object v0, Lcom/google/android/youtube/app/remote/am;->a:Lcom/google/android/youtube/app/remote/al;

    goto :goto_5

    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/backend/browserchannel/i;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;J)Lcom/google/android/ytremote/backend/model/Params;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoIds"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "currentTime"

    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/ytremote/model/CloudScreen;)Lcom/google/android/ytremote/model/CloudScreen;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->t:Lcom/google/android/ytremote/model/CloudScreen;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    return-object p1
.end method

.method private a(D)V
    .registers 5
    .parameter

    .prologue
    .line 502
    iput-wide p1, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bb;->C:J

    .line 504
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;D)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/bb;->a(D)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 75
    sget-object v0, Lcom/google/android/youtube/app/remote/bc;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3a

    :cond_d
    :goto_d
    return-void

    :pswitch_e
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_16

    iput-wide v2, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_d

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bb;->C:J

    goto :goto_d

    :pswitch_23
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_d

    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/youtube/app/remote/bb;->C:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    goto :goto_d

    :pswitch_37
    iput-wide v2, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    goto :goto_d

    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_23
        :pswitch_23
        :pswitch_23
        :pswitch_23
        :pswitch_37
        :pswitch_37
    .end packed-switch
.end method

.method private a(Lcom/google/android/youtube/app/remote/bk;Ljava/lang/String;J)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 184
    const-string v0, "screen can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_25

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/remote/bk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 189
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    .line 190
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 191
    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/bb;->a(Ljava/lang/String;)V

    .line 204
    :cond_24
    :goto_24
    return-void

    .line 194
    :cond_25
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    .line 195
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 197
    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bk;->d()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 198
    iget-object v7, p0, Lcom/google/android/youtube/app/remote/bb;->f:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bk;->b()Lcom/google/android/ytremote/model/b;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/remote/bf;-><init>(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/ytremote/model/b;Ljava/lang/String;JB)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_24

    .line 200
    :cond_44
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v8, 0x3

    new-instance v1, Lcom/google/android/youtube/app/remote/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bk;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v2

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/app/remote/be;-><init>(Lcom/google/android/ytremote/model/CloudScreen;Ljava/lang/String;JB)V

    invoke-static {v7, v8, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_24
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bb;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->D:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->u:Ljava/lang/String;

    return-object p1
.end method

.method private b(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1f

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 564
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 565
    const-string v1, "username"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 566
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->UPDATE_USERNAME:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 568
    :cond_1f
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bb;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->p:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bb;)V
    .registers 4
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->GET_NOW_PLAYING:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bb;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/google/android/youtube/app/remote/bb;->r:Z

    return p1
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/bb;)Z
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->p:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bd;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->j:Lcom/google/android/youtube/app/remote/bd;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/model/CloudScreen;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->t:Lcom/google/android/ytremote/model/CloudScreen;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->x:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/bb;)Z
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->r:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bj;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->z:Lcom/google/android/youtube/app/remote/bj;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bk;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/youtube/app/remote/bb;)Z
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->D:Z

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/app/remote/bb;)D
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    return-wide v0
.end method

.method static synthetic r(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/core/utils/l;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->i:Lcom/google/android/youtube/core/utils/l;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/logic/c;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->h:Lcom/google/android/ytremote/logic/c;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->m:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/youtube/app/remote/bb;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->o:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/backend/a/a;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->k:Lcom/google/android/ytremote/backend/a/a;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/backend/logic/b;
    .registers 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->l:Lcom/google/android/ytremote/backend/logic/b;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .registers 4

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_17

    .line 514
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    if-nez v0, :cond_18

    .line 515
    const-string v0, "We should reconnect, but we lost the screen"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 527
    :cond_17
    :goto_17
    return-void

    .line 518
    :cond_18
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 520
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->p:Z

    if-nez v0, :cond_2a

    .line 521
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->j:Lcom/google/android/youtube/app/remote/bd;

    sget-object v2, Lcom/google/android/youtube/app/remote/bb;->b:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 524
    :cond_2a
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    new-instance v1, Lcom/google/android/ytremote/backend/model/b;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/b;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->t:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/ytremote/model/LoungeToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/backend/model/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/b;->a()Lcom/google/android/ytremote/backend/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    goto :goto_17
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 286
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 287
    const-string v1, "volume"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 288
    iput p1, p0, Lcom/google/android/youtube/app/remote/bb;->A:I

    .line 289
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SET_VOLUME:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/bb;->c(I)V

    .line 291
    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 391
    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 392
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 3
    .parameter

    .prologue
    .line 406
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne p1, v0, :cond_7

    .line 407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    .line 409
    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/remote/n;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 410
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/youtube/app/remote/ax;Ljava/lang/String;J)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    check-cast p1, Lcom/google/android/youtube/app/remote/bk;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bk;Ljava/lang/String;J)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/bj;)V
    .registers 2
    .parameter

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->z:Lcom/google/android/youtube/app/remote/bj;

    .line 402
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .registers 2
    .parameter

    .prologue
    .line 378
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 2
    .parameter

    .prologue
    .line 381
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 382
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 212
    const-string v0, "videoId can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_15

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bk;Ljava/lang/String;J)V

    .line 228
    :goto_14
    return-void

    .line 217
    :cond_15
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 219
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 220
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 221
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->ADD_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 223
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 224
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 225
    const-string v1, "currentTime"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 226
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_14
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 388
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)Z
    .registers 4
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->x:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .registers 4
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->x:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    return-object v0
.end method

.method protected final b()V
    .registers 5

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_17

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 532
    :cond_17
    return-void
.end method

.method public final b(I)V
    .registers 6
    .parameter

    .prologue
    .line 294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->D:Z

    .line 295
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 296
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 297
    const-string v1, "newTime"

    div-int/lit16 v2, p1, 0x3e8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 298
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SEEK_TO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 299
    int-to-double v0, p1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(D)V

    .line 300
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 303
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 304
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 305
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->ADD_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 306
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 207
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 209
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 309
    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 310
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 311
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->REMOVE_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 312
    return-void
.end method

.method public final d()V
    .registers 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->PLAY:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 232
    return-void
.end method

.method protected final d(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 477
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bb;->u:Ljava/lang/String;

    .line 478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    .line 479
    invoke-static {p1}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 480
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    .line 481
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    .line 482
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->y:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 486
    :cond_22
    return-void
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->PAUSE:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 236
    return-void
.end method

.method public final f()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 315
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 316
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->e(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->STOP:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 319
    return-void
.end method

.method public final g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .registers 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final i()I
    .registers 2

    .prologue
    .line 346
    iget v0, p0, Lcom/google/android/youtube/app/remote/bb;->A:I

    return v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 385
    return-void
.end method

.method public final j()D
    .registers 7

    .prologue
    .line 350
    iget-wide v2, p0, Lcom/google/android/youtube/app/remote/bb;->B:D

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->q:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_12

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/youtube/app/remote/bb;->C:J

    sub-long/2addr v0, v4

    :goto_f
    long-to-double v0, v0

    add-double/2addr v0, v2

    return-wide v0

    :cond_12
    const-wide/16 v0, 0x0

    goto :goto_f
.end method

.method public final k()Ljava/util/List;
    .registers 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    return-object v0
.end method

.method public final l()Z
    .registers 2

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bb;->n:Z

    return v0
.end method

.method public final m()Lcom/google/android/youtube/app/remote/ak;
    .registers 2

    .prologue
    .line 359
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()V
    .registers 1

    .prologue
    .line 363
    return-void
.end method

.method public final o()I
    .registers 2

    .prologue
    .line 370
    const/4 v0, 0x0

    return v0
.end method

.method public final p()Z
    .registers 2

    .prologue
    .line 374
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 262
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 266
    :cond_9
    :goto_9
    return v0

    .line 265
    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 266
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_9

    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final r()V
    .registers 4

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->q()Z

    move-result v0

    if-nez v0, :cond_7

    .line 282
    :goto_6
    return-void

    .line 275
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 276
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 278
    new-instance v1, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 279
    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 280
    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 281
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_6
.end method

.method public final s()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 243
    :cond_9
    :goto_9
    return v0

    .line 242
    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 243
    if-lez v1, :cond_9

    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final t()V
    .registers 4

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->s()Z

    move-result v0

    if-nez v0, :cond_7

    .line 258
    :goto_6
    return-void

    .line 251
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bb;->w:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bb;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 252
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 254
    new-instance v1, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    .line 255
    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 256
    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_6
.end method

.method public final bridge synthetic u()Lcom/google/android/youtube/app/remote/ax;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    return-object v0
.end method

.method public final y()Lcom/google/android/youtube/app/remote/bk;
    .registers 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->s:Lcom/google/android/youtube/app/remote/bk;

    return-object v0
.end method

.method public final z()V
    .registers 4

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_11

    .line 396
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bb;->g:Lcom/google/android/ytremote/backend/browserchannel/i;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->UPDATE_USERNAME:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    .line 398
    :cond_11
    return-void
.end method
