.class final Lcom/google/android/youtube/app/remote/be;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/ytremote/model/CloudScreen;

.field private final b:Ljava/lang/String;

.field private final c:J


# direct methods
.method private constructor <init>(Lcom/google/android/ytremote/model/CloudScreen;Ljava/lang/String;J)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1000
    const-string v0, "cloudScreen can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/be;->a:Lcom/google/android/ytremote/model/CloudScreen;

    .line 1001
    iput-object p2, p0, Lcom/google/android/youtube/app/remote/be;->b:Ljava/lang/String;

    .line 1002
    iput-wide p3, p0, Lcom/google/android/youtube/app/remote/be;->c:J

    .line 1003
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ytremote/model/CloudScreen;Ljava/lang/String;JB)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 994
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/remote/be;-><init>(Lcom/google/android/ytremote/model/CloudScreen;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/be;)Lcom/google/android/ytremote/model/CloudScreen;
    .registers 2
    .parameter

    .prologue
    .line 994
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/be;->a:Lcom/google/android/ytremote/model/CloudScreen;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/be;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 994
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/be;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/be;)J
    .registers 3
    .parameter

    .prologue
    .line 994
    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/be;->c:J

    return-wide v0
.end method
