.class final Lcom/google/android/youtube/app/remote/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/logic/d;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/ytremote/model/b;

.field final synthetic c:Lcom/google/android/youtube/app/remote/bf;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;Lcom/google/android/ytremote/model/b;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 933
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bg;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/bg;->b:Lcom/google/android/ytremote/model/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->c(Lcom/google/android/youtube/app/remote/bb;Z)Z

    .line 947
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;I)Lcom/google/android/youtube/app/remote/al;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/al;)V

    .line 948
    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/CloudScreen;)V
    .registers 11
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 935
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0, v5}, Lcom/google/android/youtube/app/remote/bb;->c(Lcom/google/android/youtube/app/remote/bb;Z)Z

    .line 937
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_22

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->f(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_48

    .line 939
    :cond_22
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bf;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x3

    new-instance v0, Lcom/google/android/youtube/app/remote/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bg;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;)J

    move-result-wide v3

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/remote/be;-><init>(Lcom/google/android/ytremote/model/CloudScreen;Ljava/lang/String;JB)V

    invoke-static {v7, v8, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 942
    :cond_48
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bg;->c:Lcom/google/android/youtube/app/remote/bf;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bg;->b:Lcom/google/android/ytremote/model/b;

    invoke-static {v0, v1, p1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/ytremote/model/b;Lcom/google/android/ytremote/model/CloudScreen;)V

    .line 943
    return-void
.end method
