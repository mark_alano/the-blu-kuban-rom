.class final Lcom/google/android/youtube/app/remote/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/backend/browserchannel/s;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/bb;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/bb;)V
    .registers 2
    .parameter

    .prologue
    .line 649
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/bb;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 649
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/bh;-><init>(Lcom/google/android/youtube/app/remote/bb;)V

    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 778
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    const-string v1, "devices"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v2, v0

    .line 779
    :goto_d
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
    :try_end_10
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_10} :catch_31

    move-result v1

    if-ge v2, v1, :cond_26

    .line 781
    :try_start_13
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 782
    const-string v4, "type"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 783
    const-string v4, "LOUNGE_SCREEN"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_22
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_22} :catch_27

    move-result v1

    if-eqz v1, :cond_2d

    .line 784
    const/4 v0, 0x1

    .line 793
    :cond_26
    :goto_26
    return v0

    .line 786
    :catch_27
    move-exception v1

    .line 787
    :try_start_28
    const-string v4, "Error parsing lounge status message"

    invoke-static {v4, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2d
    .catch Lorg/json/JSONException; {:try_start_28 .. :try_end_2d} :catch_31

    .line 779
    :cond_2d
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_d

    .line 790
    :catch_31
    move-exception v1

    .line 791
    const-string v2, "Error parsing lounge status message"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_26
.end method

.method private b(Lorg/json/JSONObject;)V
    .registers 6
    .parameter

    .prologue
    .line 797
    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 800
    :cond_10
    :try_start_10
    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 801
    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-double v0, v0

    .line 806
    :goto_21
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->p(Lcom/google/android/youtube/app/remote/bb;)Z

    move-result v2

    if-nez v2, :cond_39

    .line 807
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2, v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;D)V

    .line 817
    :cond_2e
    :goto_2e
    return-void

    .line 804
    :cond_2f
    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-double v0, v0

    goto :goto_21

    .line 808
    :cond_39
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->q(Lcom/google/android/youtube/app/remote/bb;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x409f400000000000L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    .line 810
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;Z)Z

    .line 811
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_5d
    .catch Lorg/json/JSONException; {:try_start_10 .. :try_end_5d} :catch_5e

    goto :goto_2e

    .line 813
    :catch_5e
    move-exception v0

    .line 814
    const-string v1, "Error parsing current time"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2e
.end method

.method private c(Lorg/json/JSONObject;)V
    .registers 4
    .parameter

    .prologue
    .line 820
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 822
    :try_start_8
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->valueOf(I)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    .line 824
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    .line 825
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    .line 826
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    :try_end_27
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_27} :catch_28

    .line 831
    :cond_27
    :goto_27
    return-void

    .line 827
    :catch_28
    move-exception v0

    .line 828
    const-string v1, "Error receiving state changed message"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_27
.end method

.method private d(Lorg/json/JSONObject;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 835
    :try_start_1
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_16

    .line 860
    :cond_15
    :goto_15
    return v2

    .line 839
    :cond_16
    new-instance v3, Lorg/json/JSONArray;

    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v1, v2

    move v0, v2

    .line 840
    :goto_23
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_6e

    .line 841
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 842
    const-string v5, "NOT_PLAYABLE"

    const-string v6, "error"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6b

    .line 843
    const-string v5, "videoId"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 844
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6b

    .line 845
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->g(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/bh;->e(Lorg/json/JSONObject;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    const/4 v0, 0x1

    .line 847
    iget-object v4, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6b

    .line 848
    iget-object v4, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/bb;->l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 840
    :cond_6b
    add-int/lit8 v1, v1, 0x1

    goto :goto_23

    .line 853
    :cond_6e
    if-eqz v0, :cond_15

    .line 854
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->v()V
    :try_end_75
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_75} :catch_76

    goto :goto_15

    .line 856
    :catch_76
    move-exception v0

    .line 857
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid \'errors\' value in request: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_15
.end method

.method private static e(Lorg/json/JSONObject;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .registers 4
    .parameter

    .prologue
    .line 865
    :try_start_0
    const-string v0, "reason"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 866
    invoke-static {}, Lcom/google/android/youtube/app/remote/bb;->C()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    :try_end_10
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_10} :catch_13

    .line 868
    if-eqz v0, :cond_2a

    .line 874
    :goto_12
    return-object v0

    .line 871
    :catch_13
    move-exception v0

    .line 872
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid \'reason\' value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 874
    :cond_2a
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    goto :goto_12
.end method


# virtual methods
.method public final a(Lorg/json/JSONArray;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 654
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_d

    .line 770
    :cond_c
    :goto_c
    return-void

    .line 658
    :cond_d
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eqz v0, :cond_c

    .line 665
    const/4 v0, 0x0

    :try_start_14
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 666
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-le v0, v2, :cond_42

    .line 667
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_22
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_22} :catch_47

    move-result-object v0

    .line 675
    :goto_23
    invoke-static {v1}, Lcom/google/android/ytremote/backend/model/Method;->fromString(Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Method;

    move-result-object v2

    .line 676
    if-nez v2, :cond_5b

    .line 677
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid method: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Ignoring."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_c

    .line 669
    :cond_42
    :try_start_42
    invoke-static {}, Lcom/google/android/youtube/app/remote/bb;->B()Lorg/json/JSONObject;
    :try_end_45
    .catch Lorg/json/JSONException; {:try_start_42 .. :try_end_45} :catch_47

    move-result-object v0

    goto :goto_23

    .line 671
    :catch_47
    move-exception v0

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid JSON array: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_c

    .line 681
    :cond_5b
    sget-object v1, Lcom/google/android/youtube/app/remote/bc;->b:[I

    invoke-virtual {v2}, Lcom/google/android/ytremote/backend/model/Method;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1f0

    goto :goto_c

    .line 683
    :pswitch_67
    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bh;->a(Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 684
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 685
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->h(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    goto :goto_c

    .line 687
    :cond_80
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->i(Lcom/google/android/youtube/app/remote/bb;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 688
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->j(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bj;

    move-result-object v0

    if-eqz v0, :cond_9f

    .line 689
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->j(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->k(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bk;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bj;->a(Lcom/google/android/youtube/app/remote/bk;)V

    .line 691
    :cond_9f
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    sget-object v1, Lcom/google/android/youtube/app/remote/am;->b:Lcom/google/android/youtube/app/remote/al;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/al;)V

    goto/16 :goto_c

    .line 696
    :pswitch_a8
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 697
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->h(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    goto/16 :goto_c

    .line 700
    :pswitch_bc
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->j(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bj;

    move-result-object v0

    if-eqz v0, :cond_d3

    .line 701
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->j(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->k(Lcom/google/android/youtube/app/remote/bb;)Lcom/google/android/youtube/app/remote/bk;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bj;->a(Lcom/google/android/youtube/app/remote/bk;)V

    .line 703
    :cond_d3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 704
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->l(Lcom/google/android/youtube/app/remote/bb;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_c

    .line 707
    :pswitch_e6
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13e

    .line 709
    :try_start_ee
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 710
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->m(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11d

    .line 711
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;

    .line 712
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;

    .line 713
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/bb;->m(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/remote/bb;->e(Ljava/lang/String;)V

    .line 714
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;D)V

    .line 716
    :cond_11d
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12f

    .line 717
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;
    :try_end_12f
    .catch Lorg/json/JSONException; {:try_start_ee .. :try_end_12f} :catch_137

    .line 722
    :cond_12f
    :goto_12f
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bh;->b(Lorg/json/JSONObject;)V

    .line 728
    :goto_132
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bh;->c(Lorg/json/JSONObject;)V

    goto/16 :goto_c

    .line 719
    :catch_137
    move-exception v1

    .line 720
    const-string v2, "Error receiving now playing message"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_12f

    .line 724
    :cond_13e
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/remote/bb;->d(Ljava/lang/String;)V

    .line 725
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/remote/bb;->e(Ljava/lang/String;)V

    goto :goto_132

    .line 731
    :pswitch_149
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_163

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->m(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 734
    :cond_163
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bh;->b(Lorg/json/JSONObject;)V

    .line 737
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bh;->c(Lorg/json/JSONObject;)V

    goto/16 :goto_c

    .line 740
    :pswitch_16b
    const-string v1, "videoIds"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a2

    .line 742
    :try_start_173
    const-string v1, "videoIds"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 743
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->o(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 744
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_197

    .line 745
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->o(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/List;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 747
    :cond_197
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bb;->o(Lcom/google/android/youtube/app/remote/bb;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/remote/bb;->a(Ljava/util/List;)V
    :try_end_1a2
    .catch Lorg/json/JSONException; {:try_start_173 .. :try_end_1a2} :catch_1e4

    .line 753
    :cond_1a2
    :goto_1a2
    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 755
    :try_start_1aa
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    const-string v2, "videoId"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/bb;->b(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;

    .line 756
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;)Ljava/lang/String;

    .line 757
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1d5

    .line 758
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bb;->n(Lcom/google/android/youtube/app/remote/bb;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bb;->e(Ljava/lang/String;)V

    .line 761
    :cond_1d5
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bh;->a:Lcom/google/android/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bb;->c(Lcom/google/android/youtube/app/remote/bb;)V
    :try_end_1da
    .catch Lorg/json/JSONException; {:try_start_1aa .. :try_end_1da} :catch_1dc

    goto/16 :goto_c

    .line 762
    :catch_1dc
    move-exception v0

    .line 763
    const-string v1, "Error receiving playlist modified message"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_c

    .line 748
    :catch_1e4
    move-exception v1

    .line 749
    const-string v2, "Error receiving playlist modified message"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1a2

    .line 769
    :pswitch_1eb
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bh;->d(Lorg/json/JSONObject;)Z

    goto/16 :goto_c

    .line 681
    :pswitch_data_1f0
    .packed-switch 0x1
        :pswitch_67
        :pswitch_a8
        :pswitch_bc
        :pswitch_e6
        :pswitch_149
        :pswitch_16b
        :pswitch_1eb
    .end packed-switch
.end method
