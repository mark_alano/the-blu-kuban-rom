.class public final Lcom/google/android/youtube/app/remote/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/ay;


# instance fields
.field private final a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final e:Lcom/google/android/youtube/core/async/l;

.field private final f:Lcom/google/android/youtube/app/remote/bl;

.field private final g:Lcom/google/android/youtube/app/remote/bb;

.field private final h:Ljava/util/Map;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/youtube/core/utils/l;

.field private final k:Z

.field private final l:Lcom/google/android/ytremote/logic/a;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/core/utils/l;ZLcom/google/android/youtube/app/remote/bb;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, "screensClient can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bl;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->f:Lcom/google/android/youtube/app/remote/bl;

    .line 66
    const-string v0, "networkStatus can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->j:Lcom/google/android/youtube/core/utils/l;

    .line 67
    const-string v0, "youTubeTvRemoteControl can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bb;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->g:Lcom/google/android/youtube/app/remote/bb;

    .line 69
    iput-boolean p3, p0, Lcom/google/android/youtube/app/remote/bm;->k:Z

    .line 71
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 72
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 73
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 74
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 76
    new-instance v0, Lcom/google/android/youtube/app/remote/bv;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/bv;-><init>(Lcom/google/android/youtube/app/remote/bm;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->e:Lcom/google/android/youtube/core/async/l;

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->h:Ljava/util/Map;

    .line 78
    new-instance v0, Lcom/google/android/ytremote/b/k;

    invoke-direct {v0}, Lcom/google/android/ytremote/b/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->l:Lcom/google/android/ytremote/logic/a;

    .line 80
    new-instance v0, Lcom/google/android/youtube/app/remote/bu;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/bu;-><init>(Lcom/google/android/youtube/app/remote/bm;B)V

    invoke-virtual {p4, v0}, Lcom/google/android/youtube/app/remote/bb;->a(Lcom/google/android/youtube/app/remote/bj;)V

    .line 82
    new-instance v0, Lcom/google/android/youtube/app/remote/bn;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/google/android/youtube/app/remote/bn;-><init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/app/remote/bb;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    .line 136
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/bq;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/bq;-><init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bm;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bm;->m:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bm;)Lcom/google/android/youtube/core/async/l;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->e:Lcom/google/android/youtube/core/async/l;

    return-object v0
.end method

.method private b()V
    .registers 6

    .prologue
    const/4 v3, 0x2

    .line 252
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->j:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->c()Z

    move-result v0

    if-nez v0, :cond_f

    .line 266
    :goto_e
    return-void

    .line 258
    :cond_f
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/common/collect/Sets;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 260
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 262
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 265
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->l:Lcom/google/android/ytremote/logic/a;

    new-instance v2, Lcom/google/android/youtube/app/remote/bs;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/remote/bs;-><init>(Lcom/google/android/youtube/app/remote/bm;Ljava/util/Set;)V

    invoke-interface {v1, v2}, Lcom/google/android/ytremote/logic/a;->a(Lcom/google/android/ytremote/logic/b;)V

    goto :goto_e
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/bp;-><init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/bm;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/bm;)V
    .registers 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/bm;->b()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->h:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/az;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->f:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->e:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/youtube/core/async/l;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bm;->k:Z

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/bm;->b()V

    :cond_14
    iput-boolean v4, p0, Lcom/google/android/youtube/app/remote/bm;->m:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bm;->k:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 141
    :cond_29
    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/bt;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bm;->k:Z

    if-nez v0, :cond_8

    .line 149
    invoke-interface {p2}, Lcom/google/android/youtube/app/remote/bt;->a()V

    .line 181
    :goto_7
    return-void

    .line 152
    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bk;

    .line 153
    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bk;->b()Lcom/google/android/ytremote/model/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/b;->d()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 154
    invoke-interface {p2, v0}, Lcom/google/android/youtube/app/remote/bt;->a(Lcom/google/android/youtube/app/remote/bk;)V

    goto :goto_7

    .line 159
    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->h:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->l:Lcom/google/android/ytremote/logic/a;

    new-instance v1, Lcom/google/android/youtube/app/remote/bo;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/app/remote/bo;-><init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/bt;)V

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/logic/a;->a(Lcom/google/android/ytremote/logic/b;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_7
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->f:Lcom/google/android/youtube/app/remote/bl;

    new-instance v1, Lcom/google/android/youtube/app/remote/br;

    invoke-direct {v1, p0, p3}, Lcom/google/android/youtube/app/remote/br;-><init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/youtube/app/remote/bl;->a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/l;)V

    .line 230
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/remote/az;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 186
    iput-boolean v1, p0, Lcom/google/android/youtube/app/remote/bm;->m:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 188
    :cond_1b
    return-void
.end method
