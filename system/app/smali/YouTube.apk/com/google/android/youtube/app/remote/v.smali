.class public final Lcom/google/android/youtube/app/remote/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/bl;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/av;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/Map;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/content/res/Resources;

.field private final f:Ljava/util/concurrent/Executor;

.field private g:Z

.field private final h:Lcom/google/android/ytremote/backend/a/a;

.field private final i:Lcom/google/android/ytremote/backend/a/d;

.field private final j:Lcom/google/android/youtube/app/remote/bb;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Lcom/google/android/youtube/app/remote/bb;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const-string v0, "executor can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    .line 59
    const-string v0, "preferences can not be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    .line 60
    const-string v0, "resources can not be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->e:Landroid/content/res/Resources;

    .line 61
    const-string v0, "youTubeTvRemoteControl can not be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bb;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->j:Lcom/google/android/youtube/app/remote/bb;

    .line 64
    new-instance v0, Lcom/google/android/ytremote/backend/a/f;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/f;-><init>()V

    .line 65
    new-instance v1, Lcom/google/android/youtube/app/remote/w;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/remote/w;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/backend/a/f;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/async/d;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/av;)Lcom/google/android/youtube/core/async/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->a:Lcom/google/android/youtube/core/async/av;

    .line 76
    new-instance v0, Lcom/google/android/ytremote/backend/a/a;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->h:Lcom/google/android/ytremote/backend/a/a;

    .line 77
    new-instance v0, Lcom/google/android/ytremote/backend/a/d;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->i:Lcom/google/android/ytremote/backend/a/d;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->c:Ljava/util/Map;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/v;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;
    .registers 5
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    .line 204
    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 208
    :goto_1c
    return-object v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private a()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v1, 0x1

    .line 212
    monitor-enter p0

    .line 213
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    if-nez v0, :cond_1e

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    const-string v2, "screenIds"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/v;->a(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 219
    :cond_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_2 .. :try_end_1f} :catchall_39

    move v0, v1

    .line 221
    :goto_20
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/v;->e:Landroid/content/res/Resources;

    const v3, 0x7f0b0235

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 222
    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/remote/v;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v3

    .line 223
    if-nez v3, :cond_3c

    .line 224
    return-object v2

    .line 219
    :catchall_39
    move-exception v0

    monitor-exit p0

    throw v0

    .line 220
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_20
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/v;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/SharedPreferences;)Ljava/util/List;
    .registers 9
    .parameter

    .prologue
    .line 230
    const-string v0, "screenIds"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 231
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 245
    :goto_c
    return-object v0

    .line 234
    :cond_d
    const-string v0, "screenIds"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 235
    const-string v0, "screenNames"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 237
    const/4 v0, 0x0

    :goto_2f
    array-length v4, v2

    if-ge v0, v4, :cond_4d

    .line 238
    aget-object v4, v2, v0

    .line 239
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4a

    .line 240
    new-instance v5, Lcom/google/android/ytremote/model/CloudScreen;

    new-instance v6, Lcom/google/android/ytremote/model/ScreenId;

    invoke-direct {v6, v4}, Lcom/google/android/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    aget-object v4, v3, v0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v4, v7}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2f

    :cond_4d
    move-object v0, v1

    .line 245
    goto :goto_c
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Landroid/content/SharedPreferences;)Ljava/util/List;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-static {p1}, Lcom/google/android/youtube/app/remote/v;->a(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Ljava/util/List;)Ljava/util/List;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    new-instance v3, Lcom/google/android/youtube/app/remote/bk;

    invoke-direct {v3, v0}, Lcom/google/android/youtube/app/remote/bk;-><init>(Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_1e
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/aa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/aa;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/v;)Z
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/v;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->j:Lcom/google/android/youtube/app/remote/bb;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/a;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->h:Lcom/google/android/ytremote/backend/a/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/d;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->i:Lcom/google/android/ytremote/backend/a/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/l;)V
    .registers 4
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/y;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 177
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->a:Lcom/google/android/youtube/core/async/av;

    new-instance v1, Lcom/google/android/youtube/app/remote/x;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/youtube/app/remote/x;-><init>(Lcom/google/android/youtube/app/remote/v;Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 104
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/async/l;)V
    .registers 4
    .parameter

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/z;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 192
    return-void
.end method
