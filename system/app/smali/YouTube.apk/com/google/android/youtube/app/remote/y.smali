.class final Lcom/google/android/youtube/app/remote/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/l;

.field final synthetic b:Lcom/google/android/youtube/app/remote/v;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/youtube/core/async/l;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/y;->a:Lcom/google/android/youtube/core/async/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 110
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    monitor-enter v1

    .line 111
    :try_start_6
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->b(Lcom/google/android/youtube/app/remote/v;)Z

    move-result v0

    if-nez v0, :cond_29

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/youtube/app/remote/v;Z)Z

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/v;->c(Lcom/google/android/youtube/app/remote/v;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/youtube/app/remote/v;Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 115
    :cond_29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_40

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->a:Lcom/google/android/youtube/core/async/l;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v5, v1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174
    :goto_3f
    return-void

    .line 115
    :catchall_40
    move-exception v0

    monitor-exit v1

    throw v0

    .line 125
    :cond_43
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_a2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_a2

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bk;->e()Z

    move-result v0

    if-eqz v0, :cond_a2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bk;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a2

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->a:Lcom/google/android/youtube/core/async/l;

    new-array v1, v4, [Lcom/google/android/youtube/app/remote/bk;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/common/collect/Lists;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v5, v1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 134
    :cond_a2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b1
    :goto_b1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_dd

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    .line 136
    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getAccessType()Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    move-result-object v3

    sget-object v4, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    if-ne v3, v4, :cond_b1

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/v;->f(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b1

    .line 138
    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b1

    .line 142
    :cond_dd
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->f(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/v;->g(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/ytremote/backend/a/a;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 144
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->d(Lcom/google/android/youtube/app/remote/v;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_ff
    :goto_ff
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_140

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/ytremote/model/CloudScreen;

    .line 146
    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen;->getAccessType()Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    move-result-object v0

    sget-object v4, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    if-ne v0, v4, :cond_13c

    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->f(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->f(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/LoungeToken;

    invoke-virtual {v1, v0}, Lcom/google/android/ytremote/model/CloudScreen;->withLoungeToken(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_ff

    .line 152
    :cond_13c
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_ff

    .line 156
    :cond_140
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->h(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/ytremote/backend/a/d;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    .line 159
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 160
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_153
    :goto_153
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    .line 161
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_153

    .line 162
    new-instance v4, Lcom/google/android/youtube/app/remote/bk;

    invoke-direct {v4, v0}, Lcom/google/android/youtube/app/remote/bk;-><init>(Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_153

    .line 166
    :cond_16e
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1a9

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bk;->e()Z

    move-result v0

    if-eqz v0, :cond_1a9

    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a9

    .line 169
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->e(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_1a9
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/y;->a:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v5, v3}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_3f
.end method
