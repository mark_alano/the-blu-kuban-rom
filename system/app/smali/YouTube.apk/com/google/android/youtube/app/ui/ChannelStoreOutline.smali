.class public final Lcom/google/android/youtube/app/ui/ChannelStoreOutline;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/youtube/core/a/g;


# instance fields
.field private final c:I

.field private final d:[Lcom/google/android/youtube/core/a/e;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "ChannelStoreList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a:Lcom/google/android/youtube/core/a/g;

    return-void
.end method

.method private constructor <init>([Lcom/google/android/youtube/core/a/e;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 91
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    .line 92
    iput p2, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    .line 93
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/cx;Landroid/view/View;)Lcom/google/android/youtube/app/ui/ChannelStoreOutline;
    .registers 32
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-static {}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->values()[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    move-result-object v13

    .line 66
    array-length v14, v13

    .line 67
    mul-int/lit8 v2, v14, 0x2

    new-array v15, v2, [Lcom/google/android/youtube/core/a/l;

    .line 69
    const/4 v2, 0x0

    move v12, v2

    :goto_b
    if-ge v12, v14, :cond_ac

    .line 70
    mul-int/lit8 v2, v12, 0x2

    aget-object v3, v13, v12

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v3, v1}, Lcom/google/android/youtube/app/ui/i;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Lcom/google/android/youtube/app/a;)Lcom/google/android/youtube/app/ui/i;

    move-result-object v3

    aput-object v3, v15, v2

    .line 72
    mul-int/lit8 v2, v12, 0x2

    add-int/lit8 v16, v2, 0x1

    sget-object v17, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a:Lcom/google/android/youtube/core/a/g;

    aget-object v18, v13, v12

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v2, 0x7f0d0009

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v20

    const v2, 0x7f0a005a

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p0

    invoke-static/range {v2 .. v11}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/cx;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/a/c;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/a/g;

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/cq;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    mul-int/lit8 v7, v21, 0x2

    new-instance v8, Lcom/google/android/youtube/app/ui/l;

    invoke-direct {v8}, Lcom/google/android/youtube/app/ui/l;-><init>()V

    const/4 v9, 0x0

    move-object/from16 v5, v17

    move/from16 v6, v20

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/app/adapter/cq;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v21

    invoke-virtual {v2, v4, v0, v5, v6}, Lcom/google/android/youtube/app/adapter/cq;->a(IIII)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f04001e

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    new-instance v9, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v9, v4}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    new-instance v8, Lcom/google/android/youtube/core/a/l;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/youtube/core/a/e;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v9, v4, v2

    invoke-direct {v8, v4}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v4, Lcom/google/android/youtube/app/ui/k;

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object v7, v3

    move-object/from16 v10, v19

    move-object/from16 v11, v18

    invoke-direct/range {v4 .. v11}, Lcom/google/android/youtube/app/ui/k;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/a/c;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/cy;Landroid/content/res/Resources;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V

    aput-object v4, v15, v16

    .line 69
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto/16 :goto_b

    .line 86
    :cond_ac
    new-instance v2, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-direct {v2, v15, v14}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;-><init>([Lcom/google/android/youtube/core/a/e;I)V

    return-object v2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 102
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    if-ge v1, v0, :cond_17

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/youtube/app/ui/k;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/k;->a(Landroid/net/Uri;)V

    .line 102
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 105
    :cond_17
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    if-ge v1, v0, :cond_17

    .line 121
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/youtube/app/ui/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/k;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 123
    :cond_17
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 5
    .parameter

    .prologue
    .line 96
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    if-ge v1, v0, :cond_17

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/youtube/app/ui/k;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/k;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 96
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 99
    :cond_17
    return-void
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 108
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    if-ge v1, v0, :cond_17

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/youtube/app/ui/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/k;->b()V

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 111
    :cond_17
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 114
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:I

    if-ge v1, v0, :cond_17

    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->d:[Lcom/google/android/youtube/core/a/e;

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/youtube/app/ui/k;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/k;->b(Landroid/net/Uri;)V

    .line 114
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 117
    :cond_17
    return-void
.end method
