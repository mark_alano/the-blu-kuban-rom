.class public Lcom/google/android/youtube/app/ui/DefaultSlider;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/Slider;


# instance fields
.field protected final a:Landroid/app/Activity;

.field private b:I

.field private final c:Landroid/util/DisplayMetrics;

.field private d:Z

.field private e:I

.field private f:I

.field private g:[Lcom/google/android/youtube/app/ui/ax;

.field private h:[Landroid/view/View;

.field private i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

.field private j:[I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:I

.field private o:Landroid/widget/Scroller;

.field private p:Z

.field private q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 88
    const-string v0, "activity must not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 65
    iput-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    .line 89
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->a:Landroid/app/Activity;

    .line 90
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    .line 91
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 93
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    .line 94
    new-instance v0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/DefaultSlider;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    .line 97
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->HORIZONTAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->setOrientation(Lcom/google/android/youtube/app/ui/Slider$Orientation;)V

    .line 98
    new-array v0, v4, [Lcom/google/android/youtube/app/ui/ax;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    .line 99
    iput v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 100
    new-array v0, v4, [Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v1, v0, v3

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v1, v0, v2

    .line 103
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    .line 104
    new-array v0, v4, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v2

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    .line 110
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->setChildrenDrawingOrderEnabled(Z)V

    .line 111
    return-void
.end method

.method private b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 610
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    if-eq v0, p1, :cond_9

    .line 611
    if-eqz p1, :cond_9

    .line 612
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a()V

    .line 614
    :cond_9
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    .line 618
    return-void
.end method

.method private c(I)V
    .registers 3
    .parameter

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_d

    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 273
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    .line 274
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-eq v0, p1, :cond_1e

    .line 275
    iput p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 276
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->d(I)V

    .line 278
    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->invalidate()V

    .line 279
    return-void
.end method

.method private d(I)V
    .registers 4
    .parameter

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v0, v0, p1

    if-eqz v0, :cond_d

    .line 305
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ax;->j()V

    .line 307
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    rsub-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_1e

    .line 308
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    rsub-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ax;->k()V

    .line 310
    :cond_1e
    return-void
.end method

.method private e(I)V
    .registers 2
    .parameter

    .prologue
    .line 655
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    .line 656
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    .line 657
    return-void
.end method

.method private f(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 688
    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 689
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    .line 690
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-eqz v1, :cond_11

    .line 715
    :goto_10
    return-void

    .line 694
    :cond_11
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 696
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    .line 697
    if-eqz v3, :cond_2c

    if-eqz v1, :cond_2c

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    iget v5, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    aget-object v4, v4, v5

    if-ne v3, v4, :cond_2c

    .line 698
    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    .line 702
    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v3

    mul-int/2addr v0, v3

    .line 703
    iget v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int v3, v0, v3

    .line 704
    mul-int/lit16 v5, v1, 0xc8

    .line 705
    if-nez v5, :cond_3d

    .line 706
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 710
    :cond_3d
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_4a

    .line 711
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 713
    :cond_4a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 714
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->invalidate()V

    goto :goto_10
.end method

.method private h()V
    .registers 3

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    .line 664
    const/4 v0, 0x0

    :goto_4
    const/4 v1, 0x2

    if-ge v0, v1, :cond_17

    .line 665
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    .line 666
    if-eqz v1, :cond_11

    .line 667
    if-nez v0, :cond_14

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    .line 664
    :cond_11
    :goto_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 667
    :cond_14
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    goto :goto_11

    .line 670
    :cond_17
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 768
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v3

    if-ne v2, v3, :cond_15

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v2, v2, v0

    if-nez v2, :cond_15

    const/16 v0, 0x8

    :cond_15
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 770
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/Slider$Order;)Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;
    .registers 4
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected a()V
    .registers 1

    .prologue
    .line 626
    return-void
.end method

.method protected a(I)V
    .registers 3
    .parameter

    .prologue
    .line 641
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    .line 642
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    .line 643
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 7
    .parameter

    .prologue
    .line 736
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_3b

    .line 737
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    .line 738
    if-eqz v1, :cond_38

    .line 739
    invoke-interface {v1, p1}, Lcom/google/android/youtube/app/ui/ax;->a(Landroid/content/res/Configuration;)V

    .line 740
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->e()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v2, v0

    .line 743
    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->o()Landroid/view/View;

    move-result-object v1

    .line 744
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eq v1, v2, :cond_38

    .line 745
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->removeView(Landroid/view/View;)V

    .line 746
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aput-object v1, v2, v0

    .line 747
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    .line 736
    :cond_38
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 751
    :cond_3b
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 6
    .parameter

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 240
    if-eqz v3, :cond_d

    .line 241
    invoke-interface {v3, p1}, Lcom/google/android/youtube/app/ui/ax;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 239
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 244
    :cond_10
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 197
    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    if-eqz p2, :cond_f

    .line 199
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    .line 203
    :goto_e
    return-void

    .line 201
    :cond_f
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    goto :goto_e
.end method

.method protected a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 652
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 473
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 474
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    if-ne p2, v0, :cond_1b

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v0, v2, :cond_1b

    .line 475
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 479
    :cond_1a
    :goto_1a
    return-void

    .line 476
    :cond_1b
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    if-ne p2, v0, :cond_1a

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-nez v0, :cond_1a

    .line 477
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_1a
.end method

.method public addView(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 283
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 284
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 285
    return-void
.end method

.method protected final b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b()Lcom/google/android/youtube/app/ui/Slider$Orientation;
    .registers 2

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    :goto_6
    return-object v0

    :cond_7
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->HORIZONTAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    goto :goto_6
.end method

.method protected final b(I)V
    .registers 2
    .parameter

    .prologue
    .line 758
    iput p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    .line 759
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->i()V

    .line 760
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->h()V

    .line 761
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)V
    .registers 3
    .parameter

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    .line 248
    return-void
.end method

.method public final c()V
    .registers 5

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-eqz v0, :cond_5

    .line 224
    :goto_4
    return-void

    .line 218
    :cond_5
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_9
    if-ge v0, v2, :cond_15

    aget-object v3, v1, v0

    .line 219
    if-eqz v3, :cond_12

    .line 220
    invoke-interface {v3}, Lcom/google/android/youtube/app/ui/ax;->g()V

    .line 218
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 223
    :cond_15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    goto :goto_4
.end method

.method public computeScroll()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 314
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 315
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    .line 316
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->postInvalidate()V

    .line 325
    :cond_15
    :goto_15
    return-void

    .line 317
    :cond_16
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    if-eq v0, v3, :cond_15

    .line 318
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 319
    iput v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    .line 320
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-eq v0, v1, :cond_15

    .line 321
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 322
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->d(I)V

    goto :goto_15
.end method

.method public final d()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v0, :cond_6

    .line 236
    :goto_5
    return-void

    .line 230
    :cond_6
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    array-length v3, v2

    move v0, v1

    :goto_a
    if-ge v0, v3, :cond_16

    aget-object v4, v2, v0

    .line 231
    if-eqz v4, :cond_13

    .line 232
    invoke-interface {v4}, Lcom/google/android/youtube/app/ui/ax;->h()V

    .line 230
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 235
    :cond_16
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    goto :goto_5
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 460
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    if-ne p2, v1, :cond_e

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v1, v0, :cond_e

    .line 461
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    .line 468
    :goto_d
    return v0

    .line 464
    :cond_e
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    if-ne p2, v1, :cond_1a

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-nez v1, :cond_1a

    .line 465
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_d

    .line 468
    :cond_1a
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_d
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 292
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    return v0
.end method

.method protected final f()I
    .registers 4

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getHeight()I

    move-result v0

    .line 300
    :goto_8
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    return v0

    .line 299
    :cond_15
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getWidth()I

    move-result v0

    goto :goto_8
.end method

.method protected final g()I
    .registers 2

    .prologue
    .line 764
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 427
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v2, :cond_a

    .line 428
    const/4 v0, 0x0

    .line 430
    :cond_a
    if-nez p2, :cond_e

    rsub-int/lit8 v0, v0, 0x1

    :cond_e
    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 489
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_40

    .line 531
    :cond_b
    :goto_b
    :pswitch_b
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    :cond_d
    return v0

    .line 495
    :pswitch_e
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    if-nez v1, :cond_d

    .line 501
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 502
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    goto :goto_b

    .line 508
    :pswitch_1e
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b(Landroid/view/MotionEvent;)V

    .line 514
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_2f

    :goto_2b
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    goto :goto_b

    :cond_2f
    move v0, v1

    goto :goto_2b

    .line 521
    :pswitch_31
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    .line 522
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_b

    .line 526
    :pswitch_3a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f(Landroid/view/MotionEvent;)V

    goto :goto_b

    .line 489
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_31
        :pswitch_e
        :pswitch_31
        :pswitch_b
        :pswitch_b
        :pswitch_3a
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    .line 380
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v1, 0x1

    aget v3, v0, v1

    .line 381
    const/4 v0, 0x2

    new-array v4, v0, [I

    .line 382
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 383
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 384
    const/4 v0, 0x0

    :goto_14
    const/4 v1, 0x2

    if-ge v0, v1, :cond_2e

    .line 385
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    aput v1, v4, v0

    .line 386
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    aput v1, v5, v0

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 388
    :cond_2e
    sub-int v1, p4, p2

    .line 389
    sub-int v0, p5, p3

    .line 390
    iget-boolean v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v7, :cond_6d

    .line 393
    :goto_36
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    const/4 v7, 0x0

    aget-object v1, v1, v7

    sget-object v7, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v7, :cond_6f

    .line 394
    const/4 v1, 0x0

    iget v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    neg-int v7, v7

    aput v7, v6, v1

    .line 398
    :goto_45
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    const/4 v7, 0x1

    aget-object v1, v1, v7

    sget-object v7, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v7, :cond_74

    .line 399
    const/4 v0, 0x1

    aput v2, v6, v0

    .line 405
    :goto_51
    const/4 v0, 0x0

    :goto_52
    const/4 v1, 0x2

    if-ge v0, v1, :cond_8e

    .line 406
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v1, :cond_7c

    .line 407
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    aget v3, v6, v0

    aget v7, v4, v0

    aget v8, v6, v0

    aget v9, v5, v0

    add-int/2addr v8, v9

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 405
    :goto_6a
    add-int/lit8 v0, v0, 0x1

    goto :goto_52

    :cond_6d
    move v0, v1

    .line 390
    goto :goto_36

    .line 396
    :cond_6f
    const/4 v1, 0x0

    const/4 v7, 0x0

    aput v7, v6, v1

    goto :goto_45

    .line 401
    :cond_74
    const/4 v1, 0x1

    sub-int/2addr v0, v3

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v0, v2

    aput v0, v6, v1

    goto :goto_51

    .line 409
    :cond_7c
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    aget v2, v6, v0

    const/4 v3, 0x0

    aget v7, v6, v0

    aget v8, v4, v0

    add-int/2addr v7, v8

    aget v8, v5, v0

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    goto :goto_6a

    .line 413
    :cond_8e
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-eqz v0, :cond_9f

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    .line 415
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a0

    .line 417
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    .line 422
    :cond_9f
    :goto_9f
    return-void

    .line 419
    :cond_a0
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    goto :goto_9f
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v3, 0x0

    const/high16 v9, 0x4000

    .line 335
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 337
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 338
    if-eq v0, v9, :cond_16

    .line 339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Slider can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_16
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 342
    if-eq v0, v9, :cond_24

    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Slider can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v4, v0, v3

    .line 347
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v5, v0, v10

    .line 348
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 349
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 350
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_72

    move v0, v1

    .line 351
    :goto_39
    new-array v6, v11, [I

    .line 354
    iget-object v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v7, v7, v3

    sget-object v8, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v7, v8, :cond_74

    .line 355
    sub-int v7, v0, v5

    iget v8, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v7, v8

    aput v7, v6, v3

    .line 359
    :goto_4a
    iget-object v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v7, v7, v10

    sget-object v8, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v7, v8, :cond_79

    .line 360
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    add-int/2addr v0, v5

    aput v0, v6, v10

    :goto_57
    move v0, v3

    .line 366
    :goto_58
    if-ge v0, v11, :cond_8f

    .line 367
    iget-boolean v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v3, :cond_7d

    .line 368
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    aget v5, v6, v0

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 366
    :goto_6f
    add-int/lit8 v0, v0, 0x1

    goto :goto_58

    :cond_72
    move v0, v2

    .line 350
    goto :goto_39

    .line 357
    :cond_74
    sub-int v7, v0, v5

    aput v7, v6, v3

    goto :goto_4a

    .line 362
    :cond_79
    sub-int/2addr v0, v4

    aput v0, v6, v10

    goto :goto_57

    .line 371
    :cond_7d
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    aget v4, v6, v0

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    goto :goto_6f

    .line 375
    :cond_8f
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 446
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_12

    .line 447
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:I

    .line 451
    :goto_7
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    .line 452
    if-eqz v0, :cond_15

    .line 453
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 455
    :goto_11
    return v0

    .line 449
    :cond_12
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    goto :goto_7

    .line 455
    :cond_15
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter

    .prologue
    .line 726
    check-cast p1, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;

    .line 727
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 728
    iget v0, p1, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 729
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ltz v0, :cond_16

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_19

    .line 730
    :cond_16
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 732
    :cond_19
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    .line 733
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 719
    new-instance v0, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 720
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    iput v1, v0, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    .line 721
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 330
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    .line 331
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 545
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a(Landroid/view/MotionEvent;)V

    .line 547
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_98

    .line 606
    :cond_10
    :goto_10
    :pswitch_10
    return v4

    .line 553
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 554
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 556
    :cond_1e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b(Landroid/view/MotionEvent;)V

    goto :goto_10

    .line 560
    :pswitch_24
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    if-eqz v0, :cond_10

    .line 562
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c(Landroid/view/MotionEvent;)I

    move-result v0

    .line 564
    if-gez v0, :cond_3f

    .line 565
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    if-lez v1, :cond_10

    .line 566
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    neg-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a(I)V

    goto :goto_10

    .line 568
    :cond_3f
    if-lez v0, :cond_10

    .line 569
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v1, v2

    .line 570
    if-lez v1, :cond_10

    .line 571
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a(I)V

    goto :goto_10

    .line 578
    :pswitch_52
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:Z

    if-eqz v0, :cond_67

    .line 579
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->e(Landroid/view/MotionEvent;)Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;

    move-result-object v0

    .line 580
    sget-object v1, Lcom/google/android/youtube/app/ui/an;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_aa

    .line 592
    :cond_67
    :goto_67
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    .line 593
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_10

    .line 582
    :pswitch_70
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_67

    .line 585
    :pswitch_74
    invoke-direct {p0, v4}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_67

    .line 588
    :pswitch_78
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    div-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_67

    .line 597
    :pswitch_87
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    .line 598
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_10

    .line 602
    :pswitch_90
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f(Landroid/view/MotionEvent;)V

    goto/16 :goto_10

    .line 547
    nop

    :pswitch_data_98
    .packed-switch 0x0
        :pswitch_11
        :pswitch_52
        :pswitch_24
        :pswitch_87
        :pswitch_10
        :pswitch_10
        :pswitch_90
    .end packed-switch

    .line 580
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_70
        :pswitch_74
        :pswitch_78
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 536
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 537
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 538
    if-ltz v0, :cond_12

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_12

    .line 539
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    .line 541
    :cond_12
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 435
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 436
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v0, v1, :cond_10

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_15

    .line 437
    :cond_10
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    .line 438
    const/4 v0, 0x1

    .line 440
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 129
    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v0, "strategy may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    .line 132
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object p2, v1, v0

    .line 133
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    rsub-int/lit8 v2, v0, 0x1

    aget-object v1, v1, v2

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v2, :cond_2d

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne p2, v1, :cond_2d

    .line 135
    const-string v1, "Both collapse strategies cannot be OCCLUDE"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    rsub-int/lit8 v2, v0, 0x1

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v3, v1, v2

    .line 138
    :cond_2d
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v0, v1, v0

    if-eqz v0, :cond_35

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    .line 141
    :cond_35
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->h()V

    .line 142
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-nez v0, :cond_3f

    .line 143
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    .line 145
    :cond_3f
    return-void
.end method

.method public setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/ax;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 152
    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getChildCount()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_62

    const/4 v0, 0x1

    :goto_e
    const-string v2, "there must be two children"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 154
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    .line 157
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2f

    .line 158
    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v2, :cond_28

    .line 159
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/youtube/app/ui/ax;->g()V

    .line 161
    :cond_28
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/youtube/app/ui/ax;->i()V

    .line 164
    :cond_2f
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aput-object p2, v2, v0

    .line 170
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    rsub-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 171
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->removeView(Landroid/view/View;)V

    .line 172
    if-nez p2, :cond_64

    .line 174
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aput v1, v2, v0

    .line 175
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    .line 191
    :goto_54
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    .line 192
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->i()V

    .line 193
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->h()V

    .line 194
    return-void

    :cond_62
    move v0, v1

    .line 153
    goto :goto_e

    .line 178
    :cond_64
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->f()Z

    move-result v1

    if-nez v1, :cond_75

    .line 179
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->b()V

    .line 181
    :cond_75
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->c()V

    .line 182
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v1, :cond_87

    .line 183
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/ax;->h()V

    .line 187
    :cond_87
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    invoke-interface {p2}, Lcom/google/android/youtube/app/ui/ax;->e()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v1, v0

    .line 188
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    invoke-interface {p2}, Lcom/google/android/youtube/app/ui/ax;->o()Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 189
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/ax;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    goto :goto_54
.end method

.method public setOrientation(Lcom/google/android/youtube/app/ui/Slider$Orientation;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 114
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    if-ne p1, v0, :cond_25

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    .line 115
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_27

    const/16 v0, 0x21

    :goto_e
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    .line 116
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_2a

    const/16 v0, 0x82

    :goto_16
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    .line 117
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-nez v0, :cond_24

    .line 118
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    .line 119
    iput v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    .line 120
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    .line 122
    :cond_24
    return-void

    :cond_25
    move v0, v1

    .line 114
    goto :goto_6

    .line 115
    :cond_27
    const/16 v0, 0x11

    goto :goto_e

    .line 116
    :cond_2a
    const/16 v0, 0x42

    goto :goto_16
.end method
