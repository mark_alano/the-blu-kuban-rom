.class public Lcom/google/android/youtube/app/ui/FadingListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    .line 35
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 38
    const v0, 0x555555

    iput v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    .line 39
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    .line 41
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/FadingListView;->setFadingEdgeLength(I)V

    .line 42
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/FadingListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 43
    return-void
.end method


# virtual methods
.method protected getBottomFadingEdgeStrength()F
    .registers 2

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    if-eqz v0, :cond_9

    invoke-super {p0}, Landroid/widget/ListView;->getBottomFadingEdgeStrength()F

    move-result v0

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public getSolidColor()I
    .registers 2

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .registers 2

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    if-eqz v0, :cond_9

    invoke-super {p0}, Landroid/widget/ListView;->getTopFadingEdgeStrength()F

    move-result v0

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public setBottomFadingEdgeVisibility(Z)V
    .registers 2
    .parameter

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    .line 55
    return-void
.end method

.method public setFadeColor(I)V
    .registers 2
    .parameter

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    .line 47
    return-void
.end method

.method public setTopFadingEdgeVisibility(Z)V
    .registers 2
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    .line 51
    return-void
.end method
