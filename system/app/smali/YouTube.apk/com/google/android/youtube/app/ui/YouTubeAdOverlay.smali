.class public final Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/a;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private final c:Landroid/widget/FrameLayout;

.field private final d:Landroid/widget/TextView;

.field private final e:F

.field private final f:I

.field private g:Lcom/google/android/youtube/core/player/b;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    const v2, -0x333334

    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    iput-object p2, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->b:Lcom/google/android/youtube/core/Analytics;

    .line 62
    iput p3, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->f:I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->e:F

    .line 66
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 73
    iget v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->e:F

    invoke-static {v5, v0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a(IF)I

    move-result v0

    .line 74
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    const/high16 v1, -0x7800

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 77
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 79
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 80
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    .line 83
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->e:F

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a(IF)I

    move-result v0

    .line 91
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 94
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 96
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 97
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 98
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->addView(Landroid/view/View;)V

    .line 102
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->setFullscreen(Z)V

    .line 103
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->setVisibility(I)V

    .line 104
    return-void
.end method

.method private static a(IF)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 115
    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->h:Z

    if-nez v0, :cond_2c

    move v0, v1

    .line 120
    :goto_7
    iget-object v4, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_2e

    move v3, v2

    :goto_c
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    if-eqz v0, :cond_2b

    .line 122
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->h:Z

    if-nez v0, :cond_31

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0069

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->j:Ljava/lang/String;

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_2b
    :goto_2b
    return-void

    :cond_2c
    move v0, v2

    .line 119
    goto :goto_7

    .line 120
    :cond_2e
    const/16 v3, 0x8

    goto :goto_c

    .line 124
    :cond_31
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_50

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b006a

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->i:Ljava/lang/String;

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2b

    .line 127
    :cond_50
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2b
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 107
    return-object p0
.end method

.method public final a(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 154
    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->j:Ljava/lang/String;

    .line 155
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d()V

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    if-ne v0, v1, :cond_36

    .line 157
    div-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, 0x5

    .line 158
    if-gtz v0, :cond_37

    .line 159
    sget-object v0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->g:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_36

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->g:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->b()V

    .line 168
    :cond_36
    :goto_36
    return-void

    .line 165
    :cond_37
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b006d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_36
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->j:Ljava/lang/String;

    .line 138
    iget-object v0, p1, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->i:Ljava/lang/String;

    .line 139
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d()V

    .line 140
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 141
    sget-object v0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_35
    :goto_35
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->setVisibility(I)V

    .line 151
    return-void

    .line 145
    :cond_39
    sget-object v0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;->NOT_SKIPPABLE:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_35

    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_35
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 111
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 178
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->setVisibility(I)V

    .line 179
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->g:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_1c

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->k:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/app/ui/YouTubeAdOverlay$State;

    if-ne v0, v1, :cond_1c

    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "SkipAd"

    const-string v2, "Overlay"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->g:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->c()V

    .line 189
    :cond_1c
    return-void
.end method

.method public final setFullscreen(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 171
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->h:Z

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 173
    const/16 v1, 0x19

    iget v3, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->e:F

    invoke-static {v1, v3}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->a(IF)I

    move-result v3

    if-eqz p1, :cond_1f

    iget v1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->f:I

    :goto_17
    add-int/2addr v1, v3

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 174
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->d()V

    .line 175
    return-void

    :cond_1f
    move v1, v2

    .line 173
    goto :goto_17
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/b;)V
    .registers 2
    .parameter

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;->g:Lcom/google/android/youtube/core/player/b;

    .line 134
    return-void
.end method
