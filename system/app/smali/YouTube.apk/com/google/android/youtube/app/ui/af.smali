.class final Lcom/google/android/youtube/app/ui/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/ad;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/ad;)V
    .registers 2
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .parameter

    .prologue
    .line 124
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ad;->b(Lcom/google/android/youtube/app/ui/ad;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ad;->c(Lcom/google/android/youtube/app/ui/ad;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/ad;->a(Lcom/google/android/youtube/app/ui/ad;Z)V

    .line 133
    :goto_25
    return-void

    .line 128
    :cond_26
    invoke-static {}, Lcom/google/android/youtube/app/ui/ad;->a()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ad;->d(Lcom/google/android/youtube/app/ui/ad;)V

    goto :goto_25

    .line 131
    :cond_3e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/af;->a:Lcom/google/android/youtube/app/ui/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ad;->e(Lcom/google/android/youtube/app/ui/ad;)V

    goto :goto_25
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    return-void
.end method
