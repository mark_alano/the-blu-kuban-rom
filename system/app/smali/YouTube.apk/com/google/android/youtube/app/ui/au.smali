.class final Lcom/google/android/youtube/app/ui/au;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 290
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 291
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/au;->a:Landroid/view/LayoutInflater;

    .line 292
    iput-object p2, p0, Lcom/google/android/youtube/app/ui/au;->b:Ljava/util/List;

    .line 293
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 318
    if-nez p2, :cond_1e

    .line 319
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->a:Landroid/view/LayoutInflater;

    const v1, 0x1090009

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 322
    :goto_c
    check-cast v0, Landroid/widget/TextView;

    .line 323
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/au;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    return-object v0

    :cond_1e
    move-object v0, p2

    goto :goto_c
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 304
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 308
    if-nez p2, :cond_1e

    .line 309
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->a:Landroid/view/LayoutInflater;

    const v1, 0x1090008

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 311
    :goto_c
    check-cast v0, Landroid/widget/TextView;

    .line 312
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/au;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    return-object v0

    :cond_1e
    move-object v0, p2

    goto :goto_c
.end method
