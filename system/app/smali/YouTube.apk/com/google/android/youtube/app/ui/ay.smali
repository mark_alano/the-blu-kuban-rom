.class public abstract Lcom/google/android/youtube/app/ui/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/a;

.field private final b:Ljava/lang/String;

.field protected final f:Landroid/app/Activity;

.field protected final g:Lcom/google/android/youtube/core/Analytics;

.field protected final h:Lcom/google/android/youtube/app/compat/r;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    .line 39
    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    .line 40
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 41
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->g:Lcom/google/android/youtube/core/Analytics;

    .line 42
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ay;->b:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/google/android/youtube/app/ui/ay;->h:Lcom/google/android/youtube/app/compat/r;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->h:Lcom/google/android/youtube/app/compat/r;

    const v1, 0x7f110002

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 67
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_b2

    .line 118
    :goto_9
    return v0

    .line 69
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->a()V

    move v0, v1

    .line 70
    goto :goto_9

    .line 73
    :sswitch_11
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->b()V

    move v0, v1

    .line 74
    goto :goto_9

    .line 77
    :sswitch_18
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/ay;->d()V

    move v0, v1

    .line 79
    goto :goto_9

    .line 82
    :sswitch_2e
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    const/4 v3, 0x0

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;I)V

    move v0, v1

    .line 83
    goto :goto_9

    .line 86
    :sswitch_36
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->c()V

    move v0, v1

    .line 87
    goto :goto_9

    .line 90
    :sswitch_3d
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadFromMenu"

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/app/Activity;)V

    move v0, v1

    .line 92
    goto :goto_9

    .line 95
    :sswitch_55
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->i()V

    move v0, v1

    .line 96
    goto :goto_9

    .line 99
    :sswitch_5c
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->b:Ljava/lang/String;

    if-eqz v0, :cond_76

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    const v2, 0x7f0b021e

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ay;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_6b
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    move v0, v1

    .line 104
    goto :goto_9

    .line 99
    :cond_76
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    const v2, 0x7f0b021d

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6b

    .line 107
    :sswitch_80
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    const/16 v2, 0x3f1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->showDialog(I)V

    :goto_8f
    move v0, v1

    .line 115
    goto/16 :goto_9

    .line 110
    :cond_92
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 111
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ay;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f0b021f

    invoke-static {v3, v0, v4}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_8f

    .line 67
    nop

    :sswitch_data_b2
    .sparse-switch
        0x7f080197 -> :sswitch_3d
        0x7f08019e -> :sswitch_18
        0x7f08019f -> :sswitch_2e
        0x7f0801a0 -> :sswitch_55
        0x7f0801a1 -> :sswitch_5c
        0x7f0801a2 -> :sswitch_80
        0x7f0801aa -> :sswitch_a
        0x7f0801ab -> :sswitch_11
        0x7f0801ac -> :sswitch_36
    .end sparse-switch
.end method

.method protected abstract d()V
.end method
