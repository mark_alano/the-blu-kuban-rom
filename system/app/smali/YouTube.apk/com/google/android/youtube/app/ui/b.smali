.class public final Lcom/google/android/youtube/app/ui/b;
.super Lcom/google/android/youtube/app/adapter/cy;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:I

.field private final c:I

.field private final d:I

.field private e:Lcom/google/android/youtube/app/ui/c;

.field private f:I

.field private final g:Landroid/view/ViewGroup;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/Button;

.field private final k:Landroid/view/View;

.field private final l:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 49
    const v0, 0x7f040013

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 23
    iput v3, p0, Lcom/google/android/youtube/app/ui/b;->a:I

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/app/ui/b;->c:I

    .line 25
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/youtube/app/ui/b;->d:I

    .line 50
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->g:Landroid/view/ViewGroup;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->h:Landroid/view/View;

    .line 52
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->k:Landroid/view/View;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/b;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 60
    return-void
.end method

.method private e(I)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    iget v0, p0, Lcom/google/android/youtube/app/ui/b;->f:I

    if-eq v0, p1, :cond_1b

    .line 110
    iput p1, p0, Lcom/google/android/youtube/app/ui/b;->f:I

    .line 111
    iget v0, p0, Lcom/google/android/youtube/app/ui/b;->f:I

    if-ne v0, v3, :cond_1c

    .line 112
    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_1b
    :goto_1b
    return-void

    .line 115
    :cond_1c
    iget v0, p0, Lcom/google/android/youtube/app/ui/b;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2f

    .line 116
    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1b

    .line 119
    :cond_2f
    iget v0, p0, Lcom/google/android/youtube/app/ui/b;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1b

    .line 120
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    goto :goto_1b
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/c;)V
    .registers 2
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/b;->e:Lcom/google/android/youtube/app/ui/c;

    .line 64
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 75
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 96
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    if-eqz p2, :cond_23

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    :goto_1d
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 106
    return-void

    .line 103
    :cond_23
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 72
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    .line 88
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 93
    return-void
.end method

.method public final k_()V
    .registers 2

    .prologue
    .line 67
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/b;->e(I)V

    .line 68
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->e:Lcom/google/android/youtube/app/ui/c;

    if-eqz v0, :cond_d

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_e

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->e:Lcom/google/android/youtube/app/ui/c;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/c;->g()V

    .line 141
    :cond_d
    :goto_d
    return-void

    .line 137
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->e:Lcom/google/android/youtube/app/ui/c;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/c;->h()V

    goto :goto_d
.end method
