.class final Lcom/google/android/youtube/app/ui/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bg;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bg;)V
    .registers 2
    .parameter

    .prologue
    .line 758
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    if-nez v0, :cond_9

    .line 773
    :goto_8
    return-void

    .line 764
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 765
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->k(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueRemoveVideoWatchPage"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 771
    :goto_3d
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->p(Lcom/google/android/youtube/app/ui/bg;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 772
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->q(Lcom/google/android/youtube/app/ui/bg;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_8

    .line 768
    :cond_52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->k(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueAddVideo"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/br;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3d
.end method
