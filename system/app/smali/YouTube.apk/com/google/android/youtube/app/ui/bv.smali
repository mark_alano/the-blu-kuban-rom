.class public final Lcom/google/android/youtube/app/ui/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/youtube/core/async/l;
.implements Lcom/google/android/youtube/core/ui/h;


# instance fields
.field private final a:Lcom/google/android/youtube/core/ui/g;

.field private final b:Lcom/google/android/youtube/core/async/av;

.field private final c:Lcom/google/android/youtube/core/d;

.field private final d:Lcom/google/android/youtube/core/a/j;

.field private final e:Lcom/google/android/youtube/core/ui/i;

.field private final f:Lcom/google/android/youtube/core/async/c;

.field private final g:Landroid/os/Handler;

.field private h:Ljava/util/List;

.field private i:I

.field private j:Ljava/util/List;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private final p:Lcom/google/android/youtube/app/a;

.field private final q:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string v0, "targetAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v0, "analytics can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->q:Lcom/google/android/youtube/core/Analytics;

    .line 74
    const-string v0, "view cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    .line 75
    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->b:Lcom/google/android/youtube/core/async/av;

    .line 76
    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->c:Lcom/google/android/youtube/core/d;

    .line 77
    const-string v0, "navigation cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->p:Lcom/google/android/youtube/app/a;

    .line 79
    invoke-interface {p2}, Lcom/google/android/youtube/core/ui/g;->h()Lcom/google/android/youtube/core/ui/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/youtube/core/a/j;->a(Lcom/google/android/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/youtube/core/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/g;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    instance-of v0, p3, Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_88

    .line 86
    check-cast p3, Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/app/ui/by;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 90
    :goto_6c
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/i;->a(Landroid/view/View$OnClickListener;)V

    .line 93
    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->f:Lcom/google/android/youtube/core/async/c;

    .line 95
    new-instance v0, Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/bw;-><init>(Lcom/google/android/youtube/app/ui/bv;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->g:Landroid/os/Handler;

    .line 103
    return-void

    .line 88
    :cond_88
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_6c
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bv;)V
    .registers 1
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bv;->b()V

    return-void
.end method

.method private b()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 196
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bv;->o:Z

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->i:I

    iget v2, p0, Lcom/google/android/youtube/app/ui/bv;->l:I

    if-lt v1, v2, :cond_c

    .line 206
    :cond_b
    :goto_b
    return-void

    .line 199
    :cond_c
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bv;->o:Z

    .line 200
    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->i:I

    mul-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    .line 201
    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    add-int/lit8 v1, v1, 0xf

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 202
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->h:Ljava/util/List;

    iget v3, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    invoke-interface {v2, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->j:Ljava/util/List;

    .line 203
    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/youtube/app/ui/bv;->i:I

    .line 204
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3f

    const/4 v0, 0x1

    :cond_3f
    if-eqz v0, :cond_5a

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    .line 205
    :goto_50
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->b:Lcom/google/android/youtube/core/async/av;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->f:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_b

    .line 204
    :cond_5a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->f()V

    goto :goto_50
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->f()V

    .line 107
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/ui/g;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 164
    add-int v0, p2, p3

    iput v0, p0, Lcom/google/android/youtube/app/ui/bv;->m:I

    .line 165
    iget v0, p0, Lcom/google/android/youtube/app/ui/bv;->m:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->n:I

    if-lt v0, v1, :cond_10

    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 168
    :cond_10
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p1, Ljava/util/List;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->c:Lcom/google/android/youtube/core/d;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_43

    const/4 v0, 0x1

    :goto_31
    if-eqz v0, :cond_45

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    :goto_42
    return-void

    :cond_43
    const/4 v0, 0x0

    goto :goto_31

    :cond_45
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/g;->c(Ljava/lang/String;)V

    goto :goto_42
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_85

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v0, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/BatchEntry;

    iget v4, v0, Lcom/google/android/youtube/core/model/BatchEntry;->b:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_33

    iget-object v0, v0, Lcom/google/android/youtube/core/model/BatchEntry;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_18

    :cond_33
    new-instance v4, Lcom/google/android/youtube/core/model/Video$Builder;

    invoke-direct {v4}, Lcom/google/android/youtube/core/model/Video$Builder;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/core/model/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video$Builder;->build()Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2f

    :cond_4c
    iget v0, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/ui/bv;->n:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/a/j;->a(ILjava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-eqz v0, :cond_86

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    :goto_79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bv;->o:Z

    iget v0, p0, Lcom/google/android/youtube/app/ui/bv;->m:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/bv;->n:I

    if-le v0, v1, :cond_85

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bv;->b()V

    :cond_85
    return-void

    :cond_86
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->e()V

    goto :goto_79
.end method

.method public final a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bv;->h:Ljava/util/List;

    .line 172
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->a:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->e()V

    .line 175
    :cond_d
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bv;->i:I

    .line 176
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x402e

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/bv;->l:I

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bv;->o:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_55

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 180
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_3c
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_50

    .line 181
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/a/j;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 183
    :cond_50
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/j;->a(Ljava/util/Collection;)V

    .line 185
    :cond_55
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bv;->b()V

    .line 186
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->j:Ljava/util/List;

    if-eqz v0, :cond_d

    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->b:Lcom/google/android/youtube/core/async/av;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bv;->f:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 113
    :cond_d
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_21

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/core/a/j;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 157
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bv;->p:Lcom/google/android/youtube/app/a;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/app/m;->T:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bv;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteQueueGoToWatch"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 160
    :cond_21
    return-void
.end method
