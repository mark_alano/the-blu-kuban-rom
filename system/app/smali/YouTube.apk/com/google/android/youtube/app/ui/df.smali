.class public final Lcom/google/android/youtube/app/ui/df;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/DialogInterface$OnClickListener;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/view/View;

.field private final f:Landroid/app/AlertDialog;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/ui/df;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "context may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/df;->a:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/youtube/app/ui/df;->b:Landroid/content/DialogInterface$OnClickListener;

    .line 47
    const-string v0, "youtube"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/df;->c:Landroid/content/SharedPreferences;

    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->c:Landroid/content/SharedPreferences;

    const-string v1, "upload_policy"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/df;->g:Z

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/df;->d:Landroid/view/LayoutInflater;

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0400c6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/df;->e:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->e:Landroid/view/View;

    const v1, 0x7f080185

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 56
    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/ui/dg;-><init>(Lcom/google/android/youtube/app/ui/df;Landroid/widget/RadioButton;)V

    .line 71
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b00cf

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/df;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/df;->f:Landroid/app/AlertDialog;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/df;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/df;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/df;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/df;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/df;)Landroid/content/DialogInterface$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->b:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/df;->f:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/df;->g:Z

    return v0
.end method
