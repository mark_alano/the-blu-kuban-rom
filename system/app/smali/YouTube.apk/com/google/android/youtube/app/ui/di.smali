.class public final Lcom/google/android/youtube/app/ui/di;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private final g:Lcom/google/android/youtube/app/ui/dq;

.field private final h:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private final i:Lcom/google/android/youtube/core/b/al;

.field private final j:Lcom/google/android/youtube/core/async/av;

.field private final k:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final l:Lcom/google/android/youtube/app/g;

.field private final m:Lcom/google/android/youtube/app/k;

.field private final n:Lcom/google/android/youtube/core/d;

.field private final o:Lcom/google/android/youtube/app/ui/dr;

.field private final p:Lcom/google/android/youtube/app/ui/z;

.field private final q:Lcom/google/android/youtube/app/ui/do;

.field private r:Lcom/google/android/youtube/core/model/Video;

.field private s:Landroid/net/Uri;

.field private final t:Lcom/google/android/youtube/app/a;

.field private final u:Lcom/google/android/youtube/core/player/bx;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/dq;Lcom/google/android/youtube/app/YouTubePlatformUtil;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/player/bx;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p9, p0, Lcom/google/android/youtube/app/ui/di;->t:Lcom/google/android/youtube/app/a;

    .line 115
    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    .line 116
    const-string v0, "analytics can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    .line 117
    const-string v0, "userAuthorizer can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 119
    const-string v0, "youTubeAuthorizer can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->l:Lcom/google/android/youtube/app/g;

    .line 121
    const-string v0, "config can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->m:Lcom/google/android/youtube/app/k;

    .line 122
    const-string v0, "likeDislikeCountTracker can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->g:Lcom/google/android/youtube/app/ui/dq;

    .line 124
    const-string v0, "youTubePlatformUtil can not be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubePlatformUtil;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->h:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 126
    const-string v0, "gdataClient can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->i:Lcom/google/android/youtube/core/b/al;

    .line 127
    const-string v0, "errorHelper can not be null"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->n:Lcom/google/android/youtube/core/d;

    .line 128
    const-string v0, "player can not be null"

    invoke-static {p11, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/bx;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->u:Lcom/google/android/youtube/core/player/bx;

    .line 130
    invoke-interface {p2}, Lcom/google/android/youtube/core/b/al;->n()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->j:Lcom/google/android/youtube/core/async/av;

    .line 132
    new-instance v0, Lcom/google/android/youtube/app/ui/dr;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/dr;-><init>(Lcom/google/android/youtube/app/ui/di;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->o:Lcom/google/android/youtube/app/ui/dr;

    .line 133
    new-instance v0, Lcom/google/android/youtube/app/ui/z;

    invoke-direct {v0, p1, p3, p2, p10}, Lcom/google/android/youtube/app/ui/z;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->p:Lcom/google/android/youtube/app/ui/z;

    .line 135
    new-instance v0, Lcom/google/android/youtube/app/ui/do;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/do;-><init>(Lcom/google/android/youtube/app/ui/di;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->q:Lcom/google/android/youtube/app/ui/do;

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->s:Landroid/net/Uri;

    .line 138
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/di;->s:Landroid/net/Uri;

    .line 211
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/di;->h()Z

    move-result v0

    if-eqz v0, :cond_27

    const v0, 0x7f0b01e2

    .line 212
    :goto_b
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_14

    .line 213
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 216
    :cond_14
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/di;->h()Z

    move-result v0

    if-eqz v0, :cond_2b

    const v0, 0x7f0201fc

    :goto_1d
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->f:Landroid/widget/ImageView;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 217
    :cond_26
    return-void

    .line 211
    :cond_27
    const v0, 0x7f0b01e1

    goto :goto_b

    .line 216
    :cond_2b
    const v0, 0x7f02005c

    goto :goto_1d
.end method

.method private static a(Landroid/view/View;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 248
    if-eqz p0, :cond_b

    .line 249
    invoke-virtual {p0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 250
    if-nez p1, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 252
    :cond_b
    return-void

    .line 250
    :cond_c
    const/4 v0, 0x0

    goto :goto_8
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/di;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/di;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/di;Landroid/net/Uri;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/di;Landroid/view/View;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-static {p1, p2}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/di;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/model/Video;)Z
    .registers 3
    .parameter

    .prologue
    .line 141
    const-string v0, "video can\'t be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/google/android/youtube/app/ui/dk;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Video$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1c

    .line 150
    const/4 v0, 0x0

    :goto_13
    return v0

    .line 146
    :pswitch_14
    const/4 v0, 0x1

    goto :goto_13

    .line 148
    :pswitch_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    goto :goto_13

    .line 142
    nop

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_16
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->n:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    .line 256
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/di;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/di;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->i:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/ui/dq;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->g:Lcom/google/android/youtube/app/ui/dq;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->s:Landroid/net/Uri;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/async/av;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->j:Lcom/google/android/youtube/core/async/av;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/k;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->m:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/g;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->l:Lcom/google/android/youtube/app/g;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .registers 4
    .parameter

    .prologue
    .line 230
    sparse-switch p1, :sswitch_data_20

    .line 243
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 232
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->o:Lcom/google/android/youtube/app/ui/dr;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dr;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 234
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->p:Lcom/google/android/youtube/app/ui/z;

    new-instance v1, Lcom/google/android/youtube/app/ui/dj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/dj;-><init>(Lcom/google/android/youtube/app/ui/di;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/z;->a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 241
    :sswitch_18
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->q:Lcom/google/android/youtube/app/ui/do;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/do;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 230
    nop

    :sswitch_data_20
    .sparse-switch
        0x3ed -> :sswitch_c
        0x3f4 -> :sswitch_5
        0x3ff -> :sswitch_18
    .end sparse-switch
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Like"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/du;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/ui/du;-><init>(Lcom/google/android/youtube/app/ui/di;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 176
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->c:Landroid/view/View;

    .line 155
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/di;->d:Landroid/view/View;

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 159
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    .line 160
    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/net/Uri;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->c:Landroid/view/View;

    if-eqz v0, :cond_14

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->c:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/view/View;Z)V

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/view/View;Z)V

    .line 165
    :cond_14
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Dislike"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/du;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/ui/du;-><init>(Lcom/google/android/youtube/app/ui/di;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 181
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Share"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/content/Context;Lcom/google/android/youtube/core/model/Video;)V

    .line 186
    return-void
.end method

.method public final d()V
    .registers 4

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "CopyURL"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->h:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->watchUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 191
    const v0, 0x7f0b00d2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/di;->b(I)V

    .line 192
    return-void
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Flag"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/di;->q:Lcom/google/android/youtube/app/ui/do;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 197
    return-void
.end method

.method public final f()V
    .registers 4

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/di;->o:Lcom/google/android/youtube/app/ui/dr;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 207
    return-void
.end method

.method public final g()V
    .registers 4

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "YouTubeTvAddScreen"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 264
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/di;->t:Lcom/google/android/youtube/app/a;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/di;->r:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    :goto_11
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/di;->u:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;I)V

    .line 265
    return-void

    .line 264
    :cond_1b
    const/4 v0, 0x0

    goto :goto_11
.end method
