.class final Lcom/google/android/youtube/app/ui/dr;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/di;

.field private final b:Lcom/google/android/youtube/app/adapter/bi;

.field private final d:Lcom/google/android/youtube/core/ui/j;

.field private final e:Lcom/google/android/youtube/core/ui/PagedListView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/di;)V
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 462
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    .line 463
    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->b(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/d;)V

    .line 465
    new-instance v0, Lcom/google/android/youtube/app/adapter/ar;

    iget-object v1, p1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    const v2, 0x7f040086

    const/4 v3, 0x1

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/youtube/app/adapter/ar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/ToolbarHelper;IZ)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/bi;

    .line 467
    iget-object v0, p1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 469
    new-instance v0, Lcom/google/android/youtube/core/ui/j;

    iget-object v1, p1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/bi;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->i(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/async/av;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/d;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->d:Lcom/google/android/youtube/core/ui/j;

    .line 476
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/youtube/app/ui/ds;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/ui/ds;-><init>(Lcom/google/android/youtube/app/ui/dr;Lcom/google/android/youtube/app/ui/di;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 510
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/app/adapter/bi;
    .registers 2
    .parameter

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/bi;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->d:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/di;->e(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/b/al;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->g(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 515
    return-void
.end method

.method public final b()Landroid/app/Dialog;
    .registers 3

    .prologue
    .line 518
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b01e4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
