.class final Lcom/google/android/youtube/app/ui/ds;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/di;

.field final synthetic b:Lcom/google/android/youtube/app/ui/dr;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/dr;Lcom/google/android/youtube/app/ui/di;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 477
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ds;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    .line 480
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dr;->a(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/app/adapter/bi;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/bi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    .line 481
    if-nez v0, :cond_b7

    .line 482
    if-nez p3, :cond_5b

    .line 483
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->h(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "WatchLater"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dv;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/di;->f(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v5, v5, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v5}, Lcom/google/android/youtube/app/ui/di;->e(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/b/al;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v6}, Lcom/google/android/youtube/app/ui/dr;->b(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/d;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/dv;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 508
    :cond_5a
    :goto_5a
    return-void

    .line 486
    :cond_5b
    const/4 v0, 0x1

    if-ne p3, v0, :cond_88

    .line 487
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->k(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dl;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/ui/dl;-><init>(Lcom/google/android/youtube/app/ui/di;)V

    const v3, 0x7f0b01be

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/di;->j(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->c()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    goto :goto_5a

    .line 492
    :cond_88
    const/4 v0, 0x2

    if-ne p3, v0, :cond_5a

    .line 493
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->k(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dt;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/dr;->c(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/d;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/ui/dt;-><init>(Lcom/google/android/youtube/app/ui/ds;Lcom/google/android/youtube/core/d;)V

    const v3, 0x7f0b01bd

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/di;->j(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/app/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->b()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    goto :goto_5a

    .line 505
    :cond_b7
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/di;->h(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "SaveToPlaylist"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 506
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/dn;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-direct {v3, v4, v0}, Lcom/google/android/youtube/app/ui/dn;-><init>(Lcom/google/android/youtube/app/ui/di;Lcom/google/android/youtube/core/model/Playlist;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    goto/16 :goto_5a
.end method
