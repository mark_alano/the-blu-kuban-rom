.class public Lcom/google/android/youtube/app/ui/eb;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Z

.field private final b:Lcom/google/android/youtube/app/ui/az;

.field private final h:Lcom/google/android/youtube/core/a/a;

.field private final i:Lcom/google/android/youtube/app/ui/by;

.field private final j:Lcom/google/android/youtube/app/ui/ed;

.field private k:Lcom/google/android/youtube/app/ui/ee;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    new-instance v1, Lcom/google/android/youtube/app/ui/ec;

    const-string v2, "navigation cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/app/a;

    const-string v3, "referrer cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/b/aq;

    const-string v3, "analytics cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/core/Analytics;

    const-string v3, "logCategory cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move/from16 v3, p8

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/app/ui/ec;-><init>(Lcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move/from16 v8, p6

    move-object v9, v1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/ui/ed;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/ui/ed;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    .line 95
    iput-boolean p6, p0, Lcom/google/android/youtube/app/ui/eb;->a:Z

    .line 96
    new-instance v0, Lcom/google/android/youtube/app/ui/az;

    invoke-direct {v0}, Lcom/google/android/youtube/app/ui/az;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->b:Lcom/google/android/youtube/app/ui/az;

    .line 97
    const-string v0, "onVideoClickListener can\'t be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ed;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->j:Lcom/google/android/youtube/app/ui/ed;

    .line 100
    instance-of v0, p3, Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_2d

    move-object v0, p3

    .line 101
    check-cast v0, Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/by;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 102
    check-cast p3, Lcom/google/android/youtube/app/ui/by;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eb;->i:Lcom/google/android/youtube/app/ui/by;

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->i:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/by;->b()Lcom/google/android/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->h:Lcom/google/android/youtube/core/a/a;

    .line 109
    :goto_2c
    return-void

    .line 105
    :cond_2d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->i:Lcom/google/android/youtube/app/ui/by;

    .line 106
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eb;->h:Lcom/google/android/youtube/core/a/a;

    .line 107
    invoke-interface {p2, p0}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_2c
.end method


# virtual methods
.method protected a(I)V
    .registers 2
    .parameter

    .prologue
    .line 154
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 140
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    .line 144
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p2, Lcom/google/android/youtube/core/model/Page;->elementsPerPage:I

    if-ge v0, v1, :cond_1c

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1c

    .line 145
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 149
    :goto_18
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/eb;->a(I)V

    .line 150
    return-void

    .line 147
    :cond_1c
    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    goto :goto_18
.end method

.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_28

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_28

    instance-of v0, p2, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_28

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 132
    new-instance v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Page$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Page$Builder;->build()Lcom/google/android/youtube/core/model/Page;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/ui/eb;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    .line 136
    :goto_27
    return-void

    .line 134
    :cond_28
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    goto :goto_27
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/ui/eb;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/ui/eb;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/eb;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->b:Lcom/google/android/youtube/app/ui/az;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/az;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    if-eqz p1, :cond_1a

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 159
    :goto_6
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_19

    .line 160
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 161
    if-eqz v0, :cond_19

    .line 162
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eb;->j:Lcom/google/android/youtube/app/ui/ed;

    invoke-interface {v1, v0, p3}, Lcom/google/android/youtube/app/ui/ed;->a(Lcom/google/android/youtube/core/model/Video;I)V

    .line 165
    :cond_19
    return-void

    .line 157
    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->h:Lcom/google/android/youtube/core/a/a;

    goto :goto_6
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    if-eqz p1, :cond_15

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 169
    :goto_6
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 170
    if-eqz v0, :cond_18

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->k:Lcom/google/android/youtube/app/ui/ee;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ee;->a()Z

    move-result v0

    .line 173
    :goto_14
    return v0

    .line 168
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->h:Lcom/google/android/youtube/core/a/a;

    goto :goto_6

    .line 173
    :cond_18
    const/4 v0, 0x0

    goto :goto_14
.end method
