.class public final Lcom/google/android/youtube/app/ui/ef;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/cy;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cy;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 56
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cy;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->a:Lcom/google/android/youtube/app/adapter/cy;

    .line 57
    const-string v0, "artistInfoView cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const v0, 0x7f08003d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->c:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f08003e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->a:Lcom/google/android/youtube/app/adapter/cy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    .line 84
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 67
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    const v1, 0x7f0b01d0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 72
    :goto_e
    return-void

    .line 70
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    const-string v1, "[\\r\\n]+"

    const-string v2, "\r\n\r\n"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_e
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method
