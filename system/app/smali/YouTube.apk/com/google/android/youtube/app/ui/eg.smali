.class public final Lcom/google/android/youtube/app/ui/eg;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/c;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/bt;

.field private final c:Lcom/google/android/youtube/app/adapter/ac;

.field private final d:Lcom/google/android/youtube/app/adapter/bc;

.field private final e:Lcom/google/android/youtube/app/ui/b;

.field private final f:Lcom/google/android/youtube/core/a/l;

.field private final g:Landroid/app/Activity;

.field private final h:Lcom/google/android/youtube/core/async/av;

.field private final i:Ljava/util/Map;

.field private final j:Lcom/google/android/youtube/core/async/l;

.field private k:Lcom/google/android/youtube/core/async/n;

.field private final l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/adapter/bc;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/b;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 148
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    aput-object p4, v0, v3

    const/4 v1, 0x1

    aput-object p5, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 152
    const-string v0, "headingOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ac;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->c:Lcom/google/android/youtube/app/adapter/ac;

    .line 154
    const-string v0, "pagingAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/bc;

    .line 155
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/bc;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bt;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bt;

    .line 156
    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/b;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/app/ui/c;)V

    .line 159
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->f:Lcom/google/android/youtube/core/a/l;

    .line 161
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    .line 162
    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-interface {p2}, Lcom/google/android/youtube/core/b/al;->z()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->h:Lcom/google/android/youtube/core/async/av;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/Map;

    .line 165
    new-instance v0, Lcom/google/android/youtube/app/ui/ei;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/ui/ei;-><init>(Lcom/google/android/youtube/app/ui/eg;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->j:Lcom/google/android/youtube/core/async/l;

    .line 167
    const v0, 0x7f0b0113

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->l:Ljava/lang/String;

    .line 168
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/eg;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/core/async/n;

    .line 210
    :cond_c
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->j:Lcom/google/android/youtube/core/async/l;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/core/async/n;

    .line 212
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->h:Lcom/google/android/youtube/core/async/av;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 213
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/b;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/eg;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/eg;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eg;->m:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->a()V

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 182
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->l:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/b;->a(Ljava/lang/String;Z)V

    .line 193
    :goto_18
    return-void

    .line 185
    :cond_19
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1d
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    .line 186
    iget-object v2, v0, Lcom/google/android/youtube/core/model/MusicVideo;->trackId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eg;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 187
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1d

    .line 190
    :cond_3b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->e()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/eg;->b()V

    goto :goto_18
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->f:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    .line 224
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eg;->n:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->c:Lcom/google/android/youtube/app/adapter/ac;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    const v2, 0x7f0b01cd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ac;->a(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bc;->c()Z

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bc;->b()Z

    move-result v0

    if-nez v0, :cond_13

    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    .line 200
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/eg;->k()V

    .line 201
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->e()V

    .line 205
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/eg;->b()V

    .line 206
    return-void
.end method
