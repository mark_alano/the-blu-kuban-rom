.class final Lcom/google/android/youtube/app/ui/eh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/cr;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bt;

.field final synthetic b:Lcom/google/android/youtube/core/Analytics;

.field final synthetic c:Lcom/google/android/youtube/app/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/adapter/bt;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eh;->a:Lcom/google/android/youtube/app/adapter/bt;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/eh;->b:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eh;->c:Lcom/google/android/youtube/app/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/adapter/cq;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eh;->a:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 107
    if-eqz v0, :cond_1f

    .line 108
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    .line 109
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eh;->b:Lcom/google/android/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ArtistTracks:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    .line 110
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eh;->c:Lcom/google/android/youtube/app/a;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/app/m;->D:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)V

    .line 113
    :cond_1f
    return-void
.end method
