.class public final Lcom/google/android/youtube/app/ui/ej;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/g;

.field private final c:Lcom/google/android/youtube/app/k;

.field private final d:Lcom/google/android/youtube/core/Analytics;

.field private final e:Lcom/google/android/youtube/core/b/al;

.field private final f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final g:Lcom/google/android/youtube/core/d;

.field private final h:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final i:Lcom/google/android/youtube/core/ui/j;

.field private final j:Z

.field private final k:Landroid/view/View;

.field private final l:Landroid/widget/EditText;

.field private final m:Landroid/widget/ImageButton;

.field private final n:Ljava/lang/CharSequence;

.field private final o:Lcom/google/android/youtube/app/adapter/af;

.field private p:Lcom/google/android/youtube/core/model/Video;

.field private q:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedListView;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/d;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    .line 83
    const-string v0, "gdataClient cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->e:Lcom/google/android/youtube/core/b/al;

    .line 84
    const-string v0, "youTubeAuthorizer cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->b:Lcom/google/android/youtube/app/g;

    .line 86
    const-string v0, "config cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/k;

    .line 87
    const-string v0, "analytics cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/core/Analytics;

    .line 88
    const-string v0, "errorHelper cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/d;

    .line 89
    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ej;->h:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 90
    invoke-interface {p3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 91
    iput-boolean v2, p0, Lcom/google/android/youtube/app/ui/ej;->j:Z

    .line 93
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_a1

    const v0, 0x7f040097

    :goto_57
    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    const v1, 0x7f080037

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->l:Landroid/widget/EditText;

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    const v1, 0x7f0800b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->m:Landroid/widget/ImageButton;

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    .line 109
    new-instance v0, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/app/adapter/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->o:Lcom/google/android/youtube/app/adapter/af;

    .line 110
    new-instance v0, Lcom/google/android/youtube/core/ui/j;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ej;->o:Lcom/google/android/youtube/app/adapter/af;

    invoke-interface {p3}, Lcom/google/android/youtube/core/b/al;->w()Lcom/google/android/youtube/core/async/av;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Lcom/google/android/youtube/core/ui/j;

    .line 112
    return-void

    .line 93
    :cond_a1
    const v0, 0x7f040006

    goto :goto_57
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->l:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 8
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ej;->q:Lcom/google/android/youtube/core/model/UserAuth;

    .line 181
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/ej;->j:Z

    if-eqz v0, :cond_13

    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    .line 187
    :goto_12
    return-void

    .line 185
    :cond_13
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ej;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_5a

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v3, "Comment"

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ej;->e:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ej;->p:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ej;->q:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    invoke-static {v5, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v5

    invoke-interface {v2, v3, v4, v0, v5}, Lcom/google/android/youtube/core/b/al;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_12

    :cond_5a
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/d;

    const v3, 0x7f0b01f7

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/d;->a(I)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_12
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 7
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v4, 0x0

    .line 119
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ej;->p:Lcom/google/android/youtube/core/model/Video;

    .line 120
    if-eqz p1, :cond_3c

    .line 121
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    if-eqz v0, :cond_28

    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ej;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 133
    :goto_27
    return-void

    .line 126
    :cond_28
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->h:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const v2, 0x7f0b0117

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Ljava/lang/String;)V

    goto :goto_27

    .line 130
    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/j;->e()V

    goto :goto_27
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 45
    if-eqz p2, :cond_18

    const/16 v0, 0x193

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b00d9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    :goto_12
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b00d8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    goto :goto_12
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 45
    check-cast p2, Lcom/google/android/youtube/core/model/Comment;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const v1, 0x7f0b01f6

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->o:Lcom/google/android/youtube/app/adapter/af;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/youtube/app/adapter/af;->c(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 196
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 116
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    .line 191
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->b:Lcom/google/android/youtube/app/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ej;->a:Landroid/app/Activity;

    const v2, 0x7f0b01bc

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->d()Z

    move-result v3

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 177
    return-void
.end method
