.class public final Lcom/google/android/youtube/app/ui/eu;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/cr;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final A:Landroid/widget/ImageButton;

.field private final B:Landroid/widget/ImageButton;

.field private final C:Landroid/widget/TextView;

.field private final D:Landroid/widget/FrameLayout;

.field private final E:Landroid/widget/ProgressBar;

.field private F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private G:I

.field private H:I

.field private I:Lcom/google/android/youtube/core/model/Video;

.field private final a:Lcom/google/android/youtube/app/adapter/cy;

.field private final c:Lcom/google/android/youtube/app/adapter/cy;

.field private final d:Landroid/app/Activity;

.field private final e:Lcom/google/android/youtube/core/Analytics;

.field private final f:Lcom/google/android/youtube/app/a;

.field private final g:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/core/b/al;

.field private final i:Lcom/google/android/youtube/core/b/an;

.field private final j:Lcom/google/android/youtube/core/utils/l;

.field private k:Lcom/google/android/youtube/app/ui/di;

.field private final l:Lcom/google/android/youtube/app/ui/ez;

.field private final m:Lcom/google/android/youtube/app/ui/ey;

.field private final n:Landroid/widget/ImageView;

.field private final o:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/ProgressBar;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/TextView;

.field private final x:Lcom/google/android/youtube/app/b/g;

.field private final y:Lcom/google/android/youtube/plus1/PlusOneButton;

.field private final z:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/adapter/cy;Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/b/g;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/youtube/core/a/e;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 158
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eu;->a:Lcom/google/android/youtube/app/adapter/cy;

    .line 159
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eu;->c:Lcom/google/android/youtube/app/adapter/cy;

    .line 161
    const-string v1, "activity cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    .line 162
    const-string v1, "analytics cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->e:Lcom/google/android/youtube/core/Analytics;

    .line 163
    const-string v1, "navigation cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/a;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->f:Lcom/google/android/youtube/app/a;

    .line 164
    const-string v1, "userAuth cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 165
    const-string v1, "gdataClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/b/al;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->h:Lcom/google/android/youtube/core/b/al;

    .line 166
    const-string v1, "imageClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/b/an;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->i:Lcom/google/android/youtube/core/b/an;

    .line 167
    const-string v1, "networkStatus cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/l;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->j:Lcom/google/android/youtube/core/utils/l;

    .line 169
    new-instance v1, Lcom/google/android/youtube/app/ui/ez;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/ez;-><init>(Lcom/google/android/youtube/app/ui/eu;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->l:Lcom/google/android/youtube/app/ui/ez;

    .line 170
    new-instance v1, Lcom/google/android/youtube/app/ui/ey;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/ey;-><init>(Lcom/google/android/youtube/app/ui/eu;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->m:Lcom/google/android/youtube/app/ui/ey;

    .line 172
    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->o:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    .line 173
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->o:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    const v2, 0x7f0800ec

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->n:Landroid/widget/ImageView;

    .line 175
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080046

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->p:Landroid/widget/TextView;

    .line 176
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->p:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08005a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->t:Landroid/widget/ImageView;

    .line 180
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->u:Landroid/view/View;

    .line 181
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->u:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->v:Landroid/widget/TextView;

    .line 185
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800ae

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    .line 186
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    if-eqz v1, :cond_126

    .line 187
    invoke-virtual {p4}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v6

    .line 188
    new-instance v1, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const-string v8, "Watch"

    move-object v2, p4

    move-object v3, p5

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 190
    invoke-virtual {p4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/eu;->H:I

    .line 191
    invoke-virtual {p4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/eu;->G:I

    .line 192
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {v2, p4}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 195
    :cond_126
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    .line 196
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_14a

    .line 197
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 198
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 203
    :cond_14a
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08005b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->E:Landroid/widget/ProgressBar;

    .line 206
    const-string v1, "plusOneClient cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/b/g;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->x:Lcom/google/android/youtube/app/b/g;

    .line 207
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/plus1/PlusOneButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    .line 209
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080094

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->z:Landroid/widget/TextView;

    .line 210
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800ed

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->q:Landroid/widget/TextView;

    .line 211
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800ea

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->w:Landroid/widget/TextView;

    .line 212
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0800eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->r:Landroid/widget/TextView;

    .line 213
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08018d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->s:Landroid/widget/ProgressBar;

    .line 215
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const-string v2, "like_button"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    .line 216
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1d6

    .line 217
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    :cond_1d6
    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const-string v2, "dislike_button"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    .line 220
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1ed

    .line 221
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    :cond_1ed
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    .line 227
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eu;->d(Z)V

    .line 229
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/eu;)Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->o:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/b/g;)Lcom/google/android/youtube/app/ui/eu;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 113
    const v1, 0x7f0400ce

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 114
    const v1, 0x7f0400cf

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 115
    const v1, 0x7f0400cd

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 118
    new-instance v2, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 119
    new-instance v3, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v3, v0}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 121
    new-instance v0, Lcom/google/android/youtube/app/ui/eu;

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eu;-><init>(Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/adapter/cy;Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/b/g;)V

    return-object v0
.end method

.method private a(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 364
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_27

    .line 365
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    const v4, 0x7f0b01d9

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3f

    .line 371
    add-int v3, p1, p2

    .line 374
    iget-object v4, p0, Lcom/google/android/youtube/app/ui/eu;->s:Landroid/widget/ProgressBar;

    if-nez v3, :cond_62

    move v0, v1

    :goto_32
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 375
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 376
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 378
    :cond_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->w:Landroid/widget/TextView;

    if-eqz v0, :cond_61

    .line 379
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->w:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    const v4, 0x7f0b01d8

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    iget v5, v5, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    :cond_61
    return-void

    :cond_62
    move v0, v2

    .line 374
    goto :goto_32
.end method

.method private a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->j:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v0

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->x:Lcom/google/android/youtube/app/b/g;

    invoke-interface {v0}, Lcom/google/android/youtube/app/b/g;->b()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 291
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 292
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eu;->x:Lcom/google/android/youtube/app/b/g;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->j:Lcom/google/android/youtube/core/utils/l;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/google/android/youtube/plus1/PlusOneButton;->a(Landroid/app/Activity;Lcom/google/android/youtube/plus1/a;Lcom/google/android/youtube/core/utils/l;Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v1}, Lcom/google/android/youtube/plus1/c;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setUri(Landroid/net/Uri;)V

    .line 299
    :goto_2f
    return-void

    .line 296
    :cond_30
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 297
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setVisibility(I)V

    goto :goto_2f
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/eu;)Lcom/google/android/youtube/app/adapter/cy;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->a:Lcom/google/android/youtube/app/adapter/cy;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/eu;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/eu;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private d(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 473
    if-eqz p1, :cond_11

    const v0, 0x7f020089

    .line 474
    :goto_6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 475
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->c:Lcom/google/android/youtube/app/adapter/cy;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    .line 476
    return-void

    .line 473
    :cond_11
    const v0, 0x7f020087

    goto :goto_6
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/eu;)Lcom/google/android/youtube/app/ui/ey;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->m:Lcom/google/android/youtube/app/ui/ey;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/eu;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->i:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/eu;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/eu;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/eu;)V
    .registers 4
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020187

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/eu;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->t:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .registers 7
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 469
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/ex;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_90

    .line 470
    :goto_15
    return-void

    .line 469
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    const v2, 0x7f0b018d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/eu;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_48
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    const v2, 0x7f0b018b

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/eu;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    const v1, 0x7f020084

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_7a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_data_90
    .packed-switch 0x1
        :pswitch_16
        :pswitch_48
        :pswitch_48
        :pswitch_7a
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/ui/di;)V
    .registers 4
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eu;->k:Lcom/google/android/youtube/app/ui/di;

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    if-eqz v0, :cond_11

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Landroid/view/View;Landroid/view/View;)V

    .line 236
    :cond_11
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 6
    .parameter

    .prologue
    .line 317
    if-eqz p1, :cond_6

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    if-nez v0, :cond_7

    .line 344
    :cond_6
    :goto_6
    return-void

    .line 321
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->i:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/ev;

    invoke-direct {v3, p0, p1}, Lcom/google/android/youtube/app/ui/ev;-><init>(Lcom/google/android/youtube/app/ui/eu;Lcom/google/android/youtube/core/model/Branding;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_6
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 286
    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eu;->a(Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 8
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 239
    const-string v0, "video can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->a:Lcom/google/android/youtube/app/adapter/cy;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    .line 243
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->p:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    if-eqz v0, :cond_9b

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->q:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    :goto_33
    iget v0, p1, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    iget v0, p1, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    iget v3, p1, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/app/ui/eu;->a(II)V

    .line 254
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_a1

    .line 255
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->v:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    :goto_47
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    if-eqz v0, :cond_50

    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 266
    :cond_50
    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    if-eqz v3, :cond_66

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    if-eqz v3, :cond_66

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 268
    :cond_66
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->z:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    const-string v4, "[\\r\\n]+"

    const-string v5, "\r\n\r\n"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->z:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->z:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_b8

    move v0, v1

    :goto_84
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->t:Landroid/widget/ImageView;

    if-eqz v0, :cond_9a

    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->h:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->ownerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eu;->d:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->l:Lcom/google/android/youtube/app/ui/ez;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 275
    :cond_9a
    return-void

    .line 249
    :cond_9b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_33

    .line 257
    :cond_a1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->v:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_47

    :cond_b8
    move v0, v2

    .line 269
    goto :goto_84
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 282
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eu;->a(Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 386
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    iget v3, v0, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    if-eqz p1, :cond_1a

    move v0, v1

    :goto_d
    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    iget v3, v3, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    if-eqz p1, :cond_1c

    :goto_14
    add-int v1, v3, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/ui/eu;->a(II)V

    .line 388
    return-void

    :cond_1a
    move v0, v2

    .line 386
    goto :goto_d

    :cond_1c
    move v2, v1

    goto :goto_14
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->c:Lcom/google/android/youtube/app/adapter/cy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    .line 487
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eu;->a(Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->u:Landroid/view/View;

    if-ne p1, v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_19

    .line 348
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "WatchChannel"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->f:Lcom/google/android/youtube/app/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eu;->I:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;)V

    .line 361
    :cond_18
    :goto_18
    return-void

    .line 351
    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->A:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_27

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->k:Lcom/google/android/youtube/app/ui/di;

    if-eqz v0, :cond_27

    .line 352
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->k:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->a()V

    goto :goto_18

    .line 353
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->B:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_35

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->k:Lcom/google/android/youtube/app/ui/di;

    if-eqz v0, :cond_35

    .line 354
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->k:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->b()V

    goto :goto_18

    .line 355
    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->D:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_3f

    .line 356
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    goto :goto_18

    .line 357
    :cond_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->p:Landroid/widget/TextView;

    if-ne p1, v0, :cond_18

    .line 359
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eu;->c:Lcom/google/android/youtube/app/adapter/cy;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cy;->l()Z

    move-result v0

    if-nez v0, :cond_50

    const/4 v0, 0x1

    :goto_4c
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eu;->d(Z)V

    goto :goto_18

    :cond_50
    const/4 v0, 0x0

    goto :goto_4c
.end method
