.class public final Lcom/google/android/youtube/app/ui/fe;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/c;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/bn;

.field private final c:Lcom/google/android/youtube/app/adapter/bc;

.field private final d:Lcom/google/android/youtube/app/ui/b;

.field private final e:Lcom/google/android/youtube/core/a/l;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/bc;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/b;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 118
    const-string v0, "pagingAdapter cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->c:Lcom/google/android/youtube/app/adapter/bc;

    .line 119
    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/bc;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bn;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->a:Lcom/google/android/youtube/app/adapter/bn;

    .line 120
    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/b;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->d:Lcom/google/android/youtube/app/ui/b;

    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->d:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/app/ui/c;)V

    .line 123
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->e:Lcom/google/android/youtube/core/a/l;

    .line 125
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->f:Ljava/lang/String;

    .line 126
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .registers 5
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->a:Lcom/google/android/youtube/app/adapter/bn;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bn;->clear()V

    .line 130
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->d:Lcom/google/android/youtube/app/ui/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fe;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/b;->a(Ljava/lang/String;Z)V

    .line 137
    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->d:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    .line 138
    return-void

    .line 133
    :cond_19
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ArtistBundle$Related;

    .line 134
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fe;->a:Lcom/google/android/youtube/app/adapter/bn;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/bn;->add(Ljava/lang/Object;)V

    goto :goto_1d
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->e:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    .line 154
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->c:Lcom/google/android/youtube/app/adapter/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bc;->c()Z

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->c:Lcom/google/android/youtube/app/adapter/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bc;->b()Z

    move-result v0

    if-nez v0, :cond_13

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fe;->d:Lcom/google/android/youtube/app/ui/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    .line 145
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/fe;->k()V

    .line 146
    return-void
.end method

.method public final h()V
    .registers 1

    .prologue
    .line 150
    return-void
.end method
