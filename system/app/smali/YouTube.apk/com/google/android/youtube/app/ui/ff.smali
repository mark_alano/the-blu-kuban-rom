.class final Lcom/google/android/youtube/app/ui/ff;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/cr;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bn;

.field final synthetic b:Lcom/google/android/youtube/core/Analytics;

.field final synthetic c:Lcom/google/android/youtube/app/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/adapter/bn;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ff;->a:Lcom/google/android/youtube/app/adapter/bn;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ff;->b:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ff;->c:Lcom/google/android/youtube/app/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/adapter/cq;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ff;->a:Lcom/google/android/youtube/app/adapter/bn;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/bn;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ArtistBundle$Related;

    .line 77
    if-eqz v0, :cond_1a

    .line 78
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ff;->b:Lcom/google/android/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->RelatedArtist:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    .line 79
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ff;->c:Lcom/google/android/youtube/app/a;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ArtistBundle$Related;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/app/m;->C:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)V

    .line 81
    :cond_1a
    return-void
.end method
