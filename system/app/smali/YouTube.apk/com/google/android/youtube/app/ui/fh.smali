.class public final Lcom/google/android/youtube/app/ui/fh;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bb;
.implements Lcom/google/android/youtube/app/ui/c;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/b;

.field private final c:Lcom/google/android/youtube/app/adapter/az;

.field private final d:Lcom/google/android/youtube/core/a/l;

.field private final e:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private g:Lcom/google/android/youtube/core/model/Video;

.field private h:Lcom/google/android/youtube/core/async/GDataRequest;

.field private i:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final j:I

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;ILcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/adapter/az;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 188
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p6, v0, v1

    const/4 v1, 0x1

    aput-object p5, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 191
    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/b;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/app/ui/c;)V

    .line 194
    const-string v0, "pagedOutline cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/az;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/app/adapter/bb;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    new-instance v1, Lcom/google/android/youtube/app/ui/az;

    invoke-direct {v1}, Lcom/google/android/youtube/app/ui/az;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/core/utils/p;)V

    .line 197
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->d:Lcom/google/android/youtube/core/a/l;

    .line 198
    const-string v0, "userAuth cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 199
    const-string v0, "gdataRequestFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 201
    iput p4, p0, Lcom/google/android/youtube/app/ui/fh;->j:I

    .line 202
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->k:Ljava/lang/String;

    .line 203
    return-void
.end method

.method private i()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 261
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_1b

    .line 262
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/az;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 270
    :goto_1a
    return-void

    .line 263
    :cond_1b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_2b

    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    new-array v1, v4, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/az;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_1a

    .line 265
    :cond_2b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_3b

    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    new-array v1, v4, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/az;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_1a

    .line 268
    :cond_3b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->a_()V

    goto :goto_1a
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 5
    .parameter

    .prologue
    .line 225
    if-eqz p1, :cond_12

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/youtube/app/ui/fh;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Ljava/lang/String;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 229
    :cond_12
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    if-eqz v0, :cond_22

    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 234
    :goto_21
    return-void

    .line 232
    :cond_22
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/fh;->i()V

    goto :goto_21
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_15

    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/fh;->j:I

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 240
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/fh;->i()V

    .line 242
    :cond_15
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 214
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    .line 215
    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->i:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 216
    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 217
    if-eqz p1, :cond_f

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->b()V

    .line 222
    :goto_e
    return-void

    .line 220
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->a_()V

    goto :goto_e
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_15

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/fh;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 248
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/fh;->i()V

    .line 250
    :cond_15
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/b;->b(Ljava/lang/String;Z)V

    .line 299
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->d:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    .line 311
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    .line 274
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    .line 287
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 315
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fh;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/b;->a(Ljava/lang/String;Z)V

    .line 291
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->k_()V

    .line 295
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->e()V

    .line 303
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->d()V

    .line 307
    return-void
.end method

.method public final i_()V
    .registers 4

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_15

    .line 254
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fh;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/fh;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 256
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/fh;->i()V

    .line 258
    :cond_15
    return-void
.end method

.method public final l_()V
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fh;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->e()V

    .line 278
    return-void
.end method
