.class public final Lcom/google/android/youtube/app/ui/i;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field private final c:Landroid/app/Activity;

.field private final d:Lcom/google/android/youtube/app/a;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/content/Context;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 54
    const-string v0, "category cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/i;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    .line 55
    const-string v0, "navigation cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/i;->d:Lcom/google/android/youtube/app/a;

    .line 56
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/i;->c:Landroid/app/Activity;

    .line 57
    const-string v0, "categoryOutline cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 60
    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cy;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080062

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 61
    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {v2, p5}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 62
    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const v1, 0x7f080063

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 65
    const v1, 0x7f080064

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {v2, p5}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 67
    if-eqz v0, :cond_69

    .line 68
    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    :cond_69
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Lcom/google/android/youtube/app/a;)Lcom/google/android/youtube/app/ui/i;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 41
    const v1, 0x7f04001b

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 43
    new-instance v0, Lcom/google/android/youtube/app/ui/i;

    move-object v2, p1

    move-object v3, p0

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/i;-><init>(Lcom/google/android/youtube/app/adapter/cy;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 78
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/i;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 80
    sget-object v2, Lcom/google/android/youtube/app/ui/j;->a:[I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/i;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_40

    move-object v0, v1

    .line 98
    :goto_17
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/i;->d:Lcom/google/android/youtube/app/a;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/i;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V

    .line 99
    return-void

    .line 82
    :pswitch_1f
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_17

    .line 85
    :pswitch_26
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_17

    .line 88
    :pswitch_2d
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->j(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_17

    .line 92
    :pswitch_36
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_17

    .line 95
    :pswitch_3d
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;->r:Landroid/net/Uri;

    goto :goto_17

    .line 80
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_26
        :pswitch_2d
        :pswitch_36
        :pswitch_3d
    .end packed-switch
.end method
