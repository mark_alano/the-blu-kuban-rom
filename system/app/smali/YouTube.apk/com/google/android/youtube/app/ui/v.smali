.class final Lcom/google/android/youtube/app/ui/v;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/s;

.field private final b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/s;)V
    .registers 3
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/v;->a:Lcom/google/android/youtube/app/ui/s;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 233
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->b:Ljava/util/Map;

    .line 234
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/v;->getCount()I

    move-result v0

    .line 250
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/ui/w;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, p0, p1, p2, v4}, Lcom/google/android/youtube/app/ui/w;-><init>(Lcom/google/android/youtube/app/ui/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/v;->notifyDataSetChanged()V

    .line 252
    return v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 245
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 258
    if-nez p2, :cond_36

    .line 259
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->a:Lcom/google/android/youtube/app/ui/s;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/s;->d(Lcom/google/android/youtube/app/ui/s;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_32

    const v0, 0x7f04009a

    :goto_15
    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 261
    new-instance v0, Lcom/google/android/youtube/app/ui/x;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/app/ui/x;-><init>(Lcom/google/android/youtube/app/ui/v;Landroid/view/View;)V

    .line 262
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 266
    :goto_22
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/w;

    .line 267
    if-nez v0, :cond_3e

    move-object p2, v2

    .line 297
    :cond_31
    :goto_31
    return-object p2

    .line 259
    :cond_32
    const v0, 0x7f040024

    goto :goto_15

    .line 264
    :cond_36
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/x;

    move-object v1, v0

    goto :goto_22

    .line 270
    :cond_3e
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->a:Landroid/widget/TextView;

    if-eqz v3, :cond_56

    .line 271
    iget-object v3, v0, Lcom/google/android/youtube/app/ui/w;->a:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_87

    .line 272
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/youtube/app/ui/w;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    :cond_56
    :goto_56
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->b:Landroid/widget/TextView;

    if-eqz v3, :cond_6e

    .line 280
    iget-object v3, v0, Lcom/google/android/youtube/app/ui/w;->b:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_92

    .line 281
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->b:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/youtube/app/ui/w;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    :cond_6e
    :goto_6e
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->c:Landroid/widget/ImageView;

    if-eqz v3, :cond_31

    .line 289
    iget-object v3, v0, Lcom/google/android/youtube/app/ui/w;->c:Ljava/lang/Integer;

    if-eqz v3, :cond_9d

    .line 290
    iget-object v2, v1, Lcom/google/android/youtube/app/ui/x;->c:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/w;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 291
    iget-object v0, v1, Lcom/google/android/youtube/app/ui/x;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_31

    .line 275
    :cond_87
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_56

    .line 284
    :cond_92
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/x;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6e

    .line 293
    :cond_9d
    iget-object v0, v1, Lcom/google/android/youtube/app/ui/x;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 294
    iget-object v0, v1, Lcom/google/android/youtube/app/ui/x;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_31
.end method
