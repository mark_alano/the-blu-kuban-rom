.class public Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;
.super Lcom/google/android/youtube/athome/server/SafeOverlay;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ControllerOverlay;


# instance fields
.field private final a:Landroid/widget/RelativeLayout;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/animation/Animation;

.field private final h:Landroid/view/animation/Animation;

.field private final i:Landroid/os/Handler;

.field private final j:[Landroid/view/View;

.field private final k:[Landroid/view/View;

.field private l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

.field private m:Z

.field private n:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v5, 0x3e8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/youtube/athome/server/SafeOverlay;-><init>(Landroid/content/Context;)V

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f04

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 67
    const v0, 0x7f08002b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f08002d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->b:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->c:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f08002c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->d:Landroid/widget/ImageView;

    .line 72
    const v0, 0x7f080030

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    .line 73
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    .line 75
    const/high16 v0, 0x10a

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->g:Landroid/view/animation/Animation;

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->g:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/athome/server/b;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/athome/server/b;-><init>(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 92
    const v0, 0x10a0001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->h:Landroid/view/animation/Animation;

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->h:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/athome/server/c;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/athome/server/c;-><init>(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 108
    new-instance v0, Lcom/google/android/youtube/athome/server/d;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/athome/server/d;-><init>(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)V

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->i:Landroid/os/Handler;

    .line 117
    sget-object v0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;->HIDDEN:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->j:[Landroid/view/View;

    .line 123
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->k:[Landroid/view/View;

    .line 124
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)Landroid/widget/RelativeLayout;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;)Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    return-object p1
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/youtube/athome/server/e;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    invoke-virtual {v1}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_32

    .line 170
    :goto_d
    return-void

    .line 151
    :pswitch_e
    if-eqz p1, :cond_18

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_d

    .line 154
    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 155
    sget-object v0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;->SHOWN:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    goto :goto_d

    .line 160
    :pswitch_23
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 161
    sget-object v0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;->SHOWN:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    .line 162
    :pswitch_2d
    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l()V

    goto :goto_d

    .line 149
    nop

    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_e
        :pswitch_23
        :pswitch_2d
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)V
    .registers 1
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;)V
    .registers 3
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 183
    sget-object v0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;->HIDDEN:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->l:Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay$WatchInfoState;

    .line 184
    return-void
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->i:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 192
    iget-boolean v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->m:Z

    if-nez v0, :cond_11

    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->i:Landroid/os/Handler;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 195
    :cond_11
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 127
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->k()V

    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 313
    return-void
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x2

    .line 131
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->d:Landroid/widget/ImageView;

    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->n:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    if-eq v0, v1, :cond_22

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->m:Z

    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a(Z)V

    .line 223
    :goto_21
    return-void

    .line 221
    :cond_22
    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->k()V

    goto :goto_21
.end method

.method protected final d()[Landroid/view/View;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->k:[Landroid/view/View;

    return-object v0
.end method

.method protected final e()[Landroid/view/View;
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->j:[Landroid/view/View;

    return-object v0
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 286
    return-void
.end method

.method public final g()V
    .registers 1

    .prologue
    .line 289
    return-void
.end method

.method public final h()V
    .registers 1

    .prologue
    .line 292
    return-void
.end method

.method public final i()V
    .registers 1

    .prologue
    .line 304
    return-void
.end method

.method public final j()V
    .registers 1

    .prologue
    .line 316
    return-void
.end method

.method public setCcEnabled(Z)V
    .registers 2
    .parameter

    .prologue
    .line 310
    return-void
.end method

.method public setFullscreen(Z)V
    .registers 2
    .parameter

    .prologue
    .line 274
    return-void
.end method

.method public setHQ(Z)V
    .registers 2
    .parameter

    .prologue
    .line 268
    return-void
.end method

.method public setHQisHD(Z)V
    .registers 2
    .parameter

    .prologue
    .line 271
    return-void
.end method

.method public setHasCc(Z)V
    .registers 2
    .parameter

    .prologue
    .line 307
    return-void
.end method

.method public setHasNext(Z)V
    .registers 2
    .parameter

    .prologue
    .line 280
    return-void
.end method

.method public setHasPrevious(Z)V
    .registers 2
    .parameter

    .prologue
    .line 283
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/k;)V
    .registers 2
    .parameter

    .prologue
    .line 262
    return-void
.end method

.method public setLoading()V
    .registers 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 228
    return-void
.end method

.method public setPlaying()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->d:Landroid/widget/ImageView;

    const v1, 0x7f02006c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->n:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    if-eq v0, v1, :cond_22

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->m:Z

    .line 207
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->a(Z)V

    .line 211
    :goto_21
    return-void

    .line 209
    :cond_22
    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->k()V

    goto :goto_21
.end method

.method public setPortrait(Z)V
    .registers 2
    .parameter

    .prologue
    .line 301
    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .registers 2
    .parameter

    .prologue
    .line 295
    return-void
.end method

.method public setShowFullscreen(Z)V
    .registers 2
    .parameter

    .prologue
    .line 277
    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V
    .registers 2
    .parameter

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->n:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 199
    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .registers 2
    .parameter

    .prologue
    .line 265
    return-void
.end method

.method public setTimes(III)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 298
    return-void
.end method

.method public setVideo(Lcom/google/android/youtube/core/model/Video;)V
    .registers 4
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_22

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->n:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    if-eq v0, v1, :cond_23

    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :cond_22
    :goto_22
    return-void

    .line 143
    :cond_23
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeControllerOverlay;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_22
.end method
