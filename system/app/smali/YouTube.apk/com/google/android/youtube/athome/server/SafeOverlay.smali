.class public abstract Lcom/google/android/youtube/athome/server/SafeOverlay;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected abstract d()[Landroid/view/View;
.end method

.method protected abstract e()[Landroid/view/View;
.end method

.method protected onLayout(ZIIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const-wide v5, 0x3fb3333333333333L

    .line 29
    if-eqz p1, :cond_3c

    .line 30
    sub-int v1, p4, p2

    .line 31
    sub-int v2, p5, p3

    .line 33
    int-to-double v3, v1

    mul-double/2addr v3, v5

    double-to-int v3, v3

    .line 34
    int-to-double v1, v2

    mul-double/2addr v1, v5

    double-to-int v2, v1

    .line 36
    invoke-virtual {p0}, Lcom/google/android/youtube/athome/server/SafeOverlay;->e()[Landroid/view/View;

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_18
    if-ge v1, v5, :cond_22

    aget-object v6, v4, v1

    .line 37
    invoke-virtual {v6, v3, v2, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_18

    .line 40
    :cond_22
    invoke-virtual {p0}, Lcom/google/android/youtube/athome/server/SafeOverlay;->d()[Landroid/view/View;

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_28
    if-ge v1, v5, :cond_3c

    aget-object v6, v4, v1

    .line 41
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 43
    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 45
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_28

    .line 49
    :cond_3c
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 50
    return-void
.end method
