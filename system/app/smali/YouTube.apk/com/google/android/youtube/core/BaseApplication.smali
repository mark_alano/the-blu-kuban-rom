.class public abstract Lcom/google/android/youtube/core/BaseApplication;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a;
.implements Lcom/google/android/youtube/core/async/b;
.implements Lcom/google/android/youtube/core/async/bq;
.implements Lcom/google/android/youtube/core/e;
.implements Lcom/google/android/youtube/core/player/ay;
.implements Lcom/google/android/youtube/core/utils/m;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/youtube/core/utils/aa;

.field private d:Landroid/os/Handler;

.field private e:Ljava/util/concurrent/Executor;

.field private f:Ljava/util/concurrent/Executor;

.field private g:Ljava/util/concurrent/Executor;

.field private h:Lorg/apache/http/client/HttpClient;

.field private i:Lorg/apache/http/client/HttpClient;

.field private j:Lcom/google/android/youtube/core/converter/l;

.field private k:Landroid/content/SharedPreferences;

.field private l:Lcom/google/android/youtube/core/j;

.field private m:Lcom/google/android/youtube/core/player/ax;

.field private n:Lcom/google/android/youtube/core/player/e;

.field private o:Lcom/google/android/youtube/core/utils/l;

.field private p:Lcom/google/android/youtube/core/d;

.field private q:Lcom/google/android/youtube/core/async/a;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/Analytics;

.field private t:Lcom/google/android/youtube/core/utils/SafeSearch;

.field private u:Z

.field private v:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/BaseApplication;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(III)Ljava/util/concurrent/Executor;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 402
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/google/android/youtube/core/utils/t;

    const/16 v1, 0xa

    invoke-direct {v7, v1}, Lcom/google/android/youtube/core/utils/t;-><init>(I)V

    move v1, p0

    move v2, p0

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v0
.end method

.method private d()Lcom/google/android/youtube/core/async/br;
    .registers 3

    .prologue
    .line 352
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_10

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_16

    .line 353
    :cond_10
    new-instance v0, Lcom/google/android/youtube/core/async/ag;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/ag;-><init>()V

    .line 359
    :goto_15
    return-object v0

    .line 357
    :cond_16
    :try_start_16
    const-string v0, "com.google.android.youtube.coreicecream.c"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 359
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/br;
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_22} :catch_23

    goto :goto_15

    .line 360
    :catch_23
    move-exception v0

    .line 361
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract E()Lcom/google/android/youtube/core/c;
.end method

.method public final F()Lcom/google/android/youtube/core/Analytics;
    .registers 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->s:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method public final G()Lcom/google/android/youtube/core/utils/aa;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->c:Lcom/google/android/youtube/core/utils/aa;

    return-object v0
.end method

.method public final H()Ljava/util/concurrent/Executor;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->e:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final I()Ljava/util/concurrent/Executor;
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final J()Ljava/util/concurrent/Executor;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final K()Lorg/apache/http/client/HttpClient;
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->h:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public final L()Lorg/apache/http/client/HttpClient;
    .registers 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->i:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public final M()Lcom/google/android/youtube/core/converter/l;
    .registers 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->j:Lcom/google/android/youtube/core/converter/l;

    return-object v0
.end method

.method public final N()Lcom/google/android/youtube/core/d;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->p:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method public final O()Landroid/content/SharedPreferences;
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final P()Lcom/google/android/youtube/core/async/a;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    return-object v0
.end method

.method public final Q()Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method public final R()Lcom/google/android/youtube/core/utils/SafeSearch;
    .registers 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    return-object v0
.end method

.method public final S()Lcom/google/android/youtube/core/player/ax;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->m:Lcom/google/android/youtube/core/player/ax;

    return-object v0
.end method

.method public final T()Lcom/google/android/youtube/core/player/e;
    .registers 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->n:Lcom/google/android/youtube/core/player/e;

    return-object v0
.end method

.method public final U()Lcom/google/android/youtube/core/utils/l;
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .registers 4

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    if-nez v0, :cond_15

    .line 268
    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_10
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_10} :catch_18

    move-result-object v0

    .line 273
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    .line 275
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    :goto_17
    return-object v0

    .line 269
    :catch_18
    move-exception v0

    .line 270
    const-string v1, "could not retrieve application version name"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 271
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    goto :goto_17
.end method

.method public final W()Ljava/lang/String;
    .registers 4

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 282
    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_10
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_10} :catch_1c

    move-result-object v0

    .line 287
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    .line 289
    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    :goto_1b
    return-object v0

    .line 283
    :catch_1c
    move-exception v0

    .line 284
    const-string v1, "could not retrieve application version code"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 285
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    goto :goto_1b
.end method

.method public final X()Z
    .registers 2

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    return v0
.end method

.method public final Y()Lcom/google/android/youtube/core/j;
    .registers 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Lcom/google/android/youtube/core/j;

    return-object v0
.end method

.method protected a()V
    .registers 11

    .prologue
    const/16 v5, 0x3c

    const/16 v4, 0xa

    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 108
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;)V

    .line 110
    const-string v1, "youtube"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/BaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    .line 111
    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    const-string v2, "version"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->V()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    move v0, v9

    :cond_29
    iput-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    .line 114
    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    if-eqz v0, :cond_4a

    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "version"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_key"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 121
    :cond_4a
    new-instance v0, Lcom/google/android/youtube/core/utils/aa;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->c:Lcom/google/android/youtube/core/utils/aa;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->d:Landroid/os/Handler;

    .line 123
    new-instance v0, Lcom/google/android/youtube/core/b;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/b;-><init>(Lcom/google/android/youtube/core/BaseApplication;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->e:Ljava/util/concurrent/Executor;

    .line 124
    const/16 v0, 0x10

    invoke-static {v0, v5, v4}, Lcom/google/android/youtube/core/BaseApplication;->a(III)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    .line 125
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, v5, v4}, Lcom/google/android/youtube/core/BaseApplication;->a(III)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->V()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->E()Lcom/google/android/youtube/core/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->i(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->h:Lorg/apache/http/client/HttpClient;

    .line 129
    new-instance v1, Lcom/google/android/youtube/core/utils/ac;

    invoke-direct {v1}, Lcom/google/android/youtube/core/utils/ac;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;Lorg/apache/http/client/RedirectHandler;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->i:Lorg/apache/http/client/HttpClient;

    .line 130
    invoke-static {}, Lcom/google/android/youtube/core/converter/l;->a()Lcom/google/android/youtube/core/converter/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->j:Lcom/google/android/youtube/core/converter/l;

    .line 132
    new-instance v0, Lcom/google/android/youtube/core/async/r;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/async/r;-><init>(Landroid/accounts/AccountManager;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    .line 133
    new-instance v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/google/android/youtube/core/BaseApplication;->d()Lcom/google/android/youtube/core/async/br;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;-><init>(Lcom/google/android/youtube/core/async/a;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/br;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 136
    new-instance v0, Lcom/google/android/youtube/core/utils/SafeSearch;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Landroid/content/SharedPreferences;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/utils/SafeSearch;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->E()Lcom/google/android/youtube/core/c;

    move-result-object v1

    .line 141
    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->r()Z

    move-result v0

    if-eqz v0, :cond_18b

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_18b

    .line 142
    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->v()Ljava/lang/String;

    move-result-object v0

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->V()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_174

    const-string v0, ""

    :goto_f9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 145
    new-instance v0, Lcom/google/android/youtube/core/b/h;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->s()I

    move-result v6

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->t()I

    move-result v7

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->u()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/b/h;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    move-object v1, v0

    .line 158
    :goto_11e
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->h(Landroid/content/Context;)Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Lcom/google/android/youtube/core/j;

    .line 159
    new-instance v2, Lcom/google/android/youtube/core/utils/g;

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/BaseApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/core/utils/g;-><init>(Landroid/net/ConnectivityManager;)V

    iput-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    .line 161
    new-instance v0, Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;-><init>(Lcom/google/android/youtube/core/b/b;Lcom/google/android/youtube/core/utils/l;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->s:Lcom/google/android/youtube/core/Analytics;

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->e()Z

    move-result v0

    if-eqz v0, :cond_15b

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.youtube.ManageNetworkUsageActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    if-eq v2, v9, :cond_15b

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-virtual {v1, v0, v9, v9}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 165
    :cond_15b
    new-instance v0, Lcom/google/android/youtube/core/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/d;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/l;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->p:Lcom/google/android/youtube/core/d;

    .line 166
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->b()Lcom/google/android/youtube/core/player/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->m:Lcom/google/android/youtube/core/player/ax;

    .line 167
    new-instance v0, Lcom/google/android/youtube/core/player/e;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->c:Lcom/google/android/youtube/core/utils/aa;

    invoke-direct {v0, v1, v9}, Lcom/google/android/youtube/core/player/e;-><init>(Lcom/google/android/youtube/core/utils/d;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->n:Lcom/google/android/youtube/core/player/e;

    .line 168
    return-void

    .line 143
    :cond_174
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_f9

    .line 155
    :cond_18b
    new-instance v0, Lcom/google/android/youtube/core/b/ak;

    invoke-direct {v0}, Lcom/google/android/youtube/core/b/ak;-><init>()V

    move-object v1, v0

    goto :goto_11e
.end method

.method protected b()Lcom/google/android/youtube/core/player/ax;
    .registers 7

    .prologue
    .line 171
    new-instance v0, Lcom/google/android/youtube/core/player/q;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/l;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Lcom/google/android/youtube/core/j;

    invoke-interface {v2, p0}, Lcom/google/android/youtube/core/j;->b(Landroid/content/Context;)Z

    move-result v2

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->E()Lcom/google/android/youtube/core/c;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/core/c;->w()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/q;-><init>(Lcom/google/android/youtube/core/utils/l;ZZZZ)V

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 409
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 311
    const-string v0, "?"

    return-object v0
.end method

.method public final onCreate()V
    .registers 2

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 93
    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->v:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->v:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->a()V

    .line 94
    :cond_d
    return-void
.end method
