.class public final Lcom/google/android/youtube/core/a/c;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/ListAdapter;

.field private final c:[Lcom/google/android/youtube/core/a/g;

.field private final d:Landroid/database/DataSetObserver;


# direct methods
.method public varargs constructor <init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    .line 51
    const-string v0, "adapter cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    .line 52
    if-eqz p3, :cond_14

    array-length v0, p3

    if-nez v0, :cond_37

    .line 53
    :cond_14
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    new-array p3, v0, [Lcom/google/android/youtube/core/a/g;

    move v0, v1

    .line 54
    :goto_1b
    array-length v1, p3

    if-ge v0, v1, :cond_54

    .line 55
    new-instance v1, Lcom/google/android/youtube/core/a/g;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Auto "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    aput-object v1, p3, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 58
    :cond_37
    array-length v0, p3

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v3

    if-ne v0, v3, :cond_65

    move v0, v2

    :goto_3f
    const-string v3, "viewTypes array size must match adapter\'s view type count"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 60
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_67

    :goto_4f
    const-string v0, "viewTypes must not contain null"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 63
    :cond_54
    iput-object p3, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    .line 64
    new-instance v0, Lcom/google/android/youtube/core/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/a/d;-><init>(Lcom/google/android/youtube/core/a/c;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/c;->d:Landroid/database/DataSetObserver;

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->d:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 77
    return-void

    :cond_65
    move v0, v1

    .line 58
    goto :goto_3f

    :cond_67
    move v2, v1

    .line 60
    goto :goto_4f
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .registers 4
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 133
    if-gez v0, :cond_b

    sget-object v0, Lcom/google/android/youtube/core/a/c;->b:Lcom/google/android/youtube/core/a/g;

    :goto_a
    return-object v0

    :cond_b
    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    aget-object v0, v1, v0

    goto :goto_a
.end method

.method protected final a(Ljava/util/Set;)V
    .registers 4
    .parameter

    .prologue
    .line 115
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    array-length v1, v1

    if-ge v0, v1, :cond_10

    .line 116
    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    aget-object v1, v1, v0

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :cond_10
    return-void
.end method

.method public final b()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)J
    .registers 4
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method protected final d()V
    .registers 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/c;->k()V

    .line 103
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method protected final e()V
    .registers 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/c;->k()V

    .line 111
    return-void
.end method
