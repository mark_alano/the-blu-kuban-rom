.class public final Lcom/google/android/youtube/core/a/i;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a/f;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Z

.field private c:Lcom/google/android/youtube/core/a/e;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/i;-><init>(Z)V

    .line 38
    return-void
.end method

.method private constructor <init>(Z)V
    .registers 5
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/a/e;->b:Lcom/google/android/youtube/core/a/g;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    .line 50
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 66
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    if-nez v0, :cond_37

    move v0, v1

    :goto_6
    const-string v2, "setViewTypes called after view types are finalized"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 68
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/g;

    .line 69
    if-eqz v0, :cond_f

    iget-object v3, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 72
    iget-object v3, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 66
    :cond_37
    const/4 v0, 0x0

    goto :goto_6

    .line 75
    :cond_39
    iput-boolean v1, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/a/e;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-ne v0, p1, :cond_7

    .line 120
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->notifyDataSetChanged()V

    .line 122
    :cond_7
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/google/android/youtube/core/a/e;)V
    .registers 4
    .parameter

    .prologue
    .line 95
    if-eqz p1, :cond_30

    .line 96
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/android/youtube/core/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_40

    :cond_c
    const/4 v0, 0x1

    :goto_d
    const-string v1, "outline must have stable IDs as required by this OutlinerAdapter"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 99
    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/a/e;->a(Ljava/util/Set;)V

    .line 100
    iget-boolean v1, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    if-eqz v1, :cond_42

    .line 101
    iget-object v1, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    const-string v1, "outline must not use unknown view types"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 106
    :goto_2d
    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/a/e;->a(Lcom/google/android/youtube/core/a/f;)V

    .line 108
    :cond_30
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-eqz v0, :cond_3a

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/e;->a(Lcom/google/android/youtube/core/a/f;)V

    .line 111
    :cond_3a
    iput-object p1, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->notifyDataSetChanged()V

    .line 116
    return-void

    .line 96
    :cond_40
    const/4 v0, 0x0

    goto :goto_d

    .line 104
    :cond_42
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/i;->a(Ljava/util/Set;)V

    goto :goto_2d
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v0

    goto :goto_5
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_9

    .line 159
    const-wide/16 v0, -0x1

    .line 161
    :goto_8
    return-wide v0

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->c(I)J

    move-result-wide v0

    goto :goto_8
.end method

.method public final getItemViewType(I)I
    .registers 6
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->a(I)Lcom/google/android/youtube/core/a/g;

    move-result-object v1

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 139
    if-nez v0, :cond_2f

    .line 140
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown view type \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_2f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .registers 3

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    const-string v1, "OutlinerAdapter used before view types are known"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 132
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .registers 2

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->d(I)Z

    move-result v0

    return v0
.end method
