.class public Lcom/google/android/youtube/core/a/l;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a/f;


# instance fields
.field private final a:[Lcom/google/android/youtube/core/a/e;

.field private final c:[I

.field private final d:Z


# direct methods
.method public varargs constructor <init>([Lcom/google/android/youtube/core/a/e;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    .line 41
    const-string v0, "components cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/a/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    .line 42
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    .line 43
    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/core/a/l;->a(IZ)V

    .line 45
    array-length v4, p1

    move v3, v2

    move v0, v1

    :goto_1a
    if-ge v3, v4, :cond_2f

    aget-object v5, p1, v3

    .line 46
    invoke-virtual {v5, p0}, Lcom/google/android/youtube/core/a/e;->a(Lcom/google/android/youtube/core/a/f;)V

    .line 47
    if-eqz v0, :cond_2d

    invoke-virtual {v5}, Lcom/google/android/youtube/core/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_2d

    move v0, v1

    .line 45
    :goto_2a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    :cond_2d
    move v0, v2

    .line 47
    goto :goto_2a

    .line 49
    :cond_2f
    iput-boolean v0, p0, Lcom/google/android/youtube/core/a/l;->d:Z

    .line 50
    return-void
.end method

.method private a(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    .line 61
    :goto_4
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    array-length v1, v1

    if-ge p1, v1, :cond_21

    .line 62
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, p1

    .line 65
    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    if-nez p2, :cond_1a

    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    aget v1, v1, p1

    if-eq v0, v1, :cond_21

    .line 68
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    aput v0, v1, p1

    .line 61
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    .line 74
    :cond_21
    return-void
.end method

.method private e(I)I
    .registers 4
    .parameter

    .prologue
    .line 84
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    goto :goto_3
.end method

.method private f(I)I
    .registers 4
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 93
    if-ltz v0, :cond_11

    .line 97
    :cond_8
    add-int/lit8 v0, v0, 0x1

    .line 98
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->c:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_8

    .line 103
    :goto_10
    return v0

    :cond_11
    xor-int/lit8 v0, v0, -0x1

    goto :goto_10
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    if-ltz p1, :cond_21

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->a()I

    move-result v0

    if-ge p1, v0, :cond_21

    const/4 v0, 0x1

    :goto_9
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->f(I)I

    move-result v0

    .line 159
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    .line 160
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 156
    :cond_21
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .registers 4
    .parameter

    .prologue
    .line 113
    if-ltz p1, :cond_21

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->a()I

    move-result v0

    if-ge p1, v0, :cond_21

    const/4 v0, 0x1

    :goto_9
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->f(I)I

    move-result v0

    .line 116
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    .line 117
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/a/e;->a(I)Lcom/google/android/youtube/core/a/g;

    move-result-object v0

    return-object v0

    .line 113
    :cond_21
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final a(Lcom/google/android/youtube/core/a/e;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    move v0, v1

    .line 173
    :goto_2
    iget-object v2, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_13

    .line 174
    iget-object v2, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v2, v2, v0

    if-ne p1, v2, :cond_14

    .line 175
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/a/l;->a(IZ)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->k()V

    .line 180
    :cond_13
    return-void

    .line 173
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method protected final a(Ljava/util/Set;)V
    .registers 4
    .parameter

    .prologue
    .line 122
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_10

    .line 123
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/a/e;->a(Ljava/util/Set;)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 125
    :cond_10
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 129
    if-ltz p1, :cond_21

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->a()I

    move-result v0

    if-ge p1, v0, :cond_21

    const/4 v0, 0x1

    :goto_9
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 131
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->f(I)I

    move-result v0

    .line 132
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    .line 133
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/a/e;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 129
    :cond_21
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final c(I)J
    .registers 4
    .parameter

    .prologue
    .line 143
    if-ltz p1, :cond_25

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->a()I

    move-result v0

    if-ge p1, v0, :cond_25

    const/4 v0, 0x1

    :goto_9
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 145
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/l;->d:Z

    if-eqz v0, :cond_27

    .line 146
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->f(I)I

    move-result v0

    .line 147
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    .line 148
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/a/e;->c(I)J

    move-result-wide v0

    .line 150
    :goto_24
    return-wide v0

    .line 143
    :cond_25
    const/4 v0, 0x0

    goto :goto_9

    .line 150
    :cond_27
    int-to-long v0, p1

    goto :goto_24
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/l;->d:Z

    return v0
.end method

.method public d(I)Z
    .registers 4
    .parameter

    .prologue
    .line 165
    if-ltz p1, :cond_21

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/l;->a()I

    move-result v0

    if-ge p1, v0, :cond_21

    const/4 v0, 0x1

    :goto_9
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/l;->f(I)I

    move-result v0

    .line 168
    iget-object v1, p0, Lcom/google/android/youtube/core/a/l;->a:[Lcom/google/android/youtube/core/a/e;

    aget-object v1, v1, v0

    .line 169
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;->e(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/a/e;->d(I)Z

    move-result v0

    return v0

    .line 165
    :cond_21
    const/4 v0, 0x0

    goto :goto_9
.end method
