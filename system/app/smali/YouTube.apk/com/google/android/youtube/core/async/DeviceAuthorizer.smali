.class public final Lcom/google/android/youtube/core/async/DeviceAuthorizer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/aj;

.field private final b:Landroid/content/SharedPreferences;

.field private volatile c:Z

.field private final d:Landroid/os/ConditionVariable;

.field private volatile e:Lcom/google/android/youtube/core/model/d;

.field private volatile f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/b/aj;Landroid/content/SharedPreferences;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aj;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->a:Lcom/google/android/youtube/core/b/aj;

    .line 40
    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->b:Landroid/content/SharedPreferences;

    .line 42
    const-string v0, "device_id"

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "device_key"

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_40

    if-eqz v0, :cond_40

    invoke-static {v0, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    new-instance v0, Lcom/google/android/youtube/core/model/d;

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/core/model/d;-><init>(Ljava/lang/String;[B)V

    :goto_32
    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    if-nez v0, :cond_3d

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1, v4}, Landroid/os/ConditionVariable;-><init>(Z)V

    :cond_3d
    iput-object v1, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->d:Landroid/os/ConditionVariable;

    .line 44
    return-void

    :cond_40
    move-object v0, v1

    .line 42
    goto :goto_32
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    if-eqz v1, :cond_c

    .line 56
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/d;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_b
    return-object v0

    .line 59
    :cond_c
    const/4 v1, 0x0

    .line 61
    monitor-enter p0

    .line 62
    :try_start_e
    iget-boolean v2, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->c:Z

    if-nez v2, :cond_39

    .line 63
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->c:Z

    .line 65
    iget-object v1, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 67
    :goto_1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_e .. :try_end_1b} :catchall_29

    .line 69
    if-eqz v0, :cond_2c

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->a:Lcom/google/android/youtube/core/b/aj;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/b/aj;->a(Lcom/google/android/youtube/core/async/l;)V

    .line 75
    :goto_22
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    if-eqz v0, :cond_32

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    throw v0

    .line 67
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0

    .line 72
    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    goto :goto_22

    .line 78
    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/d;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    :cond_39
    move v0, v1

    goto :goto_1a
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    new-instance v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->c:Z

    const-string v0, "device registration failed"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 24
    check-cast p2, Lcom/google/android/youtube/core/model/d;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->e:Lcom/google/android/youtube/core/model/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/model/d;->a(Lcom/google/android/youtube/core/model/d;Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->c:Z

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    return-void
.end method
