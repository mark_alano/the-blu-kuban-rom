.class public final Lcom/google/android/youtube/core/async/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/youtube/core/b/al;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    if-nez p2, :cond_14

    .line 41
    invoke-interface {p0}, Lcom/google/android/youtube/core/b/al;->h()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    .line 42
    invoke-interface {p0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    .line 49
    :goto_e
    new-instance v2, Lcom/google/android/youtube/core/async/a/f;

    invoke-direct {v2, v0, v1, p3}, Lcom/google/android/youtube/core/async/a/f;-><init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/GDataRequest;I)V

    return-object v2

    .line 44
    :cond_14
    invoke-interface {p0}, Lcom/google/android/youtube/core/b/al;->i()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    .line 45
    invoke-interface {p0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    goto :goto_e
.end method

.method public static a(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 169
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 170
    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/youtube/core/async/a/d;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/google/android/youtube/core/async/a/d;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/model/UserAuth;I)V

    return-object v0
.end method
