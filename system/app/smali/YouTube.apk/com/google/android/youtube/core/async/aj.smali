.class public final Lcom/google/android/youtube/core/async/aj;
.super Lcom/google/android/youtube/core/async/o;
.source "SourceFile"


# instance fields
.field private final a:Lorg/apache/http/client/HttpClient;

.field private b:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p2, p3}, Lcom/google/android/youtube/core/async/o;-><init>(Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/b;)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/aj;->b:Z

    .line 50
    const-string v0, "httpClient may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/aj;->a:Lorg/apache/http/client/HttpClient;

    .line 51
    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;)V
    .registers 2
    .parameter

    .prologue
    .line 118
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_9

    .line 120
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 122
    :cond_9
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;Ljava/lang/Exception;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p2, Lorg/apache/http/client/methods/HttpUriRequest;

    instance-of v0, p4, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_4e

    move-object v0, p4

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Http error: request=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Http error: status=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] msg=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :cond_4e
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/o;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;Ljava/lang/Exception;)V

    return-void
.end method

.method protected final synthetic b(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 31
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v0, "request can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/aj;->b:Z

    if-eqz v0, :cond_11

    invoke-static {p1}, Lcom/google/android/youtube/core/async/ak;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    :cond_11
    const/4 v1, 0x0

    :try_start_12
    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1b
    .catchall {:try_start_12 .. :try_end_1b} :catchall_3e
    .catch Ljava/lang/IllegalStateException; {:try_start_12 .. :try_end_1b} :catch_28
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_1b} :catch_39
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_1b} :catch_45

    if-eqz v1, :cond_20

    :try_start_1d
    invoke-static {v1}, Lcom/google/android/youtube/core/async/aj;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_21

    :cond_20
    :goto_20
    return-void

    :catch_21
    move-exception v0

    const-string v1, "Error consuming content response"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_20

    :catch_28
    move-exception v0

    :try_start_29
    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_3e

    if-eqz v1, :cond_20

    :try_start_2e
    invoke-static {v1}, Lcom/google/android/youtube/core/async/aj;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_32

    goto :goto_20

    :catch_32
    move-exception v0

    const-string v1, "Error consuming content response"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_20

    :catch_39
    move-exception v0

    :try_start_3a
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    throw v0
    :try_end_3e
    .catchall {:try_start_3a .. :try_end_3e} :catchall_3e

    :catchall_3e
    move-exception v0

    if-eqz v1, :cond_44

    :try_start_41
    invoke-static {v1}, Lcom/google/android/youtube/core/async/aj;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_44} :catch_56

    :cond_44
    :goto_44
    throw v0

    :catch_45
    move-exception v0

    :try_start_46
    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_3e

    if-eqz v1, :cond_20

    :try_start_4b
    invoke-static {v1}, Lcom/google/android/youtube/core/async/aj;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_4f

    goto :goto_20

    :catch_4f
    move-exception v0

    const-string v1, "Error consuming content response"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_20

    :catch_56
    move-exception v1

    const-string v2, "Error consuming content response"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_44
.end method
