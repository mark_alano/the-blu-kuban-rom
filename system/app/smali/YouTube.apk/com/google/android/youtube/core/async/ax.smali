.class final Lcom/google/android/youtube/core/async/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/av;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/av;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/av;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const-string v0, "target requester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ax;->a:Lcom/google/android/youtube/core/async/av;

    .line 100
    if-lez p2, :cond_18

    const/4 v0, 0x1

    :goto_10
    const-string v1, "pageSize must be greater than zero"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 101
    iput p2, p0, Lcom/google/android/youtube/core/async/ax;->b:I

    .line 102
    return-void

    .line 100
    :cond_18
    const/4 v0, 0x0

    goto :goto_10
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/ax;)Lcom/google/android/youtube/core/async/av;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/core/async/ax;->a:Lcom/google/android/youtube/core/async/av;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 93
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/youtube/core/async/ax;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/async/aw;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ax;->a:Lcom/google/android/youtube/core/async/av;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/async/GDataRequest;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/core/async/ay;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/core/async/ay;-><init>(Lcom/google/android/youtube/core/async/ax;Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method
