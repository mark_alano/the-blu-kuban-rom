.class public final Lcom/google/android/youtube/core/async/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/av;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Lcom/google/android/youtube/core/async/av;

.field private final c:Lcom/google/android/youtube/core/async/av;

.field private final d:Lcom/google/android/youtube/core/utils/d;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/av;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, "headRequester can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->b:Lcom/google/android/youtube/core/async/av;

    .line 73
    const-string v0, "getRequester can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/async/av;

    .line 74
    const-string v0, "preferences can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/content/SharedPreferences;

    .line 75
    const-string v0, "clock can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/d;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->d:Lcom/google/android/youtube/core/utils/d;

    .line 76
    return-void
.end method

.method public static a(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;)Lcom/google/android/youtube/core/async/av;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    const-string v0, "httpClient can\'t be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v0, "preferences can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v0, "clock can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v0, Lcom/google/android/youtube/core/converter/http/ak;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/ak;-><init>()V

    .line 51
    new-instance v1, Lcom/google/android/youtube/core/async/bd;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/async/bd;-><init>(B)V

    .line 52
    new-instance v2, Lcom/google/android/youtube/core/async/bc;

    new-instance v3, Lcom/google/android/youtube/core/async/aj;

    invoke-direct {v3, p0, v0, v0}, Lcom/google/android/youtube/core/async/aj;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)V

    new-instance v0, Lcom/google/android/youtube/core/async/aj;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/android/youtube/core/async/aj;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)V

    invoke-direct {v2, v3, v0, p2, p3}, Lcom/google/android/youtube/core/async/bc;-><init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/av;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;)V

    .line 58
    invoke-static {p1, v2}, Lcom/google/android/youtube/core/async/d;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/av;)Lcom/google/android/youtube/core/async/d;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/google/android/youtube/core/cache/b;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/cache/b;-><init>(I)V

    .line 63
    const-wide/32 v2, 0x1b7740

    invoke-static {v1, v0, p3, v2, v3}, Lcom/google/android/youtube/core/async/bj;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/utils/d;J)Lcom/google/android/youtube/core/async/bj;

    move-result-object v0

    .line 66
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/bc;)V
    .registers 5
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_failed_head_request"

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bc;->d:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/d;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/bc;)Lcom/google/android/youtube/core/async/av;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/async/av;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide/16 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 28
    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/content/SharedPreferences;

    const-string v3, "last_failed_head_request"

    invoke-interface {v0, v3, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    cmp-long v0, v3, v5

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->d:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/d;->a()J

    move-result-wide v5

    sub-long v3, v5, v3

    const-wide/32 v5, 0x5265c00

    cmp-long v0, v3, v5

    if-lez v0, :cond_2f

    :cond_21
    move v0, v2

    :goto_22
    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->b:Lcom/google/android/youtube/core/async/av;

    new-instance v1, Lcom/google/android/youtube/core/async/be;

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/youtube/core/async/be;-><init>(Lcom/google/android/youtube/core/async/bc;Lcom/google/android/youtube/core/async/l;Z)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    :goto_2e
    return-void

    :cond_2f
    move v0, v1

    goto :goto_22

    :cond_31
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/async/av;

    new-instance v2, Lcom/google/android/youtube/core/async/be;

    invoke-direct {v2, p0, p2, v1}, Lcom/google/android/youtube/core/async/be;-><init>(Lcom/google/android/youtube/core/async/bc;Lcom/google/android/youtube/core/async/l;Z)V

    invoke-interface {v0, p1, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_2e
.end method
