.class public final Lcom/google/android/youtube/core/async/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final a:Landroid/os/ConditionVariable;

.field private volatile b:Ljava/lang/String;

.field private volatile c:Lcom/google/android/youtube/core/model/UserAuth;

.field private volatile d:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Landroid/os/ConditionVariable;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/google/android/youtube/core/async/bf;->c:Lcom/google/android/youtube/core/model/UserAuth;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 33
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->c:Lcom/google/android/youtube/core/model/UserAuth;

    .line 45
    iput-object p2, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 47
    return-void
.end method

.method public final b()Lcom/google/android/youtube/core/model/UserAuth;
    .registers 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_11

    .line 52
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 54
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->c:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method public final i_()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->c:Lcom/google/android/youtube/core/model/UserAuth;

    .line 38
    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    .line 39
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 40
    return-void
.end method
