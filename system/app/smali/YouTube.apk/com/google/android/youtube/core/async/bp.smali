.class final Lcom/google/android/youtube/core/async/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final b:Lcom/google/android/youtube/core/async/bn;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/bn;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/youtube/core/async/bp;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453
    iput-object p2, p0, Lcom/google/android/youtube/core/async/bp;->b:Lcom/google/android/youtube/core/async/bn;

    .line 454
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 8
    .parameter

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/UserAuthorizer;)Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 459
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->b:Lcom/google/android/youtube/core/async/bn;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/async/bn;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 464
    :goto_11
    return-void

    .line 461
    :cond_12
    iget-object v1, p0, Lcom/google/android/youtube/core/async/bp;->b:Lcom/google/android/youtube/core/async/bn;

    new-instance v2, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/youtube/core/model/UserAuth;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v5, p1, Lcom/google/android/youtube/core/model/UserAuth;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/async/bn;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_11
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c(Lcom/google/android/youtube/core/async/UserAuthorizer;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 473
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->b(Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    .line 475
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->b:Lcom/google/android/youtube/core/async/bn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bn;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 476
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->b(Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bp;->b:Lcom/google/android/youtube/core/async/bn;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bn;->i_()V

    .line 469
    return-void
.end method
