.class final Lcom/google/android/youtube/core/async/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/bt;

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/youtube/core/async/l;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/bt;Ljava/util/List;Lcom/google/android/youtube/core/async/l;Ljava/util/Map;Ljava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/youtube/core/async/bu;->a:Lcom/google/android/youtube/core/async/bt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p2, p0, Lcom/google/android/youtube/core/async/bu;->b:Ljava/util/List;

    .line 140
    iput-object p3, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/l;

    .line 141
    iput-object p4, p0, Lcom/google/android/youtube/core/async/bu;->d:Ljava/util/Map;

    .line 142
    iput-object p5, p0, Lcom/google/android/youtube/core/async/bu;->e:Ljava/util/List;

    .line 143
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/async/bt;Ljava/util/List;Lcom/google/android/youtube/core/async/l;Ljava/util/Map;Ljava/util/List;B)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/async/bu;-><init>(Lcom/google/android/youtube/core/async/bt;Ljava/util/List;Lcom/google/android/youtube/core/async/l;Ljava/util/Map;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bu;->b:Ljava/util/List;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 128
    check-cast p2, Ljava/util/List;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/BatchEntry;

    iget v5, v1, Lcom/google/android/youtube/core/model/BatchEntry;->b:I

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_4d

    iget-object v5, p0, Lcom/google/android/youtube/core/async/bu;->a:Lcom/google/android/youtube/core/async/bt;

    invoke-static {v5}, Lcom/google/android/youtube/core/async/bt;->c(Lcom/google/android/youtube/core/async/bt;)Lcom/google/android/youtube/core/cache/a;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/core/async/bu;->a:Lcom/google/android/youtube/core/async/bt;

    invoke-static {v6}, Lcom/google/android/youtube/core/async/bt;->a(Lcom/google/android/youtube/core/async/bt;)Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v6

    new-instance v7, Lcom/google/android/youtube/core/async/Timestamped;

    iget-object v8, v1, Lcom/google/android/youtube/core/model/BatchEntry;->a:Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/youtube/core/async/bu;->a:Lcom/google/android/youtube/core/async/bt;

    invoke-static {v9}, Lcom/google/android/youtube/core/async/bt;->b(Lcom/google/android/youtube/core/async/bt;)Lcom/google/android/youtube/core/utils/d;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/youtube/core/utils/d;->a()J

    move-result-wide v9

    invoke-direct {v7, v8, v9, v10}, Lcom/google/android/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {v5, v6, v7}, Lcom/google/android/youtube/core/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4d
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    goto :goto_f

    :cond_52
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bu;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bu;->a:Lcom/google/android/youtube/core/async/bt;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/bu;->b:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/youtube/core/async/bu;->d:Ljava/util/Map;

    invoke-static {v2, v4, v5, v3}, Lcom/google/android/youtube/core/async/bt;->a(Lcom/google/android/youtube/core/async/bt;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
