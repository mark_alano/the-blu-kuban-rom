.class public final Lcom/google/android/youtube/core/async/bv;
.super Lcom/google/android/youtube/core/async/bh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/async/bh;-><init>(Lcom/google/android/youtube/core/async/l;)V

    .line 21
    const-string v0, "view cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bv;->a:Landroid/view/View;

    .line 22
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/bv;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/youtube/core/async/bv;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/async/bv;-><init>(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bv;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 31
    return-void
.end method
