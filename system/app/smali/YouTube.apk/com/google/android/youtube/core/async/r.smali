.class public final Lcom/google/android/youtube/core/async/r;
.super Lcom/google/android/youtube/core/async/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/youtube/core/async/r;->a:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    const-string v1, "com.google"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/core/async/a;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lcom/google/android/youtube/core/model/UserAuth;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 58
    :try_start_1
    iget-object v1, p0, Lcom/google/android/youtube/core/async/r;->b:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/r;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_b
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_b} :catch_2c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_b} :catch_18
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_b} :catch_1f

    move-result-object v1

    .line 68
    if-eqz v1, :cond_26

    .line 69
    new-instance v0, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/r;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    .line 72
    :goto_17
    return-object v0

    .line 61
    :catch_18
    move-exception v1

    .line 62
    const-string v2, "blockingGetUserAuth failed with AuthenticatorException"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_17

    .line 64
    :catch_1f
    move-exception v1

    .line 65
    const-string v2, "blockingGetUserAuth failed with IOException"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_17

    .line 71
    :cond_26
    const-string v1, "got null authToken for the selected account"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_17

    .line 60
    :catch_2c
    move-exception v1

    goto :goto_17
.end method

.method protected final a(Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bn;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/core/async/r;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/r;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v2, v1, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/youtube/core/async/s;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v4, p0, v1, p2}, Lcom/google/android/youtube/core/async/s;-><init>(Lcom/google/android/youtube/core/async/r;Ljava/lang/String;Lcom/google/android/youtube/core/async/bn;)V

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 46
    return-void
.end method

.method protected final b(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/core/async/r;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/r;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v2, v1, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    new-instance v5, Lcom/google/android/youtube/core/async/s;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v5, p0, v1, p3}, Lcom/google/android/youtube/core/async/s;-><init>(Lcom/google/android/youtube/core/async/r;Ljava/lang/String;Lcom/google/android/youtube/core/async/bn;)V

    move-object v1, p1

    move-object v4, p2

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 52
    return-void
.end method
