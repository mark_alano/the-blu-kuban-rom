.class final Lcom/google/android/youtube/core/async/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/r;

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/android/youtube/core/async/bn;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/r;Ljava/lang/String;Lcom/google/android/youtube/core/async/bn;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/youtube/core/async/s;->a:Lcom/google/android/youtube/core/async/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p2, p0, Lcom/google/android/youtube/core/async/s;->b:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    .line 90
    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 94
    :try_start_1
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 95
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bn;->i_()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_7c
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_14} :catch_34
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_14} :catch_5c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_14} :catch_6c

    .line 119
    :goto_14
    iput-object v5, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    .line 120
    :goto_16
    return-void

    .line 100
    :cond_17
    :try_start_17
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_3d

    .line 103
    iget-object v2, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    new-instance v3, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/s;->a:Lcom/google/android/youtube/core/async/r;

    iget-object v4, v4, Lcom/google/android/youtube/core/async/r;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-direct {v3, v1, v4, v0}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/async/bn;->a(Lcom/google/android/youtube/core/model/UserAuth;)V
    :try_end_33
    .catchall {:try_start_17 .. :try_end_33} :catchall_7c
    .catch Landroid/accounts/OperationCanceledException; {:try_start_17 .. :try_end_33} :catch_34
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_33} :catch_5c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_17 .. :try_end_33} :catch_6c

    goto :goto_14

    .line 109
    :catch_34
    move-exception v0

    .line 110
    :try_start_35
    iget-object v0, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bn;->i_()V
    :try_end_3a
    .catchall {:try_start_35 .. :try_end_3a} :catchall_7c

    .line 119
    iput-object v5, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    goto :goto_16

    .line 105
    :cond_3d
    :try_start_3d
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "got null authToken for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/s;->b:Ljava/lang/String;

    new-instance v2, Landroid/accounts/AuthenticatorException;

    invoke-direct {v2}, Landroid/accounts/AuthenticatorException;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/bn;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5b
    .catchall {:try_start_3d .. :try_end_5b} :catchall_7c
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3d .. :try_end_5b} :catch_34
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_5b} :catch_5c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3d .. :try_end_5b} :catch_6c

    goto :goto_14

    .line 111
    :catch_5c
    move-exception v0

    .line 112
    :try_start_5d
    const-string v1, "login IOException"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/s;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/bn;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_69
    .catchall {:try_start_5d .. :try_end_69} :catchall_7c

    .line 119
    iput-object v5, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    goto :goto_16

    .line 114
    :catch_6c
    move-exception v0

    .line 115
    :try_start_6d
    const-string v1, "login AuthenticatorException"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/s;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/bn;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_79
    .catchall {:try_start_6d .. :try_end_79} :catchall_7c

    .line 119
    iput-object v5, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    goto :goto_16

    :catchall_7c
    move-exception v0

    iput-object v5, p0, Lcom/google/android/youtube/core/async/s;->c:Lcom/google/android/youtube/core/async/bn;

    throw v0
.end method
