.class public final Lcom/google/android/youtube/core/b/ae;
.super Lcom/google/android/youtube/core/b/c;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/b/as;


# instance fields
.field private final g:Lcom/google/android/youtube/core/async/av;

.field private final h:Lcom/google/android/youtube/core/async/av;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/l;Ljava/lang/String;Lcom/google/android/youtube/core/utils/d;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/32 v6, 0x240c8400

    const-wide/32 v4, 0x6ddd00

    const/16 v3, 0x14

    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/b/c;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/l;Ljava/lang/String;Lcom/google/android/youtube/core/utils/d;)V

    .line 45
    new-instance v0, Lcom/google/android/youtube/core/converter/http/dm;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/dm;-><init>()V

    invoke-static {v3}, Lcom/google/android/youtube/core/b/ae;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)Lcom/google/android/youtube/core/async/aj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ae;->d:Ljava/lang/String;

    if-eqz v2, :cond_24

    invoke-virtual {p0}, Lcom/google/android/youtube/core/b/ae;->b()Lcom/google/android/youtube/core/cache/d;

    move-result-object v2

    invoke-virtual {p0, v2, v0, v6, v7}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/av;J)Lcom/google/android/youtube/core/async/bj;

    move-result-object v0

    :cond_24
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/async/av;)Lcom/google/android/youtube/core/async/d;

    move-result-object v0

    invoke-virtual {p0, v1, v0, v4, v5}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/av;J)Lcom/google/android/youtube/core/async/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/ae;->g:Lcom/google/android/youtube/core/async/av;

    .line 46
    new-instance v0, Lcom/google/android/youtube/core/converter/http/dj;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ae;->f:Lcom/google/android/youtube/core/converter/l;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/dj;-><init>(Lcom/google/android/youtube/core/converter/l;)V

    invoke-static {v3}, Lcom/google/android/youtube/core/b/ae;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)Lcom/google/android/youtube/core/async/aj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ae;->d:Ljava/lang/String;

    if-eqz v2, :cond_49

    invoke-virtual {p0}, Lcom/google/android/youtube/core/b/ae;->b()Lcom/google/android/youtube/core/cache/d;

    move-result-object v2

    invoke-virtual {p0, v2, v0, v6, v7}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/av;J)Lcom/google/android/youtube/core/async/bj;

    move-result-object v0

    :cond_49
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/async/av;)Lcom/google/android/youtube/core/async/d;

    move-result-object v0

    invoke-virtual {p0, v1, v0, v4, v5}, Lcom/google/android/youtube/core/b/ae;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/av;J)Lcom/google/android/youtube/core/async/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/ae;->h:Lcom/google/android/youtube/core/async/av;

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/l;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 61
    iget-object v0, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    const-string v1, "subtitleTrack must have videoId set"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ae;->h:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 63
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 67
    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 69
    new-instance v0, Lcom/google/android/youtube/core/converter/http/dn;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/converter/http/dn;-><init>(Lcom/google/android/youtube/core/async/l;)V

    .line 70
    iget-object v1, p0, Lcom/google/android/youtube/core/b/ae;->g:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 71
    return-void
.end method
