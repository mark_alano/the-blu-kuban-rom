.class public final Lcom/google/android/youtube/core/b/j;
.super Lcom/google/android/youtube/core/b/c;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/b/aj;


# instance fields
.field private final g:Lcom/google/android/youtube/core/async/av;

.field private final h:Lcom/google/android/youtube/core/utils/ad;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/ad;[B[BLjava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/b/c;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    .line 54
    :try_start_3
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p4, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v1, "developerKey cannot be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/j;->i:Ljava/lang/String;
    :try_end_12
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_12} :catch_2f

    .line 60
    const-string v0, "serial cannot be null or empty"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/j;->j:Ljava/lang/String;

    .line 61
    iput-object p3, p0, Lcom/google/android/youtube/core/b/j;->h:Lcom/google/android/youtube/core/utils/ad;

    .line 63
    new-instance v0, Lcom/google/android/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/dp;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v1, Lcom/google/android/youtube/core/converter/http/al;

    invoke-direct {v1, p5}, Lcom/google/android/youtube/core/converter/http/al;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/b/j;->a(Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)Lcom/google/android/youtube/core/async/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/j;->g:Lcom/google/android/youtube/core/async/av;

    .line 64
    return-void

    .line 56
    :catch_2f
    move-exception v0

    .line 58
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/core/b/j;->h:Lcom/google/android/youtube/core/utils/ad;

    if-nez v0, :cond_34

    const-string v0, "https://www.google.com/youtube/accounts/registerDevice"

    .line 75
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?developer=%s&serialNumber=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/b/j;->i:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/core/b/j;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/google/android/youtube/core/b/j;->g:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v1, v0, p1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 79
    return-void

    .line 73
    :cond_34
    iget-object v0, p0, Lcom/google/android/youtube/core/b/j;->h:Lcom/google/android/youtube/core/utils/ad;

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method
