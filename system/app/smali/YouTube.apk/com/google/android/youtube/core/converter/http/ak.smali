.class public final Lcom/google/android/youtube/core/converter/http/ak;
.super Lcom/google/android/youtube/core/converter/http/bi;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/converter/a;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bi;-><init>()V

    return-void
.end method

.method private d(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;
    .registers 4
    .parameter

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/ak;->c(Lorg/apache/http/HttpResponse;)V

    .line 36
    const-string v0, "Content-Length"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 37
    if-nez v0, :cond_13

    .line 38
    new-instance v0, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v1, "Missing content length header"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_13
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 42
    :try_start_17
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1e
    .catch Ljava/lang/NumberFormatException; {:try_start_17 .. :try_end_1e} :catch_20

    move-result-object v0

    return-object v0

    .line 43
    :catch_20
    move-exception v0

    .line 44
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    check-cast p1, Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/core/converter/http/HttpMethod;->HEAD:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/ak;->d(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/ak;->d(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
