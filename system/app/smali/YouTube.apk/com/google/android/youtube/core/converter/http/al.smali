.class public final Lcom/google/android/youtube/core/converter/http/al;
.super Lcom/google/android/youtube/core/converter/http/bi;
.source "SourceFile"


# instance fields
.field private final a:[B


# direct methods
.method public constructor <init>([B)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bi;-><init>()V

    .line 34
    const-string v0, "developerSecret cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/al;->a:[B

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;)[B
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0x14

    const/4 v2, 0x0

    .line 54
    :try_start_3
    const-string v0, "AES/ECB/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_8
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_3 .. :try_end_8} :catch_31
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_8} :catch_38

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/al;->a:[B

    invoke-static {v1, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    .line 66
    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 69
    :try_start_13
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 70
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 71
    invoke-virtual {v0, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 72
    array-length v0, v1

    if-le v0, v5, :cond_3f

    .line 73
    const/16 v0, 0x14

    new-array v0, v0, [B

    .line 74
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x14

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_30
    .catch Ljava/security/InvalidKeyException; {:try_start_13 .. :try_end_30} :catch_41
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_13 .. :try_end_30} :catch_48
    .catch Ljavax/crypto/BadPaddingException; {:try_start_13 .. :try_end_30} :catch_4f

    .line 77
    :goto_30
    return-object v0

    .line 59
    :catch_31
    move-exception v0

    .line 60
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 61
    :catch_38
    move-exception v0

    .line 62
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3f
    move-object v0, v1

    .line 77
    goto :goto_30

    .line 79
    :catch_41
    move-exception v0

    .line 80
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 81
    :catch_48
    move-exception v0

    .line 82
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 83
    :catch_4f
    move-exception v0

    .line 84
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    .line 29
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v1, "DeviceId"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "DeviceKey"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_22

    if-eqz v0, :cond_22

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/converter/http/al;->a(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/core/model/d;

    invoke-direct {v2, v1, v0}, Lcom/google/android/youtube/core/model/d;-><init>(Ljava/lang/String;[B)V

    return-object v2

    :cond_22
    new-instance v0, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v1, "invalid device registration response"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
