.class public final Lcom/google/android/youtube/core/converter/http/aw;
.super Lcom/google/android/youtube/core/converter/http/bu;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

.field public final b:Lcom/google/android/youtube/core/utils/ad;

.field public final c:Lcom/google/android/youtube/core/async/GDataRequest$Version;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/utils/ad;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/bu;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    .line 38
    const-string v0, "deviceAuthorizer can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->a:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    .line 40
    const-string v0, "uriRewriter can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/ad;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->b:Lcom/google/android/youtube/core/utils/ad;

    .line 41
    const-string v0, "gdataVersion can\'t be empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->c:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Ljava/lang/String;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/utils/ad;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/converter/http/bu;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Ljava/lang/String;)V

    .line 48
    const-string v0, "deviceAuthProvider can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->a:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    .line 50
    const-string v0, "uriRewriter can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/ad;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->b:Lcom/google/android/youtube/core/utils/ad;

    .line 51
    const-string v0, "gdataVersion can\'t be empty"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->c:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/an;)Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 6
    .parameter

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/converter/http/bu;->a(Lcom/google/android/youtube/core/async/an;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 58
    const-string v1, "GData-Version"

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/http/aw;->c:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    iget-object v2, v2, Lcom/google/android/youtube/core/async/GDataRequest$Version;->headerValue:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :try_start_14
    const-string v1, "X-GData-Device"

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/http/aw;->a:Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    iget-object v3, p1, Lcom/google/android/youtube/core/async/an;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/DeviceAuthorizer;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_21
    .catch Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException; {:try_start_14 .. :try_end_21} :catch_22

    .line 67
    return-object v0

    .line 63
    :catch_22
    move-exception v0

    .line 64
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final b(Lcom/google/android/youtube/core/async/an;)Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 4
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->b:Lcom/google/android/youtube/core/utils/ad;

    if-nez v0, :cond_d

    iget-object v0, p1, Lcom/google/android/youtube/core/async/an;->c:Landroid/net/Uri;

    .line 73
    :goto_6
    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/aw;->d:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0

    .line 72
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/aw;->b:Lcom/google/android/youtube/core/utils/ad;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/an;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/ad;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_6
.end method
