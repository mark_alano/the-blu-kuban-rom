.class public final Lcom/google/android/youtube/core/converter/http/ej;
.super Lcom/google/android/youtube/core/converter/http/gg;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/youtube/core/converter/c;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/l;)V
    .registers 5
    .parameter

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/gg;-><init>(Lcom/google/android/youtube/core/converter/l;)V

    .line 37
    new-instance v0, Lcom/google/android/youtube/core/converter/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/d;-><init>()V

    const-string v1, "/VAST"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/es;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/es;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Impression"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/er;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/er;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Creatives/Creative/Linear/MediaFiles/MediaFile"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eq;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/eq;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Creatives/Creative/Linear/TrackingEvents/Tracking"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ep;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ep;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/ClickThrough"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eo;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/eo;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/ClickTracking"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/en;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/en;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/CustomClick"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/em;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/em;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Extensions/Extension/CustomTracking/Tracking"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/el;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/el;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine/Extensions/Extension/SkippableAdType"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ek;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ek;-><init>(Lcom/google/android/youtube/core/converter/http/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/d;->a()Lcom/google/android/youtube/core/converter/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/ej;->b:Lcom/google/android/youtube/core/converter/c;

    .line 194
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/c;
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/ej;->b:Lcom/google/android/youtube/core/converter/c;

    return-object v0
.end method
