.class final Lcom/google/android/youtube/core/converter/http/ft;
.super Lcom/google/android/youtube/core/converter/m;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/y;Lorg/xml/sax/Attributes;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 308
    new-instance v0, Lcom/google/android/youtube/core/model/BatchEntry$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/BatchEntry$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/y;->offer(Ljava/lang/Object;)Z

    .line 309
    new-instance v0, Lcom/google/android/youtube/core/model/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Video$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/y;->offer(Ljava/lang/Object;)Z

    .line 310
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/y;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 314
    const-class v0, Lcom/google/android/youtube/core/model/Video$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/y;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video$Builder;

    .line 315
    const-class v1, Lcom/google/android/youtube/core/model/BatchEntry$Builder;

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/utils/y;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/BatchEntry$Builder;

    .line 317
    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/BatchEntry$Builder;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1f

    .line 318
    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video$Builder;->build()Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/BatchEntry$Builder;->setResult(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/BatchEntry$Builder;

    .line 321
    :cond_1f
    const-class v0, Lcom/google/android/youtube/core/model/g;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/y;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/g;

    .line 322
    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/BatchEntry$Builder;->build()Lcom/google/android/youtube/core/model/BatchEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/g;->a(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/g;

    .line 323
    return-void
.end method
