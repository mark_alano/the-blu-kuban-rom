.class public Lcom/google/android/youtube/core/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/f;->a:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Typeface;
    .registers 3

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/f;->b:Z

    if-nez v0, :cond_15

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/f;->b:Z

    .line 64
    :try_start_7
    iget-object v0, p0, Lcom/google/android/youtube/core/f;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "Roboto-Light.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/f;->c:Landroid/graphics/Typeface;
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_15} :catch_18

    .line 69
    :cond_15
    :goto_15
    iget-object v0, p0, Lcom/google/android/youtube/core/f;->c:Landroid/graphics/Typeface;

    return-object v0

    .line 66
    :catch_18
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/f;->c:Landroid/graphics/Typeface;

    goto :goto_15
.end method

.method public final a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 38
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/f;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_15

    const/4 v1, 0x3

    if-lt v0, v1, :cond_17

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public final c(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    .line 44
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 45
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 47
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 48
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 49
    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_22

    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public final d(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method
