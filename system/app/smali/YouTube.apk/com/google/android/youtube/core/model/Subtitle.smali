.class public Lcom/google/android/youtube/core/model/Subtitle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private transient lastHtml:Ljava/lang/CharSequence;

.field private transient lastText:Ljava/lang/CharSequence;

.field private final lines:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Ljava/util/ArrayList;)V
    .registers 2
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/youtube/core/model/Subtitle;->lines:Ljava/util/ArrayList;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/ArrayList;Lcom/google/android/youtube/core/model/n;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitle;-><init>(Ljava/util/ArrayList;)V

    return-void
.end method

.method private findTextAt(I)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    .line 43
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    .line 46
    :goto_b
    if-gt v2, v1, :cond_2e

    .line 47
    sub-int v0, v1, v2

    div-int/lit8 v0, v0, 0x2

    add-int v3, v2, v0

    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subtitle$Line;

    .line 49
    iget v4, v0, Lcom/google/android/youtube/core/model/Subtitle$Line;->startTimeMillis:I

    if-ge p1, v4, :cond_23

    .line 50
    add-int/lit8 v0, v3, -0x1

    move v1, v0

    goto :goto_b

    .line 51
    :cond_23
    iget v2, v0, Lcom/google/android/youtube/core/model/Subtitle$Line;->endTimeMillis:I

    if-le p1, v2, :cond_2b

    .line 52
    add-int/lit8 v0, v3, 0x1

    move v2, v0

    goto :goto_b

    .line 54
    :cond_2b
    iget-object v0, v0, Lcom/google/android/youtube/core/model/Subtitle$Line;->text:Ljava/lang/String;

    .line 56
    :goto_2d
    return-object v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method


# virtual methods
.method public declared-synchronized getTextAt(I)Ljava/lang/CharSequence;
    .registers 4
    .parameter

    .prologue
    .line 30
    monitor-enter p0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitle;->findTextAt(I)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_19

    move-result-object v0

    .line 31
    if-nez v0, :cond_a

    .line 32
    const/4 v0, 0x0

    .line 39
    :goto_8
    monitor-exit p0

    return-object v0

    .line 35
    :cond_a
    :try_start_a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/Subtitle;->lastText:Ljava/lang/CharSequence;

    if-eq v0, v1, :cond_16

    .line 36
    iput-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lastText:Ljava/lang/CharSequence;

    .line 37
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lastHtml:Ljava/lang/CharSequence;

    .line 39
    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lastHtml:Ljava/lang/CharSequence;
    :try_end_18
    .catchall {:try_start_a .. :try_end_18} :catchall_19

    goto :goto_8

    .line 30
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
