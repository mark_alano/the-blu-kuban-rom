.class public Lcom/google/android/youtube/core/model/VastAd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;


# instance fields
.field public final adVideoId:Ljava/lang/String;

.field public final clickthroughPingUris:Ljava/util/List;

.field public final clickthroughUri:Landroid/net/Uri;

.field public final closePingUris:Ljava/util/List;

.field public final completePingUris:Ljava/util/List;

.field public final duration:I

.field public final engagedViewPingUris:Ljava/util/List;

.field public final firstQuartilePingUris:Ljava/util/List;

.field public final fullscreenPingUris:Ljava/util/List;

.field public final impressionUris:Ljava/util/List;

.field public final midpointPingUris:Ljava/util/List;

.field public final mutePingUris:Ljava/util/List;

.field public final originalVideoId:Ljava/lang/String;

.field public final pausePingUris:Ljava/util/List;

.field public final resumePingUris:Ljava/util/List;

.field public final shouldPingVssOnEngaged:Z

.field public final skipPingUris:Ljava/util/List;

.field public final skipShownPingUris:Ljava/util/List;

.field public final startPingUris:Ljava/util/List;

.field public final streamUri:Landroid/net/Uri;

.field public final thirdQuartilePingUris:Ljava/util/List;

.field public final title:Ljava/lang/String;

.field public final videoTitleClickedPingUris:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/youtube/core/model/VastAd;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/VastAd;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VastAd;->EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;

    .line 613
    new-instance v0, Lcom/google/android/youtube/core/model/p;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/p;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VastAd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    .line 207
    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    .line 208
    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    .line 209
    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    .line 210
    iput v2, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    .line 211
    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    .line 212
    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    .line 213
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    .line 214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    .line 215
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    .line 216
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    .line 217
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    .line 218
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    .line 219
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    .line 220
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    .line 221
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    .line 222
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    .line 223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    .line 224
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    .line 225
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    .line 226
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    .line 227
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    .line 228
    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    .line 229
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 27
    .parameter

    .prologue
    .line 656
    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v9

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v10

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v15

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v16

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v17

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v18

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v20

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v21

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v22

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v1, v0, :cond_7e

    const/16 v24, 0x1

    :goto_78
    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v24}, Lcom/google/android/youtube/core/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 679
    return-void

    .line 656
    :cond_7e
    const/16 v24, 0x0

    goto :goto_78
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V
    .registers 27
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    const-string v1, "Impression uris cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_85

    const/4 v1, 0x1

    :goto_f
    const-string v2, "Impression uris cannot be empty"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 177
    invoke-static {p1}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    .line 178
    iput-object p2, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    .line 179
    iput-object p3, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    .line 180
    iput-object p4, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    .line 181
    iput p5, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    .line 182
    iput-object p6, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    .line 183
    iput-object p7, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    .line 184
    invoke-static {p8}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    .line 185
    invoke-static {p9}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    .line 186
    invoke-static {p10}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    .line 187
    invoke-static {p11}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    .line 188
    invoke-static {p12}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    .line 189
    invoke-static/range {p13 .. p13}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    .line 190
    invoke-static/range {p14 .. p14}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    .line 191
    invoke-static/range {p15 .. p15}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    .line 192
    invoke-static/range {p16 .. p16}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    .line 193
    invoke-static/range {p17 .. p17}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    .line 194
    invoke-static/range {p18 .. p18}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    .line 195
    invoke-static/range {p19 .. p19}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    .line 196
    invoke-static/range {p20 .. p20}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    .line 197
    invoke-static/range {p21 .. p21}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    .line 198
    invoke-static/range {p22 .. p22}, Lcom/google/android/youtube/core/model/VastAd;->makeSafe(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    .line 199
    move/from16 v0, p23

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    .line 200
    return-void

    .line 176
    :cond_85
    const/4 v1, 0x0

    goto :goto_f
.end method

.method private static makeSafe(Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 145
    if-nez p0, :cond_7

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_6
    return-object v0

    :cond_7
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_6
.end method

.method private static readUriList(Landroid/os/Parcel;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 650
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 651
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 652
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/q;
    .registers 4

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/youtube/core/model/q;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/q;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->b(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->a(I)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->q(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->r(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->c(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->d(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->e(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->f(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->g(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->h(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->i(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->j(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->k(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->l(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->m(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->n(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->o(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->p(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->q(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->a(Z)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .registers 2

    .prologue
    .line 610
    const/4 v0, 0x0

    return v0
.end method

.method public isDummy()Z
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isSkippable()Z
    .registers 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VastAd: [adVideoId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoTitle= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", streamUri = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 624
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 625
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 626
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 627
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 628
    iget v1, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 629
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 630
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 631
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 632
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 633
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 634
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 635
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 636
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 637
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 638
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 639
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 640
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 641
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 642
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 643
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 644
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 645
    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 646
    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v1, :cond_74

    const/4 v0, 0x1

    :cond_74
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 647
    return-void
.end method
