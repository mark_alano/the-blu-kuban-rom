.class public final Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/j;


# static fields
.field public static final ARTIST_TRACK_FIELD_NUMBER:I = 0x4

.field public static final BUNDLE_FOR_COUNTRY_FIELD_NUMBER:I = 0x2

.field public static final MAIN_ARTIST_FIELD_NUMBER:I = 0x1

.field public static final MIX_TRACK_FIELD_NUMBER:I = 0x5

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final RELATED_ARTIST_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

.field private static final serialVersionUID:J


# instance fields
.field private artistTrack_:Ljava/util/List;

.field private bitField0_:I

.field private bundleForCountry_:Ljava/lang/Object;

.field private mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private mixTrack_:Ljava/util/List;

.field private relatedArtist_:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1593
    new-instance v0, Lcom/google/android/youtube/core/model/proto/b;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/b;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    .line 3937
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;-><init>(Z)V

    .line 3938
    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->initFields()V

    .line 3939
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 1513
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3099
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    .line 3154
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedSerializedSize:I

    .line 1514
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->initFields()V

    .line 1515
    const/4 v1, 0x0

    .line 1517
    const/4 v0, 0x0

    move v3, v0

    .line 1518
    :goto_14
    if-nez v3, :cond_ed

    .line 1519
    :try_start_16
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 1520
    sparse-switch v0, :sswitch_data_12e

    .line 1525
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v0

    if-nez v0, :cond_12c

    move v3, v4

    .line 1527
    goto :goto_14

    :sswitch_25
    move v3, v4

    .line 1523
    goto :goto_14

    .line 1532
    :sswitch_27
    const/4 v0, 0x0

    .line 1533
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_129

    .line 1534
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->toBuilder()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    move-object v2, v0

    .line 1536
    :goto_35
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ah;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    .line 1537
    if-eqz v2, :cond_4c

    .line 1538
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    .line 1539
    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/proto/d;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    .line 1541
    :cond_4c
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I
    :try_end_52
    .catchall {:try_start_16 .. :try_end_52} :catchall_59
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_16 .. :try_end_52} :catch_53
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_52} :catch_8f

    goto :goto_14

    .line 1575
    :catch_53
    move-exception v0

    .line 1576
    :goto_54
    :try_start_54
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_59
    .catchall {:try_start_54 .. :try_end_59} :catchall_59

    .line 1581
    :catchall_59
    move-exception v0

    :goto_5a
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v5, :cond_66

    .line 1582
    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    .line 1584
    :cond_66
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v6, :cond_72

    .line 1585
    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    .line 1587
    :cond_72
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_7e

    .line 1588
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    .line 1590
    :cond_7e
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->makeExtensionsImmutable()V

    throw v0

    .line 1545
    :sswitch_82
    :try_start_82
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    .line 1546
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;
    :try_end_8e
    .catchall {:try_start_82 .. :try_end_8e} :catchall_59
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_82 .. :try_end_8e} :catch_53
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_8e} :catch_8f

    goto :goto_14

    .line 1577
    :catch_8f
    move-exception v0

    .line 1578
    :goto_90
    :try_start_90
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_9e
    .catchall {:try_start_90 .. :try_end_9e} :catchall_59

    .line 1550
    :sswitch_9e
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v5, :cond_ab

    .line 1551
    :try_start_a2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    .line 1552
    or-int/lit8 v1, v1, 0x4

    .line 1554
    :cond_ab
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    sget-object v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->PARSER:Lcom/google/protobuf/ah;

    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ah;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    .line 1558
    :sswitch_b8
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v6, :cond_c5

    .line 1559
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    .line 1560
    or-int/lit8 v1, v1, 0x8

    .line 1562
    :cond_c5
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    sget-object v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ah;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    .line 1566
    :sswitch_d2
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v7, :cond_127

    .line 1567
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;
    :try_end_dd
    .catchall {:try_start_a2 .. :try_end_dd} :catchall_59
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_a2 .. :try_end_dd} :catch_53
    .catch Ljava/io/IOException; {:try_start_a2 .. :try_end_dd} :catch_8f

    .line 1568
    or-int/lit8 v0, v1, 0x10

    .line 1570
    :goto_df
    :try_start_df
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    sget-object v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ah;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_ea
    .catchall {:try_start_df .. :try_end_ea} :catchall_115
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_df .. :try_end_ea} :catch_121
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_ea} :catch_11b

    :goto_ea
    move v1, v0

    .line 1574
    goto/16 :goto_14

    .line 1581
    :cond_ed
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v5, :cond_f9

    .line 1582
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    .line 1584
    :cond_f9
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v6, :cond_105

    .line 1585
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    .line 1587
    :cond_105
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_111

    .line 1588
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    .line 1590
    :cond_111
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->makeExtensionsImmutable()V

    .line 1591
    return-void

    .line 1581
    :catchall_115
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_5a

    .line 1577
    :catch_11b
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_90

    .line 1575
    :catch_121
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_54

    :cond_127
    move v0, v1

    goto :goto_df

    :cond_129
    move-object v2, v0

    goto/16 :goto_35

    :cond_12c
    move v0, v1

    goto :goto_ea

    .line 1520
    :sswitch_data_12e
    .sparse-switch
        0x0 -> :sswitch_25
        0xa -> :sswitch_27
        0x12 -> :sswitch_82
        0x1a -> :sswitch_9e
        0x22 -> :sswitch_b8
        0x2a -> :sswitch_d2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1491
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1496
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 3099
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    .line 3154
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedSerializedSize:I

    .line 1498
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1491
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1499
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3099
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    .line 3154
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedSerializedSize:I

    .line 1499
    return-void
.end method

.method static synthetic access$2902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1491
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 1

    .prologue
    .line 1503
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 3093
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    .line 3094
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    .line 3095
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    .line 3096
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    .line 3097
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    .line 3098
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/f;
    .registers 1

    .prologue
    .line 3244
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/f;->a()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;
    .registers 2
    .parameter

    .prologue
    .line 3247
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->newBuilder()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2
    .parameter

    .prologue
    .line 3224
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3230
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2
    .parameter

    .prologue
    .line 3194
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3200
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2
    .parameter

    .prologue
    .line 3235
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3241
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2
    .parameter

    .prologue
    .line 3214
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3220
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2
    .parameter

    .prologue
    .line 3204
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3210
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method


# virtual methods
.method public final getArtistTrack(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter

    .prologue
    .line 3046
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public final getArtistTrackCount()I
    .registers 2

    .prologue
    .line 3040
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getArtistTrackList()Ljava/util/List;
    .registers 2

    .prologue
    .line 3027
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    return-object v0
.end method

.method public final getArtistTrackOrBuilder(I)Lcom/google/android/youtube/core/model/proto/v;
    .registers 3
    .parameter

    .prologue
    .line 3053
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/v;

    return-object v0
.end method

.method public final getArtistTrackOrBuilderList()Ljava/util/List;
    .registers 2

    .prologue
    .line 3034
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    return-object v0
.end method

.method public final getBundleForCountry()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2954
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    .line 2955
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2956
    check-cast v0, Ljava/lang/String;

    .line 2964
    :goto_8
    return-object v0

    .line 2958
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 2960
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 2961
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2962
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 2964
    goto :goto_8
.end method

.method public final getBundleForCountryBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 2972
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    .line 2973
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2974
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 2977
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;

    .line 2980
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 2

    .prologue
    .line 1507
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1491
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    return-object v0
.end method

.method public final getMainArtist()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2

    .prologue
    .line 2938
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public final getMixTrack(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter

    .prologue
    .line 3082
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public final getMixTrackCount()I
    .registers 2

    .prologue
    .line 3076
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getMixTrackList()Ljava/util/List;
    .registers 2

    .prologue
    .line 3063
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    return-object v0
.end method

.method public final getMixTrackOrBuilder(I)Lcom/google/android/youtube/core/model/proto/v;
    .registers 3
    .parameter

    .prologue
    .line 3089
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/v;

    return-object v0
.end method

.method public final getMixTrackOrBuilderList()Ljava/util/List;
    .registers 2

    .prologue
    .line 3070
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    return-object v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 1605
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getRelatedArtist(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;
    .registers 3
    .parameter

    .prologue
    .line 3010
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    return-object v0
.end method

.method public final getRelatedArtistCount()I
    .registers 2

    .prologue
    .line 3004
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getRelatedArtistList()Ljava/util/List;
    .registers 2

    .prologue
    .line 2991
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    return-object v0
.end method

.method public final getRelatedArtistOrBuilder(I)Lcom/google/android/youtube/core/model/proto/i;
    .registers 3
    .parameter

    .prologue
    .line 3017
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/i;

    return-object v0
.end method

.method public final getRelatedArtistOrBuilderList()Ljava/util/List;
    .registers 2

    .prologue
    .line 2998
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3156
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedSerializedSize:I

    .line 3157
    const/4 v0, -0x1

    if-eq v3, v0, :cond_9

    .line 3181
    :goto_8
    return v3

    .line 3160
    :cond_9
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_79

    .line 3161
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3164
    :goto_17
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_26

    .line 3165
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getBundleForCountryBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_26
    move v2, v1

    move v3, v0

    .line 3168
    :goto_28
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_42

    .line 3169
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v3, v0

    .line 3168
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_28

    :cond_42
    move v2, v1

    .line 3172
    :goto_43
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5d

    .line 3173
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v3, v0

    .line 3172
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_43

    .line 3176
    :cond_5d
    :goto_5d
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_76

    .line 3177
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v3, v0

    .line 3176
    add-int/lit8 v1, v1, 0x1

    goto :goto_5d

    .line 3180
    :cond_76
    iput v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedSerializedSize:I

    goto :goto_8

    :cond_79
    move v0, v1

    goto :goto_17
.end method

.method public final hasBundleForCountry()Z
    .registers 3

    .prologue
    .line 2948
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasMainArtist()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 2932
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3101
    iget-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    .line 3102
    const/4 v3, -0x1

    if-eq v0, v3, :cond_b

    if-ne v0, v2, :cond_a

    move v1, v2

    .line 3131
    :cond_a
    :goto_a
    return v1

    .line 3104
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->hasMainArtist()Z

    move-result v0

    if-nez v0, :cond_14

    .line 3105
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    goto :goto_a

    .line 3108
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getMainArtist()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_21

    .line 3109
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    goto :goto_a

    :cond_21
    move v0, v1

    .line 3112
    :goto_22
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getRelatedArtistCount()I

    move-result v3

    if-ge v0, v3, :cond_38

    .line 3113
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getRelatedArtist(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_35

    .line 3114
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    goto :goto_a

    .line 3112
    :cond_35
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    :cond_38
    move v0, v1

    .line 3118
    :goto_39
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getArtistTrackCount()I

    move-result v3

    if-ge v0, v3, :cond_4f

    .line 3119
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getArtistTrack(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4c

    .line 3120
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    goto :goto_a

    .line 3118
    :cond_4c
    add-int/lit8 v0, v0, 0x1

    goto :goto_39

    :cond_4f
    move v0, v1

    .line 3124
    :goto_50
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getMixTrackCount()I

    move-result v3

    if-ge v0, v3, :cond_66

    .line 3125
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getMixTrack(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_63

    .line 3126
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    goto :goto_a

    .line 3124
    :cond_63
    add-int/lit8 v0, v0, 0x1

    goto :goto_50

    .line 3130
    :cond_66
    iput-byte v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->memoizedIsInitialized:B

    move v1, v2

    .line 3131
    goto :goto_a
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/f;
    .registers 2

    .prologue
    .line 3245
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->newBuilder()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1491
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/f;
    .registers 2

    .prologue
    .line 3249
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1491
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->toBuilder()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3188
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3136
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getSerializedSize()I

    .line 3137
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_11

    .line 3138
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 3140
    :cond_11
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1e

    .line 3141
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getBundleForCountryBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    :cond_1e
    move v1, v2

    .line 3143
    :goto_1f
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_37

    .line 3144
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 3143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f

    :cond_37
    move v1, v2

    .line 3146
    :goto_38
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_50

    .line 3147
    const/4 v3, 0x4

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 3146
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_38

    .line 3149
    :cond_50
    :goto_50
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_67

    .line 3150
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 3149
    add-int/lit8 v2, v2, 0x1

    goto :goto_50

    .line 3152
    :cond_67
    return-void
.end method
