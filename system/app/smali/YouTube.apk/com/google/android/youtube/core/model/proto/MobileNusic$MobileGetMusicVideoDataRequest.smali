.class public final Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/p;


# static fields
.field public static final COUNTRY_CODE_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final VIDEO_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private countryCode_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoId_:Lcom/google/protobuf/ab;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 4632
    new-instance v0, Lcom/google/android/youtube/core/model/proto/n;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/n;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    .line 5118
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;-><init>(Z)V

    .line 5119
    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->initFields()V

    .line 5120
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v5, 0x2

    .line 4587
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4725
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    .line 4749
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedSerializedSize:I

    .line 4588
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->initFields()V

    move v1, v0

    .line 4592
    :cond_f
    :goto_f
    if-nez v1, :cond_73

    .line 4593
    :try_start_11
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 4594
    sparse-switch v3, :sswitch_data_8a

    .line 4599
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 4601
    goto :goto_f

    :sswitch_20
    move v1, v2

    .line 4597
    goto :goto_f

    .line 4606
    :sswitch_22
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    .line 4607
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;
    :try_end_2e
    .catchall {:try_start_11 .. :try_end_2e} :catchall_84
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_11 .. :try_end_2e} :catch_2f
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_2e} :catch_61

    goto :goto_f

    .line 4620
    :catch_2f
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 4621
    :try_start_33
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_38
    .catchall {:try_start_33 .. :try_end_38} :catchall_38

    .line 4626
    :catchall_38
    move-exception v0

    :goto_39
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_46

    .line 4627
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-direct {v1, v2}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    .line 4629
    :cond_46
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->makeExtensionsImmutable()V

    throw v0

    .line 4611
    :sswitch_4a
    and-int/lit8 v3, v0, 0x2

    if-eq v3, v5, :cond_57

    .line 4612
    :try_start_4e
    new-instance v3, Lcom/google/protobuf/aa;

    invoke-direct {v3}, Lcom/google/protobuf/aa;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    .line 4613
    or-int/lit8 v0, v0, 0x2

    .line 4615
    :cond_57
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/e;)V
    :try_end_60
    .catchall {:try_start_4e .. :try_end_60} :catchall_84
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4e .. :try_end_60} :catch_2f
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_60} :catch_61

    goto :goto_f

    .line 4622
    :catch_61
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 4623
    :try_start_65
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_73
    .catchall {:try_start_65 .. :try_end_73} :catchall_38

    .line 4626
    :cond_73
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_80

    .line 4627
    new-instance v0, Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    .line 4629
    :cond_80
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->makeExtensionsImmutable()V

    .line 4630
    return-void

    .line 4626
    :catchall_84
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_39

    .line 4594
    nop

    :sswitch_data_8a
    .sparse-switch
        0x0 -> :sswitch_20
        0xa -> :sswitch_22
        0x12 -> :sswitch_4a
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4565
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4570
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 4725
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    .line 4749
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedSerializedSize:I

    .line 4572
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4565
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4573
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4725
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    .line 4749
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedSerializedSize:I

    .line 4573
    return-void
.end method

.method static synthetic access$4400(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 4565
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4565
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 4565
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4565
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    return-object p1
.end method

.method static synthetic access$4602(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4565
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 1

    .prologue
    .line 4577
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 4722
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    .line 4723
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    .line 4724
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/o;
    .registers 1

    .prologue
    .line 4832
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/o;->g()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;
    .registers 2
    .parameter

    .prologue
    .line 4835
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 4812
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4818
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 4782
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4788
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 4823
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4829
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 4802
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4808
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 4792
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4798
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method


# virtual methods
.method public final getCountryCode()Ljava/lang/String;
    .registers 3

    .prologue
    .line 4661
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    .line 4662
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 4663
    check-cast v0, Ljava/lang/String;

    .line 4671
    :goto_8
    return-object v0

    .line 4665
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 4667
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 4668
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 4669
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 4671
    goto :goto_8
.end method

.method public final getCountryCodeBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 4679
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    .line 4680
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 4681
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 4684
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;

    .line 4687
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 2

    .prologue
    .line 4581
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4565
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 4644
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4751
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedSerializedSize:I

    .line 4752
    const/4 v2, -0x1

    if-eq v0, v2, :cond_8

    .line 4769
    :goto_7
    return v0

    .line 4755
    :cond_8
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3e

    .line 4756
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getCountryCodeBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_18
    move v2, v1

    .line 4761
    :goto_19
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v3}, Lcom/google/protobuf/ab;->size()I

    move-result v3

    if-ge v1, v3, :cond_2f

    .line 4762
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v3, v1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4761
    add-int/lit8 v1, v1, 0x1

    goto :goto_19

    .line 4765
    :cond_2f
    add-int/2addr v0, v2

    .line 4766
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getVideoIdList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4768
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedSerializedSize:I

    goto :goto_7

    :cond_3e
    move v0, v1

    goto :goto_18
.end method

.method public final getVideoId(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 4711
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getVideoIdBytes(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 4718
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final getVideoIdCount()I
    .registers 2

    .prologue
    .line 4705
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method

.method public final getVideoIdList()Ljava/util/List;
    .registers 2

    .prologue
    .line 4699
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public final hasCountryCode()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 4655
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4727
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    .line 4728
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 4735
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 4728
    goto :goto_9

    .line 4730
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->hasCountryCode()Z

    move-result v2

    if-nez v2, :cond_16

    .line 4731
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 4732
    goto :goto_9

    .line 4734
    :cond_16
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/o;
    .registers 2

    .prologue
    .line 4833
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 4565
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/o;
    .registers 2

    .prologue
    .line 4837
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 4565
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->toBuilder()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4776
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 4740
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getSerializedSize()I

    .line 4741
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_11

    .line 4742
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getCountryCodeBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 4744
    :cond_11
    const/4 v0, 0x0

    :goto_12
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    if-ge v0, v1, :cond_27

    .line 4745
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;

    invoke-interface {v2, v0}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 4744
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 4747
    :cond_27
    return-void
.end method
