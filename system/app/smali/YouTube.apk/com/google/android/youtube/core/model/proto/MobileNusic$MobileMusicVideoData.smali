.class public final Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/v;


# static fields
.field public static final ARTIST_ID_FIELD_NUMBER:I = 0x2

.field public static final ARTIST_NAME_FIELD_NUMBER:I = 0x3

.field public static final ENCRYPTED_ID_FIELD_NUMBER:I = 0x1

.field public static final LICENSED_FIELD_NUMBER:I = 0x6

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final PARTNER_UPLOADED_FIELD_NUMBER:I = 0x7

.field public static final TRACK_ID_FIELD_NUMBER:I = 0x4

.field public static final TRACK_NAME_FIELD_NUMBER:I = 0x5

.field public static final VEVO_FIELD_NUMBER:I = 0x8

.field public static final VIDEO_LENGTH_IN_SECONDS_FIELD_NUMBER:I = 0xb

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

.field private static final serialVersionUID:J


# instance fields
.field private artistId_:Ljava/lang/Object;

.field private artistName_:Ljava/lang/Object;

.field private bitField0_:I

.field private encryptedId_:Ljava/lang/Object;

.field private licensed_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private partnerUploaded_:Z

.field private trackId_:Ljava/lang/Object;

.field private trackName_:Ljava/lang/Object;

.field private vevo_:Z

.field private videoLengthInSeconds_:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/youtube/core/model/proto/t;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/t;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    .line 1408
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;-><init>(Z)V

    .line 1409
    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->initFields()V

    .line 1410
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 154
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 534
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    .line 599
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedSerializedSize:I

    .line 155
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->initFields()V

    .line 156
    const/4 v0, 0x0

    .line 159
    :cond_d
    :goto_d
    if-nez v0, :cond_b2

    .line 160
    :try_start_f
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v2

    .line 161
    sparse-switch v2, :sswitch_data_b6

    .line 166
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 168
    goto :goto_d

    :sswitch_1e
    move v0, v1

    .line 164
    goto :goto_d

    .line 173
    :sswitch_20
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 174
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;
    :try_end_2c
    .catchall {:try_start_f .. :try_end_2c} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_f .. :try_end_2c} :catch_2d
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_2c} :catch_45

    goto :goto_d

    .line 219
    :catch_2d
    move-exception v0

    .line 220
    :try_start_2e
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_33
    .catchall {:try_start_2e .. :try_end_33} :catchall_33

    .line 225
    :catchall_33
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->makeExtensionsImmutable()V

    throw v0

    .line 178
    :sswitch_38
    :try_start_38
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 179
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;
    :try_end_44
    .catchall {:try_start_38 .. :try_end_44} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_38 .. :try_end_44} :catch_2d
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_44} :catch_45

    goto :goto_d

    .line 221
    :catch_45
    move-exception v0

    .line 222
    :try_start_46
    new-instance v1, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_54
    .catchall {:try_start_46 .. :try_end_54} :catchall_33

    .line 183
    :sswitch_54
    :try_start_54
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 184
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    goto :goto_d

    .line 188
    :sswitch_61
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 189
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    goto :goto_d

    .line 193
    :sswitch_6e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 194
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    goto :goto_d

    .line 198
    :sswitch_7b
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 199
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    goto :goto_d

    .line 203
    :sswitch_88
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 204
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    goto/16 :goto_d

    .line 208
    :sswitch_96
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 209
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    goto/16 :goto_d

    .line 213
    :sswitch_a4
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    .line 214
    invoke-virtual {p1}, Lcom/google/protobuf/h;->b()I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I
    :try_end_b0
    .catchall {:try_start_54 .. :try_end_b0} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_54 .. :try_end_b0} :catch_2d
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_b0} :catch_45

    goto/16 :goto_d

    .line 225
    :cond_b2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->makeExtensionsImmutable()V

    .line 226
    return-void

    .line 161
    :sswitch_data_b6
    .sparse-switch
        0x0 -> :sswitch_1e
        0xa -> :sswitch_20
        0x12 -> :sswitch_38
        0x1a -> :sswitch_54
        0x22 -> :sswitch_61
        0x2a -> :sswitch_6e
        0x30 -> :sswitch_7b
        0x38 -> :sswitch_88
        0x40 -> :sswitch_96
        0x58 -> :sswitch_a4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 137
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 534
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    .line 599
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedSerializedSize:I

    .line 139
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 140
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 534
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    .line 599
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedSerializedSize:I

    .line 140
    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method private initFields()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 524
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    .line 525
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    .line 526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    .line 527
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    .line 528
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    .line 529
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    .line 530
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    .line 531
    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    .line 532
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I

    .line 533
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/u;
    .registers 1

    .prologue
    .line 705
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/u;->a()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;
    .registers 2
    .parameter

    .prologue
    .line 708
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->newBuilder()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2
    .parameter

    .prologue
    .line 685
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 691
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2
    .parameter

    .prologue
    .line 655
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 661
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2
    .parameter

    .prologue
    .line 696
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 702
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2
    .parameter

    .prologue
    .line 675
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 681
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2
    .parameter

    .prologue
    .line 665
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 671
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method


# virtual methods
.method public final getArtistId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    .line 301
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 302
    check-cast v0, Ljava/lang/String;

    .line 310
    :goto_8
    return-object v0

    .line 304
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 306
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 307
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 308
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 310
    goto :goto_8
.end method

.method public final getArtistIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    .line 319
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 320
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 323
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;

    .line 326
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getArtistName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    .line 344
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 345
    check-cast v0, Ljava/lang/String;

    .line 353
    :goto_8
    return-object v0

    .line 347
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 349
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 350
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 351
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 353
    goto :goto_8
.end method

.method public final getArtistNameBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    .line 362
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 363
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 366
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;

    .line 369
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 2

    .prologue
    .line 148
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    return-object v0
.end method

.method public final getEncryptedId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    .line 258
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 259
    check-cast v0, Ljava/lang/String;

    .line 267
    :goto_8
    return-object v0

    .line 261
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 263
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 264
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 265
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 267
    goto :goto_8
.end method

.method public final getEncryptedIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    .line 276
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 277
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 280
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;

    .line 283
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getLicensed()Z
    .registers 2

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    return v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 240
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getPartnerUploaded()Z
    .registers 2

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    return v0
.end method

.method public final getSerializedSize()I
    .registers 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 601
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedSerializedSize:I

    .line 602
    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    .line 642
    :goto_a
    return v0

    .line 604
    :cond_b
    const/4 v0, 0x0

    .line 605
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1c

    .line 606
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getEncryptedIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 609
    :cond_1c
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2b

    .line 610
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getArtistIdBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 613
    :cond_2b
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3b

    .line 614
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getArtistNameBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 617
    :cond_3b
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4a

    .line 618
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getTrackIdBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 621
    :cond_4a
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5c

    .line 622
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getTrackNameBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 625
    :cond_5c
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6c

    .line 626
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 629
    :cond_6c
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7c

    .line 630
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 633
    :cond_7c
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8b

    .line 634
    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 637
    :cond_8b
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9c

    .line 638
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 641
    :cond_9c
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedSerializedSize:I

    goto/16 :goto_a
.end method

.method public final getTrackId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    .line 387
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 388
    check-cast v0, Ljava/lang/String;

    .line 396
    :goto_8
    return-object v0

    .line 390
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 392
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 393
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 394
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 396
    goto :goto_8
.end method

.method public final getTrackIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    .line 405
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 406
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 409
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;

    .line 412
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getTrackName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    .line 430
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 431
    check-cast v0, Ljava/lang/String;

    .line 439
    :goto_8
    return-object v0

    .line 433
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 435
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 436
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 437
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 439
    goto :goto_8
.end method

.method public final getTrackNameBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    .line 448
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 449
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 452
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;

    .line 455
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getVevo()Z
    .registers 2

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    return v0
.end method

.method public final getVideoLengthInSeconds()I
    .registers 2

    .prologue
    .line 520
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I

    return v0
.end method

.method public final hasArtistId()Z
    .registers 3

    .prologue
    .line 294
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasArtistName()Z
    .registers 3

    .prologue
    .line 337
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasEncryptedId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 251
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasLicensed()Z
    .registers 3

    .prologue
    .line 466
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasPartnerUploaded()Z
    .registers 3

    .prologue
    .line 482
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasTrackId()Z
    .registers 3

    .prologue
    .line 380
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasTrackName()Z
    .registers 3

    .prologue
    .line 423
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasVevo()Z
    .registers 3

    .prologue
    .line 498
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasVideoLengthInSeconds()Z
    .registers 3

    .prologue
    .line 514
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 536
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    .line 537
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 564
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 537
    goto :goto_9

    .line 539
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasEncryptedId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 540
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 541
    goto :goto_9

    .line 543
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasArtistId()Z

    move-result v2

    if-nez v2, :cond_20

    .line 544
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 545
    goto :goto_9

    .line 547
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasArtistName()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 548
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 549
    goto :goto_9

    .line 551
    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasTrackId()Z

    move-result v2

    if-nez v2, :cond_34

    .line 552
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 553
    goto :goto_9

    .line 555
    :cond_34
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasTrackName()Z

    move-result v2

    if-nez v2, :cond_3e

    .line 556
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 557
    goto :goto_9

    .line 559
    :cond_3e
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasVideoLengthInSeconds()Z

    move-result v2

    if-nez v2, :cond_48

    .line 560
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    move v0, v1

    .line 561
    goto :goto_9

    .line 563
    :cond_48
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/u;
    .registers 2

    .prologue
    .line 706
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->newBuilder()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/u;
    .registers 2

    .prologue
    .line 710
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->toBuilder()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 649
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 569
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getSerializedSize()I

    .line 570
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_15

    .line 571
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getEncryptedIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 573
    :cond_15
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_22

    .line 574
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getArtistIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 576
    :cond_22
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_30

    .line 577
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getArtistNameBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 579
    :cond_30
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3d

    .line 580
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getTrackIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 582
    :cond_3d
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4d

    .line 583
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getTrackNameBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 585
    :cond_4d
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5b

    .line 586
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 588
    :cond_5b
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_69

    .line 589
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 591
    :cond_69
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_76

    .line 592
    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    .line 594
    :cond_76
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_85

    .line 595
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    .line 597
    :cond_85
    return-void
.end method
