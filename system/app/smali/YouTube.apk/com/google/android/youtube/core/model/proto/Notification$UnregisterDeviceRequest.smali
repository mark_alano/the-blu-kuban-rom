.class public final Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/ag;


# static fields
.field public static final AUTH_TOKEN_FIELD_NUMBER:I = 0x3

.field public static final CHANNEL_IDS_FIELD_NUMBER:I = 0x4

.field public static final DEVICE_ID_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final USER_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

.field private static final serialVersionUID:J


# instance fields
.field private authToken_:Ljava/lang/Object;

.field private bitField0_:I

.field private channelIds_:Lcom/google/protobuf/ab;

.field private deviceId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private userId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ae;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ae;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    .line 933
    new-instance v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;-><init>(Z)V

    .line 934
    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->initFields()V

    .line 935
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v5, 0x8

    .line 104
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 340
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    .line 378
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedSerializedSize:I

    .line 105
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->initFields()V

    move v1, v0

    .line 109
    :cond_10
    :goto_10
    if-nez v1, :cond_94

    .line 110
    :try_start_12
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 111
    sparse-switch v3, :sswitch_data_a6

    .line 116
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 118
    goto :goto_10

    :sswitch_21
    move v1, v2

    .line 114
    goto :goto_10

    .line 123
    :sswitch_23
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    .line 124
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;
    :try_end_2f
    .catchall {:try_start_12 .. :try_end_2f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_12 .. :try_end_2f} :catch_30
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_2f} :catch_58

    goto :goto_10

    .line 147
    :catch_30
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 148
    :try_start_34
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_39
    .catchall {:try_start_34 .. :try_end_39} :catchall_39

    .line 153
    :catchall_39
    move-exception v0

    :goto_3a
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_47

    .line 154
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v1, v2}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 156
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->makeExtensionsImmutable()V

    throw v0

    .line 128
    :sswitch_4b
    :try_start_4b
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    .line 129
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;
    :try_end_57
    .catchall {:try_start_4b .. :try_end_57} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4b .. :try_end_57} :catch_30
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_57} :catch_58

    goto :goto_10

    .line 149
    :catch_58
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 150
    :try_start_5c
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_39

    .line 133
    :sswitch_6a
    :try_start_6a
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    .line 134
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    goto :goto_10

    .line 153
    :catchall_77
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_3a

    .line 138
    :sswitch_7c
    and-int/lit8 v3, v0, 0x8

    if-eq v3, v5, :cond_89

    .line 139
    new-instance v3, Lcom/google/protobuf/aa;

    invoke-direct {v3}, Lcom/google/protobuf/aa;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 140
    or-int/lit8 v0, v0, 0x8

    .line 142
    :cond_89
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/e;)V
    :try_end_92
    .catchall {:try_start_6a .. :try_end_92} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6a .. :try_end_92} :catch_30
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_92} :catch_58

    goto/16 :goto_10

    .line 153
    :cond_94
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_a1

    .line 154
    new-instance v0, Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 156
    :cond_a1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->makeExtensionsImmutable()V

    .line 157
    return-void

    .line 111
    nop

    :sswitch_data_a6
    .sparse-switch
        0x0 -> :sswitch_21
        0xa -> :sswitch_23
        0x12 -> :sswitch_4b
        0x1a -> :sswitch_6a
        0x22 -> :sswitch_7c
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 87
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 340
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    .line 378
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedSerializedSize:I

    .line 89
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 90
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 340
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    .line 378
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedSerializedSize:I

    .line 90
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 1

    .prologue
    .line 94
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 335
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    .line 336
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    .line 337
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    .line 338
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 339
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/af;
    .registers 1

    .prologue
    .line 469
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/af;->a()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;
    .registers 2
    .parameter

    .prologue
    .line 472
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2
    .parameter

    .prologue
    .line 449
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 455
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2
    .parameter

    .prologue
    .line 419
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 425
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2
    .parameter

    .prologue
    .line 460
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 466
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2
    .parameter

    .prologue
    .line 439
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 445
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2
    .parameter

    .prologue
    .line 429
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 435
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method


# virtual methods
.method public final getAuthToken()Ljava/lang/String;
    .registers 3

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    .line 275
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 276
    check-cast v0, Ljava/lang/String;

    .line 284
    :goto_8
    return-object v0

    .line 278
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 280
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 281
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 282
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 284
    goto :goto_8
.end method

.method public final getAuthTokenBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    .line 293
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 294
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 297
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;

    .line 300
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getChannelIds(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelIdsBytes(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final getChannelIdsCount()I
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method

.method public final getChannelIdsList()Ljava/util/List;
    .registers 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 2

    .prologue
    .line 98
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    .line 189
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 190
    check-cast v0, Ljava/lang/String;

    .line 198
    :goto_8
    return-object v0

    .line 192
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 194
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 196
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 198
    goto :goto_8
.end method

.method public final getDeviceIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    .line 207
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 208
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 211
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;

    .line 214
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 171
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 380
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedSerializedSize:I

    .line 381
    const/4 v2, -0x1

    if-eq v0, v2, :cond_9

    .line 406
    :goto_8
    return v0

    .line 384
    :cond_9
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5f

    .line 385
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 388
    :goto_19
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_28

    .line 389
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 392
    :cond_28
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_39

    .line 393
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_39
    move v2, v1

    .line 398
    :goto_3a
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3}, Lcom/google/protobuf/ab;->size()I

    move-result v3

    if-ge v1, v3, :cond_50

    .line 399
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3, v1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v3

    add-int/2addr v2, v3

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_3a

    .line 402
    :cond_50
    add-int/2addr v0, v2

    .line 403
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getChannelIdsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 405
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedSerializedSize:I

    goto :goto_8

    :cond_5f
    move v0, v1

    goto :goto_19
.end method

.method public final getUserId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    .line 232
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 233
    check-cast v0, Ljava/lang/String;

    .line 241
    :goto_8
    return-object v0

    .line 235
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 237
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 239
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 241
    goto :goto_8
.end method

.method public final getUserIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    .line 250
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 251
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 254
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;

    .line 257
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final hasAuthToken()Z
    .registers 3

    .prologue
    .line 268
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasDeviceId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 182
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasUserId()Z
    .registers 3

    .prologue
    .line 225
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    .line 343
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 358
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 343
    goto :goto_9

    .line 345
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasDeviceId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 346
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 347
    goto :goto_9

    .line 349
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasUserId()Z

    move-result v2

    if-nez v2, :cond_20

    .line 350
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 351
    goto :goto_9

    .line 353
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasAuthToken()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 354
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 355
    goto :goto_9

    .line 357
    :cond_2a
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/af;
    .registers 2

    .prologue
    .line 470
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/af;
    .registers 2

    .prologue
    .line 474
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->toBuilder()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 413
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 363
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getSerializedSize()I

    .line 364
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_13

    .line 365
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 367
    :cond_13
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_20

    .line 368
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 370
    :cond_20
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2e

    .line 371
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 373
    :cond_2e
    const/4 v0, 0x0

    :goto_2f
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    if-ge v0, v1, :cond_43

    .line 374
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1, v0}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_2f

    .line 376
    :cond_43
    return-void
.end method
