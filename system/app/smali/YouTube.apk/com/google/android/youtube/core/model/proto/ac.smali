.class public final Lcom/google/android/youtube/core/model/proto/ac;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/ad;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/protobuf/ab;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 2581
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 2727
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->b:Ljava/lang/Object;

    .line 2801
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->c:Ljava/lang/Object;

    .line 2875
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->d:Ljava/lang/Object;

    .line 2949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->e:Ljava/lang/Object;

    .line 3023
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    .line 2582
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/ac;
    .registers 1

    .prologue
    .line 2576
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ac;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ac;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ac;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 2711
    const/4 v2, 0x0

    .line 2713
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 2718
    if-eqz v0, :cond_e

    .line 2719
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;

    .line 2722
    :cond_e
    return-object p0

    .line 2714
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 2715
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 2716
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 2718
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 2719
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;

    :cond_21
    throw v0

    .line 2718
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/ac;
    .registers 3

    .prologue
    .line 2607
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ac;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ac;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->h()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 2623
    new-instance v2, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V

    .line 2624
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2625
    const/4 v1, 0x0

    .line 2626
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_58

    .line 2629
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2002(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2630
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 2631
    or-int/lit8 v0, v0, 0x2

    .line 2633
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2102(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2634
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_26

    .line 2635
    or-int/lit8 v0, v0, 0x4

    .line 2637
    :cond_26
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->d:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2202(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_33

    .line 2639
    or-int/lit8 v0, v0, 0x8

    .line 2641
    :cond_33
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->e:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2302(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2642
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4f

    .line 2643
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    invoke-direct {v1, v3}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    .line 2645
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2647
    :cond_4f
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2402(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;

    .line 2648
    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2502(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;I)I

    .line 2649
    return-object v2

    :cond_58
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;
    .registers 4
    .parameter

    .prologue
    .line 2653
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 2684
    :cond_6
    :goto_6
    return-object p0

    .line 2654
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasDeviceId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2655
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2656
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2000(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->b:Ljava/lang/Object;

    .line 2659
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasUserId()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 2660
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2661
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2100(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->c:Ljava/lang/Object;

    .line 2664
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasAuthToken()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 2665
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2666
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2200(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->d:Ljava/lang/Object;

    .line 2669
    :cond_3d
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasRegistrationId()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 2670
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2671
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2300(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->e:Ljava/lang/Object;

    .line 2674
    :cond_4f
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2400(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2675
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 2676
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2400(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    .line 2677
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    goto :goto_6

    .line 2679
    :cond_6e
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_85

    new-instance v0, Lcom/google/protobuf/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/aa;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    .line 2680
    :cond_85
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/ac;->f:Lcom/google/protobuf/ab;

    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->access$2400(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/ab;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 2576
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2576
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2576
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 2576
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->g()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2576
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->g()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 2576
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->g()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 2576
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->h()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 2576
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ac;->h()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 2576
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2688
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 2704
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 2688
    goto :goto_9

    .line 2692
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2f

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    .line 2696
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_31

    move v2, v1

    :goto_20
    if-eqz v2, :cond_b

    .line 2700
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_33

    move v2, v1

    :goto_2b
    if-eqz v2, :cond_b

    move v0, v1

    .line 2704
    goto :goto_b

    :cond_2f
    move v2, v0

    .line 2692
    goto :goto_16

    :cond_31
    move v2, v0

    .line 2696
    goto :goto_20

    :cond_33
    move v2, v0

    .line 2700
    goto :goto_2b
.end method
