.class public final Lcom/google/android/youtube/core/model/proto/af;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/ag;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/protobuf/ab;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 615
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->b:Ljava/lang/Object;

    .line 689
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->c:Ljava/lang/Object;

    .line 763
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->d:Ljava/lang/Object;

    .line 837
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    .line 485
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/af;
    .registers 1

    .prologue
    .line 479
    new-instance v0, Lcom/google/android/youtube/core/model/proto/af;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/af;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/af;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 599
    const/4 v2, 0x0

    .line 601
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 606
    if-eqz v0, :cond_e

    .line 607
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;

    .line 610
    :cond_e
    return-object p0

    .line 602
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 603
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 604
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 606
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 607
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;

    :cond_21
    throw v0

    .line 606
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/af;
    .registers 3

    .prologue
    .line 508
    new-instance v0, Lcom/google/android/youtube/core/model/proto/af;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/af;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 524
    new-instance v2, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V

    .line 525
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 526
    const/4 v1, 0x0

    .line 527
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4b

    .line 530
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$302(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 532
    or-int/lit8 v0, v0, 0x2

    .line 534
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$402(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_26

    .line 536
    or-int/lit8 v0, v0, 0x4

    .line 538
    :cond_26
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->d:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$502(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_42

    .line 540
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    invoke-direct {v1, v3}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    .line 542
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 544
    :cond_42
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$602(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;

    .line 545
    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$702(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;I)I

    .line 546
    return-object v2

    :cond_4b
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/android/youtube/core/model/proto/af;
    .registers 4
    .parameter

    .prologue
    .line 550
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 576
    :cond_6
    :goto_6
    return-object p0

    .line 551
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasDeviceId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 552
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 553
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$300(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->b:Ljava/lang/Object;

    .line 556
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasUserId()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 557
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 558
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->userId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$400(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->c:Ljava/lang/Object;

    .line 561
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->hasAuthToken()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 562
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 563
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->authToken_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$500(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->d:Ljava/lang/Object;

    .line 566
    :cond_3d
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$600(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 567
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 568
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$600(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    .line 569
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    goto :goto_6

    .line 571
    :cond_5c
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_73

    new-instance v0, Lcom/google/protobuf/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/aa;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    .line 572
    :cond_73
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/af;->e:Lcom/google/protobuf/ab;

    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->access$600(Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;)Lcom/google/protobuf/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/ab;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 479
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 479
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 479
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->g()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->g()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->g()Lcom/google/android/youtube/core/model/proto/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/af;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/af;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 479
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnregisterDeviceRequest;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 580
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 592
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 580
    goto :goto_9

    .line 584
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_24

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    .line 588
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/af;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_26

    move v2, v1

    :goto_20
    if-eqz v2, :cond_b

    move v0, v1

    .line 592
    goto :goto_b

    :cond_24
    move v2, v0

    .line 584
    goto :goto_16

    :cond_26
    move v2, v0

    .line 588
    goto :goto_20
.end method
