.class public final Lcom/google/android/youtube/core/model/proto/ai;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/aj;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/protobuf/ab;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 3599
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 3730
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->b:Ljava/lang/Object;

    .line 3804
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->c:Ljava/lang/Object;

    .line 3878
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->d:Ljava/lang/Object;

    .line 3952
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    .line 3600
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/ai;
    .registers 1

    .prologue
    .line 3594
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ai;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ai;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ai;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 3714
    const/4 v2, 0x0

    .line 3716
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 3721
    if-eqz v0, :cond_e

    .line 3722
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;

    .line 3725
    :cond_e
    return-object p0

    .line 3717
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 3718
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 3719
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 3721
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 3722
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;

    :cond_21
    throw v0

    .line 3721
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/ai;
    .registers 3

    .prologue
    .line 3623
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ai;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ai;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 3639
    new-instance v2, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V

    .line 3640
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3641
    const/4 v1, 0x0

    .line 3642
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4b

    .line 3645
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$2902(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3646
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 3647
    or-int/lit8 v0, v0, 0x2

    .line 3649
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3002(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3650
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_26

    .line 3651
    or-int/lit8 v0, v0, 0x4

    .line 3653
    :cond_26
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->d:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3102(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3654
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_42

    .line 3655
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    invoke-direct {v1, v3}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    .line 3657
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3659
    :cond_42
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3202(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;

    .line 3660
    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3302(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;I)I

    .line 3661
    return-object v2

    :cond_4b
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;
    .registers 4
    .parameter

    .prologue
    .line 3665
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 3691
    :cond_6
    :goto_6
    return-object p0

    .line 3666
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasDeviceId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 3667
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3668
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$2900(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->b:Ljava/lang/Object;

    .line 3671
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasUserId()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 3672
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3673
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3000(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->c:Ljava/lang/Object;

    .line 3676
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasAuthToken()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 3677
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3678
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3100(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->d:Ljava/lang/Object;

    .line 3681
    :cond_3d
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3200(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 3682
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 3683
    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3200(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    .line 3684
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    goto :goto_6

    .line 3686
    :cond_5c
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_73

    new-instance v0, Lcom/google/protobuf/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/aa;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    .line 3687
    :cond_73
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/ai;->e:Lcom/google/protobuf/ab;

    #getter for: Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->access$3200(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/protobuf/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/ab;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 3594
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3594
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3594
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->g()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->g()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->g()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 3594
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/ai;->h()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 3594
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3695
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 3707
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 3695
    goto :goto_9

    .line 3699
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_24

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    .line 3703
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_26

    move v2, v1

    :goto_20
    if-eqz v2, :cond_b

    move v0, v1

    .line 3707
    goto :goto_b

    :cond_24
    move v2, v0

    .line 3699
    goto :goto_16

    :cond_26
    move v2, v0

    .line 3703
    goto :goto_20
.end method
