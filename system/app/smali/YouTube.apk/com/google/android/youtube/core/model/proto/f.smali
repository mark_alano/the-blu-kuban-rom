.class public final Lcom/google/android/youtube/core/model/proto/f;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/j;


# instance fields
.field private a:I

.field private b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 3259
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 3424
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    .line 3485
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->c:Ljava/lang/Object;

    .line 3559
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    .line 3684
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    .line 3809
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    .line 3260
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/f;
    .registers 1

    .prologue
    .line 3254
    new-instance v0, Lcom/google/android/youtube/core/model/proto/f;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/f;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/f;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 3408
    const/4 v2, 0x0

    .line 3410
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 3415
    if-eqz v0, :cond_e

    .line 3416
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;

    .line 3419
    :cond_e
    return-object p0

    .line 3411
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 3412
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 3413
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 3415
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 3416
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;

    :cond_21
    throw v0

    .line 3415
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/f;
    .registers 3

    .prologue
    .line 3285
    new-instance v0, Lcom/google/android/youtube/core/model/proto/f;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/f;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 3301
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 3302
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3303
    const/4 v1, 0x0

    .line 3304
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_73

    .line 3307
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mainArtist_:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$2902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    .line 3308
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1a

    .line 3309
    or-int/lit8 v0, v0, 0x2

    .line 3311
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3312
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_34

    .line 3313
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    .line 3314
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3316
    :cond_34
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3102(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;

    .line 3317
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4f

    .line 3318
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    .line 3319
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3321
    :cond_4f
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3202(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;

    .line 3322
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_6a

    .line 3323
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    .line 3324
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3326
    :cond_6a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3302(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;Ljava/util/List;)Ljava/util/List;

    .line 3327
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;I)I

    .line 3328
    return-object v2

    :cond_73
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Lcom/google/android/youtube/core/model/proto/f;
    .registers 5
    .parameter

    .prologue
    .line 3332
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 3371
    :cond_6
    :goto_6
    return-object p0

    .line 3333
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->hasMainArtist()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 3334
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getMainArtist()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a4

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v2

    if-eq v1, v2, :cond_a4

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-static {v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/d;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    :goto_30
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3336
    :cond_36
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->hasBundleForCountry()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 3337
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3338
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->bundleForCountry_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->c:Ljava/lang/Object;

    .line 3341
    :cond_48
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3100(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_66

    .line 3342
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 3343
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3100(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    .line 3344
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3351
    :cond_66
    :goto_66
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3200(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_84

    .line 3352
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 3353
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3200(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    .line 3354
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3361
    :cond_84
    :goto_84
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 3362
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 3363
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    .line 3364
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    goto/16 :goto_6

    .line 3334
    :cond_a4
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    goto :goto_30

    .line 3346
    :cond_a7
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_bd

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3347
    :cond_bd
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->relatedArtist_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3100(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_66

    .line 3356
    :cond_c7
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_de

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3357
    :cond_de
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->artistTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3200(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_84

    .line 3366
    :cond_e8
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_ff

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    .line 3367
    :cond_ff
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->mixTrack_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->access$3300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 3254
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3254
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3254
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 3254
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->g()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3254
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->g()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 3254
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->g()Lcom/google/android/youtube/core/model/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 3254
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 3254
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/f;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/f;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 3254
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3375
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/f;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    move v0, v3

    :goto_9
    if-nez v0, :cond_e

    .line 3401
    :cond_b
    :goto_b
    return v2

    :cond_c
    move v0, v2

    .line 3375
    goto :goto_9

    .line 3379
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->b:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_b

    move v1, v2

    .line 3383
    :goto_17
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_31

    .line 3384
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3383
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    :cond_31
    move v1, v2

    .line 3389
    :goto_32
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4c

    .line 3390
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_32

    :cond_4c
    move v1, v2

    .line 3395
    :goto_4d
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_67

    .line 3396
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/f;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4d

    :cond_67
    move v2, v3

    .line 3401
    goto :goto_b
.end method
