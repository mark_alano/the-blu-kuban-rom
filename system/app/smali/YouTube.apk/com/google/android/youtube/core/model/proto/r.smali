.class public final Lcom/google/android/youtube/core/model/proto/r;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/s;


# instance fields
.field private a:I

.field private b:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 5374
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 5463
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    .line 5375
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/r;
    .registers 1

    .prologue
    .line 5369
    new-instance v0, Lcom/google/android/youtube/core/model/proto/r;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/r;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/r;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 5447
    const/4 v2, 0x0

    .line 5449
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 5454
    if-eqz v0, :cond_e

    .line 5455
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;

    .line 5458
    :cond_e
    return-object p0

    .line 5450
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 5451
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 5452
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 5454
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 5455
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;

    :cond_21
    throw v0

    .line 5454
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/r;
    .registers 3

    .prologue
    .line 5392
    new-instance v0, Lcom/google/android/youtube/core/model/proto/r;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/r;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 4

    .prologue
    .line 5408
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 5409
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    .line 5410
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1d

    .line 5411
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    .line 5412
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    .line 5414
    :cond_1d
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->access$5002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;Ljava/util/List;)Ljava/util/List;

    .line 5415
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;
    .registers 4
    .parameter

    .prologue
    .line 5419
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 5430
    :cond_6
    :goto_6
    return-object p0

    .line 5420
    :cond_7
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->access$5000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 5421
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 5422
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->access$5000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    .line 5423
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    goto :goto_6

    .line 5425
    :cond_26
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/r;->a:I

    .line 5426
    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->access$5000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 5369
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5369
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5369
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 5369
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->g()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 5369
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->g()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 5369
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->g()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 5369
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 5369
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/r;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 5369
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v2, 0x0

    move v1, v2

    .line 5434
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 5435
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_19

    .line 5440
    :goto_18
    return v2

    .line 5434
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 5440
    :cond_1d
    const/4 v2, 0x1

    goto :goto_18
.end method
