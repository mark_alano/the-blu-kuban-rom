.class public final Lcom/google/android/youtube/core/player/DefaultAdOverlay;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/a;


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Landroid/widget/TextView;

.field private final f:F

.field private final g:I

.field private final h:I

.field private i:Lcom/google/android/youtube/core/player/b;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 64
    const/16 v0, 0xf

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;-><init>(Landroid/content/Context;II)V

    .line 66
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;II)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const v5, -0x333334

    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 70
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->h:I

    .line 71
    iput p2, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->g:I

    .line 73
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->f:F

    .line 75
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    .line 76
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_11f

    .line 86
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    :goto_85
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->f:F

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(IF)I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    const/high16 v1, -0x7800

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 101
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 103
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->addView(Landroid/view/View;)V

    .line 107
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_123

    .line 108
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    .line 109
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->f:F

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(IF)I

    move-result v0

    .line 117
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 120
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 122
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 123
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 124
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->addView(Landroid/view/View;)V

    .line 132
    :goto_112
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->setFullscreen(Z)V

    .line 135
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->setVisibility(I)V

    .line 136
    return-void

    .line 94
    :cond_11f
    iput-object v6, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    goto/16 :goto_85

    .line 128
    :cond_123
    iput-object v6, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    .line 129
    iput-object v6, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    goto :goto_112
.end method

.method private static a(IF)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 151
    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 147
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->h:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 155
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->j:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-nez v0, :cond_18

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->j:Z

    if-nez v0, :cond_3e

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3e

    :cond_18
    move v0, v2

    .line 157
    :goto_19
    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_40

    move v3, v1

    :goto_1e
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 158
    if-eqz v0, :cond_3d

    .line 159
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->j:Z

    if-nez v0, :cond_43

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0069

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->l:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :cond_3d
    :goto_3d
    return-void

    :cond_3e
    move v0, v1

    .line 155
    goto :goto_19

    .line 157
    :cond_40
    const/16 v3, 0x8

    goto :goto_1e

    .line 161
    :cond_43
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_62

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b006a

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->k:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3d

    .line 164
    :cond_62
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3d
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 139
    return-object p0
.end method

.method public final a(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 195
    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->l:Ljava/lang/String;

    .line 196
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d()V

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_36

    .line 198
    div-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, 0x5

    .line 199
    if-gtz v0, :cond_37

    .line 200
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_36

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->b()V

    .line 209
    :cond_36
    :goto_36
    return-void

    .line 206
    :cond_37
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b006d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_36
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 174
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->l:Ljava/lang/String;

    .line 175
    iget-object v0, p1, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->k:Ljava/lang/String;

    .line 176
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d()V

    .line 177
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5b

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 178
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006d

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :cond_3d
    :goto_3d
    invoke-direct {p0, v6}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 188
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v0, :cond_6a

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b006c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_54
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_57
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->setVisibility(I)V

    .line 192
    return-void

    .line 182
    :cond_5b
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;->NOT_SKIPPABLE:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3d

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_3d

    .line 188
    :cond_6a
    const/4 v0, 0x0

    goto :goto_54
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 143
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 221
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->setVisibility(I)V

    .line 222
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_d

    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_e

    .line 227
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->a()V

    .line 234
    :cond_d
    :goto_d
    return-void

    .line 228
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->m:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/core/player/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_1e

    .line 229
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->c()V

    goto :goto_d

    .line 230
    :cond_1e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->j:Z

    if-eqz v0, :cond_d

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->d()V

    goto :goto_d
.end method

.method public final setFullscreen(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 212
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->j:Z

    .line 213
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 215
    const/16 v1, 0x19

    iget v3, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->f:F

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->a(IF)I

    move-result v3

    if-eqz p1, :cond_27

    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->g:I

    :goto_1f
    add-int/2addr v1, v3

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 217
    :cond_23
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->d()V

    .line 218
    return-void

    :cond_27
    move v1, v2

    .line 215
    goto :goto_1f
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/b;)V
    .registers 2
    .parameter

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/b;

    .line 171
    return-void
.end method
