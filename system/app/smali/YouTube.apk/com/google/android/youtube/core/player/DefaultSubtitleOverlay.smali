.class public Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/bd;


# static fields
.field private static a:Ljava/lang/reflect/Method;

.field private static b:Z


# instance fields
.field private final c:I

.field private d:I

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;-><init>(Landroid/content/Context;I)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x2

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->d:I

    .line 61
    iput p2, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->c:I

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->setFontSizeLevel(I)V

    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->a(Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    .line 65
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->setVisibility(I)V

    .line 66
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/widget/TextView;
    .registers 10
    .parameter

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v6, 0x1

    .line 82
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 83
    const/high16 v1, -0x7800

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 84
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 86
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 87
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 88
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 89
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v1

    const/16 v2, 0xb

    if-lt v1, v2, :cond_32

    const/16 v2, 0xd

    if-le v1, v2, :cond_3c

    .line 91
    :cond_32
    :goto_32
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v7, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    return-object v0

    .line 90
    :cond_3c
    :try_start_3c
    sget-boolean v1, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->b:Z

    if-nez v1, :cond_57

    const-class v1, Landroid/widget/TextView;

    const-string v2, "setLayerType"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/graphics/Paint;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->a:Ljava/lang/reflect/Method;

    :cond_57
    sget-object v1, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->a:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_6f

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->a:Ljava/lang/reflect/Method;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6f
    .catchall {:try_start_3c .. :try_end_6f} :catchall_76
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_6f} :catch_72

    :cond_6f
    sput-boolean v6, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->b:Z

    goto :goto_32

    :catch_72
    move-exception v1

    sput-boolean v6, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->b:Z

    goto :goto_32

    :catchall_76
    move-exception v0

    sput-boolean v6, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->b:Z

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 69
    return-object p0
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 4

    .prologue
    .line 73
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 75
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 76
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 77
    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->c:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 78
    return-object v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 157
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->setVisibility(I)V

    .line 158
    return-void
.end method

.method protected onMeasure(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x4150

    .line 122
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 128
    const v1, 0x3ce66666

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v3

    .line 130
    cmpg-float v3, v1, v0

    if-gez v3, :cond_57

    .line 135
    :goto_1d
    const-wide v3, 0x3ff6666660000000L

    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->d:I

    add-int/lit8 v1, v1, -0x2

    int-to-double v5, v1

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-float v1, v3

    mul-float/2addr v0, v1

    .line 136
    float-to-int v1, v0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float v2, v3, v2

    float-to-int v2, v2

    if-ge v1, v2, :cond_4c

    .line 140
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->removeView(Landroid/view/View;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->a(Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    .line 143
    :cond_4c
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->measureChild(Landroid/view/View;II)V

    .line 145
    return-void

    :cond_57
    move v0, v1

    goto :goto_1d
.end method

.method public setFontSizeLevel(I)V
    .registers 2
    .parameter

    .prologue
    .line 166
    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->d:I

    .line 167
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->requestLayout()V

    .line 168
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_12

    .line 150
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->setVisibility(I)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;->requestLayout()V

    .line 154
    :cond_12
    return-void
.end method
