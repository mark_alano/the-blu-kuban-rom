.class public final enum Lcom/google/android/youtube/core/player/Director$LifecycleEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum AD_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum LOADED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum LOADING:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum NEXT:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum PREVIOUS:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum VIDEO_ENDED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

.field public static final enum VIDEO_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 162
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADING:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 163
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 164
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "AD_STARTED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->AD_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 165
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "VIDEO_STARTED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 166
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "VIDEO_ENDED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_ENDED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 167
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "NEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->NEXT:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 168
    new-instance v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    const-string v1, "PREVIOUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->PREVIOUS:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 161
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADING:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->AD_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_ENDED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->NEXT:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->PREVIOUS:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->$VALUES:[Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/player/Director$LifecycleEvent;
    .registers 2
    .parameter

    .prologue
    .line 161
    const-class v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/player/Director$LifecycleEvent;
    .registers 1

    .prologue
    .line 161
    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->$VALUES:[Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    return-object v0
.end method
