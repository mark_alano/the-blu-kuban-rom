.class public final Lcom/google/android/youtube/core/player/DirectorException;
.super Ljava/lang/Exception;
.source "SourceFile"


# instance fields
.field public final reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p2, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/Throwable;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 34
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    .line 35
    return-void
.end method
