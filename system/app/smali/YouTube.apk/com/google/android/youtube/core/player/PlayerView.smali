.class public Lcom/google/android/youtube/core/player/PlayerView;
.super Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/au;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 29
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/youtube/core/player/au;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/youtube/core/player/au;)V

    .line 34
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/youtube/core/player/au;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x2

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-interface {p3}, Lcom/google/android/youtube/core/player/au;->a()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    :goto_c
    const-string v2, "playerSurface must provide a view"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 45
    const-string v0, "playerSurface cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/PlayerView;->a:Lcom/google/android/youtube/core/player/au;

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 48
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 49
    invoke-interface {p3}, Lcom/google/android/youtube/core/player/au;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 50
    return-void

    :cond_2d
    move v0, v1

    .line 43
    goto :goto_c
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/player/au;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/core/player/PlayerView;->a:Lcom/google/android/youtube/core/player/au;

    return-object v0
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/high16 v7, -0x8000

    const v6, 0x3fe374bc

    const/high16 v5, 0x4000

    .line 58
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 59
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 60
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 61
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 65
    if-ne v3, v5, :cond_31

    if-ne v4, v5, :cond_31

    move v1, v2

    .line 92
    :goto_1d
    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/PlayerView;->resolveSize(II)I

    move-result v1

    .line 93
    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/PlayerView;->resolveSize(II)I

    move-result v0

    .line 95
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 96
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 98
    invoke-super {p0, v1, v0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->onMeasure(II)V

    .line 99
    return-void

    .line 69
    :cond_31
    if-eq v3, v5, :cond_37

    if-ne v3, v7, :cond_3c

    if-nez v4, :cond_3c

    .line 72
    :cond_37
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_1d

    .line 73
    :cond_3c
    if-eq v4, v5, :cond_42

    if-ne v4, v7, :cond_46

    if-nez v3, :cond_46

    .line 76
    :cond_42
    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    goto :goto_1d

    .line 77
    :cond_46
    if-ne v3, v7, :cond_5a

    if-ne v4, v7, :cond_5a

    .line 78
    int-to-float v1, v0

    int-to-float v3, v2

    div-float/2addr v3, v6

    cmpg-float v1, v1, v3

    if-gez v1, :cond_55

    .line 79
    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    .line 80
    goto :goto_1d

    .line 83
    :cond_55
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_1d

    :cond_5a
    move v0, v1

    .line 88
    goto :goto_1d
.end method
