.class final enum Lcom/google/android/youtube/core/player/TvControllerOverlay$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field public static final enum ENDED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field public static final enum ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field public static final enum LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field public static final enum PAUSED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field public static final enum PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 47
    new-instance v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 48
    new-instance v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 49
    new-instance v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 50
    new-instance v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->$VALUES:[Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/player/TvControllerOverlay$State;
    .registers 2
    .parameter

    .prologue
    .line 45
    const-class v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/player/TvControllerOverlay$State;
    .registers 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->$VALUES:[Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    return-object v0
.end method
