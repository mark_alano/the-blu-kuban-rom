.class public Lcom/google/android/youtube/core/player/TvControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/ControllerOverlay;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lcom/google/android/youtube/core/player/TvControlsView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/Button;

.field private final g:Landroid/widget/Button;

.field private final h:I

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/youtube/core/ui/o;

.field private k:Lcom/google/android/youtube/core/player/k;

.field private l:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

.field private m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Z

.field private final s:Lcom/google/android/youtube/core/player/MediaActionHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 55
    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const/16 v1, 0x59

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/16 v1, 0x7f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a:Ljava/util/Map;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter

    .prologue
    .line 89
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 90
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 92
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 93
    const v1, 0x7f0400bc

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    const v1, 0x7f0d0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h:I

    .line 97
    new-instance v0, Lcom/google/android/youtube/core/player/bj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/bj;-><init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->i:Landroid/os/Handler;

    .line 106
    const v0, 0x7f080164

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/TvControlsView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    .line 107
    const v0, 0x7f080163

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->c:Landroid/view/View;

    .line 108
    const v0, 0x7f08002f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->d:Landroid/view/View;

    .line 109
    const v0, 0x7f080161

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->e:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f080162

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f:Landroid/widget/Button;

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    new-instance v1, Lcom/google/android/youtube/core/player/MediaActionHelper;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/player/bl;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/google/android/youtube/core/player/bl;-><init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;B)V

    const v0, 0x7f080166

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/youtube/core/player/MediaActionHelper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/an;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->s:Lcom/google/android/youtube/core/player/MediaActionHelper;

    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->s:Lcom/google/android/youtube/core/player/MediaActionHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TvControlsView;->setMediaActionHelper(Lcom/google/android/youtube/core/player/MediaActionHelper;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    const v1, 0x7f08016f

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TvControlsView;->a(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    const v1, 0x7f080165

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->g:Landroid/widget/Button;

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    new-instance v0, Lcom/google/android/youtube/core/ui/o;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->j:Lcom/google/android/youtube/core/ui/o;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h()V

    .line 124
    return-void
.end method

.method private a(I)Lcom/google/android/youtube/core/player/MediaActionHelper$Action;
    .registers 4
    .parameter

    .prologue
    .line 315
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 316
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    .line 321
    :goto_18
    return-object v0

    .line 318
    :cond_19
    const/16 v0, 0x55

    if-eq p1, v0, :cond_21

    const/16 v0, 0x3e

    if-ne p1, v0, :cond_2d

    .line 319
    :cond_21
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v1, :cond_2a

    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    goto :goto_18

    :cond_2a
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    goto :goto_18

    .line 321
    :cond_2d
    const/4 v0, 0x0

    goto :goto_18
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/TvControllerOverlay$State;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/TvControllerOverlay;)I
    .registers 2
    .parameter

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->q:I

    return v0
.end method

.method private d()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->i:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v1, :cond_14

    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->i:Landroid/os/Handler;

    iget v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h:I

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 148
    :cond_14
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 127
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 249
    if-eqz p2, :cond_17

    .line 250
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 254
    :goto_14
    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->p:Z

    .line 255
    return-void

    .line 252
    :cond_17
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    goto :goto_14
.end method

.method public final a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->j:Lcom/google/android/youtube/core/ui/o;

    new-instance v1, Lcom/google/android/youtube/core/player/bk;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/bk;-><init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/o;->a(Ljava/util/List;Lcom/google/android/youtube/core/ui/q;)V

    .line 275
    return-void
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 131
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 187
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->i:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 190
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->d()V

    .line 311
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final f()V
    .registers 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 155
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v4, :cond_60

    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;->PLAYING:Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;

    :goto_d
    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->setPlaybackState(Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;)V

    .line 158
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v4, :cond_63

    move v0, v1

    :goto_19
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v4, :cond_65

    move v0, v1

    :goto_25
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v4, :cond_67

    const/4 v0, 0x1

    :goto_31
    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->setErrorState(Z)V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-eq v3, v4, :cond_3d

    move v2, v1

    :cond_3d
    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/TvControlsView;->setVisibility(I)V

    .line 163
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->n:Z

    if-eqz v0, :cond_69

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v2, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-eq v0, v2, :cond_69

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    sget-object v2, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-eq v0, v2, :cond_69

    .line 164
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h()V

    .line 170
    :goto_53
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->d()V

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    if-eqz v0, :cond_5f

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->q()V

    .line 174
    :cond_5f
    return-void

    .line 155
    :cond_60
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;

    goto :goto_d

    :cond_63
    move v0, v2

    .line 158
    goto :goto_19

    :cond_65
    move v0, v2

    .line 159
    goto :goto_25

    :cond_67
    move v0, v1

    .line 160
    goto :goto_31

    .line 166
    :cond_69
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->o:Z

    .line 167
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->setVisibility(I)V

    goto :goto_53
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 194
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 195
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->o:Z

    .line 220
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    if-eqz v0, :cond_10

    .line 223
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->g()V

    .line 225
    :cond_10
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a()V

    .line 229
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->j:Lcom/google/android/youtube/core/ui/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/o;->a()V

    .line 279
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 299
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f:Landroid/widget/Button;

    if-ne p1, v1, :cond_11

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->p:Z

    if-eqz v1, :cond_11

    .line 300
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->p:Z

    .line 301
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->o()V

    .line 306
    :cond_10
    :goto_10
    return-void

    .line 302
    :cond_11
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->g:Landroid/widget/Button;

    if-ne p1, v1, :cond_10

    .line 303
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->r:Z

    if-nez v1, :cond_1a

    const/4 v0, 0x1

    :cond_1a
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->setFullscreen(Z)V

    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->r:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/k;->c(Z)V

    goto :goto_10
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 284
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 326
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_18

    move v0, v1

    .line 327
    :goto_9
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a(I)Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    move-result-object v3

    .line 328
    if-eqz v3, :cond_22

    .line 329
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->l:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 359
    :cond_17
    :goto_17
    return v1

    :cond_18
    move v0, v2

    .line 326
    goto :goto_9

    .line 332
    :cond_1a
    if-nez v0, :cond_17

    .line 333
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->s:Lcom/google/android/youtube/core/player/MediaActionHelper;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    goto :goto_17

    .line 338
    :cond_22
    sparse-switch p1, :sswitch_data_3e

    .line 359
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_17

    .line 345
    :sswitch_2a
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->o:Z

    if-eqz v0, :cond_32

    .line 346
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    goto :goto_17

    :cond_32
    move v1, v2

    .line 349
    goto :goto_17

    .line 352
    :sswitch_34
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->o:Z

    if-nez v0, :cond_3c

    .line 353
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h()V

    goto :goto_17

    :cond_3c
    move v1, v2

    .line 356
    goto :goto_17

    .line 338
    :sswitch_data_3e
    .sparse-switch
        0x4 -> :sswitch_34
        0x13 -> :sswitch_2a
        0x14 -> :sswitch_2a
        0x15 -> :sswitch_2a
        0x16 -> :sswitch_2a
        0x17 -> :sswitch_2a
        0x42 -> :sswitch_2a
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 365
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a(I)Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    move-result-object v0

    .line 366
    if-eqz v0, :cond_d

    .line 367
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->s:Lcom/google/android/youtube/core/player/MediaActionHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 368
    const/4 v0, 0x1

    .line 371
    :goto_c
    return v0

    :cond_d
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_c
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 289
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 295
    :goto_7
    return v0

    .line 291
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->o:Z

    if-eqz v1, :cond_10

    .line 292
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    goto :goto_7

    .line 295
    :cond_10
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public setCcEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TvControlsView;->setCcEnabled(Z)V

    .line 267
    return-void
.end method

.method public setControlsPermanentlyHidden(Z)V
    .registers 2
    .parameter

    .prologue
    .line 212
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->n:Z

    .line 213
    if-eqz p1, :cond_7

    .line 214
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->h()V

    .line 216
    :cond_7
    return-void
.end method

.method public setFullscreen(Z)V
    .registers 3
    .parameter

    .prologue
    .line 443
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->r:Z

    .line 444
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    .line 445
    return-void
.end method

.method public setHQ(Z)V
    .registers 2
    .parameter

    .prologue
    .line 451
    return-void
.end method

.method public setHQisHD(Z)V
    .registers 2
    .parameter

    .prologue
    .line 453
    return-void
.end method

.method public setHasCc(Z)V
    .registers 2
    .parameter

    .prologue
    .line 455
    return-void
.end method

.method public setHasNext(Z)V
    .registers 3
    .parameter

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TvControlsView;->setNextEnabled(Z)V

    .line 242
    return-void
.end method

.method public setHasPrevious(Z)V
    .registers 3
    .parameter

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TvControlsView;->setPreviousEnabled(Z)V

    .line 246
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/k;)V
    .registers 2
    .parameter

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->k:Lcom/google/android/youtube/core/player/k;

    .line 136
    return-void
.end method

.method public setLoading()V
    .registers 2

    .prologue
    .line 177
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 178
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 179
    return-void
.end method

.method public setPlaying()V
    .registers 2

    .prologue
    .line 182
    sget-object v0, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->m:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    .line 183
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->f()V

    .line 184
    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TvControlsView;->setScrubbingEnabled(Z)V

    .line 233
    return-void
.end method

.method public setShowFullscreen(Z)V
    .registers 4
    .parameter

    .prologue
    .line 439
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->g:Landroid/widget/Button;

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 440
    return-void

    .line 439
    :cond_9
    const/16 v0, 0x8

    goto :goto_5
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V
    .registers 3
    .parameter

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->l:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TvControlsView;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    .line 141
    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .registers 2
    .parameter

    .prologue
    .line 449
    return-void
.end method

.method public setTimes(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 236
    iput p1, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->q:I

    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b:Lcom/google/android/youtube/core/player/TvControlsView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/TvControlsView;->a(III)V

    .line 238
    return-void
.end method
