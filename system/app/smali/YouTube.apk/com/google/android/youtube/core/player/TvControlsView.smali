.class public Lcom/google/android/youtube/core/player/TvControlsView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Map;


# instance fields
.field private final c:Landroid/widget/ToggleButton;

.field private final d:Landroid/widget/Button;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;

.field private final h:Ljava/util/Set;

.field private final i:Lcom/google/android/youtube/core/player/bo;

.field private j:Lcom/google/android/youtube/core/player/MediaActionHelper;

.field private k:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 41
    const v1, 0x7f08016a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const v1, 0x7f080168

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const v1, 0x7f08016c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const v1, 0x7f080169

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const v1, 0x7f08016b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const v1, 0x7f08016e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const v1, 0x7f08016f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->HOME:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const v1, 0x7f08003a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->a:Ljava/util/Map;

    .line 51
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 52
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 53
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7a

    .line 55
    :cond_92
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->b:Ljava/util/Map;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    sget-object v0, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->k:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 82
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bd

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 84
    new-instance v0, Lcom/google/android/youtube/core/player/bo;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/bo;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->i:Lcom/google/android/youtube/core/player/bo;

    .line 86
    const v0, 0x7f08016a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->setPlaybackState(Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;)V

    .line 90
    const-class v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->f:Ljava/util/Set;

    .line 91
    const-class v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->g:Ljava/util/Set;

    .line 92
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    sget-object v1, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->HOME:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->h:Ljava/util/Set;

    .line 94
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 95
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5a
    :goto_5a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 97
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    .line 98
    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    sget-object v4, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    if-eq v1, v4, :cond_5a

    sget-object v4, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    if-eq v1, v4, :cond_5a

    .line 103
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5a

    .line 106
    :cond_92
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->e:Ljava/util/Map;

    .line 108
    const v0, 0x7f08016e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->d:Landroid/widget/Button;

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v0, 0x7f08016f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 111
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void
.end method

.method private a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 206
    if-nez v0, :cond_d

    .line 237
    :cond_c
    :goto_c
    return-void

    .line 209
    :cond_d
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->k:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/player/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)Lcom/google/android/youtube/core/player/MediaActionHelper$ActionState;

    move-result-object v1

    .line 210
    sget-object v4, Lcom/google/android/youtube/core/player/MediaActionHelper$ActionState;->GONE:Lcom/google/android/youtube/core/player/MediaActionHelper$ActionState;

    if-eq v1, v4, :cond_1f

    iget-object v4, p0, Lcom/google/android/youtube/core/player/TvControlsView;->g:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 211
    :cond_1f
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_c

    .line 213
    :cond_25
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 214
    sget-object v4, Lcom/google/android/youtube/core/player/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/MediaActionHelper$ActionState;

    if-ne v1, v4, :cond_4e

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->f:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4e

    move v1, v2

    .line 216
    :goto_35
    if-eqz v1, :cond_50

    .line 217
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_43

    .line 219
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 220
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 222
    :cond_43
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 223
    sget-object v1, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    if-eq p1, v1, :cond_c

    .line 224
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_c

    :cond_4e
    move v1, v3

    .line 214
    goto :goto_35

    .line 227
    :cond_50
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 228
    if-eqz v1, :cond_65

    .line 229
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 230
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    const/high16 v4, -0x6000

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v4, v5}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 233
    :cond_65
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 234
    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_c
.end method

.method private a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 190
    if-eqz p1, :cond_b

    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    :goto_7
    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/player/TvControlsView;->a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 196
    return-void

    .line 193
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_7
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->i:Lcom/google/android/youtube/core/player/bo;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/android/youtube/core/player/bo;->a(III)V

    .line 144
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 199
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->a:Ljava/util/Map;

    const v1, 0x7f08016f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    .line 200
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 202
    return-void
.end method

.method public final a(III)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->i:Lcom/google/android/youtube/core/player/bo;

    div-int/lit16 v1, p1, 0x3e8

    div-int/lit16 v2, p2, 0x3e8

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/youtube/core/player/bo;->a(III)V

    .line 148
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->j:Lcom/google/android/youtube/core/player/MediaActionHelper;

    if-eqz v0, :cond_1e

    .line 261
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f08016a

    if-ne v0, v1, :cond_22

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    .line 262
    :cond_17
    :goto_17
    if-eqz v0, :cond_36

    .line 263
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->j:Lcom/google/android/youtube/core/player/MediaActionHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->execute(Lcom/google/android/youtube/core/player/MediaActionHelper;)V

    .line 268
    :cond_1e
    return-void

    .line 261
    :cond_1f
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    goto :goto_17

    :cond_22
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->a:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    if-nez v0, :cond_17

    const/4 v0, 0x0

    goto :goto_17

    .line 266
    :cond_36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported onClick widget: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onFinishInflate()V
    .registers 7

    .prologue
    .line 125
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControlsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    const v0, 0x7f08016d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 129
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvControlsView;->getChildCount()I

    move-result v2

    .line 130
    const/4 v1, 0x0

    :goto_1c
    if-ge v1, v2, :cond_3f

    .line 131
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/TvControlsView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 132
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    .line 133
    if-eqz v5, :cond_3c

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 134
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/player/TvControlsView;->removeView(Landroid/view/View;)V

    .line 135
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 136
    add-int/lit8 v2, v2, -0x1

    .line 137
    add-int/lit8 v1, v1, -0x1

    .line 130
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    .line 140
    :cond_3f
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 285
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 286
    if-nez p2, :cond_a

    .line 287
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->requestFocus()Z

    .line 289
    :cond_a
    return-void
.end method

.method public setCcEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->d:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    .line 246
    return-void
.end method

.method public setErrorState(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    if-nez p1, :cond_2a

    move v0, v1

    :goto_5
    sget-object v3, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 177
    if-nez p1, :cond_2c

    move v0, v1

    :goto_d
    sget-object v3, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 178
    if-nez p1, :cond_2e

    move v0, v1

    :goto_15
    sget-object v3, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 179
    if-nez p1, :cond_30

    move v0, v1

    :goto_1d
    sget-object v3, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 180
    if-nez p1, :cond_32

    :goto_24
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 181
    return-void

    :cond_2a
    move v0, v2

    .line 176
    goto :goto_5

    :cond_2c
    move v0, v2

    .line 177
    goto :goto_d

    :cond_2e
    move v0, v2

    .line 178
    goto :goto_15

    :cond_30
    move v0, v2

    .line 179
    goto :goto_1d

    :cond_32
    move v1, v2

    .line 180
    goto :goto_24
.end method

.method public setFocus(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V
    .registers 3
    .parameter

    .prologue
    .line 279
    sget-object v0, Lcom/google/android/youtube/core/player/TvControlsView;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 280
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 281
    return-void
.end method

.method public setMediaActionHelper(Lcom/google/android/youtube/core/player/MediaActionHelper;)V
    .registers 4
    .parameter

    .prologue
    .line 115
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaActionHelper;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->j:Lcom/google/android/youtube/core/player/MediaActionHelper;

    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->i:Lcom/google/android/youtube/core/player/bo;

    new-instance v1, Lcom/google/android/youtube/core/player/bm;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/core/player/bm;-><init>(Lcom/google/android/youtube/core/player/TvControlsView;Lcom/google/android/youtube/core/player/MediaActionHelper;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/bo;->a(Lcom/google/android/youtube/core/player/bq;)V

    .line 121
    return-void
.end method

.method public setNextEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 165
    return-void
.end method

.method public setPlayPauseEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 173
    return-void
.end method

.method public setPlaybackState(Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;)V
    .registers 4
    .parameter

    .prologue
    .line 151
    sget-object v0, Lcom/google/android/youtube/core/player/bn;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/TvControlsView$PlaybackState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_20

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 153
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 157
    :goto_17
    return-void

    .line 156
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->c:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_17

    .line 151
    nop

    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_11
        :pswitch_18
    .end packed-switch
.end method

.method public setPreviousEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 168
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 169
    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 185
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 186
    sget-object v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(ZLcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    .line 187
    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V
    .registers 4
    .parameter

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/youtube/core/player/TvControlsView;->k:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->i:Lcom/google/android/youtube/core/player/bo;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsTimeBar:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/bo;->a(Z)V

    .line 273
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvControlsView;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/MediaActionHelper$Action;

    .line 274
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/TvControlsView;->a(Lcom/google/android/youtube/core/player/MediaActionHelper$Action;)V

    goto :goto_f

    .line 276
    :cond_1f
    return-void
.end method
