.class public final Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;
.super Lcom/google/android/youtube/core/player/s;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Set;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field private d:I

.field private e:I

.field private f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 29
    const-string v1, "video/wvm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->a:Ljava/util/Set;

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->e:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 129
    packed-switch p2, :pswitch_data_50

    .line 149
    :cond_4
    :goto_4
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/s;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    .line 150
    const/4 v0, 0x0

    return v0

    .line 131
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->getDuration()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->getCurrentPosition()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 132
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_2c

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_2c

    .line 133
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->pause()V

    .line 134
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 135
    const/16 v0, 0x2bd

    invoke-super {p0, p1, v0, v4}, Lcom/google/android/youtube/core/player/s;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    .line 137
    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4

    .line 141
    :pswitch_36
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_4

    .line 143
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->start()V

    .line 145
    const/16 v0, 0x2be

    invoke-super {p0, p1, v0, v4}, Lcom/google/android/youtube/core/player/s;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    goto :goto_4

    .line 129
    :pswitch_data_50
    .packed-switch 0x2f1
        :pswitch_9
        :pswitch_36
    .end packed-switch
.end method

.method public final b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final b(Lcom/google/android/youtube/core/player/aq;)V
    .registers 6
    .parameter

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 156
    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->d:I

    if-eqz v0, :cond_11

    .line 157
    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekTo(I)V

    .line 169
    :goto_d
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/s;->b(Lcom/google/android/youtube/core/player/aq;)V

    .line 170
    return-void

    .line 159
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->pause()V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->start()V

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/core/player/br;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/br;-><init>(Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_d
.end method

.method public final getCurrentPosition()I
    .registers 2

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->e:I

    if-ltz v0, :cond_7

    .line 111
    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->e:I

    .line 113
    :goto_6
    return v0

    :cond_7
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->getCurrentPosition()I

    move-result v0

    goto :goto_6
.end method

.method public final isPlaying()Z
    .registers 3

    .prologue
    .line 118
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final pause()V
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_7

    .line 77
    :goto_6
    return-void

    .line 76
    :cond_7
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->pause()V

    goto :goto_6
.end method

.method public final release()V
    .registers 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 88
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->release()V

    .line 89
    return-void
.end method

.method public final seekTo(I)V
    .registers 4
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->e:I

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_b

    .line 95
    iput p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->d:I

    .line 106
    :goto_a
    return-void

    .line 98
    :cond_b
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->d:I

    .line 101
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_17

    .line 102
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->start()V

    .line 104
    :cond_17
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 105
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/s;->seekTo(I)V

    goto :goto_a
.end method

.method public final start()V
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->f:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_7

    .line 69
    :goto_6
    return-void

    .line 68
    :cond_7
    invoke-super {p0}, Lcom/google/android/youtube/core/player/s;->start()V

    goto :goto_6
.end method
