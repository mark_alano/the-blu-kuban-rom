.class public final Lcom/google/android/youtube/core/player/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/core/player/bd;

.field private final d:Lcom/google/android/youtube/core/player/ba;

.field private final e:Lcom/google/android/youtube/core/b/as;

.field private final f:Ljava/lang/String;

.field private g:Z

.field private h:Lcom/google/android/youtube/core/model/Subtitle;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/util/List;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/youtube/core/async/n;

.field private n:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/player/ba;Lcom/google/android/youtube/core/b/as;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->a:Landroid/os/Handler;

    .line 85
    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    .line 86
    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/as;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->e:Lcom/google/android/youtube/core/b/as;

    .line 87
    const-string v0, "subtitleOverlay cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/bd;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    .line 88
    invoke-static {p4}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ba;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 90
    iput-object p6, p0, Lcom/google/android/youtube/core/player/az;->f:Ljava/lang/String;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/az;Lcom/google/android/youtube/core/model/Subtitle;)Lcom/google/android/youtube/core/model/Subtitle;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/az;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/youtube/core/player/az;->k:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/az;)Z
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->i:Z

    return v0
.end method

.method private b(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .registers 5
    .parameter

    .prologue
    .line 194
    new-instance v0, Lcom/google/android/youtube/core/player/bb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/bb;-><init>(Lcom/google/android/youtube/core/player/az;B)V

    invoke-static {v0}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->n:Lcom/google/android/youtube/core/async/n;

    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->e:Lcom/google/android/youtube/core/b/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/az;->n:Lcom/google/android/youtube/core/async/n;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/b/as;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/l;)V

    .line 197
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/az;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->g()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/az;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/az;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    move-object v2, v0

    goto :goto_6

    :cond_1e
    if-nez v1, :cond_40

    const-string v4, "en"

    iget-object v5, v0, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    :goto_2a
    move-object v1, v0

    goto :goto_6

    :cond_2c
    if-nez v2, :cond_3a

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->i:Z

    if-eqz v0, :cond_3a

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-object v2, v0

    :cond_3a
    if-eqz v2, :cond_3f

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    :cond_3f
    return-void

    :cond_40
    move-object v0, v1

    goto :goto_2a
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/az;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->h()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/az;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->k:Ljava/util/List;

    return-object v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    if-eqz v0, :cond_c

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ba;->d()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    .line 98
    :cond_c
    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/az;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->f()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/az;)Lcom/google/android/youtube/core/player/ba;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    return-object v0
.end method

.method private f()V
    .registers 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->g:Z

    if-eqz v0, :cond_c

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->g:Z

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ba;->b()V

    .line 105
    :cond_c
    return-void
.end method

.method private g()V
    .registers 2

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->g:Z

    if-nez v0, :cond_c

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/az;->g:Z

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ba;->a()V

    .line 112
    :cond_c
    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/az;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->e()V

    return-void
.end method

.method private h()V
    .registers 4

    .prologue
    .line 185
    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->k:Ljava/util/List;

    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 188
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/az;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 190
    :goto_19
    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/ba;->a(Ljava/util/List;)V

    .line 191
    return-void

    :cond_1f
    move-object v0, v1

    goto :goto_19
.end method

.method private i()I
    .registers 6

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x2

    .line 248
    iget-object v2, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    const-string v3, "subtitles_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 251
    if-le v2, v1, :cond_31

    .line 254
    sparse-switch v2, :sswitch_data_34

    .line 261
    :goto_17
    :sswitch_17
    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "subtitles_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 264
    :goto_2a
    return v0

    .line 255
    :sswitch_2b
    const/4 v0, 0x1

    goto :goto_17

    .line 257
    :sswitch_2d
    const/4 v0, 0x3

    goto :goto_17

    :sswitch_2f
    move v0, v1

    .line 258
    goto :goto_17

    :cond_31
    move v0, v2

    goto :goto_2a

    .line 254
    nop

    :sswitch_data_34
    .sparse-switch
        0xc -> :sswitch_2b
        0x12 -> :sswitch_17
        0x19 -> :sswitch_2d
        0x23 -> :sswitch_2f
    .end sparse-switch
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 156
    iput-object v1, p0, Lcom/google/android/youtube/core/player/az;->l:Ljava/lang/String;

    .line 157
    iput-object v1, p0, Lcom/google/android/youtube/core/player/az;->k:Ljava/util/List;

    .line 159
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->f()V

    .line 160
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->e()V

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_16

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 164
    iput-object v1, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    .line 166
    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->n:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_21

    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->n:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 168
    iput-object v1, p0, Lcom/google/android/youtube/core/player/az;->n:Lcom/google/android/youtube/core/async/n;

    .line 170
    :cond_21
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    if-nez v0, :cond_a

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/bd;->c()V

    .line 214
    :cond_9
    :goto_9
    return-void

    .line 210
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/Subtitle;->getTextAt(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_9

    .line 212
    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/bd;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_9
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .registers 5
    .parameter

    .prologue
    .line 268
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->h:Lcom/google/android/youtube/core/model/Subtitle;

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->d:Lcom/google/android/youtube/core/player/ba;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ba;->d()V

    .line 277
    :goto_1d
    return-void

    .line 273
    :cond_1e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 275
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    goto :goto_1d
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 9
    .parameter

    .prologue
    .line 115
    const-string v0, "video cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    iget-boolean v4, p1, Lcom/google/android/youtube/core/model/Video;->showSubtitlesByDefault:Z

    iget-boolean v1, p1, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->getDefaultSubtitleLanguageCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/az;->a()V

    const-string v5, "videoId cannot be empty"

    invoke-static {v2, v5}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/youtube/core/player/az;->l:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/az;->i:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    const-string v5, "subtitles_language_code"

    const/4 v6, 0x0

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v4, :cond_75

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_40

    :goto_2f
    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->i()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/bd;->setFontSizeLevel(I)V

    if-nez v3, :cond_4f

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->f()V

    .line 118
    :goto_3f
    return-void

    .line 116
    :cond_40
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_75

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2f

    :cond_4f
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_71

    new-instance v0, Lcom/google/android/youtube/core/player/bc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/bc;-><init>(Lcom/google/android/youtube/core/player/az;Z)V

    invoke-static {v0}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->e:Lcom/google/android/youtube/core/b/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/youtube/core/b/as;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_3f

    :cond_71
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->g()V

    goto :goto_3f

    :cond_75
    move-object v0, v1

    goto :goto_2f
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->l:Ljava/lang/String;

    if-eqz v0, :cond_13

    move v0, v1

    :goto_6
    const-string v2, "call init() first"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->k:Ljava/util/List;

    if-eqz v0, :cond_15

    .line 175
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->h()V

    .line 182
    :goto_12
    return-void

    .line 173
    :cond_13
    const/4 v0, 0x0

    goto :goto_6

    .line 177
    :cond_15
    new-instance v0, Lcom/google/android/youtube/core/player/bc;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/bc;-><init>(Lcom/google/android/youtube/core/player/az;Z)V

    invoke-static {v0}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->e:Lcom/google/android/youtube/core/b/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/az;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/az;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/az;->m:Lcom/google/android/youtube/core/async/n;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/as;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_12
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/az;->a()V

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 202
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/bd;->c()V

    .line 218
    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 242
    const-string v0, "subtitles_size"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 243
    iget-object v0, p0, Lcom/google/android/youtube/core/player/az;->c:Lcom/google/android/youtube/core/player/bd;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/az;->i()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/bd;->setFontSizeLevel(I)V

    .line 245
    :cond_11
    return-void
.end method
