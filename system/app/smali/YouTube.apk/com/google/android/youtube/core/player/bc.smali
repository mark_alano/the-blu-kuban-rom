.class final Lcom/google/android/youtube/core/player/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/az;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/az;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/bc;->b:Z

    .line 311
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->e(Lcom/google/android/youtube/core/player/az;)V

    .line 339
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bc;->b:Z

    if-eqz v0, :cond_12

    .line 340
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->f(Lcom/google/android/youtube/core/player/az;)Lcom/google/android/youtube/core/player/ba;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ba;->e()V

    .line 342
    :cond_12
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 306
    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bc;->a()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 306
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "SubtitleTrack response was empty"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bc;->a()V

    :goto_10
    return-void

    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_25

    :cond_20
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;)V

    :cond_25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;Ljava/util/List;)Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bc;->b:Z

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->c(Lcom/google/android/youtube/core/player/az;)V

    :cond_33
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/bc;->a:Lcom/google/android/youtube/core/player/az;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/az;->d(Lcom/google/android/youtube/core/player/az;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;Ljava/util/List;)V

    goto :goto_10
.end method
