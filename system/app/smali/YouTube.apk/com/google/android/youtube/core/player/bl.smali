.class final Lcom/google/android/youtube/core/player/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/an;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/TvControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;)V
    .registers 2
    .parameter

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bl;-><init>(Lcom/google/android/youtube/core/player/TvControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 378
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->m()V

    .line 380
    :cond_11
    return-void
.end method

.method public final a(D)V
    .registers 9
    .parameter

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 384
    const-wide/high16 v0, 0x3ff0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 385
    iget-object v2, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/k;->p()V

    .line 386
    iget-object v2, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->c(Lcom/google/android/youtube/core/player/TvControllerOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sub-int v0, v3, v0

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/k;->a(I)V

    .line 388
    :cond_2f
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 424
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->p()V

    .line 425
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/k;->a(I)V

    .line 427
    :cond_1a
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 418
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->i()V

    .line 420
    :cond_11
    return-void
.end method

.method public final b(D)V
    .registers 9
    .parameter

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 410
    const-wide/high16 v0, 0x3ff0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 411
    iget-object v2, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/k;->p()V

    .line 412
    iget-object v2, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->c(Lcom/google/android/youtube/core/player/TvControllerOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L

    mul-double/2addr v0, v4

    double-to-int v0, v0

    add-int/2addr v0, v3

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/k;->a(I)V

    .line 414
    :cond_2e
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v1, :cond_b

    .line 397
    :cond_a
    :goto_a
    return-void

    .line 394
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 395
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->k()V

    goto :goto_a
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->a(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/TvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/TvControllerOverlay$State;

    if-ne v0, v1, :cond_b

    .line 406
    :cond_a
    :goto_a
    return-void

    .line 403
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 404
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->j()V

    goto :goto_a
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 431
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bl;->a:Lcom/google/android/youtube/core/player/TvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;->b(Lcom/google/android/youtube/core/player/TvControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->f()V

    .line 433
    :cond_11
    return-void
.end method
