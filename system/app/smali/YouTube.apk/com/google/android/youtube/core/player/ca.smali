.class final Lcom/google/android/youtube/core/player/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ar;
.implements Lcom/google/android/youtube/core/player/av;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/bx;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/bx;)V
    .registers 2
    .parameter

    .prologue
    .line 657
    iput-object p1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/bx;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 657
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/ca;-><init>(Lcom/google/android/youtube/core/player/bx;)V

    return-void
.end method

.method private c(Lcom/google/android/youtube/core/player/aq;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 712
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->k(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 713
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 714
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/bx;->l(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v1

    if-nez v1, :cond_24

    if-eqz v0, :cond_24

    .line 715
    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/aq;->seekTo(I)V

    .line 717
    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->m(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-nez v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->n(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 718
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;I)V

    .line 720
    :cond_3a
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 721
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->e(Lcom/google/android/youtube/core/player/bx;Z)V

    .line 724
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;Z)V

    .line 725
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/au;->b()V

    .line 727
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->e()V

    .line 729
    :cond_57
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 660
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 661
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 662
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 663
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;Lcom/google/android/youtube/core/model/Stream;)V

    .line 664
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/player/bx;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    .line 666
    :cond_22
    return-void
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    const/16 v0, 0x64

    .line 733
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/player/ce;->c(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 734
    const/16 v2, 0x5a

    if-le p1, v2, :cond_19

    if-eq v1, p1, :cond_18

    if-ne v1, v0, :cond_19

    :cond_18
    move p1, v0

    .line 737
    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->c(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 738
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 4
    .parameter

    .prologue
    .line 681
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 682
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 683
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->e(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 686
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->b(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/aq;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 688
    :cond_22
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/ca;->c(Lcom/google/android/youtube/core/player/aq;)V

    .line 689
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 802
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "media player info "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    .line 803
    sparse-switch p2, :sswitch_data_54

    .line 815
    :goto_27
    return v2

    .line 805
    :sswitch_28
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Buffering data from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/bx;->s(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    .line 806
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->e(Lcom/google/android/youtube/core/player/bx;Z)V

    goto :goto_27

    .line 809
    :sswitch_46
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->e(Lcom/google/android/youtube/core/player/bx;Z)V

    goto :goto_27

    .line 812
    :sswitch_4c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;I)V

    goto :goto_27

    .line 803
    :sswitch_data_54
    .sparse-switch
        0x2bd -> :sswitch_28
        0x2be -> :sswitch_46
        0x385 -> :sswitch_4c
    .end sparse-switch
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 669
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 670
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/aq;)V
    .registers 4
    .parameter

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->c(Lcom/google/android/youtube/core/player/bx;I)V

    .line 744
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 758
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->o(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-nez v0, :cond_a2

    .line 759
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 760
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 761
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during prepare [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 768
    :goto_36
    if-ne p2, v1, :cond_d0

    invoke-static {}, Lcom/google/android/youtube/core/player/bx;->j()Ljava/util/Set;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d0

    move v0, v1

    .line 771
    :goto_47
    if-nez v0, :cond_ed

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->p(Lcom/google/android/youtube/core/player/bx;)I

    move-result v0

    const/4 v3, 0x3

    if-ge v0, v3, :cond_ed

    .line 772
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Retrying MediaPlayer error [retry="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/bx;->q(Lcom/google/android/youtube/core/player/bx;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", max=3"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 774
    const/16 v0, 0x64

    if-ne p2, v0, :cond_89

    .line 776
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/au;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/bx;->r(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ca;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 779
    :cond_89
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;II)V

    .line 781
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->l(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 782
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/bx;->s(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;)V

    .line 798
    :goto_a1
    return v1

    .line 763
    :cond_a2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 764
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 765
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during playback [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto/16 :goto_36

    :cond_d0
    move v0, v2

    .line 768
    goto/16 :goto_47

    .line 784
    :cond_d3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/bx;->s(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_a1

    .line 787
    :cond_ed
    const-string v0, "Reporting MediaPlayer error"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 789
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;Z)V

    .line 791
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 792
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 793
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/bx;->d(Lcom/google/android/youtube/core/player/bx;I)I

    .line 795
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->g()V

    .line 796
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/player/bx;II)V

    goto :goto_a1
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 675
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 676
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->h()V

    .line 677
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 678
    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/player/aq;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 692
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 693
    if-lez p2, :cond_7

    if-gtz p3, :cond_8

    .line 709
    :cond_7
    :goto_7
    return-void

    .line 696
    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->g(Lcom/google/android/youtube/core/player/bx;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_37

    .line 697
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->h(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/au;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/core/player/au;->setVideoSize(II)V

    .line 705
    :goto_25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->j(Lcom/google/android/youtube/core/player/bx;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 706
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->c(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 707
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/ca;->c(Lcom/google/android/youtube/core/player/aq;)V

    goto :goto_7

    .line 699
    :cond_37
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->i(Lcom/google/android/youtube/core/player/bx;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/player/cb;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/youtube/core/player/cb;-><init>(Lcom/google/android/youtube/core/player/ca;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_25
.end method

.method public final e()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 747
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 749
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ce;->b()V

    .line 750
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 751
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/bx;->f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->b(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 752
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/16 v2, 0x64

    invoke-static {v1, v0, v2, v0}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;III)V

    .line 753
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/player/bx;->g(Lcom/google/android/youtube/core/player/bx;Z)Z

    .line 754
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ca;->a:Lcom/google/android/youtube/core/player/bx;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/player/bx;I)V

    .line 755
    return-void
.end method
