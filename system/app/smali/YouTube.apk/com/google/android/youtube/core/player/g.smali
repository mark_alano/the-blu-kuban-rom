.class final Lcom/google/android/youtube/core/player/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/f;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/f;)V
    .registers 2
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/f;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/g;-><init>(Lcom/google/android/youtube/core/player/f;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 88
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve branding info from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/f;->e(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/h;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 88
    check-cast p2, Lcom/google/android/youtube/core/model/Branding;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    new-instance v1, Lcom/google/android/youtube/core/player/i;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkTargetUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/player/i;-><init>(Lcom/google/android/youtube/core/player/f;Landroid/net/Uri;)V

    invoke-static {v1}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/player/f;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/f;->c(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/player/f;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/f;->b(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/async/n;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    :cond_33
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Branding;->interstitialUri:Landroid/net/Uri;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/f;->d(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/j;

    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/g;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/f;->e(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/h;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/player/h;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method
