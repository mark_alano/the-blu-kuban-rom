.class final Lcom/google/android/youtube/core/player/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/f;

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/f;Landroid/net/Uri;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/youtube/core/player/i;->a:Lcom/google/android/youtube/core/player/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p2, p0, Lcom/google/android/youtube/core/player/i;->b:Landroid/net/Uri;

    .line 116
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 111
    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve watermark from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 111
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/i;->a:Lcom/google/android/youtube/core/player/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/f;->d(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/i;->b:Landroid/net/Uri;

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/player/j;->setWatermark(Landroid/graphics/Bitmap;Landroid/net/Uri;)V

    return-void
.end method
