.class final Lcom/google/android/youtube/core/player/o;
.super Landroid/view/SurfaceView;
.source "SourceFile"


# instance fields
.field protected a:F

.field protected b:F

.field final synthetic c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    .line 199
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 200
    new-instance v0, Lcom/google/android/youtube/core/player/p;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/player/p;-><init>(Lcom/google/android/youtube/core/player/o;Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/o;->d:Ljava/lang/Runnable;

    .line 208
    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/player/o;->getDefaultSize(II)I

    move-result v3

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/o;->getDefaultSize(II)I

    move-result v1

    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    if-lez v0, :cond_bc

    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    if-lez v0, :cond_bc

    .line 223
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v2

    mul-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v0, v2

    sub-float/2addr v0, v4

    .line 224
    const v2, 0x3c23d70a

    cmpl-float v2, v0, v2

    if-lez v2, :cond_8b

    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    mul-int/2addr v0, v3

    iget-object v2, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v2

    div-int/2addr v0, v2

    move v2, v3

    .line 230
    :goto_4e
    iget-object v5, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v5}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Z

    move-result v5

    if-eqz v5, :cond_b6

    .line 231
    if-ge v2, v3, :cond_a3

    .line 232
    int-to-float v1, v2

    int-to-float v5, v3

    div-float/2addr v1, v5

    .line 233
    iget-object v5, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v5}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v5

    sub-int/2addr v3, v2

    mul-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x64

    add-int/2addr v2, v3

    move v6, v4

    move v4, v1

    move v1, v0

    move v0, v6

    .line 241
    :goto_6a
    iget v3, p0, Lcom/google/android/youtube/core/player/o;->a:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_76

    iget v3, p0, Lcom/google/android/youtube/core/player/o;->b:F

    cmpl-float v3, v3, v0

    if-eqz v3, :cond_87

    .line 243
    :cond_76
    iput v4, p0, Lcom/google/android/youtube/core/player/o;->a:F

    .line 244
    iput v0, p0, Lcom/google/android/youtube/core/player/o;->b:F

    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/aw;

    move-result-object v0

    if-eqz v0, :cond_87

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/o;->post(Ljava/lang/Runnable;)Z

    .line 249
    :cond_87
    invoke-virtual {p0, v2, v1}, Lcom/google/android/youtube/core/player/o;->setMeasuredDimension(II)V

    .line 250
    return-void

    .line 226
    :cond_8b
    const v2, -0x43dc28f6

    cmpg-float v0, v0, v2

    if-gez v0, :cond_b9

    .line 227
    iget-object v0, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v2

    div-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto :goto_4e

    .line 234
    :cond_a3
    if-ge v0, v1, :cond_b6

    .line 235
    int-to-float v3, v0

    int-to-float v5, v1

    div-float/2addr v3, v5

    .line 236
    iget-object v5, p0, Lcom/google/android/youtube/core/player/o;->c:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-static {v5}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v5

    sub-int/2addr v1, v0

    mul-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x64

    add-int/2addr v0, v1

    move v1, v0

    move v0, v3

    goto :goto_6a

    :cond_b6
    move v1, v0

    move v0, v4

    goto :goto_6a

    :cond_b9
    move v0, v1

    move v2, v3

    goto :goto_4e

    :cond_bc
    move v0, v4

    move v2, v3

    goto :goto_6a
.end method
