.class public final Lcom/google/android/youtube/core/player/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ax;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/l;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/utils/l;ZZZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 36
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/player/q;-><init>(Lcom/google/android/youtube/core/utils/l;ZZZZZ)V

    .line 37
    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/utils/l;ZZZZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "networkStatus cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/l;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/q;->a:Lcom/google/android/youtube/core/utils/l;

    .line 47
    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/q;->b:Z

    .line 48
    iput-boolean p3, p0, Lcom/google/android/youtube/core/player/q;->c:Z

    .line 49
    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/q;->d:Z

    .line 50
    iput-boolean p5, p0, Lcom/google/android/youtube/core/player/q;->e:Z

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/q;->f:Z

    .line 52
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 101
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    .line 103
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/q;->b:Z

    if-nez v1, :cond_65

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v5, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v1, v5, :cond_65

    move v1, v2

    :goto_1d
    or-int/lit8 v5, v1, 0x0

    .line 105
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/q;->c:Z

    if-eqz v1, :cond_67

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v1, v6, :cond_35

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_480P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v1, v6, :cond_35

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v1, v6, :cond_67

    :cond_35
    move v1, v2

    :goto_36
    or-int/2addr v5, v1

    .line 109
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/q;->e:Z

    if-nez v1, :cond_69

    iget-boolean v1, v0, Lcom/google/android/youtube/core/model/Stream;->is3D:Z

    if-eqz v1, :cond_69

    move v1, v2

    :goto_40
    or-int/2addr v5, v1

    .line 110
    if-eqz p2, :cond_6b

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6b

    move v1, v2

    :goto_4c
    or-int/2addr v1, v5

    .line 113
    iget-boolean v5, v0, Lcom/google/android/youtube/core/model/Stream;->is3D:Z

    if-eqz v5, :cond_6d

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v5, v6, :cond_6d

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v5, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v0, v5, :cond_6d

    move v0, v2

    :goto_5e
    or-int/2addr v0, v1

    .line 116
    if-eqz v0, :cond_6

    .line 117
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_6

    :cond_65
    move v1, v3

    .line 103
    goto :goto_1d

    :cond_67
    move v1, v3

    .line 105
    goto :goto_36

    :cond_69
    move v1, v3

    .line 109
    goto :goto_40

    :cond_6b
    move v1, v3

    .line 110
    goto :goto_4c

    :cond_6d
    move v0, v3

    .line 113
    goto :goto_5e

    .line 120
    :cond_6f
    return-void
.end method

.method private b()[Lcom/google/android/youtube/core/model/Stream$Quality;
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/core/player/q;->a:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->f()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 134
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v2, v0, v1

    .line 136
    :goto_1a
    return-object v0

    :cond_1b
    new-array v0, v3, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    goto :goto_1a
.end method

.method private c()[Lcom/google/android/youtube/core/model/Stream$Quality;
    .registers 6

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/core/player/q;->a:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->f()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 142
    new-array v0, v1, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_240P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_144P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v4

    .line 144
    :goto_1a
    return-object v0

    :cond_1b
    new-array v0, v1, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_240P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_144P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v4

    goto :goto_1a
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 123
    if-eqz p2, :cond_21

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/q;->b()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    move-object v1, v0

    .line 124
    :goto_7
    array-length v3, v1

    const/4 v0, 0x0

    move v2, v0

    :goto_a
    if-ge v2, v3, :cond_2b

    aget-object v4, v1, v2

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v4, v0, :cond_12

    :goto_20
    return-object v0

    .line 123
    :cond_21
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/q;->c()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    move-object v1, v0

    goto :goto_7

    .line 124
    :cond_27
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    :cond_2b
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;)Lcom/google/android/youtube/core/model/v;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 57
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 59
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 60
    sget-object v0, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 62
    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    .line 64
    :cond_1f
    new-instance v1, Lcom/google/android/youtube/core/model/v;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/model/v;-><init>(Lcom/google/android/youtube/core/model/Stream;)V

    move-object v0, v1

    .line 96
    :goto_2f
    return-object v0

    .line 68
    :cond_30
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/q;->e:Z

    if-eqz v0, :cond_5d

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    if-eqz v0, :cond_5d

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$ThreeDSource;->DECLARED:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    if-eq v0, v2, :cond_44

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$ThreeDSource;->UPLOADED:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    if-ne v0, v2, :cond_5d

    :cond_44
    invoke-static {v1}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_5d

    const/4 v0, 0x1

    .line 71
    :goto_4b
    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;Z)V

    .line 73
    invoke-direct {p0, v1, p2}, Lcom/google/android/youtube/core/player/q;->a(Ljava/util/Set;Ljava/util/Set;)V

    .line 75
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 76
    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    .line 68
    :cond_5d
    const/4 v0, 0x0

    goto :goto_4b

    .line 79
    :cond_5f
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/q;->f:Z

    if-nez v0, :cond_68

    .line 80
    sget-object v0, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    .line 83
    :cond_68
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/q;->b()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    .line 84
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/q;->c()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    .line 86
    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;[Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v3

    .line 87
    if-eqz v3, :cond_7b

    .line 89
    iget-object v0, v3, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    .line 91
    :cond_7b
    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/z;->a(Ljava/util/Set;[Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    .line 93
    if-nez v1, :cond_89

    if-nez v3, :cond_89

    .line 94
    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    .line 96
    :cond_89
    new-instance v0, Lcom/google/android/youtube/core/model/v;

    invoke-direct {v0, v3, v1}, Lcom/google/android/youtube/core/model/v;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    goto :goto_2f
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/youtube/core/player/q;->a:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->f()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/q;->d:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/core/player/q;->a:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->b()Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method
