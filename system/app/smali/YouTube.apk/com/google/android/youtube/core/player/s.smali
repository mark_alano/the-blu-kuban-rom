.class public abstract Lcom/google/android/youtube/core/player/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/aq;
.implements Lcom/google/android/youtube/core/player/ar;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/aq;

.field private b:Lcom/google/android/youtube/core/player/ar;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    .line 28
    invoke-interface {p1, p0}, Lcom/google/android/youtube/core/player/aq;->a(Lcom/google/android/youtube/core/player/ar;)V

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/s;->b(I)V

    .line 131
    return-void
.end method

.method public a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/s;->f()V

    .line 139
    return-void
.end method

.method public a(Lcom/google/android/youtube/core/player/ar;)V
    .registers 2
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    .line 105
    return-void
.end method

.method protected final a(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/ar;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/s;->a(II)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final b(I)V
    .registers 3
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ar;->a(I)V

    .line 175
    :cond_9
    return-void
.end method

.method public b(Lcom/google/android/youtube/core/player/aq;)V
    .registers 3
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ar;->b(Lcom/google/android/youtube/core/player/aq;)V

    .line 127
    :cond_9
    return-void
.end method

.method protected final b(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/ar;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b(Lcom/google/android/youtube/core/player/aq;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/s;->b(II)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/youtube/core/player/aq;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/ar;->c(Lcom/google/android/youtube/core/player/aq;II)V

    .line 135
    :cond_9
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->c()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ar;->e()V

    .line 123
    :cond_9
    return-void
.end method

.method protected final f()V
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ar;->a(Lcom/google/android/youtube/core/player/aq;)V

    .line 187
    :cond_9
    return-void
.end method

.method public getCurrentPosition()I
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->pause()V

    .line 53
    return-void
.end method

.method public prepare()V
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->prepare()V

    .line 41
    return-void
.end method

.method public prepareAsync()V
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->prepareAsync()V

    .line 45
    return-void
.end method

.method public release()V
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->release()V

    .line 61
    return-void
.end method

.method public seekTo(I)V
    .registers 3
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->seekTo(I)V

    .line 77
    return-void
.end method

.method public setAudioStreamType(I)V
    .registers 3
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->setAudioStreamType(I)V

    .line 89
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/aq;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 33
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->setDataSource(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 93
    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .registers 3
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->setScreenOnWhilePlaying(Z)V

    .line 81
    return-void
.end method

.method public setSurface(Landroid/view/Surface;)V
    .registers 3
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->setSurface(Landroid/view/Surface;)V

    .line 97
    return-void
.end method

.method public start()V
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/player/s;->a:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->start()V

    .line 49
    return-void
.end method
