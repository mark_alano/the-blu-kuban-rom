.class final Lcom/google/android/youtube/core/player/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/k;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/Director;)V
    .registers 2
    .parameter

    .prologue
    .line 1218
    iput-object p1, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/Director;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1218
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/y;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 1250
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    .line 1251
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .registers 3
    .parameter

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    .line 1283
    return-void
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1270
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 1271
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->p()V

    .line 1267
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 1274
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aa;->k()V

    .line 1275
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->o()V

    .line 1263
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 1254
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->m()V

    .line 1255
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 1227
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->k()V

    .line 1228
    return-void
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1221
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->l()V

    .line 1223
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    .line 1224
    return-void
.end method

.method public final m()V
    .registers 2

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->n()V

    .line 1259
    return-void
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 1235
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 1236
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->i()V

    .line 1237
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1238
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->l()V

    .line 1242
    :goto_1d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    .line 1243
    return-void

    .line 1240
    :cond_23
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->j()V

    goto :goto_1d
.end method

.method public final o()V
    .registers 2

    .prologue
    .line 1231
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->x(Lcom/google/android/youtube/core/player/Director;)V

    .line 1232
    return-void
.end method

.method public final p()V
    .registers 2

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->k()V

    .line 1247
    return-void
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 1278
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aa;->l()V

    .line 1279
    return-void
.end method
