.class public Lcom/google/android/youtube/core/ui/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/async/l;
.implements Lcom/google/android/youtube/core/ui/h;
.implements Lcom/google/android/youtube/core/utils/p;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/av;

.field private final b:Lcom/google/android/youtube/core/d;

.field protected final c:Landroid/app/Activity;

.field final d:Landroid/os/Handler;

.field protected final e:Lcom/google/android/youtube/core/ui/g;

.field protected final f:Lcom/google/android/youtube/core/a/j;

.field protected g:I

.field private final h:Lcom/google/android/youtube/core/async/c;

.field private final i:Lcom/google/android/youtube/core/ui/i;

.field private j:Ljava/util/LinkedList;

.field private k:Lcom/google/android/youtube/core/async/GDataRequest;

.field private l:I

.field private m:Z

.field private n:Landroid/net/Uri;

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->c:Landroid/app/Activity;

    .line 87
    const-string v0, "view cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    .line 88
    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->a:Lcom/google/android/youtube/core/async/av;

    .line 89
    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->b:Lcom/google/android/youtube/core/d;

    .line 90
    const-string v0, "targetAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    invoke-interface {p2}, Lcom/google/android/youtube/core/ui/g;->h()Lcom/google/android/youtube/core/ui/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/youtube/core/a/j;->a(Lcom/google/android/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/youtube/core/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/g;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/g;->setOnRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/i;->a(Landroid/view/View$OnClickListener;)V

    .line 100
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    .line 101
    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->h:Lcom/google/android/youtube/core/async/c;

    .line 103
    new-instance v0, Lcom/google/android/youtube/core/ui/k;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/ui/k;-><init>(Lcom/google/android/youtube/core/ui/j;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Landroid/os/Handler;

    .line 111
    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 321
    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/j;->m:Z

    .line 322
    iput-object p1, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 323
    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2a

    :goto_11
    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    .line 324
    :goto_22
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->a:Lcom/google/android/youtube/core/async/av;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->h:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 325
    return-void

    .line 323
    :cond_2a
    const/4 v0, 0x0

    goto :goto_11

    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->f()V

    goto :goto_22
.end method

.method private i()V
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 312
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->b()V

    .line 318
    :goto_b
    return-void

    .line 314
    :cond_c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    .line 315
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_b
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->e()V

    .line 271
    return-void
.end method

.method public a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 166
    iput-boolean v8, p0, Lcom/google/android/youtube/core/ui/j;->m:Z

    .line 169
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eq p1, v0, :cond_9

    .line 223
    :goto_8
    return-void

    .line 173
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->c()I

    move-result v2

    .line 176
    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->l:I

    .line 178
    iget-object v3, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    .line 179
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 181
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    iget v5, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    sub-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 182
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v10, v0

    move v0, v1

    move v1, v10

    :goto_3a
    if-ge v1, v5, :cond_61

    if-ge v0, v2, :cond_61

    .line 183
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 184
    iget v7, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    if-lez v7, :cond_4f

    .line 185
    iget v6, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    .line 182
    :cond_4c
    :goto_4c
    add-int/lit8 v1, v1, 0x1

    goto :goto_3a

    .line 186
    :cond_4f
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4c

    .line 187
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    add-int/lit8 v0, v0, 0x1

    .line 189
    iget v6, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    goto :goto_4c

    .line 192
    :cond_61
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    iget v1, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries; after filtering "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; realLastIndex = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    .line 196
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e6

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/core/a/j;->a(Ljava/lang/Iterable;)V

    .line 198
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    .line 200
    invoke-virtual {p0, p1, v4}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V

    .line 205
    :goto_b3
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-nez v0, :cond_e9

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 206
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->a()V

    .line 212
    :goto_c6
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_d7

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->j()V

    .line 218
    :cond_d7
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v1}, Lcom/google/android/youtube/core/ui/g;->d()I

    move-result v1

    if-ge v0, v1, :cond_fe

    .line 219
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->f()V

    goto/16 :goto_8

    .line 202
    :cond_e6
    iput-object v9, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    goto :goto_b3

    .line 208
    :cond_e9
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, v9}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->c()V

    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    goto :goto_c6

    .line 221
    :cond_fe
    iput v8, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    goto/16 :goto_8
.end method

.method public a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 280
    iput-boolean v3, p0, Lcom/google/android/youtube/core/ui/j;->m:Z

    .line 281
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    .line 282
    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_45

    move-object v0, p2

    .line 283
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 284
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_45

    .line 285
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->c:Landroid/app/Activity;

    const v2, 0x7f0b0017

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v1}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/String;ZZ)V

    .line 290
    :goto_44
    return-void

    .line 289
    :cond_45
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->b:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/String;ZZ)V

    goto :goto_44
.end method

.method protected a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 264
    return-void
.end method

.method public a(Lcom/google/android/youtube/core/ui/g;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 294
    add-int v0, p2, p3

    .line 295
    if-ne v0, p4, :cond_a

    .line 297
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 299
    :cond_a
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method

.method protected a(Ljava/lang/String;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 336
    if-nez p3, :cond_15

    const/4 v0, 0x1

    .line 337
    :goto_3
    if-eqz v0, :cond_17

    .line 338
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    .line 340
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->a()V

    .line 348
    :goto_14
    return-void

    .line 336
    :cond_15
    const/4 v0, 0x0

    goto :goto_3

    .line 342
    :cond_17
    if-eqz p2, :cond_1f

    .line 343
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/g;->c(Ljava/lang/String;)V

    goto :goto_14

    .line 345
    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/g;->b(Ljava/lang/String;)V

    goto :goto_14
.end method

.method public final varargs a([Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 123
    const-string v0, "requests cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    array-length v0, p1

    if-lez v0, :cond_38

    const/4 v0, 0x1

    :goto_a
    const-string v2, "requests cannot be empty"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    .line 126
    :goto_12
    array-length v0, p1

    if-ge v1, v0, :cond_3a

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    aget-object v2, p1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot be null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    :cond_38
    move v0, v1

    .line 124
    goto :goto_a

    .line 130
    :cond_3a
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->i()V

    .line 131
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 252
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .registers 1

    .prologue
    .line 275
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 150
    iput-object v1, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    .line 151
    iput-object v1, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 152
    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->g:I

    .line 153
    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->l:I

    .line 154
    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    .line 155
    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->a()V

    .line 158
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/core/a/j;->a(ILjava/lang/Object;)V

    .line 162
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->o:I

    .line 163
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->f()V

    .line 139
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/g;->e()V

    .line 147
    return-void
.end method

.method protected final f()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 228
    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/j;->m:Z

    if-eqz v0, :cond_6

    .line 243
    :cond_5
    :goto_5
    return-void

    .line 231
    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    if-nez v0, :cond_e

    .line 232
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->i()V

    goto :goto_5

    .line 233
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    if-eq v0, v1, :cond_5

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->f:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/ui/g;

    invoke-interface {v1}, Lcom/google/android/youtube/core/ui/g;->c()I

    move-result v1

    if-ge v0, v1, :cond_36

    .line 235
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    .line 236
    iput-object v2, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    .line 237
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_5

    .line 239
    :cond_36
    iput-object v2, p0, Lcom/google/android/youtube/core/ui/j;->n:Landroid/net/Uri;

    .line 240
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->b()V

    goto :goto_5
.end method

.method public final g()Lcom/google/android/youtube/core/async/GDataRequest;
    .registers 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method protected final h()I
    .registers 2

    .prologue
    .line 332
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->l:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_9

    .line 303
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 305
    :cond_9
    return-void
.end method
