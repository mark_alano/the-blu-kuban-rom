.class public abstract Lcom/google/android/youtube/coreicecream/Controller;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/Analytics;

.field private final b:Landroid/os/Bundle;

.field private c:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

.field private d:Landroid/view/View;

.field protected final f:Landroid/app/Application;

.field protected final g:Landroid/app/Activity;


# direct methods
.method protected constructor <init>(Landroid/app/Application;Lcom/google/android/youtube/core/Analytics;Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->c:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 63
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/Controller;->f:Landroid/app/Application;

    .line 64
    iput-object p2, p0, Lcom/google/android/youtube/coreicecream/Controller;->a:Lcom/google/android/youtube/core/Analytics;

    .line 65
    iput-object p3, p0, Lcom/google/android/youtube/coreicecream/Controller;->g:Landroid/app/Activity;

    .line 66
    iput-object p4, p0, Lcom/google/android/youtube/coreicecream/Controller;->b:Landroid/os/Bundle;

    .line 67
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method protected a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 212
    if-nez p2, :cond_7

    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->c(I)Landroid/app/Dialog;

    move-result-object v0

    .line 215
    :goto_6
    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method protected a(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 172
    return-void
.end method

.method final a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->a()I

    move-result v1

    .line 77
    if-eqz v1, :cond_b

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :cond_b
    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->d:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->d:Landroid/view/View;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 79
    return-void
.end method

.method protected a(Landroid/view/Menu;)V
    .registers 2
    .parameter

    .prologue
    .line 202
    return-void
.end method

.method protected a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 195
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 93
    return-void
.end method

.method final a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V
    .registers 4
    .parameter

    .prologue
    .line 104
    const-string v0, "newState cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->c:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    if-ne v0, p1, :cond_a

    .line 144
    :goto_9
    return-void

    .line 108
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->c:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 109
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/Controller;->c:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 110
    sget-object v1, Lcom/google/android/youtube/coreicecream/a;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_5c

    goto :goto_9

    .line 123
    :pswitch_1a
    sget-object v0, Lcom/google/android/youtube/coreicecream/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_66

    goto :goto_9

    .line 128
    :pswitch_26
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->e()V

    goto :goto_9

    .line 112
    :pswitch_2a
    sget-object v0, Lcom/google/android/youtube/coreicecream/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6e

    goto :goto_9

    .line 114
    :pswitch_36
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->b()V

    goto :goto_9

    .line 117
    :pswitch_3a
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->b()V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->e()V

    goto :goto_9

    .line 125
    :pswitch_41
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->c()V

    goto :goto_9

    .line 133
    :pswitch_45
    sget-object v0, Lcom/google/android/youtube/coreicecream/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_76

    :pswitch_50
    goto :goto_9

    .line 135
    :pswitch_51
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->g()V

    goto :goto_9

    .line 138
    :pswitch_55
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->g()V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/Controller;->c()V

    goto :goto_9

    .line 110
    :pswitch_data_5c
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_45
        :pswitch_2a
    .end packed-switch

    .line 123
    :pswitch_data_66
    .packed-switch 0x2
        :pswitch_26
        :pswitch_41
    .end packed-switch

    .line 112
    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_36
        :pswitch_3a
    .end packed-switch

    .line 133
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_51
        :pswitch_50
        :pswitch_55
    .end packed-switch
.end method

.method protected a(IILandroid/content/Intent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 269
    const/4 v0, 0x0

    return v0
.end method

.method protected a(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->a:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method protected b(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method protected c(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 219
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 186
    return-void
.end method

.method protected d()V
    .registers 1

    .prologue
    .line 192
    return-void
.end method

.method protected e()V
    .registers 1

    .prologue
    .line 159
    return-void
.end method

.method protected g()V
    .registers 1

    .prologue
    .line 179
    return-void
.end method

.method protected h()Z
    .registers 2

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method protected n()Z
    .registers 2

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method final p()Landroid/view/View;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->d:Landroid/view/View;

    return-object v0
.end method

.method protected final r()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/Controller;->b:Landroid/os/Bundle;

    return-object v0
.end method
