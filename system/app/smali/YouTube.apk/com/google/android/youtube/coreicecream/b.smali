.class public Lcom/google/android/youtube/coreicecream/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/b;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Typeface;
    .registers 3

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/b;->b:Z

    if-nez v0, :cond_15

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/b;->b:Z

    .line 81
    :try_start_7
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "Roboto-Light.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/b;->c:Landroid/graphics/Typeface;
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_15} :catch_18

    .line 86
    :cond_15
    :goto_15
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/b;->c:Landroid/graphics/Typeface;

    return-object v0

    .line 83
    :catch_18
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/b;->c:Landroid/graphics/Typeface;

    goto :goto_15
.end method

.method public final a(Landroid/content/Context;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 46
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v3

    const/16 v4, 0xd

    if-lt v3, v4, :cond_21

    .line 47
    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x2d0

    if-lt v2, v3, :cond_1f

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-nez v2, :cond_1f

    .line 49
    :cond_1e
    :goto_1e
    return v0

    :cond_1f
    move v0, v1

    .line 47
    goto :goto_1e

    .line 49
    :cond_21
    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1e

    move v0, v1

    goto :goto_1e
.end method

.method public final b(Landroid/content/Context;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/coreicecream/b;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_21

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v3

    const/16 v4, 0xd

    if-lt v3, v4, :cond_25

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-lt v2, v3, :cond_23

    move v2, v1

    :goto_1f
    if-eqz v2, :cond_22

    :cond_21
    move v0, v1

    :cond_22
    return v0

    :cond_23
    move v2, v0

    goto :goto_1f

    :cond_25
    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2e

    move v2, v1

    goto :goto_1f

    :cond_2e
    move v2, v0

    goto :goto_1f
.end method

.method public final c(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    .line 59
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 60
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 62
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 63
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 64
    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_22

    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public final d(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    .line 68
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 70
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v0

    const/16 v1, 0x80

    if-lt v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
