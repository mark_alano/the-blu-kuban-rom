.class public Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;
.super Lcom/android/ex/carousel/CarouselView;
.source "SourceFile"


# static fields
.field private static f:Landroid/graphics/Bitmap;


# instance fields
.field private b:Landroid/widget/Adapter;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private final e:Lcom/google/android/youtube/coreicecream/ui/d;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/android/youtube/coreicecream/ui/f;

.field private j:Z

.field private k:I

.field private l:F

.field private m:I

.field private n:I

.field private o:F

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Z

.field private v:Ljava/lang/Float;

.field private w:[I

.field private x:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/android/ex/carousel/CarouselView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    new-instance v0, Lcom/google/android/youtube/coreicecream/ui/f;

    invoke-direct {v0}, Lcom/google/android/youtube/coreicecream/ui/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->i:Lcom/google/android/youtube/coreicecream/ui/f;

    .line 88
    const v0, 0x7f0b00c5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->g:Ljava/lang/String;

    .line 89
    const v0, 0x7f0b00c6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->h:Ljava/lang/String;

    .line 91
    sget-object v0, Lcom/google/android/youtube/b;->c:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 92
    const/4 v1, 0x6

    const/high16 v2, 0x3f80

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->o:F

    .line 93
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->k:I

    .line 94
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->l:F

    .line 95
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    .line 96
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->m:I

    .line 97
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->p:F

    .line 98
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->q:I

    .line 99
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->r:I

    .line 100
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->s:I

    .line 101
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->t:I

    .line 102
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->j:Z

    .line 103
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setOverscrollSlots(F)V

    .line 104
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setSwaySensitivity(F)V

    .line 105
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 108
    invoke-virtual {p0, v3, v3, v3, v3}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setBackgroundColor(FFFF)V

    .line 109
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setZOrderOnTop(Z)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b()Lcom/android/ex/carousel/a;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/ex/carousel/a;->k(I)V

    .line 112
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->o:F

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setRadius(F)V

    .line 113
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->k:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setRowCount(I)V

    .line 114
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->l:F

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setRowSpacing(F)V

    .line 115
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setVisibleSlots(I)V

    .line 116
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setVisibleDetails(I)V

    .line 117
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setSlotCount(I)V

    .line 119
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setDrawRuler(Z)V

    .line 120
    const/16 v0, 0x102

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setDetailTextureAlignment(I)V

    .line 122
    sget-object v0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_dc

    .line 123
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020041

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->f:Landroid/graphics/Bitmap;

    .line 126
    :cond_dc
    sget-object v0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setDefaultBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    sget-object v0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setLoadingBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setCardsFaceTangent(Z)V

    .line 131
    const v0, -0x4036f025

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setCardRotation(F)V

    .line 132
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setDragModel(I)V

    .line 133
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    mul-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setPrefetchCardCount(I)V

    .line 134
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->p:F

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setCameraZ(F)V

    .line 136
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->q:I

    iget v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->r:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setCardSize(II)V

    .line 138
    new-instance v0, Lcom/google/android/youtube/coreicecream/ui/d;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/coreicecream/ui/d;-><init>(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->e:Lcom/google/android/youtube/coreicecream/ui/d;

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->e:Lcom/google/android/youtube/coreicecream/ui/d;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setCallback(Lcom/android/ex/carousel/d;)V

    .line 140
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;Landroid/view/View;IIZ)Landroid/graphics/Bitmap;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x4000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    invoke-virtual {p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz p4, :cond_46

    if-eqz v0, :cond_46

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v1, v2, :cond_3c

    :cond_36
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_3c
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->i:Lcom/google/android/youtube/coreicecream/ui/f;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/coreicecream/ui/f;->a(Landroid/graphics/Canvas;)V

    :cond_46
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;Ljava/lang/Float;)Ljava/lang/Float;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-super {p0}, Lcom/android/ex/carousel/CarouselView;->onDetachedFromWindow()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowVisibilityChanged(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->x:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;[I)[I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->w:[I

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Z
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Landroid/widget/Adapter;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b:Landroid/widget/Adapter;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->q:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->r:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .registers 5

    .prologue
    .line 176
    const-wide v0, 0x401921fb54442d18L

    iget v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->m:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 178
    const v1, 0x3fc90fdb

    iget v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v0, v2

    const/high16 v2, 0x4000

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setStartAngle(F)V

    .line 179
    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->s:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->t:I

    return v0
.end method

.method static synthetic j(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->c:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/android/ex/carousel/f;
    .registers 3

    .prologue
    .line 255
    new-instance v0, Lcom/android/ex/carousel/f;

    const/high16 v1, 0x7f07

    invoke-direct {v0, v1}, Lcom/android/ex/carousel/f;-><init>(I)V

    return-object v0
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 332
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->a(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    if-nez v0, :cond_e

    .line 334
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    .line 336
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b()Lcom/android/ex/carousel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/ex/carousel/a;->d(F)V

    .line 337
    return-void
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 241
    const/4 v0, 0x1

    return v0
.end method

.method public destroyRenderScriptGL()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->getRenderScriptGL()Landroid/renderscript/RenderScriptGL;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 265
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b()Lcom/android/ex/carousel/a;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/android/ex/carousel/a;->a(Landroid/renderscript/RenderScriptGL;Lcom/android/ex/carousel/b;)V

    .line 266
    invoke-super {p0}, Lcom/android/ex/carousel/CarouselView;->destroyRenderScriptGL()V

    .line 268
    :cond_11
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->u:Z

    if-nez v0, :cond_9

    .line 364
    new-instance p1, Landroid/graphics/Canvas;

    invoke-direct {p1}, Landroid/graphics/Canvas;-><init>()V

    .line 366
    :cond_9
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 367
    return-void
.end method

.method public final e()I
    .registers 2

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->k:I

    return v0
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->destroyRenderScriptGL()V

    .line 260
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .registers 3
    .parameter

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->u:Z

    if-eqz v0, :cond_a

    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public layout(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->x:Z

    if-nez v0, :cond_7

    .line 394
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/ex/carousel/CarouselView;->layout(IIII)V

    .line 396
    :cond_7
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->x:Z

    .line 388
    invoke-super {p0}, Lcom/android/ex/carousel/CarouselView;->onAttachedToWindow()V

    .line 389
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->requestLayout()V

    .line 401
    new-instance v0, Lcom/google/android/youtube/coreicecream/ui/c;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/coreicecream/ui/c;-><init>(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->post(Ljava/lang/Runnable;)Z

    .line 408
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 3
    .parameter

    .prologue
    .line 301
    check-cast p1, Landroid/os/Bundle;

    .line 302
    const-string v0, "rotation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    .line 303
    const-string v0, "super"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 304
    if-eqz v0, :cond_19

    .line 305
    invoke-super {p0, v0}, Lcom/android/ex/carousel/CarouselView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 307
    :cond_19
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .prologue
    .line 288
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 289
    invoke-super {p0}, Lcom/android/ex/carousel/CarouselView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_10

    .line 291
    const-string v2, "super"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 293
    :cond_10
    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    if-eqz v1, :cond_1f

    .line 294
    const-string v1, "rotation"

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 296
    :cond_1f
    return-object v0
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter

    .prologue
    .line 371
    const/16 v0, 0x8

    if-ne p1, v0, :cond_10

    .line 372
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->requestLayout()V

    .line 373
    new-instance v0, Lcom/google/android/youtube/coreicecream/ui/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/coreicecream/ui/b;-><init>(Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->post(Ljava/lang/Runnable;)Z

    .line 381
    :goto_f
    return-void

    .line 379
    :cond_10
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->onWindowVisibilityChanged(I)V

    goto :goto_f
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .registers 4
    .parameter

    .prologue
    .line 245
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b:Landroid/widget/Adapter;

    if-eqz v0, :cond_e

    .line 247
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->e:Lcom/google/android/youtube/coreicecream/ui/d;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 249
    :cond_e
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b:Landroid/widget/Adapter;

    .line 250
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->e:Lcom/google/android/youtube/coreicecream/ui/d;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 251
    return-void
.end method

.method public setBackgroundBitmap(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 227
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setBackgroundBitmap(Landroid/graphics/Bitmap;)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setZOrderOnTop(Z)V

    .line 229
    return-void
.end method

.method public setCameraZ(F)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 161
    new-array v0, v3, [F

    aput v2, v0, v4

    aput v2, v0, v5

    aput p1, v0, v6

    .line 163
    new-array v1, v3, [F

    aput v2, v1, v4

    aput v2, v1, v5

    iget v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->o:F

    neg-float v2, v2

    aput v2, v1, v6

    .line 164
    new-array v2, v3, [F

    fill-array-data v2, :array_22

    .line 165
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setLookAt([F[F[F)V

    .line 166
    return-void

    .line 164
    nop

    :array_22
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public setCardSize(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 206
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->q:I

    .line 207
    iput p2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->r:I

    .line 209
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 210
    const/high16 v1, 0x3f80

    iget v2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->r:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->q:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 211
    const/16 v1, 0x9

    new-array v1, v1, [F

    .line 212
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 213
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->setDefaultCardMatrix([F)V

    .line 214
    return-void
.end method

.method public setDetailSize(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 217
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->s:I

    .line 218
    iput p2, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->t:I

    .line 219
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->c:Landroid/widget/AdapterView$OnItemClickListener;

    .line 233
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 237
    return-void
.end method

.method public setRadius(F)V
    .registers 2
    .parameter

    .prologue
    .line 156
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->o:F

    .line 157
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setRadius(F)V

    .line 158
    return-void
.end method

.method public setRowCount(I)V
    .registers 2
    .parameter

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setRowCount(I)V

    .line 192
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->k:I

    .line 193
    return-void
.end method

.method public setRowSpacing(F)V
    .registers 2
    .parameter

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setRowSpacing(F)V

    .line 202
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->l:F

    .line 203
    return-void
.end method

.method public setSlotCount(I)V
    .registers 2
    .parameter

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setSlotCount(I)V

    .line 171
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->m:I

    .line 172
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->g()V

    .line 173
    return-void
.end method

.method public setSplitDetailViews(Z)V
    .registers 3
    .parameter

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->j:Z

    if-eq v0, p1, :cond_b

    .line 148
    iput-boolean p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->j:Z

    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->e:Lcom/google/android/youtube/coreicecream/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/ui/d;->onChanged()V

    .line 152
    :cond_b
    return-void
.end method

.method public setVisibleSlots(I)V
    .registers 2
    .parameter

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setVisibleSlots(I)V

    .line 184
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->setVisibleDetails(I)V

    .line 185
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->n:I

    .line 186
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->g()V

    .line 187
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    if-eqz v0, :cond_11

    .line 273
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->b()Lcom/android/ex/carousel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->v:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/ex/carousel/a;->d(F)V

    .line 275
    :cond_11
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/ex/carousel/CarouselView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->u:Z

    .line 277
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->invalidate()V

    .line 278
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter

    .prologue
    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/AdapterCarousel;->u:Z

    .line 283
    invoke-super {p0, p1}, Lcom/android/ex/carousel/CarouselView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 284
    return-void
.end method
